/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Injectable } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationLocationProvider, BackgroundGeolocationAuthorizationStatus, BackgroundGeolocationEvents } from '@ionic-native/background-geolocation/ngx';
import { environment } from 'src/environments/environment';
import { TokenService } from './token.service';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable()
export class GeolocationService {
    constructor(private backgroundGeolocation: BackgroundGeolocation, private http: HTTP){}

    async configureGeolocation(tokenService: TokenService){
        let token = '';

        await tokenService.getTokenFromLocalStorage().then(remoteToken => {
            if(remoteToken != null){
                token = remoteToken.toString();
            }
        });
        
        this.backgroundGeolocation.configure({
            // This objects imports the tracking object from the environment directly too. It does this at the bottom as otherwise they will get overwritten. 
            // Values listed here are merely defaults

            debug: false,
            
            locationProvider: BackgroundGeolocationLocationProvider.DISTANCE_FILTER_PROVIDER,
            
            stationaryRadius: 0,

            distanceFilter: 0,

            startForeground: true,

            interval: 60000,

            fastestInterval: 5000,

            activitiesInterval: 60000,

            stopOnStillActivity: false,

            syncUrl: environment.backendLocation + 'userLocationData',
            url: environment.backendLocation + 'userLocationData',

            syncThreshold: '60',

            pauseLocationUpdates: false,

            saveBatteryOnBackground: false,

            postTemplate: {
                lat: '@latitude',
                lon: '@longitude',
                time: '@time',
                userID: token
            },

            // Import object properties from environment, "..." is the spread operator and adds all properties of that object to this one, overwriting existing ones
            ...environment.tracking
        });

        this.backgroundGeolocation.start();

        console.log("geolocation started");
    }

    reconfigureGeolocation(newToken: string) {
        this.regainLocationAuthorization();
        let token = newToken;

        this.backgroundGeolocation.configure({
            postTemplate: {
                latitude: '@latitude',
                longitude: '@longitude',
                timestamp: '@time',
                userID: token
            }
        });
    }

    // Check for app permissions and gps settings
    regainLocationAuthorization() {
        const bg = this.backgroundGeolocation;
        if (!environment.production) console.log('Checking Authorisation');
        this.backgroundGeolocation.checkStatus()
            .then(function (status) {
                if (status.authorization != BackgroundGeolocationAuthorizationStatus.AUTHORIZED)
                    setTimeout(function (bg) {
                        if (confirm('App requires location tracking permissions and storage permissions. Would you like to open app settings?')) {
                            console.log(bg);
                            bg.showAppSettings();
                        }
                    }, 1000, this.bg);
                if (!status.locationServicesEnabled)
                    setTimeout(function (bg) {
                        if (confirm('App requires your location services to be enabled. Would you like to open your settings?')) {
                            bg.showLocationSettings();
                        }
                    }, 1000, this.bg)

            }).catch(function (reason) {
                if (!environment.production) console.log('Error gaining Authorisation: ' + reason)
            });
    }

    // Start the background geolocation service
    startService(tokenService: TokenService){
        this.regainLocationAuthorization();
        this.configureGeolocation(tokenService);
        this.backgroundGeolocation.start();
    }

    // Stop the background geolocation service
    stopService(){
        this.backgroundGeolocation.stop();
    }

    // Start the background geolocation service after a pause
    restartService(token: string) {
        this.regainLocationAuthorization();
        this.reconfigureGeolocation(token);
        this.backgroundGeolocation.start();
    }

    /* the following functions are for debugging */

    LogStoredLocations(){
        this.backgroundGeolocation.getLocations().then(result => {
            let location;
            for(let i = 0; i < result.length; i++){
                location = result[i];
                console.log('\n' + i);
                console.log('latitude ' + location.latitude);
                console.log('longitude ' + location.longitude);
                console.log('time ' + location.time);
                console.log("\n");
            }
        })
    }

    RemoveStoredLocations(){
        this.backgroundGeolocation.deleteAllLocations();
    }

    ForceSync(){
        console.log('force sync geolocation');
        this.backgroundGeolocation.forceSync()
            .then(() => {
                console.log('force syncing');
            })
            .catch(error => {
                console.log('sync failed: ' + error);
            });
    }
}