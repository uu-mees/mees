/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Injectable } from '@angular/core';
import { GeolocationService } from './geolocation.service';
import { NotificationService } from './notification.service';
import { TokenService } from './token.service';
import { environment } from 'src/environments/environment';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Injectable()
export class ManagerService {
    constructor(private geolocation: GeolocationService,
                private notification: NotificationService,
                private token: TokenService,
                private screenOrientation: ScreenOrientation){}

    setup(frontEnd){
        this.geolocation.startService(this.token);
        this.notification.setup(frontEnd);
        this.LockScreenRotation();
    }

    LogStoredLocations(){
        this.geolocation.LogStoredLocations();
    }

    RemoveStoredLocations(){
        this.geolocation.RemoveStoredLocations();
    }

    ForceSyncLocations(){
        this.geolocation.ForceSync();
    }

    async TokenFromWeb(frontEnd){
        this.token.getTokenFromWeb(frontEnd, this.geolocation);
    }

    TokenFromLocalStorage(){
        return this.token.getTokenFromLocalStorage();
    }

    SendFireBaseToken(){
        this.notification.sendToken();
    }

    LockScreenRotation(){
        if (environment.lockScreenRotationToPortrait) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        }
    }
}