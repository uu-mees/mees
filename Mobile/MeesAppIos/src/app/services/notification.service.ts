/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { TokenService } from './token.service';
import { environment } from 'src/environments/environment';
import { InAppBrowser} from '@ionic-native/in-app-browser/ngx';

@Injectable()
export class NotificationService {
    constructor(private firebase: Firebase, private http: HTTP, private token: TokenService){}

    setup(frontEnd){
        this.firebase.grantPermission();
        
        // on getting a token from firebase
        this.firebase.getToken()
        .then(token => {
            console.log("firebase token: " + token);
            this.sendToken();
        })
        .catch(err => {
            console.log("firebase error: " + err);
        });

        // on getting a refresh token from firebase
        this.firebase.onTokenRefresh()
            .subscribe(token => {
            console.log("firebase refresh token: " + token);
            this.sendToken();
        });

        // user opened a notification
        this.firebase.onNotificationOpen()
            .subscribe(data => {
                console.log('notification data: ' + data);

                // check if the notification contains an url
                if (data.url) {
                    const url = environment.frontendLocation + data.url;
                    // open the url
                    frontEnd.executeScript({ code: 'this.document.location = ' + '"' + url + '"' });
                }
            });
    }


    async sendToken() {
        // send firebase token and user token to the backend
        const url = environment.backendLocation + 'FireBaseID';

        let firebaseToken = '';
        let userToken = '';

        await this.firebase.getToken().then(token => {
            firebaseToken = token;
        });

        await this.token.getTokenFromLocalStorage().then(remoteToken => {
            if (remoteToken != null) {
                userToken = remoteToken.toString();
            }
        });

        console.log('firebaseToken: ' + firebaseToken);
        console.log('userToken: ' + userToken);

        const body = {
            'UserIDToken': userToken,
            'FireBaseID': firebaseToken
        };

        const headerDict = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type'
        };

        console.log(url);
        console.log(body);
        console.log(headerDict);

        this.http.setDataSerializer('json');

        // send firebase token and usertoken to backend
        this.http.post(url, body, headerDict)
            .then(response => console.log(response))
            .catch(err => console.log(err));
    }

}