/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ManagerService } from './services/manager.service';
import { GeolocationService } from './services/geolocation.service';
import { NotificationService } from './services/notification.service';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { TokenService } from './services/token.service';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ManagerService,
    GeolocationService,
    NotificationService,
    BackgroundGeolocation,
    Firebase,
    TokenService,
    InAppBrowser,
    HTTP,
    ScreenOrientation
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
