/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component } from '@angular/core';
import { ManagerService } from '../services/manager.service';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { InAppBrowser} from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private platform: Platform, private manager: ManagerService, private inAppBrowser: InAppBrowser) {
    platform.ready().then(() => { 
      this.openFrontEnd();
    });
  }

  openFrontEnd(){
    const frontEnd = this.inAppBrowser.create(environment.frontendLocation, '_blank', environment.production ? 'location=no,zoom=no,toolbar=no' : '');
    this.manager.TokenFromWeb(frontEnd);
    this.manager.setup(frontEnd);
  }

  showDebugButtons(){
    return environment.showDebugButtons;
  }

  LogStoredLocations(){
    this.manager.LogStoredLocations();
  }

  RemoveStoredLocations(){
    this.manager.RemoveStoredLocations();
  }

  async LogToken(){
    await this.manager.TokenFromLocalStorage().then(token => {
      console.log('usertoken: ' + token);
    });
  }

  ForceSyncLocations(){
    this.manager.ForceSyncLocations();
  }

  SendFireBaseToken(){
    this.manager.SendFireBaseToken();
  }
}
