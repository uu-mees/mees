/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// This is regarding the development release of the app
export const environment = {
  production: false,
  // Opens the frontend in a new tab, leaving the internals exposed
  showDebugButtons: true,
  // Backend and frontend locations with trailing slash
  frontendLocation: 'https://mees.uu.nl/',
  backendLocation: 'https://mees.uu.nl/api/',
  lockScreenRotationToPortrait: true,
  // This object gets merged with the constructor object of the geolocation module
  tracking: {
    // Log received data on device if this is set to true
    // gives an error on ios when set to true probably due to plugin incompatibilities
    debug: false,
    // Title and text of the notification shown on the device
    notificationTitle: 'MEES is recording your location',
    notificationText: 'MEES is tracking you in accelerated debug mode',
    // Send location data to the specified back-end server
    syncUrl: 'https://mees.uu.nl/api/userLocationData',
    url: 'https://mees.uu.nl/api/userLocationData',
    syncThreshold: '60',
    // Interval of the generated location updates in milliseconds. For debugging this is every 20 seconds
    interval: 20000,
    // Minimum time between intervals of location updates. This is the fastest interval that your application will carry out
    fastestInterval: 2000,
    // Start the tracking when the device starts (ANDROID ONLY)
    startOnBoot: true,
    // Interval that will be checked what activity is taking place
    activitiesInterval: 20000,
    // Do not stop tracking when the STILL activity is recognized
    stopOnStillActivity: false,
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
