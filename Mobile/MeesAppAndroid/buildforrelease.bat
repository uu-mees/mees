@echo off
echo Building the app for release
echo Prerequisites: Android sdk, Java sdk, ionic, cordova, /building folder
echo Deleting possible old versions...
IF EXIST .\nl.mees.uu.release.apk DEL /F del .\nl.mees.uu.release.apk
echo Building the app with ionic+cordova
timeout 1 >nul
call ionic cordova build android  --prod --release
echo Copying built apk
timeout 1 >nul
call xcopy .\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk .\building\
echo Signing apk using Jarsigner and the keystore in /building
timeout 1 >nul
call jarsigner -verbose -storepass VJYBLALETM -sigalg SHA1withRSA -digestalg SHA1 -keystore .\building\mees.uu.nl.publishingkey.keystore .\building\app-release-unsigned.apk keyforpublishingandroidappsfrommees
echo Compressing...
timeout 1 >nul
call .\building\zipalign.exe -v 4 .\building\app-release-unsigned.apk nl.mees.uu.release.apk
del .\building\app-release-unsigned.apk
echo All done! Please find your apk in the root folder, called nl.mees.uu.release.apk
pause