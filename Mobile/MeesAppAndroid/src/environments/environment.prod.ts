/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export const environment = {
  production: true,
  // Opens the frontend in a new tab, leaving the internals exposed
  showDebugButtons: false,
  // Backend and frontend locations with trailing slash
  frontendLocation: 'https://mees.uu.nl/',
  backendLocation: 'https://mees.uu.nl/api/',
  lockScreenRotationToPortrait: true,
  // This object gets merged with the constructor object of the geolocation module
  tracking: {
    // Log received data on device if this is set to true
    debug: false,
    // Title and text of the notification shown on the device
    notificationTitle: 'MEES is recording your location',
    notificationText: 'MEES is recording your location for research purposes',
    // Send location data to the specified back-end server
    syncUrl: 'https://mees.uu.nl/api/userLocationData',
    url: 'https://mees.uu.nl/api/userLocationData',
    // Interval of the generated location updates in milliseconds. This will be one minute.
    interval: 60000,
    // Minimum time between intervals of location updates. This is the fastest interval that your application will carry out
    fastestInterval: 5000,
    // Start the tracking when the device starts (ANDROID ONLY)
    startOnBoot: true,
    // Interval that will be checked what activity is taking place
    activitiesInterval: 60000,
    // Do not stop tracking when the STILL activity is recognized
    stopOnStillActivity: false,
  }

};
