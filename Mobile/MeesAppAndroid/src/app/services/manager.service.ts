/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { GeolocationService } from './geolocation.service';
import { TokenService } from './token.service';
import { NotificationService } from './notification.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { environment } from '../../environments/environment';

@Injectable()

export class ManagerService {
  constructor(private geolocation: GeolocationService,
    private token: TokenService,
    private screenOrientation: ScreenOrientation,
    private notification: NotificationService) {

    /*
     * Miscellaneous functions
     */
    if (environment.lockScreenRotationToPortrait) this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }


  /*
   * Geolocation functions
   */

  // Geolocation is configured before it is started in the geolocation.service.ts
  geolocationStart() {
    this.geolocation.startService(this.token);
  }

  /*
   * Token functions
   */

  tokenFromWeb(frontEnd) {
    this.token.getTokenFromWeb(frontEnd, this.geolocation);
  }

  /*
   * Notification functions
   */

  notificationSetup(frontEnd) {
    this.notification.notificationSetup(frontEnd);
  }

  /*
   * Debug functions
   */

  geolocationStoredLocations() {
    return this.geolocation.getStoredLocations();
  }

  geolocationRemoveLocations() {
    this.geolocation.removeStoredLocations();
  }

  geolocationForceSynch() {
    this.geolocation.forceSynch();
  }

  tokenFromLocalStorage() {
    return this.token.getTokenFromLocalStorage();
  }

  sendFireBaseToken() {
    this.notification.sendToken();
  }
}
