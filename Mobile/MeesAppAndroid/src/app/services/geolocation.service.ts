/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import {
    BackgroundGeolocation,
    BackgroundGeolocationLocationProvider,
    BackgroundGeolocationLogLevel,
    BackgroundGeolocationEvents,
    BackgroundGeolocationAuthorizationStatus,
} from '@ionic-native/background-geolocation/ngx';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable()
export class GeolocationService {

    constructor(private backgroundGeolocation: BackgroundGeolocation) { }

    async configureGeolocation(tokenService: TokenService) {
        let token = '';

        await tokenService.getTokenFromLocalStorage().then(remoteToken => {
            if (remoteToken != null) {
                token = remoteToken.toString();
            }
        });

        this.backgroundGeolocation.configure({
            // This objects imports the tracking object from the environment directly too. It does this at the bottom as otherwise they will get overwritten. 
            // Values listed here are merely defaults

            // Pop-up on location update
            debug: false,

            // Specify the location provider used. Currently all: GPS | Network | Passive | Fused
            locationProvider: BackgroundGeolocationLocationProvider.DISTANCE_FILTER_PROVIDER,

            // Minimum distance that has to be moved in meters for active tracking to engage
            stationaryRadius: 0,
            // Minimum distance that has to moved before an 'update' event is generated
            distanceFilter: 0,

            startForeground: true,

            // Interval of the generated location updates in milliseconds. This will be one minute.
            interval: 60000,
            // Minimum time between intervals of location updates. This is the fastest interval that your application will carry out
            fastestInterval: 5000,
            // Interval that will be checked what activity is taking place
            activitiesInterval: 60000,
            // Do not stop tracking when the STILL activity is recognized
            stopOnStillActivity: false,

            syncUrl: environment.backendLocation + 'userLocationData',
            url: environment.backendLocation + 'userLocationData',

            // Do not pause location updates when app is paused on iOS
            pauseLocationUpdates: false,
            // Do not switch to significantly less accurate tracking when the app is in background on iOS
            saveBatteryOnBackground: false,

            postTemplate: {
                latitude: '@latitude',
                longitude: '@longitude',
                timestamp: '@time',
                userID: token
            },
            // Import object properties from environment, "..." is the spread operator and adds all properties of that object to this one, overwriting existing ones
            ...environment.tracking,
        });
    }

    reconfigureGeolocation(newToken: string) {
        this.regainLocationAuthorization();
        let token = newToken;

        this.backgroundGeolocation.configure({
            /*
            // Do we need HTTP headers?
            httpHeaders: {
                'user': this.tokenService.getTokenFromLocalStorage()
            },
            */

            postTemplate: {
                latitude: '@latitude',
                longitude: '@longitude',
                timestamp: '@time',
                userID: token
            }
        });
    }
    // Check for app permissions and gps settings
    regainLocationAuthorization() {
        const bg = this.backgroundGeolocation;
        if (!environment.production) console.log('Checking Authorisation');
        this.backgroundGeolocation.checkStatus()
            .then(function (status) {
                if (status.authorization != BackgroundGeolocationAuthorizationStatus.AUTHORIZED)
                    setTimeout(function (bg) {
                        if (confirm('App requires location tracking permissions and storage permissions. Would you like to open app settings?')) {
                            console.log(bg);
                            bg.showAppSettings();
                        }
                    }, 1000, this.bg);
                if (!status.locationServicesEnabled)
                    setTimeout(function (bg) {
                        if (confirm('App requires your location services to be enabled. Would you like to open your settings?')) {
                            bg.showLocationSettings();
                        }
                    }, 1000, this.bg)

            }).catch(function (reason) {
                if (!environment.production) console.log('Error gaining Authorisation: ' + reason)
            });
    }
    // Start the background geolocation service
    startService(tokenService: TokenService) {
        this.regainLocationAuthorization();
        this.configureGeolocation(tokenService);
        this.backgroundGeolocation.start();
    }

    // Start the background geolocation service after a pause
    restartService(token: string) {
        this.regainLocationAuthorization();
        this.reconfigureGeolocation(token);
        this.backgroundGeolocation.start();
    }

    // Stop the background geolocation service (but why would we ever :D)
    stopService() {
        this.backgroundGeolocation.stop();
    }

    serviceStatus() {
        return this.backgroundGeolocation.checkStatus();
    }







    /* The rest is just for debugging! */

    // Retrieve all the locations stored in the local database
    getStoredLocations() {
        return this.backgroundGeolocation.getLocations();
    }

    // Remove all the locations stored in the local database
    removeStoredLocations() {
        return this.backgroundGeolocation.deleteAllLocations();
    }

    getLog() {
        this.backgroundGeolocation.getLogEntries(1000, 0, BackgroundGeolocationLogLevel.ERROR).then(function (errors) {
            errors.forEach((error) => {
                console.log('_ _ ERROR _ _ _')
                console.log(error.id + ' ' + error.timestamp);
                console.log('Message: ' + error.message);
            });
            console.log('__________________');
        });

        this.backgroundGeolocation.getLogEntries(1000, 0, BackgroundGeolocationLogLevel.WARN).then(function (errors) {
            errors.forEach((error) => {
                console.log('_ _ WARNING _ _ _')
                console.log(error.id + ' ' + error.timestamp);
                console.log('Message: ' + error.message);
            });
            console.log('__________________');
        });

        this.backgroundGeolocation.getLogEntries(1000, 0, BackgroundGeolocationLogLevel.DEBUG).then(function (errors) {
            errors.forEach((error) => {
                console.log('_ _DEBUG _ _ _')
                console.log(error.id + ' ' + error.timestamp);
                console.log('Message: ' + error.message);
            });
            console.log('__________________');
        });

        this.backgroundGeolocation.getLogEntries(1000, 0, BackgroundGeolocationLogLevel.INFO).then(function (errors) {
            errors.forEach((error) => {
                console.log('_ _INFO _ _ _')
                console.log(error.id + ' ' + error.timestamp);
                console.log('Message: ' + error.message);
            });
            console.log('__________________');
        });

        this.backgroundGeolocation.getLogEntries(1000, 0, BackgroundGeolocationLogLevel.TRACE).then(function (errors) {
            errors.forEach((error) => {
                console.log('_ _TRACE _ _ _')
                console.log(error.id + ' ' + error.timestamp);
                console.log('Message: ' + error.message);
            });
            console.log('__________________');
        });
    }

    forceSynch() {
        console.log('synch force...');
        this.backgroundGeolocation.forceSync().catch((error) => {
            console.log('synch failed: ', error);
        });
        console.log('forcesync initialized to ' + environment.backendLocation + 'userLocationData')
    }
}
