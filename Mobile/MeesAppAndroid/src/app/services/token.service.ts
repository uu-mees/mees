/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { GeolocationService } from './geolocation.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  /*
   * Token handling
   *
   * When the front end finishes loading, we start checking every X milliseconds to see if the token that we have stored
   * locally has changed. If so, we update the locally stored token and continue checking.
   *
   * To retrieve the token, use the 'getTokenFromLocalStorage()' method.
   *
   */

  constructor(private storage: Storage) { }

  async getTokenFromWeb(frontEnd, geolocation: GeolocationService) {

    // Initialize the storage module from the constructor
    const storage = this.storage;

    let update = false;
    let pause = false;
    let localTokenNew = '';
    let tokenInterval;

    // Get the current token from local storage and create a variable to store the new value
    let localToken = '';
    await this.getTokenFromLocalStorage().then(token => {
      if (token != null) {
        localToken = token;
      }
    })

    console.log('localToken initialized on ' + localToken);

    // On completion of loading the front end > check the token every 'interval'
    frontEnd.on('loadstop').subscribe(function () {

      tokenInterval = setInterval(function () {

        // @ts-ignore - Read the token from the front end
        frontEnd.executeScript({ code: 'localStorage.getItem(\'id_token\');' }, function (remoteToken) {
          localTokenNew = remoteToken;
        });

        // If geolocation is not yet paused and for two iterations no token has been found > pause background geolocation
        if (!pause && localTokenNew.toString() == '' && localToken.toString() == '') {
          pause = true;
          geolocation.stopService();
        }

        // If the geolocation is paused and for two iterations the token has not been empty > unpause background geolocation
        if (pause && localTokenNew.toString() != '' && localToken.toString() != '') {
          pause = false;
          geolocation.restartService(localTokenNew.toString());
        }

        // Check if the token has been updated: Using .toString() for the types to always match
        if (localTokenNew.toString() != localToken.toString()) {

          // Wait with updating for one iteration as 'debouncing'
          if (update) {

            // If so, update the local token with the new token in both local storage and the running variable
            console.log('token updated from ' + localToken + ' to ' + localTokenNew);
            localToken = localTokenNew;
            storage.set('localToken', localToken);

            // When the token is updated, also reconfigure the geolocation module with the new token
            geolocation.reconfigureGeolocation(localToken.toString());

          }
          // Allows the token to be updated in the next iteration
          update = true;

        } else {

          // Wait for the tokens to be different once before we actually update them
          update = false;

        }

        // Interval of 10 seconds
      }, 10000);
    });

    // Reset the interval timer on exiting the front end
    frontEnd.on('exit').subscribe(function () {
      clearInterval(tokenInterval);
    });
  }

  // Returns the token from the local storage
  getTokenFromLocalStorage(): Promise<string> {
    // Create an opponent promise that times out after its parameter time (ms) expires
    function promiseTimeout(time) {
      return new Promise(function (resolve, reject) {
        setTimeout(function () { resolve(''); }, time);
      });
    };

    // Create a promise that attempts to get the local token
    let getToken = this.storage.get('localToken');

    // Returns the promise that resolves the quickest. GetToken returns the token, promiseTimeout returns an empty string
    return Promise.race([getToken, promiseTimeout(3000)]);
  }
}
