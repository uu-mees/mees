/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';

describe('ManagerService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: NotificationService = TestBed.get(NotificationService);
        expect(service).toBeTruthy();
    });
});
