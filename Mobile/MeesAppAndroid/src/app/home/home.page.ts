/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ManagerService } from '../services/manager.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [ManagerService],
})
export class HomePage {

  constructor(private inAppBrowser: InAppBrowser, private manager: ManagerService) {
    this.openFrontEnd();
  }

  openFrontEnd() {
    // Start the location tracking service
    this.manager.geolocationStart();

    // Opens a webview showing the front end
    const frontEnd = this.inAppBrowser.create(environment.frontendLocation, '_blank', environment.production ? 'location=no,zoom=no' : '');

    // Get the token from the front end
    this.manager.tokenFromWeb(frontEnd);

    // setup push notifications
    this.manager.notificationSetup(frontEnd);
  }

  showDebugButtons() {
    return environment.showDebugButtons;
  }












  // Shows the locally stored locations on the console
  retrieveLocations() {
    console.log('_________________________________________________');

    this.manager.geolocationStoredLocations().then(function (result) {

      let location;
      for (let i = 0; i < result.length; i++) {
        location = result[i];
        console.log(i + '.');
        console.log('latitude  = ' + location.latitude);
        console.log('longitude = ' + location.longitude);
        console.log('time      = ' + location.time);
        console.log('_ _ _ _ _ _ _ _');
      }
    });
  }

  forceSynch() {
    this.manager.geolocationForceSynch();
  }

  async retrieveToken() {

    await this.manager.tokenFromLocalStorage().then(token => {
      console.log('Got token ' + token);
    })
  }

  removeLocations() {
    this.manager.geolocationRemoveLocations();
  }

  // send firebase token to backend
  sendFireBaseToken() {
    this.manager.sendFireBaseToken();
  }
}
