/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils {
    /// <summary>
    /// Additional functions available for global usage.
    /// </summary>
    public static class Extensions {
        /// <summary>
        /// Convert a given string using the given format to <see cref="DateTime"/>.
        /// </summary>
        /// <param name="date">A string containing a datetime.</param>
        /// <param name="format">The format which should be used to parse the given datetime string.</param>
        /// <returns>The datetime which was contained in the string.</returns>
        public static DateTime ParseDateTime(string date, string format) {
            DateTime result;
            if (!DateTime.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out result)) {
                throw new Exception("Could not parse DateTime with given format");
            }

            return result;
        }
    }
}
