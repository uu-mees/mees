/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Numerics;
using System.Windows.Media.Media3D;

namespace Utils {
    /// <summary>
    /// Utility functions for <see cref="GeoCoordinate"/>s.
    /// <para>This functionality is based on <see href="https://stackoverflow.com/a/38233843"/>.</para>
    /// </summary>
    public static class GeoUtils {
        /// <summary>
        /// Calculates the end-point from a given source at a given range (meters) and bearing (degrees).
        /// This methods uses simple geometry equations to calculate the end-point.
        /// </summary>
        /// <param name="source">The point of origin.</param>
        /// <param name="range">The range in meters.</param>
        /// <param name="bearing">The bearing in degrees.</param>
        /// <returns>The end-point from the source given the desired range and bearing.</returns>
        public static GeoCoordinate CalculateDerivedPosition(this GeoCoordinate source, double range, double bearing) {
            const double DegreesToRadians = Math.PI / 180.0;
            const double RadiansToDegrees = 180.0 / Math.PI;

            double latA = source.Latitude * DegreesToRadians;
            double lonA = source.Longitude * DegreesToRadians;
            double angularDistance = range / EarthRadius;
            double trueCourse = bearing * DegreesToRadians;

            double lat = Math.Asin(
                Math.Sin(latA) * Math.Cos(angularDistance) +
                Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            double dlon = Math.Atan2(
                Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
                Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));

            double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

            return new GeoCoordinate(
                lat * RadiansToDegrees,
                lon * RadiansToDegrees,
                source.Altitude);
        }

        private const double RadiansToDegrees = 180.0 / Math.PI;

        private const double DegreesToRadians = Math.PI / 180.0;

        private const double EarthRadius = 6378137.0;

        /// <summary>
        /// Determines whether a point lies inside a rectangle defined by two other points.
        /// </summary>
        /// <param name="point">Point to determine.</param>
        /// <param name="southWestPoint">The north west point of the rectangle.</param>
        /// <param name="northEastPoint">The south east point of the rectangle.</param>
        /// <returns>A bool describing </returns>
        public static bool WithinArea(this GeoCoordinate point, GeoCoordinate southWestPoint, GeoCoordinate northEastPoint) {
            return point.Latitude >= southWestPoint.Latitude &&
             point.Latitude <= northEastPoint.Latitude &&
             point.Longitude >= southWestPoint.Longitude &&
             point.Longitude <= northEastPoint.Longitude;
        }

        /// <summary>
        /// Check if a point is within the specified bounds. If this method returns false the
        /// point is certainly not within the specified area. If it returns true the point 
        /// could be inside the boundary but this is not completely certain.
        /// </summary>
        /// <param name="point">Point to determine.</param>
        /// <param name="northEastPoint">North east point of the area.</param>
        /// <param name="southEastPoint">South east point of the area.</param>
        /// <param name="southWestPoint">South west point of the area.</param>
        /// <param name="northWestPoint">North west point of the area.</param>
        /// <returns></returns>
        public static bool SpecialWithinArea(this GeoCoordinate point, GeoCoordinate northEastPoint, GeoCoordinate southEastPoint, GeoCoordinate southWestPoint, GeoCoordinate northWestPoint) {
            // Calculate half of the distance between each corner of the area
            double d1 = northEastPoint.GetDistanceTo(southEastPoint) / 2;
            double d2 = southEastPoint.GetDistanceTo(southWestPoint) / 2;
            double d3 = southWestPoint.GetDistanceTo(northWestPoint) / 2;
            double d4 = northWestPoint.GetDistanceTo(northEastPoint) / 2;
            // Calculate the bearing for each edge of the area
            double b1 = Bearing(northEastPoint, southEastPoint);
            double b2 = Bearing(southEastPoint, southWestPoint);
            double b3 = Bearing(southWestPoint, northWestPoint);
            double b4 = Bearing(northWestPoint, northEastPoint);
            // Calculate the middle point for each edge of the area
            GeoCoordinate mid0 = northEastPoint.CalculateDerivedPosition(d1, b1);
            GeoCoordinate mid1 = southEastPoint.CalculateDerivedPosition(d2, b2);
            GeoCoordinate mid2 = southWestPoint.CalculateDerivedPosition(d3, b3);
            GeoCoordinate mid3 = northWestPoint.CalculateDerivedPosition(d4, b4);

            // Check if the given coordinate is within the bounds
            return point.Latitude >= Math.Min(southWestPoint.Latitude, mid1.Latitude) &&
             point.Latitude <= Math.Max(northEastPoint.Latitude, mid3.Latitude) &&
             point.Longitude >= Math.Min(southWestPoint.Longitude, mid2.Longitude) &&
             point.Longitude <= Math.Max(northEastPoint.Longitude, mid0.Longitude);
        }

        /// <summary>
        /// Checks whether a given latitude is a valid latitude
        /// </summary>
        /// <param name="latitude">The latitude that you want to test</param>
        /// <returns>True if valid, false is invalid</returns>
        public static bool IsValidLatitude(double latitude) {
            if (latitude < -90 || latitude > 90)
                return false;

            return true;
        }

        /// <summary>
        /// Checks whether a given longitude is a valid longitude
        /// </summary>
        /// <param name="longitude">The longitude that you want to test</param>
        /// <returns>True if valid, false is invalid</returns>
        public static bool IsValidLongitude(double longitude) {
            if (longitude < -180 || longitude > 180)
                return false;

            return true;
        }

        /// source: https://www.movable-type.co.uk/scripts/latlong.html
        /// <summary>
        /// Calculate bearing between start and end GeoCoordinates in degrees.
        /// </summary>
        /// <see href="https://www.movable-type.co.uk/scripts/latlong.html"/>
        /// <param name="startCoordinate">The first coordinate. Note: Order matters.</param>
        /// <param name="endCoordinate">The second coordinate.</param>
        /// <returns>The bearing in degrees.</returns>
        public static double Bearing(GeoCoordinate startCoordinate, GeoCoordinate endCoordinate) {
            GeoCoordinate radStartCoordinate, radEndCoordinate;
            radStartCoordinate = new GeoCoordinate(startCoordinate.Latitude * DegreesToRadians, startCoordinate.Longitude * DegreesToRadians);
            radEndCoordinate = new GeoCoordinate(endCoordinate.Latitude * DegreesToRadians, endCoordinate.Longitude * DegreesToRadians);

            double y = Math.Sin(radEndCoordinate.Longitude - radStartCoordinate.Longitude) * Math.Cos(radEndCoordinate.Latitude);
            double x = Math.Cos(radStartCoordinate.Latitude) * Math.Sin(radEndCoordinate.Latitude) -
                       Math.Sin(radStartCoordinate.Latitude) * Math.Cos(radEndCoordinate.Latitude) * Math.Cos(radEndCoordinate.Longitude - radStartCoordinate.Longitude);
            double bearing = Math.Atan2(y, x) * RadiansToDegrees;

            return bearing;
        }


        //Inspired by: https://www.codeguru.com/cpp/cpp/algorithms/article.php/c5115/Geographic-Distance-and-Azimuth-Calculations.htm
        /// <summary>
        /// Calculate the intersection (if there is one) for two edges.
        /// </summary>
        /// <param name="start1">Start coordinate of the first edge.</param>
        /// <param name="end1">End coordinate of the first edge.</param>
        /// <param name="start2">Start coordinate of the second edge.</param>
        /// <param name="end2">End coordinate of the second edge.</param>
        /// <returns>The coordinate of the intersection, or null if there is no intersection.</returns>
        public static GeoCoordinate CalcIntersection(GeoCoordinate start1, GeoCoordinate end1, GeoCoordinate start2, GeoCoordinate end2) {
            // The length of the edges.
            double d1 = start1.GetDistanceTo(end1);
            double d2 = start2.GetDistanceTo(end2);

            // An edge on the earth can be used to define a plane through the earth.
            // We use this knowledge to calculate the intersection point (if there
            // is one).

            // First we calculate the normals (without normalizing for optimalisation 
            // purposes).
            Vector3D n1 = Vector3D.CrossProduct(CenterVector(start1), CenterVector(end1));
            Vector3D n2 = Vector3D.CrossProduct(CenterVector(start2), CenterVector(end2));

            // Since the intersection vector lies on both planes we calculate this vector
            // by crossing the two normals (which gives us a vector perpendicular to both 
            // normals, aka the intersection vector). 
            Vector3D vIntersection = Vector3D.CrossProduct(n1, n2);
            // Get the coordinate on the surface of the earth.
            vIntersection.Normalize();

            // If the normal could not be computed it means that the lines are parallel
            // i.e. they will never intersect or they overlap.
            if (double.IsNaN(vIntersection.X) || double.IsNaN(vIntersection.Y) || double.IsNaN(vIntersection.Z)) {
                Tuple<double, GeoCoordinate>[] options = new Tuple<double, GeoCoordinate>[]
                {
                    new Tuple<double, GeoCoordinate>(correctIntersection(end1) ? start1.GetDistanceTo(end1) : double.MaxValue, null),
                    new Tuple<double, GeoCoordinate>(correctIntersection(end2) && start1 != end2 ? start1.GetDistanceTo(end2) : double.MaxValue, end2),
                    new Tuple<double, GeoCoordinate>(correctIntersection(start2) && start1 != start2 ? start1.GetDistanceTo(start2) : double.MaxValue, start2)
                };

                double min = options.Min(x => x.Item1);

                if (min == double.MaxValue)
                    return null;

                int index = Array.FindIndex(options, x => x.Item1 == min);

                return options[index].Item2;
            }

            GeoCoordinate intersection = SurfaceCoordinate(vIntersection);

            if (correctIntersection(intersection)) {
                return intersection;
            }

            bool correctIntersection(GeoCoordinate i) {
                // Calculate the distance form each point of both edges to the intersection point
                double d1a = start1.GetDistanceTo(i);
                double d1b = end1.GetDistanceTo(i);
                double d2a = start2.GetDistanceTo(i);
                double d2b = end2.GetDistanceTo(i);

                double marginOfError = 0.000000001;
                // If the intersection is closer to all points than the points of each edge
                // are to one another, we know this is a proper intersection point.
                if ((d1a - d1) <= marginOfError && (d1b - d1) <= marginOfError && (d2a - d2) <= marginOfError && (d2b - d2) <= marginOfError) {
                    return true;
                }
                return false;
            }


            // We haven't found a valid intersection, so we try the intersection vector in the
            // opposite direction.
            vIntersection = (Vector3D.CrossProduct(n2, n1));

            // Get the coordinate on the surface of the earth.
            intersection = SurfaceCoordinate(vIntersection);

            if (correctIntersection(intersection)) {
                return intersection;
            }

            // Both points were invalid which means that the lines don't intersect.
            return null;
        }

        /// <summary>
        /// Calculate the coordinate on the surface of the earth the given vector is
        /// directed towards.
        /// </summary>
        /// <param name="v">The unit vector (in radians) to calculate the surface coordinate for. </param>
        /// <returns>The coordinate on the surface of the earth the given vector is
        /// directed towards.</returns>
        private static GeoCoordinate SurfaceCoordinate(Vector3D v) {
            GeoCoordinate surfacePoint = new GeoCoordinate();
            // Calculate the latitude longitude for the given radian vector
            surfacePoint.Latitude = Math.Atan2(v.Z, Math.Sqrt(v.X * v.X + v.Y * v.Y));
            surfacePoint.Longitude = Math.Atan2(v.Y, v.X);

            // Convert the coordinate to degrees
            surfacePoint.Latitude *= RadiansToDegrees;
            surfacePoint.Longitude *= RadiansToDegrees;

            return surfacePoint;
        }

        /// <summary>
        /// Calculate the vector (in radians) from the center of the earth to the given coordinate.
        /// </summary>
        /// <param name="coordinate">The coordinate to calculate the center vector for.</param>
        /// <returns>The vector (in radians) from the center of the earth to the given coordinate.</returns>
        private static Vector3D CenterVector(GeoCoordinate coordinate) {
            Vector3D v = new Vector3D();
            v.X = (Math.Cos(coordinate.Latitude * DegreesToRadians) * Math.Cos(coordinate.Longitude * DegreesToRadians));
            v.Y = (Math.Cos(coordinate.Latitude * DegreesToRadians) * Math.Sin(coordinate.Longitude * DegreesToRadians));
            v.Z = (Math.Sin(coordinate.Latitude * DegreesToRadians));

            return v;
        }
    }
}
