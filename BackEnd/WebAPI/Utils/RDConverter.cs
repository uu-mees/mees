/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Device.Location;

namespace Utils {

    /// <summary>
    /// Utility functions for converting Rijksdriehoeks coordinates to other formats.
    /// </summary>
    /// <see href="https://www.roelvanlisdonk.nl/2012/11/21/simple-way-for-converting-rijksdriehoek-coordinates-to-lat-and-long-wgs84-in-c/"/>
    public static class RijksdriehoekComponent {
        /// <summary>
        /// Convert the given Rijksdriehoeks coordinates to GPS coordinates.
        /// </summary>
        /// <param name="x">The x component of the coordinate.</param>
        /// <param name="y">The y component of the coordinate.</param>
        /// <returns>The corresponding GPS coordinates.</returns>
        public static GeoCoordinate ConvertToLatLong(double x, double y) {
            // The city "Amersfoort" is used as reference for "Rijksdriehoek" coordinates.
            int referenceRdX = 155000;
            int referenceRdY = 463000;
            double dX = (double)(x - referenceRdX) * (double)(Math.Pow(10, -5));
            double dY = (double)(y - referenceRdY) * (double)(Math.Pow(10, -5));
            double sumN =
            (3235.65389 * dY) +
            (-32.58297 * Math.Pow(dX, 2)) +
            (-0.2475 * Math.Pow(dY, 2)) +
            (-0.84978 * Math.Pow(dX, 2) * dY) +
            (-0.0655 * Math.Pow(dY, 3)) +
            (-0.01709 * Math.Pow(dX, 2) * Math.Pow(dY, 2)) +
            (-0.00738 * dX) +
            (0.0053 * Math.Pow(dX, 4)) +
            (-0.00039 * Math.Pow(dX, 2) * Math.Pow(dY, 3)) +
            (0.00033 * Math.Pow(dX, 4) * dY) +
            (-0.00012 * dX * dY);
            double sumE =
            (5260.52916 * dX) +
            (105.94684 * dX * dY) +
            (2.45656 * dX * Math.Pow(dY, 2)) +
            (-0.81885 * Math.Pow(dX, 3)) +
            (0.05594 * dX * Math.Pow(dY, 3)) +
            (-0.05607 * Math.Pow(dX, 3) * dY) +
            (0.01199 * dY) +
            (-0.00256 * Math.Pow(dX, 3) * Math.Pow(dY, 2)) +
            (0.00128 * dX * Math.Pow(dY, 4)) +
            (0.00022 * Math.Pow(dY, 2)) +
            (-0.00022 * Math.Pow(dX, 2)) +
            (0.00026 * Math.Pow(dX, 5));
            // The city "Amersfoort" is used as reference for "WGS84" coordinates.
            double referenceWgs84X = 52.15517;
            double referenceWgs84Y = 5.387206;
            double latitude = referenceWgs84X + (sumN / 3600);
            double longitude = referenceWgs84Y + (sumE / 3600);
            // Input
            // x = 122202
            // y = 487250
            //
            // Result
            // "52.372143838117, 4.90559760435224"
            GeoCoordinate result = new GeoCoordinate(latitude, longitude);
            return result;
        }
    }
}
