/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Exposure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;
using WebAPI.Controllers;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    class TestSensorsController {
        private SensorsController sensorsController;
        private Mock<ISensorService> sensorService;
        private ILogger<SensorsController> logger;

        [OneTimeSetUp]
        public void Setup() {
            logger = MockHelper.GetLogger<SensorsController>();
            sensorService = new Mock<ISensorService>();
            sensorsController = new SensorsController(sensorService.Object, logger);
        }

        [Test]
        public void Sensor_LatestSensorBatches_AreaNull() {
            // Act
            IActionResult result = sensorsController.GetLatestSensorBatches(null);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void Sensor_LatestSensorBatches_NoSensors() {
            // Act
            AreaModel area = new AreaModel()
            {
                SouthWestLatitude = 20,
                SouthWestLongitude = 20,
                NorthEastLatitude = 30,
                NorthEastLongitude = 30
            };
            IActionResult result = sensorsController.GetLatestSensorBatches(area);

            // Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public void Sensor_LatestSensorBatches_Ok() {
            // Arrange
            sensorService.Setup(x => x.GetSensors(It.IsAny<GeoCoordinate>(), It.IsAny<GeoCoordinate>()))
                .Returns((GeoCoordinate a, GeoCoordinate b) => GetSensors());

            // Act
            AreaModel area = new AreaModel()
            {
                SouthWestLatitude = 20,
                SouthWestLongitude = 20,
                NorthEastLatitude = 30,
                NorthEastLongitude = 30
            };
            IActionResult result = sensorsController.GetLatestSensorBatches(area);

            // Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), result);
        }

        [Test]
        public void Sensor_HistoricalSensorBatches_IdEmptyString() {
            // Act
            TimeIntervalModel interval = new TimeIntervalModel()
            {
                TimeFrom = DateTime.MinValue,
                TimeTo = DateTime.MaxValue
            };
            IActionResult result = sensorsController.GetHistoricalSensorBatches("", interval);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void Sensor_HistoricalSensorBatches_IdNull() {
            // Act
            TimeIntervalModel interval = new TimeIntervalModel()
            {
                TimeFrom = DateTime.MinValue,
                TimeTo = DateTime.MaxValue
            };
            IActionResult result = sensorsController.GetHistoricalSensorBatches(null, interval);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void Sensor_HistoricalSensorBatches_IntervalNull() {
            // Act
            IActionResult result = sensorsController.GetHistoricalSensorBatches("sensor1", null);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void Sensor_HistoricalSensorBatches_NoBatches() {
            // Act
            TimeIntervalModel interval = new TimeIntervalModel()
            {
                TimeFrom = DateTime.MinValue,
                TimeTo = DateTime.MaxValue
            };
            IActionResult result = sensorsController.GetHistoricalSensorBatches("sensor1", interval);

            // Assert
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public void Sensor_HistoricalSensorBatches_Ok() {
            // Arrange
            sensorService.Setup(x => x.GetSensorBatches(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns((string id, DateTime timeFrom, DateTime timeTo) => GetBatches());

            // Act
            TimeIntervalModel interval = new TimeIntervalModel()
            {
                TimeFrom = DateTime.MinValue,
                TimeTo = DateTime.MaxValue
            };
            IActionResult result = sensorsController.GetHistoricalSensorBatches("sensor1", interval);

            // Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), result);
        }

        private List<Sensor> GetSensors() {
            return new List<Sensor>
            {
                new Sensor(){ Id = "sensor1", Label = "s1", Latitude = 25, Longitude = 25 },
            };
        }

        private List<SensorMeasurementBatch> GetBatches() {
            return new List<SensorMeasurementBatch>
            {
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 1),
                    TimeStampTo = new DateTime(2018, 1, 2), TimeStampPolled = new DateTime(2018, 1, 2) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 2),
                    TimeStampTo = new DateTime(2018, 1, 3), TimeStampPolled = new DateTime(2018, 1, 3) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 3),
                    TimeStampTo = new DateTime(2018, 1, 4), TimeStampPolled = new DateTime(2018, 1, 4) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 4),
                    TimeStampTo = new DateTime(2018, 1, 5), TimeStampPolled = DateTime.MaxValue },
            };
        }
    }
}
