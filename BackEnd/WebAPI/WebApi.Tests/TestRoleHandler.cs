/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Domain;
using Services.Implementations;
using Services.Interfaces;
using WebAPI.Authorization;
using Microsoft.AspNetCore.Authorization;
using Moq;
using Domain.Administration;
using System.Security.Claims;

namespace WebApi.Tests {
    [TestFixture]
    class TestRoleHandler {
        RoleHandler roleHandler;


        [OneTimeSetUp]
        public void Setup() {
            roleHandler = new RoleHandler();
        }


        [Test]
        public void HandleAsync_NoRequirements() {
            // Arrange
            ClaimsPrincipal user = new ClaimsPrincipal();
            AuthorizationHandlerContext context = new AuthorizationHandlerContext(new List<IAuthorizationRequirement>(), user, new { });

            // Act
            Task t = roleHandler.HandleAsync(context);
            t.Wait();

            // Assert
            Assert.IsTrue(context.HasFailed == false);
            Assert.IsTrue(context.HasSucceeded == false);
        }

        [Test]
        public void HandleAsync_RoleRequirement_DenyNoAdmin() {
            // Arrange
            ClaimsPrincipal user = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userRole", Role.Regular.ToString())
            }));

            AuthorizationHandlerContext context = new AuthorizationHandlerContext(new List<IAuthorizationRequirement>()
            {
                new RoleRequirement(Role.Administrator)
            }, user, new { });


            // Act
            Task t = roleHandler.HandleAsync(context);
            t.Wait();

            // Assert
            Assert.IsFalse(context.HasSucceeded);
        }


        [Test]
        public void HandleAsync_RoleRequirement_AcceptAdmin() {
            // Arrange
            ClaimsPrincipal user = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userRole", Role.Administrator.ToString())
            }));

            AuthorizationHandlerContext context = new AuthorizationHandlerContext(new List<IAuthorizationRequirement>()
            {
                new RoleRequirement(Role.Administrator)
            }, user, new { });


            // Act
            Task t = roleHandler.HandleAsync(context);
            t.Wait();

            // Assert
            Assert.IsTrue(context.HasSucceeded);
        }
    }
}
