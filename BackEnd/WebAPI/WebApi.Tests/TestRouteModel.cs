/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    class TestRouteModel {
        [Test]
        public void ValidRoute() {
            // Arrange
            string request1 = "0,0,0,0,car,fastest";
            string request2 = "52.08767,5.16412,52.08338,5.17649,bike,shortest";

            RouteModel expectedResult1 = new RouteModel()
            {
                StartLat = 0,
                StartLong = 0,
                DestLat = 0,
                DestLong = 0,
                Vehicle = "car",
                Weighting = "fastest"
            };

            RouteModel expectedResult2 = new RouteModel()
            {
                StartLat = 52.08767,
                StartLong = 5.16412,
                DestLat = 52.08338,
                DestLong = 5.17649,
                Vehicle = "bike",
                Weighting = "shortest"
            };

            // Act
            RouteModel result1 = RouteModel.TryParse(request1);
            RouteModel result2 = RouteModel.TryParse(request2);

            // Assert
            Assert.NotNull(result1);
            Assert.NotNull(result2);
            Assert.IsTrue(AreEqualRouteModels(result1, expectedResult1));
            Assert.IsTrue(AreEqualRouteModels(result2, expectedResult2));
        }

        private bool AreEqualRouteModels(RouteModel leftModel, RouteModel rightModel) {
            // Assume that null models are not equal because their TryParse params might not be equal.
            if (leftModel == null || rightModel == null)
                return false;

            if (!(leftModel.Start == rightModel.Start))
                return false;

            if (!(leftModel.Destination == rightModel.Destination))
                return false;

            if (!(leftModel.Vehicle == rightModel.Vehicle))
                return false;

            if (!(leftModel.Weighting == rightModel.Weighting))
                return false;

            return true;
        }

        [Test]
        public void InvalidVehicle() {
            // Arrange 
            string q1 = "";
            string q2 = null;
            string request1 = "0,0,0,0," + q1 + ",shortest";
            string request2 = "0,0,0,0," + q2 + ",shortest";

            // Act
            RouteModel result1 = RouteModel.TryParse(request1);
            RouteModel result2 = RouteModel.TryParse(request2);

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
        }

        [Test]
        public void InvalidWeighting() {
            // Arrange
            string q1 = "";
            string q2 = null;
            string request1 = "0,0,0,0,bike," + q1;
            string request2 = "0,0,0,0,bike," + q2;

            // Act
            RouteModel result1 = RouteModel.TryParse(request1);
            RouteModel result2 = RouteModel.TryParse(request2);

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
        }

        [Test]
        public void InvalidStartCoordinate() {
            // Arrange 
            string request1 = "-181,181,0,0,bike,shortest";
            string request2 = "181,-181,0,0,bike,shortest";
            string request3 = ",,0,0,bike,shortest";

            // Act
            RouteModel result1 = RouteModel.TryParse(request1);
            RouteModel result2 = RouteModel.TryParse(request2);
            RouteModel result3 = RouteModel.TryParse(request3);

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
            Assert.Null(result3);
        }

        [Test]
        public void InvalidDestination() {
            // Arrange
            string request1 = "0,0,-181,181,bike,shortest";
            string request2 = "0,0,181,-181,bike,shortest";
            string request3 = "0,0,,,bike,shortest";

            // Act
            RouteModel result1 = RouteModel.TryParse(request1);
            RouteModel result2 = RouteModel.TryParse(request2);
            RouteModel result3 = RouteModel.TryParse(request3);

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
            Assert.Null(result3);
        }
    }
}
