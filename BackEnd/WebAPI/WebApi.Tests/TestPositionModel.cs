/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    public class TestPositionModel {
        [Test]
        public void PositionModel_TryParseString_Valid() {
            // Act
            PositionModel result = PositionModel.TryParse("50, 55");

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(50, result.Latitude);
            Assert.AreEqual(55, result.Longitude);
        }

        [Test]
        public void PositionModel_TryParseString_EmptyString() {
            // Act
            PositionModel result = PositionModel.TryParse("");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void PositionModel_TryParseString_NullString() {
            // Act
            PositionModel result = PositionModel.TryParse(null);

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void PositionModel_TryParseString_InvalidStringParts() {
            // Act
            PositionModel result = PositionModel.TryParse("10,20,30");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void PositionModel_TryParseString_InvalidDoubles() {
            // Act
            PositionModel result = PositionModel.TryParse("a,b");
            PositionModel result2 = PositionModel.TryParse("5 , 1 0");
            PositionModel result3 = PositionModel.TryParse("30.40.50, 60.12.30");

            // Assert
            Assert.Null(result);
            Assert.Null(result2);
            Assert.Null(result3);
        }

        [Test]
        public void PositionModel_TryParseDoubles_ValidDoubles() {
            // Act
            PositionModel result = PositionModel.TryParse(50, 55);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(50, result.Latitude);
            Assert.AreEqual(55, result.Longitude);
        }

        [Test]
        public void PositionModel_TryParseDoubles_InvalidDoubles() {
            // Act
            PositionModel result = PositionModel.TryParse(-500, -550);

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void PositionModel_IsValid_Valid() {
            // Act
            PositionModel result = PositionModel.TryParse(60, 66);

            // Assert
            Assert.NotNull(result);
        }

        [Test]
        public void PositionModel_IsValid_Invalid() {
            // Act
            PositionModel result1 = PositionModel.TryParse(-91, 66);
            PositionModel result2 = PositionModel.TryParse(60, 181);

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
        }
    }
}
