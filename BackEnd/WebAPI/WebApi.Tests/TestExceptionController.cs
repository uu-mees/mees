/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;
using WebAPI.Controllers;

namespace WebApi.Tests {
    [TestFixture]
    class TestExceptionController {

        [OneTimeSetUp]
        public void setup() {
        }

        //This is probably a fairly useless test
        public void Exception_Returns500() {
            ExceptionController exceptionController = new ExceptionController(MockHelper.GetLogger<ExceptionController>());
            ObjectResult result = (ObjectResult)exceptionController.Index();

            Assert.AreEqual(500, result.StatusCode);
        }
    }
}
