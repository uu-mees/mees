/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using WebAPI.Controllers;
using Services.Interfaces;
using System.Collections.Generic;
using WebAPI.WebModels;
using Domain.Tracking;
using Domain.Administration;
using Domain.Exposure;
using DataAccess;
using System.Linq;
using TestUtils;
using WebAPI.DTOs.UserDTOs;
using Services.Implementations;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.WebUtilities;
using Services.DTOs;
using Services.Interfaces.Pollers;

namespace WebApi.Tests {
    [TestFixture]
    class TestFireBaseController {
        ILogger<FireBaseService> fireBaseServiceLogger;
        ILogger<FireBaseController> fireBaseControllerLogger;
        ConfigurationService configurationService;
        [OneTimeSetUp]
        public void Setup() {
            configurationService = new ConfigurationService() { MaxOutOfBoundsPercentage = 1 };
            fireBaseServiceLogger = MockHelper.GetLogger<FireBaseService>();
        }

        [Test]
        public void Test_RetrievesFireBase() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetFireBasesContext(context, new List<FireBase>().AsQueryable());
            IFireBaseService fireBaseService = new FireBaseService(context.Object);
            FireBaseController fireBaseController = new FireBaseController(fireBaseService, fireBaseControllerLogger);

            string userIDToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";
            FireBaseDTO fireBaseDTO = new FireBaseDTO() {
                UserIDToken = userIDToken,
                FireBaseID = "TestStringID"
            };


            string userID = fireBaseController.RetrieveUserIdFromToken(userIDToken);

            User user = new User() {
                Id = Guid.Parse(userID)
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            IActionResult result = fireBaseController.RetrieveFireBaseID(fireBaseDTO);

            Assert.True(result is OkResult);
        }

        [Test]
        public void Test_BadRequestIfNoToken() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetFireBasesContext(context, new List<FireBase>().AsQueryable());
            IFireBaseService fireBaseService = new FireBaseService(context.Object);
            FireBaseController fireBaseController = new FireBaseController(fireBaseService, fireBaseControllerLogger);

            string userIDToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";
            FireBaseDTO fireBaseDTO = new FireBaseDTO() {
                UserIDToken = "",
                FireBaseID = "TestStringID"
            };


            string userID = fireBaseController.RetrieveUserIdFromToken(userIDToken);

            User user = new User() {
                Id = Guid.Parse(userID)
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            IActionResult result = fireBaseController.RetrieveFireBaseID(fireBaseDTO);

            Assert.True(result is BadRequestObjectResult);
        }

        [Test]
        public void Test_BadRequestIfNoFireBaseID() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetFireBasesContext(context, new List<FireBase>().AsQueryable());
            IFireBaseService fireBaseService = new FireBaseService(context.Object);
            FireBaseController fireBaseController = new FireBaseController(fireBaseService, fireBaseControllerLogger);

            string userIDToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";
            FireBaseDTO fireBaseDTO = new FireBaseDTO() {
                UserIDToken = userIDToken,
                FireBaseID = ""
            };


            string userID = fireBaseController.RetrieveUserIdFromToken(userIDToken);

            User user = new User() {
                Id = Guid.Parse(userID)
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            IActionResult result = fireBaseController.RetrieveFireBaseID(fireBaseDTO);

            Assert.True(result is BadRequestObjectResult);
        }

        [Test]
        public void Test_BadRequestIfInvalidUserIDToken() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetFireBasesContext(context, new List<FireBase>().AsQueryable());
            IFireBaseService fireBaseService = new FireBaseService(context.Object);
            FireBaseController fireBaseController = new FireBaseController(fireBaseService, fireBaseControllerLogger);

            string userIDToken = "NotAValidEncodedToken";
            FireBaseDTO fireBaseDTO = new FireBaseDTO() {
                UserIDToken = userIDToken,
                FireBaseID = "TestStringID"
            };

            User user = new User() {
                Id = Guid.NewGuid()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            IActionResult result = fireBaseController.RetrieveFireBaseID(fireBaseDTO);

            Assert.True(result is BadRequestObjectResult);
        }

        [Test]
        public void Test_BadRequestIfUserIDDoesntExist() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetFireBasesContext(context, new List<FireBase>().AsQueryable());
            IFireBaseService fireBaseService = new FireBaseService(context.Object);
            FireBaseController fireBaseController = new FireBaseController(fireBaseService, fireBaseControllerLogger);

            string userIDToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";
            FireBaseDTO fireBaseDTO = new FireBaseDTO() {
                UserIDToken = userIDToken,
                FireBaseID = "TestStringID"
            };

            User user = new User() {
                Id = Guid.NewGuid()
            };

            context = MockHelper.GetUserContext(context, (new User[] { }).AsQueryable());

            IActionResult result = fireBaseController.RetrieveFireBaseID(fireBaseDTO);

            Assert.True(result is BadRequestObjectResult);
        }
    }
}
