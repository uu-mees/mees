/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using Services.Interfaces;
using WebAPI.Controllers;
using WebAPI.DTOs;
using NUnit.Framework;
using Moq;
using Microsoft.AspNetCore.Mvc;
using System.Device.Location;
using WebAPI.WebModels;
using Domain.Administration;
using WebAPI.DTOs.UserDTOs;
using Microsoft.Extensions.Logging;
using TestUtils;

namespace WebApi.Tests {
    class TestRatingController {

        private ILogger<RatingsController> logger;

        [OneTimeSetUp]
        public void Setup() {
            logger = MockHelper.GetLogger<RatingsController>();
        }

        [Test]
        public void RatingsController_GetRatings() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatings(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new List<UserRating>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatings(new PositionModel() { Latitude = 20, Longitude = 20 }, 20);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public void RatingsController_GetRatings_BadPosition() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatings(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new List<UserRating>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatings(null, 20);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);


        }

        [Test]
        public void RatingsController_GetRatings_BadRadius() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatings(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new List<UserRating>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatings(new PositionModel() { Latitude = 20, Longitude = 20 }, 0);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void RatingsController_GetAverage() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetAverageRating(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => 0);

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetAverageRating(new PositionModel() { Latitude = 20, Longitude = 20 }, 20);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public void RatingsController_GetAverage_BadPosition() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetAverageRating(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => 0);

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetAverageRating(null, 20);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);


        }

        [Test]
        public void RatingsController_GetAverage_BadRadius() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetAverageRating(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => 0);

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetAverageRating(new PositionModel() { Latitude = 20, Longitude = 20 }, 0);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void RatingsController_PostRating() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.AddRating(It.IsAny<UserRating>()))
                .Returns((UserRating r) => new UserRating());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.PostRating(new UserRatingDTO() { Position = PositionModel.TryParse(0, 0), Rating = 2 });

            // Assert
            Assert.IsInstanceOf<OkResult>(result);

        }

        [Test]
        public void RatingsController_PostRating_BadRating() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.AddRating(It.IsAny<UserRating>()))
                .Returns((UserRating r) => new UserRating());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.PostRating(null);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);

        }

        [Test]
        public void RatingsController_PostRating_BadService() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.AddRating(It.IsAny<UserRating>()))
                .Returns((UserRating r) => null);

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.PostRating(new UserRatingDTO() { Position = PositionModel.TryParse(0, 0), Rating = 2 });

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);

        }

        [Test]
        public void RatingsController_GetRatingNumbers() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatingNumbers(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new Dictionary<float, int>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatingNumbers(new PositionModel() { Latitude = 20, Longitude = 20 }, 20);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public void RatingsController_GetRatingNumbers_BadPosition() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatingNumbers(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new Dictionary<float, int>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatingNumbers(null, 20);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void RatingsController_GetRatingNumbers_BadRadius() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetRatingNumbers(It.IsAny<GeoCoordinate>(), It.IsAny<float>()))
                .Returns((GeoCoordinate a, float b) => new Dictionary<float, int>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetRatingNumbers(new PositionModel() { Latitude = 20, Longitude = 20 }, 0);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void RatingsController_GetAllRatings() {
            // Arrange
            Mock<IRatingService> service = new Mock<IRatingService>();
            service.Setup(x => x.GetAllRatings())
                .Returns(new List<UserRating>());

            RatingsController ratingsController = new RatingsController(service.Object, logger);

            // Act
            IActionResult result = ratingsController.GetAllRatings();

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }
    }
}
