/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Text;
using TestUtils;
using WebAPI.Controllers;
using WebAPI.DTOs;
using WebAPI.DTOs.UserDTOs;

namespace WebApi.Tests {
    [TestFixture]
    public class TestTokenController {
        private User user;
        private string username, password;
        private Mock<ConfigurationService> mockConfigurationService;
        private Mock<IAuthenticationService> mockAuthenticationService;
        private AuthTokenConfiguration tokenConfiguration;
        private ILogger<TokenController> logger;

        [OneTimeSetUp]
        public void Setup() {
            user = new User { UserName = "victor", Id = Guid.NewGuid(), Email = "victor@test.nl" };
            username = "asdf";
            password = "asdf";
            tokenConfiguration = new AuthTokenConfiguration() {
                Issuer = "localhost:50500",
                TimeToLiveMinutes = 20,
                AuthTokenKey = "sleutelsleutelsleutelsleutelsleutelsleutelsleutel",
            };

            mockConfigurationService = new Mock<ConfigurationService>();
            mockConfigurationService.Setup(x => x.AuthTokenConfiguration).Returns(tokenConfiguration);

            mockAuthenticationService = new Mock<IAuthenticationService>();
            mockAuthenticationService.Setup(x => x.CheckLogin(username, password)).Returns(user);

            logger = MockHelper.GetLogger<TokenController>();
        }

        [Test]
        public void Token_AuthorizedResult() {
            // Arrange
            TokenController controller = new TokenController(mockConfigurationService.Object, mockAuthenticationService.Object, logger);

            // Act
            IActionResult valid = controller.GenerateToken(new UserAuthenticationDTO() { Username = username, Password = password });

            // Assert
            Assert.True(valid is OkObjectResult);
        }


        [Test]
        public void Token_UnauthorizedResult() {
            // Arrange
            TokenController controller = new TokenController(mockConfigurationService.Object, mockAuthenticationService.Object, logger);

            // Act
            IActionResult invalid = controller.GenerateToken(new UserAuthenticationDTO() { Username = username, Password = "wrongPassword" });

            // Assert
            Assert.True(invalid is UnauthorizedResult);
        }
    }
}
