/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    public class TestAreaModel {
        [Test]
        public void AreaModel_TryParse_Valid() {
            // Act
            AreaModel result = AreaModel.TryParse("10,20,30,40");

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(10, result.SouthWestLatitude);
            Assert.AreEqual(20, result.SouthWestLongitude);
            Assert.AreEqual(30, result.NorthEastLatitude);
            Assert.AreEqual(40, result.NorthEastLongitude);
        }

        [Test]
        public void AreaModel_TryParse_EmptyString() {
            // Act
            AreaModel result = AreaModel.TryParse("");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void AreaModel_TryParse_NullString() {
            // Act
            AreaModel result = AreaModel.TryParse(null);

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void AreaModel_TryParse_InvalidStringParts() {
            // Act
            AreaModel result = AreaModel.TryParse("20,20,20");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void AreaModel_TryParse_InvalidFloats() {
            // Act
            AreaModel result = AreaModel.TryParse("a,b,c,d");
            AreaModel result2 = AreaModel.TryParse("50.30.40,50.30.40,50.30.40,50.30.40");
            AreaModel result3 = AreaModel.TryParse("5 1,53 0,1 5,5 2");

            // Assert
            Assert.Null(result);
            Assert.Null(result2);
            Assert.Null(result3);
        }

        [Test]
        public void AreaModel_IsValid_Valid() {
            // Act
            AreaModel result1 = AreaModel.TryParse("10,10,20,20");
            AreaModel result2 = AreaModel.TryParse("10.53,11.34,20.99,60.44");

            // Assert
            Assert.NotNull(result1);
            Assert.NotNull(result2);
        }

        [Test]
        public void AreaModel_IsValid_InvalidLatitude() {
            // Act
            AreaModel result1 = AreaModel.TryParse("-91,10,10,10");
            AreaModel result2 = AreaModel.TryParse("10,10,-91,10");
            AreaModel result3 = AreaModel.TryParse("91,10,10,10");
            AreaModel result4 = AreaModel.TryParse("10,10,91,10");

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
            Assert.Null(result3);
            Assert.Null(result4);
        }

        [Test]
        public void AreaModel_IsValid_InvalidLongitude() {
            // Act
            AreaModel result1 = AreaModel.TryParse("10,-181,10,10");
            AreaModel result2 = AreaModel.TryParse("10,10,10,-181");
            AreaModel result3 = AreaModel.TryParse("10,181,10,10");
            AreaModel result4 = AreaModel.TryParse("10,10,10,181");

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
            Assert.Null(result3);
            Assert.Null(result4);
        }

        [Test]
        public void AreaModel_IsValid_InvalidCorners() {
            // Act
            AreaModel result1 = AreaModel.TryParse("10,10,10,10");
            AreaModel result2 = AreaModel.TryParse("10,10,9,10");
            AreaModel result3 = AreaModel.TryParse("10,10,10,9");

            // Assert
            Assert.Null(result1);
            Assert.Null(result2);
            Assert.Null(result3);
        }
    }
}
