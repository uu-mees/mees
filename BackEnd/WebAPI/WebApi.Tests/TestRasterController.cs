/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using WebAPI.Controllers;
using Services.Interfaces;
using System.Collections.Generic;
using WebAPI.WebModels;
using Domain.Exposure;
using DataAccess;
using System.Linq;
using TestUtils;
using Services.Implementations;
using Microsoft.Extensions.Logging;
using Services.DTOs;
using Services.Interfaces.Pollers;

namespace WebApi.Tests {
    [TestFixture]
    class TestRasterController {
        IRasterService mockRasterService;
        Mock<ITriggerService> mockTriggerService;
        private DatabaseContext context;
        Mock<RasterContainer> mockRasterContainer;
        DateTime min;
        ILogger<RasterController> logger;
        ILogger<RasterService> rasterServiceLogger;

        [OneTimeSetUp]
        public void Setup() {
            IRasterContainer rasterContainer = new RasterContainer();
            mockTriggerService = new Mock<ITriggerService>();
            min = DateTime.MinValue;
            logger = MockHelper.GetLogger<RasterController>();
            rasterServiceLogger = MockHelper.GetLogger<RasterService>();
            mockRasterService = new RasterService(context, rasterServiceLogger, rasterContainer);
        }

        [Test]
        public void Raster_NormalResult() {
            //Arrange
            RasterController contr = new RasterController(mockRasterService, mockTriggerService.Object, logger);

            //Act
            IActionResult result = contr.GetRasterImages(min);

            //Assert
            Assert.True(result is OkObjectResult);
        }

        [Test]
        public void GetExposureFromLocation_ValidResult() {
            //Arrange
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());

            IRasterService rasterService = new RasterService(context.Object, MockHelper.GetLogger<RasterService>(), container.Object);
            RasterController contr = new RasterController(rasterService, mockTriggerService.Object, logger);

            PositionModel position = new PositionModel() { Latitude = 7, Longitude = 7 };

            //Act
            IActionResult result = contr.GetExposureFromLocation(position);

            //Assert
            Assert.True(result is OkObjectResult);
        }

        [Test]
        public void GetExposureFromLocation_OutOfBoundsQuery() {
            //Arrange
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());
            IRasterService rasterService = new RasterService(context.Object, MockHelper.GetLogger<RasterService>(), container.Object);
            RasterController contr = new RasterController(rasterService, mockTriggerService.Object, logger);

            PositionModel position = new PositionModel() { Latitude = 10, Longitude = 10 };

            //Act
            IActionResult result = contr.GetExposureFromLocation(position);

            //Assert
            Assert.True(result is OkObjectResult);
        }

        [Test]
        public void GetExposureFromLocation_NullQuery() {
            //Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);


            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            IRasterService rasterService = new RasterService(context.Object, MockHelper.GetLogger<RasterService>(), null);
            RasterController contr = new RasterController(rasterService, mockTriggerService.Object, logger);

            //Act
            IActionResult result = contr.GetExposureFromLocation(null);

            //Assert
            Assert.True(result is BadRequestResult);
        }
        
    }
}
