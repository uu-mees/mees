/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain;
using Domain.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using TestUtils;
using WebAPI.Controllers;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    class TestGeocodingController {
        Mock<IGeocodingService> mockGeocodingService;
        ILogger<GeocodingController> logger;

        [OneTimeSetUp]
        public void Setup() {
            mockGeocodingService = new Mock<IGeocodingService>();
            logger = MockHelper.GetLogger<GeocodingController>();
        }

        [Test]
        public void Geocode_EmptyAddress() {
            // Arrange
            GeocodingController geocodingController = new GeocodingController(mockGeocodingService.Object, logger);

            // Act
            IActionResult emptyResult = geocodingController.Geocode("");
            IActionResult nullResult = geocodingController.Geocode(null);

            // Assert
            Assert.IsInstanceOf<NotFoundResult>(emptyResult);
            Assert.IsInstanceOf<BadRequestResult>(nullResult);
        }

        [Test]
        public void ReverseGeocode_InvalidLatitude() {
            // Arrange
            GeocodingController geocodingController = new GeocodingController(mockGeocodingService.Object, logger);

            // Act
            IActionResult result1 = geocodingController.ReverseGeocode(PositionModel.TryParse(-181, 90));
            IActionResult result2 = geocodingController.ReverseGeocode(PositionModel.TryParse(181, 90));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result1);
            Assert.IsInstanceOf<BadRequestResult>(result2);
        }

        [Test]
        public void ReverseGeocode_InvalidLongitude() {
            // Arrange
            GeocodingController geocodingController = new GeocodingController(mockGeocodingService.Object, logger);

            // Act
            IActionResult result1 = geocodingController.ReverseGeocode(PositionModel.TryParse(90, -181));
            IActionResult result2 = geocodingController.ReverseGeocode(PositionModel.TryParse(90, 181));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result1);
            Assert.IsInstanceOf<BadRequestResult>(result2);
        }

        [Test]
        public void ReverseGeocode_NullRequest() {
            // Arrange
            GeocodingController geocodingController = new GeocodingController(mockGeocodingService.Object, logger);

            // Act
            IActionResult result = geocodingController.ReverseGeocode(null);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public void ReverseGeocode_NormalResult() {
            //Arrange            
            double lat = 0, lng = 0;
            IGeocodingService service = setReverseGeocodeReturnOK(mockGeocodingService, lat, lng).Object;
            GeocodingController geocodingController = new GeocodingController(service, logger);

            //Act
            IActionResult result = geocodingController.ReverseGeocode(PositionModel.TryParse(lat, lng));

            //Assert            
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        private Mock<IGeocodingService> setReverseGeocodeReturnOK(Mock<IGeocodingService> mockGeocoding, double lat, double lng) {
            mockGeocoding.Setup(x => x.ReverseGeocode(lat, lng)).Returns(new GeocodingResponse() { Results = new List<GeocodingResult>() });
            return mockGeocoding;
        }

        [Test]
        public void ReverseGeocode_NullResult() {
            //Arrange            
            double lat = 0, lng = 0;
            IGeocodingService service = setReverseGeocodeReturnNull(mockGeocodingService, lat, lng).Object;
            GeocodingController geocodingController = new GeocodingController(service, logger);

            //Act
            IActionResult result = geocodingController.ReverseGeocode(PositionModel.TryParse(lat, lng));

            //Assert            
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        private Mock<IGeocodingService> setReverseGeocodeReturnNull(Mock<IGeocodingService> mockGeocoding, double lat, double lng) {
            mockGeocoding.Setup(x => x.ReverseGeocode(lat, lng)).Returns<GeocodingResponse>(null);
            return mockGeocoding;
        }

        [Test]
        public void Geocode_NormalResult() {
            //Arrange            
            string address = "Buys Ballot gebouw"; // Doesn't matter
            IGeocodingService service = setGeocodingReturnOK(mockGeocodingService, address).Object;
            GeocodingController geocodingController = new GeocodingController(service, logger);

            //Act
            IActionResult result = geocodingController.Geocode(address);

            //Assert            
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        private Mock<IGeocodingService> setGeocodingReturnOK(Mock<IGeocodingService> mockGeocoding, string address) {
            mockGeocoding.Setup(x => x.Geocode(address)).Returns(new GeocodingResponse() { Results = new List<GeocodingResult>() });
            return mockGeocoding;
        }

        [Test]
        public void Geocode_NullResult() {
            //Arrange            
            string address = "Buys Ballot gebouw"; // Doesn't matter
            IGeocodingService service = setGeocodingReturnNull(mockGeocodingService, address).Object;
            GeocodingController geocodingController = new GeocodingController(service, logger);

            //Act
            IActionResult result = geocodingController.Geocode(address);

            //Assert            
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        private Mock<IGeocodingService> setGeocodingReturnNull(Mock<IGeocodingService> mockGeocoding, string address) {
            mockGeocoding.Setup(x => x.Geocode(address)).Returns<GeocodingResponse>(null);
            return mockGeocoding;
        }
    }
}
