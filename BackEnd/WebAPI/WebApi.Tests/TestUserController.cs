/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using WebAPI.Controllers;
using WebAPI.WebModels;
using Services.Implementations;
using WebAPI.DTOs.UserDTOs;
using Microsoft.Extensions.Logging;
using TestUtils;

namespace WebApi.Tests {
    class TestUserController {
        private UserController userController;
        private Mock<IUserService> userService;
        private ILogger<UserController> logger;

        [OneTimeSetUp]
        public void OneTimeSetUp() {
            userService = new Mock<IUserService>();
            logger = MockHelper.GetLogger<UserController>();

            userController = new UserController(userService.Object, logger);
        }

        [Test]
        public void User_RegisterUser_NullCheck() {
            // Arrange
            userService.Setup(x => x.CreateNewUser(It.IsAny<UserRegistration>()))
                .Returns((UserRegistration x) => new User());

            // Act
            IActionResult result = userController.RegisterUser(null);

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public void User_RegisterUser_OkCheck() {
            // Arrange
            userService.Setup(x => x.CreateNewUser(It.IsAny<UserRegistration>()))
                .Returns((UserRegistration x) => new User());

            // Act
            IActionResult result = userController.RegisterUser(new UserRegistrationDTO()
            {
                Username = "test",
                Password = "testpassword",
                Email = "test@test.com"
            });

            // Assert
            Assert.IsInstanceOf(typeof(OkObjectResult), result);
        }

        [Test]
        public void User_RegisterUser_ServiceFailed() {
            // Arrange
            userService.Setup(x => x.CreateNewUser(It.IsAny<UserRegistration>()))
                .Returns((UserRegistration x) => null);

            // Act
            IActionResult result = userController.RegisterUser(new UserRegistrationDTO()
            {
                Username = "test",
                Password = "testpassword",
                Email = "test@test.com"
            });

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public void GetDailyExposure_NormalResult() {
            // Arrange
            userService.Setup(x => x.GetDailyExposure(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(new List<DailyExposure>() { new DailyExposure() });

            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userId", Guid.NewGuid().ToString())
            });

            userController.ControllerContext = new ControllerContext();
            userController.ControllerContext.HttpContext = new DefaultHttpContext();
            userController.ControllerContext.HttpContext.User = new ClaimsPrincipal(identity);

            // Act
            IActionResult result = userController.GetDailyExposure(new TimeIntervalModel() {
                TimeFrom = new DateTime(2001, 1, 1),
                TimeTo = new DateTime(2001, 1, 31)
            });

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public void GetDailyExposure_FutureInterval() {
            // Arrange
            userService.Setup(x => x.GetDailyExposure(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(new List<DailyExposure>());

            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userId", Guid.NewGuid().ToString())
            });
            userController.ControllerContext = new ControllerContext();
            userController.ControllerContext.HttpContext = new DefaultHttpContext();
            userController.ControllerContext.HttpContext.User = new ClaimsPrincipal(identity);

            // Act
            IActionResult result = userController.GetDailyExposure(new TimeIntervalModel()
            {
                TimeFrom = DateTime.UtcNow + TimeSpan.FromDays(1),
                TimeTo = DateTime.UtcNow + TimeSpan.FromDays(31)
            });

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);

        }

        [Test]
        public void GetDailyExposure_NullInterval() {
            // Arrange
            userService.Setup(x => x.GetDailyExposure(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(new List<DailyExposure>());

            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userId", Guid.NewGuid().ToString())
            });
            userController.ControllerContext = new ControllerContext();
            userController.ControllerContext.HttpContext = new DefaultHttpContext();
            userController.ControllerContext.HttpContext.User = new ClaimsPrincipal(identity);

            // Act
            IActionResult result = userController.GetDailyExposure(null);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);

        }

        [Test]
        public void GetDailyExposure_NoData() {
            // Arrange
            userService.Setup(x => x.GetDailyExposure(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns((ICollection<DailyExposure>)null);

            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim>()
            {
                new Claim("userId", Guid.NewGuid().ToString())
            });
            userController.ControllerContext = new ControllerContext();
            userController.ControllerContext.HttpContext = new DefaultHttpContext();
            userController.ControllerContext.HttpContext.User = new ClaimsPrincipal(identity);

            // Act
            IActionResult result = userController.GetDailyExposure(new TimeIntervalModel()
            {
                TimeFrom = new DateTime(2001, 1, 1),
                TimeTo = new DateTime(2001, 1, 31)
            });

            // Assert
            Assert.AreEqual(((OkObjectResult)result).Value, new OkObjectResult(new List<DailyExposure> { }).Value);
        }

        /// <summary>
        /// Test to see if <see cref="UserController.GeneratePasswordResetToken(string)"/> returns <see cref="OkResult"/>.
        /// </summary>
        [Test]
        public void GeneratePasswordResetToken_OkResult() {
            // Arrange
            PasswordResetToken token = new PasswordResetToken();
            userService.Setup(x => x.GenerateResetToken(It.IsAny<string>())).Returns(token);

            // Act
            IActionResult result = userController.GeneratePasswordResetToken("tEaMkOl@iBr.IE");

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
        }

        /// <summary>
        /// Test to see if <see cref="UserController.GeneratePasswordResetToken(string)"/> returns <see cref="BadRequestResult"/>.
        /// </summary>
        [Test]
        public void GeneratePasswordResetToken_BadRequest() {
            // Arrange
            userService.Setup(x => x.GenerateResetToken(It.IsAny<string>())).Throws<Exception>();

            // Act
            IActionResult result = userController.GeneratePasswordResetToken("openstREEEEEEEEEEEE@tm.ap");

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        /// <summary>
        /// Test to see if <see cref="UserController.ResetPassword(ResetPasswordDTO)"/> returns <see cref="OkResult"/>.
        /// </summary>
        [Test]
        public void ResetPassword_OkResult() {
            // Arrange
            ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO
            {
                Token = "In the beginning God created the heavens and the earth. Now the earth was formless and empty, darkness was over the surface of the deep, and the Spirit of God was hovering over the waters.",
                NewPassword = "And God said, “Let there be light,” and there was light.",
                ConfirmPassword = "And God said, “Let there be light,” and there was light."
            };
            userService.Setup(x => x.ResetPassword(resetPasswordDTO.Token, resetPasswordDTO.NewPassword, resetPasswordDTO.ConfirmPassword)).Returns(true);

            // Act
            IActionResult result = userController.ResetPassword(resetPasswordDTO);

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
        }

        /// <summary>
        /// Test to see if <see cref="UserController.ResetPassword(ResetPasswordDTO)"/> returns <see cref="BadRequestResult"/>.
        /// </summary>
        [Test]
        public void ResetPassword_BadRequest() {
            // Arrange
            ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO
            {
                Token = "grote mannen gebruiken geen sokken maar broeken",
                NewPassword = "Same",
                ConfirmPassword = "Same"
            };
            userService.Setup(x => x.ResetPassword(resetPasswordDTO.Token, resetPasswordDTO.NewPassword, resetPasswordDTO.ConfirmPassword)).Throws<Exception>();

            // Act
            IActionResult result = userController.ResetPassword(resetPasswordDTO);

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }
    }
}
