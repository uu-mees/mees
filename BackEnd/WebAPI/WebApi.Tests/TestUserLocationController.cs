/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using WebAPI.Controllers;
using Services.Interfaces;
using System.Collections.Generic;
using WebAPI.WebModels;
using Domain.Tracking;
using Domain.Administration;
using Domain.Exposure;
using DataAccess;
using System.Linq;
using TestUtils;
using Services.Implementations;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.WebUtilities;
using Services.DTOs;
using Services.Interfaces.Pollers;

namespace WebApi.Tests {
    [TestFixture]
    class TestUserLocationController {

        Raster raster;
        Mock<IRasterContainer> container;
        ILogger<UserLocationController> userlogger;
        private ConfigurationService configurationService;
        private ILogger<ExposureService> logger;
        private ILogger<RasterService> rasterlogger;
        private ILogger<LocationExposureService> locationExposureLogger;
        Mock<ILocationExposureService> mockLocationService;
        Mock<ITriggerService> mockTriggerService;
        private TriggerService triggerService;
        QuestionnaireConfigurationService questionnaireConfigurationService;
        TriggersConfigurationService triggersConfigurationService;

        [OneTimeSetUp]
        public void Setup() {
            raster = GetRaster();
            configurationService = new ConfigurationService() { MaxOutOfBoundsPercentage = 1 };
            logger = MockHelper.GetLogger<ExposureService>();
            rasterlogger = MockHelper.GetLogger<RasterService>();
            locationExposureLogger = MockHelper.GetLogger<LocationExposureService>();
            userlogger = MockHelper.GetLogger<UserLocationController>();
            container = MockHelper.GetRasterContainer(raster);
            questionnaireConfigurationService = new QuestionnaireConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/questionnairesettings.json");
            triggersConfigurationService = new TriggersConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/triggersettings.json");
        }


        [Test]
        public void CorrectlyParsesPacket_GetLocationpackage() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());
            context = MockHelper.GetLocationExposuresContext(context, (new UserLocationExposure[] { }).AsQueryable());
            context = MockHelper.GetDailyExposuresContext(context, (new DailyExposure[] { }).AsQueryable());
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            ExposureService exposureService = new ExposureService(configurationService, rasterService, logger);
            ILocationExposureService locationExposureService = new LocationExposureService(context.Object, locationExposureLogger, exposureService);
            TriggerService triggerService = new TriggerService(context.Object, exposureService, questionnaireConfigurationService, triggersConfigurationService);
            UserLocationController userLocationController = new UserLocationController(locationExposureService, userlogger, triggerService, configurationService);

            UserLocationDataDTO[] testDTOs = new UserLocationDataDTO[10];
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";

            string userId = userLocationController.RetrieveUserIdFromToken(token);

            User user = new User(){
                Id = Guid.Parse(userId),
                DailyExposures = new List<DailyExposure>()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            for (int i = 0; i < testDTOs.Length; i++) {
                testDTOs[i] = new UserLocationDataDTO() {
                    latitude = 10,
                    longitude = 5,
                    userID = token,
                    timestamp = DateTime.UtcNow.AddHours(i).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
                };
            }

            IActionResult result = userLocationController.GetLocationPackage(testDTOs);

            Assert.True(result is OkResult);

        }

        [Test]
        public void ReturnBadRequestInvalidLocation() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());
            context = MockHelper.GetLocationExposuresContext(context, (new UserLocationExposure[] { }).AsQueryable());
            context = MockHelper.GetDailyExposuresContext(context, (new DailyExposure[] { }).AsQueryable());
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            ExposureService exposureService = new ExposureService(configurationService, rasterService, logger);
            ILocationExposureService locationExposureService = new LocationExposureService(context.Object, locationExposureLogger, exposureService);
            TriggerService triggerService = new TriggerService(context.Object, exposureService, questionnaireConfigurationService, triggersConfigurationService);
            UserLocationController userLocationController = new UserLocationController(locationExposureService, userlogger, triggerService, configurationService);

            UserLocationDataDTO[] testDTOs = new UserLocationDataDTO[10];
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";

            string userId = userLocationController.RetrieveUserIdFromToken(token);

            User user = new User(){
                Id = Guid.Parse(userId),
                DailyExposures = new List<DailyExposure>()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            for (int i = 0; i < testDTOs.Length; i++) {
                testDTOs[i] = new UserLocationDataDTO() {
                    latitude = 1000,
                    longitude = 500,
                    userID = token,
                    timestamp = DateTime.UtcNow.AddHours(i).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
                };
            }

            IActionResult result = userLocationController.GetLocationPackage(testDTOs);

            Assert.True(result is BadRequestObjectResult);
        }

        [Test]
        public void IgnoresIfNoUserID() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());
            context = MockHelper.GetLocationExposuresContext(context, (new UserLocationExposure[] { }).AsQueryable());
            context = MockHelper.GetDailyExposuresContext(context, (new DailyExposure[] { }).AsQueryable());
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            ExposureService exposureService = new ExposureService(configurationService, rasterService, logger);
            ILocationExposureService locationExposureService = new LocationExposureService(context.Object, locationExposureLogger, exposureService);
            TriggerService triggerService = new TriggerService(context.Object, exposureService, questionnaireConfigurationService, triggersConfigurationService);
            UserLocationController userLocationController = new UserLocationController(locationExposureService, userlogger, triggerService, configurationService);

            UserLocationDataDTO[] testDTOs = new UserLocationDataDTO[10];
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";

            string userId = userLocationController.RetrieveUserIdFromToken(token);

            User user = new User(){
                Id = Guid.Parse(userId),
                DailyExposures = new List<DailyExposure>()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            for (int i = 0; i < testDTOs.Length; i++) {
                testDTOs[i] = new UserLocationDataDTO() {
                    latitude = 1000,
                    longitude = 500,
                    userID = "",
                    timestamp = DateTime.UtcNow.AddHours(i).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
                };
            }

            IActionResult result = userLocationController.GetLocationPackage(testDTOs);

            Assert.True(result is BadRequestObjectResult);
        }

        [Test]
        public void IgnoreOutOfRangeLocationsAndReturnOkNormally() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());
            context = MockHelper.GetLocationExposuresContext(context, (new UserLocationExposure[] { }).AsQueryable());
            context = MockHelper.GetDailyExposuresContext(context, (new DailyExposure[] { }).AsQueryable());
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            ExposureService exposureService = new ExposureService(configurationService, rasterService, logger);
            ILocationExposureService locationExposureService = new LocationExposureService(context.Object, locationExposureLogger, exposureService);
            TriggerService triggerService = new TriggerService(context.Object, exposureService, questionnaireConfigurationService, triggersConfigurationService);
            UserLocationController userLocationController = new UserLocationController(locationExposureService, userlogger, triggerService, configurationService);

            UserLocationDataDTO[] testDTOs = new UserLocationDataDTO[10];
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";

            string userId = userLocationController.RetrieveUserIdFromToken(token);

            User user = new User(){
                Id = Guid.Parse(userId),
                DailyExposures = new List<DailyExposure>()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            for (int i = 0; i < testDTOs.Length; i++) {
                testDTOs[i] = new UserLocationDataDTO() {
                    latitude = 50,
                    longitude = 20,
                    userID = token,
                    timestamp = DateTime.UtcNow.AddHours(i).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
                };
            }

            IActionResult result = userLocationController.GetLocationPackage(testDTOs);

            Assert.True(result is OkResult);
        }

        [Test]
        public void ReturnBadRequestGivenUserDoesntExist() {
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());
            context = MockHelper.GetLocationExposuresContext(context, (new UserLocationExposure[] { }).AsQueryable());
            context = MockHelper.GetDailyExposuresContext(context, (new DailyExposure[] { }).AsQueryable());
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            ExposureService exposureService = new ExposureService(configurationService, rasterService, logger);
            ILocationExposureService locationExposureService = new LocationExposureService(context.Object, locationExposureLogger, exposureService);
            TriggerService triggerService = new TriggerService(context.Object, exposureService, questionnaireConfigurationService, triggersConfigurationService);
            UserLocationController userLocationController = new UserLocationController(locationExposureService, userlogger, triggerService, configurationService);

            UserLocationDataDTO[] testDTOs = new UserLocationDataDTO[10];
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                    "eyJ1c2VySWQiOiJhMGEzNWIyNy1mZjY3LWU5MTEtOTE3MC1jNDlkZWQwNzRkNTEiLCJ1c2VyUm9sZSI6IkFkbWluaXN0cmF0b3IiLCJuYmYiOjE1NTYyNzU5NDYsImV4cCI6MTU1NjcwNzk0NiwiaWF0IjoxNTU2Mjc1OTQ2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwNTAwLyJ9." +
                    "w4zqPw7ZDlWb-VlTP2-2bBJvDdRMNJYGe2yR3CtrraA";

            User user = new User(){
                Id = Guid.NewGuid(),
                DailyExposures = new List<DailyExposure>()
            };

            context = MockHelper.GetUserContext(context, (new User[] { user }).AsQueryable());

            for (int i = 0; i < testDTOs.Length; i++) {
                testDTOs[i] = new UserLocationDataDTO() {
                    latitude = 1000,
                    longitude = 500,
                    userID = token,
                    timestamp = DateTime.UtcNow.AddHours(i).ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds
                };
            }

            IActionResult result = userLocationController.GetLocationPackage(testDTOs);

            Assert.True(result is BadRequestObjectResult);
        }
        static Raster GetRaster() {
            int rasterId = 0;
            IQueryable<RasterCell> cells = GetCells();

            Raster raster = new Raster()
            {
                NoDataValue = -1,
                Id = rasterId,
                Cells = cells.ToList(),
                CellSize = 10,
                Height = 2,
                Width = 2,
                NorthEastLat = cells.First(x => x.CellIndex == 1).NorthEastLatitude,
                NorthEastLng = cells.First(x => x.CellIndex == 1).NorthEastLongitude,
                SouthWestLat = cells.First(x => x.CellIndex == 2).SouthWestLatitude,
                SouthWestLng = cells.First(x => x.CellIndex == 2).SouthWestLongitude,
                TimeStampPolled = DateTime.Today
            };

            foreach (RasterCell cell in cells) {
                cell.Raster = raster;
                cell.RasterId = rasterId;
            }

            return raster;
        }

        static IQueryable<RasterCell> GetCells() {
            //A square of cells with the following values
            //  5  |  0
            // ----------
            // 15  | 20
            IQueryable<RasterCell> cells = new List<RasterCell>{
                new RasterCell(){CellId = 0, SouthWestLatitude = 10, SouthWestLongitude = 0, NorthEastLatitude = 20, NorthEastLongitude = 10, Value = 5, CellIndex = 0},
                new RasterCell(){CellId = 1, SouthWestLatitude = 10, SouthWestLongitude = 10, NorthEastLatitude = 20, NorthEastLongitude = 20, Value = 0, CellIndex = 1},
                new RasterCell(){CellId = 2, SouthWestLatitude = 0, SouthWestLongitude = 0, NorthEastLatitude = 10, NorthEastLongitude = 10, Value = 15, CellIndex = 2},
                new RasterCell(){CellId = 3, SouthWestLatitude = 0, SouthWestLongitude = 10, NorthEastLatitude = 10, NorthEastLongitude = 20, Value = 20, CellIndex = 3}
            }.AsQueryable();

            return cells;
        }


    }
}
