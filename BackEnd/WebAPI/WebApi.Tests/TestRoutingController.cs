/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain;
using Domain.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.DTOs;
using Services.Interfaces;
using System.Collections.Generic;
using System.Device.Location;
using TestUtils;
using WebAPI.Controllers;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    class TestRoutingController {
        Mock<IRoutingService> mockRoutingService;
        ILogger<RoutingController> logger;

        [OneTimeSetUp]
        public void Setup() {
            mockRoutingService = new Mock<IRoutingService>();
            logger = MockHelper.GetLogger<RoutingController>();
        }

        [Test]
        public void GetRoute_EmptyVehicle() {
            // Arrange
            RoutingController routingController = new RoutingController(mockRoutingService.Object, logger);
            string q1 = "";
            string q2 = null;
            string request1 = "0,0,0,0," + q1 + ",shortest";
            string request2 = "0,0,0,0," + q2 + ",shortest";

            // Act
            IActionResult emptyResult = routingController.GetRoute(RouteModel.TryParse(request1));
            IActionResult nullResult = routingController.GetRoute(RouteModel.TryParse(request2));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(emptyResult);
            Assert.IsInstanceOf<BadRequestResult>(nullResult);
        }

        [Test]
        public void GetRoute_EmptyWeighting() {
            // Arrange
            RoutingController routingController = new RoutingController(mockRoutingService.Object, logger);
            string q1 = "";
            string q2 = null;
            string request1 = "0,0,0,0,bike," + q1;
            string request2 = "0,0,0,0,bike," + q2;

            // Act
            IActionResult emptyResult = routingController.GetRoute(RouteModel.TryParse(request1));
            IActionResult nullResult = routingController.GetRoute(RouteModel.TryParse(request2));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(emptyResult);
            Assert.IsInstanceOf<BadRequestResult>(nullResult);
        }

        [Test]
        public void GetRoute_InvalidStart() {
            // Arrange
            RoutingController routingController = new RoutingController(mockRoutingService.Object, logger);
            string request1 = "-181,181,0,0,bike,shortest";
            string request2 = "181,-181,0,0,bike,shortest";
            string request3 = ",,0,0,bike,shortest";

            // Act
            IActionResult result1 = routingController.GetRoute(RouteModel.TryParse(request1));
            IActionResult result2 = routingController.GetRoute(RouteModel.TryParse(request2));
            IActionResult result3 = routingController.GetRoute(RouteModel.TryParse(request3));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result1);
            Assert.IsInstanceOf<BadRequestResult>(result2);
            Assert.IsInstanceOf<BadRequestResult>(result3);
        }

        [Test]
        public void GetRoute_InvalidDestination() {
            // Arrange
            RoutingController routingController = new RoutingController(mockRoutingService.Object, logger);
            string request1 = "0,0,-181,181,bike,shortest";
            string request2 = "0,0,181,-181,bike,shortest";
            string request3 = "0,0,,,bike,shortest";

            // Act
            IActionResult result1 = routingController.GetRoute(RouteModel.TryParse(request1));
            IActionResult result2 = routingController.GetRoute(RouteModel.TryParse(request2));
            IActionResult result3 = routingController.GetRoute(RouteModel.TryParse(request3));

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result1);
            Assert.IsInstanceOf<BadRequestResult>(result2);
            Assert.IsInstanceOf<BadRequestResult>(result3);
        }

        [Test]
        public void GetRoute_NormalResult() {
            //Arrange            
            double slat = 0, slng = 0, dlat = 0, dlng = 0;
            string vehicle = "bike", weighting = "shortest";
            IRoutingService service = SetGetAllRoutesReturnOK(mockRoutingService, slat, slng, dlat, dlng, vehicle, weighting).Object;
            RoutingController routingController = new RoutingController(service, logger);
            string request = slat + "," + slng + "," + dlat + "," + dlng + "," + vehicle + "," + weighting;
            //Act
            IActionResult result = routingController.GetRoute(RouteModel.TryParse(request));

            //Assert            
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        private Mock<IRoutingService> SetGetAllRoutesReturnOK(Mock<IRoutingService> mockRouting, double sLat, double sLng, double dLat, double dLng, string vehicle, string weighting) {
            GeoCoordinate start = new GeoCoordinate(sLat, sLng), dest = new GeoCoordinate(dLat, dLng);
            mockRouting.Setup(x => x.GetAllRoutes(start, dest, vehicle, weighting)).Returns(new Routes() {
                Count = 0,
                Destination = dest,
                Start = start,
                RouteList = new List<Route>(),
                Vehicle = vehicle,
                Weighting = weighting
            });
            return mockRouting;
        }

        [Test]
        public void GetRoute_NullResult() {
            //Arrange            
            double slat = 0, slng = 0, dlat = 0, dlng = 0;
            string vehicle = "bike", weighting = "shortest";
            IRoutingService service = SetGetAllRoutesReturnNull(mockRoutingService, slat, slng, dlat, dlng, vehicle, weighting).Object;
            RoutingController routingController = new RoutingController(service, logger);
            string request = slat + "," + slng + "," + dlat + "," + dlng + "," + vehicle + "," + weighting;

            //Act
            IActionResult result = routingController.GetRoute(RouteModel.TryParse(request));

            //Assert            
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        private Mock<IRoutingService> SetGetAllRoutesReturnNull(Mock<IRoutingService> mockRouting, double sLat, double sLng, double dLat, double dLng, string vehicle, string weighting) {
            mockRouting.Setup(x => x.GetAllRoutes(new GeoCoordinate(sLat, sLng), new GeoCoordinate(dLat, dLng), vehicle, weighting)).Returns<Routes>(null);
            return mockRouting;
        }
    }
}
