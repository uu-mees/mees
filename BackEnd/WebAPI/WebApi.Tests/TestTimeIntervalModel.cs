/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.WebModels;

namespace WebApi.Tests {
    [TestFixture]
    public class TestTimeIntervalModel {
        [Test]
        public void TimeIntervalModel_TryParse_Valid() {
            // Act
            TimeIntervalModel result = TimeIntervalModel.TryParse("2018-03-12T06:00:00,2018-03-12T09:00:00");

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(new DateTime(2018, 3, 12, 6, 0, 0).ToUniversalTime(), result.TimeFrom);
            Assert.AreEqual(new DateTime(2018, 3, 12, 9, 0, 0).ToUniversalTime(), result.TimeTo);
        }

        [Test]
        public void TimeIntervalModel_TryParse_EmptyString() {
            // Act
            TimeIntervalModel result = TimeIntervalModel.TryParse("");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void TimeIntervalModel_TryParse_NullString() {
            // Act
            TimeIntervalModel result = TimeIntervalModel.TryParse(null);

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void TimeIntervalModel_TryParse_InvalidStringParts() {
            // Act
            TimeIntervalModel result = TimeIntervalModel.TryParse("2018-03-12T06:00:00,2018-03-12T09:00:00,2018-03-12T13:00:00");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void TimeIntervalModel_TryParse_InvalidDateTimes() {
            // Act
            AreaModel result = AreaModel.TryParse("2018-03-12T09:00:00,2018-03-12T06:00:00");

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void TimeIntervalModel_TryParse_IncorrectOrder() {
            // Act
            AreaModel result = AreaModel.TryParse("a,b");
            AreaModel result2 = AreaModel.TryParse("2018-03-1206:00:00,2018-03-12T09:00:00");
            AreaModel result3 = AreaModel.TryParse("2018-03-12T36:00:00,2018-03-12T09:00:00");
            AreaModel result4 = AreaModel.TryParse("-2018-03-12T06:00:00,2018-03-12T09:00:00");

            // Assert
            Assert.Null(result);
            Assert.Null(result2);
            Assert.Null(result3);
            Assert.Null(result4);
        }
    }
}
