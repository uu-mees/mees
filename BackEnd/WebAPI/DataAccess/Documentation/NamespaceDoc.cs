/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
namespace DataAccess {
    /// <summary>
    /// The DataAccess namespace contains database table configurations and data seeders.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace DataAccess.Seeders {
    /// <summary>
    /// The DataAccess.Configurations namespace contains all database seeders.
    /// </summary>
    internal class NamespaceDoc {
    }
}
