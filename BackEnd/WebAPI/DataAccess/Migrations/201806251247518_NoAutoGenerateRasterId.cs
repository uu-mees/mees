namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NoAutoGenerateRasterId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RasterCells", "RasterId", "dbo.Rasters");
            DropPrimaryKey("dbo.PlaceExposureValues");
            DropPrimaryKey("dbo.Rasters");
            CreateTable(
                "dbo.PasswordResetTokens",
                c => new
                    {
                        Token = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Token)
                .ForeignKey("dbo.Users", t => t.Token)
                .Index(t => t.Token);
            
            AddColumn("dbo.DailyExposures", "TotalDuration", c => c.Long(nullable: false));
            AddColumn("dbo.PathLocations", "ElapsedTime", c => c.Long(nullable: false));
            AddColumn("dbo.PlaceExposureValues", "Id", c => c.Guid(nullable: false, identity: true));
            AddColumn("dbo.Triggers", "Handled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Triggers", "Exposure", c => c.Double());
            AddColumn("dbo.Triggers", "Location_Latitude", c => c.Double());
            AddColumn("dbo.Triggers", "Location_Longitude", c => c.Double());
            AddColumn("dbo.Triggers", "StartHourOne", c => c.DateTime());
            AddColumn("dbo.Triggers", "HourOneExposure", c => c.Double());
            AddColumn("dbo.Triggers", "StartHourTwo", c => c.DateTime());
            AddColumn("dbo.Triggers", "HourTwoExposure", c => c.Double());
            AddColumn("dbo.Triggers", "DeltaPercentageDifference", c => c.Double());
            AddColumn("dbo.Triggers", "DeltaExposureDifference", c => c.Double());
            AlterColumn("dbo.Rasters", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.PlaceExposureValues", "Id");
            AddPrimaryKey("dbo.Rasters", "Id");
            AddForeignKey("dbo.RasterCells", "RasterId", "dbo.Rasters", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RasterCells", "RasterId", "dbo.Rasters");
            DropForeignKey("dbo.PasswordResetTokens", "Token", "dbo.Users");
            DropIndex("dbo.PasswordResetTokens", new[] { "Token" });
            DropPrimaryKey("dbo.Rasters");
            DropPrimaryKey("dbo.PlaceExposureValues");
            AlterColumn("dbo.Rasters", "Id", c => c.Guid(nullable: false, identity: true));
            DropColumn("dbo.Triggers", "DeltaExposureDifference");
            DropColumn("dbo.Triggers", "DeltaPercentageDifference");
            DropColumn("dbo.Triggers", "HourTwoExposure");
            DropColumn("dbo.Triggers", "StartHourTwo");
            DropColumn("dbo.Triggers", "HourOneExposure");
            DropColumn("dbo.Triggers", "StartHourOne");
            DropColumn("dbo.Triggers", "Location_Longitude");
            DropColumn("dbo.Triggers", "Location_Latitude");
            DropColumn("dbo.Triggers", "Exposure");
            DropColumn("dbo.Triggers", "Handled");
            DropColumn("dbo.PlaceExposureValues", "Id");
            DropColumn("dbo.PathLocations", "ElapsedTime");
            DropColumn("dbo.DailyExposures", "TotalDuration");
            DropTable("dbo.PasswordResetTokens");
            AddPrimaryKey("dbo.Rasters", "Id");
            AddPrimaryKey("dbo.PlaceExposureValues", new[] { "PlaceSegmentUserId", "TimeStamp" });
            AddForeignKey("dbo.RasterCells", "RasterId", "dbo.Rasters", "Id", cascadeDelete: true);
        }
    }
}
