/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Data.Entity;
using DataAccess.Seeders;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess {
    /// <summary>
    /// Configure the database for the given <see cref="DatabaseContext"/>.
    /// This database is configured to always drop all tables on shutdown.
    /// </summary>
#if DEBUG
    [ExcludeFromCodeCoverage]
    public class DatabaseInitializer : DropCreateDatabaseAlways<DatabaseContext>
#else
    public class DatabaseInitializer : CreateDatabaseIfNotExists<DatabaseContext>
#endif
    {
        /// <summary>
        /// Seed the given <see cref="DatabaseContext"/> with initial data.
        /// </summary>
        /// <param name="context">
        /// The DatabaseContext which should be seeded.
        /// </param>
        protected override void Seed(DatabaseContext context) {
            DefaultSeeder.Seed(context);

            base.Seed(context);
        }
    }
}
