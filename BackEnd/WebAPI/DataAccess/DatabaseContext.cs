/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Data.Entity;
using DataAccess.Configurations;
using Domain.Exposure;
using Domain.Minigames;
using Domain.Administration;
using Domain.Tracking;
using DataAccess.Configurations.TrackingConfigurations;
using Domain.Questionnaire;
using DataAccess.Configurations.TriggerConfiguration;
using DataAccess.Configurations.MinigameConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess {
    /// <summary>
    /// An implementation of <see cref="DbContext"/> representing the code-first database.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DatabaseContext : DbContext {
        /// <summary>The instance of the <see cref="Sensor"/> database table.</summary>
        public virtual DbSet<Sensor> Sensors { get; set; }
        /// <summary>The instance of the <see cref="DataSource"/> database table.</summary>
        public virtual DbSet<DataSource> DataSources { get; set; }
        /// <summary>The instance of the <see cref="SensorMeasurement"/> database table.</summary>
        public virtual DbSet<SensorMeasurement> SensorMeasurements { get; set; }
        /// <summary>The instance of the <see cref="SensorMeasurementBatch"/> database table.</summary>
        public virtual DbSet<SensorMeasurementBatch> SensorMeasurementBatches { get; set; }
        /// <summary>The instance of the <see cref="SensorComponent"/> database table.</summary>
        public virtual DbSet<SensorComponent> SensorComponents { get; set; }
        /// <summary>The instance of the <see cref="Raster"/> database table.</summary>
        public virtual DbSet<Raster> Rasters { get; set; }
        /// <summary>The instance of the <see cref="RasterCell"/> database table.</summary>
        public virtual DbSet<RasterCell> RasterCells { get; set; }
        /// <summary>The instance of the <see cref="UserRating"/> database table.</summary>
        public virtual DbSet<UserRating> UserRatings { get; set; }
        /// <summary>The instance of the <see cref="UserResearches"/> database table.</summary>
        public virtual DbSet<UserResearches> UserResearches { get; set; }
        /// <summary>The instance of the <see cref="User"/> database table.</summary>
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<DailyExposure> DailyExposures { get; set; }
        public virtual DbSet<UserLocationExposure> UserLocationExposures { get; set; }

        public virtual DbSet<MinigameScores> MinigameScores { get; set; }
        public virtual DbSet<FireBase> FireBases { get; set; }
        public virtual DbSet<Trigger> Triggers { get; set; }
        public virtual DbSet<Questionnaire> Questionnaires { get; set; }

        public virtual DbSet<Researches> Researches { get; set; }

        /// <summary>
        /// The default class constructor.
        /// </summary>
        public DatabaseContext() {

        }

        /// <summary>
        /// An alternate constructor using a connection string
        /// to set up a connection to the database.
        /// </summary>
        /// <param name="conn">
        /// The connection string for connecting to the (remote) database.
        /// </param>
        public DatabaseContext(string conn) : base(conn) {
            Database.SetInitializer(new DatabaseInitializer());
        }

        /// <summary>
        /// Automatically called, this method generates all tables for the database.
        /// </summary>
        /// <param name="modelBuilder">
        /// An instance of the <see cref="DbModelBuilder"/> class, used for building all tables.
        /// </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Properties<string>().Configure(c => c.HasMaxLength(250));
            modelBuilder.Configurations.Add(new DataSourceConfiguration());
            modelBuilder.Configurations.Add(new FireBaseConfiguration());

            modelBuilder.Configurations.Add(new MinigameConfiguration());
            modelBuilder.Configurations.Add(new ResearchConfiguration());
            modelBuilder.Configurations.Add(new UserResearchesConfiguration());

            modelBuilder.Configurations.Add(new SensorConfiguration());
            modelBuilder.Configurations.Add(new SensorMeasurementConfiguration());
            modelBuilder.Configurations.Add(new SensorMeasurementBatchConfiguration());
            modelBuilder.Configurations.Add(new SensorComponentConfiguration());

            modelBuilder.Configurations.Add(new RasterConfiguration());
            modelBuilder.Configurations.Add(new RasterCellConfiguration());

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new PasswordResetTokenConfiguration());
            modelBuilder.Configurations.Add(new UserRatingConfiguration());
            modelBuilder.Configurations.Add(new DailyExposureConfiguration());
            modelBuilder.Configurations.Add(new UserLocationExposureConfiguration());
            modelBuilder.Configurations.Add(new Configurations.TriggerConfiguration.TriggerConfiguration());
            modelBuilder.Configurations.Add(new Configurations.TriggerConfiguration.QuestionnaireConfiguration());
        }
    }
}
