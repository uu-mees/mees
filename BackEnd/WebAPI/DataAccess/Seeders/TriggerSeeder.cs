/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Domain.Administration;
using System.Diagnostics.CodeAnalysis;
using Domain.Questionnaire;

namespace DataAccess.Seeders {
    /// <summary>
    /// A seeder adding initial <see cref="Trigger"/>s data to the database.
    /// </summary>
    [ExcludeFromCodeCoverage]
    class TriggerSeeder {
        public void Seed(DatabaseContext context) {

            //Build a list of initial researches
            List<Trigger> triggers = new List<Trigger>(){
                new DistanceTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "user2").Id },
                new ExposureTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "user2").Id },
                new DefaultTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "user2").Id },
                new DistanceTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "admin").Id },
                new ExposureTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "admin").Id },
                new DefaultTrigger() { Priority = 2, TriggerId = Guid.NewGuid(), UserId = context.Users.FirstOrDefault(u => u.UserName == "admin").Id }
            };

            //Add all the researches to the research table
            foreach (Trigger trigger in triggers) {
                context.Triggers.Add(trigger);
            }

            //save the changes made to the database state
            context.SaveChanges();
        }
    }
}
