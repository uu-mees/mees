/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Linq;
using System.Text;
using Domain.Administration;
using System.Security.Cryptography;
using Domain.Tracking;
using Domain.Exposure;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    /// <summary>
    /// A seeder adding initial <see cref="User"/> data to the database.
    /// </summary>
    /// <seealso cref="DefaultSeeder"/>
    [ExcludeFromCodeCoverage]
    public class UserSeeder {
        /// <summary>
        /// Seed the given <see cref="DatabaseContext"/> with user login information.
        /// </summary>
        /// <param name="context">
        /// The context for which the database should be seeded.
        /// </param>
        public static void SeedUsers(DatabaseContext context,
            string username,
            string password,
            string email,
            string access_token,
            string refresh_token,
            DateTime Date,
            Role role,
            bool researchParticipant) {
            // Create a new User and add this to the Users table.
            User u = new User()
            {
                UserName = username,
                Email = email,
                Role = role,
                TrackLocation = true,
                Height = 170,
                Weight = (float)72.1,
                Sex = "Male",
                DateOfBirth = DateTime.UtcNow
            };


            // Create the corresponding password and add the hashed version to the user's information.
            // This happens after the user is added to the table,
            // as the salt for the user is generated after adding them to the database.
            SHA256 hasher = SHA256.Create();
            byte[] hashedPassword = hasher.ComputeHash(Encoding.UTF8.GetBytes(password))
                                                .Concat(BitConverter.GetBytes(u.Salt.GetHashCode()))
                                                .ToArray();

            u.HashedPassword = hashedPassword;

            context.Users.Add(u);
            context.SaveChanges();

        }

    }
}
