/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    /// <summary>
    /// A seeder adding initial <see cref="UserRating"/> data to the database.
    /// </summary>
    /// <seealso cref="DefaultSeeder"/>
    [ExcludeFromCodeCoverage]
    public class UserRatingSeeder {
        /// <summary>
        /// Seed the given <see cref="DatabaseContext"/> with mock user ratings.
        /// </summary>
        /// <param name="context">
        /// The context for which the database should be seeded.
        /// </param>
        public void Seed(DatabaseContext context) {
            // South west corner: 52.0777202, 5.1448982
            // North east corner: 52.0934792, 5.1847402

            // Build a list of initial UserRatings.
            List<UserRating> ratings = new List<UserRating>()
            {
                new UserRating() { Latitude = 52.0869132f, Longitude = 5.1581592f, Rating = 2, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0777202f, Longitude = 5.1448982f, Rating = 3, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0871491f, Longitude = 5.1741242f, Rating = 5, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0854882f, Longitude = 5.1654122f, Rating = 4, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0903921f, Longitude = 5.1674722f, Rating = 3, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0826121f, Longitude = 5.1575592f, Rating = 2, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0826901f, Longitude = 5.1718502f, Rating = 3, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0890441f, Longitude = 5.1645972f, Rating = 1, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0850092f, Longitude = 5.1593182f, Rating = 3, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0847181f, Longitude = 5.1734372f, Rating = 4, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0890151f, Longitude = 5.1572582f, Rating = 2, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0868782f, Longitude = 5.1745532f, Rating = 4, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0934792f, Longitude = 5.1847402f, Rating = 5, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
                new UserRating() { Latitude = 52.0934792f, Longitude = 5.1847402f, Rating = 1, TimeStamp = DateTime.UtcNow, UserId = context.Users.FirstOrDefault().Id },
            };

            // Add each UserRating in the ratings list to the UserRatings table.
            foreach (UserRating rating in ratings) {
                context.UserRatings.Add(rating);
            }

            // Save the changes made to the database state.
            context.SaveChanges();
        }
    }
}
