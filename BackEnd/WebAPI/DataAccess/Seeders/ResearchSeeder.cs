/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    /// <summary>
    /// A seeder adding initial <see cref="Researches"/> data to the database.
    /// </summary>
    /// <seealso cref="DefaultSeeder"/>
    [ExcludeFromCodeCoverage]
    class ResearchSeeder {
        public void Seed(DatabaseContext context) {

            //Build a list of initial researches
            List<Researches> researches = new List<Researches>(){
                new Researches() { Id = Guid.Parse("00000000-0000-0000-0000-000000000000"), Name = "No research", Description = "You are curently not in a research.", StartTime = DateTime.UtcNow, EndTime = new DateTime(2050, 1, 1), ViewExposure = true, ViewHighScores = true, ViewMinigameScores = true, ViewRaster = true, ViewRatings = true, ViewRoutePlanner = true, ViewSensors = true},
                new Researches() { Id = Guid.Parse("12345678-1234-1234-1234-123456789012"), Name = "Special research", Description = "You are not allowed to see anything", StartTime = DateTime.UtcNow, EndTime = new DateTime(2050, 1, 1), ViewExposure = false, ViewHighScores = false, ViewMinigameScores = false, ViewRaster = false, ViewRatings = false, ViewRoutePlanner = false, ViewSensors = false }
            };

            //Add all the researches to the research table
            foreach (Researches research in researches) {
                context.Researches.Add(research);
            }

            //save the changes made to the database state
            context.SaveChanges();
        }
    }
}
