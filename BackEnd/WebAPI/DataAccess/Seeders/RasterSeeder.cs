/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    class RasterSeeder {
        [ExcludeFromCodeCoverage]
        public void SeedRaster(DatabaseContext context) {
            ICollection<Domain.Exposure.RasterCell> rasterCells = new List<Domain.Exposure.RasterCell>();

            rasterCells.Add(new Domain.Exposure.RasterCell() { Value = 27.8f, SouthWestLatitude = 52.0843124, SouthWestLongitude = 5.163404, NorthEastLatitude = 52.08467, NorthEastLongitude = 5.16398573 });
            rasterCells.Add(new Domain.Exposure.RasterCell() { Value = 27.27f, SouthWestLatitude = 52.0875473, SouthWestLongitude = 5.162804, NorthEastLatitude = 52.0879059, NorthEastLongitude = 5.163386 });

            context.Rasters.Add(
                new Domain.Exposure.Raster() {
                    CellSize = 40f,
                    Height = 100,
                    Width = 100,
                    NoDataValue = -1,
                    TimeStampPolled = new DateTime(2018, 04, 11, 14, 54, 55, 29),
                    SouthWestLat = 52.082275226931465,
                    SouthWestLng = 5.162544250488281,
                    NorthEastLat = 52.08787248903425,
                    NorthEastLng = 5.180386304855347,
                    Cells = rasterCells
                });

            context.SaveChanges();
        }
    }
}
