/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Domain.Administration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    /// <summary>
    /// The default seeder for the <see cref="DatabaseContext"/>, 
    /// adding initial data to the database.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class DefaultSeeder {
        /// <summary>
        /// Seed the given <see cref="DatabaseContext"/> with initial data.
        /// </summary>
        /// <param name="context">
        /// The context for which the database should be seeded.
        /// </param>
        public static void Seed(DatabaseContext context) {
            new ResearchSeeder().Seed(context);
            UserSeeder.SeedUsers(context, "user", "user", "user@example.nl", "32FWTdSFdVPzDe1CYhXMMZfN5Jt4SVEezj9J451mCOyu3NXMbIZCXcPb9cWORdVp", "0Y81AJ2_52hQ3J4M81P0gkqhLaI2Lr8x1RNv195KG76k3rWPHbHf_87Yx7Y_D41f", new DateTime(2018, 4, 9), Role.Regular, true);
            UserSeeder.SeedUsers(context, "user2", "user2", "user2@example.nl", "32FWTdSFdVPzDe1CYhXMMZfN5Jt4SVEezj9J451mCOyu3NXMbIZCXcPb9cWORdVp", "0Y81AJ2_52hQ3J4M81P0gkqhLaI2Lr8x1RNv195KG76k3rWPHbHf_87Yx7Y_D41f", new DateTime(2018, 5, 18), Role.Regular, false);
            UserSeeder.SeedUsers(context, "admin", "admin", "admin@example.nl", "E904BhTRu1LAM01dtNM089MchY03yAhD_7WPFU6205qQavjJ044I3qqJGB_CYP3n", "ls44t0zDir6UmXKWCmLee5nwXVTedX3RHrxGpKTQ5wHa75Bc32Ui00bt008qKM6H", new DateTime(2018, 4, 9), Role.Administrator, false);
            context.SaveChanges();
            new UserRatingSeeder().Seed(context);
            new UserResearchSeeder().Seed(context);
            new TriggerSeeder().Seed(context);
            //new RasterSeeder().SeedRaster(context);
            context.SaveChanges();
        }
    }
}
