/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Seeders {
    /// <summary>
    /// A seeder adding initial <see cref="UserResearches"/> data to the database.
    /// </summary>
    /// <seealso cref="DefaultSeeder"/>
    [ExcludeFromCodeCoverage]
    class UserResearchSeeder {
        public void Seed(DatabaseContext context) {

            //Build a list of initial researches
            List<UserResearches> userResearches = new List<UserResearches>(){
                new UserResearches() { userId = context.Users.FirstOrDefault(u => u.UserName == "user2").Id, researchId = context.Researches.FirstOrDefault(r => r.Name == "No research").Id, startTime = DateTime.UtcNow, endTime = new DateTime(3000, 1, 1)},
                new UserResearches() { userId = context.Users.FirstOrDefault(u => u.UserName == "user").Id, researchId = context.Researches.FirstOrDefault(r => r.Name == "No research").Id, startTime = new DateTime(2018, 1, 1), endTime = new DateTime(2018, 12, 31)},
                new UserResearches() { userId = context.Users.FirstOrDefault(u => u.UserName == "user").Id, researchId = context.Researches.FirstOrDefault(r => r.Name == "Special research").Id, startTime = DateTime.UtcNow, endTime = new DateTime(2050, 1, 1)}
            };

            //Add all the researches to the research table
            foreach (UserResearches userResearch in userResearches) {
                context.UserResearches.Add(userResearch);
            }

            //save the changes made to the database state
            context.SaveChanges();
        }
    }
}
