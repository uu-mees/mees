/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Minigames;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Domain.Exposure;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations.MinigameConfiguration {
    [ExcludeFromCodeCoverage]
    class MinigameConfiguration : EntityTypeConfiguration<MinigameScores> {
        public MinigameConfiguration() {

            // Define the Id property as the primary key for the table.
            HasKey(x => new { x.userID, x.timeStamp });

        }
    }
}
