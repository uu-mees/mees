/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class RasterCellConfiguration : EntityTypeConfiguration<RasterCell> {
        /// <summary>
        /// Define the properties for the <see cref="RasterCell"/> table.
        /// </summary>
        /// <seealso cref="RasterConfiguration"/>
        public RasterCellConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => x.CellId);

            // Set the Id property to be auto-generated unique values.
            Property(x => x.CellId).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            // Define the many-to-one relation between the RasterCell and Raster tables.
            HasRequired(x => x.Raster)
                .WithMany(x => x.Cells)
                .HasForeignKey(x => x.RasterId);

        }
    }
}
