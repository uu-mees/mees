/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Tracking;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations.TrackingConfigurations {
    [ExcludeFromCodeCoverage]
    class UserLocationExposureConfiguration : EntityTypeConfiguration<UserLocationExposure> {

        /// <summary>
        /// This very likely has to be changed. Giving it an easier way to find the right data than just searching the entire thing
        /// </summary>
        public UserLocationExposureConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => new { x.UserID, x.Timestamp });


        }
    }
}
