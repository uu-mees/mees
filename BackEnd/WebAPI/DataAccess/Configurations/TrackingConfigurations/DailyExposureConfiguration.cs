/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations.TrackingConfigurations {
    [ExcludeFromCodeCoverage]
    class DailyExposureConfiguration : EntityTypeConfiguration<DailyExposure> {
        public DailyExposureConfiguration() {
            HasKey(x => new { x.UserId, x.Date });

            // User Id as foreign key.
            HasRequired(x => x.User)
                .WithMany(x => x.DailyExposures)
                .HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
        }
    }
}
