﻿/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Tracking;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Configurations.TrackingConfigurations
{
    class DailySummaryConfiguration : EntityTypeConfiguration<DailySummary>
    {
        public DailySummaryConfiguration()
        {
            HasKey(x => new { x.UserId, x.Date });

            // UserId as foreign key
            HasRequired(x => x.User)
                .WithMany(x => x.DailySummaries)
                .HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
        }
    }
}
