/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations.TriggerConfiguration {
    [ExcludeFromCodeCoverage]
    public class QuestionnaireConfiguration : EntityTypeConfiguration<Questionnaire> {
        public QuestionnaireConfiguration() {
            HasKey(x => x.QuestionnaireId);
            Property(x => x.QuestionnaireId).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
        }
    }
}
