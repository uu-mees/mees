/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class PasswordResetTokenConfiguration : EntityTypeConfiguration<PasswordResetToken> {
        /// <summary>
        /// Define the properties for the <see cref="PasswordResetToken"/> table.
        /// </summary>
        public PasswordResetTokenConfiguration() {
            // Define the Token property as the primary key for the table.
            HasKey(x => x.UserId);

            // Define foreign key userId.
            HasRequired(x => x.User).WithOptional(x => x.PasswordResetToken).WillCascadeOnDelete(true);
        }
    }
}
