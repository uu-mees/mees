/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Domain.Administration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class FireBaseConfiguration : EntityTypeConfiguration<FireBase> {
        /// <summary>
        /// Define the properties for the <see cref="FireBase"/> table.
        /// </summary>
        /// <seealso cref="FireBaseConfiguration"/>
        public FireBaseConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => x.FireBaseID);

            HasRequired(x => x.User)
                .WithMany(x => x.FireBases)
                .HasForeignKey(x => x.UserID).WillCascadeOnDelete(true);
        }
    }
}
