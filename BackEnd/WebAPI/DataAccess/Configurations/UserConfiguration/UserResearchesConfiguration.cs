/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Administration;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class UserResearchesConfiguration : EntityTypeConfiguration<UserResearches> {
        /// <summary>
        /// Define the properties for the <see cref="UserResearches"/> table.
        /// </summary>
        public UserResearchesConfiguration() {

            // Define the Id property as the primary key for the table.
            HasKey(x => new { x.userId, x.researchId, x.startTime });

            // Set the userId and researchId as foreign keys
            HasRequired(x => x.User)
                .WithMany(x => x.UserResearches)
                .HasForeignKey(x => x.userId).WillCascadeOnDelete(true);
            HasRequired(x => x.Research)
                .WithMany(x => x.UserResearches)
                .HasForeignKey(x => x.researchId).WillCascadeOnDelete(true);

        }
    }
}
