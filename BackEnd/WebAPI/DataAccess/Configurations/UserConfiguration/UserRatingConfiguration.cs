/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class UserRatingConfiguration : EntityTypeConfiguration<UserRating> {
        /// <summary>
        /// Define the properties for the <see cref="UserRating"/> table.
        /// </summary>
        /// <seealso cref="UserConfiguration"/>
        public UserRatingConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => x.Id);

            // Set the Id property to be auto-generated unique values.
            Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            // Set the userId as foreign key
            HasRequired(x => x.User)
                .WithMany(x => x.UserRatings)
                .HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
        }
    }
}
