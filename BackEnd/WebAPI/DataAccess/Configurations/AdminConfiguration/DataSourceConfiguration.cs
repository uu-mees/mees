/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Data.Entity.ModelConfiguration;
using Domain.Administration;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace DataAccess.Configurations.MinigameConfiguration {
    [ExcludeFromCodeCoverage]
    class DataSourceConfiguration : EntityTypeConfiguration<DataSource> {
        public DataSourceConfiguration() {

            // Define the Id property as the primary key for the table.
            HasKey(x => new { x.ID });
            Property(x => x.URL).HasMaxLength(500);
        }
    }
}
