/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Data.Entity.ModelConfiguration;
using Domain;
using Domain.Exposure;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class SensorMeasurementBatchConfiguration : EntityTypeConfiguration<SensorMeasurementBatch> {
        /// <summary>
        /// Define the properties for the <see cref="SensorMeasurementBatch"/> table.
        /// </summary>
        /// <seealso cref="SensorConfiguration"/>
        /// <seealso cref="SensorComponentConfiguration"/>
        /// <seealso cref="SensorMeasurementConfiguration"/>
        public SensorMeasurementBatchConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => x.Id);

            // Set the Id property to be auto-generated unique values.
            Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            // Define the many-to-one relation between the SensorMeasurementBatch and Sensor tables.
            HasRequired(x => x.Sensor)
                .WithMany(x => x.Batches)
                .HasForeignKey(x => x.SensorId);
        }
    }
}
