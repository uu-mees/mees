/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using Domain.Exposure;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Configurations {
    [ExcludeFromCodeCoverage]
    class SensorMeasurementConfiguration : EntityTypeConfiguration<SensorMeasurement> {
        /// <summary>
        /// Define the properties for the <see cref="SensorMeasurement"/> table.
        /// </summary>
        /// <seealso cref="SensorConfiguration"/>
        /// <seealso cref="SensorComponentConfiguration"/>
        /// <seealso cref="SensorMeasurementBatchConfiguration"/>
        public SensorMeasurementConfiguration() {
            // Define the Id property as the primary key for the table.
            HasKey(x => x.Id);

            // Set the Id property to be auto-generated unique values.
            Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            // Define the one-to-many relation between the SensorMeasurement and SensorMeasurementBatch tables.
            HasRequired(x => x.Batch)
                .WithMany(x => x.Measurements)
                .HasForeignKey(x => x.BatchId);

            // Define the many-to-one relation between the sensorMeasurement and SensorComponent tables.
            HasRequired(x => x.Component)
                .WithMany(x => x.Measurements)
                .HasForeignKey(x => x.SensorComponentId);
        }
    }
}
