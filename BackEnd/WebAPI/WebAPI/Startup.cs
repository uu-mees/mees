/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services.Interfaces;
using Services.Implementations;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using WebAPI.WebModels.ModelBinderProviders;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Domain.Administration;
using WebAPI.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using System.Security.Claims;
using Services.Interfaces.Pollers;

namespace WebAPI {

    public class Startup {

        public Startup(IHostingEnvironment env, IConfiguration configuration) {
            // When we run/build as release we use the release settings.
            string jsonFile;
            if (env.IsProduction()) {
                jsonFile = "appsettings.release.json";
            }
            else {
                jsonFile = "appsettings.Development.json";
            }

            IConfigurationBuilder builder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath)
                    .AddJsonFile(jsonFile, optional: false, reloadOnChange: true);

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            QuestionnaireConfigurationService questionnaireConfigurationService = new QuestionnaireConfigurationService("questionnairesettings.json");
            TriggersConfigurationService triggersConfigurationService = new TriggersConfigurationService("triggersettings.json");

            ConfigurationService configurationService = new ConfigurationService()
            {
                ConnectionString = Configuration.GetConnectionString("Database"),
                SensorURL = Configuration["PollingURLs:SensorURL"],
                RasterURL = Configuration["PollingURLs:RasterURL"],
                SensorTimeoutHours = int.Parse(Configuration["SensorTimeoutHours"]),
                MaxOutOfBoundsPercentage = float.Parse(Configuration["MaxOutOfBoundsPercentage"]),
                AuthTokenConfiguration = new AuthTokenConfiguration()
                {
                    AuthTokenKey = Configuration["AuthTokenConfiguration:AuthTokenKey"],
                    TimeToLiveMinutes = int.Parse(Configuration["AuthTokenConfiguration:TimeToLiveMinutes"]),
                    Issuer = Configuration["AuthTokenConfiguration:Issuer"]
                },
                GeocodeConfiguration = new GeocodeConfiguration()
                {
                    ApiKey = Configuration["GeocodingConfiguration:APIKey"],
                    ApiUrl = Configuration["GeocodingConfiguration:APIURL"],
                    ResultType = Configuration["GeocodingConfiguration:ResultType"]
                },
                RoutingConfiguration = new RoutingConfiguration()
                {
                    ApiUrl = Configuration["RoutingConfiguration:APIURL"],
                },
                EmailConfiguration = new EmailConfiguration()
                {
                    Email = Configuration["EmailConfiguration:Email"],
                    Password = Configuration["EmailConfiguration:Password"]
                }
            };


            services.AddIdentityCore<ClaimsIdentity>(x => { })
               .AddDefaultTokenProviders();

            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o => {
                o.IncludeErrorDetails = true;
                o.TokenValidationParameters = new TokenValidationParameters() {
                    ValidateAudience = false,
                    ValidateIssuer = true,
                    ValidIssuer = configurationService.AuthTokenConfiguration.Issuer,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configurationService.AuthTokenConfiguration.AuthTokenKey)),
                    ClockSkew = new TimeSpan(0)
                };

            });

            services.AddAuthorization(x => x.AddPolicy("RequireUser", policy => {
                policy.RequireClaim("userId");
            }));

            services.AddAuthorization(x => x.AddPolicy("RequireAdmin", policy =>
                policy.Requirements.Add(new RoleRequirement(Role.Administrator))
            ));

            services.AddScoped<IAuthorizationHandler, RoleHandler>();

            services.AddCors();

            services.AddMvc(options => {
                options.ModelBinderProviders.Insert(0, new AreaModelBinderProvider());
                options.ModelBinderProviders.Insert(1, new PositionModelBinderProvider());
                options.ModelBinderProviders.Insert(2, new TimeIntervalModelBinderProvider());
                options.ModelBinderProviders.Insert(3, new RouteModelBinderProvider());
                //var policy = new AuthorizationPolicyBuilder()
                //         .RequireAuthenticatedUser()
                //         .Build();
                //options.Filters.Add(new AuthorizeFilter(policy));
                //var policy2 = new AuthorizationPolicyBuilder();
                //policy.Requirements.Add(new RoleRequirement(Role.Administrator));
                //options.Filters.Add(new AuthorizeAttribute("RequireAdmin"))
            });




            services.AddScoped(sp => new DatabaseContext(configurationService.ConnectionString));

            services.AddTransient(typeof(IPollingService<>), typeof(PollingService<>));
            services.AddSingleton(typeof(ISensorDataInjectionService), typeof(SensorDataInjectionService));
            services.AddSingleton(typeof(IRasterDataInjectionService), typeof(RasterDataInjectionService));
            services.AddSingleton(sp => configurationService);
            services.AddSingleton(typeof(IRasterContainer), typeof(RasterContainer));

            services.AddSingleton(sp => questionnaireConfigurationService);
            services.AddSingleton(sp => triggersConfigurationService);

            services.AddTransient(typeof(MinigameService));

            services.AddTransient(typeof(IUserService), typeof(UserService));
            services.AddTransient(typeof(IEmailService), typeof(EmailService));
            services.AddTransient(typeof(ISensorService), typeof(SensorService));
            services.AddTransient(typeof(IRasterService), typeof(RasterService));
            services.AddTransient(typeof(IExposureService), typeof(ExposureService));
            services.AddTransient(typeof(ILocationExposureService), typeof(LocationExposureService));
            services.AddTransient(typeof(IRatingService), typeof(RatingService));
            services.AddTransient(typeof(IAuthenticationService), typeof(AuthenticationService));
            services.AddTransient(typeof(IRoutingService), typeof(RoutingService));
            services.AddTransient(typeof(IGeocodingService), typeof(GeocodingService));
            services.AddTransient(typeof(ITriggerService), typeof(TriggerService));
            services.AddTransient(typeof(IQuestionnaireService), typeof(QuestionnaireService));
            services.AddTransient(typeof(IFireBaseService), typeof(FireBaseService));
            services.AddScoped(typeof(IAuthenticationService), typeof(AuthenticationService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              ISensorDataInjectionService sensorDataInjectionService,
                              IRasterDataInjectionService rasterDataInjectionService) {
            app.UseAuthentication();

            app.UseCors(
                options => options.WithOrigins("*").AllowAnyMethod().AllowAnyHeader()
            );

            //app.UseDeveloperExceptionPage();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseExceptionHandler("/error");
            }


            app.UseAuthentication();

            app.UseStatusCodePages();

            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}
