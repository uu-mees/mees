/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Authorization {
    public class RoleRequirement : IAuthorizationRequirement {
        public Role Role { get; private set; }

        public RoleRequirement(Role role) {
            this.Role = role;
        }
    }
}
