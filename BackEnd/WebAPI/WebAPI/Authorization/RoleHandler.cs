/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Authorization {
    public class RoleHandler : IAuthorizationHandler {
        public Task HandleAsync(AuthorizationHandlerContext context) {
            List<IAuthorizationRequirement> reqs = context.PendingRequirements.ToList();

            foreach (IAuthorizationRequirement r in reqs) {
                if (r is RoleRequirement) {
                    if (context.User.Claims.FirstOrDefault(x => x.Type == "userRole") == null) {
                        continue;
                    }

                    if (context.User.Claims.FirstOrDefault(x => x.Type == "userRole").Value == ((RoleRequirement)r).Role.ToString()) {
                        context.Succeed(r);
                    }
                }
            }

            return Task.CompletedTask;
        }
    }
}
