/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Domain.Minigames;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Converters;
using WebAPI.DTOs;
using WebAPI.DTOs.UserDTOs;
using WebAPI.WebModels;
using WebAPI.Converters.UserConverters;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers {
    [Route("minigames")]
    public class MinigameController : Controller {
        private MinigameService minigameService;

        public MinigameController(MinigameService minigameService) {
            this.minigameService = minigameService;
        }

        public IActionResult Index() //i have no idea why this is here, probably can be removed
        {
            return View();
        }

        [Route("scores")]
        [HttpPost()]
        public IActionResult saveScores([FromBody] MinigameScoresDTO minigameScoresDTO) {
            if (minigameScoresDTO == null) {
                return BadRequest("failed to get minigameScores ");
            }

            try {
                MinigameScores minigameScores = minigameScoresDTO.FromDTO();
                minigameService.saveMinigameScores(minigameScores);
                return Ok();
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }
    }
}
