/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using Services.Retrievers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Converters;
using WebAPI.DTOs;
using WebAPI.DTOs.UserDTOs;
using WebAPI.WebModels;
using WebAPI.Converters.UserConverters;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace WebAPI.Controllers {
    /// <summary>
    /// Controller for User related actions.
    /// </summary>
    [Route("users")]
    public class UserController : Controller {
        private IUserService userService;
        private ILogger<UserController> logger;

        public UserController(IUserService userService, ILogger<UserController> logger) {
            this.userService = userService;
            this.logger = logger;
        }

        /// <summary>
        /// Register a new User.
        /// </summary>
        /// <param name="userRegistrationDTO">DTO containing registration information.</param>
        /// <returns>HTTP Ok with new User if information is validated. Otherwise, returns HTTP BadRequest.</returns>
        [Route("register")]
        [HttpPost()]
        public IActionResult RegisterUser([FromBody] UserRegistrationDTO userRegistrationDTO) {
            if (userRegistrationDTO == null) {
                return BadRequest("Failed to create account");
            }

            try {
                User newUser = userService.CreateNewUser(userRegistrationDTO.FromDTO());
                return Ok(newUser.ToDTO());
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Deletes the currently logged in user and all its dependencies
        /// </summary>
        /// <returns>BadRequest if the user doesn't exist, Unauthorized if users id is not valid, Ok on success</returns>
        [Authorize(Policy = "RequireUser")]
        [Route("")]
        [HttpDelete()]
        public IActionResult DeleteUser() {
            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);
            if (userId == null) {
                return Unauthorized();
            }

            bool success = userService.DeleteEntireUser(userId);
            return success ? (IActionResult)Ok() : BadRequest();
        }

        /// <summary>
        /// Get all <see cref="DailyExposure"/> from within a given time interval.
        /// </summary>
        /// <param name="interval"><see cref="TimeIntervalModel"/> describing time interval between which exposure must be retrieved.</param>
        /// <returns>List of <see cref="DailyExposureDTO"/> containing exposure of all days within interval.</returns>
        [Authorize(Policy = "RequireUser")]
        [HttpGet("exposure")]
        public IActionResult GetDailyExposure(TimeIntervalModel interval) {
            if (interval == null || (interval.TimeFrom > DateTime.UtcNow && interval.TimeTo > DateTime.UtcNow)) {
                return BadRequest();
            }

            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);

            ICollection<DailyExposure> exposures = userService.GetDailyExposure(userId, interval.TimeFrom, interval.TimeTo);
            if (exposures == null) {
                return Ok(new List<DailyExposure>() { });
            }

            return Ok(exposures.Select(x => x.ToDTO()));
        }

        /// <summary>
        /// Get the <see cref="UserProfile"/> for the specific user. Requires user to be logged on as form of authentication and identification to select appropriate <see cref="UserProfile"/>.
        /// </summary>
        /// <returns><see cref="UserProfileDTO"/> if successful, see <see cref="IUserService.GetUserProfile(Guid)"/> for constraints. <see langword="null"/> when failed.</returns>
        [Authorize(Policy = "RequireUser")]
        [Route("settings/UserProfile")]
        [HttpGet()]
        public IActionResult GetUserProfile() {
            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);
            UserProfile profile = userService.GetUserProfile(userId);

            if (profile == null) { return BadRequest(); }

            return Ok(profile.ToDTO());
        }

        /// <summary>
        /// Updates the <see cref="User"/> for requesting user, only information from the subset <see cref="UserProfile"/> will be updated if constraints are met.
        /// </summary>
        /// <param name="profile"><see cref="UserProfileDTO"/> with personal information about <see cref="User"/></param>
        /// <returns><see cref="UserProfileDTO"/> if successful, see <see cref="IUserService.UpdateUserProfile(Guid, UserProfile)"/> for constraints. <see langword="null"/> when failed.</returns>
        [Authorize(Policy = "RequireUser")]
        [Route("settings/UserProfile")]
        [HttpPost()]
        public IActionResult UpdateUserProfile([FromBody] UserProfileDTO profile) {
            if (profile == null) { return BadRequest(); }

            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);

            try {
                userService.UpdateUserProfile(userId, profile.FromDTO());
                return Ok();
            }
            catch (Exception e) {
                // If an exception was thrown, then the request was rejected.
                return BadRequest(e.Message);
            }


        }

        /// <summary>
        /// Updates the <see cref="User.HashedPassword"/> for requesting user.
        /// </summary>
        /// <param name="passwordPair"><see cref="PasswordPairDTO"/> containing unhashed <see cref="PasswordPairDTO.OldPassword"/> and <see cref="PasswordPairDTO.NewPassword"/></param>
        /// <returns>Unhashed <see cref="PasswordPairDTO"/> if successful, see <see cref="IUserService.UpdatePassword(Guid, PasswordPair)"/> for constraints. <see langword="null"/> when failed.</returns>
        [Authorize(Policy = "RequireUser")]
        [Route("settings/password")]
        [HttpPost()]
        public IActionResult UpdatePassword([FromBody] PasswordPairDTO passwordPair) {
            if (passwordPair == null) {
                return BadRequest();
            }

            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);

            try {
                userService.UpdatePassword(userId, passwordPair.FromDTO());
                return Ok();
            }
            catch (Exception e) {
                // If an exception was thrown, then the request was rejected.
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Generate a token to reset the <see cref="User"/>'s password.
        /// </summary>
        /// <param name="email">Email address registered to the <see cref="User"/></param>
        /// <returns><see cref="OkResult()"/> if successful.</returns>
        [HttpGet("settings/token")]
        public IActionResult GeneratePasswordResetToken(string email) {
            try {
                userService.GenerateResetToken(email);
                return Ok();
            }
            catch (Exception e) {
                // If an exception was thrown, then the request was rejected.
                return BadRequest(e.Message);
            }
        }


        /// <summary>
        /// Attempts to reset the <see cref="User"/>'s password.
        /// </summary>
        /// <param name="resetPasswordDTO">DTO containing a token, newpassword and confirmpassword.</param>
        /// <returns><see cref="OkResult()"/> if successful.</returns>
        [HttpPost("settings/token")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDTO resetPasswordDTO) {

            try {
                userService.ResetPassword(resetPasswordDTO.Token, resetPasswordDTO.NewPassword, resetPasswordDTO.ConfirmPassword);
                return Ok();
            }
            catch (Exception e) {
                // If an exception was thrown, then the request was rejected.
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Sends the Restrictions of the front-end features of a user, based on the research the user is in.
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = "RequireUser")]
        [Route("")]
        [HttpGet("research")]
        public IActionResult requestUserResearch() {
            Guid userId = Guid.Parse(Request.HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userId").Value);

            if (userId == null) {
                return BadRequest();
            }

            Researches userResearch;
            try {
                userResearch = userService.GetResearchRestrictions(userId);
            }
            catch (Exception e) {
                return BadRequest(e);
            }


            try {
                return Ok(userResearch.toDTO());
            }
            catch {
                return BadRequest();
            }
        }
    }
}
