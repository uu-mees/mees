/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.DTOs;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using Utils;
using WebAPI.WebModels;

namespace WebAPI.Controllers {
    [Route("raster")]
    public class RasterController : Controller {
        private IRasterService rasterService;
        private ITriggerService triggerService;
        private readonly ILogger<RasterController> logger;

        public RasterController(IRasterService rasterService, ITriggerService triggerService, ILogger<RasterController> logger) {
            this.rasterService = rasterService;
            this.triggerService = triggerService;
            this.logger = logger;
        }

        /// <summary>
        /// Gets the latest raster with the cells contained in the given area.
        /// </summary>
        /// <param name="area">Areamodel designating the area</param>
        /// <param name="timestamp">Optional timestamp used for comparing to the newest raster</param>
        /// <returns>
        /// Raster with cells in the given range in JSON format.
        /// <para>304 Not Modified Statuscode if there is no newer raster.</para>
        /// <para>204 NoContent StatusCode if there is no Raster at all.</para>
        /// </returns>
        [HttpGet("")]
        public IActionResult GetRasterImages(DateTime timestamp) {
            RasterDTO rasterDTO = rasterService.GetRasterImage();

            //If there are no rasters or all stored rasters are too old(because its collection was turned off or the rivm page is down,
            //Send a fake raster to show a gray overlay in the front-end to show the lack of data.
            if (rasterDTO == null || rasterDTO.TimeStampPolled.AddHours(2) < DateTime.UtcNow)
            {
                RasterDTO fakeRasterDTO = rasterService.rasterContainer.CreateImages(rasterService.CreateFakeRaster());
                logger.LogWarning("Raster was requested before it was available");
                return Ok(fakeRasterDTO);
            }
            
            // If no timestamp is included, it defaults to DateTime.Min.
            if (rasterDTO.TimeStampPolled <= timestamp.ToUniversalTime()) {
                return StatusCode(304);
            }

            return Ok(rasterDTO);
        }

        /// <summary>
        /// Get the timestamp of the last time the raster was polled.
        /// </summary>
        /// <returns>Timestamp of the latest raster, DateTime.Min if there is no raster</returns>
        [HttpGet("new")]
        public IActionResult GetLatestRasterTime() {
            return Ok(rasterService.GetLatestRasterTimeStamp());
        }

        /// <summary>
        /// Get the exposure value for a given position (from the latest raster)
        /// </summary>
        /// <example>
        /// http://localhost:50500/raster/exposure?position=52.08767,5.16412
        /// </example>
        /// <param name="position">The coordinate to get the exposure for.</param>
        /// <returns>The exposure value.</returns>
        [HttpGet("exposure")]
        public IActionResult GetExposureFromLocation(PositionModel position, Guid? userId = null) {
            if (position == null || !GeoUtils.IsValidLatitude(position.Latitude) || !GeoUtils.IsValidLongitude(position.Longitude)) {
                return BadRequest();
            }

            GeoCoordinate coordinate = new GeoCoordinate(position.Latitude, position.Longitude);

            double? exposure = rasterService.GetExposureFromLocation(coordinate);

            // Do not return NotFound when the polled location is out of bounds since that would give us
            // 'null' as a value which is the correct value for a location OOB.
            return Ok(exposure);
        }
    }
}
