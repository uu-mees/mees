/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebAPI.Controllers {
    [Route("error")]
    public class ExceptionController : Controller {
        private readonly ILogger<ExceptionController> logger;

        public ExceptionController(ILogger<ExceptionController> logger) {
            this.logger = logger;
        }

        [Route("")]
        public IActionResult Index() {
            logger.LogWarning("Internal server error, user was shown 500 page");
            return StatusCode(500, "Internal Server Error");
        }
    }
}
