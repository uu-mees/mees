/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using WebAPI.DTOs.UserDTOs;
using System;
using System.Text;
using Services.Interfaces;
using WebAPI.Converters.UserConverters;
using Domain.Administration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.WebUtilities;
using WebAPI.Converters;
using Newtonsoft.Json.Linq;



namespace WebAPI.Controllers {
    public class FireBaseController : Controller {

        private IFireBaseService fireBaseService;
        private ILogger<FireBaseController> logger;

        public FireBaseController(IFireBaseService fireBaseService, ILogger<FireBaseController> logger) {
            this.fireBaseService = fireBaseService;
            this.logger = logger;
        }

        [HttpPost("FireBaseID")]
        public IActionResult RetrieveFireBaseID([FromBody] FireBaseDTO fireBaseDTO) {
            if (fireBaseDTO.UserIDToken == "") {
                return BadRequest("User token was empty");
            }

            if (fireBaseDTO.FireBaseID == "") {
                return BadRequest("FireBaseID string was empty");
            }
            try {
                fireBaseDTO.UserIDToken = RetrieveUserIdFromToken(fireBaseDTO.UserIDToken);
                FireBase firebase = fireBaseDTO.FromDTO();
                fireBaseService.AddFireBaseToDatabase(firebase);
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        public string RetrieveUserIdFromToken(string token) {

            string encodedToken = token.Split('.')[1];
            Byte[] decodedToken = Base64UrlTextEncoder.Decode(encodedToken);
            string stringifiedDecodedToken = Encoding.Default.GetString(decodedToken);

            JObject jsonDecodedToken = JObject.Parse(stringifiedDecodedToken);
            return jsonDecodedToken["userId"].ToString();

        }
    }
}
