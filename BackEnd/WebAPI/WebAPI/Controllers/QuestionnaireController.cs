/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using WebAPI.Converters;
using System.Device.Location;
using WebAPI.WebModels;
using Microsoft.Extensions.Logging;
using Domain.Routing;
using Services.Implementations;
using Domain.Tracking;
using Microsoft.AspNetCore.Authorization;
using Domain.Questionnaire;
using Newtonsoft.Json;
using WebAPI.DTOs;

namespace WebAPI.Controllers {
    [Route("questionnaire")]
    public class QuestionnaireController : Controller {
        private IQuestionnaireService questionnaireService;
        private QuestionnaireConfigurationService configurationService;
        private ILogger<QuestionnaireController> logger;

        public QuestionnaireController(IQuestionnaireService questionnaireService, QuestionnaireConfigurationService questionnaireConfigurationService
            , ILogger<QuestionnaireController> logger) {
            this.questionnaireService = questionnaireService;
            configurationService = questionnaireConfigurationService;
            this.logger = logger;
        }

        [Authorize(Policy = "RequireUser")]
        [HttpGet("")]
        public IActionResult GetQuestionnaire(Guid userId) {
            Questionnaire questionnaire = questionnaireService.CheckForQuestionnaire(userId);
            if (questionnaire == null) {
                return NoContent();
            }
            return Ok(QuestionnaireConverter.ToDTO(questionnaire));
        }

        [Authorize(Policy = "RequireAdmin")]
        [HttpPost("update")]
        public IActionResult UpdateConfiguration([FromBody] QuestionnaireConfiguration configuration) {
            if (configuration.TriggerConfigurations == null)
                return BadRequest();
            configurationService.UpdateConfiguration(configuration);
            return Ok();
        }

        [Authorize(Policy = "RequireAdmin")]
        [HttpGet("configuration")]
        public IActionResult GetConfiguration() {
            return Ok(configurationService.GetConfigurationJSON());
        }

        [Authorize(Policy = "RequireAdmin")]
        [HttpGet("results")]
        public IActionResult GetQuestionnaireResults() {
            return Ok(questionnaireService.GetAllQuestionnaires().ToDTO());
        }
    }
}
