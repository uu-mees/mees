/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Tracking;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.WebUtilities;
using Services.Interfaces;
using System;
using System.Text;
using WebAPI.Converters;
using Services.DTOs;
using Utils;
using Newtonsoft.Json.Linq;
using Services.Implementations;


namespace WebAPI.Controllers {
    /// <summary>
    /// Controller for the user location data.
    /// </summary>
    public class UserLocationController : Controller {

        private ILocationExposureService locationExposureService;
        private ILogger<UserLocationController> logger;
        private ITriggerService triggerService;
        private ConfigurationService configurationService;

        public UserLocationController(ILocationExposureService locationExposureService, ILogger<UserLocationController> logger, ITriggerService triggerService, ConfigurationService configurationService) {
            this.locationExposureService = locationExposureService;
            this.logger = logger;
            this.triggerService = triggerService;
            this.configurationService = configurationService;
        }

        /// <summary>
        /// Receives a package of locations every hour from the mobile application which is then parsed and sent to the LocationExposureService for processing of data.
        /// </summary>
        /// <param name="locations">Array of packages containing the location, timestamp and user token</param>
        /// <returns></returns>
        [HttpPost("userLocationData")]
        public IActionResult GetLocationPackage([FromBody] UserLocationDataDTO[] locations) {
            UserLocationExposure[] userLocationExposures = new UserLocationExposure[locations.Length];

            for (int i = 0; i < locations.Length; i++) {
                //Sometimes a package might be sent without a token. This can't be used and is thus skipped
                if (locations[i].userID == "") {
                    return BadRequest("User token was empty");
                }

                UserLocationDataDTO userLocation = locations[i];
                //Parse the token string to retrieve the userId within it
                try {
                    locations[i].userID = RetrieveUserIdFromToken(userLocation.userID);
                }
                catch (Exception e) {
                    return BadRequest(e.Message);
                }

                if (!GeoUtils.IsValidLatitude(userLocation.latitude) || !GeoUtils.IsValidLongitude(userLocation.longitude)) {
                    return BadRequest("Location out of range");
                }
                //Convert the package into the used format for processing
                userLocationExposures[i] = userLocation.fromDTO();
            }

            try {
                locationExposureService.LinkUserAndExposuretoLocation(userLocationExposures);
            }
            catch (Exception e) {
                return BadRequest(e.Message);
            }

            // Check if we need to let the user play a minigame and/or fill in a questionnaire
            if ((triggerService.DistanceTrigger(userLocationExposures) || triggerService.ExposureTrigger(userLocationExposures)) && triggerService.LocationInRaster(userLocationExposures)) {
                triggerService.SendNotificationPostRequest(Guid.Parse(locations[0].userID), configurationService.ConnectionString);
            }

            return Ok();
        }


        /// <summary>
        /// The Mobile application which this receives from has an encoded token containing the userID we need.
        /// This function Gets this id from the token and returns it.
        /// </summary>
        /// <param name="token">Base64 encoded JSON Web Token</param>
        /// <returns></returns>
        public string RetrieveUserIdFromToken(string token) {

            string encodedToken = token.Split('.')[1];
            Byte[] decodedToken = Base64UrlTextEncoder.Decode(encodedToken);
            string stringifiedDecodedToken = Encoding.Default.GetString(decodedToken);

            JObject jsonDecodedToken = JObject.Parse(stringifiedDecodedToken);
            return jsonDecodedToken["userId"].ToString();

        }
    }
}
