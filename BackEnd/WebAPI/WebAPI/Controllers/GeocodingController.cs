/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using WebAPI.Converters;
using System.Device.Location;
using Utils;
using Domain.Routing;
using Microsoft.Extensions.Logging;
using System.Globalization;
using WebAPI.WebModels;

namespace WebAPI.Controllers {
    [Route("geocoding")]
    public class GeocodingController : Controller {
        private IGeocodingService geocodingService;
        private ILogger<GeocodingController> logger;

        public GeocodingController(IGeocodingService geocodingService, ILogger<GeocodingController> logger) {
            this.geocodingService = geocodingService;
            this.logger = logger;
        }

        /// <summary>
        /// Find the coordinate corresponding to the given address, can return multiple or zero matches as well.
        /// </summary>
        /// <example>
        /// http://localhost:50500/geocoding?address=buys+ballot+gebouw
        /// </example>
        /// <param name="address">Address to find the coordinate for.</param>
        [HttpGet("")]
        public IActionResult Geocode([FromQuery(Name = "address")] string address) {
            if (address == null)
                return BadRequest();

            GeocodingResponse response = geocodingService.Geocode(address);
            return HandleResponse(response);
        }

        /// <summary>
        /// Find the address corresponding to the given coordinate, can return multiple or zero matches as well.
        /// </summary>
        /// <example>
        /// http://localhost:50500/geocoding/reverse?lat=52.0873639&lng=5.1653642999999994
        /// </example>
        /// <param name="lat">Latitude.</param>
        /// <param name="lng">Longitude.</param>
        [HttpGet("reverse")]
        public IActionResult ReverseGeocode(PositionModel position) {
            if (position == null)
                return BadRequest();

            GeocodingResponse response = geocodingService.ReverseGeocode(position.Latitude, position.Longitude);

            return HandleResponse(response);
        }

        private IActionResult HandleResponse(GeocodingResponse response) {
            if (response == null)
                return NotFound();

            return Ok(response.ToDTO());
        }
    }
}
