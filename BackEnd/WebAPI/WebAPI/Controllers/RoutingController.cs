/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using WebAPI.Converters;
using System.Device.Location;
using WebAPI.WebModels;
using Microsoft.Extensions.Logging;
using Domain.Routing;

namespace WebAPI.Controllers {
    [Route("routing")]
    public class RoutingController : Controller {
        private IRoutingService routingService;
        private ILogger<RoutingController> logger;

        public RoutingController(IRoutingService routingService, ILogger<RoutingController> logger) {
            this.routingService = routingService;
            this.logger = logger;
        }

        /// <summary>
        /// Get the route from GraphHopper.
        /// </summary>
        /// <example>
        /// http://localhost:50500/routing?route=52.08767,5.16412,52.08338,5.17649,bike,shortest
        /// </example>
        /// <param name="startLat">Start latitude.</param>
        /// <param name="startLong">Start longitude.</param>
        /// <param name="destLat">Destination latitude.</param>
        /// <param name="destLong">Destination longitude.</param>
        /// <param name="vehicle">Vehicle for routing, example: "bike".</param>
        /// <param name="weighting">Weighting for routing, example: "shortest".</param>
        [HttpGet("")]
        public IActionResult GetRoute(RouteModel route) {
            if (route == null) {
                return BadRequest();
            }

            GeoCoordinate start = route.Start;
            GeoCoordinate dest = route.Destination;

            Routes routes = null;
            try {
                routes = routingService.GetAllRoutes(start, dest, route.Vehicle, route.Weighting);
            }
            catch (Exception RoutingError) {
                logger.LogInformation(RoutingError.StackTrace);
            }

            if (routes == null)
                return NotFound();

            return Ok(routes.ToDTO());
        }
    }
}
