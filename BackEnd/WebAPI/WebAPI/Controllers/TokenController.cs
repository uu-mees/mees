/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DTOs;
using Services.Implementations;
using System.Text;
using System.Security.Claims;
using Services.Interfaces;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Principal;
using Domain.Administration;
using WebAPI.DTOs.UserDTOs;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers {
    [Route("token")]
    public class TokenController : Controller {
        private readonly ConfigurationService configurationService;
        private readonly IAuthenticationService authenticationService;
        private ILogger<TokenController> logger;

        public TokenController(ConfigurationService configurationService, IAuthenticationService authenticationService,
            ILogger<TokenController> logger) {
            this.configurationService = configurationService;
            this.authenticationService = authenticationService;
            this.logger = logger;
        }

        [AllowAnonymous]
        [HttpPost()]
        public IActionResult GenerateToken([FromBody] UserAuthenticationDTO userAuthentication) {
            User user = authenticationService.CheckLogin(userAuthentication.Username, userAuthentication.Password);
            if (user == null) {
                return Unauthorized();
            }

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configurationService.AuthTokenConfiguration.AuthTokenKey));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime expires = DateTime.UtcNow.AddMinutes(configurationService.AuthTokenConfiguration.TimeToLiveMinutes);
            string issuer = configurationService.AuthTokenConfiguration.Issuer;

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            ClaimsIdentity identity = new ClaimsIdentity();
            identity.AddClaim(new Claim("userId", user.Id.ToString()));
            identity.AddClaim(new Claim("userRole", user.Role.ToString()));

            JwtSecurityToken token = handler.CreateJwtSecurityToken(
                subject: identity,
                signingCredentials: credentials,
                expires: expires,
                issuer: issuer);

            AuthenticationTokenDTO tokenDTO = new AuthenticationTokenDTO();
            tokenDTO.Token = handler.WriteToken(token);

            return Ok(tokenDTO);
        }
    }
}
