/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using WebAPI.DTOs;
using WebAPI.Converters;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Device.Location;
using System.Collections.Generic;
using WebAPI.WebModels;
using Domain.Exposure;
using System;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers {
    [Route("sensors")]
    public class SensorsController : Controller {
        private ISensorService sensorService;
        private ILogger<SensorsController> logger;

        public SensorsController(ISensorService sensorService, ILogger<SensorsController> logger) {
            this.sensorService = sensorService;
            this.logger = logger;
        }

        /// <summary>
        /// Gets all Sensors and their latest batches within an area.
        /// </summary>
        /// <param name="area">AreaModel defining the area with 4 points.</param>
        /// <returns>
        ///     Status code Ok with a collection of DTO objects containing Sensor and latest SensorMeasurementBatch data.
        ///     Returns status code BadRequest if area input is invalid. Returns status code NotFound if no sensors were found in the area.
        /// </returns>
        [HttpGet("batch")]
        public IActionResult GetLatestSensorBatches(AreaModel area) {
            if (area == null) {
                return BadRequest();
            }

            IEnumerable<Sensor> sensors = sensorService.GetSensors(new GeoCoordinate(area.SouthWestLatitude, area.SouthWestLongitude),
                                                                   new GeoCoordinate(area.NorthEastLatitude, area.NorthEastLongitude));

            if (sensors == null) {
                return NotFound();
            }

            IEnumerable <SensorBatchDTO> sensorBatchDtos = sensors.Select(x => x.ToBatchDTO(sensorService.GetLatestBatch(x)));

            return Ok(sensorBatchDtos);
        }

        /// <summary>
        /// Gets all SensorMeasurementBatches from a specific Sensor within a certain time interval.
        /// </summary>
        /// <param name="id">Id of sensor.</param>
        /// <param name="interval">Time interval.</param>
        /// <returns>
        ///     Status code Ok with collection of DTO objects containing SensorMeasurementBatch data.
        ///     Returns status code BadRequest if input id or interval are invalid. Returns status code NotFound if 
        ///     no batches were found within time interval or Sensor does not exist.
        /// </returns>
        [HttpGet("{id}/batches")]
        public IActionResult GetHistoricalSensorBatches(string id,
                                                        TimeIntervalModel interval) {
            if (interval == null || id == null || id == "") {
                return BadRequest();
            }

            IEnumerable<SensorMeasurementBatch> batches = sensorService.GetSensorBatches(id, interval.TimeFrom, interval.TimeTo);

            if (batches == null) {
                return NotFound();
            }

            return Ok(batches.Select(x => x.ToDTO()));
        }
    }
}
