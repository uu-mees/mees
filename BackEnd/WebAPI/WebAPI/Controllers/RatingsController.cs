/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;
using WebAPI.Converters;
using System.Device.Location;
using WebAPI.WebModels;
using Domain.Administration;
using WebAPI.DTOs.UserDTOs;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers {
    [Route("ratings")]
    public class RatingsController : Controller {
        private IRatingService ratingService;
        private ILogger<RatingsController> logger;

        public RatingsController(IRatingService ratingService, ILogger<RatingsController> logger) {
            this.ratingService = ratingService;
            this.logger = logger;
        }

        /// <summary>
        /// Add a UserRating to the database.
        /// </summary>
        /// <param name="rating">UserRating to add to database.</param>
        /// <returns>Ok if UserRating is sucessfully added to database, otherwise returns BadRequest.</returns>
        [HttpPost("")]
        public IActionResult PostRating([FromBody] UserRatingDTO rating) {
            if (rating == null || rating.Position == null || rating.Rating == 0 || rating.TimeStamp == null)
                return BadRequest();

            if (ratingService.AddRating(rating.FromDTO()) != null)
                return Ok();

            return BadRequest();
        }

        /// <summary>
        /// Gets all UserRatings within a certain circle.
        /// </summary>
        /// <param name="position">Center of circle.</param>
        /// <param name="radius">Radius of circle.</param>
        /// <returns>Ok with list of UserRatingDTO objects. List is empty if no ratings are found.</returns>
        [HttpGet("")]
        public IActionResult GetRatings(PositionModel position, float radius) {
            if (position == null || radius <= 0) {
                return BadRequest();
            }

            ICollection<UserRating> ratings = ratingService.GetRatings(new GeoCoordinate(position.Latitude, position.Longitude), radius);

            if (ratings == null)
                return NotFound();

            return Ok(ratings.Select(x => x.ToDTO()));
        }

        /// <summary>
        /// Gets all UserRatings that have been posted after a certain time.
        /// </summary>
        /// <param name="from">Start of the interval.</param>
        /// <returns>Ok with list of UserRatingDTO objects. List is empty if no ratings are found.</returns>
        [HttpGet("new")]
        public IActionResult GetRatings(DateTime from) {
            ICollection<UserRating> ratings = ratingService.GetNewRatings(from);

            if (ratings == null)
                return NotFound();

            return Ok(ratings.Select(x => x.ToDTO()));
        }

        /// <summary>
        /// Gets all UserRatings within a certain circle.
        /// </summary>
        /// <returns>Ok with list of UserRatingDTO objects. List is empty if no ratings are found.</returns>
        [HttpGet("all")]
        public IActionResult GetAllRatings() {
            ICollection<UserRating> ratings = ratingService.GetAllRatings();

            if (ratings == null)
                return NotFound();

            return Ok(ratings.Select(x => x.ToDTO()));
        }

        /// <summary>
        /// Gets average of all UserRatings within a certain circle.
        /// </summary>
        /// <param name="position">Center of circle.</param>
        /// <param name="radius">Radius of circle.</param>
        /// <returns>Ok with average rating as float.</returns>
        [HttpGet("average")]
        public IActionResult GetAverageRating(PositionModel position,
                                              [FromQuery(Name = "radius")] float radius) {
            if (position == null || radius <= 0) {
                return BadRequest();
            }
            double averageRating = ratingService.GetAverageRating(new GeoCoordinate(position.Latitude, position.Longitude), radius);

            return Ok(averageRating);
        }

        /// <summary>
        /// Gets all distinct UserRatings within a certain circle and the amount of times each rating occurs.
        /// </summary>
        /// <param name="position">Center of circle.</param>
        /// <param name="radius">Radius of circle.</param>
        /// <returns>Ok with dictionary of distinct ratings and their amount of occurence.</returns>
        [HttpGet("statistics")]
        public IActionResult GetRatingNumbers(PositionModel position,
                                                 [FromQuery(Name = "radius")] float radius) {
            if (position == null || radius <= 0) {
                return BadRequest();
            }

            Dictionary<float, int> ratingStatistics = ratingService.GetRatingNumbers(new GeoCoordinate(position.Latitude, position.Longitude), radius);

            return Ok(ratingStatistics);
        }
    }
}
