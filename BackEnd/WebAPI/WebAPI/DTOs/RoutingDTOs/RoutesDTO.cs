/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class RoutesDTO {
        //GeoJson format (so Start[0] = long, Start[1] = lat)
        public double[] Start { get; set; }
        public double[] Destination { get; set; }
        public string Vehicle { get; set; }
        public string Weighting { get; set; }
        public int Count { get; set; }
        //All routes found (can be more than 1 because GraphHopper tries to find as many alternative routes as possible).
        public List<RouteDTO> Routes { get; set; }
    }
}
