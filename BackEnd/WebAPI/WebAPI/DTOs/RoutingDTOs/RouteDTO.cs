/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class RouteDTO {
        //Total distance in meters
        public double Distance { get; set; }
        //Duration in miliseconds
        public long Duration { get; set; }
        //Calculated average exposure
        public double? Exposure { get; set; }
        //GeoJson format (so [0] = long, [1] = lat)
        public List<double[]> Path { get; set; }
        public List<double?> EdgeExposures { get; set; }
    }
}
