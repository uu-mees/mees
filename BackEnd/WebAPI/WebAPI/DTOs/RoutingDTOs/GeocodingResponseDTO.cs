/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class GeocodingResponseDTO {
        public List<GeocodingResultDTO> Results;
    }
    public class GeocodingResultDTO {
        public string Formatted_address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
