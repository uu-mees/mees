/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class TriggerDTO {
        public Guid TriggerId { get; set; }
        public DateTime TimeStamp { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Type { get; set; }
    }

    public class CurrentLocationTriggerDTO : TriggerDTO {
        public double[] Coordinate { get; set; }
        public double Exposure { get; set; }
    }

    public class ExposureDifferenceTriggerDTO : TriggerDTO {
        public double HourOne { get; set; }
        public double HourTwo { get; set; }
    }
}
