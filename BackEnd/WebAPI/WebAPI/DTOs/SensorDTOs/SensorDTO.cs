/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class SensorDTO {
        public string Id { get; set; }
        public string Label { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }

    public class SensorBatchDTO : SensorDTO {
        public SensorMeasurementBatchDTO Batch { get; set; }
    }
}
