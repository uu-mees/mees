/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class TokenAuthenticationPayloadDTO {
        public Guid UserId { get; set; }
        public string Issuer { get; set; }
        public DateTime Expires { get; set; }
        public string Audience { get; set; }

    }
}
