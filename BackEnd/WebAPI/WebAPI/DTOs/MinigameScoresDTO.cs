/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    public class MinigameScoresDTO {
        public string userID { get; set; }
        public DateTime timeStamp { get; set; }
        public int? score { get; set; }
        public int? misses { get; set; }
        public double? timeInSec { get; set; }
        public string gameMode { get; set; }
        public bool mobile { get; set; }
    }
}
