/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs.UserDTOs {
    /// <summary>
    /// DTO for <see cref="Domain.Administration.UserProfile"/> conversion. Used for partial updates on <see cref="Domain.Administration.User"/>.
    /// </summary>
    public class UserProfileDTO {
        public string Username { get; set; }
        public string Email { get; set; }

        public bool TrackLocation { get; set; }
        public int? Height { get; set; }
        public float? Weight { get; set; }
        public string Sex { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomeAddress { get; set; }
        public string WorkAddress { get; set; }
    }
}
