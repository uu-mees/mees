/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs {
    /// <summary>
    /// DTO for returning User information to the client.
    /// </summary>
    public class UserDTO {
        public string Id;
        public string Username;
        public Role Role;
    }
}
