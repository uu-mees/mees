/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.DTOs.UserDTOs {
    /// <summary>
    /// DTO used in resetting the <see cref="Domain.Administration.User"/> password.
    /// </summary>
    public class ResetPasswordDTO {
        // In the database this token is a GUID.
        public string Token { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
