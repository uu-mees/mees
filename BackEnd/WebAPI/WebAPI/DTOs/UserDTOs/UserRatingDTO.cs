/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.WebModels;

namespace WebAPI.DTOs.UserDTOs {
    /// <summary>
    /// DTO for <see cref="Domain.Administration.UserRating"/>. Used in <see cref="Services.Implementations.RatingService"/>.
    /// </summary>
    public class UserRatingDTO {
        public PositionModel Position { get; set; }

        public int Rating { get; set; }

        public DateTime TimeStamp { get; set; }

        public Guid UserId { get; set; }
    }
}
