/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace WebAPI.DTOs.UserDTOs {
    /// <summary>
    /// DTO for <see cref="Domain.Administration.PasswordPair"/> conversion. Used for updating <see cref="Domain.Administration.User.HashedPassword"/> of <see cref="Domain.Administration.User"/>.
    /// </summary>
    public class PasswordPairDTO {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
