/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Threading;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Topshelf;
using NLog.Web;

namespace WebAPI {
    public class Program {
        // This method utilizes the TopShelf package to run our application as a service.
        public static void Main(string[] args) {
            TopshelfExitCode rc = HostFactory.Run(x => {
                x.Service<Wrapper>(s => {
                    s.ConstructUsing(name => new Wrapper(name));
                    s.WhenStarted(wr => wr.start());
                    s.WhenStopped(wr => wr.stop());
                });
                x.UseNLog();
                x.SetDescription("Mijn leefomgeving en ik Back-End service");
                x.StartAutomatically();
                x.SetDisplayName("MijnLeefomgeving");
                x.SetServiceName("MijnLeefomgeving");
            });

            int exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }

    }

    // This wrapper object is a handler for TopShelf and contains the necessary Start and Stop methods.
    public class Wrapper {
        private IWebHost host;
        Thread t;
        string name;
        public Wrapper(string name) {
            this.name = name;
        }

        // Start creates a new thread. It is the new thread that actually builds the WebAPI and then runs it.
        // The reason is that we thread only has ~30 seconds to return to the caller before Windows deems the service unrunnable.
        // The host build call can be long, and the run call is blocking, so we won't make that.
        public void start() {
            t = new Thread(() => deepstart());
            t.Name = name;
            t.Start();
        }

        private void deepstart() {
            host = BuildWebHost();
            host.Run();
        }

        // When we stop we stop the host and abort the thread.
        public void stop() {
            NLog.LogManager.Shutdown();

            if (host != null) {
                host.StopAsync();
            }
            t.Abort();
        }

        // Build the WebHost with the necessary settings.
        public static IWebHost BuildWebHost() {
#if !DEBUG
            return WebHost.CreateDefaultBuilder()
              .UseStartup<Startup>()
              .UseEnvironment("Production")
              .UseNLog()
              .Build();
#else
            return WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .UseEnvironment("Development")
                .UseUrls("http://localhost:50500")
                .UseNLog()
                .Build();
#endif

        }
    }
}
