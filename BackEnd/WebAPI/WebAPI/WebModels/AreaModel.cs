/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Device.Location;
using System.Globalization;
using System.Web.Http.ModelBinding;
using Utils;
using WebAPI.WebModels.ModelBinders;

namespace WebAPI.WebModels {
    /// <summary>
    /// AreaModel containing a south west and north east position.
    /// </summary>
    [ModelBinder(typeof(AreaModelBinder))]
    public class AreaModel {
        public double SouthWestLatitude { get; set; }
        public double SouthWestLongitude { get; set; }
        public double NorthEastLatitude { get; set; }
        public double NorthEastLongitude { get; set; }
        public GeoCoordinate SouthWestPoint { get => new GeoCoordinate(SouthWestLatitude, SouthWestLongitude); }
        public GeoCoordinate NorthEastPoint { get => new GeoCoordinate(NorthEastLatitude, NorthEastLongitude); }

        /// <summary>
        /// Try parsing a string to AreaModel.
        /// </summary>
        /// <param name="s">String to try to parse.</param>
        /// <returns>Null if parsing failed, otherwise returns AreaModel.</returns>
        public static AreaModel TryParse(string s) {
            if (s == null) {
                return null;
            }

            string[] parts = s.Split(',');
            if (parts.Length != 4) {
                return null;
            }

            if (double.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out double swLat) &&
               double.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out double swLong) &&
               double.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out double neLat) &&
               double.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out double neLong)) {
                if (IsValid(swLat, swLong, neLat, neLong)) {
                    return new AreaModel { SouthWestLatitude = swLat, SouthWestLongitude = swLong, NorthEastLatitude = neLat, NorthEastLongitude = neLong };
                }
            }

            return null;
        }
        /// <summary>
        /// Checks whether the given coordinates indicate a valid area.
        /// </summary>
        /// <param name="southWestLatitude">Southwest point latitude.</param>
        /// <param name="southWestLongitude">Southwest point longitude.</param>
        /// <param name="northEastLatitude">Northeast point latitude.</param>
        /// <param name="northEastLongitude">Northeast point longitude.</param>
        /// <returns>Boolean indicating whether the given coordinates are valid.</returns>
        private static bool IsValid(double southWestLatitude, double southWestLongitude, double northEastLatitude, double northEastLongitude) {
            if (!GeoUtils.IsValidLatitude(southWestLatitude) || !GeoUtils.IsValidLatitude(northEastLatitude)) {
                return false;
            }

            if (!GeoUtils.IsValidLongitude(southWestLongitude) || !GeoUtils.IsValidLongitude(northEastLongitude)) {
                return false;
            }

            // Check if the southwest corner is below and to the right of the northeast corner.
            if (southWestLatitude >= northEastLatitude)
                return false;
            if (southWestLongitude >= northEastLongitude)
                return false;

            return true;
        }
    }
}
