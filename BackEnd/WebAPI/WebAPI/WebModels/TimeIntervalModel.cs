/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Device.Location;
using System.Globalization;
using System.Web.Http.ModelBinding;
using WebAPI.WebModels.ModelBinders;

namespace WebAPI.WebModels {
    /// <summary>
    /// TimeIntervalModel containing two points in time.
    /// </summary>
    [ModelBinder(typeof(TimeIntervalModelBinder))]
    public class TimeIntervalModel {
        public DateTime TimeFrom;
        public DateTime TimeTo;

        /// <summary>
        /// Try parsing a string to TimeIntervalModel.
        /// </summary>
        /// <param name="s">String to try to parse.</param>
        /// <returns>Null if parsing failed, otherwise returns TimeIntervalModel.</returns>
        public static TimeIntervalModel TryParse(string s) {
            if (s == null) {
                return null;
            }

            string[] parts = s.Split(',');
            // A TimeInterval consists of 2 DateTimes (TimeFrom and TimeTo).
            if (parts.Length != 2) {
                return null;
            }

            // Try parsing the DateTimes culture-independent and with standard format.
            if (DateTime.TryParse(parts[0], CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime timeFrom) &&
               DateTime.TryParse(parts[1], CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime timeTo)) {
                // Start time can not be later than end time.
                if (timeFrom >= timeTo) {
                    return null;
                }

                return new TimeIntervalModel { TimeFrom = timeFrom.ToUniversalTime(), TimeTo = timeTo.ToUniversalTime() };
            }

            return null;
        }
    }
}
