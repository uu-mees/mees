/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.WebModels.ModelBinders;

namespace WebAPI.WebModels.ModelBinderProviders {
    /// <summary>
    /// ModelBinderProvider for PositionModel.
    /// </summary>
    public class PositionModelBinderProvider : IModelBinderProvider {
        /// <summary>
        /// Get the correct ModelBinder for PositionModel.
        /// </summary>
        public IModelBinder GetBinder(ModelBinderProviderContext context) {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(PositionModel)) {
                return new PositionModelBinder();
            }

            return null;
        }
    }
}
