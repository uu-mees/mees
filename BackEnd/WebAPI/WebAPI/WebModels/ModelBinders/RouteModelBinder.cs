/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.WebModels.ModelBinders {
    /// <summary>
    /// ModelBinder for RouteModelBinder
    /// </summary>
    public class RouteModelBinder : IModelBinder {
        /// <summary>
        /// Bind the RouteModel.
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public Task BindModelAsync(ModelBindingContext bindingContext) {
            if (bindingContext == null) {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            // Specify a default argument name if none is set by ModelBinderAttribute
            string modelname = bindingContext.BinderModelName;
            if (string.IsNullOrEmpty(modelname)) {
                modelname = "Route";
            }

            // Try to fetch the value of the argument by name
            ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(modelname);
            if (valueProviderResult == ValueProviderResult.None) {
                return Task.CompletedTask;
            }

            bindingContext.ModelState.SetModelValue(modelname, valueProviderResult);
            string value = valueProviderResult.FirstValue;

            // Check if the argument value is null or empty
            if (string.IsNullOrEmpty(value)) {
                return Task.CompletedTask;
            }

            RouteModel route = RouteModel.TryParse(value);
            if (route == null) {
                // Invalid arguments result in model state errors (see RouteModel.cs for specifics)
                bindingContext.ModelState.TryAddModelError(bindingContext.ModelName, "Could not parse to RouteModel.");
                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(route);
            return Task.CompletedTask;
        }
    }
}
