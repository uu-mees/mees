/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace WebAPI.WebModels.ModelBinders {
    /// <summary>
    /// ModelBinder for TimeIntervalModel.
    /// </summary>
    public class TimeIntervalModelBinder : IModelBinder {
        /// <summary>
        /// Bind the correct Model.
        /// </summary>
        public Task BindModelAsync(ModelBindingContext bindingContext) {
            if (bindingContext == null) {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            // Specify a default argument name if none is set by ModelBinderAttribute.
            string modelName = bindingContext.BinderModelName;
            if (string.IsNullOrEmpty(modelName)) {
                modelName = "Interval";
            }

            // Try to fetch the value of the argument by name.
            ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);

            if (valueProviderResult == ValueProviderResult.None) {
                return Task.CompletedTask;
            }

            bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            string value = valueProviderResult.FirstValue;

            // Check if the argument value is null or empty.
            if (string.IsNullOrEmpty(value)) {
                return Task.CompletedTask;
            }

            TimeIntervalModel interval = TimeIntervalModel.TryParse(value);
            if (interval == null) {
                // Non-integer arguments result in model state errors.
                bindingContext.ModelState.TryAddModelError(bindingContext.ModelName, "Could not parse to TimeIntervalModel.");
                return Task.CompletedTask;
            }

            bindingContext.Result = ModelBindingResult.Success(interval);
            return Task.CompletedTask;
        }

    }
}
