/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using Utils;
using WebAPI.WebModels.ModelBinders;

namespace WebAPI.WebModels {
    /// <summary>
    /// PositionModel containing Latitude and Longitude values.
    /// </summary>
    [ModelBinder(typeof(PositionModelBinder))]
    public class PositionModel {
        public double Latitude;
        public double Longitude;

        /// <summary>
        /// Try parsing a string to PositionModel.
        /// </summary>
        /// <param name="s">String to parse.</param>
        /// <returns>Null if parsing failed, otherwise returns PositionModel.</returns>
        public static PositionModel TryParse(string s) {
            if (s == null) {
                return null;
            }

            string[] parts = s.Split(',');
            // A Position consists of a latitude and longitude
            if (parts.Length != 2) {
                return null;
            }

            if (double.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out double latitude) &&
                double.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out double longitude)) {
                if (IsValid(latitude, longitude)) {
                    return new PositionModel { Latitude = latitude, Longitude = longitude };
                }
            }

            return null;
        }

        /// <summary>
        /// Try parsing longitude and latitude to PositionModel.
        /// </summary>
        /// <param name="latitude">Double latitude to parse.</param>
        /// <param name="longitude">Double longitude to parse.</param>
        /// <returns>Null if parameters are not valid, otherwise returns PositionModel.</returns>
        public static PositionModel TryParse(double latitude, double longitude) {
            if (IsValid((float)latitude, (float)longitude)) {
                return new PositionModel { Latitude = latitude, Longitude = longitude };
            }
            else {
                return null;
            }
        }


        /// <summary>
        /// Test whether the given latitude and longitude are valid.
        /// </summary>
        /// <param name="latitude">The latitude you want to test.</param>
        /// <param name="longitude">The longitude you want to test.</param>
        /// <returns>Bool indicating the validity of both values.</returns>
        private static bool IsValid(double latitude, double longitude) {
            if (GeoUtils.IsValidLatitude(latitude) && GeoUtils.IsValidLongitude(longitude)) {
                return true;
            }

            return false;
        }
    }
}
