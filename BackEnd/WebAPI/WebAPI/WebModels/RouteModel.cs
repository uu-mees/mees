/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using Utils;
using WebAPI.WebModels;
using WebAPI.WebModels.ModelBinders;

namespace WebAPI.WebModels {
    /// <summary>
    /// RouteModel that is used to request a route with a start location, destination, form of transport(vehicle), and search algorithm.
    /// </summary>
    [ModelBinder(typeof(RouteModelBinder))]
    public class RouteModel {
        public double StartLat { get; set; }
        public double StartLong { get; set; }
        public double DestLat { get; set; }
        public double DestLong { get; set; }
        public string Vehicle { get; set; }
        public string Weighting { get; set; }
        public GeoCoordinate Start { get => new GeoCoordinate(StartLat, StartLong); }
        public GeoCoordinate Destination { get => new GeoCoordinate(DestLat, DestLong); }

        /// <summary>
        /// Validates request input and binds it to a model if valid.
        /// </summary>
        /// <param name="s">The GET request string that needs to be parsed.</param>
        /// <returns>RouteModel if request is valid, NULL when invalid.</returns>
        public static RouteModel TryParse(string s) {
            if (s == null) {
                return null;
            }

            string[] parts = s.Split(',');
            // Request must have 6 parameters.
            if (parts.Length != 6) {
                return null;
            }

            // Cleans input, if parse fails then request is invalid.
            if (double.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out double sLat) &&
                double.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out double sLong) &&
                double.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out double dLat) &&
                double.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out double dLong) &&
                parts[4] != null &&
                parts[5] != null) {
                string vehicle = parts[4];
                string weighting = parts[5];

                // If true, request is valid. Return a RouteModel.
                if (IsValid(sLat, sLong, dLat, dLong, vehicle, weighting)) {
                    return new RouteModel() {
                        StartLat = sLat,
                        StartLong = sLong,
                        DestLat = dLat,
                        DestLong = dLong,
                        Vehicle = vehicle,
                        Weighting = weighting
                    };
                }
            }
            return null;
        }

        /// <summary>
        /// Validate the request string.
        /// </summary>
        /// <param name="sLat">Latitude of start coordinate.</param>
        /// <param name="sLong">Longitude of start coordinate.</param>
        /// <param name="dLat">Latitude of destination coordinate.</param>
        /// <param name="dLong">Longitude of destination coordinate.</param>
        /// <param name="vehicle">String that specifies vehicle.</param>
        /// <param name="weighting">Specifies what routeplanning algorithm is used in GraphHopper.</param>
        /// <returns>True if string is valid for RouteModel.</returns>
        private static bool IsValid(double sLat, double sLong, double dLat, double dLong, string vehicle, string weighting) {
            // Verify that sLat, sLong, dLat && dLong are valid geocoordinates.
            if (!GeoUtils.IsValidLatitude(sLat) || !GeoUtils.IsValidLongitude(sLong)) {
                return false;
            }

            if (!GeoUtils.IsValidLatitude(dLat) || !GeoUtils.IsValidLongitude(dLong)) {
                return false;
            }

            // Vehicle (mode of transport) && Weighting can not be empty ("").
            if (!(vehicle.Length > 0) || vehicle == null || weighting == null || !(weighting.Length > 0)) {
                return false;
            }

            return true;
        }
    }
}
