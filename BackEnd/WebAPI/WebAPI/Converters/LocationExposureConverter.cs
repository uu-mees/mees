/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Tracking;
using Services.DTOs;
using Services.Implementations;

namespace WebAPI.Converters {
    public static class LocationExposureConverter {

        /// <summary>
        /// Converts the received locationDataDTO and turn it into LocationExposure. 
        /// Can't get the exposure value from ExposureService from here so that's done where this is called.
        /// </summary>
        /// <param name="locationDataDTO"></param>
        /// <returns></returns>
        public static UserLocationExposure fromDTO(this UserLocationDataDTO locationDataDTO) {

            UserLocationExposure locationExposure = new UserLocationExposure(){

                Position = new Location(locationDataDTO.latitude, locationDataDTO.longitude),
                Timestamp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(locationDataDTO.timestamp),
                UserID = Guid.Parse(locationDataDTO.userID)
            };

            return locationExposure;
        }
    }
}
