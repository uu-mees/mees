/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class TriggerConverter {
        public static TriggerDTO ToDTO(this Trigger trigger) {
            if (trigger == null) {
                return null;
            }
            else if (trigger is DefaultTrigger) {
                return new TriggerDTO() {
                    Type = "DefaultTrigger",
                    TimeStamp = (DateTime)trigger.TimeStamp,
                    TriggerId = trigger.TriggerId,
                    UserId = trigger.UserId,
                    Username = trigger.User.UserName
                };
            }
            else if (trigger is ExposureTrigger) {
                return new TriggerDTO() {
                    Type = "ExposureTrigger",
                    TimeStamp = (DateTime)trigger.TimeStamp,
                    TriggerId = trigger.TriggerId,
                    UserId = trigger.UserId,
                    Username = trigger.User.UserName
                };
            }
            else if (trigger is DistanceTrigger) {
                return new TriggerDTO() {
                    Type = "DistanceTrigger",
                    TimeStamp = (DateTime)trigger.TimeStamp,
                    TriggerId = trigger.TriggerId,
                    UserId = trigger.UserId,
                    Username = trigger.User.UserName
                };
            }
            else {
                throw new NotImplementedException();
            }
        }
    }
}
