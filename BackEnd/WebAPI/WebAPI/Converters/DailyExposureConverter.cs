/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class DailyExposureConverter {
        public static DailyExposureDTO ToDTO(this DailyExposure exposure) {
            return new DailyExposureDTO() {
                Date = exposure.Date,
                AverageExposure = exposure.AverageExposure,
            };
        }
    }
}
