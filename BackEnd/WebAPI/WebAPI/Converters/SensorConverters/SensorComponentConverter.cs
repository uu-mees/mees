/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Exposure;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class SensorComponentConverter {
        public static SensorComponentDTO ToDTO(this SensorComponent component) {
            return new SensorComponentDTO() {
                Id = component.Id,
                Component = component.Component,
                Unit = component.Unit
            };
        }
    }
}
