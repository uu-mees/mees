/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class SensorMeasurementConverter {
        public static SensorMeasurementDTO ToDTO(this SensorMeasurement measurement) {
            float sensorValue;
            if (measurement.Value == "None")
                sensorValue = -999.0f;
            else
                sensorValue = float.Parse(measurement.Value, System.Globalization.CultureInfo.InvariantCulture);
            return new SensorMeasurementDTO() {
                Id = measurement.Id,
                BatchId = measurement.BatchId,
                Value = sensorValue,
                Component = measurement.Component.Component,
                Unit = measurement.Component.Unit,
            };
        }
    }
}
