/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class SensorConverter {
        public static SensorDTO ToDTO(this Sensor sensor) {
            return new SensorDTO() {
                Id = sensor.Id,
                Label = sensor.Label,
                Latitude = sensor.Latitude,
                Longitude = sensor.Longitude
            };
        }

        public static SensorBatchDTO ToBatchDTO(this Sensor sensor, SensorMeasurementBatch batch) {
            return new SensorBatchDTO() {
                Id = sensor.Id,
                Label = sensor.Label,
                Latitude = sensor.Latitude,
                Longitude = sensor.Longitude,
                Batch = batch.ToDTO()
            };
        }
    }
}
