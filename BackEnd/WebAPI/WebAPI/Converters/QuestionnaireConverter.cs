/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class QuestionnaireConverter {
        public static QuestionnaireDTO ToDTO(this Questionnaire questionnaire) {
            return new QuestionnaireDTO() {
                QuestionnaireId = questionnaire.QuestionnaireId,
                QuestionnaireString = questionnaire.QuestionnaireString
            };
        }
    }
}
