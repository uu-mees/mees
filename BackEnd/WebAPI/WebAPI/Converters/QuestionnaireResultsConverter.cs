/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class QuestionnaireResultsConverter {
        public static QuestionnaireResultsDTO ToDTO(this List<Questionnaire> questionnaires) {
            return new QuestionnaireResultsDTO() {
                Results = questionnaires.ConvertAll(x => QuestionnaireResultConverter.ToDTO(x))
            };
        }
    }

    public static class QuestionnaireResultConverter {
        public static QuestionnaireResultDTO ToDTO(this Questionnaire questionnaire) {
            return new QuestionnaireResultDTO() {
                QuestionnaireId = questionnaire.QuestionnaireId,
                QuestionnaireString = questionnaire.QuestionnaireString,
                Trigger = questionnaire.Trigger.ToDTO()
            };
        }
    }
}
