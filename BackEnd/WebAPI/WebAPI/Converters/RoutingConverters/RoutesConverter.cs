/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class RoutesConverter {
        /// <summary>
        /// Convert Routes to RoutesDTO.
        /// </summary>
        /// <param name="routes">The Routes object to convert.</param>
        /// <returns>RoutesDTO.</returns>
        public static RoutesDTO ToDTO(this Routes routes) {
            return new RoutesDTO() {
                Start = new double[2] { routes.Start.Longitude, routes.Start.Latitude },
                Destination = new double[2] { routes.Destination.Longitude, routes.Destination.Latitude },
                Vehicle = routes.Vehicle,
                Weighting = routes.Weighting,
                Count = routes.Count,
                Routes = routes.RouteList.ConvertAll(x => x.ToDTO())
            };
        }
    }
}
