/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class RouteConverter {
        /// <summary>
        /// Convert Route to RouteDTO.
        /// </summary>
        /// <param name="route">The Route object to convert.</param>
        /// <returns>RouteDTO.</returns>
        public static RouteDTO ToDTO(this Route route) {
            return new RouteDTO() {
                Duration = route.Duration,
                Distance = route.Distance,
                Exposure = route.ExposurePath.Exposure,
                Path = route.ExposurePath.Path.ConvertAll(x => new double[2] { x.Longitude, x.Latitude }),
                EdgeExposures = route.ExposurePath.EdgeExposures
            };
        }
    }
}
