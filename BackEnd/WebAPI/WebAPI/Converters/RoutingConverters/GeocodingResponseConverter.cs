/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class GeocodingResponseConverter {
        /// <summary>
        /// Convert GeocodingResponse to GeocodingResponseDTO.
        /// </summary>
        /// <param name="geocodingResponse">The GeocodingResponse object to convert.</param>
        /// <returns>GeocodingResponseDTO.</returns>
        public static GeocodingResponseDTO ToDTO(this GeocodingResponse geocodingResponse) {
            return new GeocodingResponseDTO() {
                Results = geocodingResponse.Results.ConvertAll(x => x.ToDTO())
            };
        }
    }
}
