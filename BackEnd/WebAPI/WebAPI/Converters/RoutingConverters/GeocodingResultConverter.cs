/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    public static class GeocodingResultConverter {
        /// <summary>
        /// Convert GeocodingResult to GeocodingResultDTO.
        /// </summary>
        /// <param name="geocodingResult">The GeocodingResult object to convert.</param>
        /// <returns>GeocodingResultDTO.</returns>
        public static GeocodingResultDTO ToDTO(this GeocodingResult geocodingResult) {
            return new GeocodingResultDTO() {
                Formatted_address = geocodingResult.FormattedAddress,
                Latitude = geocodingResult.Location.Latitude,
                Longitude = geocodingResult.Location.Longitude
            };
        }
    }
}
