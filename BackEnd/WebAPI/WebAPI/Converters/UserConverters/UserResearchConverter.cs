/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Administration;
using WebAPI.DTOs.UserDTOs;

namespace WebAPI.Converters.UserConverters {
    public static class UserResearchConverter {
        public static UserResearchDTO toDTO(this Researches research) {
            return new UserResearchDTO() {
                Id = research.Id,
                Name = research.Name,
                Description = research.Description,
                StartTime = research.StartTime,
                EndTime = research.EndTime,
                ViewRatings = research.ViewRatings,
                ViewSensors = research.ViewSensors,
                ViewRaster = research.ViewRaster,
                ViewExposure = research.ViewExposure,
                ViewMinigameScores = research.ViewMinigameScores,
                ViewHighScores = research.ViewHighScores,
                ViewRoutePlanner = research.ViewRoutePlanner
            };
        }
    }
}
