/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Converters {
    /// <summary>
    /// Converter for converting User to UserDTO.
    /// </summary>
    public static class UserConverter {
        public static UserDTO ToDTO(this User user) {
            return new UserDTO {
                Username = user.UserName,
                Id = user.Id.ToString(),
                Role = user.Role,
            };
        }
    }
}
