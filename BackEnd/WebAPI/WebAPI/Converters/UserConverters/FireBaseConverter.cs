/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using WebAPI.DTOs.UserDTOs;
using System;

namespace WebAPI.Converters.UserConverters {
    /// <summary>
    /// Converts FireBaseDTO into a FireBase to be used to store in the database
    /// </summary>
    public static class FireBaseConverter {
        public static FireBase FromDTO(this FireBaseDTO fireBaseDTO) {
            return new FireBase() {
                FireBaseID = fireBaseDTO.FireBaseID,
                UserID = Guid.Parse(fireBaseDTO.UserIDToken)
            };
        }
    }
}
