/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs.UserDTOs;
using Domain.Administration;
using System.Runtime;

namespace WebAPI.Converters.UserConverters {
    /// <summary>
    /// Converter for <see cref="PasswordPairDTO"/> used to change the <see cref="User.HashedPassword"/>.
    /// </summary>
    public static class PasswordPairConverter {
        public static PasswordPair FromDTO(this PasswordPairDTO passwordPair) {
            return new PasswordPair() {
                OldPassword = passwordPair.OldPassword,
                NewPassword = passwordPair.NewPassword
            };
        }
    }
}
