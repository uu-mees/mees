/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.DTOs.UserDTOs;

namespace WebAPI.Converters {
    public static class UserRegistrationConverter {
        /// <summary>
        /// Parse UserRegistrationDTO to UserRegistration domain object.
        /// </summary>
        /// <param name="userRegistrationDTO">UserRegistrationDTO to be parsed.</param>
        /// <returns>UserRegistration domain object containing values from the UserRegistrationDTO.</returns>
        public static UserRegistration FromDTO(this UserRegistrationDTO userRegistrationDTO) {
            return new UserRegistration() {
                Username = userRegistrationDTO.Username,
                Password = userRegistrationDTO.Password,
                Email = userRegistrationDTO.Email,
                TrackLocation = userRegistrationDTO.TrackLocation,
                Height = userRegistrationDTO.Height,
                Weight = userRegistrationDTO.Weight,
                Sex = userRegistrationDTO.Sex,
                DateOfBirth = userRegistrationDTO.DateOfBirth,
                HomeAddress = userRegistrationDTO.HomeAddress,
                WorkAddress = userRegistrationDTO.WorkAddress
            };
        }
    }
}
