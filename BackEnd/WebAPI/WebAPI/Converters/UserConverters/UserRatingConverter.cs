/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WebAPI.DTOs;
using WebAPI.DTOs.UserDTOs;
using WebAPI.WebModels;

namespace WebAPI.Converters {
    public static class UserRatingConverter {
        public static UserRatingDTO ToDTO(this UserRating rating) {
            return new UserRatingDTO() {
                Position = PositionModel.TryParse(rating.Latitude, rating.Longitude),
                Rating = rating.Rating,
                TimeStamp = rating.TimeStamp
            };

        }

        public static UserRating FromDTO(this UserRatingDTO rating) {
            return new UserRating() {
                Latitude = rating.Position.Latitude,
                Longitude = rating.Position.Longitude,
                Rating = rating.Rating,
                TimeStamp = rating.TimeStamp,
                UserId = rating.UserId
            };
        }
    }
}
