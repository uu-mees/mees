/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Administration;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using WebAPI.DTOs.UserDTOs;

namespace WebAPI.Converters.UserConverters {
    /// <summary>
    /// Converter for <see cref="UserProfileDTO"/> used for retrieving or updating the <see cref="UserProfile"/>.
    /// </summary>
    public static class UserProfileConverter {
        public static UserProfileDTO ToDTO(this UserProfile profile) {
            return new UserProfileDTO() {
                Username = profile.Username,
                Email = profile.Email,
                TrackLocation = profile.TrackLocation,
                Height = profile.Height,
                Weight = profile.Weight,
                Sex = profile.Sex,
                DateOfBirth = profile.DateOfBirth,
                HomeAddress = profile.HomeAddress,
                WorkAddress = profile.WorkAddress
            };
        }

        public static UserProfile FromDTO(this UserProfileDTO profile) {
            return new UserProfile() {
                Username = profile.Username,
                Email = profile.Email,
                TrackLocation = profile.TrackLocation,
                Height = profile.Height,
                Weight = profile.Weight,
                Sex = profile.Sex,
                DateOfBirth = profile.DateOfBirth,
                HomeAddress = profile.HomeAddress,
                WorkAddress = profile.WorkAddress
            };
        }
    }
}
