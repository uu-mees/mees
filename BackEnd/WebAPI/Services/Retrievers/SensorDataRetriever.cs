/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using HtmlAgilityPack;
using Services.DTOs;
using Services.Implementations;
using System;
using Domain.Administration;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using Utils;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Services.Retrievers {
    /// <summary>
    /// A container class for hyperlinks to files.
    /// </summary>
    public class DataLink {
        /// <summary>The link to the data.</summary>
        public string Link { get; set; }

        /// <summary>The <see cref="DateTime"/> the file associated to the link was uploaded</summary>
        public DateTime TimeStamp { get; set; }
    }

    /// <summary>
    /// A retriever service for sensor data.
    /// </summary>
    /* Retrievers are dependent on third party data which might change or be unavailable at certain times,
     * this makes testing these consistently impossible. It receives data and passes it on, which is trivial
     * as long as the end points and their format stay the same. If this changes there will need to be alterations
     * anyway, unit testing it will not help. These are covered by integration tests.
     */
    [ExcludeFromCodeCoverage]
    public class SensorDataRetriever : IRetriever<List<DataLink>> {
        bool active = true;
        private ConfigurationService configurationService;
        private readonly ILogger<SensorDataInjectionService> logger;

        /// <summary>
        /// The constructor for the retriever service.
        /// </summary>
        /// <param name="configurationService">
        /// An instance of <see cref="ConfigurationService"/>, 
        /// containing the location from where the data should be retrieved.
        /// </param>
        public SensorDataRetriever(ConfigurationService configurationService, ILogger<SensorDataInjectionService> logger) {
            this.configurationService = configurationService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieve page containing links to XML files containing sensordata.
        /// </summary>
        /// <returns>A list of <see cref="DataLink"/>s.</returns>
        public List<DataLink> RetrieveData() {
            try {
                using (DatabaseContext context = new DatabaseContext(configurationService.ConnectionString)) {
                    //Grabs the dataSource from the database. 
                    //If this is not possible it just skips the rest and continues with the current settings/defaults
                    //Type 1 == Sensor, Type 0 == Raster
                    DataSource dataSource = context.DataSources.FirstOrDefault(x => x.Type == 1);
                    if (dataSource != null) {
                        string newDataSource = dataSource.URL;
                        active = dataSource.Active;
                        if (newDataSource != configurationService.SensorURL)
                            configurationService.SensorURL = newDataSource;
                    }
                }
            }
            catch {
                Console.WriteLine("Couldn't retrieve the Sensor DataSource");
            }

            //Only retrieve new data if retrieval should be active
            if (active) {
                // Retrieve Data
                string url = configurationService.SensorURL;

                List<DataLink> dataLinks = null;

                try {
                    // Request the index of links to sensordata from the given server.
                    WebRequest webRequest = WebRequest.Create(url);
                    WebResponse webResponse = webRequest.GetResponse();

                    HtmlDocument html = new HtmlDocument();
                    string htmlString = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                    html.LoadHtml(htmlString);

                    // Parse the response.
                    dataLinks = ParseDataLinks(html);

                    webResponse.Close();
                }
                catch (Exception e) {
                    logger.LogCritical(e.StackTrace);
                }


                return dataLinks;
            }
            return null;
        }

        /// <summary>
        /// Retrieve sensordata from specific URL.
        /// </summary>
        /// <param name="dataLink">A <see cref="DataLink"/> object.</param>
        /// <returns>The retrieved, correctly formatted <see cref="SensorDataDTO"/>.</returns>
        public SensorDataDTO RetrieveSensorData(DataLink dataLink) {
            string completeURL = configurationService.SensorURL + dataLink.Link;

            // Request the data corresponding to the DataLink.
            WebRequest webRequest = WebRequest.Create(completeURL);
            WebResponse webResponse = webRequest.GetResponse();

            // Parse the response.
            SensorDataDTO sensorDataDTO = ParseXML(webResponse.GetResponseStream(), dataLink.TimeStamp);

            webResponse.Close();

            return sensorDataDTO;
        }

        /// <summary>
        /// Parse HTML page into a list of <see cref="DataLink"/>s.
        /// </summary>
        /// <param name="html"><see cref="HtmlDocument"/> containing HTML page.</param>
        /// <returns>A list of <see cref="DataLink"/>s.</returns>
        public List<DataLink> ParseDataLinks(HtmlDocument html) {
            List<DataLink> result = new List<DataLink>();

            // Loop over all external links.
            foreach (HtmlNode link in html.DocumentNode.SelectNodes("//a[@href]")) {
                // Select only the links to xml files.
                if (link.InnerHtml.EndsWith(".xml")) {
                    string[] split = link.InnerHtml.Split('-', '.');
                    string format = "yyyyMMddHHmmss";

                    if (split[1] == "actueel")
                        continue;

                    DateTime dateTime = Extensions.ParseDateTime(split[1], format).ToUniversalTime();

                    result.Add(new DataLink() { Link = link.InnerHtml, TimeStamp = dateTime });
                }
            }

            // Sort the results by their timestamp.
            result.Sort((a, b) => a.TimeStamp < b.TimeStamp ? -1 : 1);

            return result;
        }

        /// <summary>
        /// Parse XML file into a <see cref="SensorDataDTO"/>.
        /// </summary>
        /// <param name="xmlStream">Stream containing XML file.</param>
        /// <param name="timeStampPolled"><see cref="DateTime"/> at which the XML file was uploaded.</param>
        /// <returns>The parsed <see cref="SensorDataDTO"/>.</returns>
        public SensorDataDTO ParseXML(Stream xmlStream, DateTime timeStampPolled) {
            using (XmlReader reader = XmlReader.Create(xmlStream)) {
                XmlSerializer serializer = new XmlSerializer(typeof(SensorDataDTO));

                SensorDataDTO sensorDataDTO = (SensorDataDTO)serializer.Deserialize(reader);
                sensorDataDTO.TimeStampPolled = timeStampPolled;

                return sensorDataDTO;
            }
        }
    }
}
