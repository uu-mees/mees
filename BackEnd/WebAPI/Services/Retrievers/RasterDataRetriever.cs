/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using Microsoft.Extensions.Logging;
using Services.Implementations;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Net;
using System.Linq;
using DataAccess;
using Utils;
using Domain.Administration;

namespace Services.Retrievers {
    /// <summary>
    /// A retriever service for raster data.
    /// </summary>
    /* Retrievers are dependent on third party data which might change or be unavailable at certain times,
     * this makes testing these consistently impossible. It receives data and passes it on, which is trivial
     * as long as the end points and their format stay the same. If this changes there will need to be alterations
     * anyway, unit testing it will not help. These are covered by integration tests.
     */
    [ExcludeFromCodeCoverage]
    public class RasterDataRetriever : IRetriever<Raster> {
        private ConfigurationService configurationService;
        private ILogger<RasterDataInjectionService> logger;
        private bool active = true;
        /// <summary>
        /// The constructor for the retriever service.
        /// </summary>
        /// <param name="configurationService">
        /// An instance of <see cref="ConfigurationService"/>, 
        /// containing the location from where the data should be retrieved.
        /// </param>
        public RasterDataRetriever(ConfigurationService configurationService, ILogger<RasterDataInjectionService> logger) {
            this.configurationService = configurationService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieve raster data from the remote location.
        /// </summary>
        /// <returns>The retrieved, correctly formatted <see cref="Raster"/>.</returns>
        public Raster RetrieveData() {
            // Retrieve Data
            try {
                using (DatabaseContext context = new DatabaseContext(configurationService.ConnectionString)) {
                    //Grabs the dataSource from the database. 
                    //If this is not possible it just skips the rest and continues with the current settings/defaults
                    //Type 1 == Sensor, Type 0 == Raster
                    DataSource dataSource = context.DataSources.FirstOrDefault(x => x.Type == 0);
                    if (dataSource != null) {
                        string newDataSource = dataSource.URL;
                        active = dataSource.Active;
                        if (newDataSource != configurationService.RasterURL)
                            configurationService.RasterURL = newDataSource;
                    }
                }
            }
            catch {
                Console.WriteLine("Couldn't retrieve the Raster DataSource");
            }
            string url = configurationService.RasterURL;

            //Only retrieve new data if retrieval should be active
            if (active) {
                Raster raster = null;

                try {
                    // Request the asc raster file from the given server.
                    WebRequest webRequest = WebRequest.Create(url);
                    WebResponse webResponse = webRequest.GetResponse();

                    // Parse the response.
                    raster = ParseRaster(webResponse.GetResponseStream());
                    webResponse.Close();
                }
                catch (Exception e) {
                    logger.LogCritical(e.StackTrace);
                }
                return raster;
            }
            return null;
        }

        /// <summary>
        /// Parse .asc file into <see cref="Raster"/> object.
        /// </summary>
        /// <param name="rasterStream">Stream containing asc file.</param>
        /// <returns>The parsed <see cref="Raster"/>.</returns>
        public Raster ParseRaster(Stream rasterStream) {
            string plainText = new StreamReader(rasterStream).ReadToEnd();

            string[] data = plainText.Split('\n');

            Raster raster = new Raster();
            raster.HashCode = plainText.GetHashCode();
            raster.TimeStampPolled = DateTime.UtcNow;

            // Parse metadata.
            raster.CellSize = float.Parse(data[4].Split('\t')[1], CultureInfo.InvariantCulture);
            raster.NoDataValue = float.Parse(data[5].Split('\t')[1], CultureInfo.InvariantCulture);

            raster.Width = int.Parse(data[0].Split('\t')[1]);
            raster.Height = int.Parse(data[1].Split('\t')[1]);

            float xllcenter = float.Parse(data[2].Split('\t')[1], CultureInfo.InvariantCulture);
            float yllcenter = float.Parse(data[3].Split('\t')[1], CultureInfo.InvariantCulture);

            List<RasterCell> rasterCells = new List<RasterCell>();

            int to1DIndex(int x, int y) {
                return y * raster.Width + x;
            }

            // We loop y, x rather than x, y because all of the x values are on the same line.
            // By doing this we only have to split once.
            for (int y = 0; y < raster.Height; y++) {
                // y + 6 because the first 5 rows contain metadata
                string[] line = data[y + 6].Split(null as char[], StringSplitOptions.RemoveEmptyEntries);
                for (int x = 0; x < raster.Width; x++) {
                    GeoCoordinate southWest = new GeoCoordinate();
                    GeoCoordinate northEast = new GeoCoordinate();
                    // The first raster cell of the first row
                    if (x == 0 && y == 0) {
                        // Calculate the lat/long of the south west corner and north east corner
                        southWest = RijksdriehoekComponent.ConvertToLatLong(xllcenter + raster.CellSize * x,
                                                                            yllcenter + raster.CellSize * (raster.Height - y));
                        northEast = RijksdriehoekComponent.ConvertToLatLong(xllcenter + raster.CellSize * x + raster.CellSize,
                                                                            yllcenter + raster.CellSize * (raster.Height - y) + raster.CellSize);
                    }
                    // The first raster cell of a line
                    else if (x == 0) {
                        RasterCell cellAbove = rasterCells[to1DIndex(x, y - 1)];
                        // Since this is the first cell of a line we need to make sure that there is no empty space
                        // between this line and the one above it. 
                        southWest = RijksdriehoekComponent.ConvertToLatLong(xllcenter + raster.CellSize * x,
                                                                            yllcenter + raster.CellSize * (raster.Height - y));
                        // The southWest longitude has to match the longitude of the southWest point of the previous line (same X).
                        southWest.Longitude = cellAbove.SouthWestLongitude;

                        // The northEast longitude has to match the longitude of the northEast point of the previous line (same X).
                        northEast.Longitude = cellAbove.NorthEastLongitude;
                        // The northEast latitude has to match the latitude of the southWest point of the previous line (same Y).
                        northEast.Latitude = cellAbove.SouthWestLatitude;
                    }
                    // Any other raster cell on the first row
                    else if (y == 0) {
                        RasterCell cellLeft = rasterCells[to1DIndex(x - 1, y)];
                        // Calculate the lat/long of the south west corner and north east corner
                        northEast = RijksdriehoekComponent.ConvertToLatLong(xllcenter + raster.CellSize * x + raster.CellSize,
                                                                            yllcenter + raster.CellSize * (raster.Height - y) + raster.CellSize);
                        // The northEast latitude has to match the latitude of the northEast point of the previous cell (same Y).
                        northEast.Latitude = cellLeft.NorthEastLatitude;

                        // The southWest latitude has to match the latitude of the southWest point of the previous cell (same Y).
                        southWest.Latitude = cellLeft.SouthWestLatitude;
                        // The southWest longitude has to match the longitude of the northEast point of the previous cell (same X).
                        southWest.Longitude = cellLeft.NorthEastLongitude;
                    }
                    // Any other raster cell
                    else {
                        RasterCell cellLeft = rasterCells[to1DIndex(x - 1, y)];
                        RasterCell cellAbove = rasterCells[to1DIndex(x, y - 1)];
                        // The northEast longitude has to match the longitude of the northEast point of the previous line (same X).
                        northEast.Longitude = cellAbove.NorthEastLongitude;
                        // The northEast latitude has to match the latitude of the northEast point of the previous cell (same Y).
                        northEast.Latitude = cellLeft.NorthEastLatitude;

                        // The southWest latitude has to match the latitude of the southWest point of the previous cell (same Y).
                        southWest.Latitude = cellLeft.SouthWestLatitude;
                        // The southWest longitude has to match the longitude of the northEast point of the previous cell (same X).
                        southWest.Longitude = cellLeft.NorthEastLongitude;
                    }

                    float value = float.Parse(line[x], CultureInfo.InvariantCulture);

                    // Add the parsed RasterCell to the Raster.
                    rasterCells.Add(new RasterCell() {
                        SouthWestLatitude = southWest.Latitude,
                        SouthWestLongitude = southWest.Longitude,
                        NorthEastLatitude = northEast.Latitude,
                        NorthEastLongitude = northEast.Longitude,
                        Value = value,
                        CellIndex = y * raster.Width + x
                    });
                }
            }

            // Set the northEast and southWest values of the raster (the boundary).
            RasterCell northEastCell = rasterCells[raster.Width - 1];
            RasterCell southWestCell = rasterCells[rasterCells.Count - raster.Width];
            raster.SouthWestLat = southWestCell.SouthWestLatitude;
            raster.SouthWestLng = southWestCell.SouthWestLongitude;
            raster.NorthEastLat = northEastCell.NorthEastLatitude;
            raster.NorthEastLng = northEastCell.NorthEastLongitude;

            raster.Cells = rasterCells;

            return raster;
        }
    }
}
