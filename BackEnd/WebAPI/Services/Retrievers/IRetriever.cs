/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
namespace Services.Retrievers {
    /// <summary>
    /// The general interface for all retriever services.
    /// </summary>
    /// <typeparam name="T">The type to be returned by the retrieve function.</typeparam>
    public interface IRetriever<T> {
        /// <summary>
        /// A function which retrieves data from the relevant location.
        /// </summary>
        /// <returns>An object of type T.</returns>
        T RetrieveData();
    }
}
