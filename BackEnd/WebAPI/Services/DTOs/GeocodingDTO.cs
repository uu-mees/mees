/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;

namespace Services.DTOs.Routing {

    /// <summary>
    /// A wrapper DTO for multiple <see cref="GeocodingResultDTO"/>s.
    /// </summary>
    public class GeocodingDTO {
        /// <summary>The error message (if applicable) of the request.</summary>
        public string ErrorMessage;

        /// <summary>A list of formatted addresses.</summary>
        public List<GeocodingResultDTO> Results;

        /// <summary>The status code of the request.</summary>
        public string Status;
    }

    /// <summary>
    /// A DTO for formatted addresses.
    /// </summary>
    /// <seealso cref="GeocodingResultDTO"/>
    public class GeocodingResultDTO {
        /// <summary>The formatted address for the requested location.</summary>
        public string Formatted_Address;

        /// <summary>The geometry related to the address.</summary>
        public Geometry Geometry;

        /// <summary>
        /// A boolean indicating if there was complete or partial match.
        /// </summary>
        public bool Partial_Match;
    }

    /// <summary>
    /// A class describing a location and its bounds.
    /// </summary>
    public class Geometry {
        /// <summary>A GPS location.</summary>
        public Location Location;

        /// <summary>The bounds related to the location.</summary>
        public Bounds Bounds;

        /// <summary>The bounds for centering the camera on the location.</summary>
        public Bounds Viewport;
    }

    /// <summary>
    /// A class describing a GPS bounding box.
    /// </summary>
    public class Bounds {
        /// <summary>The location of the north-east corner.</summary>
        public Location Northeast;

        /// <summary>The location of the south-west corner.</summary>
        public Location Southwest;
    }

    /// <summary>
    /// A class describing a GPS location.
    /// </summary>
    public class Location {
        /// <summary>The latitude of the location.</summary>
        public double Lat;

        /// <summary>The longitude of the location.</summary>
        public double Lng;
    }
}
