/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;

namespace Services.DTOs {

    /// <summary>
    /// A wrapper DTO for multiple <see cref="RouteResponseDTO"/>s.
    /// </summary>
    public class RoutingResponseDTO {
        /// <summary>
        /// The GPS location of the starting point in GeoJSON format
        /// </summary>
        public double[] Start { get; set; }

        /// <summary>
        /// The GPS location of the destination point in GeoJSON format
        /// </summary>
        public double[] Destination { get; set; }

        /// <summary>
        /// A string defining the vehicle for which the route should be planned.
        /// </summary>
        public string Vehicle { get; set; }

        /// <summary>
        /// A string defining the weighting method for the calculation of the route.
        /// </summary>
        public string Weighting { get; set; }

        /// <summary>The amount of routes determined.</summary>
        public int Count { get; set; }

        /// <summary>
        /// A list of <see cref="RouteResponseDTO"/>s, representing all routes found.
        /// </summary>
        public List<RouteResponseDTO> Routes { get; set; }

        /// <summary>
        /// A list of errors (if there are errors) that were encountered by the routingAPI.
        /// </summary>
        public List<string> Errors { get; set; }
    }

    /// <summary>
    /// A DTO for routes.
    /// </summary>
    /// <seealso cref="RoutingResponseDTO"/>
    public class RouteResponseDTO {
        /// <summary>The total distance in meters.</summary>
        public double Distance { get; set; }

        /// <summary>The duration in miliseconds.</summary>
        public long Duration { get; set; }

        /// <summary>
        /// A list of <see cref="GHPointDTO"/>s describing the route.
        /// </summary>
        public List<GHPointDTO> Path { get; set; }
    }

    /// <summary>
    /// A DTO for points on a route.
    /// </summary>
    public class GHPointDTO {
        /// <summary>
        /// The GPS location of the point in GeoJSON format.
        /// </summary>
        public double[] Point { get; set; }

        /// <summary>
        /// The total expected time elapsed from traveling along the path until this point.
        /// </summary>
        public long ElapsedTime { get; set; }
    }

}
