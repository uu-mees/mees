/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
namespace Services.DTOs {
    public class UserLocationDataDTO {

        public double longitude { get; set; }
        public double latitude { get; set; }
        public double timestamp { get; set; }
        public string userID { get; set; }
    }

}
