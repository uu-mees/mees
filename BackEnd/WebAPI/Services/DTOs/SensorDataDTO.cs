/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Xml.Serialization;

namespace Services.DTOs {
    /// <summary>
    /// A DTO wrapper for <see cref="SensorDTO"/>s.
    /// </summary>
    [Serializable()]
    [XmlRoot(ElementName = "root")]
    public class SensorDataDTO {
        /// <summary>An array of <see cref="SensorDTO"/>s.</summary>
        [XmlArray(ElementName = "message")]
        [XmlArrayItem(ElementName = "sensor")]
        public SensorDTO[] Sensors { get; set; }

        /// <summary>
        /// The <see cref="DateTime"/> when the values for the sensor have been polled.
        /// </summary>
        public DateTime TimeStampPolled { get; set; }
    }

    /// <summary>
    /// A DTO for sensors and wrapper for <see cref="MeasurementDTO"/>s.
    /// </summary>
    [Serializable()]
    public class SensorDTO {
        /// <summary>The ID of the sensor.</summary>
        [XmlElement("id")]
        public string Id { get; set; }

        /// <summary>The label of the sensor.</summary>
        [XmlElement("label")]
        public string Label { get; set; }

        /// <summary>
        /// The latitude of the GPS location for the sensor.
        /// </summary>
        [XmlElement("latitude")]
        public float Latitude { get; set; }

        /// <summary>
        /// The longitude of the GPS location for the sensor.
        /// </summary>
        [XmlElement("longitude")]
        public float Longitude { get; set; }

        /// <summary>The <see cref="DateTime"/> when the measurement has started.</summary>
        [XmlElement("timestamp_from")]
        public DateTime TimeStamp_From { get; set; }

        /// <summary>The <see cref="DateTime"/> when the measurement has ended.</summary>
        [XmlElement("timestamp_to")]
        public DateTime TimeStamp_To { get; set; }

        /// <summary>
        /// An array of <see cref="MeasurementDTO"/>s, 
        /// representing all measurements taken by the sensor.
        /// </summary>
        [XmlArray(ElementName = "measurements")]
        [XmlArrayItem(ElementName = "measurement")]
        public MeasurementDTO[] Measurements;
    }

    /// <summary>
    /// A DTO for measurements.
    /// </summary>
    [Serializable()]
    public class MeasurementDTO {
        /// <summary>
        /// The component for which the measurement was taken.
        /// </summary>
        [XmlElement("component")]
        public string Component { get; set; }

        /// <summary>The value of the measurement taken.</summary>
        [XmlElement("value")]
        public string Value { get; set; }

        /// <summary>The unit in which the value is measured.</summary>
        [XmlElement("unit")]
        public string Unit { get; set; }
    }

}
