/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.DTOs {
    public class RasterDTO {
        /// <summary>
        /// A png of the raster with every cell being a pixel, encoded as a base-64 string.
        /// </summary>
        public string Image;

        /// <summary>
        /// The datetime of when this raster was polled from the RIVM servers.
        /// </summary>

        public DateTime TimeStampPolled;

        /// <summary>
        /// Latitude of the north east corner of the image.
        /// </summary>
        public double NorthEastLatitude;

        /// <summary>
        /// Longitude of the north east corner of the image.
        /// </summary>
        public double NorthEastLongitude;

        /// <summary>
        /// Latitude of the south west corner of the image.
        /// </summary>
        public double SouthWestLatitude;

        /// <summary>
        /// Longitude of the south west corner of the image.
        /// </summary>
        public double SouthWestLongitude;
    }
}
