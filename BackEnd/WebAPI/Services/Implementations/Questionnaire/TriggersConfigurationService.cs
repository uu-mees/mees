/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Domain.Questionnaire;
using Newtonsoft.Json;

namespace Services.Implementations {
    public class TriggersConfigurationService {

        private string jsonFile;
        public TriggersConfiguration TriggersConfiguration;

        public TriggersConfigurationService(string jsonFile) {
            this.jsonFile = jsonFile;
            StreamReader reader = new StreamReader(this.jsonFile);
            TriggersConfiguration = JsonConvert.DeserializeObject<TriggersConfiguration>(reader.ReadToEnd());
        }
    }
}
