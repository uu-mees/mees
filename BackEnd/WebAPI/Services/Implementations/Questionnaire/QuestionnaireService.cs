/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Questionnaire;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Domain.Tracking;
using Domain.Exposure;
using System.Device.Location;
using System.Diagnostics.CodeAnalysis;
using Utils;
using System.Net.Http;

namespace Services.Implementations {
    public class QuestionnaireService : IQuestionnaireService {
        private DatabaseContext context;

        private QuestionnaireConfigurationService configurationService;
        private ITriggerService triggerService;

        /// <summary>
        /// QuestionnaireService constructor.
        /// </summary>
        public QuestionnaireService(DatabaseContext context, QuestionnaireConfigurationService questionnaireConfiguration, ITriggerService triggerService) {
            this.context = context;
            configurationService = questionnaireConfiguration;
            this.triggerService = triggerService;
        }

        /// <summary>
        /// Check if a user can receive a questionnaire and if so, return it.
        /// </summary>
        /// <param name="userId">The userId from the user to check for.</param>
        public Questionnaire CheckForQuestionnaire(Guid userId) {
            User user;
            lock (context) {
                user = context.Users.FirstOrDefault(x => x.Id == userId);
            }
            if (user == null)
                return null;

            if (user.LastQuestionnaireTime + new TimeSpan(0, 0, configurationService.QuestionnaireConfiguration.CooldownSeconds) >= DateTime.UtcNow) {
                return null;
            }

            List<Trigger> triggers = user.Triggers.Where(t => t.TimeStamp == null).ToList();

            Trigger trigger = GetTrigger(triggers, userId);

            string questionnaireStr = GetQuestionnaire(trigger);

            Questionnaire questionnaire = null;

            if (questionnaireStr != null) {
                trigger.TimeStamp = DateTime.UtcNow;
                // There is a questionnaire which we will send to the user, save the questionnaire object so it
                // can be used for research purposes.
                questionnaire = new Questionnaire() {
                    QuestionnaireString = questionnaireStr,
                    Trigger = trigger
                };
                lock (context) {
                    context.Questionnaires.Add(questionnaire);
                    user.LastQuestionnaireTime = DateTime.UtcNow;
                    context.Triggers.Add(trigger);
                    context.SaveChanges();
                }
            }

            return questionnaire;
        }

        /// <summary>
        /// Get the questionnaire corresponding to the type of the Trigger.
        /// </summary>
        /// <param name="trigger">The Trigger to get a questionnaire for.</param>
        /// <returns></returns>
        private string GetQuestionnaire(Trigger trigger) {
            TriggerConfigurations tConfig = configurationService.QuestionnaireConfiguration.TriggerConfigurations;
            if (trigger == null) {
                return null;
            }
            else if (trigger is DefaultTrigger) {
                return tConfig.DefaultConfiguration.TriggerConfiguration.Questionnaire;
            }
            else if (trigger is DistanceTrigger) {
                return tConfig.DistanceConfiguration.TriggerConfiguration.Questionnaire;
            }
            else if (trigger is ExposureTrigger) {
                return tConfig.ExposureConfiguration.TriggerConfiguration.Questionnaire;
            }
            else {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Checks which trigger we should return, based on the locations + exposures of the user of the last hour
        /// </summary>
        /// <param name="triggers"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private Trigger GetTrigger(List<Trigger> triggers, Guid userId) {
            Trigger trigger = null;
            UserLocationExposure[] userLocationExposures = GetLocationExposures(userId);

            foreach (Trigger t in triggers) {
                if (t is ExposureTrigger && triggerService.ExposureTrigger(userLocationExposures)) {
                    trigger = t;
                }
                else if (t is DistanceTrigger && triggerService.DistanceTrigger(userLocationExposures)) {
                    trigger = t;
                }
            }
            if (trigger == null && triggers.Any(t => t is DefaultTrigger)) {
                trigger = triggers.FirstOrDefault(t => t is DefaultTrigger);
            }

            return trigger;
        }

        /// <summary>
        /// Retrieves the users locations + exposures of the last hour
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserLocationExposure[] GetLocationExposures(Guid userId) {
            DateTime hourAgo = DateTime.UtcNow.Subtract(new TimeSpan(1, 0, 0));
            lock (context) {
                UserLocationExposure[] array = context.UserLocationExposures.Where(u => u.UserID == userId && u.Timestamp >= hourAgo).ToArray();
                return array;
            }
        }

        /// <summary>
        /// Get a list of all the Questionnaire objects from the database.
        /// </summary>
        public List<Questionnaire> GetAllQuestionnaires() {
            lock (context) {
                List<Questionnaire> questionnaires = context.Questionnaires.ToList();
                return questionnaires;
            }
        }
    }
}
