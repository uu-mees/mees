/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using Domain.Tracking;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations {
    public class QuestionnaireConfigurationService {
        public QuestionnaireConfiguration QuestionnaireConfiguration { get; set; }
        private string jsonFile;

        public QuestionnaireConfigurationService(string jsonFile) {
            this.jsonFile = jsonFile;
            StreamReader reader = new StreamReader(jsonFile);
            QuestionnaireConfiguration = JsonConvert.DeserializeObject<QuestionnaireConfiguration>(reader.ReadToEnd());
        }

        /// <summary>
        /// Get the questionnaire configuration in JSON format.
        /// </summary>
        /// <returns>string</returns>
        public string GetConfigurationJSON() {
            return JsonConvert.SerializeObject(QuestionnaireConfiguration);
        }

        /// <summary>
        /// Replaces the questionnaire configuration with the given value.
        /// </summary>
        /// <param name="newConfig">The new questionnaire configuration.</param>
        public void UpdateConfiguration(QuestionnaireConfiguration newConfig) {
            if (newConfig == null)
                return;
            QuestionnaireConfiguration = newConfig;
            File.WriteAllText(jsonFile, JsonConvert.SerializeObject(newConfig));
        }
    }
}
