/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Exposure;
using Domain.Questionnaire;
using Domain.Tracking;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;
using Newtonsoft.Json;
using System.Net.Http;

namespace Services.Implementations {
    public class TriggerService : ITriggerService {
        private DatabaseContext context;
        private IExposureService exposureService;
        private QuestionnaireConfigurationService questionnaireConfigurationService;
        private TriggersConfigurationService triggersConfigurationService;

        public TriggerService(DatabaseContext context, IExposureService exposureService, QuestionnaireConfigurationService questionnaireConfigurationService, TriggersConfigurationService triggersConfigurationService) {
            this.context = context;
            this.exposureService = exposureService;
            this.questionnaireConfigurationService = questionnaireConfigurationService;
            this.triggersConfigurationService = triggersConfigurationService;
        }

        /// <summary>
        /// Check if the average exposure of the last hour is > 40 ug/m3.
        /// </summary>
        /// <returns></returns>
        public bool ExposureTrigger(UserLocationExposure[] exposures) {
            DateTime now = DateTime.UtcNow;
            List<UserLocationExposure> list = exposures.ToList();
            list.Sort(new Comparison<UserLocationExposure>((x, y) => DateTime.Compare(x.Timestamp, y.Timestamp)));
            list.Reverse();
            int count = 0;
            double? totalExposure = 0;
            foreach (UserLocationExposure exposure in list) {
                if (exposure.Timestamp < now.Subtract(new TimeSpan(1, 0, 0))) {     // Filter out the locations that are not from the last hour
                    break;
                }
                else {
                    if (exposure.Exposure == null) {
                        continue;
                    }
                    count++;
                    totalExposure += exposure.Exposure;
                }
            }
            if (totalExposure / count >= triggersConfigurationService.TriggersConfiguration.ExposureTriggerThreshold) {      // Calculate the average exposure
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Check if user has travelled in the last hour.
        /// </summary>
        /// <returns></returns>
        public bool DistanceTrigger(UserLocationExposure[] locations) {
            List<GeoCoordinate> lastHourLocations = new List<GeoCoordinate>();
            DateTime now = DateTime.UtcNow;
            List<UserLocationExposure> list = locations.ToList();
            list.Sort(new Comparison<UserLocationExposure>((x, y) => DateTime.Compare(x.Timestamp, y.Timestamp)));
            list.Reverse();
            foreach (UserLocationExposure location in list) {                       // Filter out the locations that are not from the last hour
                if (location.Timestamp < now.Subtract(new TimeSpan(1, 0, 0))) {
                    break;
                }
                else {
                    lastHourLocations.Add(new GeoCoordinate(location.Position.Latitude, location.Position.Longitude));
                }
            }
            bool travelledFarEnough = false;
            int count = 0;
            double travelled = 0;
            for (int i = 0; i < lastHourLocations.Count - 1; i++) {
                if (lastHourLocations[i].GetDistanceTo(lastHourLocations[i + 1]) < triggersConfigurationService.TriggersConfiguration.DistanceTriggerLowerThreshold) { // Filter out a location if the distance between the location and the succesive location is less than the set threshold
                    continue;
                }
                else {
                    count++;
                    travelled += lastHourLocations[i].GetDistanceTo(lastHourLocations[i + 1]);
                }
            }
            if (travelled / count >= triggersConfigurationService.TriggersConfiguration.DistanceTriggerThreshold) {      // Calculate the average distance travelled
                travelledFarEnough = true;
            }
            return travelledFarEnough;
        }

        /// <summary>
        /// Checks whether atleast half of the locations received, lays within the USP.
        /// </summary>
        /// <param name="locations"></param>
        /// <returns></returns>
        [ExcludeFromCodeCoverage]
        public bool LocationInRaster(UserLocationExposure[] locations) {
            int count = 0;
            Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> tuple;
            lock (context) {
                tuple = exposureService.GetBounds(context.Rasters.First());
            }
            foreach (UserLocationExposure location in locations) {
                if (GeoUtils.WithinArea(new GeoCoordinate(location.Position.Latitude, location.Position.Longitude), tuple.Item3, tuple.Item1)) {
                    count++;
                }
            }
            bool yesOrNo = false;
            if (count >= locations.Length / 2) {
                yesOrNo = true;
            }
            return yesOrNo;
        }

        /// <summary>
        /// Decide which notification to send to the user. Only 1 per timeslot: (first) 7:00-11:00, (second) 11:00-14:00, (th) 14:00-20:00
        /// </summary>
        /// <param name="userId"></param>
        [ExcludeFromCodeCoverage]
        public async void SendNotificationPostRequest(Guid userId, string connectionString) {
            using (DatabaseContext context = new DatabaseContext(connectionString)) {
                DateTime now = DateTime.UtcNow;
                string timeSlotNow = getTimeslot(now);
                DateTime? questionnaireTime = context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime;
                string timeSlotLastQuestionnaire = getTimeslot(questionnaireTime);
                DateTime? minigameTime = context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime;            //Time of the last minigame notification
                string timeSlotLastMiniGame = getTimeslot(minigameTime);
                bool result = false;
                if (timeSlotNow != "none") {
                    if (timeSlotLastQuestionnaire == "null") {      // The user has never received a questionnaire notification
                        result = await SendPushNotification(userId);
                        context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime = DateTime.UtcNow;
                    }
                    else if (timeSlotLastMiniGame == "null") {    // The user has never received a miniGame notification AND the last questionnaire is not send in the current time slot
                        if (questionnaireTime.Value.Date == now.Date && timeSlotLastQuestionnaire != timeSlotNow || questionnaireTime.Value.Date < now.Date) {
                            result = await SendPushNotification(userId);
                            context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime = DateTime.UtcNow;
                        }
                        else {
                            return;
                        }
                    }
                    else if (questionnaireTime.Value.Date < now.Date || minigameTime.Value.Date < now.Date) {     // The date of the last questionnaire and/or the date of the last miniGame are before the current date
                        if ((questionnaireTime.Value.Date < now.Date && minigameTime.Value.Date < now.Date && questionnaireTime < minigameTime) || (questionnaireTime.Value.Date < now.Date && minigameTime.Value.Date == now.Date && timeSlotLastMiniGame != timeSlotNow)) {          // The date of the last questionnaire and the date of the last miniGame are before the current date and the date + time of the last questionnaire < the date + time of the last miniGame OR the date of the last questionnaire is before the current date and the date of the last miniGame is the current date
                            result = await SendPushNotification(userId);
                            context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime = DateTime.UtcNow;
                        }
                        else if ((questionnaireTime.Value.Date < now.Date && minigameTime.Value.Date < now.Date && minigameTime < questionnaireTime) || (questionnaireTime.Value.Date == now.Date && minigameTime.Value.Date < now.Date && timeSlotLastQuestionnaire != timeSlotNow)) {   // The date of the last questionnaire and the date of the last miniGame are before the current date and the date + time of the last miniGame < the date + time of the last questionnaire OR the date of the last miniGame is before the current date and the date of the last questionnaire is the current date
                            result = await SendPushNotification(userId);
                            context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime = DateTime.UtcNow;
                        }
                        else {
                            return;
                        }
                    }
                    else if (timeSlotNow != timeSlotLastMiniGame && timeSlotNow != timeSlotLastQuestionnaire) {  // The time slot of the current time is either in the first, second or third AND the time slot of the current time is not the same as either the time slot of the last questionnaire or the time slot of the last miniGame
                        if (questionnaireTime < minigameTime) {         // The date + time of the last questionnaire < the date + time of the last miniGame
                            result = await SendPushNotification(userId);
                            context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime = DateTime.UtcNow;
                        }
                        else if (minigameTime < questionnaireTime) {  // The date + time of the last miniGame < the date + time of the last questionnaire
                            result = await SendPushNotification(userId);
                            context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime = DateTime.UtcNow;
                        }
                        else {
                            return;
                        }
                    }
                    if (result) {
                        context.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Send notification for questionnaire or minigame to user.
        /// </summary>
        /// <param name="userId"></param>
        [ExcludeFromCodeCoverage]
        public async Task<bool> SendPushNotification(Guid userId) {
            List<FireBase> fireBases = context.Users.FirstOrDefault(u => u.Id == userId).FireBases;
            string[] fireBaseTokens = new string[fireBases.Count];
            for (int i = 0; i < fireBases.Count; i++) {     // Gather the Firebase ID from the database
                fireBaseTokens[i] = fireBases[i].FireBaseID;
            }

            if (fireBaseTokens.Length == 0) {
                return false;
            }

            Message message = null;

            if (!(context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime > context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime)) {    // Check if it should send a questionnaire notification
                List<Trigger> triggers = context.Triggers.Where(t => t.UserId == userId).ToList();
                triggers.Reverse();
                string text;
                if (triggers.FirstOrDefault().TimeStamp >= DateTime.UtcNow - new TimeSpan(0, 0, questionnaireConfigurationService.QuestionnaireConfiguration.CooldownSeconds)) {        // Check if the last time a user filled in a questionnaire with the cooldown period added to it, is greater than now. If so, say that the user needs to wait till the cooldown period is over.
                    text = triggersConfigurationService.TriggersConfiguration.QuestionnaireNotificationWaitTextPart1 + triggers.FirstOrDefault().TimeStamp + new TimeSpan(0, 0, questionnaireConfigurationService.QuestionnaireConfiguration.CooldownSeconds) + triggersConfigurationService.TriggersConfiguration.QuestionnaireNotificationWaitTextPart2;
                }
                else {
                    text = triggersConfigurationService.TriggersConfiguration.QuestionnaireNotificationText;
                }
                message = new Message() {   // Create Message for questionnaire notification
                    notification = new Notification() {
                        title = triggersConfigurationService.TriggersConfiguration.QuestionnaireNotificationTitle,
                        text = text
                    },
                    data = new Data() { url = triggersConfigurationService.TriggersConfiguration.QuestionnaireNotificationUrl },                              // link to questionnaire
                    registration_ids = fireBaseTokens
                };
            } else if (context.Users.FirstOrDefault(u => u.Id == userId).LastMiniGameTime < context.Users.FirstOrDefault(u => u.Id == userId).LastQuestionnaireTime) {     // Check if it should send a minigame notification
                message = new Message() {   // Create Message for minigame notification
                    notification = new Notification() {
                        title = triggersConfigurationService.TriggersConfiguration.MinigameNotificationTitle,
                        text = triggersConfigurationService.TriggersConfiguration.MinigameNotificationText
                    },
                    data = new Data() { url = triggersConfigurationService.TriggersConfiguration.MinigameNotificationUrl },                                  // link to minigame
                    registration_ids = fireBaseTokens
                };
            }

            string jsonMessage = JsonConvert.SerializeObject(message);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, triggersConfigurationService.TriggersConfiguration.FirebaseAddress);
            request.Headers.TryAddWithoutValidation("Authorization", "key =" + triggersConfigurationService.TriggersConfiguration.FirebaseKey);
            request.Content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

            HttpResponseMessage result;

            using (HttpClient client = new HttpClient()) {

                result = await client.SendAsync(request);
                return true;
            }
        }

        /// <summary>
        /// Receives a DateTime and checks in which time interval this DateTime is contained and returns this interval in a string.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public string getTimeslot(DateTime? dateTime) {
            if (dateTime == null) {
                return "null";
            }
            DateTime seven = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, triggersConfigurationService.TriggersConfiguration.BeginTimeSlot1, 0, 0);
            DateTime eleven = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, triggersConfigurationService.TriggersConfiguration.BeginTimeSlot2, 0, 0);
            DateTime fourteen = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, triggersConfigurationService.TriggersConfiguration.BeginTimeSlot3, 0, 0);
            DateTime twenty = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, triggersConfigurationService.TriggersConfiguration.EndTimeSlot3, 0, 0);
            string result = "";
            if (seven <= dateTime && dateTime < eleven) {
                result = "first";
            }
            else if (eleven <= dateTime && dateTime < fourteen) {
                result = "second";
            }
            else if (fourteen <= dateTime && dateTime < twenty) {
                result = "third";
            }
            else {
                result = "none";
            }
            return result;
        }
    }

    /// <summary>
    /// These three classes are used to help with sending the notifications.
    /// </summary>

    public class Message {
        public string[] registration_ids { get; set; }
        public Notification notification { get; set; }
        public object data { get; set; }
    }

    public class Notification {
        public string title { get; set; }
        public string text { get; set; }
    }

    public class Data {
        public string url { get; set; }
    }
}
