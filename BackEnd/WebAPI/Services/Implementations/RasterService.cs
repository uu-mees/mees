/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Services.Interfaces;
using System;
using System.Device.Location;
using System.Linq;
using System.Threading;
using Utils;
using Domain.Exposure;
using Microsoft.Extensions.Logging;
using Services.DTOs;
using System.Collections.Generic;
using System.Data.Entity;
using Services.Interfaces.Pollers;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IRasterService"/>.
    /// </summary>
    public class RasterService : IRasterService {
        private DatabaseContext context;
        private ILogger<RasterService> logger;

        public IRasterContainer rasterContainer { get; set; }

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        public RasterService(DatabaseContext context, ILogger<RasterService> logger, IRasterContainer rasterContainer) {
            this.context = context;
            this.logger = logger;
            this.rasterContainer = rasterContainer;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Raster Service Thread";
        }

        public RasterDTO GetRasterImage() {
            return rasterContainer.GetRasterImage();
        }

        /// <summary>
        /// Get the rasterCell from the given raster at the specified location.
        /// </summary>
        /// <param name="rasterId">The id from the raster to check for.</param>
        /// <param name="geoCoordinate">The location.</param>
        public RasterCell GetRasterCell(int rasterId, GeoCoordinate geoCoordinate) {
            // Use the latest raster to check the coordinate before querying for the actual value
            // This increases performance immensely especially when the coordinate is outside of all rasters.
            if (GetLatestRaster() == null)
                return null;

            RasterCell cell = GetLatestRaster().Cells.Where(x => x.SouthWestLatitude <= geoCoordinate.Latitude &&
                                                    x.SouthWestLongitude <= geoCoordinate.Longitude &&
                                                    x.NorthEastLatitude >= geoCoordinate.Latitude &&
                                                    x.NorthEastLongitude >= geoCoordinate.Longitude).FirstOrDefault();
            if (cell == null)
                return null;
            if (rasterId == cell.RasterId)
                return cell;
            return GetRasterCell(cell.CellIndex, rasterId);
        }

        /// <summary>
        /// Get the rasterCell with the given cellIndex from the specified raster.
        /// </summary>
        /// <param name="cellIndex">The index of the cell.</param>
        /// <param name="rasterId">The id of the raster.</param>
        public RasterCell GetRasterCell(int cellIndex, int? rasterId) {
            if (rasterId == null)
                return null;
            if (rasterId == (GetLatestRaster()?.Id ?? null))
                return rasterContainer.GetRasterCells()?[cellIndex] ?? null;
            // Only efficient when paired with an Index on RasterId and CellIndex:
            // CREATE INDEX RasterId_CellIndex on RasterCells (RasterId, CellIndex)
            lock (context) {
                IQueryable<RasterCell> cells = from rc in context.RasterCells
                                               where rc.RasterId == rasterId && rc.CellIndex == cellIndex
                                               select rc;
                return cells.FirstOrDefault();
            }
        }

        /// <summary>
        /// Get the <see cref="DateTime"/> polled of the most recently polled raster.
        /// </summary>
        /// <returns>The corresponding <see cref="DateTime"/>.</returns>
        public DateTime GetLatestRasterTimeStamp() {
            Raster raster = GetLatestRaster();
            if (raster == null)
                return DateTime.MinValue;

            return raster.TimeStampPolled;
        }

        /// <summary>
        /// Get the latest <see cref="Raster"/>.
        /// </summary>
        /// <returns>The corresponding <see cref="Raster"/>.</returns>
        public Raster GetLatestRaster() {
            return rasterContainer.GetRaster();
        }

        /// <summary>
        /// Get the <see cref="Raster"/> which was polled before and closest to a point in time.
        /// </summary>
        /// <param name="time">Time of comparison.</param>
        /// <returns>The corresponding <see cref="Raster"/>.</returns>
        public Raster GetClosestHistoricalRaster(DateTime time) {
            DateTime startWindow = time - TimeSpan.FromMinutes(90);
            // Get the first raster before the time window
            lock (context) {
                IQueryable<Raster> rasters = from r in context.Rasters
                                             where r.TimeStampPolled <= time && r.TimeStampPolled >= startWindow
                                             orderby r.TimeStampPolled descending
                                             select r;
                return rasters.FirstOrDefault();
            }
        }

        /// <summary>
        /// Get a list of the first raster before the given timeframe + all the rasters within the range.
        /// </summary>
        /// <param name="timeFrom">The start of the range.</param>
        /// <param name="timeTo">The end of the range.</param>
        /// <returns></returns>
        public IQueryable<Raster> GetRelevantHistoricalRasters(DateTime timeFrom, DateTime timeTo) {
            Raster firstRaster = GetClosestHistoricalRaster(timeFrom);

            if (firstRaster != null) {
                timeFrom = firstRaster.TimeStampPolled;
            }

            lock (context) {
                return from r in context.Rasters
                       where r.TimeStampPolled >= timeFrom && r.TimeStampPolled <= timeTo
                       select r;
            }
        }

        /// <summary>
        /// Get the exposure value for a given position (from the latest raster)
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <returns>The exposure value.</returns>
        public double? GetExposureFromLocation(GeoCoordinate coordinate) {
            Raster raster = GetLatestRaster();

            if(raster == null)
                return null;

            if (raster.TimeStampPolled.AddHours(2) < DateTime.UtcNow)
                return null;

            RasterCell cell = GetRasterCell(raster.Id, coordinate);
            if (cell != null) {
                return cell.Value;
            }
            return null;
        }

        public bool SameDimensions(int id1, int id2) {
            if (id1 == id2)
                return true;

            lock (context) {
                List<Raster> rasters = (from r in context.Rasters
                                        where r.Id == id1 || r.Id == id2
                                        select r).ToList();

                if (rasters.Count == 2) {
                    return rasters[0].CellSize == rasters[1].CellSize
                        && rasters[0].Height == rasters[1].Height
                        && rasters[0].Width == rasters[1].Width
                        && rasters[0].NorthEastLat == rasters[1].NorthEastLat
                        && rasters[0].NorthEastLng == rasters[1].NorthEastLng;
                }

                return false;
            }
        }

        /// <summary>
        /// When all stored rasters are too old we send a fake empty raster instead. This causes the gray overlay 
        /// </summary>
        /// <returns></returns>
        public Raster CreateFakeRaster() {
            //Create a fake raster with no data
            Raster fakeRaster = new Raster();
            fakeRaster.NorthEastLat = 0;
            fakeRaster.NorthEastLng = 0;
            fakeRaster.SouthWestLat = 0;
            fakeRaster.SouthWestLng = 0;
            fakeRaster.Width = 1;
            fakeRaster.Height = 1;
            fakeRaster.Id = int.MaxValue;
            fakeRaster.TimeStampPolled = DateTime.UtcNow;
            fakeRaster.Cells = new List<RasterCell>();
            fakeRaster.CellSize = 0;
            fakeRaster.NoDataValue = -1;

            return fakeRaster;

        }
    }

    /// <summary>
    /// These functions are as a replacement to the DbFunctions used in LINQ-To-Entities. DbFunctions can
    /// not be ran while unit testing, while these functions can. During production however, Entity Framework automatically
    /// uses the correct DbFunctions instead of the functions defined below.
    /// </summary>
    public static class TestableDbFunctions {
        [System.Data.Entity.DbFunction("Edm", "DiffMinutes")]
        public static int? DiffMinutes(DateTime? dateValue1, DateTime? dateValue2) {
            if (!dateValue1.HasValue || !dateValue2.HasValue)
                return null;

            return (int)((dateValue2.Value - dateValue1.Value).TotalMinutes);
        }

        [System.Data.Entity.DbFunction("Edm", "DiffSeconds")]
        public static int? DiffSeconds(DateTime? dateValue1, DateTime? dateValue2) {
            if (!dateValue1.HasValue || !dateValue2.HasValue)
                return null;

            return (int)((dateValue2.Value - dateValue1.Value).TotalSeconds);
        }
    }
}
