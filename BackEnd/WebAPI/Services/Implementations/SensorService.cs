/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Exposure;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="ISensorService"/>.
    /// </summary>
    public class SensorService : ISensorService {
        private DatabaseContext context;
        private ILogger<SensorService> logger;
        private ConfigurationService configurationService;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="configurationService">
        /// A <see cref="ConfigurationService"/> instance specifying multiple configuration options.
        /// </param>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        public SensorService(ConfigurationService configurationService, DatabaseContext context,
            ILogger<SensorService> logger) {
            this.context = context;
            this.logger = logger;
            this.configurationService = configurationService;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Sensor Service Thread";
        }

        /// <summary>
        /// Get specific <see cref="Sensor"/> from database.
        /// </summary>
        /// <param name="sensorId">The ID of the Sensor to retrieve.</param>
        /// <returns>The <see cref="Sensor"/> corresponding to the ID.</returns>
        public Sensor GetSensor(string sensorId) {
            lock (context) {
                using (context) {
                    DateTime sensorTimeout = DateTime.UtcNow - TimeSpan.FromHours(configurationService.SensorTimeoutHours);

                    return context.Sensors.FirstOrDefault(x => x.Id == sensorId &&
                                                               // Filter out sensors which have been inactive for too long.
                                                               (x.LastValidBatch != null && x.LastValidBatch.TimeStampPolled >= sensorTimeout));
                }
            }
        }

        /// <summary>
        /// Get <see cref="Sensor"/>s within area.
        /// </summary>
        /// <param name="southWestPoint">The south west point of area.</param>
        /// <param name="northEastPoint">The north east point of area.</param>
        /// <returns>An ICollection containing all relevant sensors.</returns>
        public ICollection<Sensor> GetSensors(GeoCoordinate southWestPoint,
                                              GeoCoordinate northEastPoint) {
            DateTime sensorTimeout = DateTime.UtcNow - TimeSpan.FromHours(configurationService.SensorTimeoutHours);

            lock (context) {
                ICollection<Sensor> sensors = context.Sensors.Where(x =>
                                                            // Filter out sensors which have been inactive for too long.
                                                            (x.LastValidBatch != null && x.LastValidBatch.TimeStampPolled >= sensorTimeout) &&
                                                            // Only select sensors in the area
                                                            x.Latitude >= southWestPoint.Latitude &&
                                                            x.Latitude <= northEastPoint.Latitude &&
                                                            x.Longitude >= southWestPoint.Longitude &&
                                                            x.Longitude <= northEastPoint.Longitude)
                                                            .ToList();
                return sensors;
            }
        }

        /// <summary>
        /// Get latest <see cref="SensorMeasurementBatch"/> from a <see cref="Sensor"/>.
        /// </summary>
        /// <param name="sensor">The sensor to retrieve latest Batch from.</param>
        /// <returns>
        /// Null if sensor has no batches, 
        /// otherwise returns latest <see cref="SensorMeasurementBatch"/> from sensor.
        /// </returns>
        public SensorMeasurementBatch GetLatestBatch(Sensor sensor) {
            if (sensor == null) {
                return null;
            }

            return sensor.Batches.OrderByDescending(x => x.TimeStampPolled).FirstOrDefault();
        }

        /// <summary>
        /// Get all <see cref="SensorMeasurementBatch"/> 
        /// from a <see cref="Sensor"/> within a certain time interval.
        /// </summary>
        /// <param name="sensorId">The ID of the sensor.</param>
        /// <param name="timeFrom">The lower bound of the time interval.</param>
        /// <param name="timeTo">The upper bound of the time interval.</param>
        /// <returns>
        /// An ICollection of all <see cref="SensorMeasurementBatch"/>es for the sensor.
        /// </returns>
        public ICollection<SensorMeasurementBatch> GetSensorBatches(string sensorId,
                                                                    DateTime timeFrom,
                                                                    DateTime timeTo) {
            // I put this here * TEMPORARILY ?? * because when getting old data you probably do not
            // want to filter based on sensors that are inactive NOW. They could still have been active in the past.
            lock (context) {
                Sensor sensor = context.Sensors.FirstOrDefault(x => x.Id == sensorId);

                if (sensor == null) {
                    return null;
                }

                ICollection<SensorMeasurementBatch> batches = context.SensorMeasurementBatches.Where(x => x.SensorId == sensorId &&
                                                                                                 x.TimeStampFrom >= timeFrom &&
                                                                                                 x.TimeStampTo <= timeTo)
                                                                                          .OrderByDescending(x => x.TimeStampPolled)
                                                                                          .ToList();
                return batches;
            }
        }
    }
}
