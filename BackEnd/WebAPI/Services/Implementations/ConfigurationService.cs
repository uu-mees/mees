/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementations {
    /// <summary>
    /// The instance of the configuration service,
    /// which is used for storing configuration properties.
    /// </summary>
    public class ConfigurationService {
        /// <summary>The connection string for connecting to the database.</summary>
        public string ConnectionString { get; set; }

        /// <summary>The URL to the external sensor file</summary>
        public string SensorURL { get; set; }

        /// <summary>The URL to the external raster file.</summary>
        public string RasterURL { get; set; }

        /// <summary>
        /// The amount of hours after which a sensor will be marked as inactive.
        /// </summary>
        public int SensorTimeoutHours;

        /// <summary>
        /// The maximum percentage a route can be out of bounds. When exceeded
        /// the average exposure will not be set.
        /// </summary>
        public float MaxOutOfBoundsPercentage;

        /// <summary>The configuration for authorization tokens.</summary>
        public virtual AuthTokenConfiguration AuthTokenConfiguration { get; set; }

        /// <summary>The configuration for geocoding.</summary>
        public virtual GeocodeConfiguration GeocodeConfiguration { get; set; }

        /// <summary>The configuration for routing.</summary>
        public virtual RoutingConfiguration RoutingConfiguration { get; set; }

        public virtual EmailConfiguration EmailConfiguration { get; set; }
    }

    /// <summary>
    /// A container class for the authorization token configuration properties.
    /// </summary>
    public class AuthTokenConfiguration {
        /// <summary>The key for generating authorization tokens.</summary>
        public string AuthTokenKey { get; set; }

        /// <summary>The time a token is valid in minutes.</summary>
        public int TimeToLiveMinutes { get; set; }

        /// <summary>The instance which generated the token.</summary>
        public string Issuer { get; set; }
    }

    /// <summary>
    /// A container class for the geocoding configuration properties.
    /// </summary>
    public class GeocodeConfiguration {
        /// <summary>The api url.</summary>
        public string ApiUrl { get; set; }

        /// <summary>The api key.</summary>
        public string ApiKey { get; set; }

        /// <summary>The result type.</summary>
        public string ResultType { get; set; }
    }

    /// <summary>
    /// A container class for the routing configuration properties.
    /// </summary>
    public class RoutingConfiguration {
        /// <summary>The api url.</summary>
        public string ApiUrl { get; set; }
    }

    /// <summary>
    /// A container class for the Email configuration properties.
    /// </summary>
    public class EmailConfiguration {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
