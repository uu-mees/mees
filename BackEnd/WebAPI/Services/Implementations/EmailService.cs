/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Mail;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IEmailService"/>. Used to send emails.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class EmailService : IEmailService {
        EmailConfiguration EmailConfiguration;
        SmtpClient smtpClient;
        System.Net.NetworkCredential networkCredential;

        private ILogger<EmailService> logger;

        public EmailService(ILogger<EmailService> logger, ConfigurationService configurationService) {
            // Setup email credentials.
            EmailConfiguration = configurationService.EmailConfiguration;
            networkCredential = new System.Net.NetworkCredential(EmailConfiguration.Email, EmailConfiguration.Password);

            // Initialize client.
            smtpClient = new SmtpClient() {
                Host = "smtp.office365.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = networkCredential,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            this.logger = logger;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Email Service Thread";
        }

        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="recipient">Email recipient. (To)</param>
        /// <param name="subject">Email subject.</param>
        /// <param name="body">Email content.</param>
        /// <returns><see cref="true"/> if email was sent successfully.</returns>
        public bool SendEmail(string recipient, string subject, string body) {
            if (recipient == null || subject == null || body == null) {
                return false;
            }
            MailMessage mail = new MailMessage() {
                Subject = subject,
                Body = body,
                From = new MailAddress(networkCredential.UserName),
                IsBodyHtml = true
            };
            mail.To.Add(recipient);

            // Attempt to send email, if for some reason this fails return an error (false).
            try {
                smtpClient.Send(mail);
                return true;
            }
            catch {
                return false;
            }
        }
    }
}
