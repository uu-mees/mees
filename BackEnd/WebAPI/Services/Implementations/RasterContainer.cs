/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using Domain.Exposure;
using Services.DTOs;
using Services.Interfaces.Pollers;

namespace Services.Implementations {
    public class RasterContainer : IRasterContainer {
        private static RasterDTO rasterDTO = null;
        private static Raster raster = null;
        private static List<RasterCell> rastercells = null;

        /// <summary>
        /// Get the latest rasterDTO.
        /// </summary>
        /// <returns>The latest <see cref="RasterDTO"/> or null if there is none.</returns>
        public RasterDTO GetRasterImage() {
            return rasterDTO;
        }


        /// <summary>
        /// Get the latest raster.
        /// </summary>
        /// <returns>The latest <see cref="Raster"/> or null if there is none.</returns>
        public Raster GetRaster() {
            return raster;
        }

        /// <summary>
        /// Get the latest list of rastercells.
        /// </summary>
        /// <returns>The latest list of <see cref="RasterCell"/>s.</returns>
        public List<RasterCell> GetRasterCells() {
            return rastercells;
        }

        /// <summary>
        /// Update the currently stored raster.
        /// </summary>
        /// <param name="raster">The new raster that needs to be stored</param>
        public void UpdateRaster(Raster newraster) {
            rastercells = newraster.Cells.ToList();
            raster = newraster;
            rasterDTO = CreateImages(raster);
        }

        /// <summary>
        /// Takes a raster and converts it into a rasterDTO.
        /// </summary>
        /// <param name="raster"> that should be converted into an image</param>
        public RasterDTO CreateImages(Raster newraster) {
            List<RasterCell> cells = newraster.Cells.ToList();
            int height = newraster.Height;
            int width = newraster.Width;

            // Copy the necessary data
            RasterDTO tempRasterDTO = new RasterDTO();
            tempRasterDTO.TimeStampPolled = newraster.TimeStampPolled;

            tempRasterDTO.NorthEastLatitude = newraster.NorthEastLat;
            tempRasterDTO.NorthEastLongitude = newraster.NorthEastLng;

            tempRasterDTO.SouthWestLatitude = newraster.SouthWestLat;
            tempRasterDTO.SouthWestLongitude = newraster.SouthWestLng;

            Bitmap bitmap = new Bitmap(width, height);

            // There's a fake raster with height and width 1 that we wish to ignore
            // (This is displayed as an entirely gray image by the front-end)
            if (width != 1 && height != 1) {
                // Draw all rastercells to the bitmap.
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        int cellindex = y * newraster.Width + x;
                        bitmap.SetPixel(x, y, GetColor(cells[cellindex].Value));
                    }
                }
            }
            // Based on https://stackoverflow.com/questions/10889764/how-to-convert-bitmap-to-a-base64-string
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            byte[] imageBytes = stream.ToArray();

            tempRasterDTO.Image = Convert.ToBase64String(imageBytes);

            return tempRasterDTO;
        }

        // Convert a certain amount of exposure to a color.
        public Color GetColor(float exposure) => Color.FromArgb(
            FloatToByte(-254.9f + 12.75f * exposure),               // Red: starts low, then increases
            FloatToByte(0.159375f * exposure * (80f - exposure)),   // Green: starts and ends low, highest around 40
            FloatToByte(765f - 12.75f * exposure));                 // Blue: starts high, then decreases
        // Plot of the functions: https://www.wolframalpha.com/input/?i=Plot%5B%7B765+-+12.75x,+-255+%2B+12.75x,+0.159+(80-+x)+x%7D,+%7Bx,+-20,+100%7D,+%7By,+0,+255%7D%5D

        public static byte FloatToByte(float f) {
            if (f > 255) return 255;
            if (f < 0) return 0;
            return (byte)f;
        }
    }
}

