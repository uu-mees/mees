/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Converters.Routing;
using Services.DTOs.Routing;
using Services.Interfaces;
using System;
using System.Globalization;
using System.IO;
using System.Net;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IGeocodingService"/>.
    /// </summary>
    public class GeocodingService : IGeocodingService {
        private GeocodeConfiguration configuration;
        private ILogger<GeocodingService> logger;

        /// <summary>
        /// The constructor of the service.
        /// </summary>
        /// <param name="configurationService">
        /// A <see cref="ConfigurationService"/> instance specifying multiple configuration options.
        /// </param>
        public GeocodingService(ConfigurationService configurationService, ILogger<GeocodingService> logger) {
            configuration = configurationService.GeocodeConfiguration;
            this.logger = logger;
        }

        /// <summary>
        /// Find the coordinate corresponding to the given address.
        /// </summary>
        /// <param name="address">The address to find the coordinate for.</param>
        /// <returns>
        /// The GeocodingResponse for this query,
        /// can return multiple or zero matches as well.
        /// </returns>
        public GeocodingResponse Geocode(string address) {
            string url = FormatUrl(BuildQuery(address));
            return GetResponse(url);
        }
        /// <summary>
        /// Find the address corresponding to the given coordinate.
        /// </summary>
        /// <param name="lat">The latitude of the coordinate.</param>
        /// <param name="lng">The longitude of the coordinate.</param>
        /// <returns>
        /// The GeocodingResponse for this query,
        /// can return multiple or zero matches as well.
        /// </returns>
        public GeocodingResponse ReverseGeocode(double lat, double lng) {
            string url = FormatUrl(BuildQuery(lat, lng), true);
            return GetResponse(url);
        }

        /// <summary>
        /// Read and parse information from the url to a GeocodingResponse object.
        /// </summary>
        /// <param name="url">The Google api call.</param>
        /// <returns>The <see cref="GeocodingResponse"/> from this url.</returns>
        private GeocodingResponse GetResponse(string url) {
            string jsonStr = GetPlaintext(url);
            GeocodingDTO results = JsonConvert.DeserializeObject<GeocodingDTO>(jsonStr);
            return results.FromDTO();
        }

        private string BuildQuery(string address) {
            return String.Format(CultureInfo.InvariantCulture, "address={0}", address);
        }

        private string BuildQuery(double lat, double lng) {
            return String.Format(CultureInfo.InvariantCulture, "latlng={0},{1}", lat, lng);
        }

        /// <summary>
        /// Format the requested query into a correct google api request.
        /// </summary>
        /// <example>
        ///  Return example: https://maps.googleapis.com/maps/api/geocode/json?address=buys+ballot+gebouw&region=NL&language=EN&key=AIzaSyDyBHlmFMqkW0G-POJhSiECMB_ZOFfkkcQ
        /// </example>
        /// <param name="query">The query to build a query url for.</param>
        /// <param name="reverse">Whether the query is for reverse geocoding or not.</param>
        /// <returns>The Google api url.</returns>
        private string FormatUrl(string query, bool reverse = false) {
            // If we want to reverse geocode we define the language to write addresses in to be English, and the result
            // to be as specific as possible. 
            string reverseSpecifics = reverse ? "&location_type=ROOFTOP&result_type=street_address" : "&region=NL";
            return configuration.ApiUrl + configuration.ResultType + "?" + query + "&language=EN" + reverseSpecifics + "&key=" + configuration.ApiKey;
        }

        /// <summary>
        /// Get the information from a url as a string.
        /// </summary>
        /// <param name="url">The url to read from.</param>
        /// <returns>The information as a string.</returns>
        private string GetPlaintext(string url) {
            try {
                WebRequest webRequest = WebRequest.Create(url);
                WebResponse webResponse = webRequest.GetResponse();
                Stream dataStream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string str = reader.ReadToEnd();
                webResponse.Close();
                return str;
            }
            catch (Exception e) {
                // This can really only happen when Google returns an error or the Google servers
                // have to be offline, which is not very likely to happen.
                logger.LogCritical("GOOGLE IS ON FIRE");
                throw new GeocodingException(e.Message);
            }
        }
    }

    /// <summary>
    /// The custom exceptions to throw when Google geocoding returns an error code.
    /// </summary>
    public class GeocodingException : Exception {
        /// <summary>
        /// The constructor for the exception.
        /// </summary>
        /// <param name="message">
        /// The message corresponding to the error.
        /// </param>
        public GeocodingException(string message)
            : base(String.Format("Geocoding error: {0}", message)) { }
    }
}
