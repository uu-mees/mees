/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Utils;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using Domain.Exposure;
using Domain.Routing;
using Domain.Tracking;
using Microsoft.Extensions.Logging;

namespace Services.Implementations {
    public class ExposureService : IExposureService {
        private ILogger<ExposureService> logger;
        private IRasterService rasterService;
        private ConfigurationService configurationService;

        public ExposureService(ConfigurationService configurationService, IRasterService rasterService,
            ILogger<ExposureService> logger) {
            this.logger = logger;
            this.rasterService = rasterService;
            this.configurationService = configurationService;
        }

        /// <summary>
        /// Calculate the exposure for the route and insert the results directly into the route.
        /// This includes adding the Exposure, setting the EdgeExposures and also (possibly) changing the
        /// Path.
        /// </summary>
        /// <param name="route">The route to calculate for and insert the result into.</param>
        public void InsertExposure(Route route) {
            if (route == null)
                return;

            route.ExposurePath = GetExposurePath(route);
        }

        /// <summary>
        /// Calculate the exposure for a given route.
        /// </summary>
        /// <param name="route">The route to calculate the exposure for.</param>
        /// <returns>The average exposure.</returns>
        public double? GetExposure(Route route) {
            if (route == null)
                return null;

            ExposurePath result = GetExposurePath(route);
            return result.Exposure;
        }
        /// <summary>
        /// Get the exposure for a given coordinate.
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <returns>The exposure.</returns>
        public double? GetExposure(GeoCoordinate coordinate) {
            RasterCell cell = rasterService.GetRasterCell(rasterService.GetLatestRaster().Id, coordinate);
            return cell == null ? (float?)null : cell.Value;
        }

        /// <summary>
        /// Get the exposure for a given coordinate and raster.
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <param name="raster">The raster to get the exposure from.</param>
        /// <returns>The exposure.</returns>
        public double? GetExposureFromRaster(GeoCoordinate coordinate, Raster raster) {
            RasterCell cell = rasterService.GetRasterCell(raster.Id, coordinate);
            return cell == null ? (float?)null : cell.Value;
        }


        /// <summary>
        /// Calculate the ExposurePath for a given route.
        /// </summary>
        /// <param name="route">The route to calculate the ExposurePath for.</param>
        /// <returns>The ExposurePath.</returns>
        private ExposurePath GetExposurePath(Route route) {
            return GetExposureFromPath(route.Path, rasterService.GetLatestRaster()?.Id ?? null);
        }

        /// <summary>
        /// Calculate the ExposureResult for a given path (can be null if there is no path or no data available).
        /// </summary>
        /// <param name="path">The path to calculate the exposure for.</param>
        /// <param name="rasterId">The raster to use for exposure calculation</param>
        /// <returns>An ExposureResult holding the average exposure, the original path with possibly extra points
        /// inserted (where the entire raster was intersected), and the a list of average exposures per edge.</returns>
        private ExposurePath GetExposureFromPath(List<GHPoint> path, int? rasterId) {

            // Use the latest raster for all intersection calculations to save RAM 
            // (every calculation uses the same raster and rastercells instead of loading several different rasters)
            Raster raster = rasterService.GetLatestRaster();

            // No route -> no average.
            if (path == null)
                return new ExposurePath();

            ExposurePath emptyResult = new ExposurePath(path);
            // No raster -> no average.
            if (rasterId == null || raster == null)
                return emptyResult;

            if (!rasterService.SameDimensions(raster.Id, (int)rasterId))
                return emptyResult;

            // No path -> no average.
            if (path.Count == 0)
                return emptyResult;

            // OOB = Out-of-bounds
            bool OOB = false;

            GeoCoordinate currentCoordinate = path[0].Coordinate;

            RasterCell currentCell = rasterService.GetRasterCell(raster.Id, currentCoordinate);
            float? currentCellValue = null;

            // The starting point can be out of bounds
            if (currentCell == null)
                OOB = true;
            else
                currentCellValue = rasterService.GetRasterCell(currentCell.CellIndex, rasterId)?.Value ?? null;
            // If there is only one point the average is the value of the raster for that point
            if (path.Count == 1 && OOB)
                return emptyResult; // (unless we are OOB, then the average is null)
            else if (path.Count == 1) {
                return new ExposurePath(currentCellValue, path, new List<double?>() { currentCellValue });
            }

            // nextIndex is a "pointer" that points to the end of the current edge.
            int nextIndex = 1;
            // nextCoordinate is the coordinate of the end point of the current edge.
            GeoCoordinate nextCoordinate = path[nextIndex].Coordinate;

            // The side of the raster or raster cell that was intersected (0 = right, 1 = down, 2 = left, 3 = up, null = none)
            // (initially null since there hasn't been an intersection yet).
            int? intersectionSide = null;

            // Set the duration and velocity values for the first edge
            long currentEdgeDuration = path[nextIndex].ElapsedTime - path[nextIndex - 1].ElapsedTime;
            double currentEdgeVelocity = divOrZero(currentEdgeDuration, currentCoordinate.GetDistanceTo(nextCoordinate));
            double currentEdgeValue = 0;

            // The total sum of exposure * duration (the average is totalValue / total duration).
            double totalValue = 0;

            // The amount of time that was spent OOB (this has to be subtracted from the total duration to get a valid average).
            long OOBDuration = 0;

            // The list where the average exposure per edge is stored.
            List<double?> edgeExposures = new List<double?>();

            void updateEdgeVariables() {
                // Set the duration of the entire edge.
                currentEdgeDuration = path[nextIndex].ElapsedTime - path[nextIndex - 1].ElapsedTime;
                // Milisecs per meter
                currentEdgeVelocity = divOrZero(currentEdgeDuration, currentCoordinate.GetDistanceTo(nextCoordinate));
            }

            double divOrZero(double a, double b) {
                return b == 0 ? 0 : a / b;
            }

            long calculateDuration(GeoCoordinate next) {
                double distance = currentCoordinate.GetDistanceTo(next);
                return (long)Math.Round(currentEdgeVelocity * distance, 0, MidpointRounding.AwayFromZero);
            }

            void nextEdge() {
                // Reset the edge value
                currentEdgeValue = 0;

                // Update currentCoordinate
                currentCoordinate = nextCoordinate;

                nextIndex++;
                // If there is a next point we need to update more values
                if (nextIndex < path.Count) {
                    nextCoordinate = path[nextIndex].Coordinate;
                    updateEdgeVariables();
                }
            }

            #region Summary of the algorithm
            /* There is an implicit state that is updated during the algorithm. The variables of this state:
             * - path: The list of GHPoints that make up the polyline of the route we are calculating the exposure for.
             * - currentCoordinate: the GeoCoordinate that defines the start of the edge(part) we are currently "looking at"
             * This does not necessarily have to be the start of the entire edge edge, it can be any point between the start 
             * and end of an edge of the path.
             * - nextCoordinate: The GeoCoordinate that is the end of the current edge (always the end, not a point inbetween).            
             * - nextIndex: The index of the nextCoordinate in the path array.
             * (When referring to the "current edge" I am talking about an edge from the path, so path[nextIndex - 1] to 
             *  path[nextIndex], and not currentCoordinate to nextCoordinate)
             * - currentEdgeDuration: The variable storing the duration (in miliseconds) from the start to the end of an edge.
             * - currentVelocity: Miliseconds per meter for the current edge.
             * - currentEdgeDuration: The duration (in miliseconds) of the current edge.
             * - currentEdgeValue: The accumulated total exposure for the current edge.
             * - totalValue: The accumulated total exposure for the entire path.
             * - currentCell: The rasterCell the currentCoordinate is inside of (can be null if the currentCoordinate is
             * out of bounds).
             * - OOB: The boolean value representing the situation the currentCoordinate is in, OOB = true means the
             * currentCoordinate is out of bounds of the entire raster, OOB = false means the currentCoordinate is inside
             * the currentCell.
             * - intersectionSide (0 = right, 1 = down, 2 = left, 3 = up, null = no intersection): The side of the raster that was 
             * intersected; the side of the raster the currentCoordinate is the intersection for. This is used for preventing 
             * infinite loops, when we intersect an edge we get an intersection point, this becomes the currentCoordinate, 
             * if we were to check intersection again we would find the exact same point as an intersection again, to prevent 
             * this we exclude the edge that we just intersected from the intersection check (unless the edge is a tangent line, 
             * in which case the same intersection is returned).
             * 
             * To explain the loop: There are two phases the algorithm can be in, each phase has two possible iteration types.
             * The two phases: the currentCoordinate can be out of bounds of the raster or it can be in bounds of a rasterCell.
             * If we are OOB we check intersection with the entire raster. If we do not find an intersection the entire edge is
             * OOB, the currentCoordinate becomes the nextCoordinate and we update the nextIndex. 
             * If we do intersect the edge enters the raster again and we are done with the OOB phase, we add the intersection to 
             * the path (to split the OOB and in bounds parts) and the currentCoordinate becomes this intersection, nextCoordinate 
             * stays the same but nextIndex is updated since we added a new coordinate to path.
             * If we are in bounds we check for intersection with the currentCell. If we do not find an intersection the entire
             * edge is within the currentCell, the currentCoordinate becomes the nextCoordinate and we update the nextIndex. 
             * If we do find an intersection we leave this cell and either enter another cell or go OOB. 
             * If we enter another cell we stay in the in bounds phase, currentCoordinate becomes this intersection and currentCell
             * is updated. If we go out of bounds we add this intersection to the path (to split in bounds and OOB) currentCoordinate
             * becomes this intersection and we go back to the OOB phase.
             * These steps are continued until we reach the last point in the path.
             */
            #endregion
            while (nextIndex < path.Count) {
                // Are we out of bounds?
                if (OOB) {
                    // Calculate the intersection with the entire Raster
                    Tuple<GeoCoordinate, int?, RasterCell> rasterTuple = CalculateIntersection(currentCoordinate, nextCoordinate, raster, intersectionSide);
                    GeoCoordinate rasterIntersection = rasterTuple.Item1;
                    intersectionSide = rasterTuple.Item2;
                    currentCell = rasterTuple.Item3;

                    // If there is no intersection the entire edge is outside of the Raster
                    if (rasterIntersection == null) {
                        // OOB means no exposure data
                        edgeExposures.Add(null);
                        // Update the time spent OOB
                        OOBDuration += currentEdgeDuration;

                        nextEdge();

                        continue;
                    }
                    // If there is an intersection the edge enters the raster
                    else {
                        currentCellValue = rasterService.GetRasterCell(currentCell.CellIndex, rasterId)?.Value ?? null;
                        // Calculate the duration from the currentCoordinate to the intersection.
                        long duration = calculateDuration(rasterIntersection);

                        // This edge was partly out of bounds so we update the OOBDuration.
                        OOBDuration += duration;

                        // We need to split this edge into two edges: the part that's OOB and the part that is in bounds.
                        // We do this by adding the intersection point to the path.
                        path.Insert(nextIndex, new GHPoint() {
                            Coordinate = rasterIntersection,
                            ElapsedTime = path[nextIndex - 1].ElapsedTime + duration
                        });
                        // Update the pointer so it still points to the nextCoordinate.
                        nextIndex++;

                        // The OOB part has no value so we add that to the list.
                        edgeExposures.Add(null);

                        // Update the currentCoordinate pointer
                        currentCoordinate = rasterIntersection;

                        // We update the duration and velocity for the in bounds edge we created.
                        updateEdgeVariables();

                        /* We intersected with a side of the raster. This side would normally get flipped because 
                         * leaving cell A through the left side would mean that we have entered cell B through
                         * the right side. However, this is not the case when intersecting the entire raster.
                         * If we intersect the entire raster through the left side, we have entered the
                         * rasterCELL also through the left side. To correct this we flip the intersection side
                         * here, so when it gets flipped later on it will be correct.
                         */
                        intersectionSide = (intersectionSide + 2) % 4;

                        // We are no longer out of bounds, the normal algorithm can continue now.
                        OOB = false;
                    }
                }

                /* Calculate the intersection with the current RasterCell
                 * NOTE: One may notice that we change the entranceEdge "(intersectionSide + 2) % 4", the reason for this is best
                 * explained using an example: if we leave cell A through the left side to enter cell B, we have entered the B cell through
                 * the right side (and not through the left side). So we "flip" the intersection side.
                 */
                Tuple<GeoCoordinate, int?> intersectionTuple = CalculateIntersection(currentCoordinate, nextCoordinate, currentCell, (intersectionSide + 2) % 4);
                GeoCoordinate intersection = intersectionTuple.Item1;
                intersectionSide = intersectionTuple.Item2;

                // If there is no intersection the entire edge is within the rasterCell
                if (intersection == null) {
                    // Update the value for this edge
                    long duration = calculateDuration(nextCoordinate);
                    currentEdgeValue += (float)currentCellValue * duration;

                    // Update the total value
                    totalValue += currentEdgeValue;

                    // Store the average exposure for the current edge.
                    edgeExposures.Add(divOrZero(currentEdgeValue, currentEdgeDuration));

                    nextEdge();
                }
                // If there is an intersection the edge leaves this rasterCell
                else {
                    // Update the value for this edge
                    long duration = calculateDuration(intersection);
                    currentEdgeValue += (float)currentCellValue * duration;

                    // The currentCoordinate becomes the intersection point (but 
                    // the next point stays the same since we are still on the same edge)
                    currentCoordinate = intersection;

                    // The edge has left the cell so we need to find the rastercell next to this cell
                    // using our knowledge about the side we intersected with.
                    currentCell = GetNextRasterCell(currentCell, intersectionSide);
                    if (currentCell != null)
                        currentCellValue = rasterService.GetRasterCell(currentCell.CellIndex, rasterId)?.Value ?? null;

                    // If there is no cell in the specified direction the path has gone out of bounds.
                    if (currentCell == null) {
                        currentCellValue = null;
                        OOB = true;

                        // We need to split this edge into two parts: the in bounds, and the out of bounds parts.
                        // Calculate the duration of the in bounds part.
                        long edgeDuration = currentEdgeDuration - calculateDuration(nextCoordinate);
                        // Add the average value for this part
                        edgeExposures.Add(divOrZero(currentEdgeValue, edgeDuration));

                        // We split the edge by adding the intersection point to the path.
                        path.Insert(nextIndex, new GHPoint() {
                            Coordinate = intersection,
                            ElapsedTime = path[nextIndex - 1].ElapsedTime + edgeDuration
                        });
                        nextIndex++;

                        // Update the total value
                        totalValue += currentEdgeValue;
                        // Reset the edge value
                        currentEdgeValue = 0;

                        // We update the duration and velocity for the out of bounds edge we created.
                        updateEdgeVariables();
                    }
                }
            }

            if (path.Last().ElapsedTime - OOBDuration <= 0)
                return emptyResult;

            double? averageExposure = null;
            if (OOBDuration / (double)path.Last().ElapsedTime <= configurationService.MaxOutOfBoundsPercentage)
                averageExposure = totalValue / (path.Last().ElapsedTime - OOBDuration);
            ExposurePath result = new ExposurePath(averageExposure, path, edgeExposures);
            return result;
        }

        /// <summary>
        /// Find the neighbour of a rasterCell in a given direction.
        /// </summary>
        /// <param name="currentCell">The rasterCell we want to find the neighbour for.</param>
        /// <param name="side">The neighbour we want: 0 = right, 1 = down, 2 = left, 3 = up, null = error.</param>
        /// <returns>The next <see cref="RasterCell"/>.</returns>
        private RasterCell GetNextRasterCell(RasterCell currentCell, int? side) {
            Exception CellOutOfRangeException = new ArgumentOutOfRangeException("The neighbour cell is not in range of the raster.");

            int width = currentCell.Raster.Width;
            int height = currentCell.Raster.Height;

            // Revert 1D coordinates to their 2D representation.
            int y = currentCell.CellIndex / width;
            int x = currentCell.CellIndex % width;

            // Update the coordinate according to the selected side.
            switch (side) {
                case 0:
                    x++;
                    if (x >= width)
                        return null;
                    break;
                case 1:
                    y++;
                    if (y >= height)
                        return null;
                    break;
                case 2:
                    x--;
                    if (x < 0)
                        return null;
                    break;
                case 3:
                    y--;
                    if (y < 0)
                        return null;
                    break;
                default:
                    throw new Exception("Invalid side parameter");
            }

            // Revert back to 1D representation.
            int k = y * width + x;
            // Return the neighbour. !!!!!ElementAt(k) does not work, 6245 gave me 6698 instead
            //return rasterService.GetRasterCell(k, currentCell.RasterId);
            return rasterService.GetRasterCell(k, currentCell.Raster.Id);// rasterService.GetRasterCell(k, currentCell.RasterId);
        }

        /// <summary>
        /// Get the bounds for a given RasterCell.
        /// </summary>
        /// <param name="rasterCell">The RasterCell to get the bounds for.</param>
        /// <returns>The bounds as a tuple (order: item1: NE, item2: SE, item3: SW, item4: NW)</returns>
        private Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> GetBounds(RasterCell rasterCell) {
            GeoCoordinate corner1 = new GeoCoordinate(rasterCell.NorthEastLatitude, rasterCell.NorthEastLongitude);
            GeoCoordinate corner2 = new GeoCoordinate(rasterCell.SouthWestLatitude, rasterCell.NorthEastLongitude);
            GeoCoordinate corner3 = new GeoCoordinate(rasterCell.SouthWestLatitude, rasterCell.SouthWestLongitude);
            GeoCoordinate corner4 = new GeoCoordinate(rasterCell.NorthEastLatitude, rasterCell.SouthWestLongitude);

            return new Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate>(corner1, corner2, corner3, corner4);
        }
        /// <summary>
        /// Get the bounds for a given Raster.
        /// </summary>
        /// <param name="raster">The Raster to get the bounds for.</param>
        /// <returns>The bounds as a tuple (order: item1: NE, item2: SE, item3: SW, item4: NW)</returns>
        public Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> GetBounds(Raster raster) {
            GeoCoordinate corner1 = new GeoCoordinate(raster.NorthEastLat, raster.NorthEastLng);
            GeoCoordinate corner2 = new GeoCoordinate(raster.SouthWestLat, raster.NorthEastLng);
            GeoCoordinate corner3 = new GeoCoordinate(raster.SouthWestLat, raster.SouthWestLng);
            GeoCoordinate corner4 = new GeoCoordinate(raster.NorthEastLat, raster.SouthWestLng);

            return new Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate>(corner1, corner2, corner3, corner4);
        }

        /// <summary>
        /// Calculate the intersection of the edge defined by start and end with the given rasterCell.
        /// </summary>
        /// <param name="start">The start of the edge.</param>
        /// <param name="end">The end of the edge.</param>
        /// <param name="rasterCell">The rasterCell to test intesection with.</param>
        /// <param name="entranceEdge">The side of the cell the line entered the cell through, this side will be
        /// excluded from intersection checking, unless there is no intersection with any of the other sides (in which
        /// case the line was a tangent line). (0 = right, 1 = down, 2 = left, 3 = up, null = none).</param>
        /// <param name="OOB">The current state assumption; are we outside or inside the rasterCell we are testing the intersection for?</param>
        /// <returns>A tuple of the intersection and the side of the cell that was intersected (0 = right, 1 = down, 2 = left, 3 = up, null = none).</returns>
        private Tuple<GeoCoordinate, int?> CalculateIntersection(GeoCoordinate start, GeoCoordinate end, RasterCell rasterCell, int? entranceEdge = null, bool OOB = false) {
            return CalculateIntersection(start, end, GetBounds(rasterCell), entranceEdge, OOB);
        }
        /// <summary>
        /// Calculate the intersection of the edge defined by start and end with the given raster.
        /// </summary>
        /// <param name="start">The start of the edge.</param>
        /// <param name="end">The end of the edge.</param>
        /// <param name="raster">The raster to test intesection with.</param>
        /// <param name="entranceEdge">The side of the cell the line entered the cell through, this side will be
        /// excluded from intersection checking, unless there is no intersection with any of the other sides (in which
        /// case the line was a tangent line). (0 = right, 1 = down, 2 = left, 3 = up, null = none).</param>
        /// <returns>A tuple of the intersection and the side of the cell that was intersected (0 = right, 1 = down, 2 = left, 3 = up, null = none).</returns>
        private Tuple<GeoCoordinate, int?, RasterCell> CalculateIntersection(GeoCoordinate start, GeoCoordinate end, Raster raster, int? entranceEdge = null) {
            Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> bounds = GetBounds(raster);
            GeoCoordinate[] corners = new GeoCoordinate[]
            {
                bounds.Item1,
                bounds.Item2,
                bounds.Item3,
                bounds.Item4
            };

            // Calculate the intersection with the raster bounds (if there is one).
            Tuple<GeoCoordinate, int?> intersection = CalculateIntersection(start, end, bounds, entranceEdge, true);

            // If there is an intersection coordinate
            bool maybeIntersect = GeoUtils.SpecialWithinArea(end, corners[0], corners[1], corners[2], corners[3]);
            if (maybeIntersect || intersection.Item1 != null) {
                // Calculate the intersection with the raster bounds (if there is one).
                //Tuple<GeoCoordinate, int?> intersection = CalculateIntersection(start, end, bounds, entranceEdge, true);

                List<RasterCell> cells = new List<RasterCell>();
                if (!maybeIntersect && intersection.Item1 != null)
                    // Get all the cells for the edge of the raster we just intersected.
                    cells = GetRasterEdge((int)intersection.Item2, raster);
                else {
                    cells = GetRasterEdge(0, raster);
                    cells.AddRange(GetRasterEdge(1, raster));
                    cells.AddRange(GetRasterEdge(2, raster));
                    cells.AddRange(GetRasterEdge(3, raster));
                }

                // If we just intersected the raster and went out of bounds (in which case the entranceEdge would not be null)
                // we need to remove the cell that contains the starting point to prevent returning the same point as
                // an intersection again.
                if (entranceEdge != null)
                    cells = cells.Where(x =>
                        !GeoUtils.SpecialWithinArea(start,
                                                    new GeoCoordinate(x.NorthEastLatitude, x.NorthEastLongitude),
                                                    new GeoCoordinate(x.SouthWestLatitude, x.NorthEastLongitude),
                                                    new GeoCoordinate(x.SouthWestLatitude, x.SouthWestLongitude),
                                                    new GeoCoordinate(x.NorthEastLatitude, x.SouthWestLongitude))).ToList();

                // For every cell we check intersection
                List<Tuple<GeoCoordinate, int?, RasterCell>> cellIntersections = new List<Tuple<GeoCoordinate, int?, RasterCell>>();
                foreach (RasterCell cell in cells) {
                    Tuple<GeoCoordinate, int?> cellIntersection = CalculateIntersection(start, end, cell, null, true);
                    if (cellIntersection.Item1 != null)
                        cellIntersections.Add(new Tuple<GeoCoordinate, int?, RasterCell>(cellIntersection.Item1, cellIntersection.Item2, cell));
                }

                // If there is at least one cell intersection
                if (cellIntersections.Count > 0) {
                    // For all the intersections we found (which can be more than one), we need to check
                    // which intersection is the closest to the start point of the edge (and thus the first
                    // intersection).
                    Tuple<GeoCoordinate, int?, RasterCell> firstIntersection = new Tuple<GeoCoordinate, int?, RasterCell>(null, null, null);
                    double bestDistance = double.MaxValue;
                    foreach (Tuple<GeoCoordinate, int?, RasterCell> intersectionTuple in cellIntersections) {
                        // Calculate the distance and compare it to the current "best".
                        double distance = start.GetDistanceTo(intersectionTuple.Item1);
                        // If this one is closer we update the intersection.
                        if (distance < bestDistance) {
                            firstIntersection = intersectionTuple;
                            bestDistance = distance;
                        }
                    }

                    return firstIntersection;
                }
            }

            // We either did not find an intersection with the raster at all or we
            // did find one but the edge did not actually intersect any of the rasterCells.
            // In both cases we return a null intersection tuple
            return new Tuple<GeoCoordinate, int?, RasterCell>(null, null, null);
        }
        /// <summary>
        /// Calculate the intersection of the edge defined by start and end with the given bounds.
        /// </summary>
        /// <param name="start">The start of the edge.</param>
        /// <param name="end">The end of the edge.</param>
        /// <param name="bounds">The bounds of the area to test intesection with.</param>
        /// <param name="entranceEdge">The side of the cell the line entered the cell through, this side will be
        /// excluded from intersection checking, unless there is no intersection with any of the other sides (in which
        /// case the line was a tangent line). (0 = right, 1 = down, 2 = left, 3 = up, null = none).</param>
        /// <param name="OOB">The current state assumption; are we outside or inside the boundary we are testing the intersection for?</param>
        /// <returns>A tuple of the intersection and the side of the cell that was intersected (0 = right, 1 = down, 2 = left, 3 = up, null = none).</returns>
        private Tuple<GeoCoordinate, int?> CalculateIntersection(GeoCoordinate start, GeoCoordinate end, Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> bounds, int? entranceEdge = null, bool OOB = false) {
            int? side = null;
            GeoCoordinate[] corners = new GeoCoordinate[] { bounds.Item1, bounds.Item2, bounds.Item3, bounds.Item4 };

            // Calculate for every side of the rasterCell if the given edge intersects it.
            GeoCoordinate[] intersections = new GeoCoordinate[4];
            for (int i = 0; i < 4; i++) {
                intersections[i] = entranceEdge != i ? GeoUtils.CalcIntersection(start, end, corners[i], corners[(i + 1) % 4]) : null;
            }

            // Fix corner loop: If we find more than two intersections we have intersected a corner and another side
            // of the bounds, we do not want the corner intersection, we want the other intersection (otherwise we
            // loop in a corner forever).
            // Closer than this means the same point:
            double marginOfError = 0.000000001;
            if (intersections.Sum(x => x == null ? 0 : 1) > 2) {
                for (int i = 0; i < 4; i++) {
                    intersections[i] = intersections[i] != null && start.GetDistanceTo(intersections[i]) <= marginOfError ? null : intersections[i];
                }
            }

            // For all the intersections we found (which can be more than one), we need to check
            // which intersection is the closest to the start point of the edge (and thus the first
            // intersection).
            GeoCoordinate bestIntersection = null;
            double bestDistance = double.MaxValue;
            for (int i = 0; i < 4; i++) {
                GeoCoordinate intersection = intersections[i];
                if (intersection != null) {
                    // Calculate the distance and compare it to the current "best".
                    double distance = start.GetDistanceTo(intersection);
                    // If this one is closer we update the intersection.
                    if (distance < bestDistance) {
                        bestIntersection = intersection;
                        bestDistance = distance;
                        side = i;
                    }
                }
            }

            #region Tangent edge fix
            /* The OOB boolean gives us information about the current state assumption, true means that
             * the assumption is that we are currently outside of the boundary we are testing the intersection
             * for.
             * False means that the assumption is that we are currently inside the boundary we are testing
             * the intersection for.
             * A very important thing here is that this assumption has to remain true, OR we need to find an 
             * intersection that flips the assumption. 
             * In short: If we are in bounds we are looking for an intersection that brings us out of bounds, if 
             * we are out of bounds we are looking for an intersection that brings us in bounds.
             * 
             * There is however a problem that can occur rarely but if it does it will break the assumption
             * and will therefore cause the entire algorithm to break.
             * If the assumption was that we are in bounds but the end of the edge is outside, we should have
             * found an intersection. But if we did not, we have a problem. This is the case
             * when an edge is a tangent line to the an edge of the raster. In that case there is only one point if contact,
             * so we just return that point as intersection again.
             * Example of a request where this would happen if we did not take action:
             * http://localhost:50500/routing?route=52.1238464,5.1554057,52.0210026,5.1824907,bike,shortest
             */
            #endregion
            if (!OOB && !GeoUtils.SpecialWithinArea(end, corners[0], corners[1], corners[2], corners[3]) && bestIntersection == null) {
                if (entranceEdge != null) {
                    bestIntersection = start;
                    side = entranceEdge;
                }
            }

            return new Tuple<GeoCoordinate, int?>(bestIntersection, side);
        }

        /// <summary>
        /// Returns all the RasterCells that form the requested boundary edge (0 = right, 1 = down, 2 = left, 3 = up).
        /// </summary>
        /// <param name="side">The side of the Raster to get the cells for.</param>
        /// <param name="raster">The Raster to get the cells from.</param>
        /// <returns>Returns all the RasterCells that form the requested boundary edge (0 = right, 1 = down, 2 = left, 3 = up).</returns>
        private List<RasterCell> GetRasterEdge(int side, Raster raster) {
            // For the right side we need to set an x offset of raster.Width - 1
            int x = side == 0 ? raster.Width - 1 : 0;
            // For the bottom side we need to set an y offset of raster.Height - 1
            int y = side == 1 ? raster.Height - 1 : 0;

            List<RasterCell> cells = new List<RasterCell>();
            // For the right and left side we only need to increase the y value
            if (side == 0 || side == 2) {
                for (; y < raster.Height; y++) {
                    cells.Add(rasterService.GetRasterCell(y * raster.Width + x, raster.Id));
                }
            }
            // For the bottom and top side we only need to increase the x value
            else {
                for (; x < raster.Width; x++) {
                    cells.Add(rasterService.GetRasterCell(y * raster.Width + x, raster.Id));
                }
            }

            return cells;
        }
    }

    public struct AverageExposureAnswer {
        public double? Average;
        public long TotalMilliseconds;
        public double TotalExposure;
    }
}
