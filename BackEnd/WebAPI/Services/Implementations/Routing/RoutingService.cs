/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Converters.Routing;
using Services.DTOs;
using Services.Interfaces;
using System;
using System.Device.Location;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Net;
using Utils;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IRoutingService"/>.
    /// </summary>
    public class RoutingService : IRoutingService {
        private IExposureService exposureService;
        private RoutingConfiguration configuration;
        private ILogger<RoutingService> logger;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        public RoutingService(ConfigurationService configurationService, IExposureService exposureService,
            ILogger<RoutingService> logger) {
            this.exposureService = exposureService;
            this.logger = logger;
            configuration = configurationService.RoutingConfiguration;
        }
        /// <summary>
        /// Get the raw responseDTO from GraphHopper. 
        /// <para>To get the actual Routes object use GetAllRoutes() instead.</para>
        /// </summary>
        /// <param name="start">The start coordinate for routing.</param>
        /// <param name="dest">The destination coordinate for routing.</param>
        /// <param name="vehicle">The vehicle for routing.</param>
        /// <param name="weighting">The weighting for routing.</param>
        /// <returns>The <see cref="RoutingResponseDTO"/> for the requested query.</returns>
        public RoutingResponseDTO GetRoutingResponse(GeoCoordinate start, GeoCoordinate dest, string vehicle = "bike", string weighting = "shortest") {

            string requestUrl = String.Format(CultureInfo.InvariantCulture, "{0}routing?startLat={1}&startLong={2}&destLat={3}&destLong={4}&vehicle={5}&weighting={6}",
                configuration.ApiUrl, start.Latitude, start.Longitude, dest.Latitude, dest.Longitude, vehicle, weighting);

            string jsonStr = GetPlaintext(requestUrl);
            return JsonConvert.DeserializeObject<RoutingResponseDTO>(jsonStr);
        }


        /// <summary>
        /// Query GraphHopper for a route.
        /// </summary>
        /// <param name="start">The start coordinate for routing.</param>
        /// <param name="dest">The destination coordinate for routing.</param>
        /// <param name="vehicle">The vehicle for routing.</param>
        /// <param name="weighting">The weighting for routing.</param>
        /// <returns>The resulting <see cref="Routes"/> object for the requested query.</returns>

        //This method completely depends on other methods (which are tested separately) and will only fail if those
        //methods fail, which is why there are no tests for this method separately and it is excluded from the coverage.
        [ExcludeFromCodeCoverage]
        public Routes GetAllRoutes(GeoCoordinate start, GeoCoordinate dest, string vehicle = "bike", string weighting = "shortest") {
            RoutingResponseDTO response = GetRoutingResponse(start, dest, vehicle, weighting);

            if (response.Errors != null) {
                foreach (string message in response.Errors) {
                    logger.LogInformation(message);
                }
                return null;
            }

            Routes routes = response.FromDTO();

            //Calculate the exposure for each route
            routes.RouteList.ForEach(x => exposureService.InsertExposure(x));
            return routes;
        }

        /// <summary>
        /// Get the information from a url as a string.
        /// </summary>
        /// <param name="url">The url to read from.</param>
        /// <returns>The information as a string.</returns>
        private string GetPlaintext(string url) {
            try {
                WebRequest webRequest = WebRequest.Create(url);
                WebResponse webResponse = webRequest.GetResponse();
                Stream dataStream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string str = reader.ReadToEnd();
                webResponse.Close();
                return str;
            }
            catch (Exception e) {
                logger.LogError(e.Message);
                throw new RoutingException();
            }
        }
    }

    /// <summary>
    /// The custom exception to throw when GraphHopper does not respond 
    /// or returns an error code (because of an invalid request).
    /// </summary>
    public class RoutingException : Exception {
        /// <summary>
        /// The constructor for the exception.
        /// </summary>
        public RoutingException() : base("Routing offline or invalid request.") { }
    }
}
