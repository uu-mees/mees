/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Linq;
using System.Text;
using Services.Interfaces;
using System.Threading;
using Domain.Administration;
using System.Security.Cryptography;
using DataAccess;
using System.Net.Mail;
using Domain.Tracking;
using Domain.Exposure;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data.Entity.Migrations;
using Microsoft.Extensions.Logging;
using Services.Retrievers;
using Services.DTOs;
using Domain.Questionnaire;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IUserService"/>.
    /// </summary>
    public class UserService : IUserService {
        private readonly IAuthenticationService authenticationService;
        private ILogger<UserService> logger;
        private readonly DatabaseContext context;
        private IEmailService emailService;
        private QuestionnaireConfigurationService configurationService;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="authenticationService">
        /// An <see cref="AuthenticationService"/> instance for authenticating users.
        /// </param>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        public UserService(IAuthenticationService authenticationService,
            DatabaseContext context,
            ILogger<UserService> logger,
            QuestionnaireConfigurationService configurationService,
            IEmailService emailService
            ) {
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "User Service Thread";
            this.authenticationService = authenticationService;
            this.logger = logger;
            this.context = context;
            this.emailService = emailService;
            this.configurationService = configurationService;
        }

        /// <summary>
        /// Validates user registration information and creates new user.
        /// </summary>
        /// <param name="userRegistration">
        /// <see cref="UserRegistration"/> object containing user registration information.
        /// </param>
        /// <returns>
        /// If validation completes, returns the new <see cref="User"/>. 
        /// Otherwise returns null.
        /// </returns>
        public User CreateNewUser(UserRegistration userRegistration) {
            if (userRegistration.Username == null || userRegistration.Password == null || userRegistration.Email == null) {
                throw new Exception("Failed to create account");
            }

            if (userRegistration.Username.Length < 5 ||
                userRegistration.Username.Length > 15 ||
                userRegistration.Username.Contains(' ')) {
                throw new Exception("Failed to create account");
            }

            lock (context) {
                if (context.Users.Where(x => x.UserName == userRegistration.Username).Count() != 0) {
                    throw new Exception("Username already in use");
                }
            }
            if (!IsValidPassword(userRegistration.Password)) {
                throw new Exception("Failed to create account");
            }

            if (!IsValidEmail(userRegistration.Email)) {
                throw new Exception("Failed to create account");
            }

            lock (context) {
                if (context.Users.Where(x => x.Email == userRegistration.Email).Count() != 0) {
                    throw new Exception("Email already in use");
                }
            }
            User user = new User()
            {
                UserName = userRegistration.Username,
                Email = userRegistration.Email,
                Role = Role.Regular,
                TrackLocation = userRegistration.TrackLocation,
                Salt = Guid.NewGuid(),
                Height = userRegistration.Height,
                Weight = userRegistration.Weight,
                Sex = userRegistration.Sex,
                DateOfBirth = userRegistration.DateOfBirth,
                HomeAddress = userRegistration.HomeAddress,
                WorkAddress = userRegistration.WorkAddress
            };

            byte[] passwordBytes = Encoding.UTF8.GetBytes(userRegistration.Password);
            byte[] hashCode = BitConverter.GetBytes(user.Salt.GetHashCode());

            SHA256 hasher = SHA256.Create();
            byte[] hashedPassword = hasher.ComputeHash(passwordBytes).Concat(hashCode).ToArray();
            user.HashedPassword = authenticationService.HashPassword(userRegistration.Password, user.Salt);
            user.Triggers = new List<Trigger>() {
                new DistanceTrigger() { Priority = configurationService.QuestionnaireConfiguration.TriggerConfigurations.DistanceConfiguration.TriggerConfiguration.Priority, TriggerId = Guid.NewGuid(), UserId = user.Id, User = user },
                new ExposureTrigger() { Priority = configurationService.QuestionnaireConfiguration.TriggerConfigurations.ExposureConfiguration.TriggerConfiguration.Priority, TriggerId = Guid.NewGuid(), UserId = user.Id, User = user },
                new DefaultTrigger() { Priority = configurationService.QuestionnaireConfiguration.TriggerConfigurations.DefaultConfiguration.TriggerConfiguration.Priority, TriggerId = Guid.NewGuid(), UserId = user.Id, User = user }
            };

            // Save new user
            lock (context) {
                context.Users.Add(user);
                context.SaveChanges();
            }
            return user;
        }

        /// <summary>
        /// Updates the user profile with new or edited information.
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> of the user.</param>
        /// <param name="profile"><see cref="UserProfile"/> carrying information.</param>
        /// <returns><see cref="UserProfile"/> if update was successful. <see langword="null"/> if unsuccessful.</returns>
        public UserProfile UpdateUserProfile(Guid userId, UserProfile profile) {
            // Verify User exists.
            if (!UserExists(userId))
                throw new Exception("Failed to update profile.");
            User user;
            lock (context) {
                user = context.Users.Find(userId);
            }

            // Validate the proposed update profile, if any field does not meet requirements the user will not be updated.
            if (!IsValidProfile(profile)) {
                throw new Exception("Failed to update profile.");
            }

            // Email is only allowed to exist once in the database. If count of an email is not 0 and not already registered to User account, the request is rejected.
            lock (context) {
                int countEmail = context.Users.Where(x => x.Email == profile.Email).Count();
                if (countEmail != 0) {
                    if (user.Email == profile.Email && countEmail != 1 || user.Email != profile.Email) {
                        throw new Exception("Email already in use.");
                    }
                }
            }
            // Update the user information.
            user.Email = profile.Email;
            user.TrackLocation = profile.TrackLocation;
            user.Weight = profile.Weight;
            user.Height = profile.Height;
            user.Sex = profile.Sex;
            user.DateOfBirth = profile.DateOfBirth;
            user.HomeAddress = profile.HomeAddress;
            user.WorkAddress = profile.WorkAddress;

            // Finalize successful update.
            lock (context)
                context.SaveChanges();

            return profile;
        }


        /// <summary>
        /// Updates <see cref="User.HashedPassword"/> if <see cref="PasswordPair"/> satisfies requirements.
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> of the <see cref="User"/>.</param>
        /// <param name="passwordPair"><see cref="PasswordPair"/> containing <see cref="PasswordPair.OldPassword"/> for verification, and <see cref="PasswordPair.NewPassword"/> to set new <see cref="User.HashedPassword"/>.</param>
        /// <returns><see cref="PasswordPair"/> if Update was successful. <see langword="null"/> if Update failed."/></returns>
        public PasswordPair UpdatePassword(Guid userId, PasswordPair passwordPair) {
            // Verify User exists.
            if (!UserExists(userId))
                throw new Exception("Failed to update password.");

            User user = context.Users.Find(userId);

            // Verify old password is equal to existing password.
            byte[] hashedOldPassword = authenticationService.HashPassword(passwordPair.OldPassword, user.Salt);
            if (Encoding.UTF8.GetString(hashedOldPassword) != Encoding.UTF8.GetString(user.HashedPassword))
                throw new Exception("Current password does not match.");

            // Validate new password.
            if (!IsValidPassword(passwordPair.NewPassword))
                throw new Exception("New password is invalid.");

            // Update hashedpassword.
            user.Salt = Guid.NewGuid();
            user.HashedPassword = authenticationService.HashPassword(passwordPair.NewPassword, user.Salt);
            context.SaveChanges();

            return passwordPair;
        }

        /// <summary>
        /// Removes all files corresponding to a user.
        /// </summary>
        /// <param name="userId">The <see cref="Guid"/> of the user to be removed.</param>
        /// <returns>A boolean whether the deletion was successful.</returns>
        public bool DeleteEntireUser(Guid userId) {
            if (userId == null) {
                return false;
            }

            User userToBeDeleted = context.Users.Find(userId);
            if (userToBeDeleted == null) {
                return false;
            }

            //context.Users.p
            context.Users.Remove(userToBeDeleted);
            context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Get a range of daily exposures of a user
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> id of the <see cref="User"/>.</param>
        /// <param name="dateFrom">Inclusive starting date</param>
        /// <param name="dateTo">Inclusive ending date</param>
        /// <returns>A range of daily exposures, null if dates are invalid</returns>
        public ICollection<DailyExposure> GetDailyExposure(Guid userId, DateTime dateFrom, DateTime dateTo) {
            // Limit the range to 31 days
            if ((dateTo - dateFrom).Days > 31) {
                return null;
            }

            // DateTo happens before dateFrom
            if (dateTo < dateFrom) {
                return null;
            }

            User user = context.Users.Find(userId);

            if (user == null) {
                return null;
            }

            return context.DailyExposures.Where(x => x.UserId == userId && x.Date >= dateFrom && x.Date <= dateTo).ToList();
        }

        /// <summary>
        /// Retrieve <see cref="UserProfile"/> as subsection of <see cref="User"/> from <see cref="DatabaseContext"/>.
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> as identification of user.</param>
        /// <returns><see cref="UserProfile"/> if update was successful. <see langword="null"/> if unsuccessful.</returns>
        public UserProfile GetUserProfile(Guid userId) {
            // Verify User exists.
            if (!UserExists(userId))
                return null;
            User user = context.Users.Find(userId);

            return new UserProfile() {
                Username = user.UserName,
                Email = user.Email,
                TrackLocation = user.TrackLocation,
                Height = user.Height,
                Weight = user.Weight,
                Sex = user.Sex,
                DateOfBirth = user.DateOfBirth,
                HomeAddress = user.HomeAddress,
                WorkAddress = user.WorkAddress
            };
        }

        private bool IsValidEmail(string email) {
            if (!email.Contains('@') || email.Length < 6 || email.Length > 254)
                return false;
            try {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException) {
                return false;
            }
        }

        /// <summary>
        /// Private validator for <see cref="UserProfile"/> that verifies if parameters satisfy requirements.
        /// </summary>
        /// <param name="profile">A param containing: name, email, birthdate.</param>
        /// <returns>True if params are valid for UserProfile.</returns>
        private bool IsValidProfile(UserProfile profile) {
            if (profile == null)
                return false;
            if (profile.Email == null)
                return false;

            string email = profile.Email;

            if (!IsValidEmail(email))
                return false;

            return true;
        }

        /// <summary>
        /// Private validator for an unhashed password to verify it meets password standards.
        /// </summary>
        /// <param name="password"><see langword="string"/> of an unhashed password.</param>
        /// <returns><see langword="true"/> if <paramref name="password"/> is valid.</returns>
        private bool IsValidPassword(string password) {
            // Password can not be null.
            if (password == null)
                return false;

            // Password must be between 8 and 256 characters.
            if (password.Length < 8 || password.Length > 256)
                return false;

            return true;
        }

        private bool UserExists(Guid userId) {
            if (context.Users.Find(userId) == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Generates a reset token and emails it. Used to reset a <see cref="User"/>'s password.
        /// </summary>
        /// <param name="email">Email address registered to the <see cref="User"/> account.</param>
        /// <returns><see cref="PasswordResetToken"/> if successful. <see langword="null"/> otherwise.</returns>
        public PasswordResetToken GenerateResetToken(string email) {
            if (!IsValidEmail(email)) { throw new Exception("Email is invalid."); }
            // Verify user exists.
            User user = context.Users.FirstOrDefault(u => (u.Email == email));
            if (user == null) { throw new Exception("Could not generate a reset token."); }

            // Verify user has not already attempted to reset their password recently.
            PasswordResetToken passwordResetToken = user.PasswordResetToken;
            // Return if token already exists && it has not yet expired. (User requested a new token while one is still valid, this is denied.)
            if (passwordResetToken != null && DateTime.Compare(passwordResetToken.ExpirationDate, DateTime.UtcNow) > 0) {
                throw new Exception("Could not generate a reset token.");
            }

            // Save token to database along with expirationdate and the user id it is related to.
            passwordResetToken = new PasswordResetToken() {
                Token = RandomString(),
                UserId = user.Id,
                ExpirationDate = DateTime.UtcNow.AddHours(2) // User has 2 hours to reset their password.
            };
            user.PasswordResetToken = passwordResetToken;
            // Save changes to generate a token in the database.            
            context.SaveChanges();
            string token = user.PasswordResetToken.Token;

            // Email the token.
            string body = "<html> <body> <p> Dear " + user.UserName + ",<br/> We received a request to change your password.</p>" +
                "<p>You require the following token to change your password:" +
                "<div style=\"font-size: 1.5em;\"><pre>" + token.ToString() + "</pre></div>" +
                "This token lasts for <b>2 hours</b>. " +
                "You can change your MEES password at <a href=\"https://mees.uu.nl/account-recovery\">https://mees.uu.nl/account-recovery</a>" +
                "<br/></p>" +
                "<p> With kind regards, <br/>Institute for Risk Assessment Sciences (IRAS)<br/>" +
                "<i>If this request was not performed by you, you may ignore this email.</i>" +
                "</p></body></html>";
            bool emailResult = emailService.SendEmail(user.Email, "MEES forgot password.", body);

            // Verify email was sent successfully.
            if (!emailResult) {
                // Sending email failed, we remove the reset token.
                user.PasswordResetToken = null;
                context.SaveChanges();
                throw new Exception("Could not generate a reset token.");
            }

            context.SaveChanges();
            return user.PasswordResetToken;
        }

        private string RandomString() {
            Random random = new Random();
            string resultString = "";
            for (int i = 0; i < 8; i++) {
                char[] chars = new char[2]{(char)random.Next('A', 'Z'), (char)random.Next('a', 'z') };

                resultString += chars[random.Next(0, 2)];
            }

            return resultString;
        }
        /// <summary>
        /// Resets the <see cref="User"/>'s password.
        /// </summary>        
        /// <param name="tokenString">Previously generated token to authenticate request.</param>
        /// <param name="newPassword">New password for the <see cref="User"/>.</param>
        /// <param name="confirmPassword">Password used to decrease chance user made a typo.</param>
        /// <returns>True if email was sent and update successful.</returns>
        public bool ResetPassword(string token, string newPassword, string confirmPassword) {

            if (token == null) { throw new Exception("Token is of wrong format."); }

            // Find user related to token.
            User user = context.Users.FirstOrDefault(u => u.PasswordResetToken != null && u.PasswordResetToken.Token == token);
            if (user == null) { throw new Exception("Token is invalid."); }

            // Verify token has not expired. 
            // If now is after expirationdate == now > expirationdate. We return, because token has expired.
            if (DateTime.Compare(DateTime.UtcNow, user.PasswordResetToken.ExpirationDate) > 0) { throw new Exception("Token has expired."); ; }

            // Match and validate new password.
            if (!IsValidPassword(newPassword) || newPassword != confirmPassword) { throw new Exception("New password is invalid."); }

            // Update hashedpassword.
            user.Salt = Guid.NewGuid();
            user.HashedPassword = authenticationService.HashPassword(newPassword, user.Salt);
            context.SaveChanges();

            return true;
        }

        /// <summary>
        /// Get the restrictions of the front-end features for a user, based on the research the user is in.
        /// </summary>
        /// <param name="researchId">The id of the requested research.</param>
        /// <returns></returns>
        public Researches GetResearchRestrictions(Guid userId) {
            Guid researchId;
            DateTime? endTime;
            Researches research;
            try {
                if (context.UserResearches.Where(u => u.userId == userId).Count() == 0) {
                    research = context.Researches.FirstOrDefault(r => r.Name == "No research");
                }
                else {
                    researchId = context.UserResearches.OrderByDescending(t => t.startTime).FirstOrDefault(u => u.userId == userId).researchId; // Needs to take the last, but Last<> and LastOrDefault<> are not supported by LINQ or something
                    endTime = context.UserResearches.OrderByDescending(t => t.startTime).FirstOrDefault(u => u.userId == userId).endTime;
                    research = context.Researches.FirstOrDefault(r => r.Name == "No research");

                    if (endTime > DateTime.UtcNow) {
                        research = context.Researches.FirstOrDefault(r => r.Id == researchId);
                    }
                }
            }
            catch (Exception) {
                throw new Exception("User isn't known in the database");
            }

            if (research == null) {
                throw new Exception("No such research found");
            }

            return research;
        }
    }
}
