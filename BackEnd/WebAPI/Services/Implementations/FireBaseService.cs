/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Services.Interfaces;
using Domain.Administration;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Services.Implementations {
    public class FireBaseService : IFireBaseService {

        private DatabaseContext context;

        public FireBaseService(DatabaseContext context) {
            this.context = context;

            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "FireBase Service Thread";
        }
        public void AddFireBaseToDatabase(FireBase fireBase) {

            lock (context) {
                //Only add firebase to database if it's not in there already
                if (context.FireBases.Where((x) => x.FireBaseID == fireBase.FireBaseID).Count() == 0) {

                    if (context.Users.Where((x) => x.Id == fireBase.UserID).Count() == 0)
                        throw new Exception("User with this userID doesn't exist");
                    //Find user and link them
                    fireBase.User = (from correspondingUser in context.Users
                                     where correspondingUser.Id == fireBase.UserID
                                     select correspondingUser).FirstOrDefault();
                    context.FireBases.Add(fireBase);
                    context.SaveChanges();
                }
            }
        }
    }
}
