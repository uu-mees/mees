/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.Threading;
using Services.Interfaces;
using Domain.Administration;
using DataAccess;
using Microsoft.Extensions.Logging;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IAuthenticationService"/>.
    /// </summary>
    public class AuthenticationService : IAuthenticationService {
        private readonly DatabaseContext context;
        private ILogger<AuthenticationService> logger;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        public AuthenticationService(DatabaseContext context, ILogger<AuthenticationService> logger) {
            this.context = context;
            this.logger = logger;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Authentication Service Thread";
        }

        /// <summary>
        /// Verify if the user/password combination is correct.
        /// </summary>
        /// <param name="username">The username to be used for logging in.</param>
        /// <param name="password">The password to be verified.</param>
        /// <returns>
        /// The <see cref="User"/> object corresponding to user logged in.
        /// </returns>
        public virtual User CheckLogin(string username, string password) {
            User result;
            lock (context) {
                result = context.Users.FirstOrDefault(x => x.UserName == username);
            }
            if (result == null)
                return result;

            byte[] hashedPassword = HashPassword(password, result.Salt);
            if (Encoding.UTF8.GetString(hashedPassword) != Encoding.UTF8.GetString(result.HashedPassword))
                return null;

            return result;
        }

        /// <summary>
        /// Hash the given password with a salt.
        /// </summary>
        /// <param name="password">The password to be hashed.</param>
        /// <param name="salt">The salt to be used for hashing.</param>
        /// <returns>The hashed password.</returns>
        public byte[] HashPassword(string password, Guid salt) {
            SHA256 hasher = SHA256.Create();
            return hasher.ComputeHash(Encoding.UTF8.GetBytes(password)).Concat(BitConverter.GetBytes(salt.GetHashCode())).ToArray();
        }


    }
}
