/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading;
using Utils;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IRatingService"/>.
    /// </summary>
    public class RatingService : IRatingService {
        private DatabaseContext context;
        private ILogger<RatingService> logger;

        /// <summary>
        /// The constructor of the service.
        /// </summary>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        public RatingService(DatabaseContext context, ILogger<RatingService> logger) {
            this.context = context;
            this.logger = logger;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Rating Service Thread";
        }

        /// <summary>
        /// Validates <see cref="UserRating"/> and adds it to database.
        /// </summary>
        /// <param name="rating">The <see cref="UserRating"/> to add to database.</param>
        /// <returns>
        /// True if <see cref="UserRating"/> was correctly validated 
        /// and added to database, and false if not.
        /// </returns>
        public UserRating AddRating(UserRating rating) {
            // TODO: Add more constraints
            if (rating.Rating < 1 || rating.Rating > 5 || rating.Rating % 0.5f != 0)
                return null;

            lock (context) {
                UserRating addedRating = context.UserRatings.Add(rating);
                context.SaveChanges();

                return addedRating;
            }
        }

        /// <summary>
        /// Gets all <see cref="UserRating"/>s within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>All <see cref="UserRating"/>s lying within a circle around a certain position.</returns>
        public ICollection<UserRating> GetRatings(GeoCoordinate requestedPosition, float radius) {
            // Query all ratings from database within a square defined by the requestedPosition and radius
            GeoCoordinate southWestPoint = GeoUtils.CalculateDerivedPosition(requestedPosition, radius, 215);
            GeoCoordinate northEastPoint = GeoUtils.CalculateDerivedPosition(requestedPosition, radius, 45);

            lock (context) {
                List<UserRating> ratings = (from ur in context.UserRatings
                                            where ur.Latitude >= southWestPoint.Latitude &&
                                                            ur.Longitude >= southWestPoint.Longitude &&
                                                            ur.Latitude <= northEastPoint.Latitude &&
                                                            ur.Longitude <= northEastPoint.Longitude
                                            select ur).ToList();
                return ratings.Where(x => RatingInRadius(x, requestedPosition, radius)).ToList();
            }
        }

        /// <summary>
        /// Gets all <see cref="UserRating"/>s posted after a certain time.
        /// </summary>
        /// <param name="timeFrom">Start of the interval.</param>
        /// <returns>All <see cref="UserRating"/>s posted after a certain time.</returns>
        public ICollection<UserRating> GetNewRatings(DateTime timeFrom) {
            lock (context) {
                List<UserRating> ratings = (from ur in context.UserRatings
                                            where ur.TimeStamp > timeFrom
                                            select ur).ToList();

                return ratings;
            }
        }

        /// <summary>
        /// Gets all <see cref="UserRating"/>s.
        /// </summary>
        /// <returns>All <see cref="UserRating"/>s.</returns>
        public ICollection<UserRating> GetAllRatings() {
            lock (context) {
                return context.UserRatings.ToList();
            }
        }

        /// <summary>
        /// Gets an average of all <see cref="UserRating"/>s within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>Returns average rating of all <see cref="UserRating"/>s within a certain circle.
        /// Returns 0 if no <see cref="UserRating"/>s were found.</returns>
        public double GetAverageRating(GeoCoordinate requestedPosition, float radius) {
            ICollection<UserRating> ratings = GetRatings(requestedPosition, radius);

            if (ratings.Count == 0)
                return 0;

            return ratings.Average(x => x.Rating);
        }

        /// <summary>
        /// Gets all <see cref="UserRating"/>s and their amount of appearance within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>A dictionary with ratings as keys and their amount of appearance as values.</returns>
        public Dictionary<float, int> GetRatingNumbers(GeoCoordinate requestedPosition, float radius) {
            ICollection<UserRating> ratings = GetRatings(requestedPosition, radius);

            // Group ratings and project the rating and count of that rating to KeyValuePairs
            IEnumerable<KeyValuePair<float, int>> ratingNumbers = ratings.GroupBy(x => x.Rating)
                                                                         .Select(y => new KeyValuePair<float, int>(y.Key, y.Count()));

            return ratingNumbers.ToDictionary(x => x.Key, x => x.Value);
        }

        private bool RatingInRadius(UserRating rating, GeoCoordinate requestedPosition, float radius) {
            GeoCoordinate ratingPosition = new GeoCoordinate(rating.Latitude, rating.Longitude);

            bool dist = ratingPosition.GetDistanceTo(requestedPosition) < radius;

            return ratingPosition.GetDistanceTo(requestedPosition) < radius;
        }
    }
}
