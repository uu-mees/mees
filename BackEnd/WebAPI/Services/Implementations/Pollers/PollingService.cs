/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Threading;
using Services.Interfaces;
using Services.Retrievers;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IPollingService{T}"/>.
    /// </summary>
    public class PollingService<T> : IPollingService<T> {
        private event EventHandler<T> pollEvent;
        private IRetriever<T> retriever;
        private Thread pollThread;

        /// <summary>
        /// Start polling data using the given retriever indefinitely.
        /// </summary>
        /// <param name="interval">The poll interval in seconds.</param>
        /// <param name="eventHandler">
        /// The <see cref="EventHandler"/> which will be invoked when data is polled.
        /// </param>
        /// <param name="retriever">An instance of the retriever to be used.</param>
        public void StartPolling(int interval, EventHandler<T> eventHandler, IRetriever<T> retriever) {
            this.retriever = retriever;

            pollEvent += eventHandler;
            pollThread = new Thread(() => PollData(interval));
            pollThread.Name = retriever.ToString();
            pollThread.Start();
        }

        /// <summary>
        /// Poll the data every interval.
        /// </summary>
        /// <param name="interval">
        /// The interval with which the data should be retrieved.
        /// </param>
        private void PollData(int interval) {
            while (true) {
                pollEvent.Invoke(this, retriever.RetrieveData());
                Thread.Sleep(interval * 1000);
            }
        }
    }
}
