/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Exposure;
using EntityFramework.BulkInsert.Extensions;
using Microsoft.Extensions.Logging;
using Services.Interfaces;
using Services.Interfaces.Pollers;
using Services.Retrievers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="IRasterDataInjectionService"/>.
    /// </summary>
    /* This class is excluded from code coverage because its sole purpose is to insert data into the context,
     * which is essentially impossible to Assert. It is basically a massive converter that takes raster data 
     * and performs mostly trivial conversions, the only calculations that are performed are from services
     * which are separately tested.
     */
    [ExcludeFromCodeCoverage]
    public class RasterDataInjectionService : IRasterDataInjectionService {
        private ConfigurationService configurationService;
        private ILogger<RasterDataInjectionService> logger;
        private IRasterContainer rasterContainer;
        private static int interval = 600;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="configurationService">
        /// A <see cref="ConfigurationService"/> instance specifying multiple configuration options.
        /// </param>
        /// <param name="pollingService">
        /// The <see cref="PollingService{T}"/> instance to be used for polling.
        /// </param>
        public RasterDataInjectionService(ConfigurationService configurationService, IPollingService<Raster> pollingService,
            ILogger<RasterDataInjectionService> logger, IRasterContainer rasterContainer) {
            this.configurationService = configurationService;
            this.logger = logger;
            this.rasterContainer = rasterContainer;
            pollingService.StartPolling(interval, HandlePoll, new RasterDataRetriever(configurationService, logger));
        }

        /// <summary>
        /// Write the polled data to the database if relevant.
        /// </summary>
        /// <param name="sender">The object calling the function.</param>
        /// <param name="raster">The previous <see cref="Raster"/> stored in the database.</param>
        public void HandlePoll(object sender, Raster raster) {
            if (raster == null)
                return;

            // Inject into database if different from the previous one or if previous one does not exist.
            using (DatabaseContext context = new DatabaseContext(configurationService.ConnectionString)) {
                if (context.Database.Exists()) {
                    //This setting is temporarily set to false as it significantly slows down the BulkInsert
                    context.Configuration.AutoDetectChangesEnabled = false;
                    try {
                        context.ChangeTracker.DetectChanges();
                    }
                    catch (Exception) {
                        Console.WriteLine("Database connection already in use");
                        return;
                    }
                    Raster latest = GetLatestRaster(context);
                    if (latest == null || latest.HashCode != raster.HashCode) {
                        int getNrRaster = context.Rasters.Count();
                        Raster oldest;
                        while (getNrRaster >= 48) {                  //48 equals to the maximum amount of rasters stored in the database. 48 equals to 1~2 day(s)
                            oldest = GetOldestRaster(context);
                            context.Rasters.Remove(oldest);
                            context.RasterCells.RemoveRange(context.RasterCells.Where(x => x.RasterId == oldest.Id));
                            getNrRaster--;
                        }

                        // Decouple the rastercells from the raster so we can insert just the raster.
                        List<RasterCell> cells = new List<RasterCell>(raster.Cells);
                        raster.Cells.Clear();

                        context.Rasters.Add(raster);
                        context.SaveChanges();
                        // We get the new raster because its ID is generated in the database and we need the ID
                        // to re-establish the connection between the cells and the raster before inserting them.
                        //raster = GetLatestRaster(context);
                        int rasterId = GetLatestRaster(context).Id;
                        foreach (RasterCell cell in cells) {
                            cell.RasterId = rasterId;
                            cell.Raster = raster;
                        }

                        // Use the bulk insert plugin to insert the cells. Using just EF results in extremely poor performance.
                        try {
                            context.ChangeTracker.DetectChanges();
                            context.BulkInsert(cells);
                            context.SaveChanges();
                        }
                        catch (Exception) {
                            context.Rasters.Remove(raster);
                            throw new Exception("Bulk Insert failed. Throwing away newest raster as it cannot be updated");
                        }

                        raster.Cells = cells;

                        rasterContainer.UpdateRaster(raster);

                    }
                    else { // TODO: Check if this is needed!!!!!!!
                        int getNrRaster = context.Rasters.Count();
                        Raster oldest;
                        while (getNrRaster > 48) {                  //48 equals to the maximum amount of rasters stored in the database. 48 equals to 2 days
                            oldest = GetOldestRaster(context);
                            context.Rasters.Remove(oldest);
                            context.RasterCells.RemoveRange(context.RasterCells.Where(x => x.RasterId == oldest.Id));
                            getNrRaster--;
                        }
                    }

                    // If the back-end restarts we might poll a raster that is already in the database, in that case 
                    // it is not saved again but we still need to update the rastercontainer.
                    if (rasterContainer.GetRaster() == null) {
                        latest.Cells = context.RasterCells.Where(x => x.RasterId == latest.Id).ToList();

                        if (latest.Width * latest.Height == latest.Cells.Count)
                            rasterContainer.UpdateRaster(latest);
                        else {
                            Console.WriteLine("Latest raster's amount of cells does not match with given width and height. Throwing it away");
                            context.Rasters.Remove(GetLatestRaster(context));

                            latest = GetLatestRaster(context);
                            latest.Cells = context.RasterCells.Where(x => x.RasterId == latest.Id).ToList();
                            rasterContainer.UpdateRaster(latest);
                        }
                    }

                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Retrieves the newest raster from the database.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Raster GetLatestRaster(DatabaseContext context) {
            return context.Rasters
                .OrderByDescending(r => r.TimeStampPolled)
                .FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the oldest raster from the database.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private Raster GetOldestRaster(DatabaseContext context) {
            return context.Rasters
                .OrderByDescending(r => r.TimeStampPolled)
                .ToList().LastOrDefault();
        }
    }
}
