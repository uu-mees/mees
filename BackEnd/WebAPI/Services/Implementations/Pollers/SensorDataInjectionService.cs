/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using Services.Interfaces;
using DataAccess;
using Services.DTOs;
using Services.Retrievers;
using Domain.Exposure;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;

namespace Services.Implementations {
    /// <summary>
    /// The implementation of <see cref="ISensorDataInjectionService"/>.
    /// </summary>
    /* This class is excluded from code coverage because its sole purpose is to insert data into the context,
     * which is essentially impossible to Assert. It is basically a massive converter that takes sensor data 
     * and performs mostly trivial conversions, the only calculations that are performed are from services 
     * which are separately tested. 
     */
    [ExcludeFromCodeCoverage]
    public class SensorDataInjectionService : ISensorDataInjectionService {
        private ConfigurationService configurationService;
        private IPollingService<List<DataLink>> pollingService;
        private SensorDataRetriever retriever;
        private static int interval = 600;
        private ILogger<SensorDataInjectionService> logger;

        /// <summary>
        /// The constructor for the service.
        /// </summary>
        /// <param name="configurationService">
        /// A <see cref="ConfigurationService"/> instance specifying multiple configuration options.
        /// </param>
        /// <param name="pollingService">
        /// The <see cref="PollingService{T}"/> instance to be used for polling.
        /// </param>
        public SensorDataInjectionService(ConfigurationService configurationService,
            IPollingService<List<DataLink>> pollingService,
            ILogger<SensorDataInjectionService> logger) {
            this.configurationService = configurationService;
            this.pollingService = pollingService;
            this.logger = logger;

            retriever = new SensorDataRetriever(configurationService, logger);

            pollingService.StartPolling(interval, HandlePoll, retriever);
        }

        /// <summary>
        /// Write the polled data to the database if relevant.
        /// </summary>
        /// <param name="sender">The object calling the function.</param>
        /// <param name="dataLinks">
        /// The previous list of <see cref="DataLink"/>s stored in the database.
        /// </param>
        private void HandlePoll(object sender, List<DataLink> dataLinks) {
            if (dataLinks == null)
                return;

            // Retrieve last 5 XML files (to simulate historical data).
            // FIXME: Do not only retrieve last 5 XML files.
            int filesBack = 5;
            for (int i = dataLinks.Count - (filesBack + 1); i <= dataLinks.Count - 1; i++) {
                DataLink dataLink = dataLinks[i];

                // Retrieve, parse and inject latest XML file.
                SensorDataDTO sensorDataDTO = null;
                try {
                    sensorDataDTO = retriever.RetrieveSensorData(dataLink);
                }
                catch (Exception e) {
                    Console.WriteLine("Exception occurred while parsing XML file from link {0}: {1}", dataLink.Link, e);
                    continue;
                }

                // No sensors in XML file.
                if (sensorDataDTO.Sensors.Length == 0) {
                    continue;
                }

                using (DatabaseContext context = new DatabaseContext(configurationService.ConnectionString)) {
                    if (context.Database.Exists()) {

                        try {
                            context.ChangeTracker.DetectChanges();
                        }
                        catch (Exception) {
                            Console.WriteLine("Database connection already in use");
                            return;
                        }
                        // Deal with duplicate XML files.
                        DateTime timeFrom = sensorDataDTO.Sensors[0].TimeStamp_From;
                        DateTime timeTo = sensorDataDTO.Sensors[0].TimeStamp_To;

                        //Don't want the database to just slowly clutter up the database with ages old data
                        //So it gets removed if it's older than a week;
                        DateTime latestDateToStore = DateTime.UtcNow.AddDays(-7);

                        List<SensorMeasurementBatch> outdatedBatches = (from sb in context.SensorMeasurementBatches
                                                                        where (sb.TimeStampFrom == timeFrom && sb.TimeStampTo == timeTo) || sb.TimeStampFrom < latestDateToStore
                                                                        select sb).ToList();

                        if (outdatedBatches.Count() == 0) {
                            // No duplicate batches.
                            ConvertDTO(sensorDataDTO, context);
                        }
                        else if (outdatedBatches.First().TimeStampPolled < sensorDataDTO.TimeStampPolled) {
                            // Duplicate batches are older than current batches.
                            foreach (SensorMeasurementBatch batch in outdatedBatches) {
                                batch.Sensor.LastValidBatch = null;
                            }

                            context.SensorMeasurementBatches.RemoveRange(outdatedBatches);
                            context.SaveChanges();

                            ConvertDTO(sensorDataDTO, context);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Convert <see cref="SensorDataDTO"/> into the correct Domain objects and inject those into the database.
        /// </summary>
        /// <param name="sensorDataDTO">The DTO to be converted.</param>
        /// <param name="context">
        /// A <see cref="DatabaseContext"/> object for interfacing with the database.
        /// </param>
        private void ConvertDTO(SensorDataDTO sensorDataDTO, DatabaseContext context) {
            //Assumption: the two sensor polls are always next to each other, with the newer one first.
            for (int i = 1; i < sensorDataDTO.Sensors.Length; i += 2)
            //foreach (SensorDTO sensorDTO in sensorDataDTO.Sensors)
            {
                SensorDTO sensorDTO = sensorDataDTO.Sensors[i];
                // Create new sensor if the sensor does not exist.
                if (context.Sensors.FirstOrDefault(x => x.Id == sensorDTO.Id) == null) {
                    Sensor s = new Sensor
                    {
                        Id = sensorDTO.Id,
                        Label = sensorDTO.Label,
                        Latitude = sensorDTO.Latitude,
                        Longitude = sensorDTO.Longitude,
                        LastValidBatch = null,
                        Batches = new List<SensorMeasurementBatch>()
                    };
                    context.Sensors.Add(s);
                    context.SaveChanges();
                }

                Sensor sensor = context.Sensors.FirstOrDefault(x => x.Id == sensorDTO.Id);

                // Create a new batch for the sensor if the batch does not exist.
                if (context.SensorMeasurementBatches.FirstOrDefault(x => (x.SensorId == sensor.Id && x.TimeStampPolled == sensorDataDTO.TimeStampPolled)) == null) {
                    SensorMeasurementBatch b = new SensorMeasurementBatch()
                    {
                        SensorId = sensorDTO.Id,
                        TimeStampFrom = sensorDTO.TimeStamp_From,
                        TimeStampTo = sensorDTO.TimeStamp_To,
                        TimeStampPolled = sensorDataDTO.TimeStampPolled,
                        Sensor = sensor,
                    };
                    context.SensorMeasurementBatches.Add(b);
                    context.SaveChanges();
                }

                SensorMeasurementBatch batch = context.SensorMeasurementBatches.FirstOrDefault(
                    x => (x.SensorId == sensor.Id &&
                    x.TimeStampPolled == sensorDataDTO.TimeStampPolled));

                bool validBatch = false;

                foreach (MeasurementDTO measurementDTO in sensorDTO.Measurements) {
                    // Create a new component if the component does not exist.
                    if (context.SensorComponents.FirstOrDefault(x => x.Component == measurementDTO.Component && x.Unit == measurementDTO.Unit) == null) {
                        SensorComponent c = new SensorComponent()
                        {
                            Component = measurementDTO.Component,
                            Unit = measurementDTO.Unit,
                            Measurements = new List<SensorMeasurement>(),
                        };
                        context.SensorComponents.Add(c);
                        context.SaveChanges();
                    }

                    SensorComponent sensorComponent = context.SensorComponents.FirstOrDefault(
                        x => x.Component == measurementDTO.Component &&
                        x.Unit == measurementDTO.Unit);

                    // Create a new measurement.
                    SensorMeasurement sensorMeasurement = new SensorMeasurement()
                    {
                        Value = measurementDTO.Value,
                        Batch = batch
                    };

                    // Add the SensorMeasurement to the corresponding SensorComponent.
                    sensorComponent.Measurements.Add(sensorMeasurement);

                    // Add the SensorMeasurement to the database and save the change.
                    context.SensorMeasurements.Add(sensorMeasurement);
                    context.SaveChanges();

                    // If at least one of the components still work, consider it valid.
                    if (measurementDTO.Value == "None" || measurementDTO.Value == "-999.0") {
                        validBatch = true;
                    }
                }

                if (validBatch) {
                    // Keep track of a sensor's last valid batch, to filter out faulty sensors when sending data to front-end.
                    sensor.LastValidBatch = batch;
                }

                context.SaveChanges();
            }
        }
    }
}
