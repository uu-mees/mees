/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Tracking;
using Services.Interfaces;
using System;
using System.Device.Location;
using System.Linq;
using System.Threading;
using System.Diagnostics.CodeAnalysis;
using Domain.Administration;
using Domain.Exposure;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Services.Implementations {
    //All these functions require reading the context to check the results. 
    //This is done with integration testing and can't be unit tested
    [ExcludeFromCodeCoverage]
    public class LocationExposureService : ILocationExposureService {

        public DatabaseContext context;
        private ILogger<LocationExposureService> logger;
        private IExposureService exposureService;

        public LocationExposureService(DatabaseContext context, ILogger<LocationExposureService> logger, IExposureService exposureService) {
            this.context = context;
            this.logger = logger;
            this.exposureService = exposureService;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Location Exposure Service Thread";
        }


        /// <summary>
        /// Gets the corresponding exposures and adds the User
        /// </summary>
        /// <param name="locationExposure">A list of all recently retrieved locations and exposures</param>
        public void LinkUserAndExposuretoLocation(UserLocationExposure[] locationExposures) {
            if (locationExposures.Count() == 0) {
                throw new Exception("LocationExposure array is empty");
            }
            foreach (UserLocationExposure locationExposure in locationExposures) {

                //If an empty userId was sent we can't process it and just skip it
                if (locationExposure.UserID == null)
                    continue;

                lock (context) {
                    try {
                        locationExposure.User = context.Users.FirstOrDefault(u => u.Id == locationExposure.UserID);
                    }
                    catch (Exception) {
                        throw new Exception("Can't find users in database");
                    }

                    //Get the exposure
                    DateTime locationDate = locationExposure.Timestamp;
                    Raster correctRaster = context.Rasters.OrderByDescending(r => r.TimeStampPolled).FirstOrDefault(r => r.TimeStampPolled <= locationDate);
                    if(correctRaster.TimeStampPolled.AddHours(2) < DateTime.UtcNow) {
                        continue;
                    }
                    locationExposure.Exposure = exposureService.GetExposureFromRaster(new GeoCoordinate(locationExposure.Position.Latitude, locationExposure.Position.Longitude), correctRaster);
                }

                if (locationExposure.User == null) {
                    throw new Exception("User doesn't exist");
                }
                lock (context)
                    context.UserLocationExposures.Add(locationExposure);

            }

            lock (context) {
                try {
                    context.SaveChanges();
                }
                catch (Exception) {
                    throw new Exception("Changes can't be saved(Database likely down or broken)");
                }
            }
            //After having received all the data the daily exposures can be updated. 
            //This is done after every packet it receives instead of once every day to avoid having the server be overloaded with work at the end of a day
            CalculateDailyExposures(locationExposures);
        }


        /// <summary>
        /// Calculates the DailyExposure within the new package
        /// </summary>
        /// <param name="locationExposures"></param>
        /// <param name="user"></param>
        public void CalculateDailyExposures(UserLocationExposure[] locationExposures) {
            if (locationExposures.Count() == 0)
                return;
            User user = locationExposures[0].User;
            DailyExposure currentDailyExposure;
            UserLocationExposure locationExposure;

            //For each location received, add it to the day and user it belongs to.
            for (int i = 0; i < locationExposures.Length; i++) {
                locationExposure = locationExposures[i];
                if (locationExposure.Exposure == null)
                    continue;
                //Find the DailyExposure for the right user for the right day
                currentDailyExposure = context.DailyExposures.FirstOrDefault(x => x.Date.Year == locationExposure.Timestamp.Year &&
                                                                                x.Date.Month == locationExposure.Timestamp.Month &&
                                                                                x.Date.Day == locationExposure.Timestamp.Day &&
                                                                                x.User.Id == locationExposure.UserID);
                //If it's a new day, create a new day and save it
                if (currentDailyExposure == null) {
                    currentDailyExposure = new DailyExposure {
                        User = user,
                        UserId = user.Id,
                        TotalDuration = (long)locationExposure.Timestamp.TimeOfDay.TotalSeconds,
                        AverageExposure = (double)locationExposure.Exposure,
                        Date = locationExposure.Timestamp.Date
                    };
                    lock (context) {
                        context.DailyExposures.Add(currentDailyExposure);
                        context.SaveChanges();
                    }
                    continue;
                }
                //Update the current day's duration and exposure
                double newTotalExposure = currentDailyExposure.TotalDuration * currentDailyExposure.AverageExposure +
                                        (locationExposure.Timestamp.TimeOfDay.TotalSeconds - currentDailyExposure.TotalDuration) * (double)locationExposure.Exposure;
                long newTotalDuration = (long)locationExposure.Timestamp.TimeOfDay.TotalSeconds;
                double newAverage = newTotalExposure / newTotalDuration;

                currentDailyExposure.AverageExposure = newAverage;
                currentDailyExposure.TotalDuration = newTotalDuration;
                lock (context)
                    context.SaveChanges();
            }
        }
    }
}
