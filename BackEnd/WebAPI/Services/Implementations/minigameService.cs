/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Domain.Minigames;
using DataAccess;


namespace Services.Implementations {
    public class MinigameService {
        private DatabaseContext context;

        public MinigameService(DatabaseContext context) {
            this.context = context;

            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Minigame Service Thread";
        }
        public MinigameScores saveMinigameScores(MinigameScores minigameScores) {
            lock (context) {
                // Save the minigameScore in the database
                context.MinigameScores.Add(minigameScores);
                context.SaveChanges();
                return minigameScores;
            }
        }
    }
}
