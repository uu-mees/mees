/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace Services.Converters {
    public class DateTimeConverter : IsoDateTimeConverter {
        public DateTimeConverter() {
            DateTimeStyles = DateTimeStyles.None;
            DateTimeFormat = "yyyyMMddTHHmmssK";
        }
    }

    public class DateConverter : IsoDateTimeConverter {
        public DateConverter() {
            DateTimeStyles = DateTimeStyles.None;
            DateTimeFormat = "yyyyMMdd";
        }
    }
}
