/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs.Routing;

namespace Services.Converters.Routing {
    /// <summary>
    /// A class for converting <see cref="GeocodingDTO"/>s.
    /// </summary>
    public static class GeocodingResponseConverter {
        /// <summary>
        /// Convert from <see cref="GeocodingDTO"/> to <see cref="GeocodingResponse"/>.
        /// </summary>
        /// <param name="geocodingDTO">The <see cref="GeocodingDTO"/> object to convert.</param>
        /// <returns>The resulting <see cref="GeocodingResponse"/>.</returns>
        public static GeocodingResponse FromDTO(this GeocodingDTO geocodingDTO) {
            return new GeocodingResponse() {
                Results = geocodingDTO.Results.ConvertAll(x => x.FromDTO())
            };
        }
    }
}
