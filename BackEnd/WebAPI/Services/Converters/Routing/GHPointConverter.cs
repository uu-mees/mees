/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System.Device.Location;

namespace Services.Converters.Routing {
    /// <summary>
    /// A class for converting <see cref="GHPoint"/>s.
    /// </summary>
    public static class GHPointConverter {
        /// <summary>
        /// Convert <see cref="GHPoint"/> to <see cref="GHPointDTO"/>.
        /// </summary>
        /// <param name="point">The <see cref="GHPoint"/> object to convert.</param>
        /// <returns>The resulting <see cref="GHPointDTO"/>.</returns>
        public static GHPointDTO ToDTO(this GHPoint point) {
            return new GHPointDTO() {
                Point = new double[2] { point.Coordinate.Longitude, point.Coordinate.Latitude },
                ElapsedTime = point.ElapsedTime
            };
        }

        /// <summary>
        /// Convert from <see cref="GHPointDTO"/> to <see cref="GHPoint"/>.
        /// </summary>
        /// <param name="pointDTO">The <see cref="GHPointDTO"/> object to convert.</param>
        /// <returns>The resulting <see cref="GHPoint"/>.</returns>
        public static GHPoint FromDTO(this GHPointDTO pointDTO) {
            return new GHPoint() {
                Coordinate = new GeoCoordinate(pointDTO.Point[1], pointDTO.Point[0]),
                ElapsedTime = pointDTO.ElapsedTime
            };
        }
    }
}
