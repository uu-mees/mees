/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;
using System.Device.Location;

namespace Services.Converters.Routing {
    /// <summary>
    /// A class for converting <see cref="Routes"/>s.
    /// </summary>
    public static class RoutesConverter {
        /// <summary>
        /// Convert <see cref="Routes"/> to <see cref="RoutingResponseDTO"/>.
        /// </summary>
        /// <param name="routes">The <see cref="Routes"/> object to convert.</param>
        /// <returns>The resulting <see cref="RoutingResponseDTO"/>.</returns>
        public static RoutingResponseDTO ToDTO(this Routes routes) {
            return new RoutingResponseDTO() {
                Start = new double[2] { routes.Start.Longitude, routes.Start.Latitude },
                Destination = new double[2] { routes.Destination.Longitude, routes.Destination.Latitude },
                Vehicle = routes.Vehicle,
                Weighting = routes.Weighting,
                Count = routes.Count,
                Routes = routes.RouteList.ConvertAll(x => x.ToDTO())
            };
        }

        /// <summary>
        /// Convert from <see cref="RoutingResponseDTO"/> to <see cref="Routes"/>.
        /// </summary>
        /// <param name="routingResponse">The <see cref="RoutingResponseDTO"/> object to convert.</param>
        /// <returns>The resulting <see cref="Routes"/>.</returns>
        public static Routes FromDTO(this RoutingResponseDTO routingResponse) {
            return new Routes() {
                Start = new GeoCoordinate(routingResponse.Start[1], routingResponse.Start[0]),
                Destination = new GeoCoordinate(routingResponse.Destination[1], routingResponse.Destination[0]),
                Vehicle = routingResponse.Vehicle,
                Weighting = routingResponse.Weighting,
                Count = routingResponse.Count,
                RouteList = routingResponse.Routes.ConvertAll(x => x.FromDTO())
            };
        }
    }
}
