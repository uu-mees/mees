/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs;

namespace Services.Converters.Routing {
    /// <summary>
    /// A class for converting <see cref="Route"/>s.
    /// </summary>
    public static class RouteConverter {
        /// <summary>
        /// Convert <see cref="Route"/> to <see cref="RouteResponseDTO"/>.
        /// </summary>
        /// <param name="route">The <see cref="Route"/> object to convert.</param>
        /// <returns>The resulting <see cref="RouteResponseDTO"/>.</returns>
        public static RouteResponseDTO ToDTO(this Route route) {
            return new RouteResponseDTO() {
                Duration = route.Duration,
                Distance = route.Distance,
                Path = route.Path.ConvertAll(x => x.ToDTO())
            };
        }

        /// <summary>
        /// Convert from <see cref="RouteResponseDTO"/> to <see cref="Route"/>.
        /// </summary>
        /// <param name="routeResponse">The <see cref="RouteResponseDTO"/> object to convert.</param>
        /// <returns>The resulting <see cref="Route"/>.</returns>
        public static Route FromDTO(this RouteResponseDTO routeResponse) {
            return new Route() {
                Distance = routeResponse.Distance,
                Duration = routeResponse.Duration,
                Path = routeResponse.Path.ConvertAll(x => x.FromDTO())
            };
        }
    }
}
