/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Services.DTOs.Routing;
using System.Device.Location;

namespace Services.Converters.Routing {
    /// <summary>
    /// A class for converting <see cref="GeocodingResult"/>s.
    /// </summary>
    public static class GeocodingResultConverter {
        /// <summary>
        /// Convert from <see cref="GeocodingResultDTO"/> to <see cref="GeocodingResult"/>.
        /// </summary>
        /// <param name="geocodingResultDTO">The <see cref="GeocodingResultDTO"/> object to convert.</param>
        /// <returns>The resulting <see cref="GeocodingResult"/>.</returns>
        public static GeocodingResult FromDTO(this GeocodingResultDTO geocodingResultDTO) {
            return new GeocodingResult() {
                FormattedAddress = geocodingResultDTO.Formatted_Address,
                Location = new GeoCoordinate(geocodingResultDTO.Geometry.Location.Lat, geocodingResultDTO.Geometry.Location.Lng)
            };
        }
    }
}
