/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces.Pollers {
    public interface IRasterContainer {
        /// <summary>
        /// Get the latest rasterDTO.
        /// </summary>
        /// <returns>The latest RasterDTO or null if there is none.</returns>
        RasterDTO GetRasterImage();

        /// <summary>
        /// Creat Image from raster
        /// </summary>
        /// <returns></returns>
        RasterDTO CreateImages(Raster raster);
        /// <summary>
        /// Get The latest Raster.
        /// </summary>
        /// <returns>The latest raster or null if there is none.</returns>
        Raster GetRaster();

        /// <summary>
        /// Update the currently stored raster.
        /// </summary>
        /// <param name="raster">The new raster that needs to be stored</param>
        void UpdateRaster(Raster raster);

        /// <summary>
        /// Get the list of all the RasterCells for the latest raster.
        /// </summary>
        List<RasterCell> GetRasterCells();
    }
}
