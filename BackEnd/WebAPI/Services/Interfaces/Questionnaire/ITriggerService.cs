/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces {
    public interface ITriggerService {

        /// <summary>
        /// Check if the average exposure of the last hour is > 40 ug/m3.
        /// </summary>
        /// <returns></returns>
        bool ExposureTrigger(UserLocationExposure[] exposures);

        /// <summary>
        /// Check if user has travelled in the last hour.
        /// </summary>
        /// <returns></returns>
        bool DistanceTrigger(UserLocationExposure[] locations);

        /// <summary>
        /// Decide which notification to send to the user. Only 1 per timeslot: (first) 7:00-11:00, (second) 11:00-14:00, (th) 14:00-20:00
        /// </summary>
        /// <param name="userId"></param>
        void SendNotificationPostRequest(Guid userId, string connectionString);

        /// <summary>
        /// Checks whether atleast half of the locations received, lays within the USP.
        /// </summary>
        /// <param name="locations"></param>
        /// <returns></returns>
        bool LocationInRaster(UserLocationExposure[] locations);
    }
}
