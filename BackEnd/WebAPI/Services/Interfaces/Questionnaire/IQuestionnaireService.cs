/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Questionnaire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces {
    public interface IQuestionnaireService {
        /// <summary>
        /// Check if a user can receive a questionnaire and if so, return it.
        /// </summary>
        /// <param name="userId">The userId from the user to check for.</param>
        Questionnaire CheckForQuestionnaire(Guid userId);

        /// <summary>
        /// Get a list of all the Questionnaire objects from the database.
        /// </summary>
        List<Questionnaire> GetAllQuestionnaires();
    }
}
