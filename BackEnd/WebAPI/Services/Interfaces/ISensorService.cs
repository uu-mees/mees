/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Device.Location;

namespace Services.Interfaces {
    public interface ISensorService {
        /// <summary>
        /// Get specific Sensor from database.
        /// </summary>
        /// <param name="sensorId">Id of the Sensor to retrieve.</param>
        /// <returns></returns>
        Sensor GetSensor(string sensorId);

        /// <summary>
        /// Get Sensors within area.
        /// </summary>
        /// <param name="southWestPoint">South west point of area.</param>
        /// <param name="northEastPoint">North east point of area.</param>
        /// <returns>An ICollection containing all relevant sensors.</returns>
        ICollection<Sensor> GetSensors(GeoCoordinate southWestPoint, GeoCoordinate northEastPoint);

        /// <summary>
        /// Get latest Batch from a Sensor.
        /// </summary>
        /// <param name="sensor">Sensor to retrieve latest Batch from.</param>
        /// <returns>Null if Sensor has no batches, otherwise returns latest Batch from sensor.</returns>
        SensorMeasurementBatch GetLatestBatch(Sensor sensor);

        /// <summary>
        /// Get all Batches from a Sensor within a certain time interval.
        /// </summary>
        /// <param name="sensorId">Id of the Sensor.</param>
        /// <param name="timeFrom">Lower bound of the time interval.</param>
        /// <param name="timeTo">Upper bound of the time interval.</param>
        /// <returns></returns>
        ICollection<SensorMeasurementBatch> GetSensorBatches(string sensorId, DateTime timeFrom, DateTime timeTo);
    }
}
