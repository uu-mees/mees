/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using Services.DTOs;
using System;
using System.Device.Location;
using System.Linq;
using Services.Interfaces.Pollers;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all raster services.
    /// </summary>
    public interface IRasterService
    {
        IRasterContainer rasterContainer { get; set; }

        /// <summary>
        /// Get the latest generated image.
        /// </summary>
        /// <returns>
        /// A <see cref="RasterDTO"/> containing the image, timestamp the raster was polled and its boundaries.
        /// </returns>
        RasterDTO GetRasterImage();

        /// <summary>
        /// Get the rasterCell from the given raster at the specified location.
        /// </summary>
        /// <param name="rasterId">The id from the raster to check for.</param>
        /// <param name="geoCoordinate">The location.</param>
        RasterCell GetRasterCell(int rasterId, GeoCoordinate geoCoordinate);
        /// <summary>
        /// Get the rasterCell with the given cellIndex from the specified raster.
        /// </summary>
        /// <param name="cellIndex">The index of the cell.</param>
        /// <param name="rasterId">The id of the raster.</param>
        RasterCell GetRasterCell(int cellIndex, int? rasterId = null);

        /// <summary>
        /// Get the <see cref="DateTime"/> polled of the most recently polled raster.
        /// </summary>
        /// <returns>The corresponding <see cref="DateTime"/>.</returns>
        DateTime GetLatestRasterTimeStamp();

        /// <summary>
        /// Get the latest <see cref="Raster"/>.
        /// </summary>
        /// <returns>The corresponding <see cref="Raster"/>.</returns>
        Raster GetLatestRaster();

        /// <summary>
        /// Get the <see cref="Raster"/> which was polled before and closest to a point in time.
        /// </summary>
        /// <param name="time">Time of comparison.</param>
        /// <returns>The corresponding <see cref="Raster"/>.</returns>
        Raster GetClosestHistoricalRaster(DateTime time);

        /// <summary>
        /// Get all rasters between two points in time.
        /// </summary>
        /// <param name="timeFrom">Lower bound of time interval.</param>
        /// <param name="timeTo">Upper bound of time interval.</param>
        /// <returns>IQueryable containing the relevant rasters.</returns>
        IQueryable<Raster> GetRelevantHistoricalRasters(DateTime timeFrom, DateTime timeTo);

        /// <summary>
        /// Get the exposure value for a given position (from the latest raster)
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <returns>The exposure value.</returns>
        double? GetExposureFromLocation(GeoCoordinate coordinate);
        Raster CreateFakeRaster();
        bool SameDimensions(int id1, int id2);
    }
}
