/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Tracking;

namespace Services.Interfaces {
    public interface ILocationExposureService {
        /// <summary>
        /// Get the latest location package;
        /// </summary>
        /// <returns>
        /// A <see cref="UserLocationExposure[]"/> containing the image, timestamp the raster was polled and its boundaries.
        /// </returns>

        void LinkUserAndExposuretoLocation(UserLocationExposure[] locationExposure);

        void CalculateDailyExposures(UserLocationExposure[] locationExposures);

    }
}
