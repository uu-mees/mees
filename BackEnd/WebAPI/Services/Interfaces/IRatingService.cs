/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Device.Location;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all rating services.
    /// </summary>
    public interface IRatingService {
        /// <summary>
        /// Validates <see cref="UserRating"/> and adds it to database.
        /// </summary>
        /// <param name="rating">The <see cref="UserRating"/> to add to database.</param>
        /// <returns>
        /// True if <see cref="UserRating"/> was correctly validated 
        /// and added to database, and false if not.
        /// </returns>
        UserRating AddRating(UserRating rating);

        /// <summary>
        /// Gets all <see cref="UserRating"/>s within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>All <see cref="UserRating"/>s lying within a circle around a certain position.</returns>
        ICollection<UserRating> GetRatings(GeoCoordinate requestedPosition, float radius);

        /// <summary>
        /// Gets all <see cref="UserRating"/>s posted after a certain time.
        /// </summary>
        /// <param name="from">Start of the interval.</param>
        /// <returns>All <see cref="UserRating"/>s posted after a certain time.</returns>
        ICollection<UserRating> GetNewRatings(DateTime from);

        /// <summary>
        /// Gets an average of all <see cref="UserRating"/>s within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>Returns average rating of all <see cref="UserRating"/>s within a certain circle.
        /// Returns 0 if no <see cref="UserRating"/>s were found.</returns>
        double GetAverageRating(GeoCoordinate requestedPosition, float radius);

        /// <summary>
        /// Gets all <see cref="UserRating"/>s and their amount of appearance within a certain circle.
        /// </summary>
        /// <param name="requestedPosition">The center of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <returns>A dictionary with ratings as keys and their amount of appearance as values.</returns>
        Dictionary<float, int> GetRatingNumbers(GeoCoordinate requestedPosition, float radius);

        /// <summary>
        /// Gets all <see cref="UserRating"/>s.
        /// </summary>
        /// <returns>All <see cref="UserRating"/>s.</returns>
        ICollection<UserRating> GetAllRatings();
    }
}
