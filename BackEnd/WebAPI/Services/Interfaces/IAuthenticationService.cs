/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using Domain.Administration;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all authentication services.
    /// </summary>
    public interface IAuthenticationService {
        /// <summary>
        /// Verify if the user/password combination is correct.
        /// </summary>
        /// <param name="username">The username to be used for logging in.</param>
        /// <param name="password">The password to be verified.</param>
        /// <returns>
        /// The <see cref="User"/> object corresponding to user logged in.
        /// </returns>
        User CheckLogin(string username, string password);

        /// <summary>
        /// Hash the given password with a salt.
        /// </summary>
        /// <param name="password">The password to be hashed.</param>
        /// <param name="salt">The salt to be used for hashing.</param>
        /// <returns>The hashed password.</returns>
        byte[] HashPassword(string password, Guid salt);
    }
}
