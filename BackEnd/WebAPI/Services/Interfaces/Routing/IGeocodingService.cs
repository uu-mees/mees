/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all geocoding services.
    /// </summary>
    public interface IGeocodingService {
        /// <summary>
        /// Find the coordinate corresponding to the given address.
        /// </summary>
        /// <param name="address">The address to find the coordinate for.</param>
        /// <returns>
        /// The GeocodingResponse for this query,
        /// can return multiple or zero matches as well.
        /// </returns>
        GeocodingResponse Geocode(string address);

        /// <summary>
        /// Find the address corresponding to the given coordinate.
        /// </summary>
        /// <param name="lat">The latitude of the coordinate.</param>
        /// <param name="lng">The longitude of the coordinate.</param>
        /// <returns>
        /// The GeocodingResponse for this query,
        /// can return multiple or zero matches as well.
        /// </returns>
        GeocodingResponse ReverseGeocode(double lat, double lng);
    }
}
