/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Device.Location;
using System.Security.Cryptography.X509Certificates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Routing;
using Domain.Exposure;
using Domain.Tracking;

namespace Services.Interfaces {
    public interface IExposureService {
        /// <summary>
        /// Calculate the exposure for a given route.
        /// </summary>
        /// <param name="route">The route to calculate the exposure for.</param>
        /// <returns>The average exposure.</returns>
        double? GetExposure(Route route);

        /// <summary>
        /// Get the exposure for a given coordinate.
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <returns>The exposure.</returns>
        double? GetExposure(GeoCoordinate coordinate);

        /// <summary>
        /// Get the exposure for a given coordinate.
        /// </summary>
        /// <param name="coordinate">The coordinate to get the exposure for.</param>
        /// <param name="raster">The raster to get the exposure from.</param>
        /// <returns>The exposure.</returns>
        double? GetExposureFromRaster(GeoCoordinate coordinate, Raster raster);

        /// <summary>
        /// Calculate the exposure for the route and insert the results directly into the route.
        /// </summary>
        /// <param name="route">The route to calculate for and insert the result into.</param>
        void InsertExposure(Route route);

        /// <summary>
        /// Get the bounds for a given Raster.
        /// </summary>
        /// <param name="raster">The Raster to get the bounds for.</param>
        /// <returns>The bounds as a tuple (order: item1: NE, item2: SE, item3: SW, item4: NW)</returns>
        Tuple<GeoCoordinate, GeoCoordinate, GeoCoordinate, GeoCoordinate> GetBounds(Raster raster);
    }
}
