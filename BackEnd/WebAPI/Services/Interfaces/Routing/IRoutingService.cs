/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Domain.Tracking;
using Services.DTOs;
using System.Device.Location;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all routing services.
    /// </summary>
    public interface IRoutingService {
        /// <summary>
        /// Get the raw responseDTO from GraphHopper. 
        /// <para>To get the actual Routes object use GetAllRoutes() instead.</para>
        /// </summary>
        /// <param name="start">The start coordinate for routing.</param>
        /// <param name="dest">The destination coordinate for routing.</param>
        /// <param name="vehicle">The vehicle for routing.</param>
        /// <param name="weighting">The weighting for routing.</param>
        /// <returns>The <see cref="RoutingResponseDTO"/> for the requested query.</returns>
        RoutingResponseDTO GetRoutingResponse(GeoCoordinate start, GeoCoordinate dest, string vehicle = "bike", string weighting = "shortest");

        /// <summary>
        /// Query GraphHopper for a route.
        /// </summary>
        /// <param name="start">The start coordinate for routing.</param>
        /// <param name="dest">The destination coordinate for routing.</param>
        /// <param name="vehicle">The vehicle for routing.</param>
        /// <param name="weighting">The weighting for routing.</param>
        /// <returns>The resulting <see cref="Routes"/> object for the requested query.</returns>
        Routes GetAllRoutes(GeoCoordinate start, GeoCoordinate dest, string vehicle = "bike", string weighting = "shortest");
    }
}
