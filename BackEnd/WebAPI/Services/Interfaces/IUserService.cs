/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using System;
using System.Collections.Generic;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all user services.
    /// </summary>
    public interface IUserService {
        /// <summary>
        /// Validates user registration information and creates new user.
        /// </summary>
        /// <param name="userRegistration">
        /// <see cref="UserRegistration"/> object containing user registration information.
        /// </param>
        /// <returns>
        /// If validation completes, returns the new <see cref="User"/>. 
        /// Otherwise returns null.
        /// </returns>
        User CreateNewUser(UserRegistration userRegistration);

        /// <summary>
        /// Updates the user profile with new or edited information.
        /// </summary>
        /// <param name="userId"><see cref="
        /// 
        /// 
        /// "/> of the user.</param>
        /// <param name="profile"><see cref="UserProfile"/> carrying information.</param>
        /// <returns><see cref="UserProfile"/> if update was successful. <see langword="null"/> if unsuccessful.</returns>
        UserProfile UpdateUserProfile(Guid userId, UserProfile profile);

        /// <summary>
        /// Updates <see cref="User.HashedPassword"/> if <see cref="PasswordPair"/> satisfies requirements.
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> of the <see cref="User"/>.</param>
        /// <param name="passwordPair"><see cref="PasswordPair"/> containing <see cref="PasswordPair.OldPassword"/> for verification, and <see cref="PasswordPair.NewPassword"/> to set new <see cref="User.HashedPassword"/>.</param>
        /// <returns><see cref="PasswordPair"/> if Update was successful. <see langword="null"/> if Update failed."/></returns>
        PasswordPair UpdatePassword(Guid userId, PasswordPair passwordPair);

        /// <summary>
        /// Generates a reset token and emails it. Used to reset a <see cref="User"/>'s password.
        /// </summary>
        /// <param name="email">Email address registered to the <see cref="User"/> account.</param>
        /// <returns><see cref="PasswordResetToken"/> if successful. <see langword="null"/> otherwise.</returns>
        PasswordResetToken GenerateResetToken(string email);

        /// <summary>
        /// Resets the <see cref="User"/>'s password.
        /// </summary>        
        /// <param name="token">Generated temporary token to authenticate request.</param>
        /// <param name="newPassword">New password for the <see cref="User"/>.</param>
        /// <param name="confirmPassword">Password used to decrease chance user made a typo.</param>
        /// <returns>True if email was sent and update successful.</returns>
        bool ResetPassword(string token, string newPassword, string confirmPassword);

        /// <summary>
        /// Removes all files corresponding to a user.
        /// </summary>
        /// <param name="userId">The <see cref="Guid"/> of the user to be removed.</param>
        /// <returns>A boolean whether the deletion was successful.</returns>
        bool DeleteEntireUser(Guid userId);

        /// <summary>
        /// Retrieve <see cref="UserProfile"/> as subsection of <see cref="User"/> from <see cref="DatabaseContext"/>.
        /// </summary>
        /// <param name="userId"><see cref="Guid"/> as identification of user.</param>
        /// <returns><see cref="UserProfile"/> if update was successful. <see langword="null"/> if unsuccessful.</returns>
        UserProfile GetUserProfile(Guid userId);


        /// <summary>
        /// Get a range of daily exposures of a user
        /// </summary>
        /// <param name="dateFrom">Inclusive starting date</param>
        /// <param name="dateTo">Inclusive ending date</param>
        /// <returns>A range of daily exposures, null if dates are invalid</returns>
        ICollection<DailyExposure> GetDailyExposure(Guid userId, DateTime dateFrom, DateTime dateTo);

        /// <summary>
        /// Get the restrictions of the front-end features for a user, based on the research the user is in.
        /// </summary>
        /// <param name="researchId"></param>
        /// <returns></returns>
        Researches GetResearchRestrictions(Guid researchId);

    }
}
