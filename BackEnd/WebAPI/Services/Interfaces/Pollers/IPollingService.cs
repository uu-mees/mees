/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Services.Retrievers;
using System;

namespace Services.Interfaces {
    /// <summary>
    /// The general interface for all polling services.
    /// </summary>
    /// <typeparam name="T">
    /// The return type of the retrieved data.
    /// </typeparam>
    public interface IPollingService<T> {
        /// <summary>
        /// Start polling data using the given retriever indefinitely.
        /// </summary>
        /// <param name="interval">The poll interval in seconds.</param>
        /// <param name="eventHandler">
        /// The <see cref="EventHandler"/> which will be invoked when data is polled.
        /// </param>
        /// <param name="retriever">An instance of the retriever to be used.</param>
        void StartPolling(int interval, EventHandler<T> eventHandler, IRetriever<T> retriever);
    }
}
