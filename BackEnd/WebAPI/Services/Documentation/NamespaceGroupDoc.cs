/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
namespace Services {
    /// <summary>
    /// The Services namespace contains all data services and retrievers.
    /// </summary>
    internal class NamespaceGroupDoc {
    }
}

namespace Services.Converters {
    /// <summary>
    /// The Services.Converters namespace contains all converters for the DTOs.
    /// </summary>
    internal class NamespaceGroupDoc {
    }
}

namespace Services.DTOs {
    /// <summary>
    /// The Services.DTOs namespace contains all classes defining the data formats to be sent to the front-end.
    /// </summary>
    internal class NamespaceGroupDoc {
    }
}
