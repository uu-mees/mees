/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
namespace Services {
    /// <summary>
    /// The Services namespace contains all data services and retrievers.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.Converters {
    /// <summary>
    /// The Services.Converters namespace contains all converters for the DTOs.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.Converters.Routing {
    /// <summary>
    /// The Services.Converters.Routing namespace contains all converters for the routing services.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.DTOs {
    /// <summary>
    /// The Services.DTOs namespace contains all classes defining the data formats to be sent to the front-end.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.DTOs.Routing {
    /// <summary>
    /// The Services.DTOs.Routings namespace contains all DTOs for the routing services.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.Implementations {
    /// <summary>
    /// The Services.Implementations namespace contains all implementations of the services.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.Interfaces {
    /// <summary>
    /// The Services.Interfaces namespace contains all interfaces for the services.
    /// </summary>
    internal class NamespaceDoc {
    }
}

namespace Services.Retrievers {
    /// <summary>
    /// The Services.Retrievers namespace contains all retrievers for external data.
    /// </summary>
    internal class NamespaceDoc {
    }
}
