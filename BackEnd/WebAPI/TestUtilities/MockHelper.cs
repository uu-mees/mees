/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Moq;
using System.Data.Entity;
using System.Linq;
using DataAccess;
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Microsoft.Extensions.Logging;
using System;
using Services.Interfaces.Pollers;
using Services.DTOs;
using Domain.Questionnaire;

namespace TestUtils {
    // Utilities to simplify testing
    // In order to add a new context inserter for an object, simply copy one of the 
    // Get[..]Context methods and edit it for your object type.
    public static class MockHelper {
        private static LoggerFactory logfactory = new LoggerFactory();

        /// <summary>
        /// Generic method for editing a database context to include a new object
        /// </summary>
        /// <typeparam name="T">Any</typeparam>
        /// <param name="context">The context that needs to be updated</param>
        /// <param name="obj">The object that needs to be included in the database, needs to be an IQuerable<objtype></param>
        /// <returns>A new databasecontext</returns>
        private static (Mock<DatabaseContext>, Mock<DbSet<T>>) GetDBContext<T>(Mock<DatabaseContext> context, IQueryable obj)
          where T : class {
            Mock<DbSet<T>> mock = new Mock<DbSet<T>>();
            mock.As<IQueryable<T>>().Setup(x => x.Provider).Returns(obj.Provider);
            mock.As<IQueryable<T>>().Setup(x => x.Expression).Returns(obj.Expression);
            mock.As<IQueryable<T>>().Setup(x => x.ElementType).Returns(obj.ElementType);
            mock.As<IQueryable<T>>().Setup(x => x.GetEnumerator()).Returns(obj.Cast<T>().GetEnumerator());

            return (context, mock);
        }

        public static Mock<DatabaseContext> GetAuthenticationContext(Mock<DatabaseContext> context, IQueryable<User> users) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<User>> mock) = GetDBContext<User>(context, users);
            newContext.Setup(x => x.Users)
                          .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetRasterContext(Mock<DatabaseContext> context, IQueryable<Raster> raster) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<Raster>> mock) = GetDBContext<Raster>(context, raster);
            newContext.Setup(x => x.Rasters)
                          .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetRasterCellContext(Mock<DatabaseContext> context, IQueryable<RasterCell> rasterCell) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<RasterCell>> mock) = GetDBContext<RasterCell>(context, rasterCell);
            newContext.Setup(x => x.RasterCells)
                          .Returns(mock.Object);


            return newContext;
        }

        public static Mock<DatabaseContext> GetUserRatingContext(Mock<DatabaseContext> context, IQueryable<UserRating> userRating) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<UserRating>> mock) = GetDBContext<UserRating>(context, userRating);
            newContext.Setup(x => x.UserRatings)
                    .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetSensorMeasurementBatchContext(Mock<DatabaseContext> context,
                                                                             IQueryable<SensorMeasurementBatch> sensorMeasurementBatch) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<SensorMeasurementBatch>> mock) = GetDBContext<SensorMeasurementBatch>(context, sensorMeasurementBatch);
            newContext.Setup(x => x.SensorMeasurementBatches)
                    .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetLocationExposuresContext(Mock<DatabaseContext> context, IQueryable<UserLocationExposure> userLocationExposures) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<UserLocationExposure>> mock) = GetDBContext<UserLocationExposure>(context, userLocationExposures);
            newContext.Setup(x => x.UserLocationExposures)
                    .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetFireBasesContext(Mock<DatabaseContext> context, IQueryable<FireBase> userLocationExposures) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<FireBase>> mock) = GetDBContext<FireBase>(context, userLocationExposures);
            newContext.Setup(x => x.FireBases)
                    .Returns(mock.Object);

            return newContext;
        }
        public static Mock<DatabaseContext> GetDailyExposuresContext(Mock<DatabaseContext> context, IQueryable<DailyExposure> dailyExposures) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<DailyExposure>> mock) = GetDBContext<DailyExposure>(context, dailyExposures);
            newContext.Setup(x => x.DailyExposures)
                .Returns(mock.Object);

            return newContext;
        }
        public static Mock<DatabaseContext> GetSensorContext(Mock<DatabaseContext> context, IQueryable<Sensor> sensor) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<Sensor>> mock) = GetDBContext<Sensor>(context, sensor);
            newContext.Setup(x => x.Sensors)
                    .Returns(mock.Object);

            return newContext;
        }

        public static Mock<DatabaseContext> GetUserContext(Mock<DatabaseContext> context, IQueryable<User> users) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<User>> mock) = GetDBContext<User>(context, users);
            newContext.Setup(x => x.Users)
                          .Returns(mock.Object);

            return newContext;
        }

        public static Mock<IRasterContainer> GetRasterContainer(Raster raster = null, RasterDTO rasterDTO = null) {
            Mock<IRasterContainer> container = new Mock<IRasterContainer>();
            container.Setup(x => x.GetRaster()).Returns((Raster)raster);
            container.Setup(x => x.GetRasterCells()).Returns(raster?.Cells.ToList() ?? null);
            container.Setup(x => x.GetRasterImage()).Returns((RasterDTO)rasterDTO);
            return container;
        }

        public static Mock<DatabaseContext> GetTriggerContext(Mock<DatabaseContext> context, IQueryable<Trigger> triggers) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<Trigger>> mock) = GetDBContext<Trigger>(context, triggers);
            newContext.Setup(x => x.Triggers)
                    .Returns(mock.Object);
            return newContext;
        }

        public static Mock<DatabaseContext> GetQuestionnaireContext(Mock<DatabaseContext> context, IQueryable<Questionnaire> questionnaires) {
            (Mock<DatabaseContext> newContext, Mock<DbSet<Questionnaire>> mock) = GetDBContext<Questionnaire>(context, questionnaires);
            newContext.Setup(x => x.Questionnaires)
                    .Returns(mock.Object);
            return newContext;
        }

        public static ILogger<T> GetLogger<T>()
            where T : class {
            return new Logger<T>(logfactory);
        }
    }
}
