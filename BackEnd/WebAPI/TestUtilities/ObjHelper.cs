/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using Domain.Exposure;
using Services.DTOs;
using Utils;

namespace TestUtils {
    public static class ObjHelper {
        /// <summary>
        /// Utility function that returns four RasterCells that have their locations set
        /// </summary>
        /// <param name="cellSize"></param>
        /// <returns>Four Rastercells</returns>
        public static IQueryable<RasterCell> GetBasicRasterCells(float cellSize) {

            GeoCoordinate northEastPoint = new GeoCoordinate(20, 20).CalculateDerivedPosition(Math.Sqrt(cellSize * cellSize * 2), 45);

            IQueryable<RasterCell> cells = new List<RasterCell>{
                new RasterCell(){SouthWestLatitude = 5, SouthWestLongitude = 5,
                                 NorthEastLatitude = 8, NorthEastLongitude = 8, Value = 4.123456f },
                new RasterCell(){SouthWestLatitude = 23, SouthWestLongitude = 15,
                                 NorthEastLatitude = 28, NorthEastLongitude = 18, Value = 3.65321f },
                new RasterCell(){SouthWestLatitude = 20, SouthWestLongitude = 20,
                                 NorthEastLatitude = (float)northEastPoint.Latitude,
                                 NorthEastLongitude = (float)northEastPoint.Longitude, Value = 2.43366f },
                new RasterCell(){SouthWestLatitude = 15, SouthWestLongitude = 3,
                                 NorthEastLatitude = 18, NorthEastLongitude = 7, Value = 1.74537f }
            }.AsQueryable();

            return cells;
        }

        /// <summary>
        /// Utility function that returns an empty IQueryable of RasterCells
        /// </summary>
        /// <returns>Empty list of RasterCells</returns>
        public static IQueryable<RasterCell> GetEmptyRasterCells() {
            return new List<RasterCell> { }.AsQueryable();
        }

        /// <summary>
        /// Utility function for getting a raster cells attached
        /// </summary>
        /// <param name="cells">The cells that need to be attached to the Raster</param>
        /// <returns>An IQueryable containing a single Raster with the cells attached</returns>
        public static IQueryable<Raster> GetRaster(IQueryable<RasterCell> cells, float cellSize) {
            int id = 0;
            IQueryable<Raster> raster = new List<Raster>{
                new Raster() { NoDataValue = -1, Id = id, Cells = cells.ToList(), CellSize = cellSize, TimeStampPolled = DateTime.UtcNow
                } }.AsQueryable<Raster>();

            if (cells != null) {
                List<RasterCell> cellList = cells.ToList();
                cellList.ForEach(x => x.RasterId = id);
                cells = cellList.AsQueryable();
            }

            return raster;
        }

        /// <summary>
        /// Utility function
        /// </summary>
        /// <returns>Two rasters with emtpy cells and different timestamps</returns>
        public static IQueryable<Raster> GetRastersWithTimeStamp() {
            IQueryable<Raster> rasters = new List<Raster> { new Raster() { Cells = new List<RasterCell>(),
                                                                           TimeStampPolled = DateTime.MinValue },
                                                            new Raster() { Cells = new List<RasterCell>(),
                                                                           TimeStampPolled = DateTime.MaxValue }
                                                          }.AsQueryable();
            return rasters;
        }
    }
}
