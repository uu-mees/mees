/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;

namespace Domain.Exposure {
    public class Raster {
        public int Id { get; set; }

        public float CellSize { get; set; }
        public float NoDataValue { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public DateTime TimeStampPolled { get; set; }
        public int HashCode { get; set; }
        public double NorthEastLat { get; set; }
        public double NorthEastLng { get; set; }
        public double SouthWestLat { get; set; }
        public double SouthWestLng { get; set; }

        public virtual ICollection<RasterCell> Cells { get; set; }
    }
}
