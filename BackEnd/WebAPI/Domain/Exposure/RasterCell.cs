/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;

namespace Domain.Exposure {
    public class RasterCell {
        public int CellId { get; set; }
        public int RasterId { get; set; }

        public float Value { get; set; }

        public double SouthWestLatitude { get; set; }
        public double SouthWestLongitude { get; set; }
        public double NorthEastLatitude { get; set; }
        public double NorthEastLongitude { get; set; }

        public virtual Raster Raster { get; set; }
        public int CellIndex { get; set; }
    }
}
