/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;

namespace Domain.Exposure {
    public class DailyExposure {
        public Guid UserId { get; set; }
        public DateTime Date { get; set; }

        public double AverageExposure { get; set; }
        public long TotalDuration { get; set; }

        public virtual User User { get; set; }
    }
}
