/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Routing;
using Domain.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exposure {
    /// <summary>
    /// ExposurePath is a struct that holds all the necessary information to display the exposure for a given 
    /// polyline. The important thing to note is that this struct is supposed to be initialised using only
    /// the available constructors (not the default empty constructor) this is to ensure that the Path variable
    /// contains at least all the points of the route, such that this can always be used for drawing the route
    /// even when the exposure is null.
    /// </summary>
    public struct ExposurePath {
        // Calculated average exposure.
        public double? Exposure { get; set; }
        // Total duration.
        public long Duration { get; set; }
        public List<long> ElapsedTime { get; set; }
        public List<Location> Path { get; set; }
        public List<double?> EdgeExposures { get; set; }

        /// <summary>
        /// The "null" constructor. It sets the Path according to the given list of GHPoints, and it sets the
        /// Exposure to null and the exposure for each edge in EdgeExposures to null.
        /// </summary>
        /// <param name="path"></param>
        public ExposurePath(List<GHPoint> path) {
            if (path == null) {
                throw new ArgumentException("Path can not be null");
            }

            Exposure = null;
            Path = path.ConvertAll(x => new Location(x.Coordinate));
            ElapsedTime = path.ConvertAll(x => x.ElapsedTime);
            EdgeExposures = Enumerable.Repeat<double?>(null, Math.Max(0, path.Count - 1)).ToList();
            Duration = path.Count > 0 ? path.Last().ElapsedTime : 0;
        }

        /// <summary>
        /// Sets all variables to the given data.
        /// </summary>
        /// <param name="exposure">Exposure.</param>
        /// <param name="path">The list of GHPoints.</param>
        /// <param name="edgeExposures">The list of exposures for each edge.</param>
        public ExposurePath(double? exposure, List<GHPoint> path, List<double?> edgeExposures) {
            Exposure = exposure;
            ElapsedTime = path.ConvertAll(x => x.ElapsedTime);
            Path = path.ConvertAll(x => new Location(x.Coordinate));
            EdgeExposures = edgeExposures;
            Duration = path.Count > 0 ? path.Last().ElapsedTime : 0;
        }
    }
}
