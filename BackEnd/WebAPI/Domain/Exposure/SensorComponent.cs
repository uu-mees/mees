/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;

namespace Domain.Exposure {
    public class SensorComponent {
        public Guid Id { get; set; }

        public string Component { get; set; }
        public string Unit { get; set; }

        public virtual ICollection<SensorMeasurement> Measurements { get; set; }
    }
}
