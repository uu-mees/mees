/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;

namespace Domain.Exposure {
    public class SensorMeasurement {
        public Guid Id { get; set; }
        public Guid BatchId { get; set; }
        public Guid SensorComponentId { get; set; }

        public string Value { get; set; }

        public virtual SensorComponent Component { get; set; }
        public virtual SensorMeasurementBatch Batch { get; set; }
    }
}
