/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;

namespace Domain.Exposure {
    public class Sensor {
        public string Id { get; set; }
        public string Label { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public virtual SensorMeasurementBatch LastValidBatch { get; set; }

        public virtual ICollection<SensorMeasurementBatch> Batches { get; set; }
    }
}
