/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
* ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Questionnaire {
    public class TriggersConfiguration {
        public int ExposureTriggerThreshold { get; set; }
        public int DistanceTriggerLowerThreshold { get; set; }
        public int DistanceTriggerThreshold { get; set; }
        public string QuestionnaireNotificationTitle { get; set; }
        public string QuestionnaireNotificationWaitTextPart1 { get; set; }
        public string QuestionnaireNotificationWaitTextPart2 { get; set; }
        public string QuestionnaireNotificationText { get; set; }
        public string QuestionnaireNotificationUrl { get; set; }
        public string MinigameNotificationTitle { get; set; }
        public string MinigameNotificationText { get; set; }
        public string MinigameNotificationUrl { get; set; }
        public string FirebaseAddress { get; set; }
        public string FirebaseKey { get; set; }
        public int BeginTimeSlot1 { get; set; }
        public int BeginTimeSlot2 { get; set; }
        public int BeginTimeSlot3 { get; set; }
        public int EndTimeSlot3 { get; set; }
    }
}
