/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Questionnaire {
    public class Trigger {
        public int Priority { get; set; }
        public Guid TriggerId { get; set; }
        public Guid UserId { get; set; }
        public DateTime? TimeStamp { get; set; }

        public virtual User User { get; set; }
    }
}
