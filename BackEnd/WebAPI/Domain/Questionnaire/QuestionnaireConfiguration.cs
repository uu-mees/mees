/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Questionnaire {
    public class QuestionnaireConfiguration {
        public int CooldownSeconds { get; set; }

        public TriggerConfigurations TriggerConfigurations { get; set; }
    }

    public class TriggerConfigurations {
        public DefaultConfiguration DefaultConfiguration { get; set; }
        public DistanceConfiguration DistanceConfiguration { get; set; }
        public ExposureConfiguration ExposureConfiguration { get; set; }
    }

    public class TriggerConfiguration {
        public bool Active { get; set; }

        public string Questionnaire { get; set; }

        public int Priority { get; set; }
    }
    public class DefaultConfiguration {
        public TriggerConfiguration TriggerConfiguration { get; set; }
    }
    public class DistanceConfiguration {
        public TriggerConfiguration TriggerConfiguration { get; set; }
    }
    public class ExposureConfiguration {
        public TriggerConfiguration TriggerConfiguration { get; set; }
    }
}
