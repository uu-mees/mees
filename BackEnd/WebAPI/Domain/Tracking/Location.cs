/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Domain.Tracking {
    public class Location {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Location() {

        }

        public Location(double latitude, double longitude) {
            if (!GeoUtils.IsValidLatitude(latitude) || !GeoUtils.IsValidLongitude(longitude)) {
                throw new ArgumentException("Invalid latitude or longitude");
            }

            Latitude = latitude;
            Longitude = longitude;
        }

        public Location(GeoCoordinate coordinate) {
            Latitude = coordinate.Latitude;
            Longitude = coordinate.Longitude;
        }
    }
}
