/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using Domain.Administration;

namespace Domain.Tracking {
    public class UserLocationExposure {
        public Location Position { get; set; }
        public DateTime Timestamp { get; set; }
        public double? Exposure { get; set; }
        public Guid UserID { get; set; }

        public virtual User User { get; set; }
    }
}
