/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;
using System.Device.Location;

namespace Domain.Routing {
    public class Routes {
        public GeoCoordinate Start { get; set; }
        public GeoCoordinate Destination { get; set; }

        public string Vehicle { get; set; }
        public string Weighting { get; set; }
        public int Count { get; set; }

        public List<Route> RouteList { get; set; }
    }
}
