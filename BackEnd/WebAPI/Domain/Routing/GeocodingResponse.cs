/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;
using System.Device.Location;

namespace Domain.Routing {
    public class GeocodingResponse {
        public List<GeocodingResult> Results { get; set; }
    }

    public class GeocodingResult {
        public string FormattedAddress { get; set; }
        public GeoCoordinate Location { get; set; }
    }
}
