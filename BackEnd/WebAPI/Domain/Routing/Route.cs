/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Routing {
    public class Route {
        public double Distance { get; set; }

        //Duration in miliseconds
        public long Duration { get; set; }
        public double? Exposure { get; set; }

        public List<GHPoint> Path { get; set; }

        public ExposurePath ExposurePath { get; set; }
    }
}
