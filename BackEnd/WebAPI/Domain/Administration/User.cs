/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using Domain.Exposure;
using Domain.Questionnaire;
using Domain.Tracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Administration {
    public enum Role {
        Regular,
        Administrator
    }

    public class User {
        public Guid Id { get; set; }
        public Role Role { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public bool TrackLocation { get; set; }
        public int? Height { get; set; }
        public float? Weight { get; set; }
        public string Sex { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string HomeAddress { get; set; }
        public string WorkAddress { get; set; }
        [Required]
        public byte[] HashedPassword { get; set; }
        public Guid Salt { get; set; }
        public DateTime? LastQuestionnaireTime { get; set; }
        public DateTime? LastMiniGameTime { get; set; }
        public virtual List<FireBase> FireBases { get; set; }
        public virtual List<DailyExposure> DailyExposures { get; set; }
        public virtual PasswordResetToken PasswordResetToken { get; set; }
        public virtual List<UserRating> UserRatings { get; set; }
        public virtual List<Trigger> Triggers { get; set; }
        public virtual List<UserLocationExposure> LocationExposures { get; set; }
        public virtual List<UserResearches> UserResearches { get; set; }
    }
}
