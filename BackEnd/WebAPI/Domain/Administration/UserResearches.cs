/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Administration {
    public class UserResearches {
        public Guid userId { get; set; }
        public Guid researchId { get; set; }
        public DateTime startTime { get; set; }
        public DateTime? endTime { get; set; }

        public virtual User User { get; set; }
        public virtual Researches Research { get; set; }
    }
}
