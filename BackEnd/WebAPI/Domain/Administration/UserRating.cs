/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;

namespace Domain.Administration {
    public class UserRating {
        public Guid Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public int Rating { get; set; }

        public DateTime TimeStamp { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }
    }
}
