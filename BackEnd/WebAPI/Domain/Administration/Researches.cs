/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Administration {
    public class Researches {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool ViewRatings { get; set; }
        public bool ViewSensors { get; set; }
        public bool ViewRaster { get; set; }
        public bool ViewExposure { get; set; }
        public bool ViewMinigameScores { get; set; }
        public bool ViewHighScores { get; set; }
        public bool ViewRoutePlanner { get; set; }

        public virtual List<UserResearches> UserResearches { get; set; }
    }
}
