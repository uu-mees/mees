
/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Administration {
    public class DataSource {
        public Guid ID { get; set; }
        [Required]
        public string URL { get; set; }
        public int Type { get; set; }
        public bool Active { get; set; }
    }
}
