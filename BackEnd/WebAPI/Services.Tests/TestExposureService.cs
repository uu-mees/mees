/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Exposure;
using Domain.Routing;
using Domain.Tracking;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Device.Location;
using Utils;
using TestUtils;
using Microsoft.Extensions.Logging;
using Services.Interfaces.Pollers;

namespace Services.Tests {
    [TestFixture]
    class TestExposureService {
        private ConfigurationService configurationService;
        private ILogger<ExposureService> logger;
        private ILogger<RasterService> rasterlogger;

        [OneTimeSetUp]
        public void Setup() {
            // All but one test ignore the limit to being out of bounds
            // which is why we set the MaxOutOfBoundsPercentage to 100%
            configurationService = new ConfigurationService() { MaxOutOfBoundsPercentage = 1 };
            logger = MockHelper.GetLogger<ExposureService>();
            rasterlogger = MockHelper.GetLogger<RasterService>();
        }

        [Test]
        public void GetDistance_Valid() {
            //Arrange
            GeoCoordinate one, two;
            one = new GeoCoordinate(52.08616, 5.16348);
            two = new GeoCoordinate(52.08620, 5.16680);

            //Act
            double dist = one.GetDistanceTo(two);

            //Assert
            Assert.IsTrue(Math.Round(dist) == 227); //Approximate distance is 227
        }

        [Test]
        public void CalcIntersection_CorrectIntersection() {
            //Arrange
            GeoCoordinate start1 = new GeoCoordinate(52.0892, 5.166);
            GeoCoordinate end1 = new GeoCoordinate(52.0862, 5.166);
            GeoCoordinate start2 = new GeoCoordinate(52.0877, 5.1633);
            GeoCoordinate end2 = new GeoCoordinate(52.0876, 5.169);

            GeoCoordinate test = GeoUtils.CalcIntersection(start1, end1, start2, end2);

            Assert.IsTrue(test != null);
            //Doubles:
            //{52.0876526659012, 5.16599999999858}
            //Floats:
            //{52.0876534039527, 5.16600040780308}
        }

        [Test]
        public void CalcIntersection_NoIntersection() {
            //Arrange
            GeoCoordinate start1 = new GeoCoordinate(52.0892, 5.166);
            GeoCoordinate end1 = new GeoCoordinate(52.0877, 5.1633);
            GeoCoordinate start2 = new GeoCoordinate(52.0862, 5.166);
            GeoCoordinate end2 = new GeoCoordinate(52.0876, 5.169);

            GeoCoordinate test = GeoUtils.CalcIntersection(start1, end1, start2, end2);

            Assert.IsNull(test);
        }

        [Test]
        public void GetDistance_WithMoreCurvature() {
            //Arrange
            GeoCoordinate one, two;
            one = new GeoCoordinate(50.0359, -5.4253);
            two = new GeoCoordinate(58.3838, -3.0412);
            //Act
            double dist = one.GetDistanceTo(two);

            //Assert
            Assert.IsTrue(Math.Round(dist) == 941760); //Approximate distance is 941.760 km
        }

        [Test]
        public void InsertExposure_EmptyRoute() {
            //Arrange
            Route route = new Route()
            {
                Path = new List<GHPoint>(), //Path is empty.
                Distance = 0,
                Duration = 0
            };
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer();
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            exposureService.InsertExposure(route);

            //Assert
            Assert.IsNull(route.ExposurePath.Exposure);
        }

        [Test]
        public void InsertExposure_OutsideTargetZone() {
            //Arrange
            Route route = new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(40.73696, -73.98451),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(40.73603, -73.98227),
                        ElapsedTime = 10000
                    }
                },//Path in New York
                Distance = 100,
                Duration = 10000
            };


            Raster raster = GetRaster();
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            exposureService.InsertExposure(route);

            //Assert
            Assert.IsNull(route.ExposurePath.Exposure);
        }

        [Test]
        public void InsertExposure_RouteValidAverage() {
            //Arrange
            Route route = GetDefaultRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            exposureService.InsertExposure(route);

            //Assert
            Assert.NotNull(route.ExposurePath.Exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)route.ExposurePath.Exposure, 10.00);
            Assert.LessOrEqual((float)route.ExposurePath.Exposure, 10.05);
        }


        [Test]
        public void GetExposure_RouteValidAverage() {
            //Arrange
            Route route = GetDefaultRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 10.00);
            Assert.LessOrEqual((float)exposure, 10.05);
        }

        [Test]
        public void GetExposure_RouteNull() {
            //Arrange
            Route route = null;
            Mock<IRasterService> rasterService = new Mock<IRasterService>();
            IExposureService exposureService = new ExposureService(configurationService, rasterService.Object, logger);

            //Act
            double? result = exposureService.GetExposure(route);

            //Assert
            Assert.IsNull(result);
        }


        [Test]
        public void InsertExposure_RouteNull() {
            //Arrange
            Route route = null;
            Mock<IRasterService> rasterService = new Mock<IRasterService>();
            IExposureService exposureService = new ExposureService(configurationService, rasterService.Object, logger);

            //Act
            exposureService.InsertExposure(route);

            //Assert
            Assert.IsNull(route);
        }

        [Test]
        public void InsertExposure_SinglePoint() {
            //Arrange
            Route route = new Route()
            {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5, 5),
                        ElapsedTime = 0
                    }
                },
                Distance = 0,
                Duration = 0
            };

            Raster raster = GetRaster();
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            exposureService.InsertExposure(route);

            //Assert
            Assert.NotNull(route.ExposurePath.Exposure);
            Assert.AreEqual(route.ExposurePath.Exposure, 15.0);
        }

        [Test]
        public void GetExposure_FromCoordinate() {
            Raster raster = GetRaster();
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            double? value = exposureService.GetExposure(new GeoCoordinate(5, 5));

            Assert.NotNull(value);
            Assert.AreEqual(15, value);
        }

        [Test]
        public void GetExposure_PartlyOOB() {
            //Arrange
            Route route = GetOOBRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 11.87);
            Assert.LessOrEqual((float)exposure, 11.93);
        }

        [Test]
        public void GetExposure_OOBTooLong() {
            //Arrange
            Route route = GetOOBRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(new ConfigurationService() { MaxOutOfBoundsPercentage = 0.05f }, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.Null(exposure);
        }

        [Test]
        public void GetExposure_TangentSpecialCase() {
            //Arrange
            Route route = GetSpecialTangentRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 10.00);
            Assert.LessOrEqual((float)exposure, 10.05);
        }

        [Test]
        public void GetExposure_CloseToRasterEdge() {
            //Arrange
            Route route = GetCloseToRasterEdge();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 11.40);
            Assert.LessOrEqual((float)exposure, 11.45);
        }

        [Test]
        public void GetExposure_DiagonalCase() {
            //Arrange
            Route route = GetDiagonalRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 7.5);
            Assert.LessOrEqual((float)exposure, 7.55);
        }

        [Test]
        public void GetExposure_CornerCase() {
            //Arrange
            Route route = GetCornerRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.Null(exposure);
        }

        [Test]
        public void GetExposure_EmptyPath() {
            //Arrange
            Route route = new Route() { Path = new List<GHPoint>() };
            Raster raster = GetRaster();

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.Null(exposure);
        }

        [Test]
        public void GetExposure_NullPath() {
            //Arrange
            Route route = new Route() { Path = null };
            Raster raster = GetRaster();

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.Null(exposure);
        }

        [Test]
        public void GetExposure_SingleOOBPoint() {
            //Arrange
            Route route = new Route() { Path = new List<GHPoint>() { new GHPoint() { Coordinate = new GeoCoordinate(25, 25) } } };
            Raster raster = GetRaster();

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(null, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.Null(exposure);
        }

        [Test]
        public void GetExposure_CloseToCellEdgeCase() {
            //Arrange
            Route route = GetCloseToCellEdgeRoute();
            Raster raster = GetRaster();

            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetRasterCellContext(context, raster.Cells.AsQueryable());
            context = MockHelper.GetRasterContext(context, (new List<Raster> { raster }).AsQueryable());

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster);
            IRasterService rasterService = new RasterService(context.Object, rasterlogger, container.Object);
            IExposureService exposureService = new ExposureService(configurationService, rasterService, logger);

            //Act
            double? exposure = exposureService.GetExposure(route);

            //Assert
            Assert.NotNull(exposure);
            //Compare actual result with expected result. Approximation is used, should the average change this test will fail.
            Assert.GreaterOrEqual((float)exposure, 6.69);
            Assert.LessOrEqual((float)exposure, 6.74);
        }


        [Test]
        public void CalcIntersection_AfricaTest() {
            GeoCoordinate s1 = new GeoCoordinate(20.0657989469102, 3.73375879033783);
            GeoCoordinate e1 = new GeoCoordinate(19.9, 19.9);

            GeoCoordinate s2 = new GeoCoordinate(20, 10);
            GeoCoordinate e2 = new GeoCoordinate(20, 20);

            GeoCoordinate intersection = GeoUtils.CalcIntersection(s1, e1, s2, e2);

            Assert.NotNull(intersection);
        }

        [Test]
        public void WithinArea_AfricaTest() {
            GeoCoordinate p1 = new GeoCoordinate(20.28248, 9.99464);
            GeoCoordinate p2 = new GeoCoordinate(20.2849, 9.99464);

            GeoCoordinate northEast = new GeoCoordinate(20, 20);
            GeoCoordinate southEast = new GeoCoordinate(0, 20);
            GeoCoordinate southWest = new GeoCoordinate(0, 0);
            GeoCoordinate northWest = new GeoCoordinate(20, 0);

            bool inBounds = p1.SpecialWithinArea(northEast, southEast, southWest, northWest);
            bool outOfBounds = p2.SpecialWithinArea(northEast, southEast, southWest, northWest);

            Assert.True(inBounds);
            Assert.False(outOfBounds);
        }

        static Route GetDefaultRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, 5),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, 15),
                        ElapsedTime = 500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5, 15),
                        ElapsedTime = 1000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,5),
                        ElapsedTime = 1500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15,5),
                        ElapsedTime = 2000
                    }
                },
                Distance = 2000,
                Duration = 2000
            };
        }

        static Route GetSpecialTangentRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, 5),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, 10),
                        ElapsedTime = 500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5, 10),
                        ElapsedTime = 1000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,5),
                        ElapsedTime = 1500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15,5),
                        ElapsedTime = 2000
                    }
                },
                Distance = 2000,
                Duration = 2000
            };
        }

        static Route GetCornerRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(25, 0),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(20, 0),
                        ElapsedTime = 5000000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(25, 0),
                        ElapsedTime = 10000000
                    }
                },
                Distance = 10000000,
                Duration = 10000000
            };
        }

        static Route GetCloseToCellEdgeRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5, 5),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(20.06915, 5.03242),
                        ElapsedTime = 5000000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(20.16296, 9.99618),
                        ElapsedTime = 10000000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(16.52036, 5.57006),
                        ElapsedTime = 15000000
                    }
                },
                Distance = 15000000,
                Duration = 15000000
            };
        }

        static Route GetCloseToRasterEdge() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(19.9, 0.1),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(19.9, 19.9),
                        ElapsedTime = 500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(0.1, 19.9),
                        ElapsedTime = 1000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(0.1, 0.1),
                        ElapsedTime = 1500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(19.9, 0.1),
                        ElapsedTime = 2000
                    }
                },
                Distance = 2000,
                Duration = 2000
            };
        }

        static Route GetDiagonalRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(0, 0),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(10, 10),
                        ElapsedTime = 500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(20, 20),
                        ElapsedTime = 1000
                    }
                },
                Distance = 1000,
                Duration = 1000
            };
        }

        static Route GetOOBRoute() {
            return new Route() {
                Path = new List<GHPoint>()
                {
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, -5),
                        ElapsedTime = 0
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, 5),
                        ElapsedTime = 500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(25, 5),
                        ElapsedTime = 1000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(25, 15),
                        ElapsedTime = 1500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15,15),
                        ElapsedTime = 2000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15,25),
                        ElapsedTime = 2500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,25),
                        ElapsedTime = 3000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,15),
                        ElapsedTime = 3500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15,25),
                        ElapsedTime = 4000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,15),
                        ElapsedTime = 4500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(-5,15),
                        ElapsedTime = 5000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(-5,5),
                        ElapsedTime = 5500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,5),
                        ElapsedTime = 6000
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(5,-5),
                        ElapsedTime = 6500
                    },
                    new GHPoint()
                    {
                        Coordinate = new GeoCoordinate(15, -5),
                        ElapsedTime = 7000
                    }
                },
                Distance = 7000,
                Duration = 7000
            };
        }

        static IQueryable<RasterCell> GetCells() {
            //A square of cells with the following values
            //  5  |  0
            // ----------
            // 15  | 20
            IQueryable<RasterCell> cells = new List<RasterCell>{
                new RasterCell(){CellId = 0, SouthWestLatitude = 10, SouthWestLongitude = 0, NorthEastLatitude = 20, NorthEastLongitude = 10, Value = 5, CellIndex = 0},
                new RasterCell(){CellId = 1, SouthWestLatitude = 10, SouthWestLongitude = 10, NorthEastLatitude = 20, NorthEastLongitude = 20, Value = 0, CellIndex = 1},
                new RasterCell(){CellId = 2, SouthWestLatitude = 0, SouthWestLongitude = 0, NorthEastLatitude = 10, NorthEastLongitude = 10, Value = 15, CellIndex = 2},
                new RasterCell(){CellId = 3, SouthWestLatitude = 0, SouthWestLongitude = 10, NorthEastLatitude = 10, NorthEastLongitude = 20, Value = 20, CellIndex = 3}
            }.AsQueryable();

            return cells;
        }

        static Raster GetRaster() {
            int rasterId = 0;
            IQueryable<RasterCell> cells = GetCells();

            Raster raster = new Raster()
            {
                NoDataValue = -1,
                Id = rasterId,
                Cells = cells.ToList(),
                CellSize = 10,
                Height = 2,
                Width = 2,
                NorthEastLat = cells.First(x => x.CellIndex == 1).NorthEastLatitude,
                NorthEastLng = cells.First(x => x.CellIndex == 1).NorthEastLongitude,
                SouthWestLat = cells.First(x => x.CellIndex == 2).SouthWestLatitude,
                SouthWestLng = cells.First(x => x.CellIndex == 2).SouthWestLongitude,
                TimeStampPolled = DateTime.Today
            };

            foreach (RasterCell cell in cells) {
                cell.Raster = raster;
                cell.RasterId = rasterId;
            }

            return raster;
        }
    }
}
