﻿using DataAccess;
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Tests
{
    [TestFixture]
    class TestTrackingService
    {
        Mock<DatabaseContext> context;
        DateTime date;
        Guid userIdDefault, userIdNull, userIdEmpty;
        ITrackingService trackingService;

        [OneTimeSetUp]
        public void Setup()
        {
            context = new Mock<DatabaseContext>();

            userIdDefault = Guid.NewGuid();
            userIdNull = Guid.NewGuid();
            userIdEmpty = Guid.NewGuid();
            date = new DateTime(1996, 12, 29);
            IQueryable<TrackingSegment> segments = GetTrackingSegments(userIdDefault, date);

            MockHelper.GetUserContext(context, new List<User>() { new User()
            {
                Id = userIdDefault,
                TrackingSegments = segments.ToList()
            },
            new User()
            {
                Id = userIdNull,
                TrackingSegments = null
            },
            new User()
            {
                Id = userIdEmpty,
                TrackingSegments = new List<TrackingSegment>()
            }}.AsQueryable());

            MockHelper.GetTrackingContext(context, segments);

            Mock<IExposureService> exposureService = new Mock<IExposureService>();

            trackingService = new TrackingService(context.Object, exposureService.Object);
        }

        [Test]
        public void GetTrackingSegmentsByDay_CorrectCount()
        {
            //Act
            List<TrackingSegment> trackingSegments = trackingService.GetTrackingSegmentsByDay(userIdDefault, date);
            
            //Assert
            Assert.NotNull(trackingSegments);
            Assert.IsNotEmpty(trackingSegments);
            Assert.True(trackingSegments.Count == 3);
        }

        [Test]
        public void GetTrackingSegmentsByDay_InvalidUserId()
        {
            //Act
            List<TrackingSegment> trackingSegments = trackingService.GetTrackingSegmentsByDay(Guid.NewGuid(), date);

            //Assert
            Assert.IsNull(trackingSegments);
        }

        [Test]
        public void GetTrackingSegmentsByDay_NullResult()
        {
            //Act
            List<TrackingSegment> trackingSegments = trackingService.GetTrackingSegmentsByDay(userIdNull, date);

            //Assert
            Assert.IsNull(trackingSegments);
        }

        [Test]
        public void GetTrackingSegmentsByDay_EmptyResult()
        {
            //Act
            List<TrackingSegment> trackingSegments = trackingService.GetTrackingSegmentsByDay(userIdEmpty, date);

            //Assert
            Assert.NotNull(trackingSegments);
            Assert.IsEmpty(trackingSegments);
        }

        [Test]
        public void GetTrackingSegmentsByDay_NoDateTimeMatch()
        {
            //Act
            List<TrackingSegment> trackingSegments = trackingService.GetTrackingSegmentsByDay(userIdDefault, DateTime.MinValue);

            //Assert
            Assert.NotNull(trackingSegments);
            Assert.IsEmpty(trackingSegments);
        }

        private IQueryable<TrackingSegment> GetTrackingSegments(Guid userId, DateTime date)
        {
            int year = date.Year;
            int month = date.Month;
            int day = date.Day;

            DateTime dayAfter = date + new TimeSpan(24, 0, 0);

            return new List<TrackingSegment>()
            {
                new PlaceSegment()
                {
                    StartTime = new DateTime(dayAfter.Year, dayAfter.Month, dayAfter.Day, 11, 0, 0),
                    EndTime = new DateTime(dayAfter.Year, dayAfter.Month, dayAfter.Day, 12, 0, 0),
                    Location = new Location(52.097189, 5.219443),
                    Name = "Home",
                    UserId = userId,
                    Activities = new List<Activity>()
                },
                new PlaceSegment()
                {
                    StartTime = new DateTime(year, month, day, 11, 0, 0),
                    EndTime = new DateTime(year, month, day, 12, 0, 0),
                    Location = new Location(52.097189, 5.219443),
                    Name = "Home",
                    UserId = userId,
                    Activities = new List<Activity>()
                },
                new TrackingSegment()
                {
                    StartTime = new DateTime(year, month, day, 12, 0, 0),
                    EndTime = new DateTime(year, month, day, 12, 30, 0),
                    UserId = userId,
                    Activities = new List<Activity>()
                    {
                        new Activity()
                        {
                            StartTime = new DateTime(year, month, day, 12, 0, 0),
                            EndTime = new DateTime(year, month, day, 12, 5, 0),
                            ActivityType = ActivityType.Walk,
                            ActivityLabel = "walking",
                            TrackingPoints = new List<TrackingPoint>()
                            {
                                new TrackingPoint()
                                {
                                    Location = new Location(52.097189, 5.219443),
                                    TimeStamp = new DateTime(year, month, day, 12, 0, 0)
                                },

                                new TrackingPoint()
                                {
                                    Location = new Location(52.098717, 5.219783),
                                    TimeStamp = new DateTime(year, month, day, 12, 5, 0)
                                }
                            },
                            ExposurePath = new ExposurePath(){ Exposure = 300 }
                        },
                        new Activity()
                        {
                            StartTime = new DateTime(year, month, day, 12, 5, 0),
                            EndTime = new DateTime(year, month, day, 12, 30, 0),
                            ActivityType = ActivityType.Transport,
                            ActivityLabel = "bus",
                            TrackingPoints = new List<TrackingPoint>()
                            {
                                new TrackingPoint()
                                {
                                    Location = new Location(52.098717, 5.219783),
                                    TimeStamp = new DateTime(year, month, day, 12, 5, 0)
                                },

                                new TrackingPoint()
                                {
                                    Location = new Location(52.087242, 5.165763),
                                    TimeStamp = new DateTime(year, month, day, 12, 30, 0)
                                }
                            },
                            ExposurePath = new ExposurePath(){ Exposure = 60 }
                        },
                    }
                },
                new PlaceSegment()
                {
                    StartTime = new DateTime(year, month, day, 12, 30, 0),
                    EndTime = new DateTime(year, month, day, 15, 0, 0),
                    Location = new Location(52.087242, 5.165763),
                    Name = "Office",
                    UserId = userId,
                    Activities = new List<Activity>()
                    {
                        new Activity()
                        {
                            StartTime = new DateTime(year, month, day, 13, 0, 0),
                            EndTime = new DateTime(year, month, day, 13, 24, 0),
                            ActivityType = ActivityType.Walk,
                            ActivityLabel = "walking",
                            TrackingPoints = new List<TrackingPoint>()
                            {
                                new TrackingPoint()
                                {
                                    Location = new Location(52.087242, 5.165763),
                                    TimeStamp = new DateTime(year, month, day, 13, 0, 0)
                                },
                                new TrackingPoint()
                                {
                                    Location = new Location(52.084335, 5.171882),
                                    TimeStamp = new DateTime(year, month, day, 13, 12, 0)
                                },
                                new TrackingPoint()
                                {
                                    Location = new Location(52.087242, 5.165763),
                                    TimeStamp = new DateTime(year, month, day, 13, 24, 0)
                                },
                            },
                            ExposurePath = new ExposurePath(){ Exposure = 60 }
                        },
                    }
                }
            }.AsQueryable();
        }
    }
}
