/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Exposure;
using Domain.Tracking;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using TestUtils;
using System.Security.Cryptography;
using System.Text;
using System.Linq.Expressions;

namespace Services.Tests {
    [TestFixture]
    class TestUserService {
        private Mock<DatabaseContext> context;
        private Mock<IAuthenticationService> mockAuthenticationService;
        private IUserService userService;
        private Mock<IEmailService> mockEmailService;
        private ILogger<UserService> logger;
        private QuestionnaireConfigurationService questionnaireConfigurationService;

        [OneTimeSetUp]
        public void Setup() {
            context = new Mock<DatabaseContext>();
            logger = MockHelper.GetLogger<UserService>();
            mockAuthenticationService = new Mock<IAuthenticationService>();
            mockEmailService = new Mock<IEmailService>();
            // Make emailservice always return true (to indicate sending an email was successful)
            mockEmailService.SetReturnsDefault(true);
            questionnaireConfigurationService = new QuestionnaireConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/questionnairesettings.json");
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);
        }


        /// <summary>
        /// Tests the generateresettoken method with a valid input.
        /// Initial <see cref="PasswordResetToken"/> == <see langword="null"/>.
        /// </summary>
        [Test]
        public void User_GenerateResetToken_ValidInput() {
            // Arrange
            // Make emailservice always return true (to indicate sending an email was successful)
            mockEmailService.SetReturnsDefault(true);
            string email = "corne@vermeu.len";
            // Define user.
            User user = new User()
            {
                UserName = "FreekVonk",
                Email = email,
                PasswordResetToken = null
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act
            PasswordResetToken result = userService.GenerateResetToken(email);

            // Assert
            Assert.True(result != null);
        }

        /// <summary>
        /// When the token is expired, the method creates a new token.
        /// If done correctly, the result is not null.
        /// </summary>
        [Test]
        public void User_GenerateResetToken_ExpiredToken() {
            // Arrange
            // Make emailservice always return true (to indicate sending an email was successful)
            mockEmailService.SetReturnsDefault(true);
            string email = "corne@vermeu.len";

            User user = new User()
            {
                UserName = "FreekVonk",
                Email = email,
                PasswordResetToken = new PasswordResetToken()
                {
                    Token = "abcdefghijk",
                    ExpirationDate = DateTime.UtcNow.AddMinutes(-1)
                }
            };

            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act
            PasswordResetToken result = userService.GenerateResetToken(email);

            // Assert
            Assert.True(result != null);
        }

        /// <summary>
        /// User requests a new token while the old one has not yet expired.
        /// In this case the method should reject the request and return <see langword="null"/>.
        /// </summary>
        [Test]
        public void User_GenerateResetToken_NotExpiredToken() {
            // Arrange
            // Make emailservice always return true (to indicate sending an email was successful)
            mockEmailService.SetReturnsDefault(true);
            string email = "corne@vermeu.len";

            User user = new User()
            {
                UserName = "FreekVonk",
                Email = email,
                PasswordResetToken = new PasswordResetToken()
                {
                    ExpirationDate = DateTime.UtcNow.AddMinutes(10)
                }
            };

            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.GenerateResetToken(email));
        }

        /// <summary>
        /// Correctly handle the case where sending an email failed.
        /// Method should abort <see cref="PasswordResetToken"/> generation and return <see langword="null"/>.
        /// </summary>
        [Test]
        public void User_GenerateResetToken_ValidButEmailFailed() {
            // Arrange
            // Set default to false to imitate a failed sendEmail.
            mockEmailService.SetReturnsDefault(false);
            string email = "corne@vermeu.len";
            // Define user.
            User user = new User()
            {
                UserName = "FreekVonk",
                Email = email,
                PasswordResetToken = null
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.GenerateResetToken(email));
        }

        /// <summary>
        /// No user account registered to provided email.
        /// Should return <see langword="null"/>.
        /// </summary>
        [Test]
        public void User_GenerateResetToken_InvalidEmail() {
            // Arrange
            string requestedEmail = "Freekie@vo.nk";
            string userEmail = "corne@vermeu.len";
            // Define user.
            User user = new User()
            {
                UserName = "FreekVonk",
                Email = userEmail,
                PasswordResetToken = null
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.GenerateResetToken(requestedEmail));
        }

        /// <summary>
        /// Succesfully reset the user password:
        /// token is valid.
        /// newPassword and confirmPassword match password constraints.
        /// </summary>
        [Test]
        public void User_ResetPassword_ValidTokenAndPasswords() {
            // Arrange
            string token = "abcdefghijk";
            User user = new User()
            {
                UserName = "FronkVeek",
                PasswordResetToken = new PasswordResetToken()
                {
                    Token = token,
                    ExpirationDate = DateTime.UtcNow.AddMinutes(5)
                }
            };
            string newPassword = "freekhoudtvandiertjes";
            string confirmPassword = "freekhoudtvandiertjes";

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act 
            bool result = userService.ResetPassword(token.ToString(), newPassword, confirmPassword);

            // Assert
            Assert.True(result);
        }

        /// <summary>
        /// Unsuccessfully reset the user password.
        /// token is valid.
        /// newPassword and confirmPassword do not match constraints.
        /// </summary>
        [Test]
        public void User_ResetPassword_ValidTokenInvalidPasswords() {

            // Arrange
            string token = "abcdefghijk";
            User user = new User()
            {
                UserName = "FronkVeek",
                PasswordResetToken = new PasswordResetToken()
                {
                    Token = token,
                    ExpirationDate = DateTime.UtcNow.AddMinutes(5)
                }
            };
            string newPassword = "2short";
            string confirmPassword = "2short";

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.ResetPassword(token.ToString(), newPassword, confirmPassword));
        }

        /// <summary>
        /// Unsuccessfully reset the user password.
        /// token has expired. (<see cref="PasswordResetToken.ExpirationDate"/> is in the past.)
        /// newPassword and confirmPassword match constraints.
        /// </summary>
        [Test]
        public void User_ResetPassword_ExpiredToken() {
            // Arrange
            string token = "abcdefghijk";
            User user = new User()
            {
                UserName = "FronkVeek",
                PasswordResetToken = new PasswordResetToken()
                {
                    Token = token,
                    ExpirationDate = DateTime.UtcNow.AddMinutes(-5)
                }
            };
            string newPassword = "freekhoudtvandiertjes";
            string confirmPassword = "freekhoudtvandiertjes";

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.ResetPassword(token.ToString(), newPassword, confirmPassword));
        }

        /// <summary>
        /// Unsuccessfully reset the user password.
        /// token is valid.
        /// newPassword and confirmPassword do not match constraints. (both are <see langword="null"/>.)
        /// </summary>
        [Test]
        public void User_ResetPassword_Emptypasswords() {
            // Arrange
            string token = "abcdefhijk";
            User user = new User()
            {
                UserName = "FronkVeek",
                PasswordResetToken = new PasswordResetToken()
                {
                    Token = token,
                    ExpirationDate = DateTime.UtcNow.AddMinutes(5)
                }
            };
            string newPassword = null;
            string confirmPassword = null;

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.ResetPassword(token.ToString(), newPassword, confirmPassword));
        }

        /// <summary>
        /// Request a password reset with a token that is not bound to any user.
        /// Result should be false.
        /// </summary>
        [Test]
        public void User_ResetPassword_InvalidUser() {
            // Arrange
            Guid token = Guid.NewGuid();
            User user = new User()
            {
                UserName = "FronkVeek"
            };
            string newPassword = "whatisgoingon";
            string confirmPassword = "whatisgoingon";

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());

            // Act && Assert
            Assert.Throws<Exception>(() => userService.ResetPassword(token.ToString(), newPassword, confirmPassword));
        }

        [Test]
        public void User_UpdatePassword_ValidUserAndPasswordPair() {
            // Arrange
            Guid Id = Guid.NewGuid();
            PasswordPair passwordPair = new PasswordPair()
            {
                OldPassword = "aapje",
                NewPassword = "monkeylikesbananas"
            };
            byte[] oldPassword = PasswordHasher(passwordPair.OldPassword, Id);

            User user = new User()
            {
                Id = Id,
                Salt = Id,
                HashedPassword = oldPassword
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            // Make authenticationservice hashpassword return same byte[] as byte[] created in this test class. Used to verify stored password is same as verification password.
            mockAuthenticationService.Setup(x => x.HashPassword(It.IsAny<string>(), It.IsAny<Guid>())).Returns(oldPassword);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act
            PasswordPair result = userService.UpdatePassword(Id, passwordPair);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(passwordPair, result);
        }

        [Test]
        public void User_UpdatePassword_ValidUser_ValidOldPassword_InvalidNewPassword() {
            // Arrange
            Guid Id = Guid.NewGuid();
            PasswordPair passwordPair = new PasswordPair()
            {
                OldPassword = "aapje",
                NewPassword = "2s"
            };
            byte[] oldPassword = PasswordHasher(passwordPair.OldPassword, Id);

            User user = new User()
            {
                Id = Id,
                Salt = Id,
                HashedPassword = oldPassword
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            // Make authenticationservice hashpassword return same byte[] as byte[] created in this test class. Used to verify stored password is same as verification password.
            mockAuthenticationService.Setup(x => x.HashPassword(It.IsAny<string>(), It.IsAny<Guid>())).Returns(oldPassword);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act && Assert            
            Assert.Throws<Exception>(() => userService.UpdatePassword(Id, passwordPair));
        }

        [Test]
        public void UpdatePassword_ValidUser_InvalidOldPassword_ValidNewPassword() {
            // Arrange
            Guid Id = Guid.NewGuid();
            PasswordPair passwordPair = new PasswordPair()
            {
                OldPassword = "aapje",
                NewPassword = "monkeylikesbananas"
            };
            byte[] nonMatchingPassword = PasswordHasher("notaapje", Id);

            User user = new User()
            {
                Id = Id,
                Salt = Id,
                HashedPassword = nonMatchingPassword
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            // Make authenticationservice hashpassword return different byte[] than byte[] created in this test class. Used to verify stored password is same as verification password, in this case NOT EQUAL.
            mockAuthenticationService.Setup(x => x.HashPassword(It.IsAny<string>(), It.IsAny<Guid>())).Returns(PasswordHasher(passwordPair.OldPassword, Id));
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act && Assert
            Assert.Throws<Exception>(() => userService.UpdatePassword(Id, passwordPair));
        }

        [Test]
        public void UpdatePassword_UserNotExists() {
            // Arrange
            Guid Id = Guid.NewGuid();
            Guid differentId = Guid.NewGuid();
            PasswordPair passwordPair = new PasswordPair()
            {
                OldPassword = "aapje",
                NewPassword = "monkeylikesbananas"
            };
            byte[] oldPassword = PasswordHasher(passwordPair.OldPassword, Id);

            User user = new User()
            {
                Id = Id,
                Salt = Id,
                HashedPassword = oldPassword
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            // Make context.Users.Find return null, because user does not exist in context.
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns((User)null);
            // Make authenticationservice hashpassword return same byte[] as byte[] created in this test class. Used to verify stored password is same as verification password.
            mockAuthenticationService.Setup(x => x.HashPassword(It.IsAny<string>(), It.IsAny<Guid>())).Returns(oldPassword);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);


            // Act && Assert
            Assert.Throws<Exception>(() => userService.UpdatePassword(differentId, passwordPair));
        }

        /// <summary>
        /// Verify user exists before creating a <see cref="UserProfile"/> from the <see cref="User"/>.
        /// </summary>
        [Test]
        public void User_GetUserProfile_Exists() {
            // Arrange
            Guid Id = Guid.NewGuid();
            string email = "hoi@piepel.oi";
            User user = new User()
            {
                Email = email,
                UserName = "FreekVonk",
                PasswordResetToken = null
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act 
            UserProfile result = userService.GetUserProfile(Id);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Email == email);
        }

        /// <summary>
        /// Verify user does not exists before returning <see langword="null"/>
        /// </summary>
        [Test]
        public void User_GetUserProfile_NotExists() {
            // Arrange
            Guid Id = Guid.NewGuid();
            string email = "hoi@piepel.oi";
            User user = new User()
            {
                Email = email,
                UserName = "FreekVonk",
                PasswordResetToken = null
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            // Make context return no user because Guid is invalid.
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns((User)null);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act 
            UserProfile result = userService.GetUserProfile(Id);

            // Assert
            Assert.Null(result);
        }

        [Test]
        public void User_UpdateUserProfile_UserExists_InvalidEmail() {
            // Arrange
            Guid Id = Guid.NewGuid();
            UserProfile userProfile = new UserProfile()
            {
                Username = "kikkererwtje",
                Email = "fork",
                TrackLocation = true
            };

            User user = new User()
            {
                Id = Id,
                UserName = userProfile.Username,
                Email = "koele@ema.il",
                TrackLocation = false
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act && Assert
            Assert.Throws<Exception>(() => userService.UpdateUserProfile(Id, userProfile), "Failed to update profile.");
        }

        [Test]
        public void User_UpdateUserProfile_UserNotExists_ValidProfile() {
            // Arrange
            Guid Id = Guid.NewGuid();
            UserProfile userProfile = new UserProfile()
            {
                Username = "kikkererwtje",
                Email = "kikker@dri.ll",
                TrackLocation = true
            };

            User user = new User()
            {
                Id = Id,
                UserName = userProfile.Username,
                Email = "koele@ema.il",
                TrackLocation = false
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns((User)null);
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            // Return null because user does NOT EXIST.
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act && Assert
            Assert.Throws<Exception>(() => userService.UpdateUserProfile(Id, userProfile), "Failed to update profile.");
        }

        [Test]
        public void User_UpdateUserProfile_UserExists_NullProfile() {
            // Arrange
            Guid Id = Guid.NewGuid();
            UserProfile userProfile = new UserProfile();

            User user = new User()
            {
                Id = Id,
                UserName = userProfile.Username,
                Email = "koele@ema.il",
                TrackLocation = false
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act && Assert
            Assert.Throws<Exception>(() => userService.UpdateUserProfile(Id, userProfile), "Failed to update profile.");
        }

        [Test]
        public void User_CreateNewUser_InValidRegistrations() {
            // Arrange
            UserRegistration userRegistration1 = new UserRegistration()
            {
                Username = null,
                Password = "jetante",
                Email = "",
                TrackLocation = true
            };
            UserRegistration userRegistration2 = new UserRegistration()
            {
                Username = "aapje",
                Password = null,
                Email = "",
                TrackLocation = true
            };
            UserRegistration userRegistration3 = new UserRegistration()
            {
                Username = "s p a c e s",
                Password = "",
                Email = "",
                TrackLocation = true
            };
            UserRegistration userRegistration4 = new UserRegistration()
            {
                Username = "hoi",
                Password = "aapjeee",
                Email = null,
                TrackLocation = true
            };
            UserRegistration userRegistration5 = new UserRegistration()
            {
                Username = "abc",
                Password = "",
                Email = "",
                TrackLocation = true
            };
            UserRegistration userRegistration6 = new UserRegistration()
            {
                Username = "abcdefghijklmnopqrstuvwxyz",
                Password = "",
                Email = "",
                TrackLocation = true
            };

            // Act
            User result = new User();
            bool test1 = false; bool test2 = false; bool test3 = false; bool test4 = false; bool test5 = false; bool test6 = false;

            try { userService.CreateNewUser(userRegistration1); }
            catch { test1 = true; }
            try { userService.CreateNewUser(userRegistration2); }
            catch { test2 = true; }
            try { userService.CreateNewUser(userRegistration3); }
            catch { test3 = true; }
            try { userService.CreateNewUser(userRegistration4); }
            catch { test4 = true; }
            try { userService.CreateNewUser(userRegistration5); }
            catch { test5 = true; }
            try { userService.CreateNewUser(userRegistration6); }
            catch { test6 = true; }


            // Assert
            Assert.True(test1);
            Assert.True(test2);
            Assert.True(test3);
            Assert.True(test4);
            Assert.True(test5);
            Assert.True(test6);
        }

        [Test]
        public void User_DeleteEntireUser_UserExists() {
            // Arrange
            Guid Id = Guid.NewGuid();
            User user = new User()
            {
                Id = Id,
                UserName = "deleteme",
                Email = "koele@ema.il"
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns(user);
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act
            bool result = userService.DeleteEntireUser(Id);

            // Assert
            Assert.True(result);
        }

        [Test]
        public void User_DeleteEntireUser_UserNotExists() {
            // Arrange
            Guid Id = Guid.NewGuid();
            User user = new User()
            {
                Id = Id,
                UserName = "deleteme",
                Email = "koele@ema.il"
            };

            // Setup context.
            List<User> users = new List<User> { user };
            context.Setup(c => c.Users.Find(It.IsAny<Guid>())).Returns((User)null);
            context = MockHelper.GetUserContext(context, users.AsQueryable());
            userService = new UserService(mockAuthenticationService.Object, context.Object, logger, questionnaireConfigurationService, mockEmailService.Object);

            // Act
            bool result = userService.DeleteEntireUser(Id);

            // Assert
            Assert.False(result);
        }

        private byte[] PasswordHasher(string password, Guid salt) {
            SHA256 hasher = SHA256.Create();
            return hasher.ComputeHash(Encoding.UTF8.GetBytes(password)).Concat(BitConverter.GetBytes(salt.GetHashCode())).ToArray();
        }
    }
}
