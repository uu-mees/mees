/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Tracking;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestUtils;

namespace Services.Tests {
    [TestFixture]
    class TestTriggerService {
        Mock<DatabaseContext> context;
        QuestionnaireConfigurationService questionnaireConfigurationService;
        TriggersConfigurationService triggersConfigurationService;

        [OneTimeSetUp]
        public void Setup() {
            context = new Mock<DatabaseContext>();
            questionnaireConfigurationService = new QuestionnaireConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/questionnairesettings.json");
            triggersConfigurationService = new TriggersConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/triggersettings.json");
        }

        [Test]
        public void ExposureTrigger_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void ExposureTrigger_OneLocation_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void ExposureTrigger_TwoLocation_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void ExposureTrigger_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void ExposureTrigger_OneLocation_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 30, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void ExposureTrigger_TwoLocation_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 19, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void ExposureTrigger_AllOutOfTimeRange_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2019, 1, 1) }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void ExposureTrigger_ContainsExposureIsNull_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = null, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void ExposureTrigger_OrderTime_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = new DateTime(2019, 1, 1) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = new DateTime(2019, 3, 3) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = new DateTime(2018, 8, 12) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = new DateTime(2016, 12, 31) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.ExposureTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void DistanceTrigger_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.1234), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.1111), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.23), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.234), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2344), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2346), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2348), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.235), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.234), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2345), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2347), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.235), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.23), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2), Exposure = 60, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.21), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.24), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void DistanceTrigger_TwoLocations_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2360), Exposure = 40, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void DistanceTrigger_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void DistanceTrigger_OneLocation_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void DistanceTrigger_TwoLocations_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2344), Exposure = 40, Timestamp = DateTime.UtcNow }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void DistanceTrigger_OrderList_True() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.1234), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.1111), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 45, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 55, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.23), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 1, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.234), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 9, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 40, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 23, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2344), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 12, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 32, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2346), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 44, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2348), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 5, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.235), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 7, 21)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 52, 3)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.234), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 37, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2345), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 11, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2347), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 14, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.235), Exposure = 60, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 3, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.23), Exposure = 60, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 7, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2), Exposure = 60, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 43, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.21), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 29, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.24), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 2, 50)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 1, 20)) }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsTrue(returnValue);
        }

        [Test]
        public void DistanceTrigger_LocationOutOfTimeRange_False() {
            Guid userId = Guid.NewGuid();
            UserLocationExposure[] array = new UserLocationExposure[] {
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(0, 50, 0)) },
                new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 50, 0)) }
            };
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            bool returnValue = triggerService.DistanceTrigger(array);
            Assert.IsFalse(returnValue);
        }

        [Test]
        public void TimeSlot_Null() {
            DateTime? dateTime = null;
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            string result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "null");
        }

        [Test]
        public void TimeSlot_First() {
            DateTime? dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 7, 0, 0);
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            string result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "first");
            dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 8, 0, 0);
            result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "first");
        }

        [Test]
        public void TimeSlot_Second() {
            DateTime? dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 11, 0, 0);
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            string result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "second");
            dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 11, 0, 0);
            result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "second");
        }

        [Test]
        public void TimeSlot_Third() {
            DateTime? dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 14, 0, 0); ;
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            string result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "third");
            dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 0, 0);
            result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "third");
        }

        [Test]
        public void TimeSlot_None() {
            DateTime? dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0);
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            string result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "none");
            dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 22, 0, 0);
            result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "none");
            dateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 3, 0, 0);
            result = triggerService.getTimeslot(dateTime);
            Assert.AreEqual(result, "none");
        }
    }
}
