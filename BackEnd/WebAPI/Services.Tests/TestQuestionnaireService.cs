/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain.Administration;
using Domain.Questionnaire;
using Domain.Tracking;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;

namespace Services.Tests {
    [TestFixture]
    class TestQuestionnaireService {
        Mock<DatabaseContext> context;
        QuestionnaireConfigurationService questionnaireConfigurationService;
        TriggersConfigurationService triggersConfigurationService;

        [OneTimeSetUp]
        public void Setup() {
            context = new Mock<DatabaseContext>();
            MockHelper.GetUserContext(context, new List<User>().AsQueryable());
            MockHelper.GetQuestionnaireContext(context, new List<Questionnaire>().AsQueryable());
            MockHelper.GetTriggerContext(context, new List<Trigger>().AsQueryable());
            questionnaireConfigurationService = new QuestionnaireConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/questionnairesettings.json");
            triggersConfigurationService = new TriggersConfigurationService(TestContext.CurrentContext.TestDirectory + "/../webapi/webapi/triggersettings.json");
        }

        [Test]
        public void CheckForQuestionnaire_NoQuestionnaire() {
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            QuestionnaireService questionnaireService = new QuestionnaireService(context.Object, questionnaireConfigurationService, triggerService);

            Questionnaire questionnaire = questionnaireService.CheckForQuestionnaire(new Guid());

            Assert.IsNull(questionnaire);
        }

        //[Test]
        //public void CheckForQuestionnaire_DefaultQuestionnaire() {
        //    Guid userId = new Guid();
        //    DefaultTrigger defaultTrigger = new DefaultTrigger(){ Priority = 2, TimeStamp = null, TriggerId = new Guid(), UserId = userId };
        //    DistanceTrigger distanceTrigger = new DistanceTrigger(){ Priority = 2, TimeStamp = null, TriggerId = new Guid(), UserId = userId };
        //    ExposureTrigger exposureTrigger = new ExposureTrigger(){ Priority = 2, TimeStamp = null, TriggerId = new Guid(), UserId = userId };
        //    Questionnaire defaultQuestionnaire = new Questionnaire() {
        //        QuestionnaireId = new Guid(),
        //        QuestionnaireString = "test",
        //        Trigger = defaultTrigger
        //    };
        //    Questionnaire distanceQuestionnaire = new Questionnaire() {
        //        QuestionnaireId = new Guid(),
        //        QuestionnaireString = "test",
        //        Trigger = distanceTrigger
        //    };
        //    Questionnaire exposureQuestionnaire = new Questionnaire() {
        //        QuestionnaireId = new Guid(),
        //        QuestionnaireString = "test",
        //        Trigger = exposureTrigger
        //    };
        //    User user = new User(){
        //        Id = userId,
        //        Triggers = new List<Trigger>() { defaultTrigger, distanceTrigger, exposureTrigger}
        //    };
        //    UserLocationExposure[] array = new UserLocationExposure[] {
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow },
        //        new UserLocationExposure { UserID = userId, Position = new Location(52.3423, 5.2342), Exposure = 20, Timestamp = DateTime.UtcNow }
        //    };
        //    MockHelper.GetUserContext(context, new List<User>() { user }.AsQueryable());
        //    MockHelper.GetQuestionnaireContext(context, new List<Questionnaire>() { defaultQuestionnaire, distanceQuestionnaire, exposureQuestionnaire }.AsQueryable());
        //    MockHelper.GetLocationExposuresContext(context, array.AsQueryable());
        //    TriggerService triggerService = new TriggerService(context.Object, null);
        //    QuestionnaireService questionnaireService = new QuestionnaireService(context.Object, questionnaireConfigurationService, triggerService);

        //    Questionnaire questionnaire = questionnaireService.CheckForQuestionnaire(userId);
        //    Assert.IsTrue(questionnaire.Trigger is DefaultTrigger);
        //}

        [Test]
        public void GetAllQuestionnaires_CorrectResult() {
            Questionnaire questionnaire = new Questionnaire()
            {
                QuestionnaireId = new Guid(),
                QuestionnaireString = "test",
                Trigger = null
            };
            MockHelper.GetQuestionnaireContext(context, new List<Questionnaire>() { questionnaire }.AsQueryable());
            TriggerService triggerService = new TriggerService(context.Object, null, questionnaireConfigurationService, triggersConfigurationService);
            QuestionnaireService questionnaireService = new QuestionnaireService(context.Object, questionnaireConfigurationService, triggerService);

            List<Questionnaire> questionnaires = questionnaireService.GetAllQuestionnaires();

            Assert.NotNull(questionnaire);
            Assert.Contains(questionnaire, questionnaires);
        }

        [Test]
        public void GetConfigurationJSON_CorrectResult() {
            string result = questionnaireConfigurationService.GetConfigurationJSON();

            Assert.NotNull(result);
            Assert.AreNotEqual("", result);
        }
        [Test]
        public void UpdateConfiguration_NoUpdate() {
            questionnaireConfigurationService.UpdateConfiguration(null);

            // If it tried to write a config it would crash, if it did not we succeed.
            Assert.Pass();
        }
    }
}
