/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain;
using Moq;
using NUnit.Framework;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using TestUtils;
using Domain.Exposure;
using System.Device.Location;
using Microsoft.Extensions.Logging;
using Services.DTOs;
using Services.Interfaces.Pollers;
using System.Drawing;

namespace Services.Tests {
    [TestFixture]
    class TestRasterService {
        Mock<DatabaseContext> context;
        ILogger<RasterService> logger;

        [OneTimeSetUp]
        public void Setup() {
            context = new Mock<DatabaseContext>();
            logger = MockHelper.GetLogger<RasterService>();
        }

        [Test]
        public void Raster_LatestTimeStamp() {
            //Arrange
            MockHelper.GetRasterContext(context, ObjHelper.GetRastersWithTimeStamp());
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(context.Object.Rasters.Last());

            IRasterService rasterService = new RasterService(context.Object, logger, container.Object);

            //Act
            DateTime time = rasterService.GetLatestRasterTimeStamp();

            //Assert
            Assert.AreEqual(DateTime.MaxValue, time);
        }

        [Test]
        public void Raster_LatestTimeStampWithoutRaster() {
            //Arrange
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer();
            IRasterService rasterService = new RasterService(null, logger, container.Object);

            //Act
            DateTime time = rasterService.GetLatestRasterTimeStamp();

            //Assert
            Assert.AreEqual(DateTime.MinValue, time);
        }

        [Test]
        public void RasterCell_NullRaster() {
            //Arrange
            Mock<IRasterContainer> container = MockHelper.GetRasterContainer();
            IRasterService rasterService = new RasterService(null, logger, container.Object);

            //Act
            RasterCell result = rasterService.GetRasterCell(0, new GeoCoordinate(0, 0));

            //Assert
            Assert.Null(result);
        }

        [Test]
        public void RasterCell_IsEmpty() {
            //Arrange
            IQueryable<RasterCell> cells = ObjHelper.GetEmptyRasterCells();
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, 100);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());
            IRasterService rasterService = new RasterService(null, logger, container.Object);
            GeoCoordinate geoCoordinate = new GeoCoordinate(10, 10);

            //Act
            RasterCell queried = rasterService.GetRasterCell(raster.First().Id, geoCoordinate);

            //Assert
            Assert.IsNull(queried);
        }

        [Test]
        public void RasterCell_NotEmpty() {
            //Arrange
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);

            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());
            IRasterService rasterService = new RasterService(context.Object, logger, container.Object);
            GeoCoordinate geoCoordinate = new GeoCoordinate(10, 10);

            //Act
            RasterCell queried = rasterService.GetRasterCell(raster.First().Id, geoCoordinate);

            //Assert
            Assert.IsNull(queried);
        }

        [Test]
        public void GetExposureFromLocation_ValidResult() {
            //Arrange
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);
            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());
            IRasterService rasterService = new RasterService(context.Object, logger, container.Object);
            GeoCoordinate geoCoordinate = new GeoCoordinate(7, 7);

            //Act
            double? exposure = rasterService.GetExposureFromLocation(geoCoordinate);

            //Assert
            Assert.NotNull(exposure);
            Assert.AreEqual(exposure, 4.123456f);
        }

        [Test]
        public void GetExposureFromLocation_OutOfBounds() {
            //Arrange
            float cellSize = 125;
            IQueryable<RasterCell> cells = ObjHelper.GetBasicRasterCells(cellSize);
            IQueryable<Raster> raster = ObjHelper.GetRaster(cells, cellSize);

            context = MockHelper.GetRasterCellContext(context, cells);
            context = MockHelper.GetRasterContext(context, raster);

            Mock<IRasterContainer> container = MockHelper.GetRasterContainer(raster.Last());
            IRasterService rasterService = new RasterService(context.Object, logger, container.Object);
            GeoCoordinate geoCoordinate = new GeoCoordinate(10, 10);

            //Act
            double? exposure = rasterService.GetExposureFromLocation(geoCoordinate);

            //Assert
            Assert.IsNull(exposure);
        }

        [Test]
        public void Raster_NoData() {
            //Arrange
            Mock<IRasterContainer> container = new Mock<IRasterContainer>();
            container.Setup(x => x.GetRasterImage()).Returns((RasterDTO)null);

            IRasterService rasterService = new RasterService(context.Object, logger, container.Object);

            //Act
            RasterDTO queried = rasterService.GetRasterImage();

            //Assert
            Assert.IsNull(queried);
        }

        [Test]
        public void Raster_GetClosestInTime() {
            //Arrange
            context = MockHelper.GetRasterContext(context, new List<Raster>()
            {
                new Raster() { TimeStampPolled = DateTime.MinValue },
                new Raster() { TimeStampPolled = DateTime.UtcNow, Width = 100 },
                new Raster() { TimeStampPolled = DateTime.MaxValue }
            }.AsQueryable());

            IRasterService rasterService = new RasterService(context.Object, logger, null);

            //Act
            Raster raster = rasterService.GetClosestHistoricalRaster(DateTime.UtcNow);

            //Assert
            Assert.AreEqual(100, raster.Width);
        }

        [Test]
        public void Raster_ImageCopiesData() {
            //Arrange
            RasterContainer container = new RasterContainer();
            IRasterService rasterService = new RasterService(null, logger, container);
            Raster raster = GetCompleteRaster();

            //Act
            container.UpdateRaster(raster);
            RasterDTO dto = container.GetRasterImage();

            //Assert
            Assert.AreEqual(raster.TimeStampPolled, dto.TimeStampPolled);
            Assert.AreEqual(raster.NorthEastLat, dto.NorthEastLatitude);
            Assert.AreEqual(raster.NorthEastLng, dto.NorthEastLongitude);
            Assert.AreEqual(raster.SouthWestLat, dto.SouthWestLatitude);
            Assert.AreEqual(raster.SouthWestLng, dto.SouthWestLongitude);
            Assert.NotNull(dto.Image);
        }

        private Raster GetCompleteRaster() {
            return new Raster() {
                TimeStampPolled = DateTime.UtcNow,
                NorthEastLat = 10,
                NorthEastLng = 20,
                SouthWestLat = 30,
                SouthWestLng = 40,
                Cells = new List<RasterCell>()
                {
                    new RasterCell() { Value = 50 },
                    new RasterCell() { Value = 100 },
                    new RasterCell() { Value = 150 },
                    new RasterCell() { Value = 200 }
                },
                Height = 2,
                Width = 2
            };
        }

        [Test]
        public void Raster_GetColor() {

            //Arrange
            RasterContainer container = new RasterContainer();

            //Act & Assert
            Assert.AreEqual(Color.Blue.ToArgb(), container.GetColor(-1f).ToArgb());
            Assert.AreEqual(Color.Blue.ToArgb(), container.GetColor(0f).ToArgb());
            Assert.AreEqual((255 << 24) + (0 << 16) + (191 << 8) + 255, container.GetColor(20f).ToArgb());
            Assert.AreEqual(Color.White.ToArgb(), container.GetColor(40f).ToArgb());
            Assert.AreEqual((255 << 24) + (255 << 16) + (191 << 8) + 0, container.GetColor(60f).ToArgb());
            Assert.AreEqual(Color.Red.ToArgb(), container.GetColor(80f).ToArgb());
            Assert.AreEqual(Color.Red.ToArgb(), container.GetColor(200f).ToArgb());
        }

        [Test]
        public void Raster_FloatToByte() {
            //Arrange

            //Act & Assert
            Assert.AreEqual(RasterContainer.FloatToByte(-1f), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(-0.1f), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(0f), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(0.1f), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(0.5f), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(0.9f), 0);

            Assert.AreEqual(RasterContainer.FloatToByte(1f), 1);
            Assert.AreEqual(RasterContainer.FloatToByte(254.9f), 254);
            Assert.AreEqual(RasterContainer.FloatToByte(255f), 255);
            Assert.AreEqual(RasterContainer.FloatToByte(256f), 255);

            Assert.AreEqual(RasterContainer.FloatToByte(float.MinValue), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(float.NegativeInfinity), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(float.Epsilon), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(float.NaN), 0);
            Assert.AreEqual(RasterContainer.FloatToByte(float.MaxValue), 255);
            Assert.AreEqual(RasterContainer.FloatToByte(float.PositiveInfinity), 255);
        }
    }
}
