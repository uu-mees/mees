/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using Services.Interfaces;
using Services.Implementations;
using Domain;
using Services.Retrievers;
using System.Diagnostics;
using Domain.Administration;

namespace Services.Tests {
    class TestPollingService {

        [Test]
        public void Polling_CorrectThreadStart() {
            // Arrange
            IPollingService<User> pollingService;
            Mock<IRetriever<User>> retrieverMock = new Mock<IRetriever<User>>(); // The retriever can be of any type, user is just an example
            retrieverMock.Setup(x => x.RetrieveData())
                .Returns(() => new User());
            pollingService = new PollingService<User>();
            int threadCount = Process.GetCurrentProcess().Threads.Count;

            // Act
            pollingService.StartPolling(999, (a, b) => { }, retrieverMock.Object);

            // Assert
            Assert.IsTrue(threadCount + 1 == Process.GetCurrentProcess().Threads.Count);
        }

    }
}
