/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Domain;
using Domain.Routing;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.DTOs;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;

namespace Services.Tests {
    [TestFixture]
    class TestGeocodingService {
        private IGeocodingService geocodingService;
        private ILogger<GeocodingService> logger;

        [OneTimeSetUp]
        public void Setup() {
            logger = MockHelper.GetLogger<GeocodingService>();
            ConfigurationService configurationService = new ConfigurationService()
            {
                GeocodeConfiguration = new GeocodeConfiguration()
                {
                    ApiUrl = "https://maps.googleapis.com/maps/api/geocode/",
                    ApiKey = "AIzaSyDyBHlmFMqkW0G-POJhSiECMB_ZOFfkkcQ",
                    ResultType = "json"
                }
            };
            geocodingService = new GeocodingService(configurationService, logger);
        }

        [Test]
        [Category("Integration")]
        public void GeocodingResponse_Geocode_NoResults() {
            //Arrange
            string address = "sdfkljnsadfuwea8aw30840a92r90ajfa9309fa32fa23f32";

            //Act
            GeocodingResponse response = geocodingService.Geocode(address);
            //Assert            
            Assert.IsEmpty(response.Results);
        }

        [Test]
        [Category("Integration")]
        public void GeocodingResponse_Geocode_ValidAddress() {
            //Arrange
            string address = "Netherlands";
            //Act
            GeocodingResponse response = geocodingService.Geocode(address);
            //Assert            
            Assert.IsNotEmpty(response.Results);
            Assert.True(response.Results.TrueForAll(x => x.FormattedAddress != null && x.Location != null));
        }

        [Test]
        [Category("Integration")]
        public void GeocodingResponse_ReverseGeocode_ValidQuery() {
            //Arrange
            //Buys Ballot Gebouw
            double lat = 52.0873639, lng = 5.165364299999999;
            //Act
            GeocodingResponse response = geocodingService.ReverseGeocode(lat, lng);
            //Assert            
            Assert.IsNotEmpty(response.Results);
            Assert.True(response.Results.TrueForAll(x => x.FormattedAddress != null && x.Location != null));
        }

        [Test]
        [Category("Integration")]
        public void GeocodingResponse_ReverseGeocode_NoResults() {
            //Arrange
            double lat = 90, lng = 180;
            //Act
            GeocodingResponse response = geocodingService.ReverseGeocode(lat, lng);
            //Assert            
            Assert.IsEmpty(response.Results);
        }

        [Test]
        [Category("Integration")]
        public void GeocodingResponse_ExceptionTest() {
            //Arrange
            double lat = 91, lng = 181;
            //Act
            TestDelegate invalidQuery = () => geocodingService.ReverseGeocode(lat, lng);
            //Assert            
            Assert.Throws<GeocodingException>(invalidQuery);
        }
    }
}
