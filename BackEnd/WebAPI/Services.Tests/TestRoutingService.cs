/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Services.DTOs;
using Services.Implementations;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;

namespace Services.Tests {
    [TestFixture]
    class TestRoutingService {
        IRoutingService routingService;
        ILogger<RoutingService> logger;

        [OneTimeSetUp]
        public void Setup() {
            ConfigurationService configuration = new ConfigurationService()
            {
                RoutingConfiguration = new RoutingConfiguration()
                {
                    ApiUrl = "http://localhost:8080/"
                }
            };

            logger = MockHelper.GetLogger<RoutingService>();
            Mock<IExposureService> exposureService = new Mock<IExposureService>();
            routingService = new RoutingService(configuration, exposureService.Object, logger);
        }

        [Test]
        [Category("Integration")]
        public void RoutingResponse_GetRoutingResponse_InvalidVehicle() {
            //Arrange
            GeoCoordinate start = new GeoCoordinate(52.08720, 5.16546);
            GeoCoordinate destination = new GeoCoordinate(52.08597, 5.17217);
            //Act
            TestDelegate invalidVehicle = () => routingService.GetRoutingResponse(start
                                                                              , destination
                                                                              , "this_is_not_actually_a_valid_vehicle"
                                                                              , "shortest"
                                                                              );

            //Assert
            Assert.Throws<RoutingException>(invalidVehicle);
        }

        [Test]
        [Category("Integration")]
        public void RoutingResponse_GetRoutingResponse_InvalidWeighting() {
            //Arrange
            GeoCoordinate start = new GeoCoordinate(52.08720, 5.16546);
            GeoCoordinate destination = new GeoCoordinate(52.08597, 5.17217);
            //Act
            TestDelegate invalidWeighting = () => routingService.GetRoutingResponse(start
                                                                              , destination
                                                                              , "bike"
                                                                              , "this_is_not_actually_valid_weighting"
                                                                              );

            //Assert
            Assert.Throws<RoutingException>(invalidWeighting);
        }

        //TODO: Implement Graphhopper in testing pipeline
        //[Test]
        //[Category("Integration")]
        //public void RoutingResponse_GetRoutingResponse_OutOfTargetZone()
        //{
        //    //Arrange
        //    Mock<DatabaseContext> context = new Mock<DatabaseContext>();
        //    IRasterService rasterService = new RasterService(context.Object);
        //    IRouteExposureService exposureService = new RouteExposureService(rasterService);
        //    IRoutingService routingService = new RoutingService(exposureService);

        //    //Coordinates in New York
        //    GeoCoordinate start = new GeoCoordinate(40.73696, -73.98451);
        //    GeoCoordinate destination = new GeoCoordinate(40.73603, -73.98227);

        //    //Act
        //    RoutingResponseDTO response = routingService.GetRoutingResponse(start
        //                                                                    , destination
        //                                                                    , "bike"
        //                                                                    , "shortest"
        //                                                                    );

        //    //Assert
        //    Assert.NotNull(response);
        //    Assert.IsEmpty(response.Routes);
        //    Assert.IsTrue(response.Count == 0);
        //    Assert.IsTrue(response.Vehicle == "bike");
        //    Assert.IsTrue(response.Weighting == "shortest");
        //}

        //[Test]
        //[Category("Integration")]
        //public void RoutingResponse_GetRoutingResponse_ValidRequest()
        //{
        //    //Arrange
        //    Mock<DatabaseContext> context = new Mock<DatabaseContext>();
        //    IRasterService rasterService = new RasterService(context.Object);
        //    IRouteExposureService exposureService = new RouteExposureService(rasterService);
        //    IRoutingService routingService = new RoutingService(exposureService);

        //    //Coordinates in USP
        //    GeoCoordinate start = new GeoCoordinate(52.08720, 5.16546);
        //    GeoCoordinate destination = new GeoCoordinate(52.08597, 5.17217);

        //    //Act
        //    RoutingResponseDTO response = routingService.GetRoutingResponse(start
        //                                                                    , destination
        //                                                                    , "bike"
        //                                                                    , "shortest"
        //                                                                    );

        //    //Assert
        //    Assert.NotNull(response);
        //    Assert.IsNotEmpty(response.Routes);
        //    Assert.IsTrue(response.Count != 0);
        //    Assert.IsTrue(response.Vehicle == "bike");
        //    Assert.IsTrue(response.Weighting == "shortest");
        //}

        //[Test]
        //[Category("Integration")]
        //public void RoutingResponse_GetRoutingResponse_StartEqualsDestination()
        //{
        //    //Arrange
        //    Mock<DatabaseContext> context = new Mock<DatabaseContext>();
        //    IRasterService rasterService = new RasterService(context.Object);
        //    IRouteExposureService exposureService = new RouteExposureService(rasterService);
        //    IRoutingService routingService = new RoutingService(exposureService);

        //    //Coordinates in USP
        //    GeoCoordinate start = new GeoCoordinate(52.08720, 5.16546);
        //    GeoCoordinate destination = new GeoCoordinate(52.08720, 5.16546);

        //    //Act
        //    RoutingResponseDTO response = routingService.GetRoutingResponse(start
        //                                                                    , destination
        //                                                                    , "bike"
        //                                                                    , "shortest"
        //                                                                    );

        //    //Assert
        //    Assert.NotNull(response);
        //    Assert.IsNotEmpty(response.Routes);
        //    Assert.IsTrue(response.Routes[0].Path.Count == 1);
        //    Assert.IsTrue(response.Routes[0].Duration == 0);
        //    Assert.IsTrue(response.Routes[0].Distance == 0);
        //    Assert.IsTrue(response.Count == 1);
        //    Assert.IsTrue(response.Vehicle == "bike");
        //    Assert.IsTrue(response.Weighting == "shortest");
        //}

        //[Test]
        //[Category("Integration")]
        //public void RoutingResponse_GetRoutingResponse_DestinationOutOfTargetZone()
        //{
        //    //Arrange
        //    Mock<DatabaseContext> context = new Mock<DatabaseContext>();
        //    IRasterService rasterService = new RasterService(context.Object);
        //    IRouteExposureService exposureService = new RouteExposureService(rasterService);
        //    IRoutingService routingService = new RoutingService(exposureService);

        //    //Coordinates in New York
        //    GeoCoordinate start = new GeoCoordinate(40.73696, -73.98451);
        //    GeoCoordinate destination = new GeoCoordinate(52.08720, 5.16546);

        //    //Act
        //    RoutingResponseDTO response = routingService.GetRoutingResponse(start
        //                                                                    , destination
        //                                                                    , "bike"
        //                                                                    , "shortest"
        //                                                                    );

        //    //Assert
        //    Assert.NotNull(response);
        //    Assert.IsEmpty(response.Routes);
        //    Assert.IsTrue(response.Count == 0);
        //    Assert.IsTrue(response.Vehicle == "bike");
        //    Assert.IsTrue(response.Weighting == "shortest");
        //}

        //[Test]
        //[Category("Integration")]
        //public void RoutingResponse_GetRoutingResponse_StartOutOfTargetZone()
        //{
        //    //Arrange
        //    Mock<DatabaseContext> context = new Mock<DatabaseContext>();
        //    IRasterService rasterService = new RasterService(context.Object);
        //    IRouteExposureService exposureService = new RouteExposureService(rasterService);
        //    IRoutingService routingService = new RoutingService(exposureService);

        //    //Coordinates in New York
        //    GeoCoordinate start = new GeoCoordinate(52.08720, 5.16546);
        //    GeoCoordinate destination = new GeoCoordinate(40.73696, -73.98451);

        //    //Act
        //    RoutingResponseDTO response = routingService.GetRoutingResponse(start
        //                                                                    , destination
        //                                                                    , "bike"
        //                                                                    , "shortest"
        //                                                                    );

        //    //Assert
        //    Assert.NotNull(response);
        //    Assert.IsEmpty(response.Routes);
        //    Assert.IsTrue(response.Count == 0);
        //    Assert.IsTrue(response.Vehicle == "bike");
        //    Assert.IsTrue(response.Weighting == "shortest");
        //}
    }
}
