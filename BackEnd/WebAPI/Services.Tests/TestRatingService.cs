/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Services.Interfaces;
using Services.Implementations;
using DataAccess;
using Moq;
using Domain.Administration;
using System.Device.Location;
using TestUtils;
using Microsoft.Extensions.Logging;

namespace Services.Tests {
    [TestFixture]
    class TestRatingService {
        private ILogger<RatingService> logger;

        [OneTimeSetUp]
        public void setup() {
            logger = MockHelper.GetLogger<RatingService>();
        }

        [Test]
        public void Rating_AddRating() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            context.Setup(x => x.UserRatings.Add(It.IsAny<UserRating>()))
                .Returns((UserRating x) => new UserRating());
            IRatingService ratingService = new RatingService(context.Object, logger);

            UserRating rating = new UserRating() { Latitude = 5, Longitude = 5, Rating = 3 };

            // Act
            UserRating result = ratingService.AddRating(rating);

            // Assert
            Assert.NotNull(result);
        }

        [Test]
        public void Rating_AddRating_Validation() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            context.Setup(x => x.UserRatings.Add(It.IsAny<UserRating>()))
                .Returns((UserRating x) => new UserRating());
            IRatingService ratingService = new RatingService(context.Object, logger);

            // Act

            // Valid cases
            UserRating validResult1 = ratingService.AddRating(new UserRating() { Latitude = 5, Longitude = 5, Rating = 3 });
            UserRating validResult2 = ratingService.AddRating(new UserRating() { Latitude = 5, Longitude = 5, Rating = 3 });

            // Invalid cases
            UserRating invalidResult1 = ratingService.AddRating(new UserRating() { Latitude = 5, Longitude = 5, Rating = 0 });
            UserRating invalidResult2 = ratingService.AddRating(new UserRating() { Latitude = 5, Longitude = 5, Rating = 6 });
            UserRating invalidResult3 = ratingService.AddRating(new UserRating() { Latitude = 5, Longitude = 5, Rating = -3 });

            // Assert
            Assert.NotNull(validResult1);
            Assert.NotNull(validResult2);

            Assert.Null(invalidResult1);
            Assert.Null(invalidResult2);
            Assert.Null(invalidResult3);
        }

        [Test]
        public void Rating_GetRatings_NoDuplicates() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());

            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            ICollection<UserRating> result = ratingService.GetRatings(source, (float)radius);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.That(result.Count(x => x.Rating == 1.0f), Is.EqualTo(1));
            Assert.That(result.Count(x => x.Rating == 2.0f), Is.EqualTo(1));
            Assert.That(result.Count(x => x.Rating == 3.0f), Is.EqualTo(1));
        }

        [Test]
        public void Rating_GetRatings_NoRatings() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());

            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(10, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 1));

            // Act
            ICollection<UserRating> result = ratingService.GetRatings(source, (float)radius);

            // Assert
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void Rating_GetRatings_NegativeRadius() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());

            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            ICollection<UserRating> result = ratingService.GetRatings(source, -1f);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Rating_GetRatings_PartialSelection() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 2));

            // Act
            ICollection<UserRating> result = ratingService.GetRatings(source, (float)radius);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.That(result.Count(x => x.Rating == 1.0f), Is.EqualTo(1));
            Assert.That(result.Count(x => x.Rating == 2.0f), Is.EqualTo(0));
            Assert.That(result.Count(x => x.Rating == 3.0f), Is.EqualTo(0));
        }

        [Test]
        public void Rating_GetAverageRating_CorrectAverage() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            double result = ratingService.GetAverageRating(source, (float)radius);

            // Assert
            Assert.AreEqual(2.0f, result);
        }

        [Test]
        public void Rating_GetAverageRating_NoRatings() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            double result = ratingService.GetAverageRating(source, (float)radius);

            // Assert
            Assert.AreEqual(0f, result);
        }

        [Test]
        public void Rating_GetRatingNumbers() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            Dictionary<float, int> result = ratingService.GetRatingNumbers(source, (float)radius);

            // Assert
            Assert.AreEqual(result[1], 1);
            Assert.AreEqual(result[2], 1);
            Assert.AreEqual(result[3], 1);
            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void Rating_GetRatingNumbers_NoRatings() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            IRatingService ratingService = new RatingService(context.Object, logger);

            GeoCoordinate source = new GeoCoordinate(0, 0);
            double radius = source.GetDistanceTo(new GeoCoordinate(0, 5));

            // Act
            Dictionary<float, int> result = ratingService.GetRatingNumbers(source, (float)radius);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Rating_GetNewRatings_NoRatings() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            IRatingService ratingService = new RatingService(context.Object, logger);

            DateTime time = DateTime.MinValue;

            // Act
            ICollection<UserRating> result = ratingService.GetNewRatings(time);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Rating_GetNewRatings_NoResult() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetManyRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            DateTime time = new DateTime(2018, 12, 24);

            // Act
            ICollection<UserRating> result = ratingService.GetNewRatings(time);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Rating_GetNewRatings_ValidResult() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetManyRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            DateTime time = new DateTime(2018, 12, 16);

            // Act
            ICollection<UserRating> result = ratingService.GetNewRatings(time);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(7, result.Count);
        }

        [Test]
        public void Rating_GetAllRatings_NoRatings() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, new List<UserRating>() { }.AsQueryable());
            IRatingService ratingService = new RatingService(context.Object, logger);

            // Act
            ICollection<UserRating> result = ratingService.GetAllRatings();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void Rating_GetAllRatings_ValidResult() {
            // Arrange
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetUserRatingContext(context, GetManyRatings());
            IRatingService ratingService = new RatingService(context.Object, logger);

            // Act
            ICollection<UserRating> result = ratingService.GetAllRatings();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(14, result.Count);
        }

        private IQueryable<UserRating> GetRatings() {
            IQueryable<UserRating> ratings = new List<UserRating>()
           {
                new UserRating() {Id = new Guid(), Latitude = 0, Longitude = 1, Rating = 1 },
                new UserRating() {Id = new Guid(), Latitude = 0, Longitude = 2, Rating = 2 },
                new UserRating() {Id = new Guid(), Latitude = 0, Longitude = 3, Rating = 3 }
            }.AsQueryable();

            return ratings;
        }

        private IQueryable<UserRating> GetManyRatings() {
            // Build a list of UserRatings.
            return new List<UserRating>()
            {
                new UserRating() { Latitude = 52.0777202f, Longitude = 5.1448982f, Rating = 3, TimeStamp = new DateTime(2018, 12, 10) },
                new UserRating() { Latitude = 52.0869132f, Longitude = 5.1581592f, Rating = 2, TimeStamp = new DateTime(2018, 12, 11) },
                new UserRating() { Latitude = 52.0871491f, Longitude = 5.1741242f, Rating = 5, TimeStamp = new DateTime(2018, 12, 12) },
                new UserRating() { Latitude = 52.0854882f, Longitude = 5.1654122f, Rating = 4, TimeStamp = new DateTime(2018, 12, 13) },
                new UserRating() { Latitude = 52.0903921f, Longitude = 5.1674722f, Rating = 3, TimeStamp = new DateTime(2018, 12, 14) },
                new UserRating() { Latitude = 52.0826121f, Longitude = 5.1575592f, Rating = 2, TimeStamp = new DateTime(2018, 12, 15) },
                new UserRating() { Latitude = 52.0826901f, Longitude = 5.1718502f, Rating = 3, TimeStamp = new DateTime(2018, 12, 16) },
                new UserRating() { Latitude = 52.0890441f, Longitude = 5.1645972f, Rating = 1, TimeStamp = new DateTime(2018, 12, 17) },
                new UserRating() { Latitude = 52.0850092f, Longitude = 5.1593182f, Rating = 3, TimeStamp = new DateTime(2018, 12, 18) },
                new UserRating() { Latitude = 52.0847181f, Longitude = 5.1734372f, Rating = 4, TimeStamp = new DateTime(2018, 12, 19) },
                new UserRating() { Latitude = 52.0890151f, Longitude = 5.1572582f, Rating = 2, TimeStamp = new DateTime(2018, 12, 20) },
                new UserRating() { Latitude = 52.0868782f, Longitude = 5.1745532f, Rating = 4, TimeStamp = new DateTime(2018, 12, 21) },
                new UserRating() { Latitude = 52.0934792f, Longitude = 5.1847402f, Rating = 5, TimeStamp = new DateTime(2018, 12, 22) },
                new UserRating() { Latitude = 52.0934792f, Longitude = 5.1847402f, Rating = 1, TimeStamp = new DateTime(2018, 12, 23) },
            }.AsQueryable();
        }
    }
}
