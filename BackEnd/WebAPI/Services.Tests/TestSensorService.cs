/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Services.Interfaces;
using DataAccess;
using Moq;
using System.Device.Location;
using System;
using Services.Implementations;
using Domain.Exposure;
using TestUtils;
using Microsoft.Extensions.Logging;

namespace Services.Tests {
    [TestFixture]
    public class TestSensorService {
        private ISensorService sensorService;
        private List<Sensor> sensors;
        private List<SensorMeasurementBatch> batches;
        private ConfigurationService configurationService;
        private ILogger<SensorService> logger;

        [OneTimeSetUp]
        public void OneTimeSetUp() {
            sensors = GetSensors();
            batches = GetBatches();
            LinkSensorsAndBatches(sensors, batches);

            logger = MockHelper.GetLogger<SensorService>();
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetSensorContext(context, sensors.AsQueryable());
            context = MockHelper.GetSensorMeasurementBatchContext(context, batches.AsQueryable());

            configurationService = new ConfigurationService();
            configurationService.SensorTimeoutHours = 5;
            sensorService = new SensorService(configurationService, context.Object, logger);
        }

        [Test]
        public void Sensor_GetSensor() {
            // Act
            Sensor sensor = sensorService.GetSensor("sensor1");

            // Assert
            Assert.NotNull(sensor);
            Assert.AreEqual("sensor1", sensor.Id);
        }

        [Test]
        public void Sensor_GetSensor_InvalidId() {
            // Act
            Sensor sensor = sensorService.GetSensor("invalidSensorId");

            // Assert
            Assert.Null(sensor);
        }

        [Test]
        public void Sensor_GetSensor_LastValidBatchNull() {
            // Act
            Sensor sensor = sensorService.GetSensor("sensor2");

            // Assert
            Assert.Null(sensor);
        }

        [Test]
        public void Sensor_GetSensor_Timeout() {
            // Act
            Sensor sensor = sensorService.GetSensor("sensor3");

            // Assert
            Assert.Null(sensor);
        }

        [Test]
        public void Sensor_GetSensors() {
            // Act
            ICollection<Sensor> s = sensorService.GetSensors(new GeoCoordinate(52.0804000, 5.1605000),
                                                             new GeoCoordinate(52.0930000, 5.1893000));

            // Assert
            Assert.NotNull(s);
            // The two others are dropped because they do not fulfill the LastValidBatch requirements.
            Assert.AreEqual(1, s.Count);
        }

        [Test]
        public void Sensor_GetSensors_NoLocation() {
            // Act
            Mock<DatabaseContext> context = new Mock<DatabaseContext>();
            context = MockHelper.GetSensorContext(context, new List<Sensor>() { new Sensor() }.AsQueryable());
            SensorService newService = new SensorService(configurationService, context.Object, logger);
            List<Sensor> s = newService.GetSensors(new GeoCoordinate(52.0804000, 5.1605000),
                                                   new GeoCoordinate(52.0930000, 5.1893000))
                                                   .ToList();

            // Assert
            Assert.NotNull(s);
            Assert.IsEmpty(s);
        }

        [Test]
        public void Sensor_GetLatestBatch_Null() {
            SensorMeasurementBatch batch = sensorService.GetLatestBatch(null);
            Assert.Null(batch);
        }

        [Test]
        public void Sensor_GetLatestBatch_Success() {
            Sensor sensor = this.sensors[0];
            SensorMeasurementBatch batch = sensorService.GetLatestBatch(sensor);

            Assert.AreEqual(DateTime.MaxValue, batch.TimeStampPolled);
        }

        [Test]
        public void Sensor_GetSensorBatches_Null() {
            Sensor sensor = this.sensors[0];
            ICollection<SensorMeasurementBatch> batches = sensorService.GetSensorBatches(null, DateTime.MinValue, DateTime.MaxValue);
            Assert.Null(batches);
        }

        [Test]
        public void Sensor_GetSensorBatches_OrderedByDescending() {
            Sensor sensor = this.sensors[0];

            ICollection<SensorMeasurementBatch> batches = sensorService.GetSensorBatches(sensor.Id, DateTime.MinValue, DateTime.MaxValue);

            CollectionAssert.IsNotEmpty(batches);
            CollectionAssert.IsOrdered(batches, Comparer<SensorMeasurementBatch>.Create((b1, b2) => b2.TimeStampPolled.CompareTo(b1.TimeStampPolled)));

        }

        private List<Sensor> GetSensors() {
            return new List<Sensor>
            {
                new Sensor(){ Id = "sensor1", Label = "s1", Latitude = 52.0919f, Longitude = 5.17468f },
                new Sensor(){ Id = "sensor2", Label = "s2", Latitude = 52.0834f, Longitude = 5.17742f },
                new Sensor(){ Id = "sensor3", Label = "s3", Latitude = 52.0854f, Longitude = 5.16822f }
            };
        }

        private List<SensorMeasurementBatch> GetBatches() {
            return new List<SensorMeasurementBatch>
            {
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 1),
                    TimeStampTo = new DateTime(2018, 1, 2), TimeStampPolled = new DateTime(2018, 1, 2) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 2),
                    TimeStampTo = new DateTime(2018, 1, 3), TimeStampPolled = new DateTime(2018, 1, 3) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 3),
                    TimeStampTo = new DateTime(2018, 1, 4), TimeStampPolled = new DateTime(2018, 1, 4) },
                new SensorMeasurementBatch(){ SensorId = "sensor1", TimeStampFrom = new DateTime(2018, 1, 4),
                    TimeStampTo = new DateTime(2018, 1, 5), TimeStampPolled = DateTime.MaxValue },
            };
        }

        private void LinkSensorsAndBatches(List<Sensor> sensors, List<SensorMeasurementBatch> batches) {
            sensors[0].LastValidBatch = batches[3];
            sensors[0].Batches = batches;

            sensors[1].LastValidBatch = null;

            sensors[2].LastValidBatch = new SensorMeasurementBatch() { TimeStampPolled = new DateTime(1, 1, 1) };
        }
    }
}
