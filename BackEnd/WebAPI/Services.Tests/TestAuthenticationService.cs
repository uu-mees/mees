/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using DataAccess;
using Services.Interfaces;
using Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Moq;
using Domain.Administration;
using TestUtils;

namespace Services.Tests {
    class TestAuthenticationService {

        Mock<DatabaseContext> context;
        IAuthenticationService authenticationService;

        [OneTimeSetUp]
        public void Setup() {
            this.context = new Mock<DatabaseContext>();
            this.authenticationService = new AuthenticationService(context.Object, MockHelper.GetLogger<AuthenticationService>());

            Guid userSalt = Guid.NewGuid();
            List<User> users = new List<User>()
            {
                new User()
                {
                    Id = Guid.NewGuid(),
                    UserName = "test123",
                    Email = "test@test.com",
                    Salt = userSalt,
                    HashedPassword = authenticationService.HashPassword("test", userSalt),
                }
            };

            MockHelper.GetAuthenticationContext(context, users.AsQueryable());
        }

        [Test]
        public void Authentication_CheckLogin_RightCredentials() {
            // Arrange
            // Already arranged at one time setup

            // Act 
            User success = authenticationService.CheckLogin("test123", "test");

            // Assert
            Assert.NotNull(success);
            Assert.True(success.UserName == "test123");
        }

        [Test]
        public void Authentication_CheckLogin_WrongUsername() {
            // Arrange
            // Already arranged at one time setup

            // Act
            User wrongPassword = authenticationService.CheckLogin("asd", "test");

            // Assert
            Assert.IsNull(wrongPassword);
        }

        [Test]
        public void Authentication_CheckLogin_WrongPassword() {
            // Arrange
            // Already arranged at one time setup

            // Act
            User wrongPassword = authenticationService.CheckLogin("test123", "asd");

            // Assert
            Assert.IsNull(wrongPassword);
        }

        [Test]
        public void Authentication_CheckLogin_WrongCredentials() {
            // Arrange
            // Already arranged at one time setup

            // Act
            User wrongEverything = authenticationService.CheckLogin("asd", "asd");

            // Assert
            Assert.IsNull(wrongEverything);
        }



    }
}
