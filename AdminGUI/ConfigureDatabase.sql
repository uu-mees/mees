USE [LivingLabAir]
/****** Object:  Table [dbo].[DailyExposures]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[DailyExposures](
	[UserId] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[AverageExposure] [float] NOT NULL,
	[TotalDuration] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.DailyExposures] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[DataSources]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[DataSources](
	[ID] [uniqueidentifier] NOT NULL,
	[URL] [nvarchar](500) NOT NULL,
	[Type] [int] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.DataSources] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[FireBases]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[FireBases](
	[FireBaseID] [nvarchar](250) NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.FireBases] PRIMARY KEY CLUSTERED 
(
	[FireBaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[MinigameScores]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[MinigameScores](
	[userID] [uniqueidentifier] NOT NULL,
	[timeStamp] [datetime] NOT NULL,
	[score] [int] NULL,
	[misses] [int] NULL,
	[timeInSec] [float] NULL,
	[gameMode] [nvarchar](250) NULL,
	[mobile] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.MinigameScores] PRIMARY KEY CLUSTERED 
(
	[userID] ASC,
	[timeStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[PasswordResetTokens]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[PasswordResetTokens](
	[UserId] [uniqueidentifier] NOT NULL,
	[Token] [nvarchar](250) NULL,
	[ExpirationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.PasswordResetTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Questionnaires]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Questionnaires](
	[QuestionnaireId] [uniqueidentifier] NOT NULL,
	[QuestionnaireString] [nvarchar](250) NULL,
	[Trigger_TriggerId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo.Questionnaires] PRIMARY KEY CLUSTERED 
(
	[QuestionnaireId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[RasterCells]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[RasterCells](
	[CellId] [int] IDENTITY(1,1) NOT NULL,
	[RasterId] [int] NOT NULL,
	[Value] [real] NOT NULL,
	[SouthWestLatitude] [float] NOT NULL,
	[SouthWestLongitude] [float] NOT NULL,
	[NorthEastLatitude] [float] NOT NULL,
	[NorthEastLongitude] [float] NOT NULL,
	[CellIndex] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RasterCells] PRIMARY KEY CLUSTERED 
(
	[CellId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Rasters]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Rasters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CellSize] [real] NOT NULL,
	[NoDataValue] [real] NOT NULL,
	[Height] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[TimeStampPolled] [datetime] NOT NULL,
	[HashCode] [int] NOT NULL,
	[NorthEastLat] [float] NOT NULL,
	[NorthEastLng] [float] NOT NULL,
	[SouthWestLat] [float] NOT NULL,
	[SouthWestLng] [float] NOT NULL,
 CONSTRAINT [PK_dbo.Rasters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Researches]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Researches](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[ViewRatings] [bit] NOT NULL,
	[ViewSensors] [bit] NOT NULL,
	[ViewRaster] [bit] NOT NULL,
	[ViewExposure] [bit] NOT NULL,
	[ViewMinigameScores] [bit] NOT NULL,
	[ViewHighScores] [bit] NOT NULL,
	[ViewRoutePlanner] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Researches] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[SensorComponents]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[SensorComponents](
	[Id] [uniqueidentifier] NOT NULL,
	[Component] [nvarchar](250) NULL,
	[Unit] [nvarchar](250) NULL,
 CONSTRAINT [PK_dbo.SensorComponents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[SensorMeasurementBatches]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[SensorMeasurementBatches](
	[Id] [uniqueidentifier] NOT NULL,
	[SensorId] [nvarchar](250) NOT NULL,
	[TimeStampFrom] [datetime] NOT NULL,
	[TimeStampTo] [datetime] NOT NULL,
	[TimeStampPolled] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.SensorMeasurementBatches] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[SensorMeasurements]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[SensorMeasurements](
	[Id] [uniqueidentifier] NOT NULL,
	[BatchId] [uniqueidentifier] NOT NULL,
	[SensorComponentId] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](250) NULL,
 CONSTRAINT [PK_dbo.SensorMeasurements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Sensors]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Sensors](
	[Id] [nvarchar](250) NOT NULL,
	[Label] [nvarchar](250) NULL,
	[Latitude] [real] NOT NULL,
	[Longitude] [real] NOT NULL,
	[LastValidBatch_Id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo.Sensors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Triggers]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Triggers](
	[TriggerId] [uniqueidentifier] NOT NULL,
	[Priority] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[TimeStamp] [datetime] NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Triggers] PRIMARY KEY CLUSTERED 
(
	[TriggerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[UserLocationExposures]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[UserLocationExposures](
	[UserID] [uniqueidentifier] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Position_Latitude] [float] NOT NULL,
	[Position_Longitude] [float] NOT NULL,
	[Exposure] [float] NULL,
 CONSTRAINT [PK_dbo.UserLocationExposures] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[Timestamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[UserRatings]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[UserRatings](
	[Id] [uniqueidentifier] NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Rating] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.UserRatings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[UserResearches]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[UserResearches](
	[userId] [uniqueidentifier] NOT NULL,
	[researchId] [uniqueidentifier] NOT NULL,
	[startTime] [datetime] NOT NULL,
	[endTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.UserResearches] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[researchId] ASC,
	[startTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[Users]    Script Date: 03/06/2019 12:29:38 ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[Role] [int] NOT NULL,
	[UserName] [nvarchar](250) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[TrackLocation] [bit] NOT NULL,
	[Height] [int] NULL,
	[Weight] [real] NULL,
	[Sex] [nvarchar](250) NULL,
	[DateOfBirth] [datetime] NULL,
	[HomeAddress] [nvarchar](250) NULL,
	[WorkAddress] [nvarchar](250) NULL,
	[HashedPassword] [varbinary](max) NOT NULL,
	[Salt] [uniqueidentifier] NOT NULL,
	[LastQuestionnaireTime] [datetime] NULL,
	[LastMiniGameTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[Questionnaires] ADD  DEFAULT (newsequentialid()) FOR [QuestionnaireId]
ALTER TABLE [dbo].[Researches] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[SensorComponents] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[SensorMeasurementBatches] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[SensorMeasurements] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[Triggers] ADD  DEFAULT (newsequentialid()) FOR [TriggerId]
ALTER TABLE [dbo].[UserRatings] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[Users] ADD  DEFAULT (newsequentialid()) FOR [Id]
ALTER TABLE [dbo].[DailyExposures]  WITH CHECK ADD  CONSTRAINT [FK_dbo.DailyExposures_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[DailyExposures] CHECK CONSTRAINT [FK_dbo.DailyExposures_dbo.Users_UserId]
ALTER TABLE [dbo].[FireBases]  WITH CHECK ADD  CONSTRAINT [FK_dbo.FireBases_dbo.Users_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[FireBases] CHECK CONSTRAINT [FK_dbo.FireBases_dbo.Users_UserID]
ALTER TABLE [dbo].[PasswordResetTokens]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PasswordResetTokens_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[PasswordResetTokens] CHECK CONSTRAINT [FK_dbo.PasswordResetTokens_dbo.Users_UserId]
ALTER TABLE [dbo].[Questionnaires]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Questionnaires_dbo.Triggers_Trigger_TriggerId] FOREIGN KEY([Trigger_TriggerId])
REFERENCES [dbo].[Triggers] ([TriggerId])
ALTER TABLE [dbo].[Questionnaires] CHECK CONSTRAINT [FK_dbo.Questionnaires_dbo.Triggers_Trigger_TriggerId]
ALTER TABLE [dbo].[RasterCells]  WITH NOCHECK ADD  CONSTRAINT [FK_dbo.RasterCells_dbo.Rasters_RasterId] FOREIGN KEY([RasterId])
REFERENCES [dbo].[Rasters] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[RasterCells] CHECK CONSTRAINT [FK_dbo.RasterCells_dbo.Rasters_RasterId]
ALTER TABLE [dbo].[SensorMeasurementBatches]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SensorMeasurementBatches_dbo.Sensors_SensorId] FOREIGN KEY([SensorId])
REFERENCES [dbo].[Sensors] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[SensorMeasurementBatches] CHECK CONSTRAINT [FK_dbo.SensorMeasurementBatches_dbo.Sensors_SensorId]
ALTER TABLE [dbo].[SensorMeasurements]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SensorMeasurements_dbo.SensorComponents_SensorComponentId] FOREIGN KEY([SensorComponentId])
REFERENCES [dbo].[SensorComponents] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[SensorMeasurements] CHECK CONSTRAINT [FK_dbo.SensorMeasurements_dbo.SensorComponents_SensorComponentId]
ALTER TABLE [dbo].[SensorMeasurements]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SensorMeasurements_dbo.SensorMeasurementBatches_BatchId] FOREIGN KEY([BatchId])
REFERENCES [dbo].[SensorMeasurementBatches] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[SensorMeasurements] CHECK CONSTRAINT [FK_dbo.SensorMeasurements_dbo.SensorMeasurementBatches_BatchId]
ALTER TABLE [dbo].[Sensors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sensors_dbo.SensorMeasurementBatches_LastValidBatch_Id] FOREIGN KEY([LastValidBatch_Id])
REFERENCES [dbo].[SensorMeasurementBatches] ([Id])
ALTER TABLE [dbo].[Sensors] CHECK CONSTRAINT [FK_dbo.Sensors_dbo.SensorMeasurementBatches_LastValidBatch_Id]
ALTER TABLE [dbo].[Triggers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Triggers_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[Triggers] CHECK CONSTRAINT [FK_dbo.Triggers_dbo.Users_UserId]
ALTER TABLE [dbo].[UserLocationExposures]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLocationExposures_dbo.Users_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[UserLocationExposures] CHECK CONSTRAINT [FK_dbo.UserLocationExposures_dbo.Users_UserID]
ALTER TABLE [dbo].[UserRatings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRatings_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[UserRatings] CHECK CONSTRAINT [FK_dbo.UserRatings_dbo.Users_UserId]
ALTER TABLE [dbo].[UserResearches]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserResearches_dbo.Researches_researchId] FOREIGN KEY([researchId])
REFERENCES [dbo].[Researches] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[UserResearches] CHECK CONSTRAINT [FK_dbo.UserResearches_dbo.Researches_researchId]
ALTER TABLE [dbo].[UserResearches]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserResearches_dbo.Users_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
ALTER TABLE [dbo].[UserResearches] CHECK CONSTRAINT [FK_dbo.UserResearches_dbo.Users_userId]

/* Insert default data sources */
INSERT INTO DataSources (ID, URL, type, active) VALUES ('10000000-1000-1000-1000-100000000001', 'https://samenmeten.rivm.nl/uurkaart/images/combined_grid_utr_no2.asc', 0, 0);
INSERT INTO DataSources (ID, URL, type, active) VALUES ('10000000-1000-1000-1000-100000000002', 'https://samenmeten.rivm.nl/dataportaal/klanten/usp/xmluu/', 1, 0);
