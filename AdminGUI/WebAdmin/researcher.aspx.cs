// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Web;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class ResearcherPage : Page {
        private readonly ResearchController _researchController;
        private readonly UserController _userController;

        protected ResearcherPage() {
            _researchController = new ResearchController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));
            _userController = new UserController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString), new FirebaseNotificationProvider());
        }

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is logged in and has permission to access this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Researcher) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            try {
                ResearchRows.Text = "";
                // Get the User object of the logged in Researcher
                User researcher = _userController.GetUserById(Guid.Parse(HttpContext.Current.User.Identity.Name.Split('|')[0]));

                // Get the assigned Researches
                List<Research> researches = _researchController.GetAssignedResearches(researcher.Id);

                // Print the assigned Researches to the table
                if (researches.Count > 0) {
                    foreach (Research research in researches)
                        ResearchRows.Text += PrintRow(research);
                }
                else {
                    // No Researches have been assigned yet, show a warning and disable the buttons
                    Master.AddAlert(new Alert("No researches have been assigned to your account", AlertType.Warning, LifeTime.DeleteImmediately));
                    table_researches.Visible = false;
                    button_export.Visible = false;
                }
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert("Researcher Id is not valid: " + ex.Message, AlertType.Error));
                Master.Redirect("~/researcher.aspx");
            }
        }

        /// <summary>
        ///     Prints a Research to the table
        /// </summary>
        /// <param name="research">The Research object to print</param>
        /// <returns>The Research as an HTML-formatted row</returns>
        private static string PrintRow(Research research) {
            StringBuilder html = new StringBuilder();

            // Parse the start and end dates to a status
            string status;
            if (research.StartDate > DateTime.UtcNow)
                status = "Not yet started";
            else if (research.EndDate > DateTime.UtcNow)
                status = "Ongoing";
            else
                status = "Ended";

            // Print the fields
            html.AppendLine("<tr>");
            html.AppendLine("<td style=\"text-align: center; vertical-align: middle;\">");
            html.AppendLine("<input type=\"radio\" name=\"researchId\" value=\"" + research.Id + "\" />");
            html.AppendLine("</td>");
            html.AppendLine("<td>" + research.Name + "</td>");
            html.AppendLine("<td>" + research.Description + "</td>");
            html.AppendLine("<td>" + research.StartDate + "</td>");
            html.AppendLine("<td>" + research.EndDate + "</td>");
            html.AppendLine("<td>" + status + "</td>");
            html.AppendLine("</tr>");
            return html.ToString();
        }

        /// <summary>
        ///     Button click handler for downloading a Research
        /// </summary>
        protected void ExportResearch(object sender, EventArgs e) {
            string id = Request.Form["researchId"];
            if (string.IsNullOrWhiteSpace(id)) {
                // No Research has been selected
                Master.AddAlert(new Alert("Please select a research to export", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            else {
                // Redirect to the download page for the Research
                Master.Redirect("downloadresearch.aspx?research=" + id);
            }
        }
    }
}
