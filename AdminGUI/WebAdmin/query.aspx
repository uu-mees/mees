﻿<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="query.aspx.cs" Inherits="WebAdmin.QueryPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Query" ContentPlaceHolderID="content" runat="server">
    <div class="mees-page">
        <!-- The Input Section-->
        <div class="mees-page-small">
            <h3 class="mees-page-title">Query Data</h3>
            <div class="form group">
                <label for="QueryBox">Enter your query below:</label>
                <asp:TextBox ID="QueryBox" MaxLength="1000" TextMode="MultiLine" CssClass="form-control mees-textbox-large" Columns="50" Rows="5" runat="server"></asp:TextBox>
                <asp:Button runat="server" CssClass="btn btn-success mees-align-right" OnClick="ExecuteQuery_Click" Text="Execute Query" />
                <asp:Button runat="server" CssClass="btn btn-info mees-align-right" OnClick="DownloadQuery_Click" Text="Download Query" />
            </div>
            <asp:Label runat="server" class="mees-error" Visible="false"></asp:Label>

            <!-- Help toggle -->
            <div>
                <p style="text-align: right;">
                    <a class="btn btn-link" data-toggle="collapse" href="#description" role="button">Show Database tables description</a>
                </p>
            </div>
        </div>
        <!-- Help section -->
        <div class="collapse" id="description">
            <div class="card card-body">
                <a id="description-click" />
                <table class="tableinfo">
                    <tr class="tableinfo">
                        <td class="tableinfo tableinfo-buttons">
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="DailyExposures()" role="button">Show DailyExposures table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="DataSources()" role="button">Show DataSources table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="FireBases()" role="button">Show FireBases table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="MinigameScores()" role="button">Show MinigameScores table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="PasswordResetTokens()" role="button">Show PasswordResetTokens table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Questionnaires()" role="button">Show Questionnaires table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="RasterCells()" role="button">Show RasterCells table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Rasters()" role="button">Show Rasters table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Researches()" role="button">Show Researches table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="SensorComponents()" role="button">Show SensorComponents table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="SensorMeasurementBatches()" role="button">Show SensorMeasurementBatches table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="SensorMeasurements()" role="button">Show SensorMeasurements table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Sensors()" role="button">Show Sensors table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Triggers()" role="button">Show Triggers table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="UserLocationExposures()" role="button">Show UserLocationExposures table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="UserRatings()" role="button">Show UserRatings table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="UserResearches()" role="button">Show UserResearches table description</a>
                            <a class="btn btn-info btn-tableinfo" href="#description-click" onclick="Users()" role="button">Show Users table description</a>
                        </td>
                        <td class="tableinfo tableinfo-text" id="table-description-text">Please select a table to view documentation on with the buttons on the left side.</td>
                    </tr>
                </table>
            </div>
        </div>
        <br />
        <br />
        <div style="overflow: auto">
            <asp:Literal runat="server" ID="QueryTable" />
        </div>
    </div>
    <script type="application/javascript" src="include/js/tabledescriptions.js"></script>
</asp:Content>
