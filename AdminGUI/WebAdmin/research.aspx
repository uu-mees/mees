<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="research.aspx.cs" Inherits="WebAdmin.ResearchPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Research" ContentPlaceHolderID="content" runat="server">
    <div class="mees-page-small">
        <asp:ScriptManager runat="server" ID="manager"></asp:ScriptManager>
        <!-- Header info -->
        <h3 class="mees-page-title" runat="server" id="form_header"></h3>
        <p runat="server" id="form_description"></p>

        <!-- Form input -->
        <div id="form_input" runat="server" visible="false">
            <p>Name:</p>
            <div class="form-group">
                <asp:TextBox ID="f_name" MaxLength="50" runat="server" CssClass="form-control input-lg" placeholder="Research Name" required="True"></asp:TextBox>
            </div>
            <p>Please check the boxes of the information participants should be able to see:</p>
            <div class="form-group">
                <asp:CheckBoxList ID="f_permissions"
                    RepeatDirection="Vertical"
                    RepeatLayout="Flow"
                    TextAlign="Right"
                    runat="server">
                    <asp:ListItem>&nbsp;Show Ratings</asp:ListItem>
                    <asp:ListItem>&nbsp;Show Sensor Data</asp:ListItem>
                    <asp:ListItem>&nbsp;Show Raster Data</asp:ListItem>
                    <asp:ListItem>&nbsp;Show Exposure Profile</asp:ListItem>
                    <asp:ListItem>&nbsp;Show Minigame Scores</asp:ListItem>
                    <asp:ListItem>&nbsp;Allow Route Planner Access</asp:ListItem>
                </asp:CheckBoxList>
            </div>
            <p>Description:</p>
            <div class="form-group">
                <asp:TextBox ID="f_description" MaxLength="256" runat="server" CssClass="form-control input-lg" placeholder="Use this space to give some descriptive information about this research" required="True"></asp:TextBox>
            </div>
            <asp:UpdatePanel ID="f_panel" runat="server">
                <ContentTemplate>
                    <div class="form-group">
                        <p>Start Date:</p>
                        <asp:Calendar ID="f_startdate" runat="server" SelectionMode="Day"></asp:Calendar>
                        <p>End Date:</p>
                        <asp:Calendar ID="f_enddate" runat="server" SelectionMode="Day"></asp:Calendar>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <!-- Table -->
        <div runat="server" id="researchTable" visible="False">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                </thead>
                <asp:Literal runat="server" ID="researchTableContent"></asp:Literal>
            </table>
        </div>

        <!-- Buttons -->
        <a href="researches.aspx" class="btn btn-danger mees-align-right">Cancel</a>
        <asp:Button runat="server" ID="button_confirm" CssClass="btn btn-success mees-align-right" />
    </div>
</asp:Content>
