function DailyExposures() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>DailyExposures</h4>" +
        "<i>This table contains the total exposure per user per day.</i><br><br>" +
        "<b>UserId</b>: the unique identifier of the user the entry refers to.<br>" +
        "<b>Date</b>: the date the exposure was measured.<br>" +
        "<b>AverageExposure</b>: the total daily exposure, divided by the TotalDuration.<br>" +
        "<b>TotalDuration</b>: the total time in seconds that passed since the day started.<br><br>" +
        "<b>Primary key</b>: (UserId, Date)<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function DataSources() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>DataSources</h4>" +
        "<i>This table contains the data sources polled by the back-end.</i><br><br>" +
        "<b>Id</b>: the unique identifier of the data source.<br>" +
        "<b>Url</b>: a hyperlink to the data location.<br>" +
        "<b>Type</b>: an integer indicating whether this data source is a Raster (0) or a Sensor (1) source.<br>" +
        "<b>Active</b>: an integer indicating whether this data source is active (1) or disabled (0).<br><br>" +
        "<b>Primary key</b>: Id<br>";
}

function FireBases() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>FireBases</h4>" +
        "<i>This table contains the Firebase tokens used for sending notifications.</i><br><br>" +
        "<b>FireBaseId</b>: the token used for sending the notification via the Firebase server.<br>" +
        "<b>UserId</b>: the identifier of the user associated with the token.<br><br>" +
        "<b>Primary key</b>: FireBaseId<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function MinigameScores() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>MinigameScores</h4>" +
        "<i>This table contains the results of the minigames played by users.</i><br><br>" +
        "<b>UserId</b>: the identifier of the user the entry refers to.<br>" +
        "<b>TimeStamp</b>: the date and time the minigame was completed.<br>" +
        "<b>Score</b>: the number of points the user scored.Refer to the Minigames documentation for more information on the scoring system.<br>" +
        "<b>Misses</b>: the number of mistakes made while playing the minigame. Refer to the Minigames documentation for more information on the scoring system.<br>" +
        "<b>TimeInSec</b>: the time spent by the user playing the minigame.<br>" +
        "<b>GameMode</b>: an integer indicating which minigame was played. Refer to the Minigames documentation for more information on the scoring system.<br>" +
        "<b>Mobile</b>: whether the minigame was played on a mobile device or in a browser.<br><br>" +
        "<b>Primary key</b>: (UserId, TimeStamp)<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function PasswordResetTokens() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>PasswordResetTokens</h4>" +
        "<i>This table contains the tokens used for resetting a user’s password.</i><br><br>" +
        "<b>UserId</b>: the identifier of the user the token belongs to.<br>" +
        "<b>Token</b>: a randomized string that is used to verify the user is the owner of the account.<br>" +
        "<b>ExpirationDate</b>: the date and time after which the token cannot be used.<br><br>" +
        "<b>Primary key</b>: UserId<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function Questionnaires() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Questionnaires</h4>" +
        "<i>This table contains the identifiers used for retrieving the questionnaire data from Typeform.</i><br><br>" +
        "<b>QuestionnaireId</b>: the unique identifier that is sent with the questionnaire when it is submitted by the user.<br>" +
        "<b>QuestionnaireString</b>: the Url of the questionnaire that has been filled in.<br>" +
        "<b>Trigger_TriggerId</b>: the identifier of the trigger that notified the user of the questionnaire.<br><br>" +
        "<b>Primary key</b>: QuestionnaireId<br>" +
        "<b>Foreign key</b>: Trigger_TriggerId → Triggers.TriggerId<br>";
}

function RasterCells() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>RasterCells</h4>" +
        "<i>This table contains the individual cells of the pollution raster.</i><br><br>" +
        "<b>CellId</b>: an integer identifying this cell.<br>" +
        "<b>RasterId</b>: the identifier of the raster this cell belongs to.<br>" +
        "<b>Value</b>: the NO2 exposure in this cell.<br>" +
        "<b>SouthWestLatitude</b>: the latitude of the bottom left corner of this cell.<br>" +
        "<b>SouthWestLongitude</b>: the longitude of the bottom left corner of this cell.<br>" +
        "<b>NorthEastLatitude</b>: the latitude of the top right corner of this cell.<br>" +
        "<b>NorthEastLongitude</b>: the longitude of the top right corner of this cell.<br>" +
        "<b>CellIndex</b>: the index of this cell within the raster referenced by RasterId.<br><br>" +
        "<b>Primary key</b>: CellId<br>" +
        "<b>Foreign key</b>: RasterId → Rasters.Id<br>";
}

function Rasters() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Rasters</h4>" +
        "<i>This table contains the rasters collected by the back-end.</i><br><br>" +
        "<b>Id</b>: an auto-incrementing integer identifying this raster.<br>" +
        "<b>CellSize</b>: the size of each cell in the raster, as defined in the ASC raster source file.<br>" +
        "<b>NoDataValue</b>: as defined in the ASC raster source file.<br>" +
        "<b>Height</b>: the number of cells that the raster is large vertically.<br>" +
        "<b>Width</b>: the number of cells that the raster is large horizontally.<br>" +
        "<b>TimeStampPolled</b>: the date and time that the back - end retrieved this raster.<br>" +
        "<b>HashCode</b>: a unique hash for this raster, used to efficiently compare different rasters.<br>" +
        "<b>NorthEastLat</b>: the latitude of the top right corner of the raster.<br>" +
        "<b>NorthEastLng</b>: the longitude of the top right corner of the raster.<br>" +
        "<b>SouthWestLat</b>: the latitude of the bottom left corner of the raster.<br>" +
        "<b>SouthWestLng</b>: the longitude of the bottom left corner of the raster.<br><br>" +
        "<b>Primary key</b>: Id<br>";
}

function Researches() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Researches</h4>" +
        "<i>This table contains the research entries created by the administrators.</i><br><br>" +
        "<b>Id</b>: a unique identifier for the research.<br>" +
        "<b>Name</b>: the name of the research.<br>" +
        "<b>Description</b>: a description of the purpose of the research.<br>" +
        "<b>StartTime</b>: the date and time the research begins.<br>" +
        "<b>StopTime</b>: the date and time the research ends.<br>" +
        "<b>ViewRatings</b>: whether users in this research can see user ratings on the map.<br>" +
        "<b>ViewSensors</b>: whether users in this research can see sensor values on the map.<br>" +
        "<b>ViewRaster</b>: whether users in this research can see the pollution map.<br>" +
        "<b>ViewExposure</b>: whether users in this research can see their current / daily exposure.<br>" +
        "<b>ViewMinigameScores</b>: whether users in this research can see their score at the end of a minigame.<br>" +
        "<b>ViewHighScores</b>: whether users in this research can see the highscores of the minigames (<i>highscores have not been implemented yet, so the WebAdmin and back-end use default value ‘false’</i>).<br>" +
        "<b>ViewRoutePlanner</b>: whether users in this research can use the route planner.<br><br>" +
        "<b>Primary key</b>: Id<br>";
}

function SensorComponents() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>SensorComponents</h4>" +
        "<i>This table contains the definitions of the kinds of data that can be measured by the sensors and what unit they are measured in.</i><br><br>" +
        "<b>Id</b>: a unique identifier for the component.<br>" +
        "<b>Component</b>: the kind of data that is measured(e.g.ug / m3).<br>" +
        "<b>Unit</b>: the unit that the data is measured in (e.g.NO2).<br><br>" +
        "<b>Primary key</b>: Id<br>";
}

function SensorMeasurementBatches() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>SensorMeasurementBatches</h4>" +
        "<i>This table contains information on when the sensors are polled.</i><br><br>" +
        "<b>Id</b>: a unique identifier for the measurement.<br>" +
        "<b>SensorId</b>: the identifier of the sensor that was polled.<br>" +
        "<b>TimeStampFrom</b>: the date and time that the sensor collected the data.<br>" +
        "<b>TimeStampTo</b>: the date and time that the sensor data is refreshed and the collected data is no longer up to date.<br>" +
        "<b>TimeStampPolled</b>: the date and time that the back-end polled the sensor measurements.<br><br>" +
        "<b>Primary key</b>: Id<br>" +
        "<b>Foreign key</b>: SensorId → Sensors.Id<br>";
}

function SensorMeasurements() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>SensorMeasurements</h4>" +
        "<i>This table contains the individual measurements of the sensors.</i><br><br>" +
        "<b>Id</b>: a unique identifier for this measurement.<br>" +
        "<b>BatchId</b>: the id of the batch this measurement belongs to.<br>" +
        "<b>SensorComponentId</b>: what kind of data was measured.<br>" +
        "<b>Value</b>: the value that was collected from the sensor.<br><br>" +
        "<b>Primary key</b>: Id<br>" +
        "<b>Foreign keys</b>: SensorComponentId → SensorComponents.Id; BatchId → SensorMeasurementBatches.Id<br>";
}


function Sensors() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Sensors</h4>" +
        "<i>This table contains the sensor definitions.</i><br><br>" +
        "<b>Id</b>: a unique identifier for this sensor.<br>" +
        "<b>Label</b>: the name of the sensor.<br>" +
        "<b>Latitude</b>: the latitude of this sensor’s position.<br>" +
        "<b>Longitude</b>: the longitude of this sensor’s position.<br>" +
        "<b>LastValidBatch_Id</b>: the identifier of the last valid measurements batch that was collected from this sensor.<br><br>" +
        "<b>Primary key</b>: Id<br>" +
        "<b>Foreign key</b>: LastValidBatch_Id → SensorMeasurementBatches.Id<br>";
}

function Triggers() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Triggers</h4>" +
        "<i>This table contains the history of triggers activated by a user.</i><br><br>" +
        "<b>TriggerId</b>: a unique identifier for this trigger.<br>" +
        "<b>Priority</b>: A priority assigned to the trigger (<i>priorities have not yet been implemented, so the back-end uses default value ‘2’</i>).<br>" +
        "<b>UserId</b>: the identifier of the user that activated the trigger.<br>" +
        "<b>TimeStamp</b>: the date and time the trigger was activated.<br>" +
        "<b>Discriminator</b>: what kind of trigger was activated (e.g. ExposureTrigger or DistanceTrigger).<br><br>" +
        "<b>Primary key</b>: TriggerId<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function UserLocationExposures() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>UserLocationExposures</h4>" +
        "<i>This table contains each user’s location data along with the exposure at that position.</i><br><br>" +
        "<b>UserId</b>: the identifier of the user the row refers to.<br>" +
        "<b>TimeStamp</b>: the date/time of the location measurement.<br>" +
        "<b>Position_Latitude</b>: the latitude of the user’s location.<br>" +
        "<b>Position_Longitude</b>: the longitude of the user’s location.<br>" +
        "<b>Exposure</b>: the NO2 exposure at this place and moment.<br><br>" +
        "<b>Primary key</b>: (UserId, TimeStamp)<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function UserRatings() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>UserRatings</h4>" +
        "<i>This table contains the air quality ratings left by users.</i><br><br>" +
        "<b>Id</b>: the unique identifier for this rating.<br>" +
        "<b>Latitude</b>: the latitude of the rating’s location.<br>" +
        "<b>Longitude</b>: the longitude of the rating’s location.<br>" +
        "<b>Rating</b>: the rating left by the user - this is a value between 1 and 5.<br>" +
        "<b>TimeStamp</b>: the date/time the rating was placed.<br>" +
        "<b>UserId</b>: the identifier of the user that left the rating.<br><br>" +
        "<b>Primary key</b>: Id<br>" +
        "<b>Foreign key</b>: UserId → Users.Id<br>";
}

function UserResearches() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>UserResearches</h4>" +
        "<i>This table links user accounts to researches.</i><br><br>" +
        "<b>UserId</b>: the identifier of the user account to link.<br>" +
        "<b>ResearchId</b>: the identifier of the research to link to.<br>" +
        "<b>StartTime</b>: the date/time that the user started participating in the research. This is either the moment the user was assigned to the research, if the research had already started, or the start date and time of the research itself, if the user was assigned in advance.<br>" +
        "<b>EndTime</b>: the date/time the user stopped participating in the research. This is either the end date/time of the research itself, or the date and time the user was unassigned from the research.<br><br>" +
        "<b>Primary key</b>: (UserId, ResearchId, StartTime)<br>" +
        "<b>Foreign keys</b>: ResearchId → Researches.Id; UserId → Users.Id<br>";
}

function Users() {
    document.getElementById("table-description-text").innerHTML =
        "<h4>Users</h4>" +
        "<i>This table contains all users with their profile information.</i><br><br>" +
        "<b>Id</b>: the unique identifier of the user.<br>" +
        "<b>Role</b>: the role of the user - this can be either 0 (app user), 1 (administrator) or 2 (researcher).<br>" +
        "<b>UserName</b>: the nickname of the user.<br>" +
        "<b>Email</b>: the email address of the user.<br>" +
        "<b>TrackLocation</b>: whether the user’s location data will be stored (<i>this setting has not been implemented, and the location of all users will be tracked. The WebAdmin and back-end use default value ‘1’</i>).<br>" +
        "<b>Height</b>: the user’s height (optional).<br>" +
        "<b>Weight</b>: the user’s weight (optional).<br>" +
        "<b>Sex</b>: the user’s sex (optional).<br>" +
        "<b>DateOfBirth</b>: the user’s date of birth (optional).<br>" +
        "<b>HomeAddress</b>: the user’s home address (optional).<br>" +
        "<b>WorkAddress</b>: the user’s work address (optional).<br>" +
        "<b>HashedPassword</b>: the user’s password, salted and hashed, in binary form.<br>" +
        "<b>Salt</b>: the GUID used to salt the password.<br>" +
        "<b>LastQuestionnaireTime</b>: the last date/time the user filled out a questionnaire.<br><br>" +
        "<b>LastMinigameTime</b>: the last date/time the user played a minigame.<br>" +
        "<b>Primary key</b>: Id<br>";
}
