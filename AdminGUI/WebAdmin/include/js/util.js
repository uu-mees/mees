/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

// Erases all checkboxes
function ClearCheckboxes() {
    $(':checkbox').each(function () {
        this.checked = false;
    });
}

// Used by the Users page checkboxes to enable/disable the buttons depending on how many users are checked
$(document).on("click", ":checkbox", function () {
    // Select/deselect all boxes on clicking the selectall checkbox
    if ($(this).prop('id') === 'selectall') {
        var status = this.checked;
        $(':checkbox').each(function () {
            this.checked = status;
        });
    }

    // Count checked boxes
    var countMax = $(":checkbox").length;
    var count = $(":checkbox:checked").length;
    if ($("#selectall").prop('checked') === true) {
        count -= 1;
    }

    if (count >= countMax - 1) {
        $("#selectall").prop('checked', true);
    } else {
        $("#selectall").prop('checked', false);
    }

    // Enable/disable buttons depending on number of checkboxes selected
    if (count === 0) {
        $("#button_create").removeClass("disabled");
        $("#button_edit").addClass("disabled");
        $("#button_wipe").addClass("disabled");
        $("#button_delete").addClass("disabled");
        $("#button_notify").addClass("disabled");
        $("#button_assign").addClass("disabled");
        $("#button_unassign").addClass("disabled");
        $("#button_assigner").addClass("disabled");
        $("#button_end").addClass("disabled");
        $("#button_enable").addClass("disabled");
        $("#button_disable").addClass("disabled");
    } else if (count === 1) {
        $("#button_create").addClass("disabled");
        $("#button_edit").removeClass("disabled");
        $("#button_wipe").removeClass("disabled");
        $("#button_delete").removeClass("disabled");
        $("#button_notify").removeClass("disabled");
        $("#button_assign").removeClass("disabled");
        $("#button_unassign").removeClass("disabled");
        $("#button_assigner").removeClass("disabled");
        $("#button_end").removeClass("disabled");
        $("#button_enable").removeClass("disabled");
        $("#button_disable").removeClass("disabled");
    } else {
        $("#button_create").addClass("disabled");
        $("#button_edit").addClass("disabled");
        $("#button_wipe").removeClass("disabled");
        $("#button_end").removeClass("disabled");
        $("#button_delete").removeClass("disabled");
        $("#button_notify").removeClass("disabled");
        $("#button_assign").removeClass("disabled");
        $("#button_unassign").removeClass("disabled");
        $("#button_assigner").addClass("disabled");
        $("#button_enable").removeClass("disabled");
        $("#button_disable").removeClass("disabled");
    }
});

// Used by all pages with a dynamically generated table: fixes the alignment of the buttons and text
function FixTableAlignment() {
    $("td").addClass("align-middle");
    $("th").addClass("align-middle");
}

// Used by the Edit pages: sets the checked radio button element to active 
function FixCheckboxActiveStatus() {
    $('input[type="radio"]').each(function (i, obj) {
        $(obj).parent().parent().removeClass('active');
    });
    $('input[type="radio"]:checked').each(function (i, obj) {
        $(obj).parent().parent().addClass('active');
    });
}
