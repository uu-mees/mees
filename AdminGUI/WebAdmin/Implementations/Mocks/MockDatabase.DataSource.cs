// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Linq;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        private readonly List<DataSource> _dataSources = new List<DataSource>();
        public List<DataSource> GetDataSources() => new List<DataSource>(_dataSources);
        public DataSource GetDataSourceByUrl(string url) => _dataSources.FirstOrDefault(ds => ds.Url == url)?.Copy();

        public DataSource GetDataSourceById(Guid dataSourceId) =>
            _dataSources.FirstOrDefault(ds => ds.Id == dataSourceId)?.Copy();

        public void AddDataSource(DataSource dataSource) => _dataSources.Add(dataSource);

        public void UpdateDataSource(DataSource dataSource) {
            RemoveDataSource(dataSource.Id);
            AddDataSource(dataSource);
        }

        public void RemoveDataSource(Guid dataSourceId) =>
            _dataSources.Remove(_dataSources.FirstOrDefault(ds => ds.Id == dataSourceId));
    }
}
