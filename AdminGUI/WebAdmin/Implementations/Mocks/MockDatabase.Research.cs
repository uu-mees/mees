// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Linq;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        private readonly List<Research> _researches = new List<Research>();

        public List<Research> GetResearches(int rowsPerPage, int pageIndex, string filterName,
                                            string filterDescription) =>
            new List<Research>(_researches);

        public int GetResearchCount(string filterName, string filterDescription) => _researches.Count;
        public Research GetResearchByName(string name) => _researches.FirstOrDefault(res => res.Name == name)?.Clone();
        public Research GetResearchById(Guid id) => _researches.FirstOrDefault(res => res.Id == id)?.Clone();
        public void CreateResearch(Research research) => _researches.Add(research);

        public void UpdateResearch(Research research) {
            RemoveResearch(research.Id);
            CreateResearch(research);
        }

        public void RemoveResearch(Guid researchId) =>
            _researches.Remove(_researches.FirstOrDefault(res => res.Id == researchId));
    }
}
