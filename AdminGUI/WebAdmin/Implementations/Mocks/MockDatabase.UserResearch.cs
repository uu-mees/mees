// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Linq;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        private readonly List<Tuple<Guid, Guid, DateTime, DateTime>> _assignments = new List<Tuple<Guid, Guid, DateTime, DateTime>>();

        public List<Guid> GetAssignedResearches(Guid userId) {
            List<Guid> result = new List<Guid>();
            // Loop over assignments and add every Research Id that is assigned to this User to the result
            foreach ((Guid researchId, Guid assignedUserId, _, DateTime endTime) in _assignments)
                if (userId == assignedUserId && endTime > DateTime.UtcNow)
                    result.Add(researchId);
            return result;
        }

        public bool UserAssigned(Guid userId, Guid researchId) {
            // Loop over assignments; if we find a assignment of the given User to the given Research return true
            foreach ((Guid assignedResearchId, Guid assignedUserId, _, DateTime endTime) in _assignments)
                if (userId == assignedUserId && researchId == assignedResearchId && endTime > DateTime.UtcNow)
                    return true;
            return false;
        }

        public List<Guid> GetParticipants(Guid researchId) {
            List<Guid> result = new List<Guid>();
            // Loop over assignments and add every User Id that is assigned to this Research to the result
            foreach ((Guid assignedResearchId, Guid userId, _, _) in _assignments)
                if (researchId == assignedResearchId)
                    result.Add(userId);
            return result;
        }

        public void AssignUser(Guid userId, Guid researchId) =>
            _assignments.Add(new Tuple<Guid, Guid, DateTime, DateTime>(researchId, userId, DateTime.Now, _researches.FirstOrDefault(r => r.Id == researchId).EndDate));

        public void UnassignUser(Guid userId, Guid researchId, DateTime standardEnd) =>
            _assignments.RemoveAll(r => r.Item1 == researchId && r.Item2 == userId);

        public int CountUsersInResearch(Guid researchId) {
            return _assignments.Count(x => x.Item1 == researchId);
        }

        public int CountActiveUsersInResearch(Guid researchId) {
            if (_researches.FirstOrDefault(r => r.Id == researchId).StartDate >= DateTime.UtcNow)
                return 0;
            else
                return _assignments.Count(x => x.Item1 == researchId && x.Item3 < DateTime.UtcNow && x.Item4 > DateTime.UtcNow);
        }
    }
}
