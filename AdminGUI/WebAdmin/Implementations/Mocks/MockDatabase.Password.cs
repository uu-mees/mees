// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Linq;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        private readonly List<ResetToken> _resetTokens = new List<ResetToken>();

        public ResetToken GetResetTokenByUserId(Guid userId) =>
            _resetTokens.FirstOrDefault(t => t.UserId == userId)?.Copy();

        public ResetToken GetResetToken(string token) => _resetTokens.FirstOrDefault(t => t.Token == token)?.Copy();
        public void CreateResetToken(ResetToken resetToken) => _resetTokens.Add(resetToken);

        public void DeleteResetToken(Guid userId) =>
            _resetTokens.Remove(_resetTokens.FirstOrDefault(t => t.UserId == userId));
    }
}
