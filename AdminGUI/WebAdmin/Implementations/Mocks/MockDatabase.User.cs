// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Linq;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        private readonly List<User> _users = new List<User>();

        public List<User> GetUsers(int rowsPerPage, int pageIndex, string filterUsername, string filterEmail,
                                   string filterRole) =>
            new List<User>(_users).GetRange(0, Math.Min(rowsPerPage, _users.Count));

        public int GetUserCount(string filterUsername, string filterEmail, string filterRole) => _users.Count;
        public User GetUserByUsername(string username) => _users.FirstOrDefault(u => u.Username == username)?.Copy();
        public User GetUserById(Guid userId) => _users.FirstOrDefault(u => u.Id == userId)?.Copy();
        public void CreateUser(User user) => _users.Add(user);

        public void UpdateUser(User user) {
            DeleteUser(user.Id);
            CreateUser(user);
        }

        public void DeleteUser(Guid userId) => _users.Remove(_users.FirstOrDefault(u => u.Id == userId));
        public void DeleteUserData(Guid userId) { }
        public List<string> GetFireBaseIds(Guid userId) {
            if (_users.FirstOrDefault(u => u.Id == userId) == null)
                return new List<string> { };
            else
                return new List<string> { "token" };
        }
    }
}
