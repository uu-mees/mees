// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Data;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Mocks {
    public partial class MockDatabase : IDatabase {
        public int ExecuteQuery(string query) => 1;
        public DataTable ExecuteQueryWithResult(string query) => new DataTable();
    }
}
