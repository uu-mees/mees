// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public ResetToken GetResetTokenByUserId(Guid userId) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the token by user id
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT token, userid, expirationdate FROM dbo.PasswordResetTokens WHERE userid=@userid";
                    cmd.Parameters.Add("@userid", SqlDbType.UniqueIdentifier).Value = userId;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the token data
                            return new ResetToken(
                                                  (string)reader[0],
                                                  (Guid)reader[1],
                                                  (DateTime)reader[2]
                                                 );
                        else
                            // No token for this user exists, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public ResetToken GetResetToken(string token) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the token by its id
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT token, userid, expirationdate FROM dbo.PasswordResetTokens WHERE token=@token";
                    cmd.Parameters.Add("@token", SqlDbType.NVarChar).Value = token;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the token data
                            return new ResetToken(
                                                  (string)reader[0],
                                                  (Guid)reader[1],
                                                  (DateTime)reader[2]
                                                 );
                        else
                            // No token with this id exists, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public void CreateResetToken(ResetToken resetToken) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Insert the token into the PasswordResetTokens table
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "INSERT INTO dbo.PasswordResetTokens (UserId, Token, ExpirationDate) VALUES (@userid, @token, @expiration)";
                    cmd.Parameters.Add("@token", SqlDbType.NVarChar).Value = resetToken.Token;
                    cmd.Parameters.Add("@userid", SqlDbType.UniqueIdentifier).Value = resetToken.UserId;
                    cmd.Parameters.Add("@expiration", SqlDbType.DateTime).Value = resetToken.Expiration;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not create the reset token");
                }
            }
            finally {
                Disconnect();
            }
        }

        public void DeleteResetToken(Guid userId) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Remove the token from the PasswordResetTokens table
                    cmd.Connection = _con;
                    cmd.CommandText = "DELETE dbo.PasswordResetTokens WHERE userid=@userid";
                    cmd.Parameters.Add("@userid", SqlDbType.UniqueIdentifier).Value = userId;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not delete the reset token");
                }
            }
            finally {
                Disconnect();
            }
        }
    }
}
