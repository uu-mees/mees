// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public bool UserAssigned(Guid userId, Guid researchId) {
            try {
                User user = GetUserById(userId);
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Set-up assigning
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT endtime FROM UserResearches WHERE userid = @id AND researchid = @researchid";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = userId;
                    cmd.Parameters.Add("@researchid", SqlDbType.UniqueIdentifier).Value = researchId;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read())
                            if ((DateTime)reader[0] > DateTime.UtcNow)
                                return true;
                    }
                }

                return false;
            }
            finally {
                Disconnect();
            }
        }

        public List<Guid> GetAssignedResearches(Guid userId) {
            try {
                Connect();
                List<Guid> researches = new List<Guid>();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Get all research
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, name, description, starttime, endtime, " +
                                      "viewratings, viewsensors, viewraster, viewexposure, viewminigamescores, viewhighscores, viewrouteplanner FROM dbo.Researches WHERE id IN (SELECT researchId FROM dbo.UserResearches WHERE userId = @userId AND endTime > @present)";
                    cmd.Parameters.Add("@userId", SqlDbType.UniqueIdentifier).Value = userId;
                    //Invariant: User can only be assigned to at most 1 research that is yet to end.
                    cmd.Parameters.Add("@present", SqlDbType.DateTime).Value = DateTime.UtcNow;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            researches.Add((Guid)reader[0]);
                        }
                    }
                }
                return researches;
            }
            finally {
                Disconnect();
            }
        }

        public List<Guid> GetParticipants(Guid researchId) {
            try {
                Connect();
                List<Guid> participants = new List<Guid>();
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT userid FROM UserResearches WHERE researchid = @id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = researchId;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) participants.Add((Guid)reader[0]);
                    }
                }
                return participants;
            }
            finally {
                Disconnect();
            }
        }

        public void AssignUser(Guid userId, Guid researchId) {
            try {
                User user = GetUserById(userId);
                Research research = GetResearchById(researchId);
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Set-up assigning
                    cmd.Connection = _con;
                    cmd.CommandText = "INSERT INTO UserResearches (userid, researchid, starttime, endtime)" +
                                      "VALUES (@userid, @researchid, @startdate, @enddate)";
                    cmd.Parameters.Add("@userid", SqlDbType.UniqueIdentifier).Value = userId;
                    cmd.Parameters.Add("@researchId", SqlDbType.UniqueIdentifier).Value = researchId;
                    cmd.Parameters.Add("@startdate", SqlDbType.DateTime).Value = DateTime.UtcNow;
                    cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = (user.Role == User.UserRole.User) ? research.EndDate : DateTime.MaxValue;

                    cmd.ExecuteNonQuery();
                }
            }
            finally {
                Disconnect();
            }
        }

        public void UnassignUser(Guid userId, Guid researchId, DateTime standardEnd) {
            try {
                User user = GetUserById(userId);
                Research research = GetResearchById(researchId);
                if (user.Role == User.UserRole.Researcher)
                    standardEnd = DateTime.MaxValue;
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Set-up assigning
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "UPDATE UserResearches SET endtime = @endtime WHERE userid = @userid AND endtime = @standardend AND researchId = @researchId";
                    cmd.Parameters.Add("@userid", SqlDbType.UniqueIdentifier).Value = userId;
                    cmd.Parameters.Add("@standardend", SqlDbType.DateTime).Value = standardEnd;
                    cmd.Parameters.Add("@researchId", SqlDbType.UniqueIdentifier).Value = researchId;
                    cmd.Parameters.Add("@endtime", SqlDbType.DateTime).Value = DateTime.UtcNow;

                    cmd.ExecuteNonQuery();
                }
            }
            finally {
                Disconnect();
            }
        }

        public int CountUsersInResearch(Guid researchId) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Set-up assigning
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT COUNT(*) FROM Users u WHERE u.Role = 0 AND u.Id IN (SELECT UserId FROM UserResearches ur WHERE researchId = @researchId);";
                    cmd.Parameters.Add("@researchId", SqlDbType.UniqueIdentifier).Value = researchId;
                    return (int)cmd.ExecuteScalar();
                }
            }
            finally {
                Disconnect();
            }
        }

        public int CountActiveUsersInResearch(Guid researchId) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Set-up assigning
                    cmd.Connection = _con;
                    cmd.CommandText =
                        @"SELECT COUNT(*) FROM Users u WHERE u.Role = 0 AND u.Id IN (
	                        SELECT ur.UserId FROM UserResearches ur
	                        INNER JOIN Researches r ON ur.researchId = r.Id
	                        WHERE ur.researchId = @researchId 
	                        AND ur.startTime < getutcdate() 
	                        AND ur.endTime > getutcdate()
	                        AND r.StartTime < getutcdate()
                        );";
                    cmd.Parameters.Add("@researchId", SqlDbType.UniqueIdentifier).Value = researchId;
                    return (int)cmd.ExecuteScalar();
                }
            }
            finally {
                Disconnect();
            }
        }
    }
}
