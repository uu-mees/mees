// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public int ExecuteQuery(string query) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = query;
                    return cmd.ExecuteNonQuery();
                }
            }
            finally {
                Disconnect();
            }
        }

        public DataTable ExecuteQueryWithResult(string query) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = query;
                    DataTable dataTable = new DataTable();

                    // We can't use DataTable.Load() because that will not parse byte[] fields correctly
                    using (SqlDataReader result = cmd.ExecuteReader()) {
                        while (result.Read()) {
                            // If the columns have not been set yet, add them
                            if (dataTable.Columns.Count == 0)
                                for (int i = 0; i < result.FieldCount; i++) {
                                    // Add a suffix if a column with this name already exists
                                    string colName = result.GetName(i);
                                    int count = 1;
                                    while (dataTable.Columns.Contains(colName))
                                        colName = $"{colName}_{count++}";
                                    dataTable.Columns.Add(colName);
                                }

                            // Insert the row
                            object[] row = new object[result.FieldCount];
                            for (int i = 0; i < result.FieldCount; i++)
                                if (result.GetFieldType(i) == typeof(byte[]))
                                    // If this field is a byte[], parse it so it can be printed
                                    row[i] = "0x" + BitConverter.ToString((byte[])result[i]).Replace("-", "");
                                else
                                    row[i] = result[i].ToString();
                            dataTable.Rows.Add(row);
                        }
                    }

                    return dataTable;
                }
            }
            finally {
                Disconnect();
            }
        }
    }
}
