// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public DataSource GetDataSourceByUrl(string url) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the DataSource by Url
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, url, active, type FROM dbo.DataSources WHERE url=@url";
                    cmd.Parameters.Add("@url", SqlDbType.NVarChar).Value = url;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the DataSource data
                            return new DataSource(
                                                  (Guid)reader[0],
                                                  (string)reader[1],
                                                  (bool)reader[2],
                                                  (DataSource.DataSourceType)reader[3]
                                                 );
                        else
                            // This DataSource does not exist, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public DataSource GetDataSourceById(Guid id) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the DataSource by id
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, url, active, type FROM dbo.DataSources WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the token data
                            return new DataSource(
                                                  (Guid)reader[0],
                                                  (string)reader[1],
                                                  (bool)reader[2],
                                                  (DataSource.DataSourceType)reader[3]
                                                 );
                        else
                            // No token for this user exists, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public List<DataSource> GetDataSources() {
            try {
                Connect();
                List<DataSource> dataSources = new List<DataSource>();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Get all DataSources
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT id, url, active, type FROM dbo.DataSources ORDER BY type DESC, active DESC, url";

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read())
                            // Read the data source data
                            dataSources.Add(new DataSource(
                                                           (Guid)reader[0],
                                                           (string)reader[1],
                                                           (bool)reader[2],
                                                           (DataSource.DataSourceType)reader[3]
                                                          ));
                    }
                }

                return dataSources;
            }
            finally {
                Disconnect();
            }
        }

        public void AddDataSource(DataSource dataSource) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Insert the new data source into the DataSources table
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "INSERT INTO dbo.DataSources (Id, Url, Type, Active) VALUES (@id, @url, @type, @active)";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = dataSource.Id;
                    cmd.Parameters.Add("@url", SqlDbType.NVarChar).Value = dataSource.Url;
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = dataSource.Active;
                    cmd.Parameters.Add("@type", SqlDbType.Int).Value = dataSource.Type;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not add the data source");
                }
            }
            finally {
                Disconnect();
            }
        }

        public void UpdateDataSource(DataSource dataSource) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Update the data source fields
                    cmd.Connection = _con;
                    cmd.CommandText = "UPDATE dbo.DataSources SET url=@url, active=@active, type=@type WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = dataSource.Id;
                    cmd.Parameters.Add("@url", SqlDbType.NVarChar).Value = dataSource.Url;
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = dataSource.Active;
                    cmd.Parameters.Add("@type", SqlDbType.Int).Value = dataSource.Type;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not update the data source");
                }
            }
            finally {
                Disconnect();
            }
        }

        public void RemoveDataSource(Guid id) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Remove the data source from the DataSources table
                    cmd.Connection = _con;
                    cmd.CommandText = "DELETE dbo.DataSources WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = id;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not remove the data source");
                }
            }
            finally {
                Disconnect();
            }
        }
    }
}
