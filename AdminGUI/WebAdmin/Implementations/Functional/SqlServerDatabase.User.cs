// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public User GetUserByUsername(string username) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, username, email, hashedpassword, salt, role FROM dbo.Users WHERE username=@username";
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = username;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the user data
                            return new User(
                                            (Guid)reader[0],
                                            (string)reader[1],
                                            (string)reader[2],
                                            (byte[])reader[3],
                                            (Guid)reader[4],
                                            (User.UserRole)reader[5]
                                           );
                        else
                            // No user with this username exists, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public User GetUserById(Guid id) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, username, email, hashedpassword, salt, role FROM dbo.Users WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read())
                            // Read the user data
                            return new User(
                                            (Guid)reader[0],
                                            (string)reader[1],
                                            (string)reader[2],
                                            (byte[])reader[3],
                                            (Guid)reader[4],
                                            (User.UserRole)reader[5]
                                           );
                        else
                            // No user with this Id exists, return null
                            return null;
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public List<User> GetUsers(int rowsPerPage, int pageIndex, string filterUsername, string filterEmail,
                                   string filterRole) {
            try {
                Connect();
                List<User> users = new List<User>();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Get all users fitting the filter criteria
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT id, username, email, hashedpassword, salt, role FROM dbo.Users WHERE username LIKE @username AND email LIKE @mail AND role LIKE @role ORDER BY UserName OFFSET @offset ROWS FETCH NEXT @rows ROWS ONLY";
                    cmd.Parameters.Add("@rows", SqlDbType.Int).Value = rowsPerPage;
                    cmd.Parameters.Add("@offset", SqlDbType.Int).Value = rowsPerPage * pageIndex;
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = "%" + filterUsername + "%";
                    cmd.Parameters.Add("@mail", SqlDbType.NVarChar).Value = "%" + filterEmail + "%";
                    cmd.Parameters.Add("@role", SqlDbType.NVarChar).Value = "%" + filterRole + "%";

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read())
                            // Read the user data
                            users.Add(new User(
                                               (Guid)reader[0],
                                               (string)reader[1],
                                               (string)reader[2],
                                               (byte[])reader[3],
                                               (Guid)reader[4],
                                               (User.UserRole)reader[5]
                                              ));
                    }
                }

                return users;
            }
            finally {
                Disconnect();
            }
        }

        public int GetUserCount(string filterUsername, string filterEmail, string filterRole) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Count the users
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT COUNT(*) FROM dbo.Users WHERE username LIKE @username AND email LIKE @mail AND role LIKE @role";
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = "%" + filterUsername + "%";
                    cmd.Parameters.Add("@mail", SqlDbType.NVarChar).Value = "%" + filterEmail + "%";
                    cmd.Parameters.Add("@role", SqlDbType.NVarChar).Value = "%" + filterRole + "%";

                    return (int)cmd.ExecuteScalar();
                }
            }
            finally {
                Disconnect();
            }
        }

        public void CreateUser(User user) {
            try {
                Connect();
                // Insert the User into the Users table
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "INSERT INTO dbo.Users (Id, Role, UserName, Email, TrackLocation, HashedPassword, Salt, LastQuestionnaireTime, LastMinigameTime, Height, Weight, Sex, DateOfBirth)" +
                        "VALUES (@id, @role, @username, @email, 1, @hashedPassword, @salt, NULL, NULL, 0, 0, 'Other', '1900-01-01');";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = user.Id;
                    cmd.Parameters.Add("@role", SqlDbType.Int).Value = user.Role;
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = user.Username;
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = user.Email;
                    cmd.Parameters.Add("@hashedPassword", SqlDbType.VarBinary).Value = user.Password;
                    cmd.Parameters.Add("@salt", SqlDbType.UniqueIdentifier).Value = user.Salt;
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                        throw new Exception((string)result);
                }
            }
            finally {
                Disconnect();
            }
        }

        public void UpdateUser(User user) {
            try {
                Connect();
                // Update the User object in the database
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = "UPDATE dbo.Users SET email=@email, hashedpassword=@password WHERE id=@id;";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = user.Id;
                    cmd.Parameters.Add("@password", SqlDbType.Binary).Value = user.Password;
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = user.Email;
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                        throw new Exception((string)result);
                }
            }
            finally {
                Disconnect();
            }
        }

        public void DeleteUser(Guid id) {
            try {
                Connect();
                // Delete the User from the database
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = "DELETE dbo.Users WHERE id=@id;";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = id;
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                        throw new Exception((string)result);
                }
            }
            finally {
                Disconnect();
            }
        }

        public void DeleteUserData(Guid id) {
            try {
                Connect();
                // Delete all data belonging to this User
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Connection = _con;
                    cmd.CommandText = @"
                    DELETE dbo.PasswordResetTokens WHERE UserId = @id;
	                DELETE dbo.DailyExposures WHERE UserId = @id;
	                DELETE dbo.MinigameScores WHERE UserId = @id;
	                DELETE dbo.Triggers WHERE UserId = @id;
	                DELETE dbo.UserLocationExposures WHERE UserId = @id;
	                DELETE dbo.UserRatings WHERE UserId = @id;
                    UPDATE dbo.Users SET Height=NULL, Weight=NULL, Sex=NULL, DateOfBirth=NULL WHERE Id=@id;";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = id;
                    object result = cmd.ExecuteScalar();
                    if (result != null)
                        throw new Exception((string)result);
                }
            }
            finally {
                Disconnect();
            }
        }

        public List<string> GetFireBaseIds(Guid userId) {
            List<string> result = new List<string>();
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Get all IDs that we have selected.
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT FireBaseID FROM dbo.FireBases WHERE UserId = @userId";
                    cmd.Parameters.Add("@userId", SqlDbType.UniqueIdentifier).Value = userId;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read())
                            // Read the user data
                            result.Add((string)reader[0]);
                    }
                }
                return result;
            }
            finally {
                Disconnect();
            }
        }
    }
}
