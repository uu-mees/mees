// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Data.SqlClient;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        private readonly SqlConnection _con;
        public SqlServerDatabase(string connectionString) => _con = new SqlConnection(connectionString);

        private void Connect() {
            _con.Close();
            _con.Open();
        }

        private void Disconnect() => _con.Close();
    }
}
