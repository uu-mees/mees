// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public class FirebaseNotificationProvider : INotificationProvider {
        /// <summary>
        ///     Sends a notification to a User's device using Firebase Cloud Messaging
        /// </summary>
        /// <param name="deviceToken">The Firebase token of the device to which the notification will be sent</param>
        /// <param name="title">The title of the notification</param>
        /// <param name="body">The body of the notification</param>
        public async Task SendPushNotification(List<string> tokens, string title, string body) {
            // Setup the notification with the correct layout
            var message = new {
                notification = new {
                    title,
                    body
                },
                registration_ids = tokens.ToArray()
            };

            // Convert the notification to JSON
            string jsonNotification = JsonConvert.SerializeObject(message);

            // Setup the HTTP request and add our Firebase authorization token to the header
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://fcm.googleapis.com/fcm/send");

            request.Headers.TryAddWithoutValidation("Authorization",
                                                        "key =" +
                                                        ConfigurationManager.AppSettings["FireBaseAuthorizationToken"]);
            request.Content = new StringContent(jsonNotification, Encoding.UTF8, "application/json");


            // Send the request
            using (HttpClient client = new HttpClient()) {
                await client.SendAsync(request);
            }
        }
    }
}
