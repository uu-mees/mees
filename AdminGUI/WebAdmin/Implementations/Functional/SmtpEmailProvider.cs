// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Diagnostics.CodeAnalysis;
using System.Net.Mail;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    // Exclude from code coverage as this class is only a wrapper for the SmtpClient code
    [ExcludeFromCodeCoverage]
    public class SmtpEmailProvider : IEmailProvider {
        public void SendMail(string to, string subject, string body) {
            // Create a new SmtpClient from the Web.config configuration
            SmtpClient smtp = new SmtpClient {
                EnableSsl = true
            };

            // Create a new MailMessage and send it
            MailMessage mail = new MailMessage {
                IsBodyHtml = true
            };
            mail.Subject = subject;
            mail.Body = body;
            mail.To.Add(to);

            smtp.Send(mail);
        }
    }
}
