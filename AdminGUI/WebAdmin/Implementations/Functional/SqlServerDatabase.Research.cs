// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;

namespace WebAdmin.Implementations.Functional {
    public partial class SqlServerDatabase : IDatabase {
        public void CreateResearch(Research research) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Insert the new Research into the Research table
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "INSERT INTO dbo.Researches VALUES (@id, @name, @description, @starttime, @endtime, " +
                        "@viewratings, @viewsensors, @viewraster, @viewexposure, @viewminigamescores, @viewhighscores, @viewrouteplanner)";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = research.Id;
                    cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = research.Name;
                    cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = research.Description;
                    cmd.Parameters.Add("@starttime", SqlDbType.DateTime).Value = research.StartDate;
                    cmd.Parameters.Add("@endtime", SqlDbType.DateTime).Value = research.EndDate;
                    cmd.Parameters.Add("@viewratings", SqlDbType.Bit).Value = research.Permission.ShowRatings;
                    cmd.Parameters.Add("@viewsensors", SqlDbType.Bit).Value = research.Permission.ShowSensors;
                    cmd.Parameters.Add("@viewraster", SqlDbType.Bit).Value = research.Permission.ShowRaster;
                    cmd.Parameters.Add("@viewexposure", SqlDbType.Bit).Value = research.Permission.ShowExposure;
                    cmd.Parameters.Add("@viewminigamescores", SqlDbType.Bit).Value = research.Permission.ShowPersonalScores;
                    cmd.Parameters.Add("@viewhighscores", SqlDbType.Bit).Value = research.Permission.ShowHighScores;
                    cmd.Parameters.Add("@viewrouteplanner", SqlDbType.Bit).Value = research.Permission.ShowRoutePlanner;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not create the research");
                }
            }
            finally {
                Disconnect();
            }
        }

        public List<Research> GetResearches(int rowsPerPage, int pageIndex, string filterName, string filterDescription) {
            try {
                Connect();
                List<Research> researches = new List<Research>();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Get all research
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, name, description, starttime, endtime, " +
                                      "viewratings, viewsensors, viewraster, viewexposure, viewminigamescores, viewhighscores, viewrouteplanner FROM dbo.Researches WHERE name LIKE @name AND description LIKE @description ORDER BY starttime DESC OFFSET @offset ROWS FETCH NEXT @rows ROWS ONLY";
                    cmd.Parameters.Add("@rows", SqlDbType.Int).Value = rowsPerPage;
                    cmd.Parameters.Add("@offset", SqlDbType.Int).Value = rowsPerPage * pageIndex;
                    cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = "%" + filterName + "%";
                    cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = "%" + filterDescription + "%";

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            // Read the Research permissions
                            bool[] permissioncast = new bool[7];
                            for (int i = 5; i < 12; i++)
                                permissioncast[i - 5] = (bool)reader[i];
                            PermissionList permission = new PermissionList(permissioncast);
                            //read and insert the research data.
                            researches.Add(new Research(
                                                        (Guid)reader[0],
                                                        (string)reader[1],
                                                        (string)reader[2],
                                                        permission,
                                                        (DateTime)reader[3],
                                                        (DateTime)reader[4]
                                                       ));
                        }
                    }
                }

                return researches;
            }
            finally {
                Disconnect();
            }
        }

        public int GetResearchCount(string filterName, string filterDescription) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Count the Researches
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "SELECT COUNT(*) FROM dbo.Researches WHERE name LIKE @name AND description LIKE @description";
                    cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = "%" + filterName + "%";
                    cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = "%" + filterDescription + "%";

                    return (int)cmd.ExecuteScalar();
                }
            }
            finally {
                Disconnect();
            }
        }

        public Research GetResearchById(Guid id) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the Research by its Id
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, name, description, starttime, endtime, " +
                                      "viewratings, viewsensors, viewraster, viewexposure, viewminigamescores, viewhighscores, viewrouteplanner FROM dbo.Researches WHERE id = @id";
                    cmd.Parameters.Add("id", SqlDbType.UniqueIdentifier).Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read()) {
                            // Read the Research permissions
                            bool[] permissioncast = new bool[7];
                            for (int i = 5; i < 12; i++)
                                permissioncast[i - 5] = (bool)reader[i];
                            PermissionList permission = new PermissionList(permissioncast);
                            //read and insert the research data.
                            return new Research(
                                                (Guid)reader[0],
                                                (string)reader[1],
                                                (string)reader[2],
                                                permission,
                                                (DateTime)reader[3],
                                                (DateTime)reader[4]
                                               );
                        }
                        else {
                            return null;
                        }
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public Research GetResearchByName(string name) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Find the Research by its name
                    cmd.Connection = _con;
                    cmd.CommandText = "SELECT id, name, description, starttime, endtime, " +
                                      "viewratings, viewsensors, viewraster, viewexposure, viewminigamescores, viewhighscores, viewrouteplanner FROM dbo.Researches WHERE name = @name";
                    cmd.Parameters.Add("name", SqlDbType.NVarChar).Value = name;

                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        if (reader.Read()) {
                            // Read the Research permissions
                            bool[] permissioncast = new bool[7];
                            for (int i = 5; i < 12; i++)
                                permissioncast[i - 5] = (bool)reader[i];
                            PermissionList permission = new PermissionList(permissioncast);
                            return new Research(
                                                (Guid)reader[0],
                                                (string)reader[1],
                                                (string)reader[2],
                                                permission,
                                                (DateTime)reader[3],
                                                (DateTime)reader[4]
                                               );
                        }
                        else {
                            return null;
                        }
                    }
                }
            }
            finally {
                Disconnect();
            }
        }

        public void RemoveResearch(Guid researchId) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Remove the research from the Researches table
                    cmd.Connection = _con;
                    cmd.CommandText = "DELETE dbo.Researches WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = researchId;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not delete research");
                }
            }
            finally {
                Disconnect();
            }
        }

        public void UpdateResearch(Research research) {
            try {
                Connect();
                using (SqlCommand cmd = new SqlCommand()) {
                    // Update the research fields
                    cmd.Connection = _con;
                    cmd.CommandText =
                        "UPDATE dbo.Researches SET name=@name, description=@description, starttime=@starttime, endtime=@endtime, " +
                        "viewratings=@viewratings, viewsensors=@viewsensors, viewraster=@viewraster, viewexposure=@viewexposure, viewminigamescores=@viewminigamescores, viewhighscores=@viewhighscores, viewrouteplanner=@viewrouteplanner WHERE id=@id";
                    cmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = research.Id;
                    cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = research.Name;
                    cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = research.Description;
                    cmd.Parameters.Add("@starttime", SqlDbType.DateTime).Value = research.StartDate;
                    cmd.Parameters.Add("@endtime", SqlDbType.DateTime).Value = research.EndDate;
                    cmd.Parameters.Add("@viewratings", SqlDbType.Bit).Value = research.Permission.ShowRatings;
                    cmd.Parameters.Add("@viewsensors", SqlDbType.Bit).Value = research.Permission.ShowSensors;
                    cmd.Parameters.Add("@viewraster", SqlDbType.Bit).Value = research.Permission.ShowRaster;
                    cmd.Parameters.Add("@viewexposure", SqlDbType.Bit).Value = research.Permission.ShowExposure;
                    cmd.Parameters.Add("@viewminigamescores", SqlDbType.Bit).Value = research.Permission.ShowPersonalScores;
                    cmd.Parameters.Add("@viewhighscores", SqlDbType.Bit).Value = research.Permission.ShowHighScores;
                    cmd.Parameters.Add("@viewrouteplanner", SqlDbType.Bit).Value = research.Permission.ShowRoutePlanner;
                    if (cmd.ExecuteNonQuery() != 1)
                        throw new Exception("Could not update the research");
                }
            }
            finally {
                Disconnect();
            }
        }
    }
}
