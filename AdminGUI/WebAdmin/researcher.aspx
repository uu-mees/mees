<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="researcher.aspx.cs" Inherits="WebAdmin.ResearcherPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Researcher" ContentPlaceHolderID="content" runat="server">
    <div class="mees-page">
        <div class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Select the Research you want to export</h3>
            </div>
            <div class="mees-title-button">
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="ExportResearch" ID="button_export" class="btn btn-success">Export Research</asp:LinkButton>
            </div>
        </div>
        <div style="overflow: auto" runat="server" id="table_researches">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th scope="col" style="width: 30px;"></th>
                        <th scope="col" style="min-width: 200px;">Name</th>
                        <th scope="col" style="min-width: 200px;">Description</th>
                        <th scope="col" style="min-width: 200px;">Start Date</th>
                        <th scope="col" style="min-width: 200px;">End Date</th>
                        <th scope="col" style="min-width: 200px;">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Print research rows here -->
                    <asp:Literal runat="server" ID="ResearchRows" />
                </tbody>
            </table>
        </div>
        <script type="application/javascript">
            FixTableAlignment();
        </script>
    </div>
</asp:Content>
