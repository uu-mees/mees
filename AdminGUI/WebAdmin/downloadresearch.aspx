<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="downloadresearch.aspx.cs" Inherits="WebAdmin.DownloadResearchPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="DownloadResearch" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page-small">
        <div class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Download Research</h3>
            </div>
        </div>
        <div style="overflow: auto">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th scope="col" style="min-width: 200px;">SQL table</th>
                        <th scope="col" style="width: 180px;">Export to CSV</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Users</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadUsers_Click" ID="button_download_users" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>Research</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadResearch_Click" ID="button_download_research" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>User Ratings</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadRatings_Click" ID="button_download_ratings" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>User Location Exposures</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadLocationExposures_Click" ID="button_download_location_exposures" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>Minigame Scores</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadMinigameScores_Click" ID="button_download_minigame_scores" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>Daily Exposures</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadDailyExposures_Click" ID="button_download_daily_exposures" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>Questionnaires</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadQuestionnaires_Click" ID="button_download_questionnaires" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>Triggers</td>
                        <td>
                            <asp:LinkButton runat="server" Width="180px" OnClick="DownloadTriggers_Click" ID="button_download_triggers" CssClass="btn btn-info">Download CSV</asp:LinkButton>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <script type="application/javascript">
            FixTableAlignment();
        </script>
    </div>
</asp:Content>
