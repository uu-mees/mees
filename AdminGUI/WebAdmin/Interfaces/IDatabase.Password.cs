﻿// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using WebAdmin.DataTypes;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Gets a ResetToken from the database by its User Id
        /// </summary>
        /// <param name="userId">The Id of the User whose token to get</param>
        /// <returns>The ResetToken associated to the User, or null if no such token exists</returns>
        ResetToken GetResetTokenByUserId(Guid userId);

        /// <summary>
        ///     Gets a ResetToken from the database by its token field
        /// </summary>
        /// <param name="token">The token to look for</param>
        /// <returns>The ResetToken with this Id, or null if no such token exists</returns>
        ResetToken GetResetToken(string token);

        /// <summary>
        ///     Adds a new ResetToken to the database
        /// </summary>
        /// <param name="resetToken">The ResetToken to add</param>
        void CreateResetToken(ResetToken resetToken);

        /// <summary>
        ///     Removes a ResetToken from the database
        /// </summary>
        /// <param name="userId">The Id of the User whose ResetToken to delete</param>
        void DeleteResetToken(Guid userId);
    }
}