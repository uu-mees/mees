// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAdmin.Interfaces {
    public interface INotificationProvider {
        /// <summary>
        ///     Sends a push notification
        /// </summary>
        /// <param name="token">The token of the user to send the notification to</param>
        /// <param name="title">The title of the notification</param>
        /// <param name="body">The body of the notification</param>
        Task SendPushNotification(List<string> token, string title, string body);
    }
}
