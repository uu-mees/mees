// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Gets a List of all Researches assigned to a User
        /// </summary>
        /// <param name="userId">The Id of the User whose Researches to get</param>
        /// <returns>A List of the Researches assigned to the User</returns>
        List<Guid> GetAssignedResearches(Guid userId);

        /// <summary>
        ///     Gets a List of all User Ids assigned to a Research
        /// </summary>
        /// <param name="researchId">The Id of the Research of which the assigned Users to get</param>
        /// <returns>A List of all User Ids assigned to the Research</returns>
        List<Guid> GetParticipants(Guid researchId);

        /// <summary>
        ///     Assigns a User to a Research
        /// </summary>
        /// <param name="userId">The Id of the User to assign</param>
        /// <param name="researchId">The Id of the Research to assign to</param>
        void AssignUser(Guid userId, Guid researchId);

        /// <summary>
        ///     Returns whether a specific User is assigned to a specific Research
        /// </summary>
        /// <param name="userId">The User to test</param>
        /// <param name="researchId">The Research to test</param>
        /// <returns>Whether the User is assigned to the Research</returns>
        bool UserAssigned(Guid userId, Guid researchId);

        /// <summary>
        ///     Unassigns a User from a Research
        /// </summary>
        /// <param name="userId">The Id of the User to unassign</param>
        /// <param name="researchId">The Id of the Research to unassign from</param>
        /// <param name="standardEnd">The time the user would normally be unassigned from the research</param>
        void UnassignUser(Guid userId, Guid researchId, DateTime standardEnd);

        /// <summary>
        ///     Counts the total number of Users assigned to a Research
        /// </summary>
        /// <param name="researchId">The ID of the Research to count Users from</param>
        /// <returns>The number of Users assigned to the Research</returns>
        int CountUsersInResearch(Guid researchId);

        /// <summary>
        ///     Counts the total number of active Users assigned to a Research
        /// </summary>
        /// <param name="researchId">The ID of the Research to count Users from</param>
        /// <returns>The number of active Users in the Research</returns>
        int CountActiveUsersInResearch(Guid researchId);
    }
}
