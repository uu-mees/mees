// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

namespace WebAdmin.Interfaces {
    public interface IEmailProvider {
        /// <summary>
        ///     Sends an email
        /// </summary>
        /// <param name="to">The email address to send the email to</param>
        /// <param name="subject">The subject of the email</param>
        /// <param name="body">The body of the email</param>
        void SendMail(string to, string subject, string body);
    }
}
