﻿// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Data;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Executes a plaintext query on the database
        /// </summary>
        /// <param name="query">The query to execute</param>
        /// <returns>The number of rows changed by the query</returns>
        int ExecuteQuery(string query);

        /// <summary>
        ///     Executes a plaintext query on the database and returns a result in table form
        /// </summary>
        /// <param name="query">The query to execute</param>
        /// <returns>The DataTable resulting from the query</returns>
        DataTable ExecuteQueryWithResult(string query);
    }
}