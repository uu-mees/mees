﻿// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Gets the Research objects matching the filter criteria
        /// </summary>
        /// <param name="rowsPerPage">The maximum number of Researches to get</param>
        /// <param name="pageIndex">The offset applied when retrieving the Researches. This value is multiplied by the rowsPerPage.</param>
        /// <param name="filterName">A value to look for in the name column. Leave this empty to disable filtering.</param>
        /// <param name="filterDescription">A value to look for in the description column. Leave this empty to disable filtering.</param>
        /// <returns>A List of Researches matching the filtering criteria</returns>
        List<Research> GetResearches(int rowsPerPage, int pageIndex, string filterName, string filterDescription);

        /// <summary>
        ///     Gets the number of Research objects matching the filter criteria
        /// </summary>
        /// <param name="filterName">A value to look for in the name column. Leave this empty to disable filtering.</param>
        /// <param name="filterDescription">A value to look for in the description column. Leave this empty to disable filtering.</param>
        /// <returns>The number of Researches matching the filtering criteria</returns>
        int GetResearchCount(string filterName, string filterDescription);

        /// <summary>
        ///     Gets a Research from the database by its name
        /// </summary>
        /// <param name="name">The Research name to look for</param>
        /// <returns>The Research with this name, or null if no such Research exists</returns>
        Research GetResearchByName(string name);

        /// <summary>
        ///     Gets a Research from the database by its Id
        /// </summary>
        /// <param name="researchId">The Research Id to look for</param>
        /// <returns>The Research with this Id, or null if no such Research exists</returns>
        Research GetResearchById(Guid researchId);

        /// <summary>
        ///     Adds a new Research object to the database
        /// </summary>
        /// <param name="research">The Research object to add</param>
        void CreateResearch(Research research);

        /// <summary>
        ///     Updates a Research object in the database
        /// </summary>
        /// <param name="research">The Research object to update</param>
        void UpdateResearch(Research research);

        /// <summary>
        ///     Removes a Research object from the database
        /// </summary>
        /// <param name="researchId">The Id of the Research to delete</param>
        void RemoveResearch(Guid researchId);
    }
}