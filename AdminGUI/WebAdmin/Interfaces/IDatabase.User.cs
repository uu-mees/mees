// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Gets the User objects matching the filter criteria
        /// </summary>
        /// <param name="rowsPerPage">The maximum number of Users to get</param>
        /// <param name="pageIndex">The offset applied when retrieving the Users. This value is multiplied by the rowsPerPage</param>
        /// <param name="filterUsername">A value to look for in the username column. Leave this empty to disable filtering.</param>
        /// <param name="filterEmail">A value to look for in the email column. Leave this empty to disable filtering.</param>
        /// <param name="filterRole">A value to look for in the role column. Leave this empty to disable filtering.</param>
        /// <returns>A List of User objects matching the criteria</returns>
        List<User> GetUsers(int rowsPerPage, int pageIndex, string filterUsername, string filterEmail,
                            string filterRole);

        /// <summary>
        ///     Gets the number of User entries matching the filter criteria
        /// </summary>
        /// <param name="filterUsername">A value to look for in the username column. Leave this empty to disable filtering.</param>
        /// <param name="filterEmail">A value to look for in the email column. Leave this empty to disable filtering.</param>
        /// <param name="filterRole">A value to look for in the role column. Leave this empty to disable filtering.</param>
        /// <returns>The number of User objects matching the criteria</returns>
        int GetUserCount(string filterUsername, string filterEmail, string filterRole);

        /// <summary>
        ///     Gets a User object by its username
        /// </summary>
        /// <param name="username">The username to look for</param>
        /// <returns>The User object with the given username, or null if no such User exists</returns>
        User GetUserByUsername(string username);

        /// <summary>
        ///     Gets a User object by its Id
        /// </summary>
        /// <param name="userId">The Id to look for</param>
        /// <returns>The User object with the given Id, or null if no such User exists</returns>
        User GetUserById(Guid userId);

        /// <summary>
        ///     Adds a new User to the database
        /// </summary>
        /// <param name="user">The User object to add</param>
        void CreateUser(User user);

        /// <summary>
        ///     Updates a existing User in the database
        /// </summary>
        /// <param name="user">The User object to update</param>
        void UpdateUser(User user);

        /// <summary>
        ///     Deletes a User from the database
        /// </summary>
        /// <param name="userId">Id of the User to delete</param>
        void DeleteUser(Guid userId);

        /// <summary>
        ///     Deletes all data belonging to a User
        /// </summary>
        /// <param name="userId">The Id of the User whose data to delete</param>
        void DeleteUserData(Guid userId);

        /// <summary>
        ///     Retrieves all FireBase Ids associated to a User from the database
        /// </summary>
        /// <param name="userId">The Id of the User whose FireBase Ids to get</param>
        /// <returns></returns>
        List<string> GetFireBaseIds(Guid userId);
    }
}
