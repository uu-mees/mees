﻿// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;

namespace WebAdmin.Interfaces {
    public partial interface IDatabase {
        /// <summary>
        ///     Gets all DataSources from the database
        /// </summary>
        /// <returns>A List of DataSources</returns>
        List<DataSource> GetDataSources();

        /// <summary>
        ///     Gets a DataSource from the database by its Url
        /// </summary>
        /// <param name="url">The Url to look for</param>
        /// <returns>The DataSource object matching the Url, or null if no such DataSource exists</returns>
        DataSource GetDataSourceByUrl(string url);

        /// <summary>
        ///     Gets a DataSource from the database by its Id
        /// </summary>
        /// <param name="dataSourceId">The Id to look for</param>
        /// <returns>The DataSource object matching the Id, or null if no such DataSource exists</returns>
        DataSource GetDataSourceById(Guid dataSourceId);

        /// <summary>
        ///     Adds a new DataSource to the database
        /// </summary>
        /// <param name="dataSource">The DataSource to add</param>
        void AddDataSource(DataSource dataSource);

        /// <summary>
        ///     Updates a DataSource in the database
        /// </summary>
        /// <param name="dataSource">The DataSource to update</param>
        void UpdateDataSource(DataSource dataSource);

        /// <summary>
        ///     Removes a DataSource from the database
        /// </summary>
        /// <param name="dataSourceId">The Id of the DataSource to delete</param>
        void RemoveDataSource(Guid dataSourceId);
    }
}