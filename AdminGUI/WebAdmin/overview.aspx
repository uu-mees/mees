﻿<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="overview.aspx.cs" Inherits="WebAdmin.OverviewPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Overview" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page">
        <h3 class="mees-page-title">Overview</h3>
        <div class="card-deck">
            <!-- Users card -->
            <div class="card bg-light mb-3 h-100">
                <div class="card-header">Users</div>
                <div class="card-body">
                    <h5 class="card-title">Manage Users</h5>
                    <p class="card-text">List, search, edit and delete user accounts or wipe their data.</p>
                    <div class="text-right">
                        <a href="users.aspx" class="btn btn-info">View User Accounts</a>
                    </div>
                    <hr />
                    <h5 class="card-title">Add User</h5>
                    <p class="card-text">Create a new Administrator or Researcher account.</p>
                    <div class="text-right">
                        <a href="user.aspx?task=create" class="btn btn-info">Create Account</a>
                    </div>
                </div>
            </div>

            <!-- Research card -->
            <div class="card bg-light mb-3 h-100">
                <div class="card-header">Research</div>
                <div class="card-body">
                    <h5 class="card-title">Manage Research</h5>
                    <p class="card-text">List, search and edit existing research.</p>
                    <div class="text-right">
                        <a href="researches.aspx" class="btn btn-info">View Research</a>
                    </div>
                    <hr />
                    <h5 class="card-title">Add Research</h5>
                    <p class="card-text">Create and setup a new Research.</p>
                    <div class="text-right">
                        <a href="research.aspx?task=create" class="btn btn-info">Create Research</a>
                    </div>
                    <hr />
                    <h5 class="card-title">Query Data</h5>
                    <p class="card-text">Use SQL queries to get the data needed for your research.</p>
                    <div class="text-right">
                        <a href="query.aspx" class="btn btn-info">Create a Query</a>
                    </div>
                </div>
            </div>

            <!-- Data card -->
            <div class="card bg-light mb-3 h-100">
                <div class="card-header">Data Management</div>
                <div class="card-body">
                    <h5 class="card-title">Delete User Ratings</h5>
                    <p class="card-text">
                        Delete all User Ratings older than the selected date:
                        <asp:TextBox runat="server" ID="deleteRatingsDate" placeholder="yyyy/mm/dd" MaxLength="20" CssClass="form-control input-lg mees-top-spacing" />
                    </p>
                    <div class="text-right">
                        <asp:LinkButton runat="server" OnClick="DeleteRatings_Click" CssClass="btn btn-danger">Delete Ratings</asp:LinkButton>
                    </div>
                    <hr />
                    <h5 class="card-title">Export Data</h5>
                    <p class="card-text">Download all data from the database.</p>
                    <div class="text-right">
                        <a href="downloadadmin.aspx" class="btn btn-info">Export Data</a>
                    </div>
                    <hr />
                    <h5 class="card-title">Manage Data Sources</h5>
                    <p class="card-text">Add or delete sensor and grid data sources.</p>
                    <div class="text-right">
                        <a href="datasources.aspx" class="btn btn-info">View Data Sources</a>
                    </div>
                    <hr />
                    <h5 class="card-title">Add Data Source</h5>
                    <p class="card-text">Setup a new data source.</p>
                    <div class="text-right">
                        <a href="datasource.aspx?task=create" class="btn btn-info">Add Data Source</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
