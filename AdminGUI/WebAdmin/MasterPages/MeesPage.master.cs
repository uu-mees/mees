// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using WebAdmin.DataTypes;
using WebAdmin.Utilities;

namespace WebAdmin.MasterPages {
    [ExcludeFromCodeCoverage]
    public partial class MeesPage : MasterPage {
        private List<Alert> _alerts;
        private bool _redirecting;

        #region "Event Handlers"
        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Return to the login page if the user is not logged in
            if (!Page.User.Identity.IsAuthenticated) {
                Response.Redirect("~/login.aspx?sessionexpired=true", false);
                Context.ApplicationInstance.CompleteRequest();
            }

            // Get the users role from the authentication cookie and show the right navbar based on the role
            User.UserRole? sessionRole = GetRoleFromCookie();
            navbar_research.Visible = sessionRole == User.UserRole.Researcher;
            navbar_admin.Visible = !navbar_research.Visible;

            // Do not alter Alerts if a redirect is pending
            if (_redirecting)
                return;

            // Load the alerts from the session and print them
            _alerts = (List<Alert>)Session["alerts"];
            PrintAlerts(true);
        }

        /// <summary>
        ///     Event handler for the Click event of the Logout button
        /// </summary>
        protected void LogOut_Click(object sender, EventArgs e) => LogOut();
        #endregion

        #region "Functionality - Authentication"
        /// <summary>
        ///     Verifies that the users session is still alive
        /// </summary>
        /// <returns></returns>
        public bool VerifyAuthentication() {
            if (Page.User.Identity.IsAuthenticated)
                return true;

            Response.Redirect("~/login.aspx?sessionexpired=true", false);
            Context.ApplicationInstance.CompleteRequest();
            return false;
        }

        /// <summary>
        ///     Gets the logged in users role from the authentication cookie
        /// </summary>
        /// <returns>The UserRole of the logged in user, or null if the session has expired</returns>
        public User.UserRole? GetRoleFromCookie() {
            string cookie = HttpContext.Current.User.Identity.Name;
            // Parse the UserRole
            if (!string.IsNullOrWhiteSpace(cookie))
                return (User.UserRole)int.Parse(cookie.Split('|')[1]);

            // Authentication cookie is not set, redirect to the login page
            LogOut();
            return null;
        }

        /// <summary>
        ///     Abandons the session and redirects the user to the login page
        /// </summary>
        public void LogOut() {
            // End the session and return to the login page
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect("~/login.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
        #endregion

        #region "Functionality - Alerts"
        /// <summary>
        ///     Prints the alert list to the alert container
        /// </summary>
        /// <param name="onPageLoad">Whether the function is called from the Page_Load handler</param>
        private void PrintAlerts(bool onPageLoad = false) {
            // Clear the alert container
            alertContainer.InnerHtml = "";

            // Abort if no alerts have been set
            if (_alerts == null)
                return;

            foreach (Alert alert in _alerts.ToArray()) {
                // Remove expired alerts
                if (alert.Lifetime == Alert.LifeTime.DeleteImmediately || onPageLoad && alert.Lifetime == Alert.LifeTime.DeleteOnPageLoad)
                    _alerts.Remove(alert);

                // Print the alert
                alertContainer.InnerHtml += alert.Print();
            }
        }

        /// <summary>
        ///     Adds an alert to the alert list
        /// </summary>
        /// <param name="alert">The Alert to add</param>
        public void AddAlert(Alert alert) {
            // Initialize the list if it has not been set yet
            if (_alerts == null)
                _alerts = new List<Alert>();

            // Add the new alert to the list and set the list in the session
            _alerts.Add(alert);
            Session["alerts"] = _alerts;
        }
        #endregion

        #region "Functionality - Page Lifecycle"
        /// <summary>
        ///     Redirect the user to another page
        /// </summary>
        /// <param name="page">The page to redirect to</param>
        public void Redirect(string page) {
            _redirecting = true;
            Response.Redirect(page, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        ///     Refreshes the current page, printing any new alerts that have been generated
        /// </summary>
        public void Refresh() => PrintAlerts();
        #endregion
    }
}
