﻿// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using WebAdmin.Utilities;

namespace WebAdmin.MasterPages {
    [ExcludeFromCodeCoverage]
    public partial class MeesPageForm : MasterPage {
        private List<Alert> _alerts;
        protected string Title { get; private set; }

        public void InitializeForm(string title, string buttonText, string linkText = "") {
            // Apply form settings to elements in master page
            Title = title;
            button_send.Text = buttonText;
            button_link.InnerText = linkText;
        }

        protected void Page_Load(object sender, EventArgs e) {
            // Skip to the overview page if the user is already logged in
            if (Page.User.Identity.IsAuthenticated) {
                Response.Redirect("~/overview.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
            }

            // Load the alerts from the session
            _alerts = (List<Alert>)Session["alerts"];

            // Print the alerts
            PrintAlerts(true);
        }

        /// <summary>
        ///     Prints the alert list to the alert container
        /// </summary>
        private void PrintAlerts(bool onPageLoad = false) {
            // Clear the alert container
            alertContainer.InnerHtml = "";

            // Abort if no alerts have been set
            if (_alerts == null)
                return;

            foreach (Alert alert in _alerts.ToArray()) {
                // Remove the alert if it expired
                if (alert.Lifetime == Alert.LifeTime.DeleteImmediately || onPageLoad && alert.Lifetime == Alert.LifeTime.DeleteOnPageLoad)
                    _alerts.Remove(alert);

                // Print the alert
                alertContainer.InnerHtml += alert.Print();
            }
        }

        /// <summary>
        ///     Adds an alert to the alert list
        /// </summary>
        /// <param name="alert">The Alert to add</param>
        public void AddAlert(Alert alert) {
            // Initialize the list if it has not been set yet
            if (_alerts == null)
                _alerts = new List<Alert>();

            // Add the new alert to the list and set the list in the session
            _alerts.Add(alert);
            Session["alerts"] = _alerts;
        }

        /// <summary>
        ///     Redirect the user to another page
        /// </summary>
        /// <param name="page">The page to redirect to</param>
        public void Redirect(string page) {
            Response.Redirect(page, false);
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        ///     Print the alerts without erasing the form data
        /// </summary>
        public void Refresh() => PrintAlerts();

        public void SubscribeToButtonClick(Action<object, EventArgs> onClick) {
            // Convert Action to EventHandler
            void Handler(object obj, EventArgs args) => onClick(obj, args);
            button_send.Click += Handler;
        }

        public void SubscribeToLinkClick(Action<object, EventArgs> onClick) {
            // Convert Action to EventHandler
            void Handler(object obj, EventArgs args) => onClick(obj, args);
            button_link.ServerClick += Handler;
        }
    }
}