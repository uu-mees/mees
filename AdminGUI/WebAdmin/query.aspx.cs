// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class QueryPage : Page {
        private readonly QueryController _queryController;

        protected QueryPage() =>
            _queryController = new QueryController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is authenticated and allowed to access the query page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) Master.Redirect("~/overview.aspx");
        }

        /// <summary>
        ///     Download button click handler - stores the query in the session and redirects the browser to the download page
        /// </summary>
        protected void DownloadQuery_Click(object sender, EventArgs e) {
            Session["downloadquery"] = QueryBox.Text.TrimStart();
            Master.Redirect("~/download.aspx");
        }

        /// <summary>
        ///     Query button click handler - executes the query and prints the result to the screen
        /// </summary>
        protected void ExecuteQuery_Click(object sender, EventArgs e) {
            // Verify that the session is still alive
            if (!Master.VerifyAuthentication())
                return;

            // Initialize return variables
            string message = null;
            DataTable table = null;

            // Execute the query
            try {
                _queryController.ExecuteQuery(QueryBox.Text.TrimStart(), out message, out table);
            }
            catch (InvalidQueryLengthException) {
                // The query is too short to be valid
                Master.AddAlert(new Alert("This query is not valid", AlertType.Error, LifeTime.DeleteImmediately));
                QueryTable.Text = "";
                Master.Refresh();
            }
            catch (InvalidQueryResultException) {
                // The query returned a negative number of rows changed
                Master.AddAlert(new Alert("An error occurred while executing this query", AlertType.Error, LifeTime.DeleteImmediately));
                QueryTable.Text = "";
                Master.Refresh();
            }
            catch (InvalidQueryException ex) {
                // There is some error in the query
                Master.AddAlert(new Alert(ex.Message, AlertType.Error, LifeTime.DeleteImmediately));
                QueryTable.Text = "";
                Master.Refresh();
            }

            // Print the resulting table or message
            if (table != null) {
                string result = DataTablePrinter.Print(table);
                if (result == "EmptyTable") {
                    Master.AddAlert(new Alert("The query returned an empty result set", AlertType.Success, LifeTime.DeleteImmediately));
                    QueryTable.Text = "";
                }
                else {
                    QueryTable.Text = DataTablePrinter.Print(table);
                }

                Master.Refresh();
            }

            if (message != null) {
                Master.AddAlert(new Alert(message, AlertType.Success, LifeTime.DeleteImmediately));
                QueryTable.Text = "";
                Master.Refresh();
            }
        }
    }
}
