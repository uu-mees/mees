// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using WebAdmin.Controllers;

namespace WebAdmin.DataTypes {
    public class User {
        #region "Enums"
        /// <summary>
        ///     All possible values for the User.Role field
        /// </summary>
        public enum UserRole {
            User,
            Administrator,
            Researcher
        }
        #endregion

        #region "Constructors"
        /// <summary>
        ///     Creates a new User object
        /// </summary>
        /// <param name="id">The unique identifier of the User</param>
        /// <param name="username">The username of the User</param>
        /// <param name="email">The email address of the User</param>
        /// <param name="password">The plaintext password of the User</param>
        /// <param name="salt">The salt assigned to the User</param>
        /// <param name="role">The role assigned to the User</param>
        public User(Guid id, string username, string email, string password, Guid salt, UserRole role) {
            Id = id;
            Username = username;
            Email = email;
            Password = AuthenticationController.HashPassword(password, salt);
            Salt = salt;
            Role = role;
        }

        /// <summary>
        ///     Creates a new User object
        /// </summary>
        /// <param name="id">The unique identifier of the User</param>
        /// <param name="username">The username of the User</param>
        /// <param name="email">The email address of the User</param>
        /// <param name="password">The hashed password of the User</param>
        /// <param name="salt">The salt assigned to the User</param>
        /// <param name="role">The role assigned to the User</param>
        public User(Guid id, string username, string email, byte[] password, Guid salt, UserRole role) {
            Id = id;
            Username = username;
            Email = email;
            Password = password;
            Salt = salt;
            Role = role;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Clones a User object
        /// </summary>
        /// <returns>A copy of this User object</returns>
        public User Copy() => new User(Id, Username, Email, Password, Salt, Role);
        #endregion

        #region "Properties"
        /// <summary>
        ///     The unique identifier of the User
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        ///     The username of the User
        /// </summary>
        public string Username { get; }

        /// <summary>
        ///     The email address of the User
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     The hashed password of the User
        /// </summary>
        public byte[] Password { get; set; }

        /// <summary>
        ///     The salt assigned to the User
        /// </summary>
        public Guid Salt { get; set; }

        /// <summary>
        ///     The role assigned to the User
        /// </summary>
        public UserRole Role { get; }
        #endregion

        #region "Operators"
        /// <summary>
        ///     Tests whether two Users are identical
        /// </summary>
        /// <param name="a">The first User to compare</param>
        /// <param name="b">The second User to compare</param>
        /// <returns>Whether the Users are identical</returns>
        public static bool operator ==(User a, User b) => Equals(a, b);

        /// <summary>
        ///     Tests whether two Users are different
        /// </summary>
        /// <param name="a">The first User to compare</param>
        /// <param name="b">The second User to compare</param>
        /// <returns>Whether the Users are different</returns>
        public static bool operator !=(User a, User b) => !Equals(a, b);

        /// <summary>
        ///     Tests if an object is a User and is identical to this User
        /// </summary>
        /// <param name="obj">The object to compare to this User</param>
        /// <returns>Whether the Users are identical</returns>
        public override bool Equals(object obj) {
            // Cast the object to a User
            User other = obj as User;
            // Check if it is identical to this User
            return other != null &&
                   Id == other.Id &&
                   Username == other.Username &&
                   Password == other.Password &&
                   Email == other.Email &&
                   Salt == other.Salt &&
                   Role == other.Role;
        }

        public override int GetHashCode() => Id.GetHashCode();
        #endregion
    }
}
