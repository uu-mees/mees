// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;

namespace WebAdmin.DataTypes {
    public class PermissionList {
        #region "Functionality"
        /// <summary>
        ///     Clones a PermissionList
        /// </summary>
        /// <returns>A copy of this PermissionList</returns>
        public PermissionList Clone() => new PermissionList(ShowRatings, ShowSensors, ShowRaster, ShowExposure, ShowPersonalScores, ShowRoutePlanner);
        #endregion

        #region "Constructors"
        /// <summary>
        ///     Creates a new PermissionList
        /// </summary>
        /// <param name="showRatings">The permission to see ratings</param>
        /// <param name="showSensors">The permission to see the Sensor data</param>
        /// <param name="showRaster">The permission to see the Raster data</param>
        /// <param name="showExposure">The permission to see the Exposure History</param>
        /// <param name="showPersonalScores">The permission to see minigame scores</param>
        /// <param name="showRoutePlanner">The permission to see the Route Planner</param>
        public PermissionList(bool showRatings, bool showSensors, bool showRaster, bool showExposure, bool showPersonalScores, bool showRoutePlanner) {
            ShowRatings = showRatings;
            ShowSensors = showSensors;
            ShowRaster = showRaster;
            ShowExposure = showExposure;
            ShowPersonalScores = showPersonalScores;
            ShowHighScores = false;
            ShowRoutePlanner = showRoutePlanner;
        }

        /// <summary>
        ///     Creates a new PermissionList from a boolean array of length 6
        /// </summary>
        /// <param name="permissions">
        ///     The booleans representing whether or not participants can see User Ratings, Sensor Data,
        ///     Raster Data, Their Daily Exposure History, Minigame Scores and the Route Planner in that order
        /// </param>
        public PermissionList(bool[] permissions) {
            if (permissions.Length < 6)
                throw new ArgumentException("Permission array is too short");

            ShowRatings = permissions[0];
            ShowSensors = permissions[1];
            ShowRaster = permissions[2];
            ShowExposure = permissions[3];
            ShowPersonalScores = permissions[4];
            ShowHighScores = false;
            ShowRoutePlanner = permissions[5];
        }
        #endregion

        #region "Properties"
        /// <summary>
        ///     The permission to see the User Ratings while in Research
        /// </summary>
        public bool ShowRatings { get; }

        /// <summary>
        ///     The permission to see Sensor Data while in Research
        /// </summary>
        public bool ShowSensors { get; }

        /// <summary>
        ///     The permission to see the Raster Data while in Research
        /// </summary>
        public bool ShowRaster { get; }

        /// <summary>
        ///     The permission to see your Exposure History while in Research
        /// </summary>
        public bool ShowExposure { get; }

        /// <summary>
        ///     The permission to see the Score you got for any minigame while in Research
        /// </summary>
        public bool ShowPersonalScores { get; }

        /// <summary>
        ///     The permission to see minigame High Scores while in Research
        /// </summary>
        public bool ShowHighScores { get; }

        /// <summary>
        ///     The permission to use the Route Planner while in Research
        /// </summary>
        public bool ShowRoutePlanner { get; }
        #endregion

        #region "Operators"
        /// <summary>
        ///     Tests whether two PermissionLists are identical
        /// </summary>
        /// <param name="a">The first PermissionList to compare</param>
        /// <param name="b">The second PermissionList to compare</param>
        /// <returns>Whether the PermissionLists are identical</returns>
        public static bool operator ==(PermissionList a, PermissionList b) => Equals(a, b);

        /// <summary>
        ///     Tests whether two PermissionLists are different
        /// </summary>
        /// <param name="a">The first PermissionList to compare</param>
        /// <param name="b">The second PermissionList to compare</param>
        /// <returns>Whether the PermissionLists are different</returns>
        public static bool operator !=(PermissionList a, PermissionList b) => !Equals(a, b);

        /// <summary>
        ///     Tests if an object is a PermissionList and is identical to this PermissionList
        /// </summary>
        /// <param name="obj">The object to compare to this PermissionList</param>
        /// <returns>Whether the PermissionsLists are identical</returns>
        public override bool Equals(object obj) {
            // Cast object to PermissionList
            PermissionList other = (PermissionList)obj;
            // Check if it is identical to this PermissionList
            return other != null &&
                   other.ShowExposure == ShowExposure &&
                   other.ShowHighScores == ShowHighScores &&
                   other.ShowPersonalScores == ShowPersonalScores &&
                   other.ShowRaster == ShowRaster &&
                   other.ShowRatings == ShowRatings &&
                   other.ShowRoutePlanner == ShowRoutePlanner &&
                   other.ShowSensors == ShowSensors;
        }

        /// <summary>
        ///     Returns the hash code for this instance
        /// </summary>
        public override int GetHashCode() {
            unchecked {
                int hashCode = ShowRatings.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowSensors.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowRaster.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowExposure.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowPersonalScores.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowHighScores.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowRoutePlanner.GetHashCode();
                return hashCode;
            }
        }
        #endregion
    }
}
