// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;

namespace WebAdmin.DataTypes {
    public class DataSource {
        #region "Enums"
        /// <summary>
        ///     All possible values for the DataSource.Type field
        /// </summary>
        public enum DataSourceType {
            Raster = 0,
            Sensor = 1
        }
        #endregion

        #region "Constructors"
        /// <summary>
        ///     Creates a new DataSource object
        /// </summary>
        /// <param name="id">The unique identifier of the DataSource</param>
        /// <param name="url">The URL of the DataSource</param>
        /// <param name="active">Whether the uploading of the DataSource is active or paused</param>
        /// <param name="type">The type of DataSource, either Sensor or Raster</param>
        public DataSource(Guid id, string url, bool active, DataSourceType type) {
            Id = id;
            Url = url;
            Active = active;
            Type = type;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Clones a DataSource object
        /// </summary>
        /// <returns>A copy of this DataSource object</returns>
        public DataSource Copy() => new DataSource(Id, Url, Active, Type);
        #endregion

        #region "Properties"
        /// <summary>
        ///     The unique identifier of the DataSource
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        ///     The URL of the DataSource
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        ///     The type of the DataSource, either Sensor or Raster
        /// </summary>
        public DataSourceType Type { get; set; }

        /// <summary>
        ///     Whether the uploading of the DataSource is active or paused
        /// </summary>
        public bool Active { get; set; }
        #endregion

        #region "Operators"
        /// <summary>
        ///     Tests whether two DataSources are identical
        /// </summary>
        /// <param name="a">The first DataSource to compare</param>
        /// <param name="b">The second DataSource to compare</param>
        /// <returns>Whether the DataSources are identical</returns>
        public static bool operator ==(DataSource a, DataSource b) => Equals(a, b);

        /// <summary>
        ///     Tests whether two DataSources are different
        /// </summary>
        /// <param name="a">The first DataSource to compare</param>
        /// <param name="b">The second DataSource to compare</param>
        /// <returns>Whether the DataSources are different</returns>
        public static bool operator !=(DataSource a, DataSource b) => !Equals(a, b);

        /// <summary>
        ///     Tests if an object is a DataSource and is identical to this DataSource
        /// </summary>
        /// <param name="obj">The object to compare to this DataSource</param>
        /// <returns>Whether the DataSources are identical</returns>
        public override bool Equals(object obj) {
            // Cast the object to a DataSource
            DataSource other = obj as DataSource;
            // Check if it is identical to this DataSource
            return other != null &&
                   Id == other.Id &&
                   Active == other.Active &&
                   Type == other.Type &&
                   Url == other.Url;
        }

        /// <summary>
        ///     Returns the hash code for this instance
        /// </summary>
        public override int GetHashCode() => Id.GetHashCode();
        #endregion
    }
}
