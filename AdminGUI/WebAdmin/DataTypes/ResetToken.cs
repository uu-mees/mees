// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;

namespace WebAdmin.DataTypes {
    public class ResetToken {
        #region "Constructors"
        /// <summary>
        ///     Creates a new ResetToken object
        /// </summary>
        /// <param name="token">The value of this ResetToken</param>
        /// <param name="userid">The ID of the User this ResetToken is assigned to</param>
        /// <param name="expiration">The expiration date/time of the ResetToken</param>
        public ResetToken(string token, Guid userid, DateTime expiration) {
            Token = token;
            UserId = userid;
            Expiration = expiration;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Clones a ResetToken object
        /// </summary>
        /// <returns>A copy of this ResetToken object</returns>
        public ResetToken Copy() => new ResetToken(Token, UserId, Expiration);
        #endregion

        #region "Properties"
        /// <summary>
        ///     The value of this ResetToken
        /// </summary>
        public string Token { get; }

        /// <summary>
        ///     The ID of the User this ResetToken is assigned to
        /// </summary>
        public Guid UserId { get; }

        /// <summary>
        ///     The expiration date/time of the ResetToken
        /// </summary>
        public DateTime Expiration { get; }
        #endregion

        #region "Operators"
        /// <summary>
        ///     Tests whether two ResetTokens are identical
        /// </summary>
        /// <param name="a">The first ResetTokens to compare</param>
        /// <param name="b">The second ResetTokens to compare</param>
        /// <returns>Whether the ResetTokens are identical</returns>
        public static bool operator ==(ResetToken a, ResetToken b) => Equals(a, b);

        /// <summary>
        ///     Tests whether two ResetTokens are different
        /// </summary>
        /// <param name="a">The first ResetTokens to compare</param>
        /// <param name="b">The second ResetTokens to compare</param>
        /// <returns>Whether the ResetTokens are different</returns>
        public static bool operator !=(ResetToken a, ResetToken b) => !Equals(a, b);

        /// <summary>
        ///     Tests if an object is a ResetToken and is identical to this Research
        /// </summary>
        /// <param name="obj">The object to compare to this ResetToken</param>
        /// <returns>Whether the ResetTokens are identical</returns>
        public override bool Equals(object obj) {
            ResetToken other = obj as ResetToken;

            return other != null &&
                   Token == other.Token &&
                   UserId == other.UserId &&
                   Expiration.Ticks - other.Expiration.Ticks < 10000000; // Maximum deviation: 1 second
        }

        /// <summary>
        ///     Returns the hash code for this instance
        /// </summary>
        public override int GetHashCode() => UserId.GetHashCode();
        #endregion
    }
}
