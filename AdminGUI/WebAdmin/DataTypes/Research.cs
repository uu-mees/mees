// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;

namespace WebAdmin.DataTypes {
    public class Research {
        #region "Constructors"
        /// <summary>
        ///     Creates a new Research object
        /// </summary>
        /// <param name="id">The unique identifier of the Research</param>
        /// <param name="name">The name of the Research</param>
        /// <param name="description">The description of the Research</param>
        /// <param name="permission">The permissions Research Participants have</param>
        /// <param name="startDate">The start date/time of the Research</param>
        /// <param name="endDate">The end date/time of the Research</param>
        public Research(Guid id, string name, string description, PermissionList permission, DateTime startDate,
                        DateTime endDate) {
            Id = id;
            Name = name;
            Description = description;
            StartDate = startDate;
            EndDate = endDate;
            Permission = permission;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Clones a Research object
        /// </summary>
        /// <returns>A copy of this Research object</returns>
        public Research Clone() => new Research(Id, Name, Description, Permission, StartDate, EndDate);
        #endregion

        #region "Properties"
        /// <summary>
        ///     The unique identifier of the Research
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        ///     The name of the Research
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     The description of the Research
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     The start date/time of the Research
        /// </summary>
        public DateTime StartDate { get; }

        /// <summary>
        ///     The end date/time of the Research
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        ///     Whether users assigned to this Research can use certain functionality of the app.
        /// </summary>
        public PermissionList Permission { get; set; }
        #endregion

        #region "Operators"
        /// <summary>
        ///     Tests whether two Researches are identical
        /// </summary>
        /// <param name="a">The first Researches to compare</param>
        /// <param name="b">The second Researches to compare</param>
        /// <returns>Whether the Researches are identical</returns>
        public static bool operator ==(Research a, Research b) => Equals(a, b);

        /// <summary>
        ///     Tests whether two Researches are different
        /// </summary>
        /// <param name="a">The first Researches to compare</param>
        /// <param name="b">The second Researches to compare</param>
        /// <returns>Whether the Researches are different</returns>
        public static bool operator !=(Research a, Research b) => !Equals(a, b);

        /// <summary>
        ///     Tests if an object is a Research and is identical to this Research
        /// </summary>
        /// <param name="obj">The object to compare to this Research</param>
        /// <returns>Whether the Researches are identical</returns>
        public override bool Equals(object obj) {
            // Cast the object to a Research
            Research other = obj as Research;
            // Check if it is identical to this Research
            return other != null &&
                   Id == other.Id &&
                   Name == other.Name &&
                   Description == other.Description &&
                   StartDate == other.StartDate &&
                   EndDate == other.EndDate &&
                   Permission == other.Permission;
        }

        /// <summary>
        ///     Returns the hash code for this instance
        /// </summary>
        public override int GetHashCode() => Id.GetHashCode();
        #endregion
    }
}
