﻿<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="researches.aspx.cs" Inherits="WebAdmin.ResearchesPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Researches" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page">
        <div class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Manage Researches</h3>
            </div>
            <div class="mees-title-button">
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="ClearFilters" ID="button_clear_filter" class="btn btn-link">Clear Search Filters</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="CreateResearch" ID="button_create" class="btn btn-success">Create new Research</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="EditResearch" ID="button_edit" class="btn btn-info disabled">Edit Research</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="AssignResearch" ID="button_assigner" class="btn btn-info disabled">Assign to Research</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="EndResearch" ID="button_end" class="btn btn-warning disabled">End Research</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="DeleteResearch" ID="button_delete" class="btn btn-danger disabled">Delete Research</asp:LinkButton>
            </div>
        </div>
        <div style="overflow: auto">
            <table id="researches" class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                <tr>
                    <th scope="col" style="text-align: center; vertical-align: middle; width: 30px;">
                        <input type="checkbox" id="selectall"/>
                    </th>
                    <th scope="col" style="min-width: 200px;">
                        Name
                        <asp:Panel runat="server" DefaultButton="search_name">
                            <div class="input-group">
                                <asp:TextBox ID="filter_name" MaxLength="50" CssClass="form-control input mees-table-input mees-override-table-input" placeholder="Search Names" runat="server"></asp:TextBox>
                                <span class="input-group-button mees-button-container">
                                    <asp:Button Text="Search" ID="search_name" runat="server" OnClick="Search_Click" CssClass="btn btn-info mees-btn-table"/>
                                </span>
                            </div>
                        </asp:Panel>
                    </th>
                    <th scope="col" style="min-width: 200px;">
                        Description
                        <asp:Panel runat="server" DefaultButton="search_description">
                            <div class="input-group">
                                <asp:TextBox ID="filter_description" MaxLength="50" CssClass="form-control input mees-table-input mees-override-table-input" placeholder="Search Descriptions" runat="server"></asp:TextBox>
                                <span class="mees-button-container">
                                    <asp:Button Text="Search" ID="search_description" runat="server" OnClick="Search_Click" CssClass="btn btn-info mees-btn-table"/>
                                </span>
                            </div>
                        </asp:Panel>
                    </th>
                    <th scope="col" style="min-width: 200px;">Start Date</th>
                    <th scope="col" style="min-width: 200px;">End Date</th>
                    <th scope="col" style="min-width: 200px;">Status</th>
                    <th scope="col" style="min-width: 200px;"># Participants (Active/Total)</th>
                </tr>
                </thead>
                <tbody>
                <!-- Print user rows here -->
                <asp:Literal runat="server" ID="ResearchRows"/>
                </tbody>
            </table>
        </div>
        <div class="mees-subpage-nav">
            <asp:Literal runat="server" ID="navigation"/>
        </div>
        <script type="application/javascript">
            FixTableAlignment();
            ClearCheckboxes();
        </script>
    </div>
</asp:Content>