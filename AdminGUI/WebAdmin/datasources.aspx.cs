// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.DataSource;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class DataSourcesPage : Page {
        private readonly DataSourceController _dataSourceController;

        protected DataSourcesPage() => _dataSourceController =
                                           new DataSourceController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Check that the logged in user has permission to view this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Read all sources from the database and print them as an HTML table
            try {
                List<DataSource> dataSources = _dataSourceController.GetDataSources();
                foreach (DataSource dataSource in dataSources)
                    DataSourceRows.Text += PrintRow(dataSource);
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/overview.aspx");
            }
        }

        /// <summary>
        ///     Parses a DataSource object to an HTML table row
        /// </summary>
        /// <param name="dataSource">The DataSource object to parse</param>
        /// <returns>String containing the generated HTML</returns>
        private static string PrintRow(DataSource dataSource) {
            StringBuilder html = new StringBuilder();

            // Print the DataSource fields as HTML
            html.AppendLine("<tr>");
            html.AppendLine("<td style=\"text-align: center; vertical-align: middle;\">");
            html.AppendLine("<input type=\"checkbox\" name=\"" + dataSource.Id + "\" />");
            html.AppendLine("</td>");
            html.AppendLine("<td>" + dataSource.Url + "</td>");
            html.AppendLine("<td>" + (dataSource.Active ? "Yes" : "No") + "</td>");
            html.AppendLine("<td>" + (dataSource.Type == DataSourceType.Raster ? "Raster" : "Sensor") + "</td>");
            html.AppendLine("</tr>");
            return html.ToString();
        }

        /// <summary>
        ///     Stores all selected Ids in the session
        /// </summary>
        private void StoreIdsInSession() {
            // Read DataSource Ids from the form
            List<string> sourcelist = Request.Form.AllKeys.Where(s => Guid.TryParse(s, out _)).ToList();
            Session["sourcelist"] = string.Join(";", sourcelist);
        }

        #region "Button Click Handlers"
        /// <summary>
        ///     Button click handler for the Delete DataSources button. Stores the selected Ids in the session and redirects to the
        ///     datasource page.
        /// </summary>
        protected void DeleteSource(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("datasource.aspx?task=delete");
        }

        /// <summary>
        ///     Button click handler for the Disable DataSources button. Stores the selected Ids in the session and redirects to
        ///     the datasource page.
        /// </summary>
        protected void DisableSource(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("datasource.aspx?task=disable");
        }

        /// <summary>
        ///     Button click handler for the Edit DataSource button. Stores the selected Ids in the session and redirects to the
        ///     datasource page.
        /// </summary>
        protected void EditSource(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("datasource.aspx?task=edit");
        }

        /// <summary>
        ///     Button click handler for the Enable DataSources button. Stores the selected Ids in the session and redirects to the
        ///     datasource page.
        /// </summary>
        protected void EnableSource(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("datasource.aspx?task=enable");
        }

        /// <summary>
        ///     Button click handler for the Add DataSource button. Stores the selected Ids in the session and redirects to the
        ///     datasource page.
        /// </summary>
        protected void CreateSource(object sender, EventArgs e) =>
            Master.Redirect("datasource.aspx?task=create");
        #endregion
    }
}
