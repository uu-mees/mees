<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="WebAdmin.UsersPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="Users" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page">
        <div id="normal_view" runat="server" visible="false" class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Manage Users</h3>
            </div>
            <div class="mees-title-button">
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="ClearFilters" ID="button_clear_filter_1" CssClass="btn btn-link">Clear Search Filters</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="CreateUser" ID="button_create" CssClass="btn btn-success">Create User</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="EditUser" ID="button_edit" CssClass="btn btn-info disabled">Edit User</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="WipeUser" ID="button_wipe" CssClass="btn btn-warning disabled">Wipe User Data</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="DeleteUser" ID="button_delete" CssClass="btn btn-danger disabled">Delete User</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="NotifyUser" ID="button_notify" CssClass="btn btn-info disabled">Send Notification</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="UnassignUser" ID="button_unassign" CssClass="btn btn-info disabled">Unassign User</asp:LinkButton>
            </div>
        </div>
        <div id="assign" runat="server" visible="false" class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Assign Users</h3>
            </div>
            <div class="mees-title-button">
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="ClearFilters" ID="button_clear_filter_2" class="btn btn-link">Clear Search Filters</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="AssignUser" ID="button_assign" class="btn btn-info disabled">Assign Users</asp:LinkButton>
            </div>
        </div>
        <div style="overflow: auto">
            <table id="users" class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th scope="col" style="text-align: center; vertical-align: middle; width: 30px;">
                            <input type="checkbox" id="selectall" />
                        </th>
                        <th scope="col" style="min-width: 200px;">Username
                        <asp:Panel runat="server" DefaultButton="search_name">
                            <div class="input-group">
                                <asp:TextBox ID="filter_name" CssClass="form-control input mees-table-input mees-override-table-input" placeholder="Search Usernames" MaxLength="50" runat="server"></asp:TextBox>
                                <span class="input-group-button mees-button-container">
                                    <asp:Button Text="Search" ID="search_name" runat="server" OnClick="Search_Click" CssClass="btn btn-info mees-btn-table" />
                                </span>
                            </div>
                        </asp:Panel>
                        </th>
                        <th scope="col" style="min-width: 200px;">Email Address
                        <asp:Panel runat="server" DefaultButton="search_mail">
                            <div class="input-group">
                                <asp:TextBox ID="filter_mail" CssClass="form-control input mees-table-input mees-override-table-input" placeholder="Search Email Addresses" MaxLength="50" runat="server"></asp:TextBox>
                                <span class="mees-button-container">
                                    <asp:Button Text="Search" ID="search_mail" runat="server" OnClick="Search_Click" CssClass="btn btn-info mees-btn-table" />
                                </span>
                            </div>
                        </asp:Panel>
                        </th>
                        <th scope="col" style="min-width: 200px;">Role
                        <asp:Panel runat="server" DefaultButton="search_role">
                            <div class="input-group">
                                <asp:DropDownList ID="filter_role" runat="server" CssClass="custom-select mees-override-table-input">
                                    <asp:ListItem Enabled="true" Text="Select role..." Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="User" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Administrator" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Researcher" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <span class="input-group-button mees-button-container">
                                    <asp:Button Text="Search" ID="search_role" runat="server" OnClick="Search_Click" CssClass="btn btn-info mees-btn-table" />
                                </span>
                            </div>
                        </asp:Panel>
                        </th>
                        <th scope="col" style="min-width: 200px;">In Research                     
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Print user rows here -->
                    <asp:Literal runat="server" ID="UserRows" />
                </tbody>
            </table>
        </div>
        <div class="mees-subpage-nav">
            <asp:Literal runat="server" ID="navigation" />
        </div>
        <script type="application/javascript">
            FixTableAlignment();
            ClearCheckboxes();
        </script>
    </div>
</asp:Content>
