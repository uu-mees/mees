<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPageForm.master" AutoEventWireup="True" CodeBehind="forgotpassword.aspx.cs" Inherits="WebAdmin.ForgotPasswordPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPageForm.master" %>
<asp:Content ID="LoginForm" ContentPlaceHolderID="fields" runat="Server">
    <div class="form-group">
        <asp:TextBox ID="user" MaxLength="50" runat="server" CssClass="form-control input-lg" placeholder="Username" required="True" TabIndex="1"></asp:TextBox>
    </div>
    <div id="mail" runat="server">
    </div>
</asp:Content>
