// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Web.Security;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class LoginPage : Page {
        private readonly AuthenticationController _authenticationController;

        protected LoginPage() => _authenticationController = new AuthenticationController(
                                                                                          new SqlServerDatabase(ConfigurationManager
                                                                                                                .ConnectionStrings["database"]
                                                                                                                .ConnectionString),
                                                                                          new SmtpEmailProvider()
                                                                                         );

        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Set the Master page data
            Master.InitializeForm(
                                  "MEES Administrator - Login",
                                  "Login",
                                  "Forgot Password"
                                 );
            // Subscribe to the button press events
            Master.SubscribeToButtonClick(Login_Click);
            Master.SubscribeToLinkClick(ForgotPassword_Click);

            // Check if the session has expired
            if (!string.IsNullOrEmpty(Request.Params["sessionexpired"])) {
                Master.AddAlert(new Alert("Your session has expired, please log in", AlertType.Warning));
                Master.Refresh();
            }
        }

        /// <summary>
        ///     Redirect the user to the Forgot Password page
        /// </summary>
        private void ForgotPassword_Click(object sender, EventArgs e) {
            Response.Redirect("~/forgotpassword.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        ///     Starts the login procedure on clicking the button
        /// </summary>
        private void Login_Click(object sender, EventArgs e) {
            try {
                // Try logging in with the given credentials - if successful generate the authentication cookie and redirect to the overview page
                _authenticationController.Login(username.Text, password.Text, out User user);
                FormsAuthentication.RedirectFromLoginPage(user.Id + "|" + (int)user.Role, false);
            }
            catch (InvalidCredentialsException) {
                Master.AddAlert(new Alert("Invalid username or password", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidRoleException) {
                Master.AddAlert(new Alert("This account is not authorized as administrator", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch {
                Response.End();
            }
        }
    }
}
