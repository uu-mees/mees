<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPageForm.master" AutoEventWireup="true" CodeBehind="resetpassword.aspx.cs" Inherits="WebAdmin.ResetPasswordPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPageForm.master" %>
<asp:Content ID="LoginForm" ContentPlaceHolderID="fields" runat="Server">
    <div class="form-group">
        <asp:TextBox ID="token" MaxLength="100" runat="server" CssClass="form-control input-lg" placeholder="Token" required="True" ReadOnly="true"></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:TextBox ID="password1" MaxLength="50" runat="server" TextMode="Password" CssClass="form-control input-lg" placeholder="New Password" required="True" TabIndex="1"></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:TextBox ID="password2" MaxLength="50" runat="server" TextMode="Password" CssClass="form-control input-lg" placeholder="Repeat Password" required="True" TabIndex="2"></asp:TextBox>
    </div>
</asp:Content>
