// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class ForgotPasswordPage : Page {
        private readonly AuthenticationController _authenticationController;

        protected ForgotPasswordPage() => _authenticationController = new AuthenticationController(
                                                                                               new SqlServerDatabase(ConfigurationManager
                                                                                                                     .ConnectionStrings["database"]
                                                                                                                     .ConnectionString),
                                                                                               new SmtpEmailProvider()
                                                                                              );

        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Set up the form
            Master.InitializeForm(
                                  "MEES Administrator - Forgot Password",
                                  "Reset Password",
                                  "Return to Login"
                                 );
            // Subscribe to the button press events
            Master.SubscribeToButtonClick(Forgot_Click);
            Master.SubscribeToLinkClick(ReturnToLoginPage_Click);
        }

        /// <summary>
        ///     Redirects the user to the Login page
        /// </summary>
        private void ReturnToLoginPage_Click(object sender, EventArgs e) {
            Response.Redirect("~/login.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        ///     Start the reset process for the username entered in the form
        /// </summary>
        private void Forgot_Click(object sender, EventArgs e) {
            try {
                _authenticationController.RequestPasswordReset(user.Text);
                Master.AddAlert(new Alert("An email with instructions on how to reset your password has been sent", AlertType.Success));
                Master.Redirect("~/login.aspx");
            }
            catch (UserDoesNotExistException) {
                Master.AddAlert(new Alert("No account with this username exists", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidRoleException) {
                Master.AddAlert(new Alert("The password of this account cannot be reset in this manner", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (DuplicateTokenException) {
                Master.AddAlert(new Alert("A password reset email has already been sent", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.Message);
                Response.End();
            }
        }
    }
}
