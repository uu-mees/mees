// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class ResearchesPage : Page {
        private const int RowsPerPage = 20;
        private readonly ResearchController _researchController;

        protected ResearchesPage() => _researchController =
                                          new ResearchController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is logged in and is allowed to access this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Stop if the page has already been generated
            if (IsPostBack)
                return;

            try {
                // Fill the search textboxes with the values set in the url
                string searchName = Request.Params["search_name"];
                filter_name.Text = searchName;
                string searchDescription = Request.Params["search_description"];
                filter_description.Text = searchDescription;
                if (!int.TryParse(Request.Params["page"], out int page))
                    page = 0;

                // Get all Researches matching the filters set and print them
                List<Research> researches = _researchController.GetResearches(RowsPerPage, page, searchName, searchDescription);
                foreach (Research research in researches)
                    ResearchRows.Text += PrintRow(research,
                        _researchController.CountUsersInResearch(research.Id),
                        _researchController.CountActiveUsersInResearch(research.Id));

                // Print the navigation bar
                int researchCount = _researchController.GetResearchCount(searchName, searchDescription);
                int pageCount = (researchCount - 1) / RowsPerPage + 1;
                StringBuilder nav = new StringBuilder();
                for (int i = 1; i <= pageCount; i++)
                    if (i - 1 == page) {
                        nav.Append("<a href=\"#\" class=\"btn btn-link disabled\">" + i + "</a> ");
                    }
                    else {
                        string url = "researches.aspx?page=" + (i - 1) + "&search_name=" + searchName + "&search_description=" + searchDescription;
                        nav.Append("<a href=\"" + url + "\" class=\"btn btn-link\">" + i + "</a> ");
                    }

                navigation.Text = nav.ToString();
            }
            catch (Exception ex) {
                // An unexpected error occurred
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/overview.aspx");
            }
        }

        /// <summary>
        ///     Parses a Research object to an HTML table row
        /// </summary>
        /// <param name="research">The Research object to parse</param>
        /// <param name="users">The number of users assigned to this Research</param>
        /// <param name="activeUsers">The number of active users in this Research</param>
        /// <returns>String containing the generated HTML</returns>
        private static string PrintRow(Research research, int users, int activeUsers) {
            StringBuilder html = new StringBuilder();

            // Set the status based on the start and end dates
            string status;
            if (research.StartDate > DateTime.UtcNow)
                status = "Not yet started";
            else if (research.EndDate > DateTime.UtcNow)
                status = "Ongoing";
            else
                status = "Ended";

            // Print the fields
            html.AppendLine("<tr>");
            html.AppendLine("<td style=\"text-align: center; vertical-align: middle;\">");
            html.AppendLine("<input type=\"checkbox\" name=\"" + research.Id + "\" />");
            html.AppendLine("</td>");
            html.AppendLine("<td>" + research.Name + "</td>");
            html.AppendLine("<td>" + research.Description + "</td>");
            html.AppendLine("<td>" + research.StartDate + "</td>");
            html.AppendLine("<td>" + research.EndDate + "</td>");
            html.AppendLine("<td>" + status + "</td>");
            html.AppendLine("<td>" + activeUsers + "/" + users + "</td>");
            html.AppendLine("</tr>");
            return html.ToString();
        }

        /// <summary>
        ///     Stores all selected Ids in the session
        /// </summary>
        private void StoreIdsInSession() {
            // Read Research Ids from the form
            List<string> researchlist = Request.Form.AllKeys.Where(s => Guid.TryParse(s, out _)).ToList();
            Session["researchlist"] = string.Join(";", researchlist);
        }

        #region "Button Click Handlers"
        /// <summary>
        ///     Click handler for the Clear Filter button
        /// </summary>
        protected void ClearFilters(object sender, EventArgs e) => Master.Redirect("researches.aspx");

        /// <summary>
        ///     Click handler for the Add Research button
        /// </summary>
        protected void CreateResearch(object sender, EventArgs e) => Master.Redirect("research.aspx?task=create");

        /// <summary>
        ///     Click handler for the Edit Research button - stores the selected Id to the session and redirects
        /// </summary>
        protected void EditResearch(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("research.aspx?task=edit");
        }

        /// <summary>
        ///     Click handler for the End Researches button - stores the selected Ids to the session and redirects
        /// </summary>
        protected void EndResearch(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("research.aspx?task=end");
        }

        /// <summary>
        ///     Click handler for the Delete Researches button - stores the selected Ids to the session and redirects
        /// </summary>
        protected void DeleteResearch(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("research.aspx?task=delete");
        }

        /// <summary>
        ///     Click handler for the Assign Research button
        /// </summary>
        protected void AssignResearch(object sender, EventArgs e) {
            // Get the selected Id from the form
            Guid endTest = Guid.Empty;
            string research = Request.Form.AllKeys.FirstOrDefault(s => Guid.TryParse(s, out endTest));

            // Check that the Research has not yet ended
            if (_researchController.ResearchEnded(endTest)) {
                Master.AddAlert(new Alert("Unable to assign users to a concluded research", AlertType.Error));
                Master.Refresh();
            }
            else {
                // Redirect to the assign page
                Master.Redirect("users.aspx?task=assign&id=" + research);
            }
        }

        /// <summary>
        ///     Click handler for the Search buttons
        /// </summary>
        protected void Search_Click(object sender, EventArgs e) {
            // Collect search strings
            string name = filter_name.Text;
            string description = filter_description.Text;
            // Set up url and redirect to it
            string url = "~/researches.aspx?search_name=" + name + "&search_description=" + description;
            Master.Redirect(url);
        }
        #endregion
    }
}
