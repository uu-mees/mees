<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPageForm.master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WebAdmin.LoginPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPageForm.master" %>
<asp:Content ID="LoginForm" ContentPlaceHolderID="fields" runat="Server">
    <div class="form-group">
        <asp:TextBox ID="username" MaxLength="50" runat="server" CssClass="form-control input-lg" placeholder="Username" required="True" TabIndex="1"></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:TextBox ID="password" MaxLength="50" runat="server" TextMode="Password" CssClass="form-control input-lg" placeholder="Password" required="True" TabIndex="2"></asp:TextBox>
    </div>
</asp:Content>
