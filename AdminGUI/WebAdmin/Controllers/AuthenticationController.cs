// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;

namespace WebAdmin.Controllers {
    public class AuthenticationController {
        private readonly IDatabase _database;
        private readonly IEmailProvider _emailProvider;

        #region "Constructors"
        public AuthenticationController(IDatabase database, IEmailProvider emailProvider) {
            _database = database;
            _emailProvider = emailProvider;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Attempt to login using the parameters passed
        /// </summary>
        /// <param name="username">Username to attempt login with</param>
        /// <param name="password">Password to attempt login with</param>
        /// <param name="user">The User object matching the passed username; null if no User with this username exists</param>
        public void Login(string username, string password, out User user) {
            // Find the User in the database by the username
            user = _database.GetUserByUsername(username);

            if (user == null)
                // No User with this username exists
                throw new InvalidCredentialsException();

            if (Encoding.UTF8.GetString(user.Password) != Encoding.UTF8.GetString(HashPassword(password, user.Salt)))
                // The passwords do not match
                throw new InvalidCredentialsException();

            if (user.Role == UserRole.User)
                // The User attempting to login is not a Researcher or Administrator
                throw new InvalidRoleException();

            // Successful login
        }

        /// <summary>
        ///     Request a token for the password reset process
        /// </summary>
        /// <param name="username">The username for which to generate the token</param>
        /// <param name="hours">The amount of hours until the token expires</param>
        /// <returns>Status code indicating the result of the token request</returns>
        public void RequestPasswordReset(string username, double hours = 2) {
            // Find the User in the database by the username
            User user = _database.GetUserByUsername(username);

            if (user == null)
                // No User with this username exists
                throw new UserDoesNotExistException();

            if (user.Role == UserRole.User)
                // The User attempting to reset the password is not a Researcher or Administrator
                throw new InvalidRoleException();

            // Check if there is already a ResetToken associated with this User
            ResetToken existingToken = _database.GetResetTokenByUserId(user.Id);
            if (existingToken != null)
                // Check if the existing ResetToken has expired
                if (existingToken.Expiration < DateTime.UtcNow)
                    // Remove the expired ResetToken
                    _database.DeleteResetToken(user.Id);
                else
                    // The existing ResetToken is still valid, cancel the process
                    throw new DuplicateTokenException();


            // Generate a new ResetToken and insert it into the database
            ResetToken token = new ResetToken(Guid.NewGuid().ToString(), user.Id, DateTime.UtcNow.AddHours(hours));
            _database.CreateResetToken(token);

            // Generate the password reset email
            string url = "https://mees.uu.nl/admin/resetpassword.aspx?token=" + token.Token;
            string body = @"<html><body><p>Dear user,<br/> We received a request to change your password.<br/>
                If this request was not performed by you, you may ignore this email.<br/><br/>
                Please click the following link to reset your password:<br/>
                <a href=""" + url + @""">Reset password</a><br/>
                This link is valid for the next 2 hours.<br/><br/>
                With kind regards,<br/><br/>
                Institute for Risk Assessment Sciences (IRAS)<br/>
                <b>Utrecht University</b>
                </p></body></html>";

            // Send the email
            _emailProvider.SendMail(user.Email, "MEES Password Reset", body);
        }

        /// <summary>
        ///     Reset the password using a ResetToken
        /// </summary>
        /// <param name="tokenId">The token identifier sent in the email</param>
        /// <param name="newPassword">The new password to set</param>
        /// <returns>Status code indicating the result of the password reset process</returns>
        public void ApplyPasswordReset(string tokenId, string newPassword) {
            // Get the ResetToken from the database
            ResetToken token = _database.GetResetToken(tokenId);

            if (token == null)
                // The token does not exist, stop the process
                throw new TokenDoesNotExistException();

            if (token.Expiration < DateTime.UtcNow) {
                // The token is no longer valid, delete it and cancel the process
                _database.DeleteResetToken(token.UserId);
                throw new InvalidTokenException();
            }

            // The token is valid, update the Users password and delete the token
            User user = _database.GetUserById(token.UserId);
            if (user != null) {
                user.Password = HashPassword(newPassword, user.Salt);
                _database.UpdateUser(user);
            }
            _database.DeleteResetToken(token.UserId);
        }

        /// <summary>
        ///     Hash the given password with a salt
        /// </summary>
        /// <param name="password">The plaintext password to be hashed</param>
        /// <param name="salt">The salt to be used for hashing</param>
        /// <returns>The hashed password</returns>
        public static byte[] HashPassword(string password, Guid salt) {
            SHA256 hasher = SHA256.Create();
            return hasher.ComputeHash(Encoding.UTF8.GetBytes(password))
                         .Concat(BitConverter.GetBytes(salt.GetHashCode())).ToArray();
        }
        #endregion
    }
}
