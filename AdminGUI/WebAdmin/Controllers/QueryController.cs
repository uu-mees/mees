// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Data;
using System.Data.SqlClient;
using WebAdmin.Interfaces;
using WebAdmin.Utilities;

namespace WebAdmin.Controllers {
    public class QueryController {
        private readonly IDatabase _database;

        #region "Constructors"
        public QueryController(IDatabase database) => _database = database;
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Executes the given query
        /// </summary>
        /// <param name="query">The query to execute</param>
        /// <param name="message">The resulting message, if the query is not a SELECT query</param>
        /// <param name="table">The resulting DataTable, if the query is a SELECT query</param>
        /// <param name="rawResult">Whether to return just the number of changed rows or a full text message</param>
        public void ExecuteQuery(string query, out string message, out DataTable table, bool rawResult = false) {
            // Check that the query length is valid
            if (query.Length < 6)
                throw new InvalidQueryLengthException();

            try {
                if (query.Substring(0, 6).ToLower() == "select") {
                    // The query is a SELECT query: return the DataTable resulting from the query
                    table = _database.ExecuteQueryWithResult(query);
                    message = null;
                }
                else {
                    // Return the number of rows affected
                    int rows = _database.ExecuteQuery(query);
                    if (rows >= 0) {
                        message = rawResult ? rows.ToString() : "Command successfully executed - " + rows + " row(s) affected.";
                        table = null;
                    }
                    else {
                        throw new InvalidQueryResultException();
                    }
                }
            }
            catch (SqlException ex) {
                // Return the error as a message
                throw new InvalidQueryException("An error occured while executing your query: <i>" + ex.Message + "(" +
                                                ex.Number + ")</i>");
            }
        }
        #endregion
    }
}
