// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Net.Mail;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;

namespace WebAdmin.Controllers {
    public class UserController {
        private readonly IDatabase _database;
        private readonly INotificationProvider _notificationProvider;

        #region "Constructors"
        public UserController(IDatabase database, INotificationProvider notificationProvider) {
            _database = database;
            _notificationProvider = notificationProvider;
        }
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Creates a new User using the given parameters
        /// </summary>
        /// <param name="username">The name of the User</param>
        /// <param name="password">The plaintext password of the User</param>
        /// <param name="email">The email address of the User</param>
        /// <param name="role">The role of the User</param>
        public void CreateUser(string username, string password, string email, UserRole role) {
            // Validate the input
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password) ||
                string.IsNullOrWhiteSpace(email))
                throw new InvalidInputException();

            try {
                MailAddress emailValidation = new MailAddress(email);
            }
            catch {
                throw new InvalidEmailException();
            }

            // Check that the username does not contain any symbols
            if (!ValidateUserName(username))
                throw new InvalidUsernameException();

            // Check that a user does not share a username with another user.
            if (_database.GetUserByUsername(username) != null)
                throw new UsernameUnavailableException();

            // Insert the User into the database
            _database.CreateUser(new User(Guid.NewGuid(), username, email, password, Guid.NewGuid(), role));
        }

        /// <summary>
        ///     Updates the fields of a User
        /// </summary>
        /// <param name="user">the User object to update</param>
        public void EditUser(User user) {
            // Verify that the User exists
            User oldUser = _database.GetUserById(user.Id);
            if (oldUser == null)
                throw new UserDoesNotExistException();

            // Validate input
            if (string.IsNullOrWhiteSpace(user.Email) || !user.Email.Contains("@") || !user.Email.Contains("."))
                throw new InvalidInputException();

            // Update the user in the database
            _database.UpdateUser(user);
        }

        /// <summary>
        ///     Deletes a User from the database
        /// </summary>
        /// <param name="userId">The unique identifier of the User to delete</param>
        public void DeleteUser(Guid userId) {
            User user = _database.GetUserById(userId);
            if (user == null)
                throw new UserDoesNotExistException();

            // Delete the Users data
            _database.DeleteUserData(userId);

            // Delete the User from the database
            _database.DeleteUser(userId);
        }

        /// <summary>
        ///     Deletes all data associated to a User
        /// </summary>
        /// <param name="userId">The unique identifier of the User whose data should be deleted</param>
        public void DeleteUserData(Guid userId) {
            // Get the User from the database
            User user = _database.GetUserById(userId);

            // Verify that the user exists
            if (user == null)
                throw new UserDoesNotExistException();

            // Check that the account is a regular user
            if (user.Role != UserRole.User)
                throw new InvalidRoleException();

            // Delete all data related to the user
            _database.DeleteUserData(userId);
        }

        /// <summary>
        ///     Gets all Users in the database
        /// </summary>
        /// <param name="rows">The maximum number of results to return</param>
        /// <param name="page">The offset to the number of results to return</param>
        /// <param name="searchUsername">A string to search for in the User name</param>
        /// <param name="searchEmail">A string to search for in the User email address</param>
        /// <param name="searchRole">The role to search for</param>
        /// <returns>A List containing all User objects</returns>
        public List<User> GetUsers(int rows = 30, int page = 0, string searchUsername = "", string searchEmail = "",
                                   string searchRole = "") {
            // Default to this if invalid values are passed
            if (rows < 1) rows = 30;
            if (page < 0) page = 0;

            // Get the Users from the database
            return _database.GetUsers(rows, page, searchUsername, searchEmail, searchRole);
        }

        /// <summary>
        ///     Returns the number of Users in the database that match the search criteria
        /// </summary>
        /// <param name="searchUsername">A string to search for in the User name</param>
        /// <param name="searchEmail">A string to search for in the User email address</param>
        /// <param name="searchRole">The role to search for</param>
        /// <returns>The number of Users found</returns>
        public int GetUserCount(string searchUsername = "", string searchEmail = "", string searchRole = "") =>
            _database.GetUserCount(searchUsername, searchEmail, searchRole);

        /// <summary>
        ///     Gets a User from the database by the Id
        /// </summary>
        /// <param name="userId">The unique identifier of the User to find</param>
        /// <returns>The User object found, or null if no User with this Id exists</returns>
        public User GetUserById(Guid userId) => _database.GetUserById(userId);

        /// <summary>
        ///     Gets a User from the database by the username
        /// </summary>
        /// <param name="username">The unique identifier of the User to find</param>
        /// <returns>The User object found, or null if no User with this username exists</returns>
        public User GetUserByUsername(string username) => _database.GetUserByUsername(username);

        /// <summary>
        ///     Sends a notification to a User
        /// </summary>
        /// <param name="userId">The unique identifier of the User to send the notification to</param>
        /// <param name="title">The title of the notification</param>
        /// <param name="body">The content of the notification</param>
        /// <param name="data">Additional data to send with the notification</param>
        public void SendNotification(Guid userId, string title, string body, object data = null) {
            // Get the FireBase Id of the User
            List<string> tokens = _database.GetFireBaseIds(userId);

            // Verify that the FireBase Id is set
            if (tokens.Count < 1)
                throw new NoFirebaseTokenException();

            // Send the notification
            _notificationProvider.SendPushNotification(tokens, title, body);
        }

        /// <summary>
        ///     Assigns a User to a Research
        /// </summary>
        /// <param name="userId">The unique identifier of the User to assign</param>
        /// <param name="researchId">The unique identifier of the Research to assign to</param>
        public void AssignResearch(Guid userId, Guid researchId) {
            User user = GetUserById(userId);
            Research research = _database.GetResearchById(researchId);

            if (user == null)
                // The User does not exist
                throw new UserDoesNotExistException();

            if (research == null)
                // The Research does not exist
                throw new ResearchDoesNotExistException();

            if (UserAssigned(user, research))
                // The User is already assigned to a Research
                throw new AssignedUserException();

            // Assign the User to the Research
            _database.AssignUser(user.Id, researchId);
        }

        /// <summary>
        /// Checks if the user can be assigned to the research we want to assign them to.
        /// </summary>
        /// <param name="user">the user we want to assign</param>
        /// <param name="research">the research we want to assign them to</param>
        /// <returns>whether the user can be assigned or not</returns>
        private bool UserAssigned(User user, Research research) {
            //Admins can never be assigned.
            if (user.Role == UserRole.Administrator) {
                return true;
            }
            //Users can only be assigned to 1 active research at a time.
            else if (user.Role == UserRole.User) {
                return _database.GetAssignedResearches(user.Id).Count > 0;
            }
            //Researchers cannot be assigned to the same research twice.
            else
                return _database.UserAssigned(user.Id, research.Id);
        }

        /// <summary>
        ///     Verifies that a name contains only A-Z, a-z and 0-9 and is longer than 4 characters.
        /// </summary>
        /// <param name="username">The username to validate</param>
        private static bool ValidateUserName(string username) {
            if (username.Length < 5)
                // Username too short
                return false;

            foreach (char c in username)
                if (!char.IsLetterOrDigit(c))
                    // Invalid character in username
                    return false;

            // Username is valid
            return true;
        }

        /// <summary>
        ///     Gets all Researches assigned to a User
        /// </summary>
        /// <param name="userId">The unique identifier of the User</param>
        /// <returns>A List of all Researches the User is assigned to</returns>
        public List<Research> GetAssignedResearches(Guid userId) {
            // Get the User from the database
            User user = _database.GetUserById(userId);

            if (user == null)
                // The User does not exist
                throw new UserDoesNotExistException();

            // Get the Researches from the database
            List<Research> researches = new List<Research>();
            foreach (Guid id in _database.GetAssignedResearches(user.Id))
                researches.Add(_database.GetResearchById(id));
            return researches;
        }

        /// <summary>
        ///     Tests if a User is assigned to a Research
        /// </summary>
        /// <param name="userId">The unique identifier of the User</param>
        /// <returns>A bool indicating whether the User is actively assigned to a Research</returns>
        public bool IsInResearch(Guid userId) {
            // Get the User from the database
            User user = _database.GetUserById(userId);

            if (user == null)
                // The User does not exist
                throw new UserDoesNotExistException();

            // Check if the user is actively assigned to any research
            foreach (Guid researchId in _database.GetAssignedResearches(user.Id))
                if (_database.UserAssigned(user.Id, researchId))
                    return true;
            return false;
        }

        /// <summary>
        ///     Unassigns a User from a Research
        /// </summary>
        /// <param name="userId">The unique identifier of the User to unassign</param>
        /// <param name="researchId">The unique identifier of the Research to unassign from</param>
        public void UnassignUser(Guid userId, Guid researchId) {
            // Get the User and Research from the database
            User user = _database.GetUserById(userId);
            Research research = _database.GetResearchById(researchId);

            if (user == null)
                // The User does not exist
                throw new UserDoesNotExistException();

            if (research == null)
                // The Research does not exist
                throw new ResearchDoesNotExistException();

            DateTime end = research.EndDate;
            if (user.Role == UserRole.Researcher)
                end = DateTime.MaxValue;
            // Unassign the User
            _database.UnassignUser(user.Id, researchId, end);
        }
        #endregion
    }
}
