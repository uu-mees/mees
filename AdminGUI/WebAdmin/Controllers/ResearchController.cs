// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;
using WebAdmin.Utilities;

namespace WebAdmin.Controllers {
    public class ResearchController {
        private readonly IDatabase _database;

        #region "Constructors"
        public ResearchController(IDatabase database) => _database = database;
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Gets all Researches in the database
        /// </summary>
        /// <param name="rows">The maximum number of results to return</param>
        /// <param name="page">The offset to the number of results to return</param>
        /// <param name="searchName">A string to search for in the Research name</param>
        /// <param name="searchDescription">A string to search for in the Research description</param>
        /// <returns>A List containing all Research objects</returns>
        public List<Research> GetResearches(int rows = 30, int page = 0, string searchName = "",
                                            string searchDescription = "") {
            // Default to these values if invalid parameters are passed
            if (rows < 1) rows = 30;
            if (page < 0) page = 0;

            // Retrieve Researches from the database
            return _database.GetResearches(rows, page, searchName, searchDescription);
        }

        /// <summary>
        ///     Counts the total number of Users assigned to a Research
        /// </summary>
        /// <param name="researchId">The Id of the Research to count Users from</param>
        /// <returns>The number of Users assigned to the Research</returns>
        public int CountUsersInResearch(Guid researchId) {
            return _database.CountUsersInResearch(researchId);
        }

        /// <summary>
        ///     Counts the number of active Users assigned to a Research
        /// </summary>
        /// <param name="researchId">The ID of the Research to count active Users from</param>
        /// <returns>The number of active Users in the Research</returns>
        public int CountActiveUsersInResearch(Guid researchId) {
            return _database.CountActiveUsersInResearch(researchId);
        }


        /// <summary>
        ///     Returns the number of Researches in the database that match the given search strings
        /// </summary>
        /// <param name="searchName">A string to search for in the Research name</param>
        /// <param name="searchDescription">A string to search for in the Research description</param>
        /// <returns>The number of Researches found</returns>
        public int GetResearchCount(string searchName = "", string searchDescription = "") =>
            _database.GetResearchCount(searchName, searchDescription);

        /// <summary>
        ///     Gets all Researches assigned to the given researcher
        /// </summary>
        /// <param name="researcherId">The unique identifier of the researcher User object</param>
        /// <returns>A List containing all Researches assigned to the researcher</returns>
        public List<Research> GetAssignedResearches(Guid researcherId) {
            // Setup a empty list
            List<Research> researches = new List<Research>();

            // Get the User object matching the passed Id
            User user = _database.GetUserById(researcherId);
            if (user != null) {
                // Get all Researches assigned to the User
                foreach (Guid id in _database.GetAssignedResearches(user.Id))
                    researches.Add(_database.GetResearchById(id));
            }

            return researches;
        }

        /// <summary>
        ///     Gets a Research object by its Id
        /// </summary>
        /// <param name="researchId">The unique identifier of the Research to get</param>
        /// <returns>The Research object matching the passed Id, or null if no Research was found</returns>
        public Research GetResearchById(Guid researchId) => _database.GetResearchById(researchId);

        /// <summary>
        ///     Gets a Research object by its name
        /// </summary>
        /// <param name="name">The name of the Research to get</param>
        /// <returns>The Research object matching the passed name, or null if no Research was found</returns>
        public Research GetResearchByName(string name) => _database.GetResearchByName(name);

        /// <summary>
        ///     Creates a new Research with the given parameters
        /// </summary>
        /// <param name="name">The name of the Research</param>
        /// <param name="description">The description of the Research</param>
        /// <param name="permission">The permissions related to the Research</param>
        /// <param name="startDate">The start date of the Research</param>
        /// <param name="endDate">The end date of the Research</param>
        public void CreateResearch(string name, string description, PermissionList permission,
                                    DateTime startDate, DateTime endDate) {

            // Verify that the name is not already known
            if (GetResearchByName(name) != null)
                throw new DuplicateResearchException();

            // Validate the start and end dates
            if (startDate >= endDate)
                throw new InvalidResearchLengthException();

            // Validate the end date of the Research is in the future
            if (endDate <= DateTime.UtcNow)
                throw new InvalidResearchEndException();

            // Validate the name and description
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(description))
                throw new InvalidInputException();

            // Create the new research and insert it into the database
            Research research = new Research(Guid.NewGuid(), name, description, permission, startDate, endDate);
            _database.CreateResearch(research);
        }

        /// <summary>
        ///     Removes the Research with the given Id
        /// </summary>
        /// <param name="researchId">The unique identifier of the Research to remove</param>
        public void RemoveResearch(Guid researchId) {
            // Check that the Research exists
            Research research = GetResearchById(researchId);
            if (research == null)
                throw new ResearchDoesNotExistException();

            if (research.EndDate > DateTime.UtcNow && research.StartDate < DateTime.UtcNow)
                // Research is ongoing, can't delete before it is stopped
                throw new IllegalChangeException();

            _database.RemoveResearch(researchId);
        }

        /// <summary>
        ///     Updates the fields of a Research
        /// </summary>
        /// <param name="research">The Research object to update</param>
        public void EditResearch(Research research) {
            Research checkId = GetResearchById(research.Id);
            Research checkName = GetResearchByName(research.Name);
            // Verify that the research we want to change exists
            if (checkId == null)
                throw new ResearchDoesNotExistException();

            // Verify that the Research name is not yet in use
            if (checkName != null)
                if (checkName.Id != checkId.Id)
                    throw new DuplicateResearchException();

            // Validate the start and end dates
            if (research.StartDate >= research.EndDate)
                throw new InvalidResearchLengthException();
            if (research.EndDate <= DateTime.UtcNow)
                throw new InvalidResearchEndException();

            // Update the Research in the database
            _database.UpdateResearch(research);
        }

        /// <summary>
        ///     Ends the Research with the given Id
        /// </summary>
        /// <param name="researchId">The unique identifier of the Research to remove</param>
        public void EndResearch(Guid researchId) {
            // Get the Research from the database
            Research research = _database.GetResearchById(researchId);

            // Verify the Research exists
            if (research == null)
                throw new ResearchDoesNotExistException();

            if (research.EndDate < DateTime.UtcNow || research.StartDate > DateTime.UtcNow)
                // Research is not running so we can't end it
                throw new IllegalChangeException();

            // Get all Users assigned to this Researches and unassign them
            List<Guid> participants = _database.GetParticipants(research.Id);
            foreach (Guid userId in participants)
                if (_database.GetUserById(userId).Role == User.UserRole.User)
                    _database.UnassignUser(userId, research.Id, research.EndDate);

            // End the Research in the database
            research.EndDate = DateTime.UtcNow;
            _database.UpdateResearch(research);
        }

        /// <summary>
        ///     Tests whether a Research has been ended
        /// </summary>
        /// <param name="researchId">The unique identifier of the Research to test</param>
        /// <returns>Whether the Research has ended</returns>
        public bool ResearchEnded(Guid researchId) => _database.GetResearchById(researchId).EndDate < DateTime.UtcNow;
        #endregion
    }
}
