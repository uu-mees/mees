// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using WebAdmin.DataTypes;
using WebAdmin.Interfaces;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.DataSource;

namespace WebAdmin.Controllers {
    public class DataSourceController {
        private readonly IDatabase _database;

        #region "Constructors"
        public DataSourceController(IDatabase database) => _database = database;
        #endregion

        #region "Functionality"
        /// <summary>
        ///     Gets all DataSources from the database
        /// </summary>
        /// <returns>A List of DataSource objects</returns>
        public List<DataSource> GetDataSources() => _database.GetDataSources();

        /// <summary>
        ///     Finds a DataSource object by its Id
        /// </summary>
        /// <param name="dataSourceId">The unique identifier to search for</param>
        /// <returns>The DataSource object if it exists; null otherwise</returns>
        public DataSource GetDataSourceById(Guid dataSourceId) => _database.GetDataSourceById(dataSourceId);

        /// <summary>
        ///     Finds a DataSource object by its URL
        /// </summary>
        /// <param name="url">The URL to search for</param>
        /// <returns>The DataSource object if it exists; null otherwise</returns>
        public DataSource GetDataSourceByUrl(string url) => _database.GetDataSourceByUrl(url);

        /// <summary>
        ///     Disables the DataSource with the given Id
        /// </summary>
        /// <param name="dataSourceId">The unique identifier to search for</param>
        public void DisableDataSource(Guid dataSourceId) {
            DataSource dataSource = _database.GetDataSourceById(dataSourceId);
            dataSource.Active = false;
            _database.UpdateDataSource(dataSource);
        }

        /// <summary>
        ///     Enables the DataSource with the given Id
        /// </summary>
        /// <param name="dataSourceId">The unique identifier to search for</param>
        public void EnableDataSource(Guid dataSourceId) {
            DataSource dataSource = _database.GetDataSourceById(dataSourceId);
            dataSource.Active = true;
            _database.UpdateDataSource(dataSource);
        }

        /// <summary>
        ///     Deletes the DataSource with the given Id
        /// </summary>
        /// <param name="dataSourceId">The unique identifier to search for</param>
        public void RemoveDataSource(Guid dataSourceId) {
            // Verify that the DataSource exists
            if (GetDataSourceById(dataSourceId) == null)
                throw new DatasourceDoesNotExistException();

            _database.RemoveDataSource(dataSourceId);
        }

        /// <summary>
        ///     Adds a data source with the given parameters
        /// </summary>
        /// <param name="url">The url of the DataSource</param>
        /// <param name="type">Whether the DataSource is a sensor or raster type</param>
        /// <param name="active">Whether the source is enabled</param>
        public void AddDataSource(string url, DataSourceType type, bool active) {
            // Create a new DataSource object
            DataSource dataSource = new DataSource(Guid.NewGuid(), url, active, type);

            // Verify that the Url is valid
            if (string.IsNullOrWhiteSpace(dataSource.Url))
                throw new InvalidDatasourceException();

            // Verify that the Url is not already known
            if (_database.GetDataSourceByUrl(dataSource.Url) != null)
                throw new DuplicateDatasourceException();

            // Add the DataSource to the database
            _database.AddDataSource(dataSource);
        }

        /// <summary>
        ///     Updates the given DataSource
        /// </summary>
        /// <param name="dataSource">The DataSource object to update</param>
        public void EditDataSource(DataSource dataSource) {
            // Validate the url
            if (string.IsNullOrWhiteSpace(dataSource.Url))
                throw new InvalidDatasourceException();

            DataSource checkId = _database.GetDataSourceById(dataSource.Id);
            DataSource checkUrl = _database.GetDataSourceByUrl(dataSource.Url);

            // Verify that the source we want to change exists
            if (checkId == null)
                throw new DatasourceDoesNotExistException();

            // Verify that the Url is not yet in use
            if (checkUrl != null && checkUrl.Id != checkId.Id)
                throw new DuplicateDatasourceException();

            // Update the source in the database
            _database.UpdateDataSource(dataSource);
        }
        #endregion
    }
}
