// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using WebAdmin.Utilities;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class DownloadResearchPage : Page {
        private Guid _researchId;

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            string id = Request.Params["research"];
            if (!Guid.TryParse(id, out _researchId)) {
                Master.AddAlert(new Alert("Research Id invalid", AlertType.Error));
                Master.Redirect("~/researcher.aspx");
            }
        }

        /// <summary>
        ///     Downloads the personal data from users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadUsers_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT users.Id, users.Weight, users.Height, users.Sex, users.DateOfBirth
                            FROM dbo.Users users 
                            INNER JOIN UserResearches ur ON users.Id = ur.userId  
                            WHERE users.role = 0 
                            AND ur.researchId = %id%;",
                          "Users_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the ratings left by users in a particular research as a .csv file
        /// </summary>
        protected void DownloadRatings_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT ratings.*
                            FROM dbo.UserRatings ratings
                            INNER JOIN Users users ON users.Id = ratings.UserId
                            INNER JOIN UserResearches ur ON users.Id = ur.userId
                            WHERE users.role = 0
                            AND ur.researchId = %id%
                            AND ur.startTime <= ratings.TimeStamp
                            AND ur.endTime >= ratings.TimeStamp;",
                          "Ratings_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads information about a particular research as a .csv file.
        /// </summary>
        protected void DownloadResearch_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT *
                            FROM dbo.Researches
                            WHERE id = %id%;",
                          "Research_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the exposure log of participants in a particular research as a .csv file
        /// </summary>
        protected void DownloadLocationExposures_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT exposures.*
                            FROM dbo.UserLocationExposures exposures
                            INNER JOIN Users users ON users.Id = exposures.UserId
                            INNER JOIN UserResearches ur ON users.Id = ur.userId
                            WHERE users.role = 0
                            AND ur.researchId = %id%
                            AND ur.startTime <= exposures.Timestamp 
                            AND ur.endTime >= exposures.Timestamp;",
                          "LocationExposures_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the minigame scores obtained by users in a particular research as a .csv file
        /// </summary>
        protected void DownloadMinigameScores_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT scores.*
                            FROM dbo.MinigameScores scores
                            INNER JOIN Users users ON users.Id = scores.UserId
                            INNER JOIN UserResearches ur ON users.Id = ur.userId
                            WHERE users.role = 0
                            AND ur.researchId = %id%
                            AND ur.startTime <= scores.timeStamp
                            AND ur.endTime >= scores.timeStamp;",
                          "Minigames_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the Daily exposures of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadDailyExposures_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT exposures.*
                            FROM dbo.DailyExposures exposures
                            INNER JOIN Users users ON users.Id = exposures.UserId
                            INNER JOIN UserResearches ur ON users.Id = ur.userId
                            WHERE users.role = 0
                            AND ur.researchId = %id%
                            AND ur.startTime <= exposures.Date 
                            AND ur.endTime >= exposures.Date;",
                          "DailyExposures_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the questionnaire Ids of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadQuestionnaires_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT * FROM dbo.Questionnaires WHERE Trigger_TriggerId IN (
	                        SELECT triggers.TriggerId
		                    FROM dbo.Triggers triggers
		                    INNER JOIN Users users ON users.Id = triggers.UserId
		                    INNER JOIN UserResearches ur ON users.Id = ur.userId
		                    WHERE users.role = 0
		                    AND ur.researchId = %id%
		                    AND ur.startTime <= triggers.Timestamp 
		                    AND ur.endTime >= triggers.TimeStamp
		                    );",
                          "Questionnaires_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the triggers of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadTriggers_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT triggers.*
	                        FROM dbo.Triggers triggers
		                    INNER JOIN Users users ON users.Id = triggers.UserId
		                    INNER JOIN UserResearches ur ON users.Id = ur.userId
		                    WHERE users.role = 0
		                    AND ur.researchId = %id%
		                    AND ur.startTime <= triggers.Timestamp 
		                    AND ur.endTime >= triggers.TimeStamp;",
                          "Triggers_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        private void DownloadQuery(string query, string filename = "") {
            Session["downloadquery"] = query.Replace("%id%", "'" + _researchId + "'");
            Session["downloadfilename"] = filename;
            Master.Redirect("~/download.aspx");
        }
    }
}
