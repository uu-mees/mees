// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class OverviewPage : Page {
        private readonly QueryController _queryController;

        protected OverviewPage() =>
            _queryController = new QueryController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            string cookie = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(cookie)) {
                Master.LogOut();
                return;
            }

            // Redirect to the Researcher page if a researcher logged in
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole == UserRole.Researcher)
                Master.Redirect("~/researcher.aspx");
        }

        /// <summary>
        ///     Click handler for the Delete Ratings button
        /// </summary>
        protected void DeleteRatings_Click(object sender, EventArgs e) {
            try {
                // Parse the date so different formats are supported and no SQL injection is possible
                DateTime date = DateTime.Parse(deleteRatingsDate.Text);
                string dateSql = date.ToString("yyyy/MM/dd");

                // Delete the ratings
                _queryController.ExecuteQuery("DELETE dbo.UserRatings WHERE TimeStamp < '" + dateSql + "';", out string deletionCount, out _, true);
                Master.AddAlert(new Alert("Deleted " + deletionCount + " User Rating(s)", Alert.AlertType.Success, Alert.LifeTime.DeleteImmediately));
            }
            catch {
                Master.AddAlert(new Alert("The date entered is not valid", Alert.AlertType.Error, Alert.LifeTime.DeleteImmediately));
            }

            Master.Refresh();
        }
    }
}
