<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="datasource.aspx.cs" Inherits="WebAdmin.DataSourcePage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="DataSource" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page-small">
        <!-- Header info -->
        <h3 class="mees-page-title" runat="server" id="form_header"></h3>
        <p runat="server" id="form_description"></p>

        <!-- Form -->
        <div id="form_input" runat="server" visible="false">
            <p>Url:</p>
            <div class="form-group">
                <asp:TextBox ID="f_url" runat="server" CssClass="form-control input-lg" placeholder="Url" required="True" MaxLength="157"></asp:TextBox>
            </div>
            <p>Data type:</p>
            <div class="form-group">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-info mees-radio active">
                        <asp:RadioButton runat="server" type="radio" ID="f_raster" GroupName="f_type" />
                        Raster
                    </label>
                    <label class="btn btn-info mees-radio">
                        <asp:RadioButton runat="server" type="radio" ID="f_sensor" GroupName="f_type" />
                        Sensor
                    </label>
                </div>
            </div>
            <p>Activate Upload:</p>
            <div class="form-group">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-info mees-radio active">
                        <asp:RadioButton runat="server" type="radio" ID="f_active" GroupName="f_upload" />
                        Yes
                    </label>
                    <label class="btn btn-info mees-radio">
                        <asp:RadioButton runat="server" type="radio" ID="f_inactive" GroupName="f_upload" />
                        No
                    </label>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div runat="server" id="dataSourceTable" visible="False">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th>Id</th>
                        <th>Url</th>
                        <th>Active</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <asp:Literal runat="server" ID="dataSourceTableContent"></asp:Literal>
            </table>
        </div>

        <!-- Buttons -->
        <a href="datasources.aspx" class="btn btn-danger mees-align-right">Cancel</a>
        <asp:Button runat="server" ID="button_confirm" CssClass="btn btn-success mees-align-right" />

        <!-- JavaScript -->
        <script type="text/javascript">
            FixCheckboxActiveStatus();
        </script>
    </div>
</asp:Content>
