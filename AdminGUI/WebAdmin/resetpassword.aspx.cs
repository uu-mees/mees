// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class ResetPasswordPage : Page {
        private readonly AuthenticationController _authenticationController;

        protected ResetPasswordPage() => _authenticationController = new AuthenticationController(
                                                                                                  new SqlServerDatabase(ConfigurationManager
                                                                                                                        .ConnectionStrings["database"]
                                                                                                                        .ConnectionString),
                                                                                                  new SmtpEmailProvider()
                                                                                                 );

        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Set up the Master page fields
            Master.InitializeForm(
                                  "MEES Administrator - Reset Password",
                                  "Reset Password",
                                  "Return to Login"
                                 );

            // Autofill the token field
            if (!string.IsNullOrEmpty(Request.Params["token"]))
                token.Text = Request.Params["token"];

            // Subscribe to the button press events
            Master.SubscribeToButtonClick(Reset_Click);
            Master.SubscribeToLinkClick(ReturnToLoginPage_Click);
        }

        /// <summary>
        ///     Button click handler that redirects the user to the Login page
        /// </summary>
        private void ReturnToLoginPage_Click(object sender, EventArgs e) {
            Response.Redirect("~/login.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        ///     Button click handler that starts the password reset procedure
        /// </summary>
        private void Reset_Click(object sender, EventArgs e) {
            try {
                // Verify that the passwords match
                if (password1.Text != password2.Text)
                    throw new PasswordsDoNotMatchException();

                // Apply the password reset
                _authenticationController.ApplyPasswordReset(token.Text, password1.Text);
                Master.AddAlert(new Alert("Your password has been updated", AlertType.Success));
                Master.Redirect("~/login.aspx");
            }
            catch (PasswordsDoNotMatchException) {
                Master.AddAlert(new Alert("The passwords do not match", AlertType.Error));
                Master.Refresh();
            }
            catch (InvalidTokenException) {
                Master.AddAlert(new Alert("The token is not valid", AlertType.Error));
                Master.Refresh();
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.ToString());
                Response.End();
            }
        }
    }
}
