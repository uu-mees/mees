// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.UI;
using static WebAdmin.DataTypes.User;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class DownloadAdminPage : Page {

        /// <summary>
        ///     Executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is logged in and is allowed to access this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }
        }

        /// <summary>
        ///     Downloads the personal data from users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadUsers_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT users.Id, users.Weight, users.Height, users.Sex, users.DateOfBirth
                            FROM dbo.Users users;",
                          "Users_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the ratings left by users in a particular research as a .csv file
        /// </summary>
        protected void DownloadRatings_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT ratings.*
                            FROM dbo.UserRatings ratings;",
                          "Ratings_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads information about a particular research as a .csv file.
        /// </summary>
        protected void DownloadResearch_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT *
                            FROM dbo.Researches;",
                          "Research_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the exposure log of participants in a particular research as a .csv file
        /// </summary>
        protected void DownloadLocationExposures_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT exposures.*
                            FROM dbo.UserLocationExposures exposures;",
                          "LocationExposures_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the minigame scores obtained by users in a particular research as a .csv file
        /// </summary>
        protected void DownloadMinigameScores_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT scores.*
                            FROM dbo.MinigameScores scores;",
                          "Minigames_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the Daily exposures of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadDailyExposures_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT exposures.*
                            FROM dbo.DailyExposures exposures;",
                          "DailyExposures_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the questionnaire Ids of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadQuestionnaires_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT * FROM dbo.Questionnaires;",
                          "Questionnaires_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        /// <summary>
        ///     Downloads the triggers of users participating in a particular research as a .csv file
        /// </summary>
        protected void DownloadTriggers_Click(object sender, EventArgs e) {
            DownloadQuery(
                          @"SELECT triggers.*
	                        FROM dbo.Triggers triggers;",
                          "Triggers_" + DateTime.UtcNow.ToString("yyyy-MM-dd") + ".csv"
                         );
        }

        private void DownloadQuery(string query, string filename = "") {
            Session["downloadquery"] = query;
            Session["downloadfilename"] = filename;
            Session["downloadredirect"] = "~/downloadadmin.aspx";
            Master.Redirect("~/download.aspx");
        }
    }
}
