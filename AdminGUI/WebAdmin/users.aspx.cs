// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class UsersPage : Page {
        private const int RowsPerPage = 30;
        private readonly UserController _userController;

        protected UsersPage() =>
            _userController = new UserController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString), new FirebaseNotificationProvider());

        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is logged in and is allowed to access this page
            if (Master.GetRoleFromCookie() != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Stop if the form has already been setup
            if (IsPostBack)
                return;

            try {
                // Fill search boxes from the url
                string searchUser = Request.Params["search_user"];
                filter_name.Text = searchUser;
                string searchMail = Request.Params["search_mail"];
                filter_mail.Text = searchMail;
                string searchRole = Request.Params["search_role"];
                if (int.TryParse(searchRole, out int roleIndex))
                    filter_role.SelectedIndex = roleIndex + 1;
                if (searchRole == "-1") searchRole = "";
                if (!int.TryParse(Request.Params["page"], out int page))
                    page = 0;

                // Show/hide divs if we are in assign mode
                UserRows.Text = "";
                if (Request.Params["task"] == "assign") {
                    assign.Visible = true;
                    normal_view.Visible = false;
                }
                else {
                    normal_view.Visible = true;
                    assign.Visible = false;
                }

                // Get all users matching the search criteria
                List<User> users = _userController.GetUsers(RowsPerPage, page, searchUser, searchMail, searchRole);
                // Print these users to the table
                foreach (User user in users)
                    UserRows.Text += PrintRow(user, _userController.IsInResearch(user.Id));


                // Print the navigation bar
                int userCount = _userController.GetUserCount(searchUser, searchMail, searchRole);
                int pageCount = (userCount - 1) / RowsPerPage + 1;
                StringBuilder nav = new StringBuilder();
                for (int i = 1; i <= pageCount; i++)
                    if (i - 1 == page) {
                        // Current page - print an unusable link
                        nav.Append("<a href=\"#\" class=\"btn btn-link disabled\">" + i + "</a> ");
                    }
                    else {
                        // Print a link to the page, and keep the filters/mode that is currently set
                        string url = "users.aspx?page=" + (i - 1) + "&search_user=" + searchUser + "&search_mail=" + searchMail + "&search_role=" + searchRole +
                                     (Request.Params["task"] == "assign"
                                          ? "&task=assign&id=" + Request.Params["id"]
                                          : "");
                        nav.Append("<a href=\"" + url + "\" class=\"btn btn-link\">" + i + "</a> ");
                    }

                navigation.Text = nav.ToString();
            }
            catch (Exception ex) {
                // A unexpected error occurred
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/overview.aspx");
            }
        }

        /// <summary>
        ///     Parse a User object to an HTML table row
        /// </summary>
        /// <param name="user">The User object to parse</param>
        /// <returns>String containing the generated HTML</returns>
        private static string PrintRow(User user, bool researchStatus) {
            StringBuilder html = new StringBuilder();
            html.AppendLine("<tr>");
            html.AppendLine("<td style=\"text-align: center; vertical-align: middle;\">");
            html.AppendLine("<input type=\"checkbox\" name=\"" + user.Id + "\" />");
            html.AppendLine("</td>");
            // Print the user data
            html.AppendLine("<td>" + user.Username + "</td>");
            html.AppendLine("<td>" + user.Email + "</td>");
            html.AppendLine("<td>" + user.Role + "</td>");
            html.AppendLine("<td>" + (user.Role == UserRole.User ? (researchStatus ? "Yes" : "No") : "-") + "</td>");
            html.AppendLine("</tr>");
            return html.ToString();
        }

        /// <summary>
        ///     Stores all selected Ids in the session
        /// </summary>
        private void StoreIdsInSession() {
            // Read User Ids from the form
            List<string> userlist = Request.Form.AllKeys.Where(s => Guid.TryParse(s, out _)).ToList();
            Session["userlist"] = string.Join(";", userlist);
        }

        #region "Button Click Handlers"
        /// <summary>
        ///     Click handler for the Delete User button - stores the selected Ids in the session and redirects
        /// </summary>
        protected void DeleteUser(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("user.aspx?task=delete");
        }

        /// <summary>
        ///     Click handler for the Clear Filters button
        /// </summary>
        protected void ClearFilters(object sender, EventArgs e) {
            if (Request.Params["task"] == "assign")
                Master.Redirect("users.aspx?task=assign&id=" + Request.Params["id"]);
            else
                Master.Redirect("users.aspx");
        }

        /// <summary>
        ///     Click handler for the Wipe User Data button - stores the selected Ids in the session and redirects
        /// </summary>
        protected void WipeUser(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("user.aspx?task=wipe");
        }

        /// <summary>
        ///     Click handler for the Create User button
        /// </summary>
        protected void CreateUser(object sender, EventArgs e) => Master.Redirect("user.aspx?task=create");

        /// <summary>
        ///     Click handler for the Edit User button - stores the selected Id in the session and redirects
        /// </summary>
        protected void EditUser(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("user.aspx?task=edit");
        }

        /// <summary>
        ///     Click handler for the Send Notification button - stores the selected Ids in the session and redirects
        /// </summary>
        protected void NotifyUser(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("user.aspx?task=notify");
        }

        /// <summary>
        ///     Click handler for the Unassign User from Research button - stores the selected Ids in the session and redirects
        /// </summary>
        protected void UnassignUser(object sender, EventArgs e) {
            StoreIdsInSession();
            Master.Redirect("user.aspx?task=unassign");
        }

        /// <summary>
        ///     Button click handler for assigning selected Users to a Research
        /// </summary>
        protected void AssignUser(object sender, EventArgs e) {
            // Get the Research Id from the url
            Guid.TryParse(Request.Params["id"], out Guid research);

            // Loop over the selected User Ids
            foreach (string s in Request.Form.AllKeys)
                if (Guid.TryParse(s, out Guid userId)) {
                    User user = null;
                    try {
                        // Get the User object with the selected Id
                        user = _userController.GetUserById(userId);

                        // Assign this User to the Research
                        _userController.AssignResearch(user.Id, research);
                        Master.AddAlert(new Alert("The user with id '" + userId + "' has been assigned to the research with id '" + research + "'", AlertType.Success));
                    }
                    catch (UserDoesNotExistException) {
                        Master.AddAlert(new Alert("There is no user with id " + s, AlertType.Error));
                    }
                    catch (ResearchDoesNotExistException) {
                        Master.AddAlert(new Alert("There is no research with id " + research, AlertType.Error));
                    }
                    catch (AssignedUserException) {
                        if (user != null)
                            switch (user.Role) {
                                case UserRole.User:
                                    Master.AddAlert(new
                                                        Alert("Could not assign the user with id '" + userId + "' because they were already assigned to a research",
                                                              AlertType.Error));
                                    break;
                                case UserRole.Researcher:
                                    Master.AddAlert(new
                                                        Alert("The researcher with id '" + userId + "' is already assigned to the research with id '" + research + "'",
                                                              AlertType.Error));
                                    break;
                                case UserRole.Administrator:
                                    Master.AddAlert(new Alert("Cannot assign an administrator to a research", AlertType.Error));
                                    break;
                            }
                    }
                }

            Master.Redirect("researches.aspx");
        }

        /// <summary>
        ///     Click handler for the Search buttons
        /// </summary>
        protected void Search_Click(object sender, EventArgs e) {
            // Collect search strings
            string username = filter_name.Text;
            string mail = filter_mail.Text;
            string role = filter_role.Text;
            // Setup url and redirect
            string url = "~/users.aspx?search_user=" + username + "&search_mail=" + mail + "&search_role=" + role +
                         (Request.Params["task"] == "assign" ? "&task=assign&id=" + Request.Params["id"] : "");
            Master.Redirect(url);
        }
        #endregion
    }
}
