// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class UserPage : Page {
        private readonly UserController _userController;

        protected UserPage() =>
            _userController = new UserController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString), new FirebaseNotificationProvider());

        #region "Form Setup"
        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Set up form based on the selected task
            string task = Request.Params["task"];
            switch (task) {
                case "create":
                    form_header.InnerText = "Create a new User";
                    form_input.Visible = true;
                    button_confirm.Text = "Create User";
                    AddButtonClickHandler(CreateUser_Click);
                    break;
                case "edit":
                    form_header.InnerText = "Edit User";
                    form_input.Visible = true;
                    button_confirm.Text = "Save Changes";
                    if (!IsPostBack)
                        FillFormFromExistingUser();
                    AddButtonClickHandler(EditUser_Click);
                    break;
                case "delete":
                    form_header.InnerText = "Delete Users";
                    form_description.InnerText = "Are you sure you want to delete the following users?";
                    userTable.Visible = true;
                    userTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Delete Users";
                    AddButtonClickHandler(DeleteUser_Click);
                    break;
                case "wipe":
                    form_header.InnerText = "Wipe User Data";
                    form_description.InnerText = "Are you sure you want to wipe all data associated with the following users?";
                    userTable.Visible = true;
                    userTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Wipe Data";
                    AddButtonClickHandler(WipeUser_Click);
                    break;
                case "notify":
                    form_header.InnerText = "Send Notification";
                    form_description.InnerText = "The notification will be sent to the following users:";
                    userTable.Visible = true;
                    userTableContent.Text = FillTableFromSession();
                    notification_input.Visible = true;
                    button_confirm.Text = "Send Notification";
                    AddButtonClickHandler(NotifyUser_Click);
                    break;
                case "unassign":
                    form_header.InnerText = "Unassign Users from Research";
                    form_description.InnerText = "Select the users to unassign and the researches to unassign them from:";
                    unassignTable.Visible = true;
                    unassignTableContent.Text = FillTableUnassign();
                    button_confirm.Text = "Unassign Users";
                    AddButtonClickHandler(Unassign_Click);
                    break;
                default:
                    Master.AddAlert(new Alert("Users.aspx: no task set", AlertType.Error));
                    Master.Redirect("~/users.aspx");
                    break;
            }
        }

        /// <summary>
        ///     Binds a method to the button click event
        /// </summary>
        /// <param name="onClick">The method to execute on a button click</param>
        private void AddButtonClickHandler(Action<object, EventArgs> onClick) {
            void Handler(object obj, EventArgs args) => onClick(obj, args);
            button_confirm.Click += Handler;
        }
        #endregion

        #region "Data Loading"
        /// <summary>
        ///     Fills in the form with data from a existing User
        /// </summary>
        private void FillFormFromExistingUser() {
            try {
                // Load the selected Users list from the session
                Guid parsedId = Guid.Parse((string)Session["userlist"]);

                // Load the selected User from the database
                User user = _userController.GetUserById(parsedId);

                // Throw an exception if no such User exists
                if (user == null)
                    throw new UserDoesNotExistException();

                // Fill the form with the retrieved data
                f_email.Text = user.Email;
                f_username.Text = user.Username;

                // Disable fields that cannot be altered
                f_username.ReadOnly = true;
                f_password.ReadOnly = true;
                roleRadioButtons.Visible = false;
            }
            catch {
                Master.AddAlert(new Alert("No user specified", AlertType.Error));
                Master.Redirect("~/users.aspx");
            }
        }

        /// <summary>
        ///     Parses the passed User Ids to a table containing their fields
        /// </summary>
        /// <returns>The Users table as HTML</returns>
        private string FillTableFromSession() {
            try {
                // Load the selected User Ids from the session
                List<Guid> parsedIds = ((string)Session["userlist"]).Split(';').Select(Guid.Parse).ToList();

                StringBuilder result = new StringBuilder();
                // Get each User from the database and add it to the table
                foreach (Guid id in parsedIds) {
                    User user = _userController.GetUserById(id);
                    result.AppendLine("<tr>");
                    result.AppendLine("<td>" + user.Id + "</td>");
                    result.AppendLine("<td>" + user.Username + "</td>");
                    result.AppendLine("<td>" + user.Role + "</td>");
                    result.AppendLine("</tr>");
                }

                return result.ToString();
            }
            catch {
                Master.AddAlert(new Alert("No user(s) specified", AlertType.Error));
                Master.Redirect("~/users.aspx");
                return null;
            }
        }

        /// <summary>
        ///     Fills the table used in the unassign mode
        /// </summary>
        /// <returns>The unassigning table as HTML</returns>
        private string FillTableUnassign() {
            // Get the User Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return "";

            StringBuilder result = new StringBuilder();
            int rowCount = 0;

            // Print each user/research combination to the table
            foreach (Guid id in ids) {
                try {
                    User user = _userController.GetUserById(id);
                    if (user.Role != UserRole.Administrator) {
                        List<Research> research = _userController.GetAssignedResearches(id);
                        if (research.Count < 1)
                            Master.AddAlert(new Alert("No Researches are assigned to the user with id '" + user.Id + "'", AlertType.Warning));
                        foreach (Research res in research) {
                            rowCount++;
                            result.AppendLine("<tr>");
                            result.AppendLine("<td style=\"text-align: center; vertical-align: middle;\">");
                            result.AppendLine("<input type=\"checkbox\" name=\"" + res.Id + ":" + user.Id + "\" /></td>");
                            result.AppendLine("<td>" + user.Id + "</td>");
                            result.AppendLine("<td><b>" + user.Username + "</b></td>");
                            result.AppendLine("<td>" + user.Role + "</td>");
                            result.AppendLine("<td>" + res.Name + "</td>");
                            result.AppendLine("<td>" + res.Id + "</td>");
                            result.AppendLine("</tr>");
                        }
                    }
                    else
                        Master.AddAlert(new Alert("Cannot assign a administrator to a research", AlertType.Info));
                }
                catch (UserDoesNotExistException) {
                    Master.AddAlert(new Alert("No user with this id exists", AlertType.Error));
                }
            }

            if (rowCount < 1)
                Master.Redirect("~/users.aspx");

            return result.ToString();
        }
        #endregion

        #region "Button Click Handlers"
        /// <summary>
        ///     Parses the session variable to a list of Ids
        /// </summary>
        /// <returns>A List of Guids parsed</returns>
        private List<Guid> ParseSessionIds() {
            // Verify the users session is still active
            if (!Master.VerifyAuthentication())
                return null;

            // Parse the Guid(s) stored in the session
            try {
                string userlist = (string)Session["userlist"];
                return userlist.Contains(";") ? userlist.Split(';').Select(Guid.Parse).ToList() : new List<Guid> { Guid.Parse(userlist) };
            }
            catch {
                Master.AddAlert(new Alert("No user(s) specified", AlertType.Error));
                return null;
            }
        }

        /// <summary>
        ///     Click handler for the Delete Users button
        /// </summary>
        private void DeleteUser_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            foreach (Guid id in ids)
                try {
                    // Get the User object corresponding to this id
                    User user = _userController.GetUserById(id);

                    // Verify that the User exists
                    if (user == null)
                        throw new UserDoesNotExistException();

                    // Verify that the administrator is not trying to delete his/her own account
                    if (id.ToString() == HttpContext.Current.User.Identity.Name.Split('|')[0])
                        throw new CurrentUserException();

                    // Delete the User from the database
                    _userController.DeleteUser(id);
                    Master.AddAlert(new Alert("User '" + user.Username + "' has been deleted", AlertType.Success));
                }
                catch (UserDoesNotExistException) {
                    Master.AddAlert(new Alert("User Id is not valid or user does not exist", AlertType.Error));
                }
                catch (CurrentUserException) {
                    Master.AddAlert(new Alert("Cannot delete the currently logged in user", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }

            Master.Redirect("~/users.aspx");
        }

        /// <summary>
        ///     Click handler for the Wipe User Data button
        /// </summary>
        private void WipeUser_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            foreach (Guid id in ids)
                try {
                    // Get the User object by its Id
                    User user = _userController.GetUserById(id);

                    // Delete the user data
                    _userController.DeleteUserData(id);
                    Master.AddAlert(new Alert("User data of user '" + user.Username + "' has been deleted", AlertType.Success));
                }
                catch (UserDoesNotExistException) {
                    Master.AddAlert(new Alert("User Id is not valid or user does not exist", AlertType.Error));
                }
                catch (InvalidRoleException) {
                    Master.AddAlert(new Alert("Cannot wipe admin/researcher data", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }

            Master.Redirect("~/users.aspx");
        }

        /// <summary>
        ///     Click handler for the Create User button
        /// </summary>
        private void CreateUser_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            try {
                // Create the User using the form data
                _userController.CreateUser(f_username.Text,
                                           f_password.Text,
                                           f_email.Text,
                                           f_administrator.Checked ? UserRole.Administrator : UserRole.Researcher);
                Master.AddAlert(new Alert("Succesfully created user '" + f_username.Text + "'", AlertType.Success));
                Master.Redirect("~/users.aspx");
            }
            catch (InvalidUsernameException) {
                Master.AddAlert(new Alert("Please make sure that the username is 5 characters or longer. You can only use letters and numbers in the username.",
                                          AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (UsernameUnavailableException) {
                Master.AddAlert(new Alert("This username is already in use.", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidInputException) {
                Master.AddAlert(new Alert("Please fill in all fields", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidEmailException) {
                Master.AddAlert(new Alert("Please fill in a valid email adress", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/users.aspx");
            }
        }

        /// <summary>
        ///     Click handler for the Edit User button
        /// </summary>
        private void EditUser_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            try {
                // Get the User from the database
                User user = _userController.GetUserById(ids[0]);

                // Set the new email address
                user.Email = f_email.Text;

                // Save the changes to the database
                _userController.EditUser(user);
                Master.AddAlert(new Alert("Successfully saved changes to user '" + f_username.Text + "'", AlertType.Success));
                Master.Redirect("~/users.aspx");
            }
            catch (UserDoesNotExistException) {
                Master.AddAlert(new Alert("User Id is not valid or user does not exist", AlertType.Error));
                Master.Redirect("~/users.aspx");
            }
            catch (InvalidInputException) {
                Master.AddAlert(new Alert("Please fill in all fields", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/users.aspx");
            }
        }

        /// <summary>
        /// Click handler for the Send Notification button
        /// </summary>
        private void NotifyUser_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            foreach (Guid userId in ids)
                try {
                    // Verify that the User exists and is not a administrator or researcher
                    User user = _userController.GetUserById(userId);
                    if (user.Role != UserRole.User)
                        throw new InvalidRoleException();

                    // Send the notification to the User
                    _userController.SendNotification(userId, notif_title.Text, notif_body.Text);
                    Master.AddAlert(new Alert("Notification has been sent to user '" + user.Username + "'", AlertType.Success));
                }
                catch (UserDoesNotExistException) {
                    Master.AddAlert(new Alert("User Id is not valid or user does not exist", AlertType.Error));
                }
                catch (InvalidRoleException) {
                    Master.AddAlert(new Alert("User '" + userId + "' is a researcher or admin, can't send a notification", AlertType.Error));
                }
                catch (NoFirebaseTokenException) {
                    Master.AddAlert(new Alert("No Firebase token associated with user '" + userId + "'", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }

            Master.Redirect("~/users.aspx");
        }

        /// <summary>
        /// Click handler for the Unassign User button
        /// </summary>
        private void Unassign_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            foreach (string key in Request.Form.AllKeys)
                try {
                    // Parse the User and Research Ids passed from the form
                    string[] ids = key.Split(':');
                    if (!Guid.TryParse(ids[0], out Guid researchId) || !Guid.TryParse(ids[1], out Guid userId))
                        continue;

                    // Unassign the User from the Research
                    _userController.UnassignUser(userId, researchId);
                    Master.AddAlert(new Alert("The user with id '" + userId + "' has been unassigned from the research with id '" + researchId + "'",
                                              AlertType.Success));
                }
                catch (UserDoesNotExistException) {
                    Master.AddAlert(new Alert("The user in key '" + key + "' does not exist", AlertType.Error));
                }
                catch (ResearchDoesNotExistException) {
                    Master.AddAlert(new Alert("The research in key '" + key + "' does not exist", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert("An unexpected exception occurred: " + ex.Message, AlertType.Error));
                }

            Master.Redirect("~/users.aspx");
        }
        #endregion
    }
}
