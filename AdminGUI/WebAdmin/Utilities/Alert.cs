// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Diagnostics.CodeAnalysis;

namespace WebAdmin.Utilities {
    // Exclude from code coverage as this class contains only HTML and a few strings
    [ExcludeFromCodeCoverage]
    public class Alert {
        #region "Constructors"
        /// <summary>
        /// Creates a new Alert object
        /// </summary>
        /// <param name="message">The message to show</param>
        /// <param name="type">The type of Alert</param>
        /// <param name="lifetime">When to remove the Alert</param>
        public Alert(string message, AlertType type, LifeTime lifetime = LifeTime.DeleteOnPageLoad) {
            Message = message;
            Type = type;
            Lifetime = lifetime;
        }
        #endregion

        #region "Enums"
        /// <summary>
        /// What kind of message the Alert is
        /// </summary>
        public enum AlertType {
            Success,
            Warning,
            Error,
            Info
        }

        /// <summary>
        /// When to remove the Alert from the Session
        /// </summary>
        public enum LifeTime {
            /// <summary>
            /// Delete the message on refreshing the current page or navigating to another page
            /// </summary>
            DeleteImmediately,
            /// <summary>
            /// Delete the message on navigating to another page
            /// </summary>
            DeleteOnPageLoad
        }
        #endregion

        #region "Properties"
        /// <summary>
        /// When to remove this Alert
        /// </summary>
        public LifeTime Lifetime { get; }
        /// <summary>
        /// The message of this Alert
        /// </summary>
        public string Message { get; }
        /// <summary>
        /// The type of this Alert
        /// </summary>
        public AlertType Type { get; }
        #endregion

        #region "Functionality"
        /// <summary>
        /// Parses an Alert to HTML
        /// </summary>
        /// <returns>HTML code for the message</returns>
        public string Print() => "<div runat=\"server\" class=\"alert " + AlertTypeToCss(Type) + " alert-dismissible fade show\" role=\"alert\">" +
                                 "<strong>" + AlertTypeToTitle(Type) + "<div style=\"overflow:auto\"> </strong><span runat=\"server\">" + Message +
                                 "</span></div>" +
                                 "<button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span>&times;</span></button></div>";

        /// <summary>
        /// Converts an AlertType to the matching CSS class
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string AlertTypeToCss(AlertType type) {
            switch (type) {
                case AlertType.Error:
                    return "alert-danger";
                case AlertType.Warning:
                    return "alert-warning";
                case AlertType.Success:
                    return "alert-success";
                case AlertType.Info:
                    return "alert-info";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Converts an AlertType to a prefix for the message
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string AlertTypeToTitle(AlertType type) {
            switch (type) {
                case AlertType.Error:
                    return "Error: ";
                case AlertType.Warning:
                    return "Warning: ";
                case AlertType.Success:
                    return "Success: ";
                case AlertType.Info:
                    return "Info: ";
                default:
                    return "";
            }
        }
        #endregion
    }
}
