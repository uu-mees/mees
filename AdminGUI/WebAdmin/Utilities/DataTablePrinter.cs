// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace WebAdmin.Utilities {
    // Exclude from code coverage as this class only generates 'untestable' HTML
    [ExcludeFromCodeCoverage]
    public static class DataTablePrinter {
        /// <summary>
        ///     Generate HTML for displaying a generic table
        /// </summary>
        /// <param name="table">The table retrieved from the database</param>
        /// <returns>An HTML-formatted table</returns>
        public static string Print(DataTable table) {
            if (table.Columns.Count == 0)
                return "EmptyTable";

            StringBuilder html = new StringBuilder();
            html.AppendLine("<table class=\"table table-striped mees-table-border\">");
            // Print the header
            PrintHeader(table, ref html);
            // Start the table body and print all rows
            html.AppendLine("<tbody>");
            for (int i = 0; i < table.Rows.Count; i++) PrintRow(table.Rows[i], ref html);
            html.AppendLine("</tbody>");
            html.AppendLine("</table>");

            return html.ToString();
        }

        /// <summary>
        ///     Print the column names as an HTML table header
        /// </summary>
        /// <param name="table">The table retrieved from the database</param>
        /// <param name="html">The StringBuilder being used for generating the HTML</param>
        private static void PrintHeader(DataTable table, ref StringBuilder html) {
            // Start the thead element
            html.AppendLine("<thead class=\"mees-table-header\">");
            // Start a new row
            html.AppendLine("<tr>");
            for (int i = 0; i < table.Columns.Count; i++) // Print column
                html.AppendLine("<th scope=\"col\">" + table.Columns[i].ColumnName + "</th>");
            // Close the row
            html.AppendLine("</tr>");
            // Close the thead element
            html.AppendLine("</thead>");
        }

        /// <summary>
        ///     Prints a given row as an HTML table row
        /// </summary>
        /// <param name="row">The DataRow to print</param>
        /// <param name="html">The StringBuilder being used for generating the HTML</param>
        private static void PrintRow(DataRow row, ref StringBuilder html) {
            // Start a new row
            html.AppendLine("<tr>");
            // Print first field as a th element
            html.AppendLine("<th scope=\"row\">" + row[0] + "</th>");
            // Print other fields as td elements
            for (int i = 1; i < row.ItemArray.Length; i++) html.AppendLine("<td>" + row[i] + "</td>");
            // Close the row
            html.AppendLine("</tr>");
        }
    }
}
