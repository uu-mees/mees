// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Diagnostics.CodeAnalysis;

namespace WebAdmin.Utilities {
    //Excluded from code coverage as these are all exceptions for errors that might occur in the program.

    #region "Users"
    [ExcludeFromCodeCoverage]
    public class UserDoesNotExistException : Exception {
        public UserDoesNotExistException() { }
        public UserDoesNotExistException(string message) : base(message) { }
        public UserDoesNotExistException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class UsernameUnavailableException : Exception {
        public UsernameUnavailableException() { }
        public UsernameUnavailableException(string message) : base(message) { }
        public UsernameUnavailableException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidInputException : Exception {
        public InvalidInputException() { }
        public InvalidInputException(string message) : base(message) { }
        public InvalidInputException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidEmailException : Exception {
        public InvalidEmailException() { }
        public InvalidEmailException(string message) : base(message) { }
        public InvalidEmailException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidUsernameException : Exception {
        public InvalidUsernameException() { }
        public InvalidUsernameException(string message) : base(message) { }
        public InvalidUsernameException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidRoleException : Exception {
        public InvalidRoleException() { }
        public InvalidRoleException(string message) : base(message) { }
        public InvalidRoleException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class CurrentUserException : Exception {
        public CurrentUserException() { }
        public CurrentUserException(string message) : base(message) { }
        public CurrentUserException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class IllegalChangeException : Exception {
        public IllegalChangeException() { }
        public IllegalChangeException(string message) : base(message) { }
        public IllegalChangeException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class NoFirebaseTokenException : Exception {
        public NoFirebaseTokenException() { }
        public NoFirebaseTokenException(string message) : base(message) { }
        public NoFirebaseTokenException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    #region "Queries"
    [ExcludeFromCodeCoverage]
    public class InvalidQueryLengthException : Exception {
        public InvalidQueryLengthException() { }
        public InvalidQueryLengthException(string message) : base(message) { }
        public InvalidQueryLengthException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidQueryResultException : Exception {
        public InvalidQueryResultException() { }
        public InvalidQueryResultException(string message) : base(message) { }
        public InvalidQueryResultException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidQueryException : Exception {
        public InvalidQueryException() { }
        public InvalidQueryException(string message) : base(message) { }
        public InvalidQueryException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    #region "DataSources"
    [ExcludeFromCodeCoverage]
    public class DuplicateDatasourceException : Exception {
        public DuplicateDatasourceException() { }
        public DuplicateDatasourceException(string message) : base(message) { }
        public DuplicateDatasourceException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class DatasourceDoesNotExistException : Exception {
        public DatasourceDoesNotExistException() { }
        public DatasourceDoesNotExistException(string message) : base(message) { }
        public DatasourceDoesNotExistException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class DatasourceUnchangedException : Exception {
        public DatasourceUnchangedException() { }
        public DatasourceUnchangedException(string message) : base(message) { }
        public DatasourceUnchangedException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    #region "ResetTokens"
    [ExcludeFromCodeCoverage]
    public class TokenDoesNotExistException : Exception {
        public TokenDoesNotExistException() { }
        public TokenDoesNotExistException(string message) : base(message) { }
        public TokenDoesNotExistException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    #region "Research"
    // Exclude from code coverage as these simply implement Exception under a different name
    [ExcludeFromCodeCoverage]
    public class DuplicateResearchException : Exception {
        public DuplicateResearchException() { }
        public DuplicateResearchException(string message) : base(message) { }
        public DuplicateResearchException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class ResearchDoesNotExistException : Exception {
        public ResearchDoesNotExistException() { }
        public ResearchDoesNotExistException(string message) : base(message) { }
        public ResearchDoesNotExistException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidResearchEndException : Exception {
        public InvalidResearchEndException() { }
        public InvalidResearchEndException(string message) : base(message) { }
        public InvalidResearchEndException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidResearchLengthException : Exception {
        public InvalidResearchLengthException() { }
        public InvalidResearchLengthException(string message) : base(message) { }
        public InvalidResearchLengthException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidDatasourceException : Exception {
        public InvalidDatasourceException() { }
        public InvalidDatasourceException(string message) : base(message) { }
        public InvalidDatasourceException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class AssignedUserException : Exception {
        public AssignedUserException() { }
        public AssignedUserException(string message) : base(message) { }
        public AssignedUserException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion

    #region "Authentication"
    [ExcludeFromCodeCoverage]
    public class InvalidCredentialsException : Exception {
        public InvalidCredentialsException() { }
        public InvalidCredentialsException(string message) : base(message) { }
        public InvalidCredentialsException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class DuplicateTokenException : Exception {
        public DuplicateTokenException() { }
        public DuplicateTokenException(string message) : base(message) { }
        public DuplicateTokenException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class PasswordsDoNotMatchException : Exception {
        public PasswordsDoNotMatchException() { }
        public PasswordsDoNotMatchException(string message) : base(message) { }
        public PasswordsDoNotMatchException(string message, Exception inner) : base(message, inner) { }
    }

    [ExcludeFromCodeCoverage]
    public class InvalidTokenException : Exception {
        public InvalidTokenException() { }
        public InvalidTokenException(string message) : base(message) { }
        public InvalidTokenException(string message, Exception inner) : base(message, inner) { }
    }
    #endregion
}
