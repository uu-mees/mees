<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="datasources.aspx.cs" Inherits="WebAdmin.DataSourcesPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="DataSources" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page">
        <div class="mees-title-container">
            <div>
                <h3 class="mees-page-title">Manage Data Sources</h3>
            </div>
            <div class="mees-title-button">
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="CreateSource" ID="button_create" class="btn btn-success">Add Data Source</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="EditSource" ID="button_edit" class="btn btn-info disabled">Edit Data Source</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="EnableSource" ID="button_enable" class="btn btn-warning disabled">Enable Data Source</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="DisableSource" ID="button_disable" class="btn btn-warning disabled">Disable Data Source</asp:LinkButton>
                <asp:LinkButton runat="server" ClientIDMode="Static" OnClick="DeleteSource" ID="button_delete" class="btn btn-danger disabled">Delete Data Source</asp:LinkButton>
            </div>
        </div>
        <div style="overflow: auto">
            <table id="sources" class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th scope="col" style="text-align: center; vertical-align: middle; width: 30px;">
                            <input type="checkbox" id="selectall" />
                        </th>
                        <th scope="col" style="min-width: 200px;">Url</th>
                        <th scope="col" style="min-width: 200px;">Active</th>
                        <th scope="col" style="min-width: 200px;">Type</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Print data rows here -->
                    <asp:Literal runat="server" ID="DataSourceRows" />
                </tbody>
            </table>
        </div>
        <script type="text/javascript">
            FixTableAlignment();
            ClearCheckboxes();
        </script>
    </div>
</asp:Content>
