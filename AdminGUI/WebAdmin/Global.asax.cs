// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using static WebAdmin.DataTypes.User;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public class Global : HttpApplication {
        protected void Application_Start(object sender, EventArgs e) {
            // Create an admin account if it does not exist
            string connectionString = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
            SqlServerDatabase database = new SqlServerDatabase(connectionString);
            UserController userController = new UserController(database, new FirebaseNotificationProvider());

            if (userController.GetUsers().All(x => x.Role != UserRole.Administrator))
                userController.CreateUser("admin", "admin", "please@change.password", UserRole.Administrator);
        }

        protected void Session_Start(object sender, EventArgs e) { }
        protected void Application_BeginRequest(object sender, EventArgs e) { }
        protected void Application_AuthenticateRequest(object sender, EventArgs e) { }
        protected void Application_Error(object sender, EventArgs e) { }
        protected void Session_End(object sender, EventArgs e) { }
        protected void Application_End(object sender, EventArgs e) { }
    }
}
