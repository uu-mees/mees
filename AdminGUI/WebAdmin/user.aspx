<%-- This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
     ©Copyright Utrecht University(Department of Information and Computing Sciences) --%>

<%@ Page Language="C#" MasterPageFile="~/MasterPages/MeesPage.master" AutoEventWireup="true" CodeBehind="user.aspx.cs" Inherits="WebAdmin.UserPage" %>

<%@ MasterType VirtualPath="~/MasterPages/MeesPage.master" %>

<asp:Content ID="User" ContentPlaceHolderID="content" runat="Server">
    <div class="mees-page-small">
        <!-- Header info -->
        <h3 class="mees-page-title" runat="server" id="form_header"></h3>
        <p runat="server" id="form_description"></p>

        <!-- Form input -->
        <div id="form_input" runat="server" visible="false">
            <p>Username:</p>
            <div class="form-group">
                <asp:TextBox ID="f_username" MaxLength="50" runat="server" CssClass="form-control input-lg" placeholder="Username" required="True"></asp:TextBox>
            </div>
            <p>Password:</p>
            <div class="form-group">
                <asp:TextBox ID="f_password" MaxLength="50" runat="server" TextMode="Password" CssClass="form-control input-lg" placeholder="Password" required="True"></asp:TextBox>
            </div>
            <p>Email Address:</p>
            <div class="form-group">
                <asp:TextBox ID="f_email" MaxLength="100" runat="server" TextMode="email" CssClass="form-control input-lg" placeholder="Email Address" required="True"></asp:TextBox>
            </div>
            <div id="roleRadioButtons" runat="server">
                <p>Account type:</p>
                <div class="form-group">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-info mees-radio active">
                            <asp:RadioButton runat="server" type="radio" ID="f_administrator" GroupName="f_type" Checked="true" />
                            Administrator
                        </label>
                        <label class="btn btn-info mees-radio">
                            <asp:RadioButton runat="server" type="radio" ID="f_researcher" GroupName="f_type" />
                            Researcher
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <!-- Table -->
        <div runat="server" id="userTable" visible="false">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Role</th>
                    </tr>
                </thead>
                <asp:Literal runat="server" ID="userTableContent"></asp:Literal>
            </table>
        </div>

        <!-- Table - unassign from Research -->
        <div runat="server" id="unassignTable" visible="False">
            <table class="table table-striped mees-table-border">
                <thead class="mees-table-header">
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Research name</th>
                        <th>Research ID</th>
                    </tr>
                </thead>
                <asp:Literal runat="server" ID="unassignTableContent"></asp:Literal>
            </table>
        </div>

        <!-- Notification input -->
        <div id="notification_input" runat="server" visible="false">
            <p>Title:</p>
            <div class="form-group">
                <asp:TextBox ID="notif_title" MaxLength="64" runat="server" CssClass="form-control input-lg" placeholder="Insert the title" required="True"></asp:TextBox>
            </div>
            <p>Body:</p>
            <div class="form-group">
                <asp:TextBox ID="notif_body" MaxLength="960" runat="server" CssClass="form-control input-lg" placeholder="Insert the body" required="True"></asp:TextBox>
            </div>
        </div>

        <!-- Buttons -->
        <a href="users.aspx" class="btn btn-danger mees-align-right">Cancel</a>
        <asp:Button runat="server" ID="button_confirm" CssClass="btn btn-success mees-align-right" />
    </div>
</asp:Content>
