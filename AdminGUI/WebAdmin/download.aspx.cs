// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Configuration;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.UI;
using Jitbit.Utils;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.Utilities.Alert;
using static WebAdmin.DataTypes.User;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class DownloadPage : Page {
        private readonly QueryController _queryController;

        protected DownloadPage() =>
            _queryController = new QueryController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        protected void Page_Load(object sender, EventArgs e) {
            // Check that the logged in user has permission to view this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator && sessionRole != UserRole.Researcher) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Read the query and desired filename from the session
            string query = (string)Session["downloadquery"];
            string filename = (string)Session["downloadfilename"];
            if (string.IsNullOrWhiteSpace(filename))
                filename = "mees_export.csv";
            string redirect = (string)Session["downloadredirect"];
            if (string.IsNullOrWhiteSpace(redirect))
                redirect = "~/query.aspx";

            // Clear the session variables
            Session["downloadfilename"] = "";
            Session["downloadquery"] = "";

            // Stop if the query has not been set
            if (string.IsNullOrWhiteSpace(query)) {
                Master.AddAlert(new Alert("Cannot download file", AlertType.Error));
                Master.Redirect(redirect);
                return;
            }

            // Initialize return types
            string message = null;
            DataTable table = null;

            // Execute the query
            try {
                if (!query.ToLower().StartsWith("select"))
                    throw new InvalidQueryException("This query is not a SELECT query, so its result cannot be downloaded");
                _queryController.ExecuteQuery(query, out message, out table);
            }
            catch (InvalidQueryLengthException) {
                // The query is too short to be valid
                Master.AddAlert(new Alert("This query is not valid", AlertType.Error));
                Master.Redirect(redirect);
            }
            catch (InvalidQueryResultException) {
                // The query returned a negative number of rows changed
                Master.AddAlert(new Alert("An error occurred while executing this query", AlertType.Error));
                Master.Redirect(redirect);
            }
            catch (InvalidQueryException ex) {
                // There is some error in the query
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect(redirect);
            }

            if (table != null) {
                // Setup response stream
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.AddHeader("Pragma", "public");

                // Convert the DataTable to CSV
                CsvExport csv = new CsvExport();
                for (int row = 0; row < table.Rows.Count; row++) {
                    csv.AddRow();
                    for (int col = 0; col < table.Columns.Count; col++) csv[table.Columns[col].ColumnName] = table.Rows[row][col].ToString();
                }

                // Write generated file to the response stream and end the connection               
                HttpContext.Current.Response.Write(csv.Export());
                Response.Flush();
                Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (message != null) {
                Master.AddAlert(new Alert("This query does not return a result. However, it was still executed: " + message, AlertType.Warning));
                Master.Redirect(redirect);
            }
        }
    }
}
