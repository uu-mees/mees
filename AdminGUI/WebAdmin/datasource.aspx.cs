// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Web.UI;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.DataSource;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class DataSourcePage : Page {
        private readonly DataSourceController _dataSourceController;

        protected DataSourcePage() => _dataSourceController =
                                          new DataSourceController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        #region "Form Setup"
        /// <summary>
        ///     This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Check that the logged in user has permission to view this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Setup input fields based on the requested task (set header/description, fill and show selected sources table, set button text and click event, etc.)
            string task = Request.Params["task"];
            switch (task) {
                case "create":
                    form_header.InnerText = "Add a new Data Source";
                    form_input.Visible = true;
                    button_confirm.Text = "Add Data Source";
                    if (!f_raster.Checked && !f_sensor.Checked) {
                        f_raster.Checked = true;
                    }
                    f_inactive.Checked = true;
                    AddButtonClickHandler(AddSource_Click);
                    break;

                case "edit":
                    form_header.InnerText = "Edit Data Source";
                    form_input.Visible = true;
                    button_confirm.Text = "Save Changes";
                    if (!IsPostBack)
                        FillFormFromExistingDataSource();
                    AddButtonClickHandler(EditSource_Click);
                    break;

                case "delete":
                    form_header.InnerText = "Remove Data Sources";
                    form_description.InnerText = "Are you sure you want to remove the following data sources?";
                    dataSourceTable.Visible = true;
                    dataSourceTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Remove Data Sources";
                    AddButtonClickHandler(RemoveSource_Click);
                    break;

                case "enable":
                    form_header.InnerText = "Enable Data Sources";
                    form_description.InnerText = "Are you sure you want to enable the following data sources?";
                    dataSourceTable.Visible = true;
                    dataSourceTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Enable Data Sources";
                    AddButtonClickHandler(EnableSource_Click);
                    break;

                case "disable":
                    form_header.InnerText = "Disable Data Sources";
                    form_description.InnerText = "Are you sure you want to disable the following data sources?";
                    dataSourceTable.Visible = true;
                    dataSourceTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Disable Data Sources";
                    AddButtonClickHandler(DisableSource_Click);
                    break;

                default:
                    // No task has been set; return the user to the datasources page
                    Master.AddAlert(new Alert("DataSource.aspx: no task set", AlertType.Error));
                    Master.Redirect("~/datasources.aspx");
                    break;
            }
        }

        /// <summary>
        ///     Binds a method to the button click event
        /// </summary>
        /// <param name="onClick">The method to execute on a button click</param>
        private void AddButtonClickHandler(Action<object, EventArgs> onClick) {
            void Handler(object obj, EventArgs args) => onClick(obj, args);
            button_confirm.Click += Handler;
        }
        #endregion

        #region "Data Loading"
        /// <summary>
        ///     Fills the form fields using data of a DataSource in the database
        /// </summary>
        private void FillFormFromExistingDataSource() {
            try {
                // Load the selected DataSources list from the session
                Guid parsedId = Guid.Parse((string)Session["sourcelist"]);

                // Load the selected DataSource from the database
                DataSource dataSource = _dataSourceController.GetDataSourceById(parsedId);

                // Throw an exception if no such DataSource exists
                if (dataSource == null)
                    throw new DatasourceDoesNotExistException();

                // Set form values from the retrieved DataSource
                f_url.Text = dataSource.Url;
                f_raster.Checked = dataSource.Type == DataSourceType.Raster;
                f_sensor.Checked = !f_raster.Checked;
                f_active.Checked = dataSource.Active;
                f_inactive.Checked = !f_active.Checked;
            }
            catch {
                Master.AddAlert(new Alert("Invalid Data Source Id", AlertType.Error));
                Master.Redirect("~/datasources.aspx");
            }
        }

        /// <summary>
        ///     Parses the passed DataSource Ids to a table containing their fields
        /// </summary>
        /// <returns>The DataSources table as HTML</returns>
        private string FillTableFromSession() {
            try {
                // Load the selected DataSources list from the session
                List<Guid> parsedIds = ((string)Session["sourcelist"]).Split(';').Select(Guid.Parse).ToList();

                StringBuilder result = new StringBuilder();
                // Get each DataSource from the database and add it to the table
                foreach (Guid id in parsedIds) {
                    DataSource dataSource = _dataSourceController.GetDataSourceById(id);
                    result.AppendLine("<tr>");
                    result.AppendLine("<td>" + dataSource.Id + "</td>");
                    result.AppendLine("<td>" + dataSource.Url + "</td>");
                    result.AppendLine("<td>" + (dataSource.Active ? "Yes" : "No") + "</td>");
                    result.AppendLine("<td>" + (dataSource.Type == DataSourceType.Raster ? "Raster" : "Sensor") + "</td>");
                    result.AppendLine("</tr>");
                }
                return result.ToString();
            }
            catch {
                Master.AddAlert(new Alert("No data source(s) specified", AlertType.Error));
                Master.Redirect("~/datasources.aspx");
                return null;
            }
        }
        #endregion

        #region "Button Click Handlers"
        /// <summary>
        ///     Parses the session variable to a list of Ids
        /// </summary>
        /// <returns>A List of Guids parsed</returns>
        private List<Guid> ParseSessionIds() {
            // Verify the users session is still active
            if (!Master.VerifyAuthentication())
                return null;

            // Parse the Guid(s) stored in the session
            try {
                string sourcelist = (string)Session["sourcelist"];
                return sourcelist.Contains(";") ? sourcelist.Split(';').Select(Guid.Parse).ToList() : new List<Guid> { Guid.Parse(sourcelist) };
            }
            catch {
                Master.AddAlert(new Alert("No data source(s) specified", AlertType.Error));
                return null;
            }
        }

        /// <summary>
        ///     Adds a DataSource to the database on clicking the button
        /// </summary>
        private void AddSource_Click(object sender, EventArgs e) {
            // Validate the users session
            if (!Master.VerifyAuthentication())
                return;

            // Retrieve input from the form
            string url = f_url.Text;
            DataSourceType type = f_sensor.Checked ? DataSourceType.Sensor : DataSourceType.Raster;
            bool active = f_active.Checked ? true : false;

            try {
                // Add a new source from the data collected
                _dataSourceController.AddDataSource(url, type, active);
                Master.AddAlert(new Alert("Added source '" + url + "'", AlertType.Success));
                Master.Redirect("~/datasources.aspx");
            }
            catch (InvalidDatasourceException) {
                // The Url is not valid (e.g. not a valid XML sensor file)
                Master.AddAlert(new Alert("Please make sure the Url is valid", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (DuplicateDatasourceException) {
                // The Url already exists
                Master.AddAlert(new Alert("This Url has already been added", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/datasources.aspx");
            }
        }

        /// <summary>
        ///     Pushes the changes made to the DataSource into the database
        /// </summary>
        private void EditSource_Click(object sender, EventArgs e) {
            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            try {
                // Parse the form input
                DataSourceType type = f_sensor.Checked ? DataSourceType.Sensor : DataSourceType.Raster;

                // Update the source using the Id parsed from the session
                _dataSourceController.EditDataSource(new DataSource(ids[0], f_url.Text, f_active.Checked, type));
                Master.AddAlert(new Alert("Data Source with Id '" + ids[0] + "' has been updated", AlertType.Success));
                Master.Redirect("~/datasources.aspx");
            }
            catch (InvalidDatasourceException) {
                Master.AddAlert(new Alert("Please make sure the Url is valid", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (DuplicateDatasourceException) {
                Master.AddAlert(new Alert("A Data Source with the url '" + f_url.Text + "' already exists", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (DatasourceDoesNotExistException) {
                Master.AddAlert(new Alert("Data Source with Id '" + ids[0] + "' does not exist", AlertType.Error));
                Master.Redirect("~/datasources.aspx");
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/datasources.aspx");
            }
        }

        /// <summary>
        ///     Deletes a DataSource from the database
        /// </summary>
        private void RemoveSource_Click(object sender, EventArgs e) {
            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            // Loop over Ids and delete them
            foreach (Guid id in ids) {
                DataSource dataSource = _dataSourceController.GetDataSourceById(id);
                try {
                    _dataSourceController.RemoveDataSource(id);
                    Master.AddAlert(new Alert("Data source '" + dataSource.Url + "' has been deleted", AlertType.Success));
                }
                catch (DatasourceDoesNotExistException) {
                    Master.AddAlert(new Alert("Data source Id is not valid or data source does not exist", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }
            }

            Master.Redirect("~/datasources.aspx");
        }

        /// <summary>
        ///     Enables the collection of data from a DataSource
        /// </summary>
        private void EnableSource_Click(object sender, EventArgs e) {
            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            // Loop over sources and enable all of them
            foreach (Guid id in ids) {
                DataSource dataSource = _dataSourceController.GetDataSourceById(id);
                try {
                    if (dataSource.Active)
                        throw new DatasourceUnchangedException();
                    _dataSourceController.EnableDataSource(dataSource.Id);
                    Master.AddAlert(new Alert("Successfully enabled data source '" + dataSource.Url + "'", AlertType.Success));
                }
                catch (DatasourceDoesNotExistException) {
                    Master.AddAlert(new Alert("Data source '" + dataSource.Id + "' does not exist", AlertType.Error));
                }
                catch (DatasourceUnchangedException) {
                    Master.AddAlert(new Alert("Data source '" + dataSource.Url + "' is already enabled", AlertType.Warning));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }
            }

            Master.Redirect("~/datasources.aspx");
        }

        /// <summary>
        ///     Disables the collection of data from a DataSource
        /// </summary>
        private void DisableSource_Click(object sender, EventArgs e) {
            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            // Loop over sources and disable them
            foreach (Guid id in ParseSessionIds()) {
                DataSource dataSource = _dataSourceController.GetDataSourceById(id);
                try {

                    // Throw an exception if the source is already inactive
                    if (!dataSource.Active)
                        throw new DatasourceUnchangedException();

                    _dataSourceController.DisableDataSource(dataSource.Id);
                    Master.AddAlert(new Alert("Successfully disabled data source '" + dataSource.Url + "'", AlertType.Success));
                }
                catch (DatasourceDoesNotExistException) {
                    Master.AddAlert(new Alert("Data source '" + id + "' does not exist", AlertType.Error));
                }
                catch (DatasourceUnchangedException) {
                    Master.AddAlert(new Alert("Data source '" + dataSource.Url + "' is already disabled", AlertType.Warning));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }
            }
            // Return to the DataSources page
            Master.Redirect("~/datasources.aspx");
        }
        #endregion
    }
}
