/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;
using static WebAdmin.Utilities.Alert;

namespace WebAdmin {
    [ExcludeFromCodeCoverage]
    public partial class ResearchPage : System.Web.UI.Page {
        private readonly ResearchController _researchController;
        protected ResearchPage() => _researchController = new ResearchController(new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString));

        #region "Form Setup"
        /// <summary>
        /// This will be executed on loading the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e) {
            // Verify that the user is logged in and is allowed to access this page
            UserRole? sessionRole = Master.GetRoleFromCookie();
            if (sessionRole != UserRole.Administrator) {
                Master.Redirect("~/overview.aspx");
                return;
            }

            // Setup form based on the selected task
            string task = Request.Params["task"];
            switch (task) {
                case "create":
                    form_header.InnerText = "Create a new Research";
                    form_input.Visible = true;
                    button_confirm.Text = "Add Research";
                    AddButtonClickHandler(CreateResearch_Click);
                    // Set default date values
                    if (!IsPostBack) {
                        f_startdate.SelectedDate = DateTime.Today.AddDays(1);
                        f_enddate.SelectedDate = DateTime.Today.AddDays(8);
                        f_enddate.VisibleDate = f_enddate.SelectedDate;
                    }
                    break;
                case "edit":
                    form_header.InnerText = "Edit Research";
                    form_input.Visible = true;
                    button_confirm.Text = "Save Changes";
                    if (!IsPostBack)
                        FillFormFromExistingResearch();
                    AddButtonClickHandler(EditResearch_Click);
                    break;
                case "delete":
                    form_header.InnerText = "Delete Research";
                    form_description.InnerText = "Are you sure you want to remove the following reseaches?";
                    researchTable.Visible = true;
                    researchTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "Delete Research";
                    AddButtonClickHandler(RemoveResearch_Click);
                    break;
                case "end":
                    form_header.InnerText = "End Research";
                    form_description.InnerText = "Are you sure you want to end the following reseaches?";
                    researchTable.Visible = true;
                    researchTableContent.Text = FillTableFromSession();
                    button_confirm.Text = "End Research";
                    AddButtonClickHandler(EndResearch_Click);
                    break;
                default:
                    Master.AddAlert(new Alert("Research.aspx: no task set", AlertType.Error));
                    Master.Redirect("~/researches.aspx");
                    break;
            }
        }

        /// <summary>
        ///     Binds a method to the button click event
        /// </summary>
        /// <param name="onClick">The method to execute on a button click</param>
        private void AddButtonClickHandler(Action<object, EventArgs> onClick) {
            void Handler(object obj, EventArgs args) => onClick(obj, args);
            button_confirm.Click += Handler;
        }
        #endregion

        #region "Data Loading"
        /// <summary>
        /// Fills in the form with data from a existing Research
        /// </summary>
        private void FillFormFromExistingResearch() {
            try {
                // Load the selected Researches list from the session
                Guid parsedId = Guid.Parse((string)Session["researchlist"]);

                // Load the selected Research from the database
                Research research = _researchController.GetResearchById(parsedId);

                // Throw an exception if no such Research exists
                if (research == null)
                    throw new ResearchDoesNotExistException();

                // Set form values from the retrieved Research
                f_name.Text = research.Name;
                f_description.Text = research.Description;
                f_startdate.SelectedDate = research.StartDate.Date;

                // If the research has started, do not allow a restart
                if (DateTime.UtcNow > research.StartDate)
                    f_startdate.Enabled = false;
                if (DateTime.UtcNow > research.EndDate)
                    f_enddate.Enabled = false;

                // Set the permissions
                f_enddate.SelectedDate = research.EndDate.Date;
                f_enddate.VisibleDate = research.EndDate;
                f_permissions.Items[0].Selected = research.Permission.ShowRatings;
                f_permissions.Items[1].Selected = research.Permission.ShowSensors;
                f_permissions.Items[2].Selected = research.Permission.ShowRaster;
                f_permissions.Items[3].Selected = research.Permission.ShowExposure;
                f_permissions.Items[4].Selected = research.Permission.ShowPersonalScores;
                // f_permissions.Items[5].Selected = research.Permissions.ShowHighScores;
                f_permissions.Items[5].Selected = research.Permission.ShowRoutePlanner;
            }
            catch {
                Master.AddAlert(new Alert("No research(es) specified", AlertType.Error));
                Master.Redirect("~/researches.aspx");
            }
        }


        /// <summary>
        /// Parses the passed Research Ids to a table containing their fields
        /// </summary>
        /// <returns>The Researches table as HTML</returns>
        private string FillTableFromSession() {
            try {
                // Load the selected Researches list from the session
                List<Guid> parsedIds = ((string)Session["researchlist"]).Split(';').Select(Guid.Parse).ToList();

                StringBuilder result = new StringBuilder();
                // Get each Research from the database and add it to the table
                foreach (Guid id in parsedIds) {
                    Research research = _researchController.GetResearchById(id);
                    result.AppendLine("<tr>");
                    result.AppendLine("<td>" + research.Id + "</td>");
                    result.AppendLine("<td>" + research.Name + "</td>");
                    result.AppendLine("<td>" + research.StartDate + "</td>");
                    result.AppendLine("<td>" + research.EndDate + "</td>");
                    result.AppendLine("</tr>");
                }
                return result.ToString();
            }
            catch {
                Master.AddAlert(new Alert("No research(es) specified", AlertType.Error));
                Master.Redirect("~/researches.aspx");
                return null;
            }
        }
        #endregion

        #region "Button Click Handlers"
        /// <summary>
        ///     Parses the session variable to a list of Ids
        /// </summary>
        /// <returns>A List of parsed Guids</returns>
        private List<Guid> ParseSessionIds() {
            // Verify the users session is still active
            if (!Master.VerifyAuthentication())
                return null;

            // Parse the Guid(s) stored in the session
            try {
                string researchlist = (string)Session["researchlist"];
                return researchlist.Contains(";") ? researchlist.Split(';').Select(Guid.Parse).ToList() : new List<Guid> { Guid.Parse(researchlist) };
            }
            catch {
                Master.AddAlert(new Alert("No research(es) specified", AlertType.Error));
                return null;
            }
        }

        /// <summary>
        /// Click handler for the Add Research button
        /// </summary>
        private void CreateResearch_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Retrieve input from the form
            string name = f_name.Text;
            string desc = f_description.Text;
            DateTime startdate = f_startdate.SelectedDate;
            DateTime enddate = f_enddate.SelectedDate;
            bool[] permissions = new bool[6];
            for (int i = 0; i < 6; i++)
                permissions[i] = f_permissions.Items[i].Selected;
            PermissionList permissionList = new PermissionList(permissions);

            try {
                // Create the research using the variables read from the form
                _researchController.CreateResearch(name, desc, permissionList, startdate, enddate);
                Master.AddAlert(new Alert("Added Research '" + name + "'", AlertType.Success));
                Master.Redirect("~/researches.aspx");
            }
            catch (InvalidInputException) {
                // Not all fields have input
                Master.AddAlert(new Alert("Please make sure all fields have been filled in", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (DuplicateResearchException) {
                // Research name is already in use
                Master.AddAlert(new Alert("This name is already in use", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidResearchLengthException) {
                // Research duration is negative
                Master.AddAlert(new Alert("Please ensure that the research runs for a positive amount of time", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidResearchEndException) {
                //the research end is set to a date before today
                Master.AddAlert(new Alert("Please make sure the end date of this research is in the future", AlertType.Error));
                Master.Refresh();
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/researches.aspx");
            }
        }

        /// <summary>
        /// Click handler for the Edit Research button
        /// </summary>
        private void EditResearch_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Retrieve input from the form
            string name = f_name.Text;
            string desc = f_description.Text;
            DateTime startdate = f_startdate.SelectedDate;
            DateTime enddate = f_enddate.SelectedDate;
            bool[] permissions = new bool[6];
            for (int i = 0; i < 6; i++)
                permissions[i] = f_permissions.Items[i].Selected;
            PermissionList permissionList = new PermissionList(permissions);

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            try {
                // Retrieve the Research to edit
                Research research = _researchController.GetResearchById(ids[0]);

                // Update the research using the Id and form data
                _researchController.EditResearch(new Research(research.Id, name, desc, permissionList, startdate, enddate));
                Master.AddAlert(new Alert("The Research '" + name + "' has been successfully updated", AlertType.Success));
                Master.Redirect("~/researches.aspx");
            }
            catch (ResearchDoesNotExistException) {
                // The Research being edited does not exist
                Master.AddAlert(new Alert("Research '" + name + "' does not exist", AlertType.Error));
                Master.Redirect("~/researches.aspx");
            }
            catch (DuplicateResearchException) {
                // The name is already in use
                Master.AddAlert(new Alert("This name is already in use, please pick a different one", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidResearchLengthException) {
                // The research has a negative duration
                Master.AddAlert(new Alert("Please make sure the research runs for a positive amount of time", AlertType.Error, LifeTime.DeleteImmediately));
                Master.Refresh();
            }
            catch (InvalidResearchEndException) {
                //the research end is set to a date before today
                Master.AddAlert(new Alert("To end a research, please use the end research button on the research page", AlertType.Error));
                Master.Refresh();
            }
            catch (Exception ex) {
                Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                Master.Redirect("~/researches.aspx");
            }
        }

        /// <summary>
        /// Click handler for the Remove Research button
        /// </summary>
        private void RemoveResearch_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            // Loop over these Ids
            foreach (Guid id in ids) {
                // Get the corresponding Research object
                Research research = _researchController.GetResearchById(id);
                try {
                    // Try to delete this Research
                    _researchController.RemoveResearch(id);
                    Master.AddAlert(new Alert("Research '" + research.Name + "' has been deleted", AlertType.Success));
                }
                catch (IllegalChangeException) {
                    //The research being deleted has not yet ended, so it cannot be deleted.
                    Master.AddAlert(new Alert("Research '" + research.Name + "' is ongoing, please end it before deleting", AlertType.Error));
                }
                catch (ResearchDoesNotExistException) {
                    //The research being deleted does not exist.
                    Master.AddAlert(new Alert("Research Id is not valid or research does not exist", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }
            }
            Master.Redirect("~/researches.aspx");
        }

        /// <summary>
        /// Click handler for the End Research Button
        /// </summary>
        private void EndResearch_Click(object sender, EventArgs e) {
            // Verify that the user is allowed to access this page
            if (!Master.VerifyAuthentication())
                return;

            // Get the Ids from the session
            List<Guid> ids = ParseSessionIds();
            if (ids == null)
                return;

            // Loop over IDs
            foreach (Guid id in ids) {
                // Retrieve the Research from the database
                Research research = _researchController.GetResearchById(id);
                try {
                    // Try to end the Research
                    _researchController.EndResearch(id);
                    Master.AddAlert(new Alert("Research '" + research.Name + "' has been ended, and all users have been unassigned", AlertType.Success));
                }
                catch (IllegalChangeException) {
                    //The research is not active, so it cannot be ended.
                    Master.AddAlert(new Alert("Research '" + research.Name + "' is not active, so you can't end it", AlertType.Error));
                }
                catch (ResearchDoesNotExistException) {
                    //The research being ended does not exist.
                    Master.AddAlert(new Alert("Research Id is not valid or research does not exist", AlertType.Error));
                }
                catch (Exception ex) {
                    Master.AddAlert(new Alert(ex.Message, AlertType.Error));
                }
            }
            Master.Redirect("~/researches.aspx");
        }
        #endregion
    }
}
