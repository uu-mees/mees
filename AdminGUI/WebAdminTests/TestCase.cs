// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

namespace WebAdminTests {
    internal class TestCase {
        public TestCase(object item1, object item2 = null, object item3 = null, object item4 = null) {
            Item1 = item1;
            Item2 = item2;
            Item3 = item3;
            Item4 = item4;
        }

        public object Item1 { get; }
        public object Item2 { get; }
        public object Item3 { get; }
        public object Item4 { get; }
    }
}
