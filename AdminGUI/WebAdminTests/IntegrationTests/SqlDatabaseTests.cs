// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Functional;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.DataSource;
using static WebAdmin.DataTypes.User;

namespace WebAdminTests.IntegrationTests {
    [TestClass]
    public class SqlDatabaseTests {
        private SqlServerDatabase _database;

        [TestInitialize]
        public void TestInitialize() =>
            // Initialize database and controllers
            _database = new SqlServerDatabase(ConfigurationManager.ConnectionStrings["database"].ConnectionString);

        [TestMethod]
        public void UserSqlTest() {
            try {
                // Create a new user
                _database.CreateUser(new User(Guid.NewGuid(), "admintestusers", "test@email.com", AuthenticationController.HashPassword("test", Guid.NewGuid()),
                                              Guid.NewGuid(), UserRole.Administrator));
                // Verify the user has been added to the database - also tests the GetUserByUsername functionality
                User user = _database.GetUserByUsername("admintestusers");
                Assert.IsNotNull(user);

                // Update the user
                user.Email = "new@email.com";
                _database.UpdateUser(user);
                // Verify the update was successful - also tests the GetUserById functionality
                user = _database.GetUserById(user.Id);
                Assert.IsNotNull(user);

                // Test the GetUsers functionality
                List<User> users = _database.GetUsers(999, 0, "", "", "");
                Assert.IsTrue(users.Exists(t => t.Id == user.Id));

                // Delete the user
                _database.DeleteUser(user.Id);
                // Verify the user has been deleted from the database
                user = _database.GetUserByUsername("admintestusers");
                Assert.IsNull(user);
            }
            catch (Exception ex) {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void DataSourceSqlTest() {
            try {
                // Add a new datasource
                DataSource dataSource =
                    new DataSource(Guid.NewGuid(), "http://test.com/test.xml", true, DataSourceType.Raster);
                _database.AddDataSource(dataSource);
                // Verify the user has been added to the database - also tests the GetDataSourceByUrl functionality
                Assert.AreEqual(_database.GetDataSourceByUrl("http://test.com/test.xml"), dataSource);

                // Update the datasource
                dataSource.Active = false;
                _database.UpdateDataSource(dataSource);
                // Verify the update was successful - also tests the GetDataSourceById functionality
                Assert.AreEqual(_database.GetDataSourceById(dataSource.Id), dataSource);

                // Test the GetDataSources functionality
                List<DataSource> dataSources = _database.GetDataSources();
                Assert.IsTrue(dataSources.Exists(s => s.Id == dataSource.Id));

                // Delete the datasource
                _database.RemoveDataSource(dataSource.Id);
                // Verify the datasource has been deleted from the database
                Assert.IsNull(_database.GetDataSourceById(dataSource.Id));
            }
            catch (Exception ex) {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void QuerySqlTest() {
            try {
                // Test a few simple queries
                Guid id = Guid.NewGuid();
                Assert.AreEqual(_database.ExecuteQuery("INSERT INTO SensorComponents (Id, Component, Unit) VALUES ('" + id + "', 'test', 'test')"),
                                1);
                Assert.AreEqual(_database.ExecuteQuery("DELETE SensorComponents WHERE Id = '" + id + "'"),
                                1);
                Assert.IsNotNull(_database.ExecuteQueryWithResult("SELECT * FROM Users"));
                Assert.AreEqual(-1, _database.ExecuteQuery("USE LivingLabAir"));
            }
            catch (Exception ex) {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidQueryException), "A erroneous query was not detected")]
        public void InvalidQuerySqlTest() => new QueryController(_database).ExecuteQuery("select shit", out _, out _);

        [TestMethod]
        [ExpectedException(typeof(InvalidQueryResultException), "A erroneous query was not detected")]
        public void InvalidQueryResultSqlTest() => new QueryController(_database).ExecuteQuery("use master", out _, out _);

        [TestMethod]
        public void AuthenticationSqlTest() {
            try {
                // Create a user for testing
                User user = new User(Guid.NewGuid(), "admintestauthentication", "nonexisting@email.com", "password",
                                     Guid.NewGuid(), UserRole.User);
                _database.CreateUser(user);

                // Add a new token
                ResetToken token = new ResetToken(Guid.NewGuid().ToString(), user.Id, DateTime.UtcNow);
                _database.CreateResetToken(token);
                // Verify the token exists in the database
                Assert.AreEqual(_database.GetResetToken(token.Token), token);
                Assert.AreEqual(_database.GetResetTokenByUserId(token.UserId), token);

                // Delete the token
                _database.DeleteResetToken(token.UserId);
                // Verify the token has been deleted from the database
                Assert.IsNull(_database.GetResetToken(token.Token));
                Assert.IsNull(_database.GetResetTokenByUserId(token.UserId));

                // Remove the testing user
                _database.DeleteUser(user.Id);
            }
            catch (Exception ex) {
                Assert.Fail(ex.ToString());
            }
        }
    }
}
