// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.Implementations.Mocks;
using WebAdmin.Utilities;

namespace WebAdminTests.UnitTests {
    [TestClass]
    public class QueryControllerTests {
        private readonly QueryController _queryController = new QueryController(new MockDatabase());

        /// <summary>
        ///     Tests if a invalid (too short) query is detected
        /// </summary>
        [TestMethod]
        public void TooShortQueryTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test"),
                new TestCase("error"),
                new TestCase(""),
                new TestCase(" ")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _queryController.ExecuteQuery((string)test.Item1, out _, out _);
                    Assert.Fail("Invalid query was not catched");
                }
                catch (InvalidQueryLengthException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests executing a 'normal' query (--> not a SELECT query)
        /// </summary>
        [TestMethod]
        public void CommandQueryTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("INSERT INTO Sensors(Id, Latitude, Longitude) VALUES ('13', 12, 12)", true),
                new TestCase("INSERT INTO SqlUsers VALUES ('test12345')", false)
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    _queryController.ExecuteQuery((string)test.Item1, out _, out _, (bool)test.Item2);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests executing a SELECT query
        /// </summary>
        [TestMethod]
        public void TableQueryTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("SELECT * FROM SqlUsers"),
                new TestCase("SELECT * FROM Sensors")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    _queryController.ExecuteQuery((string)test.Item1, out _, out _);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
    }
}
