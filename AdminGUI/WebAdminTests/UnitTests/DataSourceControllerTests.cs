// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Mocks;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.DataSource;

namespace WebAdminTests.UnitTests {
    [TestClass]
    public class DataSourceControllerTests {
        private MockDatabase _database;
        private DataSourceController _dataSourceController;

        /// <summary>
        ///     Initializes the environment before every test
        /// </summary>
        [TestInitialize]
        public void TestInitialize() {
            // Initialize mock database and UserController
            _database = new MockDatabase();
            _dataSourceController = new DataSourceController(_database);

            // Add some sources for testing purposes
            _dataSourceController.AddDataSource("url1", DataSourceType.Raster, true);
            _dataSourceController.AddDataSource("url2", DataSourceType.Raster, false);
            _dataSourceController.AddDataSource("url3", DataSourceType.Sensor, true);
            _dataSourceController.AddDataSource("url4", DataSourceType.Sensor, false);
        }

        #region "Retrieve DataSource tests"
        /// <summary>
        ///     tests whether every datasource is obtained from the getdatasources function.
        /// </summary>
        [TestMethod]
        public void GetDataSourcesTest() {
            // Retrieve all data sources
            List<DataSource> dataSources = _dataSourceController.GetDataSources();
            Assert.IsTrue(dataSources.Count == 4);

            // Test GetById and GetByUrl return the same result
            foreach (DataSource dataSource in dataSources) {
                DataSource test1 = _dataSourceController.GetDataSourceById(dataSource.Id);
                DataSource test2 = _dataSourceController.GetDataSourceByUrl(dataSource.Url);
                if (test1 != test2)
                    Assert.Fail("DataSources do not match");
            }
        }
        #endregion

        #region "Delete DataSource tests"
        /// <summary>
        ///     Tests deleting a nonexisting source
        /// </summary>
        [TestMethod]
        public void DeleteInvalidIdTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.RemoveDataSource((Guid)test.Item1);
                    Assert.Fail("Exception was not triggered");
                }
                catch (DatasourceDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests a valid deletion request
        /// </summary>
        [TestMethod]
        public void DeleteSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("url1"),
                new TestCase("url2"),
                new TestCase("url3"),
                new TestCase("url4")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    Guid id = _dataSourceController.GetDataSourceByUrl((string)test.Item1).Id;
                    _dataSourceController.RemoveDataSource(id);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion

        #region "Add DataSource tests"
        /// <summary>
        ///     Tests adding invalid sources
        /// </summary>
        [TestMethod]
        public void AddIncorrectUrlTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(""),
                new TestCase(null),
                new TestCase(" ")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.AddDataSource((string)test.Item1, DataSourceType.Raster, true);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidDatasourceException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests adding duplicate URLs
        /// </summary>
        [TestMethod]
        public void AddDuplicateUrlTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("url1"),
                new TestCase("url2"),
                new TestCase("url3"),
                new TestCase("url4")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.AddDataSource((string)test.Item1, DataSourceType.Raster, true);
                    Assert.Fail("Exception was not triggered");
                }
                catch (DuplicateDatasourceException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests a valid creation request
        /// </summary>
        [TestMethod]
        public void AddSourceTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test", DataSourceType.Raster),
                new TestCase("http://validdatasource.com/test.xml", DataSourceType.Sensor)
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.AddDataSource((string)test.Item1, (DataSourceType)test.Item2, true);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion

        #region "Edit DataSource tests"
        /// <summary>
        ///     Tests empty edit operations
        /// </summary>
        [TestMethod]
        public void InvalidEditTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(""),
                new TestCase(null),
                new TestCase(" ")
            };

            Guid id = _database.GetDataSourceByUrl("url2").Id;

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.EditDataSource(new DataSource(id, (string)test.Item1, true, DataSourceType.Raster));
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidDatasourceException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests duplicate edit operations
        /// </summary>
        [TestMethod]
        public void DuplicateEditTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("url1"),
                new TestCase("url2"),
                new TestCase("url3")
            };

            Guid id = _database.GetDataSourceByUrl("url4").Id;

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.EditDataSource(new DataSource(id, (string)test.Item1, true, DataSourceType.Raster));
                    Assert.Fail("Exception was not triggered");
                }
                catch (DuplicateDatasourceException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests editing a nonexisting source
        /// </summary>
        [TestMethod]
        public void NonexistingEditTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.EditDataSource(new DataSource((Guid)test.Item1, "random_url", true, DataSourceType.Raster));
                    Assert.Fail("Exception was not triggered");
                }
                catch (DatasourceDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests valid edits
        /// </summary>
        [TestMethod]
        public void SuccessfulEditTest() {
            // Setup test data
            Guid id = _dataSourceController.GetDataSourceByUrl("url1").Id;
            List<TestCase> tests = new List<TestCase> {
                new TestCase(new DataSource(id, "test1", false, DataSourceType.Raster)),
                new TestCase(new DataSource(id, "test2", false, DataSourceType.Sensor)),
                new TestCase(new DataSource(id, "test3", true, DataSourceType.Raster)),
                new TestCase(new DataSource(id, "test4", false, DataSourceType.Sensor)),
                new TestCase(new DataSource(id, "test5", true, DataSourceType.Sensor))
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    _dataSourceController.EditDataSource((DataSource)test.Item1);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion

        #region "Start/Stop DataSource Upload tests"
        /// <summary>
        ///     Tests whether a source upload is successfully stopped
        /// </summary>
        [TestMethod]
        public void StopUploadTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("url1"),
                new TestCase("url3")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    Guid id = _dataSourceController.GetDataSourceByUrl((string)test.Item1).Id;
                    _dataSourceController.DisableDataSource(id);
                    Assert.IsFalse(_database.GetDataSourceById(id).Active);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests whether a source upload is successfully started
        /// </summary>
        [TestMethod]
        public void StartUploadTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("url2"),
                new TestCase("url4")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    Guid id = _dataSourceController.GetDataSourceByUrl((string)test.Item1).Id;
                    _dataSourceController.EnableDataSource(id);
                    Assert.IsTrue(_database.GetDataSourceById(id).Active);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
    }
}
