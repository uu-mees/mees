// /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
//  * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Mocks;
using WebAdmin.Utilities;
using static WebAdmin.DataTypes.User;

namespace WebAdminTests.UnitTests {
    [TestClass]
    public class AuthenticationControllerTests {
        private AuthenticationController _authenticationController;
        private MockDatabase _database;
        private MockEmailProvider _emailProvider;
        private UserController _userController;

        /// <summary>
        ///     Initializes the environment before every test
        /// </summary>
        [TestInitialize]
        public void TestInitialize() {
            // Initialize mock database, email provider and controllers
            _database = new MockDatabase();
            _emailProvider = new MockEmailProvider();
            _userController = new UserController(_database, new MockNotificationProvider());
            _authenticationController = new AuthenticationController(_database, _emailProvider);

            // Create some users for testing purposes
            _userController.CreateUser("admin1", "admin", "admin@admin.com", UserRole.Administrator);
            _userController.CreateUser("admin2", "secretpassword123", "admin@admin.com", UserRole.Administrator);
            _userController.CreateUser("researcher1", "researcher", "researcher@researcher.com", UserRole.Researcher);
            _userController.CreateUser("researcher2", "!@#$%^&*()", "researcher@researcher.com", UserRole.Researcher);
            _userController.CreateUser("user1", "user", "user@user.com", UserRole.User);
            _userController.CreateUser("user2", "user", "user@user.com", UserRole.User);
            _userController.CreateUser("user3", "user", "user@user.com", UserRole.User);
            _userController.CreateUser("user4", "user", "user@user.com", UserRole.User);
        }

        #region "Login tests"
        /// <summary>
        ///     Tests whether a admin login is successful
        /// </summary>
        [TestMethod]
        public void SuccessfulLoginTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1", "admin"),
                new TestCase("admin2", "secretpassword123"),
                new TestCase("researcher1", "researcher"),
                new TestCase("researcher2", "!@#$%^&*()")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.Login((string)test.Item1, (string)test.Item2, out _);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests whether a wrong password login fails
        /// </summary>
        [TestMethod]
        public void WrongPasswordLoginTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1", ""),
                new TestCase("admin2", " "),
                new TestCase("researcher1", "13243t4y5trhfd"),
                new TestCase("researcher2", "!@#$&*()")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.Login((string)test.Item1, (string)test.Item2, out _);
                    Assert.Fail("Exception was not catched");
                }
                catch (InvalidCredentialsException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests whether an invalid username login fails
        /// </summary>
        [TestMethod]
        public void WrongUsernameLoginTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin3", "admin"),
                new TestCase("admin4", "secretpassword123"),
                new TestCase("researcher3", "researcher"),
                new TestCase("researcher4", "!@#$%^&*()")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.Login((string)test.Item1, (string)test.Item2, out _);
                    Assert.Fail("Exception was not catched");
                }
                catch (InvalidCredentialsException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests if a user/researcher can login
        /// </summary>
        [TestMethod]
        public void IncorrectRoleLoginTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("user1", "user"),
                new TestCase("user2", "user"),
                new TestCase("user3", "user"),
                new TestCase("user4", "user")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.Login((string)test.Item1, (string)test.Item2, out _);
                    Assert.Fail("Exception was not catched");
                }
                catch (InvalidRoleException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion

        #region "Password reset tests"
        /// <summary>
        ///     Tests that a nonexisting token reset is catched
        /// </summary>
        [TestMethod]
        public void InvalidTokenResetTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid().ToString()),
                new TestCase(Guid.NewGuid().ToString()),
                new TestCase(Guid.NewGuid().ToString())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.ApplyPasswordReset((string)test.Item1, "test");
                    Assert.Fail("Exception was not catched");
                }
                catch (TokenDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests resetting the password of a nonexisting user
        /// </summary>
        [TestMethod]
        public void WrongUsernameResetTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("nonexistinguser"),
                new TestCase("testuser"),
                new TestCase("randomuser")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    Assert.Fail("Exception was not catched");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests requesting a reset for a user that already requested a token
        /// </summary>
        [TestMethod]
        public void DuplicateResetTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1"),
                new TestCase("admin2"),
                new TestCase("researcher1"),
                new TestCase("researcher2")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    Assert.Fail("Exception was not catched");
                }
                catch (DuplicateTokenException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests requesting a reset for an account with the User role
        /// </summary>
        [TestMethod]
        public void IncorrectRoleResetTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("user1"),
                new TestCase("user2"),
                new TestCase("user3"),
                new TestCase("user4")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    Assert.Fail("Exception was not catched");
                }
                catch (InvalidRoleException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests resetting a password using an expired token
        /// </summary>
        [TestMethod]
        public void ExpiredTokenTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1"),
                new TestCase("admin2"),
                new TestCase("researcher1"),
                new TestCase("researcher2")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    // Request a password reset
                    _authenticationController.RequestPasswordReset((string)test.Item1, -1);
                    // Get the reset token from the database
                    User user = _database.GetUserByUsername((string)test.Item1);
                    ResetToken resetToken = _database.GetResetTokenByUserId(user.Id);
                    // Attempt to reset the password using this token
                    _authenticationController.ApplyPasswordReset(resetToken.Token, "password");
                    Assert.Fail("Exception was not catched");
                }
                catch (InvalidTokenException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests requesting and resetting the password after a previous token expired
        /// </summary>
        [TestMethod]
        public void SuccessfulResetAfterPreviousTokenExpiredTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1"),
                new TestCase("admin2"),
                new TestCase("researcher1"),
                new TestCase("researcher2")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    // Request a expired password reset
                    _authenticationController.RequestPasswordReset((string)test.Item1, -1);
                    // Request a normal password reset
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    // Get the reset token from the database
                    User user = _database.GetUserByUsername((string)test.Item1);
                    ResetToken resetToken = _database.GetResetTokenByUserId(user.Id);
                    // Attempt to reset the password using this token
                    _authenticationController.ApplyPasswordReset(resetToken.Token, "password");
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        ///     Tests a successful password reset
        /// </summary>
        [TestMethod]
        public void SuccessfulResetTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin1"),
                new TestCase("admin2"),
                new TestCase("researcher1"),
                new TestCase("researcher2")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    // Request a password reset
                    _authenticationController.RequestPasswordReset((string)test.Item1);
                    // Get the reset token from the database
                    User user = _database.GetUserByUsername((string)test.Item1);
                    ResetToken resetToken = _database.GetResetTokenByUserId(user.Id);
                    // Attempt to reset the password using this token
                    _authenticationController.ApplyPasswordReset(resetToken.Token, "password");
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
    }
}
