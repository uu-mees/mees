/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Mocks;
using WebAdmin.Utilities;

namespace WebAdminTests.UnitTests {
    [TestClass]
    public class UserControllerTests {
        private MockDatabase _database;
        private UserController _userController;
        private ResearchController _researchController;
        private readonly PermissionList _standardPermission = new PermissionList(true, false, true, false, true, true);

        [TestInitialize]
        public void TestInitialize() {
            // Initialize mock database and UserController
            _database = new MockDatabase();
            _userController = new UserController(_database, new MockNotificationProvider());
            _researchController = new ResearchController(_database);

            // Create some users for testing purposes
            _userController.CreateUser("admin", "admin", "admin@admin.com", User.UserRole.Administrator);
            _userController.CreateUser("user123", "user", "user@user.com", User.UserRole.User);
            _userController.CreateUser("user456", "password", "user2@user.nl", User.UserRole.User);
            _userController.CreateUser("researcher", "researcher", "researcher@researcher.com", User.UserRole.Researcher);
            _researchController.CreateResearch("standard research", "Standard description", _standardPermission, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(7));
            _researchController.CreateResearch("special research", "Special description", _standardPermission, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(3));
        }
        #region "Delete User tests"
        /// <summary>
        /// Tests deleting a User with an empty Id
        /// </summary>
        [TestMethod]
        public void DeleteInvalidIdTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUser((Guid)test.Item1);
                    Assert.Fail("DeleteUser: invalid Id not catched");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests deleting a User with a nonexisting Id
        /// </summary>
        [TestMethod]
        public void DeleteNonexistingTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUser((Guid)test.Item1);
                    Assert.Fail("DeleteUser: nonexisting Id not catched");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests deleting a User successfully
        /// </summary>
        [TestMethod]
        public void DeleteSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("user123"),
                new TestCase("user456"),
                new TestCase("admin"),
                new TestCase("researcher")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    Guid userId = _userController.GetUserByUsername((string)test.Item1).Id;
                    _userController.DeleteUser(userId);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Create User tests"
        /// <summary>
        /// Tests creating a user with missing fields
        /// </summary>
        [TestMethod]
        public void CreateInvalidInputTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(null, null, null, User.UserRole.Researcher),
                new TestCase(null, null, null, User.UserRole.Researcher),
                new TestCase("hi", null, null, User.UserRole.Researcher),
                new TestCase(null, "hi", null, User.UserRole.Researcher),
                new TestCase(null, null, "hi@hi.nl", User.UserRole.Researcher),
                new TestCase("hi", "hi", null, User.UserRole.Researcher),
                new TestCase(null, "hi", "hi@hi.hi", User.UserRole.Researcher),
                new TestCase("hi", null, "hi@hi.hi", User.UserRole.Researcher)
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.CreateUser((string)test.Item1, (string)test.Item2, (string)test.Item3, (User.UserRole)test.Item4);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidInputException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests creating a user successfully
        /// </summary>
        [TestMethod]
        public void CreateSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("hihihi", "hi", "hi@hi.hi", User.UserRole.Researcher),
                new TestCase("langeusernametestcase", "hi", "hi2@hi.hi", User.UserRole.Administrator)
            };

            // Run tests and check that no exception is triggered
            foreach (TestCase test in tests)
                try {
                    _userController.CreateUser((string)test.Item1, (string)test.Item2, (string)test.Item3, (User.UserRole)test.Item4);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests username validation
        /// </summary>
        [TestMethod]
        public void CreateInvalidUsernameTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("\\(^-^)/", "password", "mail@mail.com", User.UserRole.User),
                new TestCase("yo-mama", "password", "mail@mail.com", User.UserRole.User),
                new TestCase("♫ ^%€ :)", "password", "mail@mail.com", User.UserRole.User),
                new TestCase("Robert; DROP TABLE users", "password", "mail@mail.com", User.UserRole.User),
                new TestCase("hi", "hi", "hi@hi.hi", User.UserRole.User)
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.CreateUser((string)test.Item1, (string)test.Item2, (string)test.Item3, (User.UserRole)test.Item4);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidUsernameException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }


        /// <summary>
        /// Tests creating a user with an invalid email address
        /// </summary>
        [TestMethod]
        public void CreateInvalidEmailTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test1"),
                new TestCase("test2@.test"),
                new TestCase("test3@.com")

            };

            // Run tests and check that no exception is triggered
            foreach (TestCase test in tests)
                try {
                    _userController.CreateUser("testuser", "test", (string)test.Item1, User.UserRole.Administrator);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidEmailException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests creating a user with a already used username
        /// </summary>
        [TestMethod]
        public void CreateDuplicateUsernameTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("user123"),
                new TestCase("user456")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.CreateUser((string)test.Item1, "password", "test@user.com", User.UserRole.User);
                    Assert.Fail("Exception was not triggered");
                }
                catch (UsernameUnavailableException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Get User tests"
        /// <summary>
        /// Tests getting user(s) from the database
        /// </summary>
        [TestMethod]
        public void RetrieveTest() {
            // Test partial get
            List<User> users = _userController.GetUsers(3);
            Assert.IsTrue(users.Count == 3);

            // Test invalid params get
            users = _userController.GetUsers(-1, -1);
            Assert.IsTrue(users.Count == 4);

            // Test normal get
            users = _userController.GetUsers();
            Assert.IsTrue(users.Count == _userController.GetUserCount());

            foreach (User user in users) {
                User test1, test2;
                test1 = _userController.GetUserById(user.Id);
                test2 = _userController.GetUserByUsername(user.Username);
                if (test1 != test2)
                    Assert.Fail("Users do not match");
            }
        }
        #endregion
        #region "Delete User Data tests"
        /// <summary>
        /// Tests deleting data of invalid IDs
        /// </summary>
        [TestMethod]
        public void DeleteDataInvalidUserIdTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUserData((Guid)test.Item1);
                    Assert.Fail("Exception was not triggered");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests deleting nonexisting users
        /// </summary>
        [TestMethod]
        public void DeleteDataNonexistingUserTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUserData((Guid)test.Item1);
                    Assert.Fail("Exception was not triggered");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests deleting the data of a non-user account
        /// </summary>
        [TestMethod]
        public void DeleteDataInvalidRoleTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("admin"),
                new TestCase("researcher")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUserData(_userController.GetUserByUsername((string)test.Item1).Id);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidRoleException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        [TestMethod]
        public void DeleteDataSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("user123"),
                new TestCase("user456")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    _userController.DeleteUserData(_userController.GetUserByUsername((string)test.Item1).Id);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Edit User tests"
        /// <summary>
        /// Tests invalid edits
        /// </summary>
        [TestMethod]
        public void EditInvalidInputTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(null),
                new TestCase("")
            };

            User user = _userController.GetUserByUsername("user123");

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    user.Email = (string)test.Item1;
                    _userController.EditUser(user);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidInputException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests editing a non-existing user
        /// </summary>
        [TestMethod]
        public void EditNonExistingTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            User user = _userController.GetUserByUsername("user123");

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    User editedUser = new User((Guid)test.Item1, user.Username, user.Email, user.Password, user.Salt, user.Role);
                    _userController.EditUser(editedUser);
                    Assert.Fail("Exception was not triggered");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests editing a user
        /// </summary>
        [TestMethod]
        public void EditSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("random@email.com"),
                new TestCase("verylongemailaddress@verylongdomain.com")
            };

            User user = _userController.GetUserByUsername("user123");

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    user.Email = (string)test.Item1;
                    _userController.EditUser(user);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Assign tests"
        /// <summary>
        ///     Tests assigning a User that is already assigned
        /// </summary>
        [TestMethod]
        public void DoubleUserAssignmentTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("user123")),
                new TestCase(_userController.GetUserByUsername("user456"))
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.AssignResearch(((User)test.Item1).Id, _researchController.GetResearchByName("standard research").Id);
                    _userController.AssignResearch(((User)test.Item1).Id, _researchController.GetResearchByName("special research").Id);
                    Assert.Fail("Double assignment not catched");
                }
                catch (AssignedUserException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests assigning a Researcher to the same Research twice
        /// </summary>
        [TestMethod]
        public void InvalidAssignmentTest() {
            //Set up test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("researcher").Id, _researchController.GetResearchByName("standard research").Id),
                new TestCase(_userController.GetUserByUsername("admin").Id, _researchController.GetResearchByName("standard research").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.AssignResearch((Guid)test.Item1, (Guid)test.Item2);
                }
                catch (AssignedUserException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests assigning a non-existing User
        /// </summary>
        [TestMethod]
        public void EmptyUserAssignmentTest() {
            //Set up test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.AssignResearch((Guid)test.Item1, _researchController.GetResearchByName("standard research").Id);
                    Assert.Fail("Null user not catched");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests assigning to a non-existing Research
        /// </summary>
        [TestMethod]
        public void EmptyResearchAssignmentTest() {
            //Set up test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.AssignResearch(_userController.GetUserByUsername("user123").Id, (Guid)test.Item1);
                    Assert.Fail("Null research not catched");
                }
                catch (ResearchDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests valid assignments
        /// </summary>
        [TestMethod]
        public void CorrectAssignmentTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("researcher")),
                new TestCase(_userController.GetUserByUsername("user123")),
                new TestCase(_userController.GetUserByUsername("user456"))
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.AssignResearch(((User)test.Item1).Id, _researchController.GetResearchByName("standard research").Id);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests getting all assigned Researches
        /// </summary>
        [TestMethod]
        public void GetAssignedResearches_Success() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("user123").Id, _researchController.GetResearchByName("standard research").Id),
                new TestCase(_userController.GetUserByUsername("user456").Id, _researchController.GetResearchByName("standard research").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    // Assign the user
                    _userController.AssignResearch((Guid)test.Item1, (Guid)test.Item2);
                    List<Research> researchIds = _userController.GetAssignedResearches((Guid)test.Item1);
                    if (researchIds.Count != 1 || researchIds[0].Id != (Guid)test.Item2)
                        Assert.Fail("Assignment failed or researches do not match");
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests getting all assigned Researches for non-existing Users
        /// </summary>
        [TestMethod]
        public void GetAssignedResearches_NullUser() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid(), _researchController.GetResearchByName("standard research").Id),
                new TestCase(Guid.NewGuid(), _researchController.GetResearchByName("standard research").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    List<Research> researchIds = _userController.GetAssignedResearches((Guid)test.Item1);
                    Assert.Fail("Null user not catched");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests whether a User is in an active Research
        /// </summary>
        [TestMethod]
        public void IsInActiveResearch_Success() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("user123").Id, _researchController.GetResearchByName("standard research").Id),
                new TestCase(_userController.GetUserByUsername("user456").Id, _researchController.GetResearchByName("special research").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    // Assign the user
                    _userController.AssignResearch((Guid)test.Item1, (Guid)test.Item2);

                    // Verify that the User is in an active Research
                    if (!_userController.IsInResearch((Guid)test.Item1))
                        Assert.Fail("Assignment not detected");

                    // End the Research
                    _researchController.EndResearch((Guid)test.Item2);

                    // Verify that the User is no longer in an active Research
                    if (_userController.IsInResearch((Guid)test.Item1))
                        Assert.Fail("Invalid assignment detected");

                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests the IsInActiveResearch functionality with a non-existing User
        /// </summary>
        [TestMethod]
        public void IsInActiveResearch_NullUser() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid(), _researchController.GetResearchByName("standard research").Id),
                new TestCase(Guid.NewGuid(), _researchController.GetResearchByName("special research").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    // Test if the User is in an active Research
                    _userController.IsInResearch((Guid)test.Item1);
                    Assert.Fail("Assignment not detected");
                }
                catch (UserDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }
        #endregion
        #region "Unassign tests"
        [TestMethod]
        public void EmptyUserUnassignmentTest() {
            try {
                _userController.UnassignUser(Guid.Empty, _researchController.GetResearchByName("standard research").Id);
                Assert.Fail("should've failed because of empty user");
            }
            catch (UserDoesNotExistException) {
                //success
            }
            catch {
                Assert.Fail("Incorrect Error");
            }

        }
        [TestMethod]
        public void EmptyResearchUnassignmentTest() {
            try {
                _userController.UnassignUser(_userController.GetUserByUsername("user123").Id, Guid.Empty);
                Assert.Fail("should've failed because of empty research");
            }
            catch (ResearchDoesNotExistException) {
                //success
            }
            catch {
                Assert.Fail("Incorrect Error");
            }

        }
        [TestMethod]
        public void WrongUnassignmentTest() {
            try {
                _userController.UnassignUser(_userController.GetUserByUsername("user123").Id, _researchController.GetResearchByName("standard research").Id);
            }
            catch {
                Assert.Fail("This just should not have crashed. This is impressive.");
            }

        }
        [TestMethod]
        public void CorrectUnassignmentTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("researcher")),
                new TestCase(_userController.GetUserByUsername("user123")),
                new TestCase(_userController.GetUserByUsername("user456"))
            };

            //run tests
            for (int i = 0; i < tests.Count; i++) {
                try {
                    User testuser = (User)tests[i].Item1;
                    _userController.AssignResearch(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                    _userController.UnassignUser(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                    _userController.AssignResearch(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                    _userController.UnassignUser(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                    _userController.AssignResearch(testuser.Id, _researchController.GetResearchByName("special research").Id);
                    _userController.UnassignUser(testuser.Id, _researchController.GetResearchByName("special research").Id);
                    if (i == 0) {
                        _userController.AssignResearch(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                        _userController.AssignResearch(testuser.Id, _researchController.GetResearchByName("special research").Id);
                        _userController.UnassignUser(testuser.Id, _researchController.GetResearchByName("standard research").Id);
                        _userController.UnassignUser(testuser.Id, _researchController.GetResearchByName("special research").Id);
                    }
                }
                catch {
                    Assert.Fail("Incorrect Error");
                }
            }

        }
        #endregion
        #region "Notification tests"
        /// <summary>
        ///     Tests sending a push notification
        /// </summary>
        [TestMethod]
        public void SuccessfulNotificationTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(_userController.GetUserByUsername("user123").Id),
                new TestCase(_userController.GetUserByUsername("user456").Id)
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.SendNotification((Guid)test.Item1, "test", "test");
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        ///     Tests sending a push notification when the user has no token
        /// </summary>
        [TestMethod]
        public void NoTokenNotificationTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };
            // Run tests
            foreach (TestCase test in tests) {
                try {
                    _userController.SendNotification((Guid)test.Item1, "test", "test");
                    Assert.Fail("Exception was not catched");
                }
                catch (NoFirebaseTokenException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
            }
        }
        #endregion
    }
}
