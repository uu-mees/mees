/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAdmin.Controllers;
using WebAdmin.DataTypes;
using WebAdmin.Implementations.Mocks;
using WebAdmin.Utilities;

namespace WebAdminTests.UnitTests {
    [TestClass]
    public class ResearchControllerTests {
        MockDatabase database;
        ResearchController researchController;
        PermissionList _standardPermission = new PermissionList(true, false, true, false, true, true);

        [TestInitialize]
        public void TestInitialize() {
            // Initialize mock database and ResearchController
            database = new MockDatabase();
            researchController = new ResearchController(database);

            // Create some researches for testing purposes
            researchController.CreateResearch("test 1", "the first testing research", _standardPermission, DateTime.Today.AddDays(1), DateTime.Today.AddDays(28));
            researchController.CreateResearch("test 2", "the second testing research", _standardPermission, DateTime.Today.AddDays(1), DateTime.Today.AddDays(28));
            researchController.CreateResearch("test 3", "the third testing research", _standardPermission, DateTime.Today.AddDays(1), DateTime.Today.AddDays(28));
            researchController.CreateResearch("test 4", "the fourth testing research", _standardPermission, DateTime.Today.AddDays(-20), DateTime.Today.AddDays(28));
        }
        #region "Get Research tests"
        /// <summary>
        /// Tests getting research(es) from the database
        /// </summary>
        [TestMethod]
        public void RetrieveTest() {
            List<Research> researches = researchController.GetResearches();
            Assert.IsTrue(researches.Count == 4);

            researches = researchController.GetResearches(-1, -1);
            Assert.IsTrue(researches.Count == 4);


            foreach (Research research in researches) {
                Research test1, test2;
                test1 = researchController.GetResearchById(research.Id);
                test2 = researchController.GetResearchByName(research.Name);
                if (test1 != test2)
                    Assert.Fail("Researches do not match");
            }
        }
        #endregion
        #region "Create Research tests"
        /// <summary>
        /// Tests creating researches with invalid duration/end dates
        /// </summary>
        [TestMethod]
        public void CreateInvalidDurationTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(DateTime.UtcNow, DateTime.UtcNow.AddDays(-1)),
                new TestCase(DateTime.UtcNow.AddDays(10), DateTime.UtcNow.AddDays(5)),
                new TestCase(DateTime.UtcNow.AddDays(-5), DateTime.UtcNow.AddDays(-2))
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch("test", "test", _standardPermission, (DateTime)test.Item1, (DateTime)test.Item2);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidResearchLengthException) {
                    // Success
                }
                catch (InvalidResearchEndException) {
                    // Other Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests adding a research with a duplicate name
        /// </summary>
        [TestMethod]
        public void CreateDuplicateNameTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test 1"),
                new TestCase("test 2")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item1, "test", _standardPermission, DateTime.UtcNow.AddDays(1), DateTime.UtcNow.AddDays(5));
                    Assert.Fail("Exception was not triggered");
                }
                catch (DuplicateResearchException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests adding a research that has an invalid name
        /// </summary>
        [TestMethod]
        public void CreateInvalidNameTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(""),
                new TestCase(" "),
                new TestCase(null)
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item1, "test", _standardPermission, DateTime.UtcNow.AddDays(1), DateTime.UtcNow.AddDays(5));
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidInputException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests adding a research successfully
        /// </summary>
        [TestMethod]
        public void CreateSuccesfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test 5"),
                new TestCase("test 6")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item1, "test", _standardPermission, DateTime.UtcNow.AddDays(1), DateTime.UtcNow.AddDays(5));
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Delete Research tests"
        /// <summary>
        /// Tests deleting an invalid Id
        /// </summary>
        [TestMethod]
        public void DeleteInvalidIdTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.RemoveResearch((Guid)test.Item1);
                    Assert.Fail("Exception was not triggered");
                }
                catch (ResearchDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests whether a valid research will be removed.
        /// </summary>
        [TestMethod]
        public void DeleteSuccesfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(researchController.GetResearchByName("test 1").Id),
                new TestCase(researchController.GetResearchByName("test 2").Id)
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.RemoveResearch((Guid)test.Item1);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "Edit Research tests"
        /// <summary>
        /// Tests whether invalid run time edits are catched
        /// </summary>
        [TestMethod]
        public void EditInvalidDurationTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(DateTime.UtcNow.AddDays(3), DateTime.UtcNow.AddDays(1), "research1"),
                new TestCase(DateTime.UtcNow.AddDays(100), DateTime.UtcNow.AddDays(99), "research2")
            };
            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item3, "test", _standardPermission, (DateTime)test.Item1, ((DateTime)test.Item1).AddDays(101));
                    Research research = researchController.GetResearchByName((string)test.Item3);
                    research.EndDate = (DateTime)test.Item2;
                    researchController.EditResearch(research);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidResearchLengthException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests whether invalid end date edits are catched
        /// </summary>
        [TestMethod]
        public void EditInvalidEndDateTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(DateTime.UtcNow.AddDays(-100), DateTime.UtcNow.AddDays(-10), "research1"),
                new TestCase(DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-1), "research2")
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item3, "test", _standardPermission, (DateTime)test.Item1, ((DateTime)test.Item1).AddDays(101));
                    Research research = researchController.GetResearchByName((string)test.Item3);
                    research.EndDate = (DateTime)test.Item2;
                    researchController.EditResearch(research);
                    Assert.Fail("Exception was not triggered");
                }
                catch (InvalidResearchEndException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests whether duplicate name edits are catched
        /// </summary>
        [TestMethod]
        public void EditDuplicateNameTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test 2"),
                new TestCase("test 3")
            };
            Research research = researchController.GetResearchByName("test 1");

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    research.Name = (string)test.Item1;
                    researchController.EditResearch(research);
                    Assert.Fail("Exception was not triggered");
                }
                catch (DuplicateResearchException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests whether invalid Id edits are catched
        /// </summary>
        [TestMethod]
        public void EditInvalidIdTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that the right exception occurs
            foreach (TestCase test in tests)
                try {
                    Research research = new Research((Guid)test.Item1, "test", "test", _standardPermission,
                                                     DateTime.UtcNow.AddDays(1), DateTime.UtcNow.AddDays((7)));
                    researchController.EditResearch(research);
                    Assert.Fail("Exception was not triggered");
                }
                catch (ResearchDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Tests that legal edits are successful
        /// </summary>
        [TestMethod]
        public void EditSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("description2", DateTime.UtcNow.AddDays(30)),
                new TestCase("description3", DateTime.UtcNow.AddDays(2))
            };
            Research research = researchController.GetResearchByName("test 1");

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    research.Description = (string)test.Item1;
                    research.EndDate = (DateTime)test.Item2;
                    researchController.EditResearch(research);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
        #region "End Research tests"
        /// <summary>
        /// Test ending a research
        /// </summary>
        [TestMethod]
        public void EndSuccessfulTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("test 4")
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    Research research = researchController.GetResearchByName((string)test.Item1);
                    researchController.EndResearch(research.Id);
                    Assert.IsTrue(DateTime.UtcNow >= database.GetResearchById(research.Id).EndDate);
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Test ending a non-existing research
        /// </summary>
        [TestMethod]
        public void EndNonexistingTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid()),
                new TestCase(Guid.NewGuid())
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.EndResearch((Guid)test.Item1);
                    Assert.Fail("Invalid Id not catched");
                }
                catch (ResearchDoesNotExistException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }

        /// <summary>
        /// Test ending a inactive research
        /// </summary>
        [TestMethod]
        public void EndInactiveTest() {
            // Setup test data
            List<TestCase> tests = new List<TestCase> {
                new TestCase("research1", DateTime.UtcNow.AddDays(1), DateTime.UtcNow.AddDays(10)),
                new TestCase("research2", DateTime.UtcNow.AddHours(1), DateTime.UtcNow.AddHours(2))
            };

            // Run tests and check that no exception occurs
            foreach (TestCase test in tests)
                try {
                    researchController.CreateResearch((string)test.Item1, "test", _standardPermission, (DateTime)test.Item2, (DateTime)test.Item3);
                    Research research = researchController.GetResearchByName((string)test.Item1);
                    researchController.EndResearch(research.Id);
                    Assert.Fail("Invalid Id not catched");
                }
                catch (IllegalChangeException) {
                    // Success
                }
                catch (Exception ex) {
                    Assert.Fail(ex.Message);
                }
        }
        #endregion
    }
}
