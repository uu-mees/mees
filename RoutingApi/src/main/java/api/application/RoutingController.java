/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.application;

import api.domain.Routes;
import com.graphhopper.util.shapes.GHPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
public class RoutingController {
    //We limit the vehicle and weighting options
    private ArrayList<String> possibleWeightings = new ArrayList<>(Arrays.asList("shortest", "fastest", "cleanest"));
    static ServiceFactory serviceFactory = new ServiceFactory();

    //Example: http://localhost:8080/routing?startLat=52.08720&startLong=5.16546&destLat=52.08597&destLong=5.17250&vehicle=bike&weighting=shortest
    @RequestMapping("/routing")
    //Calculate the route for the specified query.
    public ResponseEntity<Routes> routing(@RequestParam(value="startLat") float startLat,
                                          @RequestParam(value="startLong") float startLong,
                                          @RequestParam(value="destLat") float destLat,
                                          @RequestParam(value="destLong") float destLong,
                                          @RequestParam(value="vehicle",defaultValue = "bike") String vehicle,
                                          @RequestParam(value="weighting", defaultValue = "shortest") String weighting) {
        //We do not accept all the options (nor do we accept invalid inputs)
        if(!possibleWeightings.contains(weighting))
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        GHPoint start = new GHPoint(startLat, startLong);
        GHPoint dest = new GHPoint(destLat, destLong);

        IRoutingService RoutingService = serviceFactory.GetRoutingService(vehicle);

        Routes routes = RoutingService != null ? RoutingService.CalcRoutes(start, dest, weighting) : null;
        if (routes == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        return ResponseEntity.ok(routes);
    }
}
