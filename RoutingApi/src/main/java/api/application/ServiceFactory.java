/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.application;

public class ServiceFactory {
    // Create the services during startup.
    static IRoutingService bikeService = new RoutingService("bike");
    static IRoutingService carService = new RoutingService("car");
    static IRoutingService footService = new RoutingService("foot");

    public ServiceFactory(){
    }

    public static IRoutingService GetRoutingService(String vehicle){
        if(vehicle.equals("bike")) {
            return bikeService;
        }
        else if(vehicle.equals("car")) {
            return carService;
        }
        else if(vehicle.equals("foot")) {
            return footService;
        }
        else
            return null;
    }
}
