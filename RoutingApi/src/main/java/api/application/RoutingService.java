/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.application;

import api.domain.Routes;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.Parameters;
import com.graphhopper.util.shapes.GHPoint;

import java.util.List;
import java.util.Locale;

public class RoutingService implements IRoutingService{

    String osmFile = "netherlands-latest.osm.pbf";
    String graphFolder = "GHFolder";
    String vehicle;

    GraphHopper hopper;

    public RoutingService(String vehicle){
        this.vehicle = vehicle;
        // create one GraphHopper instance
        hopper = new GraphHopper().forServer();

        // Turn preloading so we can support alternative routes
        hopper.setCHEnabled(false);

        hopper.setDataReaderFile(osmFile);

        hopper.setGraphHopperLocation(graphFolder + "/" + vehicle);

        EncodingManager encoder = new EncodingManager(vehicle);
        encoder.setEnableInstructions(false);

        hopper.setEncodingManager(encoder);

        // now this can take minutes if it imports or a few seconds for loading
        // of course this is dependent on the area you import
        hopper.importOrLoad();
    }

    //Calculate the Routes for the given query.
    public Routes CalcRoutes(GHPoint start, GHPoint dest, String weighting){
        GHRequest request = new GHRequest(start, dest).
                setWeighting(weighting).
                setVehicle(vehicle).
                setLocale(Locale.getDefault()).
                setAlgorithm(Parameters.Algorithms.ALT_ROUTE);

        GHResponse response = hopper.route(request);

        return new Routes(start, dest, vehicle, weighting, response);
    }
}
