/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.domain;

import com.graphhopper.util.GPXEntry;

public class GHPoint {
    public Double[] Point;
    public long ElapsedTime;

    public GHPoint(GPXEntry entry){
        this.Point = new Double[] {entry.lon, entry.lat};
        this.ElapsedTime = entry.getTime();
    }
}
