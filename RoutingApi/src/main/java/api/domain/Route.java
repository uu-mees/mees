/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.domain;
import com.graphhopper.*;
import com.graphhopper.util.GPXEntry;

import java.util.ArrayList;
import java.util.List;

public class Route {
    public double Distance;
    public long Duration;
    public List<GHPoint> Path;

    public Route(){
    }

    public Route(PathWrapper route){
        this.Distance = route.getDistance();
        this.Duration = route.getTime();
        this.Path = new ArrayList<>();
        List<GPXEntry> entries = route.getInstructions().createGPXList();
        for(int i = 0; i < entries.size(); i++){
            Path.add(new GHPoint(entries.get(i)));
        }
    }
}
