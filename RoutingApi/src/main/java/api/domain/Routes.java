/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
package api.domain;

import api.domain.Route;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.shapes.GHPoint;

import java.util.ArrayList;
import java.util.List;

public class Routes {
    private GHPoint start, destination;
    public String Vehicle;
    public String Weighting;
    public int Count;
    public ArrayList<String> Errors;

    public ArrayList<Route> routes;

    public Routes(){
    }

    public Routes(GHPoint start, GHPoint dest, String Vehicle, String Weighting, GHResponse response){
        this.start = start;
        this.destination = dest;
        this.Vehicle = Vehicle;
        this.Weighting = Weighting;
        this.routes = new ArrayList<>();
        if (response.hasErrors()){
            List<Throwable> errors = response.getErrors();
            Errors = new ArrayList<>();
            for (Throwable error: errors) {
                Errors.add(error.getMessage());
                error.printStackTrace();
            }
        }
        else {
            for (PathWrapper p : response.getAll()) {
                this.routes.add(new Route(p));
            }
        }
        this.Count = routes.size();
    }

    public Double[] getStart() {
        return start.toGeoJson();
    }

    public Double[] getDestination() {
        return destination.toGeoJson();
    }
}
