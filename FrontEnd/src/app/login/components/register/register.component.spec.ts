/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
/**
 * Since writing these tests, the structure of RegisterComponent has changed. Tests for this component need to be
 * re-written. This file is here to keep form-based tests around for easy copying later.
 */

/* import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { AuthService } from '../../../shared/services/auth.service';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injector } from '@angular/core';
import { environment } from 'environments/environment';
import { HomeComponent } from '../../../home/components/home/home.component';
import { Location } from '@angular/common';
import { AppComponent } from '../../../app.component';
import { AppModule } from '../../../app.module';
import { RegisterComponent } from './register.component';
import { UserRegistration } from '../../../shared/models/user-registration.model';
import { UserAuthentication } from '../../../shared/models/user-authentication.model';
import { FormBuilder } from '@angular/forms';

describe('RegisterComponent', () => {
    let component: RegisterComponent;
    let appFixture: ComponentFixture<AppComponent>;
    let fixture: ComponentFixture<RegisterComponent>;
    let authService: AuthService;
    let httpMock: HttpTestingController;
    let router: Router;
    let location: Location;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([
                    { path: 'register', component: RegisterComponent },
                    { path: 'home', component: HomeComponent }
                ])
            ],
            declarations: [
            ],
            providers: [
                AuthService,
                FormBuilder
            ]
        })
            .compileComponents();

        httpMock = TestBed.get(HttpTestingController);
        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
        location = TestBed.get(Location);

        appFixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();

        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        fillValidForm();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should register and login', () => {
        const registrationBody: UserRegistration = {
            username: 'testUsername',
            password: 'testPassword',
            email: 'testEmail'
        };

        const authBody: UserAuthentication = {
            username: 'testUsername',
            password: 'testPassword'
        };

        component.registerAndLogin(registrationBody, authBody).then(x => {
            const http2 = httpMock.expectOne(environment.backEndUrl + 'token');
        });

        const http = httpMock.expectOne(environment.backEndUrl + 'users/register');
    });

    // COMMENTED CAUSE INCORRECT FAILS TODO:
    // it('should redirect to home', () => {
    //     router.navigate(['register']);

    //     const registerResponse = {
    //         id: 'testId',
    //         username: 'testUsername'
    //     };

    //     const tokenResponse = { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZFVzZXIiOnRydWV9.IIDPwKOmqJ-ZvOU8RQO5Yufwh9Pc47WgiItmeevFhso' };
    //     const registrationBody: UserRegistration = {
    //         username: 'testUsername',
    //         password: 'testPassword',
    //         email: 'testEmail'
    //     };

    //     const authBody: UserAuthentication = {
    //         username: 'testUsername',
    //         password: 'testPassword'
    //     };

    //     component.registerAndLogin(registrationBody, authBody).then(x => {
    //         // This code is executed after the HTTP flush
    //         const http2 = httpMock.expectOne(environment.backEndUrl + 'token');
    //         http2.flush(tokenResponse);
    //     }).then(x =>
    //         // This code is executed after the second HTTP flush
    //         expect(location.path()).toBe('/home')
    //     );

    //     const http = httpMock.expectOne(environment.backEndUrl + 'users/register');
    //     http.flush(registerResponse);
    // });

    function fillValidForm() {
        component.registrationForm.controls['username'].setValue('testUser');
        component.registrationForm.controls['password'].setValue('password');
        component.registrationForm.controls['confirmPassword'].setValue('password');
        component.registrationForm.controls['email'].setValue('valid@test.com');
    }

    it('should require username', () => {
        component.registrationForm.controls['username'].setValue('');
        expect(component.registrationForm.invalid).toBe(true);
    });

    it('should not allow whitespace in username', () => {
        component.registrationForm.controls['username'].setValue('testUser name');
        expect(component.registrationForm.invalid).toBe(true);
    });

    it('should require username length between 5 and 15 characters', () => {
        component.registrationForm.controls['username'].setValue('test');
        expect(component.registrationForm.invalid).toBe(true);

        component.registrationForm.controls['username'].setValue('testWithALongUse');
        expect(component.registrationForm.invalid).toBe(true);
    });

    it('should require matching passwords', () => {
        component.registrationForm.controls['password'].setValue('password1');
        component.registrationForm.controls['confirmPassword'].setValue('password2');
        expect(component.registrationForm.invalid).toBe(true);

        component.registrationForm.controls['password'].setValue('password');
        component.registrationForm.controls['confirmPassword'].setValue('password');
        expect(component.registrationForm.invalid).toBe(false);
    });

    it('should require password between 8 and 256 characters', () => {
        component.registrationForm.controls['password'].setValue('pass');
        component.registrationForm.controls['confirmPassword'].setValue('pass');
        expect(component.registrationForm.invalid).toBe(true);

        // Set passwords with string with length of 257
        component.registrationForm.controls['password'].setValue('8vCMyqDCrJZjEmGMfYhbWquLoRfQ7CJBeE034eNxDXk7o8fXC1szaEAtzvAxPfGfiqhKezRESLYIhPbbHanJD1xso2cbQ6Te9s85GeguvEx1WJ2ygEJ6vWWFZLiFwT6T8Ym7vTcxd2hkHchwuMUDz3l4ZC1AIFoCXW0ke2z1vGa4NfNxeKx8aP2NILyb2uzYdSs9YpdMIKp110SISDlofZfQFS0fqKk47nDGTEECK37u6Y1tUIyd5zwcgtlZh27sZ');
        component.registrationForm.controls['confirmPassword'].setValue('8vCMyqDCrJZjEmGMfYhbWquLoRfQ7CJBeE034eNxDXk7o8fXC1szaEAtzvAxPfGfiqhKezRESLYIhPbbHanJD1xso2cbQ6Te9s85GeguvEx1WJ2ygEJ6vWWFZLiFwT6T8Ym7vTcxd2hkHchwuMUDz3l4ZC1AIFoCXW0ke2z1vGa4NfNxeKx8aP2NILyb2uzYdSs9YpdMIKp110SISDlofZfQFS0fqKk47nDGTEECK37u6Y1tUIyd5zwcgtlZh27sZ');
        expect(component.registrationForm.invalid).toBe(true);

        component.registrationForm.controls['password'].setValue('password');
        component.registrationForm.controls['confirmPassword'].setValue('password');
        expect(component.registrationForm.invalid).toBe(false);
    });

    it('should require email', () => {
        component.registrationForm.controls['email'].setValue('');
        expect(component.registrationForm.invalid).toBe(true);
    });

});
*/
