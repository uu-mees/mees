/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';

// Import models
import { AuthService } from 'app/shared/services/auth.service';
import { UserRegistration } from 'app/shared/models/user-registration.model';
import { UserAuthentication } from 'app/shared/models/user-authentication.model';

@Component({
    selector: 'seed-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public registrationForm: FormGroup;
    public registrationError = undefined;
    public loginFailed: boolean;
    public hidePassword: boolean;
    public registering = false;

    public labelPosition = 'before';

    constructor(private formBuilder: FormBuilder,
        private authService: AuthService,
        private http: HttpClient,
        private router: Router) {
        this.hidePassword = true;
        this.createForm();
    }

    ngOnInit() {
    }

    /** Creates the registrationform with the formbuilder */
    private createForm() {
        this.registrationForm = this.formBuilder.group({
            username: ['', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(15),
                this.noWhitespaceValidator
            ])],
            password: ['', Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(256)
            ])],
            confirmPassword: ['', Validators.required],
            email: ['', Validators.compose([
                Validators.required,
                Validators.email,
            ])],
            trackLocation: [true],
            height: [0, Validators.compose([
                Validators.max(999),
                Validators.pattern(/^[0-9]+$/)
            ])],
            weight: [0, Validators.compose([
                Validators.max(999.9),
                Validators.pattern(/^[0-9.,]+$/)
            ])],
            sex: [''],
            dateOfBirth: [Date.now(), Validators.compose([
                Validators.pattern(/^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}$/)
            ])],
            homeAddress: [''],
            workAddress: ['']
        }, {
                validator: this.matchPasswordValidator
            });
    }

    /** Returns the username error message on error */
    getUsernameErrorMessage() {
        const errors = this.registrationForm.controls.username.errors;

        return errors.required ? 'Username is required' :
            errors.minlength ? 'Username must have at least 5 characters.' :
                errors.maxlength ? 'Username is too long.' :
                    errors.whitespace ? 'Username may not contain any spaces.' :
                        '';
    }

    /** Returns the password error message on error */
    getPasswordErrorMessage() {
        const errors = this.registrationForm.controls.password.errors;

        return errors.required ? 'Password is required' :
            errors.maxlength ? 'Password is too long.' :
                errors.minlength ? 'Password must have at least 8 characters.' :
                    '';
    }

    /** Returns the password does not match error message on error */
    getConfirmPasswordErrorMessage() {
        const errors = this.registrationForm.controls.confirmPassword.errors;

        return errors.required ? 'Passwords do not match.' :
            errors.matchPassword ? 'Passwords do not match.' :
                '';
    }

    /** Returns the email error message on error */
    getEmailErrorMessage() {
        const errors = this.registrationForm.controls.email.errors;

        return errors.required ? 'Email is required.' :
            errors.email ? 'Enter a valid email address' :
                '';
    }

    /** Returns the height error message on error */
    getHeightErrorMessage() {
        const errors = this.registrationForm.controls.height.errors;

        return errors.required ? 'Height is required.' :
            errors.height ? 'Enter a valid height' :
                '';
    }

    /** Returns the weight error message on error */
    getWeightErrorMessage() {
        const errors = this.registrationForm.controls.weight.errors;

        return errors.required ? 'Weight is required.' :
            errors.weight ? 'Enter a valid weight' :
                '';
    }

    /** Returns the sex error message on error. */
    getSexErrorMessage(): string {
        const errors = this.registrationForm.controls.sex.errors;
        if (errors == null) {
            return null;
        }
        return errors.required ? 'Sex is required' :
            errors.sex ? 'Enter a valid Sex.' :
                '';
    }

    /** Returns the date of birth error message on error. */
    getDateOfBirthErrorMessage(): string {
        const errors = this.registrationForm.controls.dateOfBirth.errors;
        if (errors == null) {
            return null;
        }
        return errors.required ? 'Date of birth is required' :
            errors.dateOfBirth ? 'Enter a valid date of birth.' :
                '';
    }

    /** Returns the home address error message on error. */
    getHomeAddressErrorMessage(): string {
        const errors = this.registrationForm.controls.homeAddress.errors;
        if (errors == null) {
            return null;
        }
        return errors.homeAddress ? 'Home address is not valid.' :
            '';
    }

    /** Returns the work address error message on error. */
    getWorkAddressErrorMessage(): string {
        const errors = this.registrationForm.controls.workAddress.errors;
        if (errors == null) {
            return null;
        }
        return errors.workAddress ? 'Work address is not valid.' :
            '';
    }

    /** On form submit retrieve the data from the form and perform register and login */
    public onSubmit() {
        if (!this.registrationForm.valid) { return; }
        let dob;
        if (this.registrationForm.controls.dateOfBirth.value.toString().split('-').length === 1) {
            dob = null;
        } else {
            var dobSplit = this.registrationForm.controls.dateOfBirth.value.split('-');

            dob = new Date(dobSplit[2],dobSplit[1]-1,dobSplit[0],10);

        }

        const registrationBody: UserRegistration = {
            username: this.registrationForm.value.username,
            password: this.registrationForm.value.password,
            email: this.registrationForm.controls.email.value,
            tracklocation: true,
            height: this.registrationForm.controls.height.value,
            weight: this.registrationForm.controls.weight.value,
            sex: this.registrationForm.controls.sex.value,
            dateOfBirth: dob,
            homeAddress: this.registrationForm.controls.homeAddress.value,
            workAddress: this.registrationForm.controls.workAddress.value
        };

        const authBody: UserAuthentication = {
            username: registrationBody.username,
            password: registrationBody.password,
        };

        // Attempt to register the user, if success then attempt to login. If login success then redirect to map.
        this.registerAndLogin(registrationBody, authBody);
    }

    /** General navigate function */
    public navigate(path: string) {
        this.router.navigate([path]);
    }

    /** Register the user, if successful log them in. */
    public registerAndLogin(registrationBody: UserRegistration, authBody: UserAuthentication): Promise<any> {
        this.registering = true;
        this.registrationError = undefined;
        return this.authService.performRegistration(registrationBody)
            .then(x => {
                this.authService.performLogin(authBody)
                    .then(y => {
                        this.navigate("../");
                        this.registering = false;
                        this.registrationError = undefined;
                        this.loginFailed = false;
                        return true;
                    })
                    .catch(loginError => {
                        console.error(loginError);
                        this.loginFailed = true;
                        return false;
                    });
            })
            .catch(registrationError => {
                console.error(registrationError);
                this.registrationError = registrationError;
                this.registering = false;
                return false;
            });
    }

    /** Validator to check if there is no whitespace */
    private noWhitespaceValidator(control: FormControl): { [key: string]: any } {
        const hasWhitespace = (control.value || '').trim().length === 0;
        const isValid = !hasWhitespace;

        if (/\s/.test(control.value)) {
            return { 'whitespace': true };
        } else {
            return null;
        }
    }

    /** Validator to check if password and confirm password match */
    private matchPasswordValidator(AC: AbstractControl): { [key: string]: any } {
        const password = AC.get('password').value;
        const confirmPassword = AC.get('confirmPassword').value;

        if (password !== confirmPassword) {
            AC.get('confirmPassword').setErrors({ matchPassword: true });
        } else {
            return null;
        }
    }

    /** Gets the username field of the form */
    get username() { return this.registrationForm.get('username'); }

    /** Gets the password field of the form */
    get password() { return this.registrationForm.get('password'); }

    /** Gets the confirmPassword field of the form */
    get confirmPassword() { return this.registrationForm.get('confirmPassword'); }

    /** Gets the email field of the form */
    get email() { return this.registrationForm.get('email'); }

    /** Gets the height field of the form */
    get height() { return this.registrationForm.get('height'); }

    /** Gets the weight field of the form */
    get weight() { return this.registrationForm.get('weight'); }

    /** public member for sex used in html */
    get sex() { return this.registrationForm.get('sex'); }

    /** public member for date of birth used in html */
    get dateOfBirth() { return this.registrationForm.get('dateOfBirth'); }

    /** public member for the home address used in html */
    get homeAddress() { return this.registrationForm.get('homeAddress'); }

    /** public member for the work address used in html */
    get workAddress() { return this.registrationForm.get('workAddress'); }

}
