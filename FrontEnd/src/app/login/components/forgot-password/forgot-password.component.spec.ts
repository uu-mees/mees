/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { UserService } from '../../../shared/services/user.service';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injector, ChangeDetectorRef } from '@angular/core';
import { environment } from 'environments/environment';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { HomeComponent } from '../../../home/components/home/home.component';
import { LoginComponent } from '../login/login.component';
import { AppModule } from '../../../app.module';
import { AppComponent } from '../../../app.component';
import { ForgotPasswordComponent } from './forgot-password.component';

describe('ForgotPasswordComponent', () => {
    let component: ForgotPasswordComponent;
    let appFixture: ComponentFixture<AppComponent>;
    let fixture: ComponentFixture<ForgotPasswordComponent>;
    let userService: UserService;
    let httpMock: HttpTestingController;
    let router: Router;
    let location: Location;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([
                    { path: 'login', component: LoginComponent },
                    { path: 'home', component: HomeComponent }
                ])
            ],
            declarations: [
            ],
            providers: [
                UserService,
                FormBuilder
            ]
        }).compileComponents();

        httpMock = TestBed.get(HttpTestingController);
        userService = TestBed.get(UserService);
        router = TestBed.get(Router);
        location = TestBed.get(Location);

        appFixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();

        fixture = TestBed.createComponent(ForgotPasswordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        // Setup forms.
        fillValidEmailForm();
        fillValidPasswordForm();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ForgotPasswordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        router = TestBed.get(Router);
        location = TestBed.get(Location);

        router.initialNavigation();
    });

    afterEach(async(() => {
        // Destroy fixture to reset it for each next test
        fixture.destroy();
    }));

    function fillValidEmailForm() {
        // Valid input
        component.emailForm.controls['email'].setValue('post@malo.ne');
    }

    function fillValidPasswordForm() {
        // Valid input
        component.passwordForm.controls['token'].setValue('0d6d0703-ccb3-426d-9cbc-a580b0b9acf7');
        component.passwordForm.controls['newPassword'].setValue('paranoid');
        component.passwordForm.controls['confirmPassword'].setValue('paranoid');
    }

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should trigger emailForm.email errors', () => {
        // Required.
        component.emailForm.controls['email'].setValue('');
        expect(component.emailForm.invalid).toBeTruthy('emailForm.email was valid...');
        expect(component.getEmailErrorMessage()).toEqual('Enter your email address.', 'Incorrect emailForm.email errormessage.');
    });

    it('should trigger passwordForm.token error messages', () => {
        // Required
        component.passwordForm.controls['token'].setValue('');
        expect(component.passwordForm.invalid).toBeTruthy('passwordForm.token was valid...');
        expect(component.getTokenErrorMessage()).toEqual('Enter your recovery token.', 'Incorrect passwordForm.token errormessage.');
    });

    it('should trigger passwordForm.newPassword errors', () => {
        // Required
        component.passwordForm.controls['newPassword'].setValue('');
        expect(component.passwordForm.invalid).toBeTruthy('passwordForm.newPassword required was valid...');
        expect(component.getNewPasswordErrorMessage()).toEqual('New password is required.', 'Incorrect passwordForm.newPassword required errormessage.');

        // Minlength (8)
        component.passwordForm.controls['newPassword'].setValue('2short');
        expect(component.passwordForm.invalid).toBeTruthy('passwordForm.newPassword minlength was valid...');
        expect(component.getNewPasswordErrorMessage()).toEqual('At least 8 characters required.', 'Incorrect passwordForm.newPassword minlength errormessage.');

        // Maxlength (256)
        component.passwordForm.controls['newPassword'].setValue('aapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaap');
        expect(component.passwordForm.invalid).toBeTruthy('passwordForm.newPassword maxlength was valid...');
        expect(component.getNewPasswordErrorMessage()).toEqual('New password is too long.', 'Incorrect passwordForm.newPassword maxlength errormessage.');
    });

    it('should trigger passwordForm.confirmPassword errors', () => {
        // Does not match newPassword.
        component.passwordForm.controls['newPassword'].setValue('thispasswordisvalid');
        component.passwordForm.controls['confirmPassword'].setValue('NOMATCH');
        expect(component.passwordForm.invalid).toBeTruthy('passwordForm.confirmPassword match was valid...');
        expect(component.getConfirmPasswordErrorMessage()).toEqual('Passwords do not match.', 'Incorrect passwordForm.confirmPassword errormessage.');
    });

    it('should send a GET request to back-end', () => {
        // Declare totally spies.
        const spy = spyOn(component, 'onSubmitEmail').and.callThrough();
        expect(component.emailFormFailed).toBeFalsy('Emailform request failed.');

        component.onSubmitEmail().then(x => { const http = httpMock.expectOne(environment.backEndUrl + 'users/settings/token'); });

        expect(spy).toHaveBeenCalled();
        // Verify that methods inside .then() were called.
        expect(component.emailFormFailed).toBeFalsy('Email form GET failed for some reason.');
    });

    it('should reset password in back-end and redirect user to login.', async(() => {
        // Declare totally spies.
        const spy = spyOn(component, 'onSubmitPasswordReset').and.callThrough();
        expect(component.passwordFormFailed).toBeFalsy('PasswordReset has failed before request.');

        component.onSubmitPasswordReset();
        fixture.detectChanges();

        expect(spy).toHaveBeenCalled();
        // Verify that methods inside .then() were called.
        expect(component.passwordFormFailed).toBeFalsy('PasswordReset failed.');

        // Give browser time to switch pages, Fixture.detectchanges(); does not support this.
        setTimeout(() => {
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                // Check if program navigated to correct page
                expect(location.path()).toBe('login', 'User should have been redirected to login.');
            });
        }, (100));
    }));
});
