/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { UserService } from '../../../shared/services/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})

/** Component used to reset a user password. */
export class ForgotPasswordComponent implements OnInit {

    // Declare email form variables.
    public emailForm: FormGroup;
    public emailFormFailed: boolean;
    public emailFormError = undefined;
    public sendingEmailRequest: boolean;
    // Used to disable email form if request was successful.
    public emailFormDisabled: boolean;

    // Declare password form variables.
    public passwordForm: FormGroup;
    public passwordFormFailed: boolean;
    public passwordFormError = undefined;
    public sendingResetUserPassword: boolean;

    // Helper boolean for password fields.
    public hidePassword = true;

    constructor(private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        public snackBar: MatSnackBar) {

        this.emailFormDisabled = false;
        this.emailFormFailed = false;
        this.passwordFormFailed = false;
        this.createForms();
    }

    ngOnInit() {
    }

    /** Initializes the forms with preset values. */
    private createForms() {
        // Create emailForm.
        this.emailForm = this.formBuilder.group({
            email: ['', Validators.required]
        });

        // Create passwordForm.
        this.passwordForm = this.formBuilder.group({
            token: ['', Validators.required],
            newPassword: ['', Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(256)
            ])],
            confirmPassword: ['', Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(256)
            ])]
        }, {
            validator: this.matchNewPasswordValidator
        });
    }

    /** Binds information from emailForm and calls user service to make a request to back-end. */
    public onSubmitEmail(): Promise<any> {
        // Don't send another request if email form is disabled (form is disabled when a request was successful).
        if (this.emailFormDisabled) { return; }
        const email = this.emailForm.controls.email.value;

        this.sendingEmailRequest = true;
        return this.userService.requestUserRecoveryToken(email)
            .then(() => {
                this.emailFormFailed = false;
                this.emailFormError = undefined;
                this.sendingEmailRequest = false;
                // Set disabled to true.
                this.emailFormDisabled = true;
                this.displaySnackbarMessage('An email has been sent to ' + email + '.');
            })
            .catch(error => {
                this.emailFormFailed = true;
                this.emailFormError = error;
                this.sendingEmailRequest = false;
            });
    }

    /** Binds information from passwordForm and calls user service to make a request to back-end.
     *  Redirects user to login if the change was successful.
     */
    public onSubmitPasswordReset() {
        const token = this.passwordForm.controls.token.value;
        const newPassword = this.passwordForm.controls.newPassword.value;
        const confirmPassword = this.passwordForm.controls.confirmPassword.value;

        this.sendingResetUserPassword = true;
        this.userService.resetUserPassword(token, newPassword, confirmPassword)
            .then(() => {
                this.passwordFormFailed = false;
                this.passwordFormError = undefined;
                this.sendingResetUserPassword = false;
                this.displaySnackbarMessage('Successfully changed password.');
                this.router.navigate(['/login']);
            })
            .catch(error => {
                this.passwordFormFailed = true;
                this.passwordFormError = error;
                this.sendingResetUserPassword = false;
            });
    }

    /** Reset the email form to default values. */
    public resetEmailForm() {
        this.emailForm.reset();
        this.emailFormFailed = false;
    }

    /** Reset the password form to default values. */
    public resetPasswordForm() {
        this.passwordForm.reset();
    }

    /** Opens the snackbar and displays a message. (Used to inform the user.) */
    private displaySnackbarMessage(message: string, messageduration?) {
        // Display the snackbar.
        const snackbar = this.snackBar.open(message, '', {
            duration: (!!messageduration) ? messageduration : 3000,
        });
    }

    /** public helper function to navigate (accessible from HTML) */
    public navigate(path: string): void {
        this.router.navigate([path]);
    }

    /** Verifies that both passwords match. */
    private matchNewPasswordValidator(AC: AbstractControl): { [key: string]: any } {
        const password = AC.get('newPassword').value;
        const confirmPassword = AC.get('confirmPassword').value;

        if (password !== confirmPassword) {
            AC.get('confirmPassword').setErrors({ matchPassword: true });
        } else {
            return null;
        }
    }

    /** public member for email error message. */
    public getEmailErrorMessage() {
        return 'Enter your email address.';
    }

    /** public member for token error message. */
    public getTokenErrorMessage() {
        return 'Enter your recovery token.';
    }

    /** public member for new password error message. */
    public getNewPasswordErrorMessage(): string {
        const errors = this.passwordForm.controls.newPassword.errors;

        return errors.required ? 'New password is required.' :
            errors.maxlength ? 'New password is too long.' :
                errors.minlength ? 'At least 8 characters required.' :
                    '';
    }

    /** public member for confirm password error message. */
    public getConfirmPasswordErrorMessage(): string {
        return 'Passwords do not match.';
    }

    /** public member email. */
    get email() { return this.emailForm.get('email'); }

    /** public member token. */
    get token() { return this.passwordForm.get('token'); }

    /** public member new password. */
    get newPassword() { return this.passwordForm.get('newPassword'); }

    /** public member confirm password. */
    get confirmPassword() { return this.passwordForm.get('confirmPassword'); }
}
