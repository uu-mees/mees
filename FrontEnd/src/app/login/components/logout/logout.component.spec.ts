/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LogoutComponent } from './logout.component';
import { AuthService } from '../../../shared/services/auth.service';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injector } from '@angular/core';
import { environment } from 'environments/environment';
import { HomeComponent } from '../../../home/components/home/home.component';
import { Location } from '@angular/common';
import { AppComponent } from '../../../app.component';
import { AppModule } from '../../../app.module';
import { UserAuthentication } from '../../../shared/models/user-authentication.model';
import { LoginComponent } from '../login/login.component';

describe('LogoutComponent', () => {
    let component: LogoutComponent;
    let appFixture: ComponentFixture<AppComponent>;
    let fixture: ComponentFixture<LogoutComponent>;
    let authService: AuthService;
    let router: Router;
    let location: Location;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([
                    { path: 'logout', component: LogoutComponent },
                    { path: 'login', component: LoginComponent }
                ])
            ],
            declarations: [
            ],
            providers: [
                AuthService,
            ]
        }).compileComponents();

        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
        location = TestBed.get(Location);

        appFixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();

        fixture = TestBed.createComponent(LogoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set token to null', () => {
        expect(authService.getToken()).toBe(null);
    });

    it('should redirect to map', () => {
        router.navigate(['logout']);
        expect(location.path()).toBe('/');
    });

});
