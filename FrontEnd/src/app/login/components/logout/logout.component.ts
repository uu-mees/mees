/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component } from '@angular/core';
import { Router } from '@angular/router';

// Import services
import { AuthService } from 'app/shared/services/auth.service';
import { MapService, MapState } from 'app/shared/services/map.service';

@Component({
    selector: 'seed-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

    /** Component removes token and immediately redirects */
    constructor(private authService: AuthService, private router: Router, private mapService: MapService) {

        this.authService.removeToken();

        this.mapService.mapState.next(MapState.Default);
        this.router.navigate(['']);
    }

}
