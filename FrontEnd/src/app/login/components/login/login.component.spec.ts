/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
/**
 * Since writing these tests, the structure of LoginComponent has changed. Tests for this component need to be
 * re-written. This file is here to keep form-based tests around for easy copying later.
 */

/* import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthService } from '../../../shared/services/auth.service';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injector } from '@angular/core';
import { environment } from 'environments/environment';
import { HomeComponent } from '../../../home/components/home/home.component';
import { Location } from '@angular/common';
import { AppComponent } from '../../../app.component';
import { AppModule } from '../../../app.module';
import { UserAuthentication } from '../../../shared/models/user-authentication.model';
import { MatDialogRef } from '@angular/material';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let appFixture: ComponentFixture<AppComponent>;
    let fixture: ComponentFixture<LoginComponent>;
    let authService: AuthService;
    let httpMock: HttpTestingController;
    let router: Router;
    let location: Location;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([
                    { path: 'login', component: LoginComponent },
                    { path: 'home', component: HomeComponent }
                ])
            ],
            declarations: [
            ],
            providers: [
                AuthService,
                MatDialogRef
            ]
        }).compileComponents();

        httpMock = TestBed.get(HttpTestingController);
        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
        location = TestBed.get(Location);

        appFixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();

        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        // Component should redirect to home if token is not expired
        const expiredToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZFVzZXIiOnRydWUsImV4cCI6IjAifQ.Rs6HLB9sVh1cMOhf8oPYWdSTutsZwMiO-iwmBQmnLWM';
        authService.setToken(expiredToken);
        expect(component).toBeTruthy();
    });

    it('should redirect to home', () => {
        router.navigate(['login']);

        const response = { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZFVzZXIiOnRydWV9.IIDPwKOmqJ-ZvOU8RQO5Yufwh9Pc47WgiItmeevFhso' };
        const authBody: UserAuthentication = {
            username: this.username,
            password: this.password,
        };

        component.logIn(authBody).then(x =>
            // This code is executed after the HTTP flush
            expect(location.path()).toBe('/map')
        );

        // Expect one HTTP call to token url
        const http = httpMock.expectOne(environment.backEndUrl + 'token');
        // Send HTTP response
        http.flush(response);
    });

}); */
