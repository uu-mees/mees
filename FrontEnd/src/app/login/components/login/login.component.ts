/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';

// Import services
import { AuthService, Role } from 'app/shared/services/auth.service';
import { UserService } from 'app/shared/services/user.service';
import { UserAuthentication } from 'app/shared/models/user-authentication.model';

@Component({
    selector: 'seed-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

    public loginForm: FormGroup;
    public error = undefined;
    public hidePassword = true;
    public loggingIn: boolean;
    private authenticationSub: Subscription;

    constructor(private formBuilder: FormBuilder, private authService: AuthService, private http: HttpClient, private router: Router, private userService: UserService) {
        // Check if user is already logged in
        this.authenticationSub = authService.authenticationStream.subscribe(x => {
            if (x !== Role.Unauthenticated) {
                router.navigate(['']);
            }
        });
        this.createForm();
    }

    /** Create the login form with the formbuilder */
    private createForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    /** On form submit, login */
    public onSubmit() {
        if (!this.loginForm.valid) { return; }

        const authBody: UserAuthentication = {
            username: this.loginForm.controls.username.value,
            password: this.loginForm.controls.password.value,
        };

        this.logIn(authBody);
    }

    /** General navigate function */
    public navigate(path: string): void {
        this.router.navigate([path]);
    }

    /** Log in user. Promise is true if login succeeds. */
    public async logIn(body: UserAuthentication): Promise<any> {
        this.loggingIn = true;
        this.error = undefined;
        try {
            const x = await this.authService.performLogin(body);
            this.loggingIn = false;
            self.location.reload();
        } catch (error) {
            this.error = error;
            this.loggingIn = false;
        }
    }

    /** Cleanup subscriptions */
    ngOnDestroy(): void {
        this.authenticationSub.unsubscribe();
    }

    /** Gets the username field of the form */
    get username() { return this.loginForm.get('username'); }

    /** Gets the password field of the form */
    get password() { return this.loginForm.get('password'); }

}
