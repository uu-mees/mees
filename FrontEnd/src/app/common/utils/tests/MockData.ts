/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Raster } from 'app/shared/models/raster.model';
import { SensorBatch } from 'app/shared/models/sensorbatch.model';
import { Batch } from 'app/shared/models/batch.model';
import { Measurement } from 'app/shared/models/measurement.model';
import { Sensor } from 'app/shared/models/sensor.model';

export class MockData {

    // Raster
    public mockRaster: Raster;

    // Sensors
    public mockSensorBatch: Array<SensorBatch>;
    public mockSensorBatch2: Array<SensorBatch>;
    public mockHistoricalData: Array<Sensor>;
    public sensorParams: Array<Array<number>>;
    public faultySensorParams: Array<Array<number>>;
    public historicalDataParams: {id: string, timeStampFrom: string, timeStampTo: string};

    constructor() {
        /*    =================================
         *    |      Raster Mock Data         |
         *    =================================
         */

        // Mock from: http://localhost:50500/raster
        this.mockRaster = new Raster();
        this.mockRaster.timeStampPolled = new Date('2018-06-04T13:23:00.401573Z');
        this.mockRaster.image = 'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAhfSURBVHhe7ZpRciSnEkW9IS/FK/JW/PmW4l9HeCHegMOeJ3dSnaVb2QcKKOiqVuvjhNSQeYF7B6SZmJ9+/uOfD+On3/5uxnuN3//68fHx438L//8zfXa07irQeUbSvcaRTWqvYUF8/Ph1+SqhxLqzobOMpnudoxvVfjOfAnm3UA7pH92o9i6BvMazRWcZxSH9uNFWsdibbgcE8i6hHNbWDSpYDMQ+M/7Vn63S3B572ru4QASLgdi3BMLP1quEkhvfo6RZjYvkwCYh1qdAXujZMug8OlbLkd4VFymBjXeoPoWSebZGhDJCIxLPo2esgbS6UKES2HyDas2wkc/Wkd5a6Dx6zj16+x5QoT1IgOrMuNKzZVCf4/MtPTOg8xK9fUgU2yMKUI2xBJJ/tqLBNB/R+mcQz5qjtw+JYjWoAM0bZuBeIEqphvSfjZ450lK7SxSrxQVozjAjUxj3Z4sMz41HSP9MNgaCB3G+iSjWggnQuGFGxkCIaD5B+lfmweQWSLAWE6Bxx8xMzxaEEk3PQbpnEg2sqWmCBEdgZibzLQynIxCD9M+ADKytq4YEj+JGrgHID3dHDS9B+meA5t2gWoNqqyCxEbihKYB/f+kKhHSvhBmYG+9mpgmmlwLofLJI8xVAowHspcFRuLFLKNsf7mo8QXrvQDGQEcaYRgrBnq2GQAzS+4psbg4VOCNMMY0UAvz6q+YTpPeV2ATxrEAM06FQ1HyCtL4S5wfS+GyR1lcAg3CowRlliumkENIN2f6dRAOIkJZRmpuNG0dztWwCiFCDM/LgppVuR8Oz5X36+UzcNJprYRNAhBqckSaYVgqh47cth3SfQTSNamqJWg9QkzPSBNNKITQ+WwrpziYaRjUtRL0HqMkZaYKbSs9WbSikOxMyjOpaIM0N1OSMNsH0UgDh2bpaIGjUHapvgTQ3UJMz0gQ3NQXQ8WyR5izQqBtU2wJpPkCNjhtFcy2osUsgt9vReEtIdxZo1A2qbYE0H6BGZ6QZau4SStvPEdKcBRlFdS2QJkLNyigz1NzPQOqfLdKcAZp0g2pbId0HqFEZaYabuwTy+Gz5PEF6MyCTqK4H0n6AGpWRpqjWEkr9LSG9WUSTqKaHqItQIxliUG0rrrUEUn9LSGsWahDN96K6WWITmWHEul5czwOIt0TXjJDeK4EBRGITGWHEul5UcwnkdjtSKNe6JbPAEJTYEA9v37tJWncEX8MD+L4lQmygQ482w/U+A6m7JaT1amAICjXFw482xPWMbSCft+QKocxaD4NwqIE24gbF8V7c4DUAC8N/4yIkINIbja81a00Mw4iFuQ1MDWQ1/n5LJIQNa92Ca+xB6+8RNWx9qjtCVSA5fGM014Me9NNkuSUaRI6178593LVrKe3PcV2qPcLhQEZuKh3UjVyNvd+Qu7nduN76vdw8RWvj+B09+8jzR0y7OhDDN0dzPZjWevDVDHm2fK6FVSdCgeiYrLt+T3j9HJoCaf0Tsle7CUTNbHm2FO8fzvwgErczdAUyKpSHQFZT5U9jnG9FD6z43F7dTHxtoSkQQ0Mpme3karz/YVNpswMDcQomIF4/EteNawnTAzG8LvYatKnlAIMDacVNVKhuME8LJAdtajHg5EAiXyGQWJsDD5tCueOfY80zedL6zYEY1mhG0pwRDTeoR+ceDpzCuNgteQK7gUQTDWukcTe4BPWsG3oI5R7IVUJ5wj66boib6Aa72U6s15rc+LopPXRlIK6x0ZnBVQMxrFmNcKjWoRof22zMD94RiEK1V6c7EDKA6pRcrY+tGxsUiEJ9V8L32R2IoQc2qCZC9f55s0kPQtF5QTX3oP4mCvsYweFATMS+0jyh5nifft5ssCIQ7W2BtKqYGIjt6+k3xIh9ymaTKYjys0UatZDeWfieDgVi6AFpPof2RdaNTg7EKGnpXC+lNeJn47RADO1V1gOcHIijNa14f9TMcWoghvY764HWQPZ/2yKdGmr6dZ0c1BPHajg9EEM1nHTQGIgYECGNGvb6dY0S1NvD0EAMqtkjahjpoA03xCCdPUp9pOtjEa05wqFASJDqaiCtFED8v1oZM/xrK9RXo6Vrj+RLBRLHaujtM1rrazj8ZBkqSPM1qIazhCDPVWUgUadET89MugKxxvjZ0fFatN9JRlUGopBWid6+WQy5IUegTTnPCoTGz+LUQGhDTjL4O5Ay1kDjregG4mcnGdwYhkN6r0J1IFZM4634wrnvnSWEubfjijw1EF24NGd8B1LAi2muFl2UtOL8dyAFrJDGa4mL5vR8LhmcwnjzQGxAP/sYjdfi/ZFS7RJACKMiEEPXeEWmBqK9+jWn53MYCJhPuMarUgzEi3Sslp5eq0/Gdt4Ow9d9VYo/Q7yI5kr09BjWtwTw5oHQRI5o4khMfwnjjQOhwRrITBqvxXoX8988kGjKEVSrh3cPw3j4GUJFTmk+6rSymA+BgPE5aF9XhM7vNAeSq41zrRy9HYbu58rQ+Z1DgTh783ss5n/tQOyc+n2O6p8hWhepqSkxIgxH93wl/Kz6PVEViNbkaKlVFvMhDAMM30P3fQTbm37V8ThWg/ftsQkkdyCtKdFSayzGj7sdRtx7Db4f+lyao88Rn/O6PdZAopCiDTlaap3RYRi67xK2vn/NofNUG+cJra9hDSR3GC0u0Vq/mA+BBINb0b2XoD2N4sg6KRAVUGLxKJLx6T/AjQ3DoHMQtK9RuH7POimQ3EFi8QgW88c/VQ6dg6C9XYHsDYmFI0imTQzDoLMQtL8rsPuPi9TUy2cYIRAwthc6A0H7uwLZQKj4CIv5c8Mw6CwE7fEKYCBUeIRtGN+BlNgEQgVH+QxDQnDA0KPoeUrQXs/nn4//AJRp34WmH9UJAAAAAElFTkSuQmCC';
        this.mockRaster.northEastLatitude = 52.109791395301635;
        this.mockRaster.northEastLongitude = 5.197726565916496;
        this.mockRaster.southWestLatitude = 52.0738383560203;
        this.mockRaster.southWestLongitude = 5.139337874130352;


        // Sensor Mock Data

        const swCorner = [52.078358703879076, 5.150806903839112];
        const neCorner = [52.09048957801637, 5.191640853881836];
        this.sensorParams = [swCorner, neCorner];
        this.faultySensorParams = [swCorner];

        // First mock SensorBatch, containing data for only one sensor
        this.mockSensorBatch = new Array<SensorBatch>();

        // Set to which sensor the batch belongs
        this.mockSensorBatch[0] = new SensorBatch();
        this.mockSensorBatch[0].id = 'USP_10763145';
        this.mockSensorBatch[0].label = 'PU009';
        this.mockSensorBatch[0].latitude = 52.0858;
        this.mockSensorBatch[0].longitude = 5.176;

        // Set batch data
        const batch0 = new Batch();
        batch0.id = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.sensorId = 'USP_10763145';
        batch0.timeStampFrom = '2018-05-10T17:00:00';
        batch0.timeStampTo = '2018-05-10T18:00:00';
        batch0.timeStampPolled = '2018-05-10T19:20:07';
        batch0.measurements = new Array<Measurement>();

        // Set batch measurements
        batch0.measurements[0] = new Measurement();
        batch0.measurements[0].id = '4de6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[0].batchId = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[0].value = 15.71;
        batch0.measurements[0].component = 'NO2';
        batch0.measurements[0].unit = 'ug/m3';

        batch0.measurements[1] = new Measurement();
        batch0.measurements[1].id = '4ee6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[1].batchId = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[1].value = 10.56;
        batch0.measurements[1].component = 'PM10';
        batch0.measurements[1].unit = 'ug/m3';

        batch0.measurements[2] = new Measurement();
        batch0.measurements[2].id = '4fe6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[2].batchId = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[2].value = 15.06;
        batch0.measurements[2].component = 'T';
        batch0.measurements[2].unit = 'Celsius';

        batch0.measurements[3] = new Measurement();
        batch0.measurements[3].id = '50e6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[3].batchId = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[3].value = 61.85;
        batch0.measurements[3].component = 'RH';
        batch0.measurements[3].unit = '%';

        batch0.measurements[4] = new Measurement();
        batch0.measurements[4].id = '51e6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[4].batchId = '4ce6a650-8854-e811-831b-bc5ff447a457';
        batch0.measurements[4].value = 1018.38;
        batch0.measurements[4].component = 'P';
        batch0.measurements[4].unit = 'hPa';

        // Assign batch to sensor
        this.mockSensorBatch[0].batch = batch0;

        // Second mock SensorBatch, containing data for two sensors
        this.mockSensorBatch2 = Object.assign([], this.mockSensorBatch); // copy data from first mock as demonstrated at https://stackoverflow.com/questions/44808882/create-a-clone-of-an-array-in-typescript

        // Set to which sensor the batch belongs
        this.mockSensorBatch2[1] = new SensorBatch();
        this.mockSensorBatch2[1].id = 'USP_15708774';
        this.mockSensorBatch2[1].label = 'PU018';
        this.mockSensorBatch2[1].latitude = 52.0884;
        this.mockSensorBatch2[1].longitude = 5.17685;

        // Set batch data
        const batch1 = new Batch();
        batch1.id = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.sensorId = 'USP_15708774';
        batch1.timeStampFrom = '2018-05-10T17:00:00';
        batch1.timeStampTo = '2018-05-10T18:00:00';
        batch1.timeStampPolled = '2018-05-10T19:20:07';
        batch1.measurements = new Array<Measurement>();

        // Set batch measurements
        batch1.measurements[0] = new Measurement();
        batch1.measurements[0].id = '29e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[0].batchId = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[0].value = 16.06;
        batch1.measurements[0].component = 'NO2';
        batch1.measurements[0].unit = 'ug/m3';

        batch1.measurements[1] = new Measurement();
        batch1.measurements[1].id = '2ae6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[1].batchId = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[1].value = 27.93;
        batch1.measurements[1].component = 'PM10';
        batch1.measurements[1].unit = 'ug/m3';

        batch1.measurements[2] = new Measurement();
        batch1.measurements[2].id = '2be6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[2].batchId = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[2].value = 17.06;
        batch1.measurements[2].component = 'T';
        batch1.measurements[2].unit = 'Celsius';

        batch1.measurements[3] = new Measurement();
        batch1.measurements[3].id = '2ce6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[3].batchId = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[3].value = 55.73;
        batch1.measurements[3].component = 'RH';
        batch1.measurements[3].unit = '%';

        batch1.measurements[4] = new Measurement();
        batch1.measurements[4].id = '2de6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[4].batchId = '28e6a650-8854-e811-831b-bc5ff447a457';
        batch1.measurements[4].value = 1017.79;
        batch1.measurements[4].component = 'P';
        batch1.measurements[4].unit = 'hPa';

        // Assign batch to sensor
        this.mockSensorBatch2[1].batch = batch1;

        // Historical data params
        this.historicalDataParams = {id: 'USP_10763145', timeStampFrom: '2018-05-04T13:14:29.605Z', timeStampTo: '2018-05-11T13:14:29.605Z'};

        // Mock historical data for sensor USP_10763145 (first in mock batch)
        this.mockHistoricalData = new Array<Sensor>();
        this.mockHistoricalData[0] = new Sensor();
        this.mockHistoricalData[0].id = 'a76baa55-1955-e811-82ce-b1519a3e4426';
        this.mockHistoricalData[1] = new Sensor();
        this.mockHistoricalData[1].id = '497ddd55-1655-e811-82ce-b1519a3e4426';
        this.mockHistoricalData[2] = new Sensor();
        this.mockHistoricalData[2].id = 'c675f637-1655-e811-82ce-b1519a3e4426';
    }
}
