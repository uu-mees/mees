/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Observable } from 'rxjs/Rx';

export class TestUtils {
    /** Create async observable that emits-once and completes
    *  after a JS engine turn
    *  Source: https://angular.io/guide/testing#component-with-async-service, Date retrieved: 2018-04-11*/
    public static asyncData<T>(data: T) {
        return Observable.defer(() => Promise.resolve(data));
    }

    /** Create async observable error that errors
    * after a JS engine turn
    * Source: https://angular.io/guide/testing#component-with-async-service, Date retrieved: 2018-04-06*/
    public static asyncError<T>(errorObject: any) {
        return Observable.defer(() => Promise.reject(errorObject));
    }
}
