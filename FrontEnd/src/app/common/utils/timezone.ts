/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

export class TimeZone {

    public static getTimeZone(date: Date) {
        return Number(date.toString().split('GMT')[1].split(' (')[0]) / 100;
    }
}
