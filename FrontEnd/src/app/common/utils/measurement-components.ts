/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

export class MeasurementComponent {

    /** Get a full name based on a components abbreviation. */
    public static getComponentName(component: string): string {

        switch (component) {
            case 'T':
                return 'Temperature';
            case 'RH':
                return 'Humidity';
            case 'P':
                return 'Pressure';
            case 'PM10':
                return 'PM10';
            case 'PM2.5':
                return 'PM2.5';
            case 'NO2':
                return 'NO2';

            default:
                return component;
        }
    }
}
