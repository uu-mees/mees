/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

export interface IPlatforms {
    WEB: string;
    MOBILE_WEB: string;
    MOBILE_NATIVE: string;
}

export interface IOperatingSystems {
    WINDOWS_PHONE: string;
    ANDROID: string;
    IOS: string;
    UNKNOWN: string;
}

export class Config {

    // Supported platforms
    public static PLATFORMS: IPlatforms = {
        WEB: 'web',
        MOBILE_WEB: 'mobile_web',
        MOBILE_NATIVE: 'mobile_native'
    };

    // Possible operating systems
    public static OPERATINGSYSTEMS: IOperatingSystems = {
        WINDOWS_PHONE: 'windows_phone',
        ANDROID: 'android',
        IOS: 'ios',
        UNKNOWN: 'unknown'
    };

    // Current target (defaults to web)
    public static PLATFORM_TARGET: string = Config.PLATFORMS.WEB;

    // Current operating system
    public static OS_TARGET: string = Config.OPERATINGSYSTEMS.ANDROID;

    /** Check if web browser. */
    public static get IS_WEB(): boolean {
        return Config.PLATFORM_TARGET === Config.PLATFORMS.WEB;
    }

    /** Check if native mobile application. */
    public static get IS_MOBILE_NATIVE(): boolean {
        return Config.PLATFORM_TARGET === Config.PLATFORMS.MOBILE_NATIVE;
    }

    /** Check if mobile browser. */
    public static get IS_MOBILE_WEB(): boolean {
        return Config.PLATFORM_TARGET === Config.PLATFORMS.MOBILE_WEB;
    }

    /** Check if mobile. */
    public static get IS_MOBILE(): boolean {
        return (this.IS_MOBILE_NATIVE || this.IS_MOBILE_WEB);
    }

    /** Check if Windows Phone. */
    public static get IS_WINDOWS(): boolean {
        return Config.OS_TARGET === Config.OPERATINGSYSTEMS.WINDOWS_PHONE;
    }

    /** Check if android. */
    public static get IS_ANDROID(): boolean {
        return Config.OS_TARGET === Config.OPERATINGSYSTEMS.ANDROID;
    }

    /** Check if iOS. */
    public static get IS_IOS(): boolean {
        return Config.OS_TARGET === Config.OPERATINGSYSTEMS.IOS;
    }

    /** Check if OS is known. */
    public static get IS_OS(): boolean {
        return Config.OS_TARGET !== Config.OPERATINGSYSTEMS.UNKNOWN;
    }
}
