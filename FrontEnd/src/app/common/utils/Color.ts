/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

export class Color {

    /** Return color based on exposure value. */
    public static getExposureColor(exposure: number): string {
        
        // Return a grey color when no exposure (or 0) is given
        if (!exposure) return '#d3d3d3';

        return '#' + 
            Color.numberToHex(-254.9 + 12.75 * exposure) +              // Red: starts low, then increases
            Color.numberToHex(0.159375 * exposure * (80 - exposure)) +  // Green: starts and ends low, highest around 40
            Color.numberToHex(765 - 12.75 * exposure);                  // Blue: starts high, then decreases
    } // Plot of the functions: https://www.wolframalpha.com/input/?i=Plot%5B%7B765+-+12.75x,+-255+%2B+12.75x,+0.159+(80-+x)+x%7D,+%7Bx,+-20,+100%7D,+%7By,+0,+255%7D%5D

    public static numberToHex(f: number): string {
        if(f > 255) return 'FF';
        if(f < 0) return '00';
        var s = Math.floor(f).toString(16);
        if(s.length == 1) return '0' + s;
        else return s;
    }

    // Return color based on rating value
    public static getRatingColor(rating: number) {        
        if (rating >= 4.5) return '#0020c5';
        if (rating >= 3.5) return '#2dcdfb';
        if (rating >= 2.5) return '#fffed0';
        if (rating >= 1.5) return '#fffc4d';
        else               return '#ff0a17';
    }
}
