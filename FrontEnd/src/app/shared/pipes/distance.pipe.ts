/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'shortMeters' })
export class ShortMetersPipe implements PipeTransform {
    transform(distance: number): string {
        if (distance < 1000) {
            return distance + 'm';
        }
        distance = Math.round((distance / 1000) * 10) / 10;
        return distance + 'km';
    }
}
