/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
import 'moment-duration-format';

@Pipe({ name: 'shortMoment' })
export class ShortMomentPipe implements PipeTransform {
    transform(mom: moment.Moment): string {
        return mom.format('H:mm');
    }
}

@Pipe({ name: 'shortDuration' })
export class ShortDurationPipe implements PipeTransform {
    transform(dur: moment.Duration): string {
        const d = <any>dur;
        return d.format('d [days] h [hours], m [min]');
    }
}
