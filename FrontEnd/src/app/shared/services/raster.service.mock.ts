/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

// Load models
import { Raster } from '../models/raster.model';

import { MockData } from 'app/common/utils/tests/MockData';

@Injectable()
export class RasterServiceMock {

    private mockData: MockData;
    private mockRasters: Array<Raster>;

    constructor(private http: HttpClient) {
        this.mockData = new MockData();

        this.mockRasters = [this.mockData.mockRaster];
    }

    /** Get mock raster. */
    private getRaster(): Raster {
        return this.mockRasters[0];
    }

    /** Handle error. */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable('Something bad happened; please try again later.');
    }
}
