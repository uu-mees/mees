/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Load models
import { UserAuthentication } from '../models/user-authentication.model';
import { UserRegistration } from '../models/user-registration.model';
import { AuthToken } from '../models/auth-token.model';
import { User } from '../models/user.model';

import { environment } from 'environments/environment';

export enum Role {
    Unauthenticated,
    Regular,
    Administrator,
}

@Injectable()
export class AuthService {

    private TOKEN_STORAGE_KEY = 'id_token';
    private jwt: JwtHelper = new JwtHelper();

    // Emits true for authenticated, false for otherwise
    public authenticationStream: BehaviorSubject<Role>;


    constructor(private http: HttpClient) {
        this.authenticationStream = new BehaviorSubject<Role>(this.getRole());
    }

    /** Get token from localstorage. */
    public getToken(): string {
        return localStorage.getItem(this.TOKEN_STORAGE_KEY);
    }

    /** Extract userID from token. */
    public getId(): string {
        const token = this.getToken();
        const decodedToken = this.jwt.decodeToken(token);
        return decodedToken['userId'];
    }

    /** Get user role. */
    public getRole(): Role {
        if (!this.isAuthenticated()) {
            return Role.Unauthenticated;
        }

        const token = this.getToken();
        const decodedToken = this.jwt.decodeToken(token);
        return Role[<string>decodedToken['userRole']];
    }

    /** Set token in localstorage. */
    public setToken(token: string) {
        localStorage.setItem(this.TOKEN_STORAGE_KEY, token);
    }

    /** Remove token from localstorage. */
    public removeToken() {
        localStorage.removeItem(this.TOKEN_STORAGE_KEY);
        this.authenticationStream.next(this.getRole());
    }

    /** Returns whether user is logged in or not. */
    public isAuthenticated(): boolean {
        const token = this.getToken();

        return tokenNotExpired(this.TOKEN_STORAGE_KEY, token);
    }

    /** Perform a login on an already registered user. */
    public performLogin(body: UserAuthentication): Promise<AuthToken> {
        const promise = this.http.post(environment.backEndUrl + 'token', body)
            .toPromise()
            .then(response => {
                this.setToken(response['token']);
                this.authenticationStream.next(this.getRole());
                if (response['token'] == null) {
                    throw new Error('token not set');
                }
                return response['token'];
            });
        return promise;
    }

    /** Perform a registration for a new  user. */
    public performRegistration(registrationBody: UserRegistration): Promise<User> {
        return this.http.post<User>(environment.backEndUrl + 'users/register', registrationBody)
            .toPromise();
    }

}
