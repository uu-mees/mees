/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';


import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { environment } from 'environments/environment';
import { MinigameScores } from '../models/minigame-scores.model';
import { Config } from 'app/common/utils';


@Injectable()
export class MinigamesService {

  // get variables from environment
  minigameUrl = environment.minigameUrl;
  minigameServerAdress = environment.minigameServerAdress;
  
  constructor(private http: HttpClient, private authService: AuthService, private userService: UserService) { }

  //make url for starting minigames, including the userID and if the user is research participant
  //although we cant get research value from the backend yet.
  makeUrl(){
      var userId = "";
      var variablesUrl = "";
      if(!this.authService.isAuthenticated() || this.userService.userResearch.value.viewMinigameScores){
          variablesUrl += "?canSeeScores";
      }
    return this.minigameUrl + variablesUrl;
  }

  //send the MinigameScores datatype off to the backend. if the user is logged in
  public sendScores(infoString){
    if(this.authService.isAuthenticated()){
        var minigameScores = this.formatScores(infoString);
        this.http.post(environment.backEndUrl + 'minigames/scores', minigameScores).toPromise();
    }
  }

  //receives scoredata in a string, and converts that to a minigameScores datatype.
  public formatScores(infoString):MinigameScores{
    //split the string into a dictonary
    var receivedValues = [];
    var splitstring = infoString.split(' ');
    for(var i = 0; i<splitstring.length; i++){
        var split = splitstring[i].split(':');
        var key = split[0];
        var value = split[1];
        receivedValues[key]=value;
    }

    //fill the minigamescores datatype
    var output = new MinigameScores()
    output.gameMode = receivedValues["mode"];
    if("score" in receivedValues)
        output.Score = receivedValues["score"];
    if("misses" in receivedValues)
        output.misses = receivedValues["misses"];
    if("timeInSec" in receivedValues)
        output.timeInSec = receivedValues["TimeInSec"];
    output.userID = this.authService.getId(); //add the userID
    output.timeStamp = moment();              //and time
    output.mobile = Config.IS_MOBILE;         //and weither or not the user is using a phone.
    return output;
  }

  public MinigameUrl(): string{
      return this.minigameUrl;
  }

  public MinigameServerAdress(): string {
      return this.minigameServerAdress;
  }
}
