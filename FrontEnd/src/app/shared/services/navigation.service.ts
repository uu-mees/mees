/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable()
export class NavigationService {

    private mainSideNav: MatSidenav;

    /** Set sidenav in service. */
    public setSideNav(nav: MatSidenav): void {
        this.mainSideNav = nav;
    }

    /** Get sidenav from service. */
    public getSideNav(): MatSidenav {
        return this.mainSideNav;
    }
}
