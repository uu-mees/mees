/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { TestBed, inject } from '@angular/core/testing';

import { MinigamesService } from './minigames.service';

describe('MinigamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MinigamesService]
    });
  });

  // it('should be created', inject([MinigamesService], (service: MinigamesService) => {
  //   expect(service).toBeTruthy();
  // }));
});
