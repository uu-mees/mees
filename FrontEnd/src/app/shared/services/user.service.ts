/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Import models
import { DailyExposure } from '../models/daily-exposure.model';
import { PasswordPair } from '../models/passwordpair.model';
import { UserProfile } from '../models/user-profile.model';
import { UserResearch } from '../models/user-research.model';

// Import services
import { AuthService } from './auth.service';

import { environment } from 'environments/environment';

@Injectable()
export class UserService {

    /** Observable user settings. */
    public userProfile = new BehaviorSubject<UserProfile>({
        username: '',
        email: '',
        trackLocation: false,
        height: 0,
        weight: 0,
        sex: '',
        dateOfBirth: new Date(),
        homeAddress: '',
        workAddress: ''
    });

    /** Observable user research. */
    public userResearch = new BehaviorSubject<UserResearch>({
        id: '',
        name: '',
        description: '',
        startTime: new Date(2019, 1, 1),
        endTime: null,
        viewRatings: true,
        viewSensors: true,
        viewRaster: true,
        viewExposure: true,
        viewMinigameScores: true,
        viewHighScores: true,
        viewRoutePlanner: true
    });

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    /** Deletes the current user. */
    public deleteCurrentUser(): Promise<any> {
        const promise = this.http.delete(environment.backEndUrl + 'users').toPromise();
        return promise;
    }

    /** Gets daily exposure information (just exposure) of current user on a given day. */
    public getDailyExposures(dateFrom: Date, dateTo: Date): Promise<DailyExposure[]> {
        const params = {
            interval: [dateFrom.toISOString(), dateTo.toISOString()].join(',')
        };

        return this.http.get<DailyExposure[]>(environment.backEndUrl + 'users/exposure',
            { params: params })
            .toPromise();
    }

    /** Synchronize locally edited user profile with back-end. */
    public updateUserProfile(body: UserProfile): Promise<any> {
        const promise = this.http.post(environment.backEndUrl + 'users/settings/UserProfile', body).toPromise()
            .then(response => {
                this.saveUserProfile(body);
                return response as UserProfile;
            });
        return promise;
    }

    /** Request to back-end to change password. */
    public changePassword(body: PasswordPair): Promise<any> {
        const promise = this.http.post(environment.backEndUrl + 'users/settings/password', body).toPromise()
            .then(response => {
                return response;
            });
        return promise;
    }

    /** Retrieves userinfo from back-end through requestUserProfile. */
    public getUserProfile(): void {
        this.requestUserProfile()
            .subscribe(result => {
                this.saveUserProfile(result);
            }, error => {
                console.error('Unable to retrieve settings.');
            });
    }

    /** Updates observable with new UserProfile. */
    private saveUserProfile(settings: UserProfile): void {
        this.userProfile.next(settings);
    }

    /** Retrieves user research restrictions from back-end through requestUserResearch. */
    public getUserResearch(): void {
        this.requestUserResearch()
            .subscribe(result => {
                this.saveUserResearch(result);
            }, error => {
                console.error('Unable to retrieve research restrictions of user.');
            });
    }

    /** Updates observable with new Research for User */
    private saveUserResearch(research: UserResearch): void {
        this.userResearch.next(research);
    }

    /** Requests back-end to generate a user token for account recovery. */
    public requestUserRecoveryToken(email: string) {
        const params = {
            email: email
        };
        return this.http.get(environment.backEndUrl + 'users/settings/token', { params: params}).toPromise();
    }

    /** Send password reset request to back-end.  */
    public resetUserPassword(token: string, newPassword: string, confirmPassword: string) {
        const body = {
            token: token,
            newPassword: newPassword,
            confirmPassword: confirmPassword
        };
        return this.http.post(environment.backEndUrl + 'users/settings/token', body).toPromise();
    }

    /** Request user info from back-end containing user preferences and limited information. */
    private requestUserProfile(): Observable<UserProfile> {
        return this.http.get<UserProfile>(environment.backEndUrl + 'users/settings/UserProfile');
    }

    /** Request the research the user is currently involved in from the back-end. */
    private requestUserResearch(): Observable<UserResearch> {
        return this.http.get<UserResearch>(environment.backEndUrl + 'users/research');
    }
}
