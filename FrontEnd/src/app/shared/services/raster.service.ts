/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

// Load models
import { Raster } from '../models/raster.model';

import { environment } from 'environments/environment';

@Injectable()
export class RasterService {
    // Poll location for raster data
    rasterUrl = environment.backEndUrl + 'raster/';

    constructor(private http: HttpClient) { }

    /** Create and perform a get request to get the latest raster. */
    getRaster(timestamp: Date): Promise<Raster> {    
        if((new Date(timestamp).toUTCString()).toString() == "Invalid Date") {
            timestamp = new Date(Date.UTC(1970, 1, 1, 1, 0, 0))
        }

        // Specify area to retrieve and add the timestamp
        const params = {
          timestamp: (timestamp === null) ? null : new Date(timestamp).toUTCString()
        };

        return this.http.get<Raster>(this.rasterUrl, {params: params})
            .toPromise();
    }
    
    /** Create and perform the exposure call to the back-end. */
    getExposure(latlng: number[]): Promise<number> {
        const params = {
            position: latlng.join(',')
        };

        return this.http.get<number>(this.rasterUrl + 'exposure', { params: params})
            .toPromise();

    }

    
    /** Handle error. */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable('Something bad happened; please try again later.');
    }
}
