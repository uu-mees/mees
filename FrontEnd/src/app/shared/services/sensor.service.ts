/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { environment } from 'environments/environment';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';

// Load models
import { Sensor } from '../models/sensor.model';
import { SensorBatch } from '../models/sensorbatch.model';

import * as L from 'leaflet';

@Injectable()
export class SensorService {
    sensorUrl = environment.backEndUrl;

    constructor(private http: HttpClient) { }

    /** Get all the historical data from a specific sensor. */
    public getHistoricalSensorData(id: string, timeStampFrom: string, timeStampTo: string) {
        const getUrl = this.sensorUrl + 'sensors/' + id + '/batches';

        const params = {
            interval: [new Date(timeStampFrom).toISOString(), new Date(timeStampTo).toISOString()].join(',')
        };

        const promise = this.http.get<Array<Sensor>>(getUrl, { params: params })
            .toPromise()
            .then(response => {
                return response as Array<Sensor>;
            })
            .catch((error) => catchError(this.handleError));

        return promise;
    }

    /** Get all the latest sensorbatches within a region. */
    public getLatestSensorBatches(latlng: L.LatLngBounds) {
        const getUrl = this.sensorUrl + 'sensors/batch';

        const params = {
            area: [latlng.getSouth(), latlng.getWest(), latlng.getNorth(), latlng.getEast()].join(',')
        };

        return this.http.get<Array<SensorBatch>>(getUrl, { params: params })
            .toPromise()
            .catch((error) => this.handleError(error));
    }

    /** Handle error. */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable('Something bad happened; please try again later.');
    }
}
