/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import {Component, Injectable, OnDestroy} from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

// Import models
import { GeoLocation } from '../models/geolocation.model';
import { GeoResult } from '../models/georesult.model';
import { RouteLine } from '../models/routeline.model';
import { GeoCodes } from '../models/geocode.model';

import { AuthService } from './auth.service';
import { UserService } from './user.service';

import * as L from 'leaflet';

export enum MapState {
    Default,
    RoutePlanner,
    ExposureOverview,
    Presentation
}

@Injectable()
export class MapService implements OnDestroy {

    /** Map */
    public map;

    /** Mapstate */
    public mapState = new BehaviorSubject<MapState>(MapState.Default);

    /** USP LatLng location */
    public USP = L.latLng([52.0850735, 5.1714655]);

    /** RouteLines from exposure layer */
    private routeLines: RouteLine[];

    // FeatureGroup BehaviourSubject updating for the markers on the map
    private markers = new BehaviorSubject<L.FeatureGroup>(new L.FeatureGroup());
    lastMarkers = this.markers.asObservable();

    // GeoLocation BehaviourSubject updating for the right click location
    private routeLocation = new BehaviorSubject<GeoLocation>(null);
    lastLocation = this.routeLocation.asObservable();

    // GeoLocation BehaviourSubject updating for the longpress location
    private longPressLocation = new BehaviorSubject<GeoLocation>(null);
    lastLongPressLocation = this.longPressLocation.asObservable();

    // LatLng BehaviourSubject updating to the current location
    public currentLocation = new BehaviorSubject<L.LatLng>(null);
    lastCurrentLocation = this.currentLocation.asObservable();

    // Is the location currently watched
    private mapIsLocating = false;

    private created = false;

    private currentLocationMarker: L.FeatureGroup = new L.FeatureGroup();

    private tileUrl = 'https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png';
    private tileAttribution = 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OSM Sweden</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OSM</a>';

    private settingsSubscription: Subscription;
    private authSubscription: Subscription;

    constructor(private authService: AuthService, private userService: UserService) { }

    /** Creation of the map. */
    public createMap() {

        // Init map
        if(!this.created) {
            this.map = L.map('mapid', {
                maxBounds: [[85, -180], [-70, 180]],
                maxBoundsViscosity: 0.9,
                zoomControl: false
            }).setView(this.USP, 16);
            this.created = true;
        }

        this.map.on('locationfound', (e) => this.locationFound(e));
        this.map.on('locationerror', (e) => this.locationError(e));

        // Get and start watching current location and center to this value.
        // This requires user permission: get the appropriate settings before executing.
        // Multiple subscriptions are being managed here:
        // authsubscription for when the authentication status changes
        // settingssubscription for when the user settings.tracklocation changes
        // and mapislocation (indirect subscription) to see if leaflet is actively monitoring user location
        this.authSubscription = this.authService.authenticationStream.subscribe(() => {
            if (this.authService.isAuthenticated()) {
                if (!!this.settingsSubscription) {
                    this.settingsSubscription = this.userService.userProfile.subscribe(settings => {
                        if (settings.trackLocation) { // User is logged in and wants to be tracked.
                            if (!this.mapIsLocating) { this.watchCurrentLocation(); }
                        } else { // User is logged in and does not want to be tracked.
                            if (this.mapIsLocating) { this.stopLocate(); }
                        }
                    });
                }
            } else { // User is not logged in, we start tracking them.
                if (!!this.settingsSubscription) { this.settingsSubscription.unsubscribe(); }
                if (!this.mapIsLocating) { this.watchCurrentLocation(); }
            }
        });

        // Retrieve map data
        L.tileLayer(this.tileUrl, {
            attribution: this.tileAttribution,
            minZoom: 3,
            maxZoom: 18
        }).addTo(this.map);

        // Set view to initial user location. If user doesn't want to be tracked this will setview to USP.
        this.setView(this.currentLocation.value == null ? this.USP : this.currentLocation.value);
    }

    /** Add a routeLine to array */
    public addRouteLine(routeLine: RouteLine): void {
        this.routeLines.push(routeLine);
    }

    /** Check is user is logged in */
    public checkLoggedIn(): boolean {
        return this.authService.isAuthenticated();
    }

    /** Check if user is allowed to view the raster */
    public checkRasterVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewRaster;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the exposure */
    public checkExposureVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewExposure;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the route planner */
    public checkRoutePlannerVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewRoutePlanner;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the ratings */
    public checkRatingsVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewRatings;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the sensors */
    public checkSensorsVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewSensors;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the high scores */
    public checkHighScoresVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewHighScores;
        } else {
            return false;
        }
    }

    /** Check if user is allowed to view the minigame scores */
    public checkMinigameScoresVisible(): boolean {
        if (this.checkLoggedIn()) {
            return this.userService.userResearch.value.viewMinigameScores;
        } else {
            return false;
        }
    }

    /** Clear routeLine array */
    public clearRouteLines(): void {
        this.routeLines = [];
    }

    /** Set LatLng position someone has clicked. */
    public setLocation(location: GeoLocation, latlng?: L.LatLng): void {

        // Create and set marker
        latlng = !latlng ? new L.LatLng(location.lat, location.lng) : latlng;
        const marker = L.marker(latlng);

        this.addMarker(marker, location.routeOption);

        // Set location as the next location
        this.routeLocation.next(location);
    }

    /** Set LatLng position some longpressed on. */
    public setLongPressLocation(location: GeoLocation, latlng: L.LatLng): void {

        this.clearMarkers();

        // Create and set marker
        const marker = L.marker(latlng);
        this.addMarker(marker, location.routeOption);

        // Set location as the next location
        this.longPressLocation.next(location);
    }

    /** Create GeoLocation based on a position and a GeoResult. */
    public createLocation(position: L.LatLng, routeOption: number, geoCodes: GeoCodes): GeoLocation {
        let result: GeoResult, location: GeoLocation;

        // Create a 'Selected' GeoLocation if no result was found
        if (geoCodes.results.length === 0) {
            result = new GeoResult('', position.lat, position.lng);
            location = new GeoLocation(result, 2, routeOption);
        } else {
            result = geoCodes.results[0];
            location = new GeoLocation(result, 0, routeOption);
        }

        return location;
    }


    /** Get the GPS coordinates of the device. */
    public getCurrentLocation(setView: boolean = false): L.LatLng {
        if (setView) {
            if (!!this.currentLocation.value) {
                this.map.setView(this.currentLocation.value, 16);
                this.addCurrentLocationMarker(this.currentLocation.value);
            }
        }

        return this.currentLocation.value;
    }

    /** Watch the GPS coordinates of the device. */
    public watchCurrentLocation(): Observable<L.LatLng> {
        const options = {
            setView: false,
            maxZoom: 16,
            watch: true,
            enableHighAccuracy: true,
            maximumAge: Infinity,
            timeout: 10000,
        };

        // Only locate if not yet locating.
        if (!this.mapIsLocating) {
            this.map.locate(options); this.mapIsLocating = true;
        }

        return this.lastCurrentLocation;
    }

    /** Focus location of the user's device. */
    public focusCurrentLocation() {
        this.map.setView(this.currentLocation.value, 16);
    }

    /** Stop watching the GPS coordinates of the device. */
    public stopLocate() {
        this.mapIsLocating = false;
        this.map.stopLocate();
    }

    /** Set current location. */
    public setCurrentLocation(latlng: L.LatLng): void {
        if (!!latlng && latlng !== L.latLng(0, 0)) {
            this.currentLocation.next(latlng);
        }
    }

    /** Create marker on the position a user clicked. */
    public addMarker(marker: L.Marker, routeOption: number): void {
        // Remove existing markers
        if (!marker) {
            this.clearMarkers();
        }

        // Add marker to the map
        if (!!marker) {
            marker.addTo(this.markers.value);

            this.markers.next(this.markers.value);
            this.markers.value.addTo(this.map);
        }
    }

    /** Clear marker of the map. */
    public clearMarkers(): void {
        this.map.removeLayer(this.markers.value);
        this.markers.next(new L.FeatureGroup());
    }

    /** Clear routing variables. */
    public clearRouting(): void {
        this.routeLocation.next(null);
    }

    /** Reset marker and position. */
    public reset(): void {
        this.clearMarkers();
        this.clearRouting();
    }

    /** Fit bounds to a route */
    public fitRoute(route: any, params: object): void {
        this.map.fitBounds(route.getBounds(), params);
    }

    /** Fit bounds to a routeLine and set opacity of other routeLines */
    public fitRouteLine(routeLine: RouteLine, params: object, fitBounds: boolean = true): void {
        if (fitBounds) {
            this.map.fitBounds(routeLine.activeLine.getBounds(), params);
        }
        routeLine.routeLine.bringToFront();

        routeLine.activeLine.setStyle({ opacity: 1 });
        routeLine.routeLine.setStyle({ opacity: 1 });
        for (const line of this.routeLines) {
            if (line !== routeLine) {
                line.activeLine.setStyle({ opacity: 0.4 });
                line.routeLine.setStyle({ opacity: 0.4 });
            }
        }
    }

    /** Fit bounds to all markers */
    public fitMarkers(params: object): void {
        this.map.fitBounds(this.markers.value.getBounds(), params);
    }

    /** Center view on a marker and set routeLine opacities */
    public fitMarker(pos: L.latlng): void {
        this.setView(pos);

        for (const line of this.routeLines) {
            line.activeLine.setStyle({ opacity: 0.4 });
            line.routeLine.setStyle({ opacity: 0.4 });
        }
    }

    /** Center view on a position */
    public setView(pos: L.LatLng): void {
        this.map.setView(pos);
    }

    /** Success function for leaflet event locationfound handling. */
    private locationFound(e: any): void {
        const latlng = L.latLng(e.latlng);
        this.setCurrentLocation(latlng);

        // Add the marker
        this.addCurrentLocationMarker(latlng);
    }

    /** Add a blue marker on the given location. */
    private addCurrentLocationMarker(latlng: L.LatLng) {
        const marker: L.FeatureGroup = new L.FeatureGroup();

        const markerInside = L.circleMarker(latlng, {
            radius: 5,
            color: 'white',
            fill: true,
            fillColor: '#3377ff',
            fillOpacity: 1,
            weight: 0.8,
            interactive: false
        });
        const markerBorder = L.circleMarker(latlng, {
            radius: 9,
            fill: true,
            fillColor: '#3377ff',
            fillOpacity: 0.4,
            opacity: 0,
            interactive: false
        });

        this.map.removeLayer(this.currentLocationMarker);

        markerBorder.addTo(marker);
        markerInside.addTo(marker);

        this.currentLocationMarker = marker;
        this.currentLocationMarker.addTo(this.map);
    }

    /** Error function for location handling. */
    private locationError(e: any): string {

        // Get Center of the map
        const center = this.map.getCenter();

        // Set center to USP if map has center {0, 0}
        if (center.lat === 0 && center.lng === 0) {
            this.map.setView(this.USP, 16);
        }

        // TODO: TODO: SNACKBAR MESSAGE ON ERROR 1.
        // If User denied location, stop locate
        if (e.code === 1) {
            this.stopLocate();
        }

        // Log the error message
        console.error(e.message);

        // Return the error message
        return e.message;
    }

    /** cleanup subscriptions on component destruction */
    ngOnDestroy() {
        if (!!this.settingsSubscription) { this.settingsSubscription.unsubscribe(); }
        if (!!this.authSubscription) { this.authSubscription.unsubscribe(); }
        if (this.mapIsLocating) { this.stopLocate(); }
    }
}
