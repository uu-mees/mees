/* /* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
    * ©Copyright Utrecht University(Department of Information and Computing Sciences)
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RasterService } from './raster.service';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../environments/environment';
import { RasterData } from '../models/rasterdata.model';
import { TestUtils } from '../../common/utils/tests/TestUtils';
import { MockData } from '../../common/utils/tests/MockData';

describe('`[Integration] RasterService', () => {
    let mockData: MockData;

    // For Regular mock testing
    let rasterService: RasterService;
    let httpTestingController: HttpTestingController;

    // For integration testing
    let httpClientSpy: { get: jasmine.Spy };
    let actualRasterService: RasterService;

    // Set up testing environment
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            declarations: [
            ],
            providers: [
                RasterService,
            ]
        }).compileComponents();

        mockData = new MockData();

        httpTestingController = TestBed.get(HttpTestingController);
        rasterService = TestBed.get(RasterService);
    });

    afterEach(() => {
        // Assert that there are no outstanding HTTP requests
        httpTestingController.verify();
    });

    // Integration test for erronous calls
    it('should return 404 (non-mock) error from back-end', () => {
        // Spy on methods
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        actualRasterService = new RasterService(<any> httpClientSpy);

        // Expected error
        const errorResponse = new HttpErrorResponse({
          error: '404 Not Found',
          status: 404, statusText: '404 Not Found'
        });

        // Missing fourth area point
        const mockParams = mockData.faultyRasterParams;

        // Set expected error response in spy
        httpClientSpy.get.and.returnValue(TestUtils.asyncError(errorResponse));

        // Test raster service error handling
        actualRasterService.getCells(mockParams)
            .then( rasterCells => {
                if (Object.prototype.toString.call(rasterCells) === '[object RasterData]') {
                    fail('expected a 404 error, not raster cells');
                }
            })
            .catch( error => {
                // because the back-end returns an empty response
                expect(error).toContain('404');
            });
      });

      it('should retrieve valid (non-mock) raster data from back-end', () => {
        // Spy on methods
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        actualRasterService = new RasterService(<any> httpClientSpy);

        // Missing fourth area point
        const params = mockData.rasterParams;

        // Set expected error response in spy
        httpClientSpy.get.and.callThrough();

        // Test raster service error handling
        actualRasterService.getCells(params)
            .then( rasterCells => {
                if (Object.prototype.toString.call(rasterCells) === '[object RasterData]') {
                    expect(rasterCells.cells.length).toBeGreaterThan(0);
                    expect(rasterCells.timeStampPolled).toBeDefined();
                }
            })
            .catch( error => {
                fail('An unexpected error occurred when retrieving raster data.');
            });
      });
});
 */
