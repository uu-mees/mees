/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Import models
import { Questionnaire } from '../models/questionnaire.model';

// Import services
import { AuthService } from './auth.service';

import { environment } from 'environments/environment';
import { saveAs } from 'file-saver';
import * as moment from 'moment';

// Service to load and save all data required for questionnaires and the settings of questionnaires.
@Injectable()
export class QuestionnaireService {

    questUrl = environment.backEndUrl + 'questionnaire/';

    constructor(private http: HttpClient, private authService: AuthService) { }

    /** Request a questionnaire based on the triggers passed in the backend. */
    getQuestionnaire(): Promise<Questionnaire> {
        const userId = this.authService.getId();
        return this.http.get<Questionnaire>(this.questUrl, {params: { userId: userId }})
            .toPromise();
    }
}
