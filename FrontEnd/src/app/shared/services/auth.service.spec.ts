/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../environments/environment';
import { UserRegistration } from '../models/user-registration.model';
import { UserAuthentication } from '../models/user-authentication.model';

describe('AuthService', () => {
    let authService: AuthService;
    let httpMock: HttpTestingController;

    // Set up testing environment
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            declarations: [
            ],
            providers: [
                AuthService,
            ]
        }).compileComponents();

        httpMock = TestBed.get(HttpTestingController);
        authService = TestBed.get(AuthService);
    });

    it('should request a token', () => {
        const authBody: UserAuthentication = {
            username: 'testUsername',
            password: 'testPassword'
        };

        authService.performLogin(authBody);

        const http = httpMock.expectOne(environment.backEndUrl + 'token');
        expect(http.request.method).toBe('POST');
        expect(http.request.body).toBe(authBody);
    });

    it('should set received token', () => {
        const response = { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2YWxpZFVzZXIiOnRydWV9.IIDPwKOmqJ-ZvOU8RQO5Yufwh9Pc47WgiItmeevFhso' };

        const authBody: UserAuthentication = {
            username: 'testUsername',
            password: 'testPassword'
        };

        authService.performLogin(authBody).then(x =>
            // This code is executed after the HTTP flush
            expect(authService.getToken()).toBe(response['token'])
        );

        const http = httpMock.expectOne(environment.backEndUrl + 'token');
        expect(http.request.method).toBe('POST');
        http.flush(response);
    });

    it('should post registration info', () => {
        const registrationBody: UserRegistration = {
            username: 'testUsername',
            password: 'testPassword',
            email: 'testEmail',
            tracklocation: false,
            height: 0,
            weight: 0,
            sex: '',
            dateOfBirth: new Date(2000, 1, 1)
        };

        authService.performRegistration(registrationBody);

        const http = httpMock.expectOne(environment.backEndUrl + 'users/register');
        expect(http.request.method).toBe('POST');
        expect(http.request.body).toBe(registrationBody);
    });

});
