/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { SensorService } from './sensor.service';
import { HttpErrorResponse, HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../environments/environment';
import { TestUtils } from '../../common/utils/tests/TestUtils';
import { MockData } from '../../common/utils/tests/MockData';
import { SensorBatch } from '../models/sensorbatch.model';
import { Batch } from '../models/batch.model';
import { Measurement } from '../models/measurement.model';

import * as L from 'leaflet';
import { Sensor } from '../models/sensor.model';

describe('SensorService', () => {
    let mockData: MockData;

    // For Regular mock testing
    let sensorService: SensorService;
    let httpTestingController: HttpTestingController;

    // Set up testing environment
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            declarations: [
            ],
            providers: [
                SensorService,
            ]
        }).compileComponents();

        mockData = new MockData();

        httpTestingController = TestBed.get(HttpTestingController);
        sensorService = TestBed.get(SensorService);
    });

    afterEach(() => {
        // Assert that there are no outstanding HTTP requests
        httpTestingController.verify();
    });

    // Test if service can request sensor batches
    it('should request sensor data', () => {
        const corner1 = L.latLng(mockData.sensorParams[0][0], mockData.sensorParams[0][1]),
        corner2 = L.latLng(mockData.sensorParams[1][0], mockData.sensorParams[1][1]),
        bounds = L.latLngBounds(corner1, corner2);

        const params = {
            area: [(<L.latLngBounds>bounds).getSouth(), bounds.getWest(), bounds.getNorth(), bounds.getEast()].join(',')
        };

        sensorService.getLatestSensorBatches(bounds);

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'sensors/batch' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(params.area.toString());
    });

    // Test if service can request historical data
    it('should request historical data', () => {
        const params = mockData.historicalDataParams;

        // Expected params
        const httpParams = {
            interval: [new Date(params.timeStampFrom).toISOString(), new Date(params.timeStampTo).toISOString()].join(',')
        };

        sensorService.getHistoricalSensorData(params.id, params.timeStampFrom, params.timeStampTo);

        const expectedUrl = environment.backEndUrl + 'sensors/' + params.id + '/batches';

        const http = httpTestingController.expectOne(req => req.url === expectedUrl && req.params.has('interval'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('interval')).toBe(httpParams.interval.toString());
    });

    // Test if received sensor batches get handled properly
    it('should set received sensor data', () => {
        const mockBatch = mockData.mockSensorBatch;

        const corner1 = L.latLng(mockData.sensorParams[0][0], mockData.sensorParams[0][1]),
        corner2 = L.latLng(mockData.sensorParams[1][0], mockData.sensorParams[1][1]),
        bounds = L.latLngBounds(corner1, corner2);

        const httpParams = {
            area: [(<L.latLngBounds>bounds).getSouth(), bounds.getWest(), bounds.getNorth(), bounds.getEast()].join(',')
        };

        sensorService.getLatestSensorBatches(bounds)
            .then((data) => {
                expect(<Array<SensorBatch>>data).toBeDefined();
                (<Array<SensorBatch>>data).forEach((sensorBatch) => {
                    expect(sensorBatch.id.length).toBeGreaterThan(0);
                    expect(sensorBatch.label.length).toBeGreaterThan(0);
                    expect(sensorBatch.latitude).toBeGreaterThan(0);
                    expect(sensorBatch.longitude).toBeGreaterThan(0);
                    expect((<Batch>sensorBatch.batch).id.length).toBeGreaterThan(0);
                    expect((<Batch>(sensorBatch).batch).sensorId.length).toBeGreaterThan(0);
                    expect((<Batch>(sensorBatch).batch).timeStampFrom.length).toBeGreaterThan(0);
                    expect((<Batch>(sensorBatch).batch).timeStampTo.length).toBeGreaterThan(0);
                    expect((<Batch>(sensorBatch).batch).timeStampPolled.length).toBeGreaterThan(0);
                    (<Batch>(sensorBatch).batch).measurements.forEach((measurement) => {
                        expect((<Measurement>measurement).id.length).toBeGreaterThan(0);
                        expect((<Measurement>measurement).batchId.length).toBeGreaterThan(0);
                        expect((<Measurement>measurement).value).toBeGreaterThan(0);
                        expect((<Measurement>measurement).component.length).toBeGreaterThan(0);
                        expect((<Measurement>measurement).unit.length).toBeGreaterThan(0);
                    });
                });
            })
            .catch((error) => { fail('SensorBatch request failed.'); });

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'sensors/batch' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(httpParams.area.toString());
        http.flush(mockBatch);
    });

    // Test if service can request historical data
    it('should set received historical data', () => {
        const mockHistoricalData = mockData.mockHistoricalData;

        const params = mockData.historicalDataParams;

        // Expected params
        const httpParams = {
            interval: [new Date(params.timeStampFrom).toISOString(), new Date(params.timeStampTo).toISOString()].join(',')
        };

        sensorService.getHistoricalSensorData(params.id, params.timeStampFrom, params.timeStampTo)
        .then((sensors) => {
            expect(<Array<Sensor>>sensors).toBeDefined();
            (<Array<Sensor>>sensors).forEach((sensor) => {
                expect(sensor.id.length).toBeGreaterThan(0);
                // The rest of the properties of a sensor are optional
            });
        }).catch((error) => {

        });

        const expectedUrl = environment.backEndUrl + 'sensors/' + params.id + '/batches';

        const http = httpTestingController.expectOne(req => req.url === expectedUrl && req.params.has('interval'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('interval')).toBe(httpParams.interval.toString());
        http.flush(mockHistoricalData);
    });

    // Test if service handles bad request error
    it('should handle 400 error with sensor batch retrieval', () => {
        // Expected error
        const errorResponse = new ErrorEvent('400 Bad Request');

        const corner1 = L.latLng(mockData.sensorParams[0][0], mockData.sensorParams[0][1]),
        corner2 = L.latLng(mockData.sensorParams[1][0], mockData.sensorParams[1][1]),
        bounds = L.latLngBounds(corner1, corner2);

        const httpParams = {
            area: [(<L.latLngBounds>bounds).getSouth(), bounds.getWest(), bounds.getNorth(), bounds.getEast()].join(',')
        };

        sensorService.getLatestSensorBatches(bounds)
            .then((sensorBatches) => {
                // Should not come here
                if (Object.prototype.toString.call(sensorBatches) === '[[object SensorBatch]]') {
                    fail('expected a 400 error, not sensor batches');
                }
            })
            .catch((error) => {
                expect(error.error).toBe('400 Bad Request');
            });

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'sensors/batch' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(httpParams.area.toString());

        http.error(errorResponse);
    });

    // Test if service handles bad request error
    it('should handle 400 error with historical data retrieval', () => {
        // Expected error
        const errorResponse = new ErrorEvent('400 Bad Request');

        const params = mockData.historicalDataParams;

        // Expected params
        const httpParams = {
            interval: [new Date(params.timeStampFrom).toISOString(), new Date(params.timeStampTo).toISOString()].join(',')
        };

        sensorService.getHistoricalSensorData(params.id, params.timeStampFrom, params.timeStampTo)
        .then((sensors) => {
            // Should not come here
            if (Object.prototype.toString.call(sensors) === '[[object Sensor]]') {
                fail('expected a 400 error, not historical sensor data');
            }
        }).catch((error) => {
            expect(error.error).toBe('400 Bad Request');
        });

        const expectedUrl = environment.backEndUrl + 'sensors/' + params.id + '/batches';

        const http = httpTestingController.expectOne(req => req.url === expectedUrl && req.params.has('interval'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('interval')).toBe(httpParams.interval.toString());
        http.error(errorResponse);
    });
});
