/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { Timestamp } from 'rxjs/operators/timestamp';

// Load models
import { Sensor } from '../models/sensor.model';
import { Batch } from '../models/batch.model';
import { Measurement } from '../models/measurement.model';
import { SensorBatch } from '../models/sensorbatch.model';
import { MockData } from 'app/common/utils/tests/MockData';

import * as L from 'leaflet';

@Injectable()
export class SensorServiceMock {
    private batchIdx = -1;
    private mockData: MockData;
    private mockBatches: Array<Array<SensorBatch>>;
    private historicalPromise: Promise<any>;
    private batchesPromise: Promise<any>;

    sensorUrl = environment.backEndUrl;

    constructor(private http: HttpClient) {
        this.mockData = new MockData();

        this.mockBatches = [this.mockData.mockSensorBatch, this.mockData.mockSensorBatch2];
    }

    /** Get all the historical data from a specific sensor */
    public getHistoricalSensorData(id: string, timeStampFrom: string, timeStampTo: string) {
        this.historicalPromise = new Promise<Array<Sensor>>((resolve, reject) => {
            resolve(this.mockData.mockHistoricalData);
        });

        return this.historicalPromise;

    }

    /** Get all the latest sensorbatches within a region */
    public getLatestSensorBatches(latlng: L.LatLngBounds) {
        this.nextMockBatches();

        // Not actual real-time data from the back-end
        const mockBatch = this.mockBatches[this.batchIdx];

        this.batchesPromise = new Promise<Array<SensorBatch>>((resolve, reject) => {
            resolve(mockBatch);
        });

        return this.batchesPromise;

    }

    /** Increases the index of the mock batch to get from the available mock data */
    nextMockBatches() {

        // Prevent running out of bounds
        if (this.batchIdx < (this.mockBatches.length - 1)) {
            this.batchIdx++;
        }
    }

    private handleError(error: HttpErrorResponse) {

        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }

        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable('Something bad happened; please try again later.');

    }

}







