/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { environment } from '../../../environments/environment';
import { MapService } from './map.service';
import { TestUtils } from '../../common/utils/tests/TestUtils';
import { MockData } from '../../common/utils/tests/MockData';

import * as L from 'leaflet';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MapService', () => {
    let mockData: MockData;

    // For Regular mock testing
    let mapService: MapService;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            declarations: [
            ],
            providers: [
                AuthService,
                UserService,
                MapService
            ]
        }).compileComponents();

        mockData = new MockData();

        mapService = TestBed.get(MapService);
    }));

    afterEach(() => {
    });

    it('should instantiate', () => {
        expect(mapService).toBeDefined();
    });
});
