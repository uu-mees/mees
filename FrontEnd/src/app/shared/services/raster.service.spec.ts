/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RasterService } from './raster.service';
import { HttpErrorResponse, HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../environments/environment';
import { TestUtils } from '../../common/utils/tests/TestUtils';
import { MockData } from '../../common/utils/tests/MockData';

describe('RasterService', () => {
    let mockData: MockData;

    // For Regular mock testing
    let rasterService: RasterService;
    let httpTestingController: HttpTestingController;

    // Set up testing environment
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            declarations: [
            ],
            providers: [
                RasterService,
            ]
        }).compileComponents();

        mockData = new MockData();

        httpTestingController = TestBed.get(HttpTestingController);
        rasterService = TestBed.get(RasterService);
    });

    afterEach(() => {
        // Assert that there are no outstanding HTTP requests
        httpTestingController.verify();
    });

    // Test that basic raster request works without a timestamp.
    it('should request raster', () => {
        rasterService.getRaster(null);

        const http = httpTestingController.expectOne(req => req.url.includes('raster'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('timestamp')).toBe(null);
    });

    it('should send timestamp if there is one', () => {
        const date: Date = new Date('2018-05-10T17:00:00');
        rasterService.getRaster(date);

        const http = httpTestingController.expectOne(req => req.url.includes('raster'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.has('timestamp')).toBeTruthy();
    });
/*
    // Test if service can request raster data
    it('should request raster cells', () => {
        const mockParams = {
            area: [52.082275226931465, 5.162544250488281, 52.08787248903425, 5.180386304855347]
        };

        rasterService.getCells(mockParams.area);

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'raster/' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(mockParams.area.toString());
    });

    // Test if received raster data gets handled properly
    it('should set received raster cells', () => {
        const mockRaster = mockData.mockRaster;

        const mockParams = mockData.rasterParams;

        rasterService.getCells(mockParams)
            .then((data) => {
                expect(data).toBeDefined();
                expect(data.cells.length).toBeGreaterThan(0);
                expect(data.timeStampPolled.length).toBeGreaterThan(0);
            })
            .catch((error) => { fail('Raster cell request failed.'); });

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'raster/' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(mockParams.toString());
        http.flush(mockRaster);
    });

    // Test if service handles errors when not enough parameters are specified
    it('should handle 404 error', () => {
        // Missing 4th paramter
        const mockParams = mockData.faultyRasterParams;

        // Expected error
        const errorResponse = new ErrorEvent('404 Not Found');

        rasterService.getCells(mockParams)
            .then((rasterCells) => {
                // Should not come here
                if (Object.prototype.toString.call(rasterCells) === '[object RasterData]') {
                    fail('expected a 404 error, not raster cells');
                }
            })
            .catch((error) => { expect(error.error).toBe('404 Not Found'); });

        const http = httpTestingController.expectOne(req => req.url === environment.backEndUrl + 'raster/' && req.params.has('area'));
        expect(http.request.method).toBe('GET');
        expect(http.request.params.get('area')).toBe(mockParams.toString());

        http.error(errorResponse);
    });
    */
});
