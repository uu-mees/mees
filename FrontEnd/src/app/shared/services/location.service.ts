/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';

import 'rxjs/add/observable/of';

// Load models
import { GeoCodes } from '../models/geocode.model';

@Injectable()
export class LocationService {

    constructor(private http: HttpClient) { }

    /** Get GeoLocation based on an adress. */
    getGeoLocationAddress(address: string): Observable<GeoCodes> {
        const url = environment.backEndUrl + 'geocoding';
        const params = { address: address };
        return this.http.get<GeoCodes>(url, { params: params });
    }

    /** Get GeoLocation based on latitude and longitude. */
    getGeoLocationLatLng(latitude: number, longitude: number): Observable<GeoCodes> {
        const url = environment.backEndUrl + 'geocoding/reverse';
        const params = {
            position: [latitude.toString(), longitude.toString()].join(',')
        };

        return this.http.get<GeoCodes>(url, { params: params });
    }
}
