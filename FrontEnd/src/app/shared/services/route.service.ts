/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Load models
import { Routings } from '../models/route.model';
import { GeoLocation } from '../models/geolocation.model';

import { environment } from 'environments/environment';

import * as L from 'leaflet';

@Injectable()
export class RouteService {

    private routeLocations = new BehaviorSubject<Array<GeoLocation>>([]);
    latestRoute = this.routeLocations.asObservable();

    private searchingRoute = false;

    private routeTransportation = 'bike';

    constructor(private http: HttpClient) {
        this.routeLocations.next(Array<GeoLocation>(3).fill(null));
    }

    /** Retrieve routes (standard shortest route on bike). */
    public getRoute(from: L.LatLng, dest: L.LatLng, weighting: string = 'shortest'): Observable<any> {
        const getUrl = environment.backEndUrl + 'routing';

        // Set parameters for query
        const params = {
            route: [from.lat, from.lng, dest.lat, dest.lng, this.routeTransportation, weighting].join(',')
        };

        // Return the server response
        return this.http.get<Routings>(getUrl, { params: params })
            .pipe(
                catchError(this.handleError)
            );
    }

    // FIXME: Wait for real backend implementation
    /** Retrieve route via location. */
    public getRoutevia(from: L.LatLng, via: L.LatLng, dest: L.LatLng, weighting: string = 'shortest'): Observable<any> {
        const getUrl = environment.backEndUrl + 'routing/via';

        // Set parameters for query
        const params = {
            route: [from.lat, from.lng, via.lat, via.lng, dest.lat, dest.lng, this.routeTransportation, weighting].join(',')
        };

        // Return the server response
        return this.http.get<Routings>(getUrl, { params: params })
            .pipe(
                catchError(this.handleError)
            );
    }

    /** Get fastest route. */
    public getFastestRoute(from: L.LatLng, dest: L.LatLng): Observable<any> {
        return this.getRoute(from, dest, 'fastest');
    }

    /** Get shortest route. */
    public getShortestRoute(from: L.LatLng, dest: L.LatLng): Observable<any> {
        return this.getRoute(from, dest, 'shortest');
    }

    /** Update route locations to it's current value */
    public setLocation(locations: Array<GeoLocation>): void {
        this.routeLocations.next(locations);
    }

    /** Get GeoLocation from the route locations. */
    public getLocation(index: number): GeoLocation {
        return this.routeLocations.value[index];
    }

    /** Set transporation type. */
    public setTransportation(transport: string): void {
        this.routeTransportation = transport;
    }

    /** Reset the route locations. */
    public resetLocations(): void {
        this.routeLocations.next(Array<GeoLocation>(3).fill(null));
    }

    /** Returns true if the service is searching for a route. */
    public isSearchingRoute(): boolean {
        return this.searchingRoute;
    }

    public noRoute() {
        this.routeLocations.value.some(loc => {
            return !loc;
        });

        return true;
    }

    /** Changes the state of the search. */
    public setSearchingRoute(searching: boolean): void {
        this.searchingRoute = searching;
    }

    // #endregion

    /** Handle error. */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }

        // Return an ErrorObservable with a user-facing error message
        return new ErrorObservable('Something bad happened; please try again later.');
    }
}
