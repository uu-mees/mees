/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';

import { AuthService } from './auth.service';

// Load models
import { Rating } from '../models/rating.model';

import { environment } from 'environments/environment';

@Injectable()
export class RatingService {

    private ratingUrl = environment.backEndUrl + 'ratings';

    constructor(private http: HttpClient, private authService: AuthService) { }

    /** Set latitude longitude and radius from which to get the current rating. */
    public getAverage(latitude: number, longitude: number, radius: number) {
        const params = {
            position: latitude.toString() + ',' + longitude.toString(),
            radius: radius.toString()
        };

        return this.http.get<number>(this.ratingUrl + '/average', { params: params })
        .pipe(
            catchError(this.handleError)
        );
    }

    /** Update the localStorage pulling new ratings from the backend. */
    public async updateStorage() {
        let latestUpdate = localStorage.getItem('latestUpdate');
        if (latestUpdate === null) {
            latestUpdate = '0';
        }

        const current = localStorage.getItem('ratings');
        let ratings = await this.http.get<Rating[]>(this.ratingUrl + '/new', {params: {from: latestUpdate}}).toPromise().then().catch((error) => this.handleError(error));
        if (current !== null) {
            ratings = JSON.parse(current).concat(ratings);
        }

        if (ratings !== null) {
            if ((<Rating[]>ratings).length > 0) {
                // Assumes the last rating in the list has the latest timestamp
                // Has to be changed to finding a maximum from the list if this
                // assumption does not hold true.
                localStorage.setItem('latestUpdate', ratings[(<Rating[]>ratings).length - 1].timeStamp);

                localStorage.setItem('ratings', JSON.stringify(ratings));
            }
        }
    }

    /** Get all ratings (regardless of the data in the localStorage) from the backend and store them. */
    public async storeAllRatings() {
        const ratings = await this.http.get<Rating[]>(this.ratingUrl + '/all').toPromise().then().catch((error) => this.handleError(error));
        if (ratings !== null) {
            if ((<Rating[]>ratings).length > 0) {
                // Assumes the last rating in the list has the latest timestamp
                // Has to be changed to finding a maximum from the list if this
                // assumption does not hold true.
                localStorage.setItem('latestUpdate', ratings[(<Rating[]>ratings).length - 1].timeStamp);

                localStorage.setItem('ratings', JSON.stringify(ratings));
            }
        }
    }

    /** Update the local storage and return all the ratings. */
    public async getRatings() {
        await this.updateStorage();
        return JSON.parse(localStorage.getItem('ratings'));
    }

    /** Send rating to the server. */
    public postRating(rating: Rating) {
        try {
            if (this.authService.isAuthenticated()) {
                rating.userID = this.authService.getId();
                return this.http.post(this.ratingUrl, rating);
            }
        } catch (error) {
            return this.handleError(error);
        }
    }

    /** Returns the polled url. */
    public getGeneratedUrl() {
        return this.ratingUrl;
    }

    /** Handle error. */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable(
            'Something bad happened; please try again later.');
    }
}
