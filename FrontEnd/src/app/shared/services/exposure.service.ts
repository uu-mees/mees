/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from 'environments/environment';

import * as L from 'leaflet';

@Injectable()
export class ExposureService {

    public lastLocation: L.LatLng = new L.LatLng(0, 0);

    exposure = new BehaviorSubject<number>(0);
    lastExposure = this.exposure.asObservable();

    constructor(private http: HttpClient) { }

    /** Get the exposure value of a location based on a LatLng location. */
    public getLocationExposure(latlng: L.LatLng): Observable<number> {
        if (latlng.lat !== this.lastLocation.lat && latlng.lng !== this.lastLocation.lng) {
            this.lastLocation = latlng;

            const url = environment.backEndUrl + 'raster/exposure';
            const params = { position: [latlng.lat, latlng.lng].join(',') };

            return this.http.get<number>(url, { params: params });
        }
        return Observable.of(this.exposure.value);
    }

    /** Set last found exposure. */
    public setLocationExposure(exposure: number) {
        this.exposure.next(exposure);
    }
}
