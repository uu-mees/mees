/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';

@NgModule({
    imports: [ CommonModule ],
    declarations: [],
    exports: [
        CommonModule,
        FormsModule,
        MaterialModule,
    ]
})
export class SharedModule { }
