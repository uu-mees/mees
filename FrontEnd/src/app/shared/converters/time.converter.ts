/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export class TimeConverter {

    /** Get time in hours and minutes from milliseconds */
    public static getTime(millisec: number) {
        // Define seconds, minutes and hours
        let seconds: string = (millisec / 1000).toFixed(0);
        let minutes: string = Math.floor(parseInt(seconds, 10) / 60).toString();
        let hours = '';

        // Set hours if the minutes surpass 59 minutes
        if (parseInt(minutes, 10) > 59) {
            hours = this.normalizeTime(Math.floor(parseInt(minutes, 10) / 60).toString());
            minutes = this.normalizeTime((parseInt(minutes, 10) - (parseInt(hours, 10) * 60)).toString());
        }
        seconds = this.normalizeTime(Math.floor(parseInt(seconds, 10) % 60).toString());

        // Return hours and minutes
        if (hours !== '') {
            return `${hours} h. ${minutes} min.`;
        }

        // Return minutes
        return `${minutes} min.`;
    }

    /** Add 0 in front of time string of length 1 */
    private static normalizeTime(time: string) {
        return (time.length === 1) ? ('0' + time) : time;
    }
}
