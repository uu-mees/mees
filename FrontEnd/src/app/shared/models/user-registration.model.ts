/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { UserAuthentication } from './user-authentication.model';

export class UserRegistration extends UserAuthentication {
    email?: string;
    tracklocation: boolean;
    height?: number;
    weight?: number;
    sex?: string;
    dateOfBirth?: Date;
    homeAddress?: string;
    workAddress?: string;
}
