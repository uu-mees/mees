/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
/** Formatted address from a specific latitude and longitude */
export class GeoResult {
    formatted_address: string;
    latitude: number;
    longitude: number;

    constructor(formatted_address: string, latitude: number, longitude: number) {
        this.formatted_address = formatted_address;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
