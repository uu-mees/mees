/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { GeoResult } from './georesult.model';
import { RouteOption } from './route-option.model';
import { GeoLocationOption } from './geolocation-option.model';

export class GeoLocation {
    address = '';
    lat = 0;
    lng = 0;
    name: string;
    locationOption: GeoLocationOption = 0;
    routeOption: RouteOption = null;
    query = '';

    /** Location with latitude and longitude, address and custom name */
    constructor(geoResult: GeoResult, locationOption?: GeoLocationOption, routeOption?: RouteOption, query?: string) {
        // If GeoResult exists, set values
        if (!!geoResult) {
            this.lat = geoResult.latitude;
            this.lng = geoResult.longitude;
            this.address = geoResult.formatted_address;
        }
        // Set the location option
        this.locationOption = locationOption;

        // Set the name of the location
        this.name = !!locationOption ? GeoLocationOption[locationOption] : this.address;

        // Set route option
        this.routeOption = routeOption;

        // Set the query of this location
        this.query = !!query ? query : '';
    }

    public getQuery(): string {
        switch (this.locationOption) {
            case 0:
                return 'a:' + this.query;
            case 1:
                return 'c:' + [this.lat, this.lng].join(',');
            case 2:
                return 's:' + [this.lat, this.lng].join(',');
            default:
                return '';
        }
    }
}
