/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import * as moment from 'moment';

export class MinigameScores {
    userID: string;
    timeStamp: moment.Moment;
    Score?: number;
    misses?: number;
    timeInSec? : number;
    gameMode: string;
    mobile: boolean;
}
