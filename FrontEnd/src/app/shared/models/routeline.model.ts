/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export class RouteLine {
    public routeLine: any;
    public activeLine: any;

    constructor(routeLine: any, activeLine: any) {
        this.routeLine = routeLine;
        this.activeLine = activeLine;
    }
}
