/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Measurement } from './measurement.model';

export class Batch {
    id: string;
    sensorId: string;
    timeStampFrom: string;
    timeStampTo: string;
    timeStampPolled: string;
    measurements: Array<Measurement>;
}
