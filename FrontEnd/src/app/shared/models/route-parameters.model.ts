/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Transportation } from './transportation.model';

export class RouteParameters {
    from: string;
    via: string;
    to: string;
    update: boolean;
    transport: string;

    constructor(params: any) {
        if (!!params.from) {
            this.from = params.from;
        }
        if (!!params.via) {
            this.via = params.via;
        }
        if (!!params.to) {
            this.to = params.to;
        }
        if (!!params.update) {
            this.update = params.update === 'true' ? true : false;
        }
        if (!!params.transport) {
            this.transport = params.transport;
        }
    }

    setUpdate(update: boolean): void {
        this.update = update;
    }
}
