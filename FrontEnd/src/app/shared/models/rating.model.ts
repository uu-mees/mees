/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Pos } from 'app/shared/models/pos.model';

export class Rating {
    position: Pos;
    rating: number;
    timeStamp: Date;
    userID: string;

    constructor(position: Pos, rating: number) {
        this.position = position;
        this.rating = rating;
        this.timeStamp = new Date(Date.now());
    }
}
