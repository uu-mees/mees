/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export class UserResearch {
    id: string;
    name: string;
    description: string;
    startTime: Date;
    endTime: Date;
    viewRatings: boolean;
    viewSensors: boolean;
    viewRaster: boolean;
    viewExposure: boolean;
    viewMinigameScores: boolean;
    viewHighScores: boolean;
    viewRoutePlanner: boolean;
}
