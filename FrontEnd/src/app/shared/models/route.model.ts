/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export class Routings {
    start: Array<number>;
    destination: Array<number>;
    vehicle: string;
    weighting: string;
    count: number;
    routes: Array<Route>;
}

export class Route {
    distance: number;
    duration: number;
    exposure: number;
    path: Array<Array<number>>;
    edgeExposures: Array<number>;
}
