/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export enum GeoLocationOption {
    'Location',             // location.normal
    'My location',          // location.personal
    'Selected location',    // location.selected
}
