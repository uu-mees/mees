/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { NgModule } from '@angular/core';
import { MatDividerModule,
         MatCardModule,
         MatCheckboxModule,
         MatSidenavModule,
         MatSnackBarModule,
         MatButtonModule,
         MatFormFieldModule,
         MatInputModule,
         MatToolbarModule,
         MatIconModule,
         MatListModule,
         MatSliderModule,
         MatGridListModule,
         MatNativeDateModule,
         MatExpansionModule,
         MatProgressSpinnerModule,
         MatSlideToggleModule,
         MatTooltipModule,
         MatProgressBarModule,
         MatSelectModule} from '@angular/material';

const modules = [
    MatButtonModule,
    MatSidenavModule,
    MatCardModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSliderModule,
    MatGridListModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatSelectModule
];

@NgModule({
    imports: modules,
    exports: modules
})
export class MaterialModule { }
