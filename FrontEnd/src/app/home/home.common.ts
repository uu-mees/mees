/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

// Import vendor dependencies
import { TranslateModule } from '@ngx-translate/core';

import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared';
import { HomeRoutes } from './home.routes';
import { HomeComponent } from './components/home/home.component';

export const SHARED_MODULES: any[] = [
    SharedModule,
    RouterModule.forChild(<any>HomeRoutes),
    TranslateModule.forChild(),
];

export const COMPONENT_DECLARATIONS: any[] = [
    HomeComponent
];
