/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { AuthService } from '../../../../shared/services/auth.service';
import { UserService } from '../../../../shared/services/user.service';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Injector, ChangeDetectorRef } from '@angular/core';
import { environment } from 'environments/environment';
import { HomeComponent } from '../../home/home.component';
import { Location } from '@angular/common';
import { AppComponent } from '../../../../app.component';
import { AppModule } from '../../../../app.module';
import { UserProfileSettingsComponent } from './user-profile-settings.component';
import { UserRegistration } from '../../../../shared/models/user-registration.model';
import { UserAuthentication } from '../../../../shared/models/user-authentication.model';
import { UserProfile } from '../../../../shared/models/user-profile.model';
import { User } from '../../../../shared/models/user.model';
import { FormBuilder } from '@angular/forms';
import { LogoutComponent } from '../../../../login/components/logout/logout.component';

describe('UserProfileSettingsComponent', () => {
    let component: UserProfileSettingsComponent;
    let appFixture: ComponentFixture<AppComponent>;
    let fixture: ComponentFixture<UserProfileSettingsComponent>;
    let authService: AuthService;
    let userService: UserService;
    let httpMock: HttpTestingController;
    let router: Router;
    let location: Location;

    // Set up testing environment
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([
                    { path: 'settings', component: UserProfileSettingsComponent },
                    { path: 'home', component: HomeComponent },
                    { path: 'logout', component: LogoutComponent }
                ])
            ],
            declarations: [
            ],
            providers: [
                UserService,
                FormBuilder
            ]
        }).compileComponents();

        httpMock = TestBed.get(HttpTestingController);
        authService = TestBed.get(AuthService);
        userService = TestBed.get(UserService);
        router = TestBed.get(Router);
        location = TestBed.get(Location);

        appFixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();

        fixture = TestBed.createComponent(UserProfileSettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        // Setup forms.
        fillValidProfileForm();
        fillValidAccountForm();
    }));



    function fillValidProfileForm() {
        // Valid Input
        component.profileForm.controls['email'].setValue('Japie@kreek.ol');
    }

    function fillValidAccountForm() {
        // Valid Input
        component.accountForm.controls['currentPassword'].setValue('20-02-2002');
        component.accountForm.controls['newPassword'].setValue('20-02-2002@@@/asdasl \'SELECT * FROM users');
        component.accountForm.controls['confirmNewPassword'].setValue('20-02-2002@@@/asdasl \'SELECT * FROM users');
    }

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should trigger profileForm.email errors', () => {
        // Required.
        component.profileForm.controls['email'].setValue('');
        expect(component.profileForm.invalid).toBeTruthy('form was valid...');
        expect(component.getEmailErrorMessage()).toEqual('Email address is required', 'Incorrect error on email... (required)');

        // Minlength = 6
        component.profileForm.controls['email'].setValue('aap');
        expect(component.profileForm.invalid).toBeTruthy('form was valid... 2');
        expect(component.getEmailErrorMessage()).toEqual('At least 6 characters required.', 'Incorrect error on email... (6 chars)');

        // Maxlength = 254 && Unknown issue with maxlength doesn't set form to 'Email address is too long.'
        component.profileForm.controls['email'].setValue('aapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaap');
        expect(component.profileForm.invalid).toBeTruthy('form was valid... 3');

        // Not a valid email address. = does not contain '@'
        component.profileForm.controls['email'].setValue('jemoeder.nl');
        expect(component.profileForm.invalid).toBeTruthy('form was valid... 4');
        expect(component.getEmailErrorMessage()).toEqual('Email address is not valid.', 'Incorrect error on email... (\'\')');
    });

    it('should trigger accountForm.currentPassword errors', () => {
        expect(component.accountForm.valid).toBe(true);

        // Required
        component.accountForm.controls['currentPassword'].setValue('');
        expect(component.accountForm.invalid).toBeTruthy();
        expect(component.getOldPasswordErrorMessage()).toEqual('Current password is required', 'Error message missing');
    });

    it('should trigger accountForm.newPassword errors', () => {
        expect(component.accountForm.valid).toBeTruthy('Accountform invalid.');

        // Required
        component.accountForm.controls['newPassword'].setValue('');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getNewPasswordErrorMessage()).toEqual('New password is required', 'Error message "required" incorrect.');

        // Minlength = 8
        component.accountForm.controls['newPassword'].setValue('aap');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getNewPasswordErrorMessage()).toEqual('At least 8 characters required.', 'Error message "at least 8" incorrect.');

        // Maxlength = 256
        component.accountForm.controls['newPassword'].setValue('aapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaap');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getNewPasswordErrorMessage()).toEqual('New password is too long.', 'Error message "" incorrect.');
    });

    it('should trigger accountForm.confirmNewPassword errors', () => {
        expect(component.accountForm.valid).toBe(true);

        // Required
        component.accountForm.controls['confirmNewPassword'].setValue('');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getConfirmPasswordErrorMessage() === 'Passwords do not match.').toBe(true);

        // Does not match newPassword.
        component.accountForm.controls['newPassword'].setValue('20-02-2002@@@/asdasl \'SELECT * FROM users');
        component.accountForm.controls['confirmNewPassword'].setValue('20-02-2002@@@/asdasl \'SELECT *');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getConfirmPasswordErrorMessage() === 'Passwords do not match.').toBe(true, 'Passwords do match...');

        // Minlength = 8
        component.accountForm.controls['confirmNewPassword'].setValue('aap');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getConfirmPasswordErrorMessage() === 'Passwords do not match.').toBe(true, 'aap did match');

        // Maxlength = 256
        component.accountForm.controls['confirmNewPassword'].setValue('aapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaapaap');
        expect(component.accountForm.invalid).toBe(true);
        expect(component.getConfirmPasswordErrorMessage() === 'Passwords do not match.').toBe(true, 'aap*85 did match');
    });

    it('should save profileform to back-end and update local .profile', () => {
        // Declare totally spies.
        const spy = spyOn(component, 'onSubmitProfile').and.callThrough();
        expect(component.GetProfileFormFailed).toBeFalsy('Profile form before request already failed for some reason.');

        component.onSubmitProfile();

        expect(spy).toHaveBeenCalled();
        // Verify that methods of .then() were called.
        expect(component.GetProfileFormFailed).toBeFalsy('Profile form get failed for some reason.');
    });

    it('should update password in back-end and redirect user', () => {
        // Set form values
        component.accountForm.controls['currentPassword'].setValue('apiedapie');
        component.accountForm.controls['newPassword'].setValue('jopiedopie');
        component.accountForm.controls['confirmNewPassword'].setValue('jopiedopie');

        // Declare totally spies.
        const spy = spyOn(component, 'onSubmitAccountForm').and.callThrough();
        expect(component.GetAccountFormFailed).toBeFalsy('Accountform did fail before request.');

        component.onSubmitAccountForm().then(x => { const http = httpMock.expectOne(environment.backEndUrl + 'users/settings/password'); });

        expect(spy).toHaveBeenCalled();
        // Verify that methods of .then() were called.
        expect(component.GetAccountFormFailed).toBeFalsy('Accountform did fail');
        expect(location.path()).toBe('/', 'Location path was not home (/).');
    });
});
