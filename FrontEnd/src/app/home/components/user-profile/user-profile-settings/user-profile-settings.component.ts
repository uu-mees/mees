/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'app/shared/services/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { PasswordPair } from 'app/shared/models/passwordpair.model';
import { UserProfile } from 'app/shared/models/user-profile.model';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { Config } from 'app/common/utils';
import { NavigationService } from 'app/shared/services/navigation.service';

@Component({
    selector: 'user-profile-settings',
    templateUrl: './user-profile-settings.component.html',
    styleUrls: [
        './user-profile-settings.component.scss'
    ]
})

export class UserProfileSettingsComponent implements OnInit, OnDestroy {
    // Define forms.
    public profileForm: FormGroup;
    public accountForm: FormGroup;

    // Define userprofile containing user information used in settings.
    private profile: UserProfile;

    // Subscription to user settings that updates the profile every time the settings change.
    settingsSubscription: Subscription;

    /* Define booleans used in mat-cards and form elements. */
    // Helpers for general elements.
    public hideCurrentPassword = true; public hideNewPassword = true; public labelPosition = 'before';
    // Helpers for profileForm.
    public profileFormPostError = undefined; public profileFormPostFailed = false; public profileGetFailed: boolean;
    // Helpers for accountForm.
    public accountFormPostError = undefined; public accountFormPostFailed = false;

    public isMobile: boolean;

    // Helpers for deleting account.
    public viewDeleteWarning = false; public disableDeleteAccount = false; public deleteFailed = false;

    constructor(private formBuilder: FormBuilder,
        private userService: UserService,
        private router: Router,
        public snackBar: MatSnackBar,
        public navigationService: NavigationService) {
        this.isMobile = Config.IS_MOBILE;
    }

    ngOnInit() {
        // Create forms.
        this.createProfileForm();
        this.createAccountForm();

        // Bind existing information from back-end. (Populate forms / cards.)
        this.settingsSubscription = this.userService.userProfile.subscribe(result => { this.setProfileValues(result); });
    }

    /** Initialize profile form. */
    private createProfileForm() {
        this.profileForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(254), this.validateEmail])],
            trackLocation: [false],
            height: [0, Validators.compose([Validators.max(999), Validators.pattern(/^[0-9]+$/)])],
            weight: [0, Validators.compose([Validators.max(999.9), Validators.pattern(/^[0-9.,]+$/)])],
            sex: [''],
            dateOfBirth: [Date.now(), Validators.compose([Validators.pattern(/^\d+[-]\d+[-]\d+$/)])],
            homeAddress: [''],
            workAddress: ['']
        });
    }

    /** Initialize account form. */
    private createAccountForm() {
        this.accountForm = this.formBuilder.group({
            currentPassword: ['', Validators.compose([
                Validators.required
            ])],
            newPassword: ['', Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(256)
            ])],
            confirmNewPassword: ['', Validators.compose([
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(256)
            ])],
        }, {
                validator: this.matchNewPasswordValidator
            });
    }

    /** Very basic email validator, given the complicated allowed structure. Slightly reduces false positives, and introduces no false negatives (of valid emails). */
    private validateEmail(control: FormControl) {
        const email = control.value;
        const count = email.split('@').length;

        if (count >= 2) {
            return null;
        } else {
            return { 'email': true };
        }
    }

    /** Validates if new passwords match. */
    private matchNewPasswordValidator(AC: AbstractControl): { [key: string]: any } {
        const password = AC.get('newPassword').value;
        const confirmPassword = AC.get('confirmNewPassword').value;

        if (password !== confirmPassword) {
            AC.get('confirmNewPassword').setErrors({ matchPassword: true });
        } else {
            return null;
        }
    }

    /** Bundles information contained in profile- and location card and synchronizes with back-end. */
    onSubmitProfile(): Promise<any> {
        if (!this.profileForm.valid) { return; }
        // Create a body to send to back-end from form entries.
        const body = {
            username: this.userService.userProfile.value.username,
            email: this.profileForm.controls.email.value,
            trackLocation: true,
            height: this.profileForm.controls.height.value,
            weight: this.profileForm.controls.weight.value,
            sex: this.profileForm.controls.sex.value,
            dateOfBirth: this.profileForm.controls.dateOfBirth.value,
            homeAddress: this.profileForm.controls.homeAddress.value,
            workAddress: this. profileForm.controls.workAddress.value
        } as UserProfile;
        // Send body to user service, which will call the back-end.
        // If the back-end is offline or fails for any other reason(invalid parameters), we inform the user.
        return this.userService.updateUserProfile(body)
            .then(x => {
                this.profileFormPostError = undefined;
                this.profileFormPostFailed = false;
                this.displaySuccessMessage('Updated Profile.');
                this.updateLocalProfile(body);
            })
            .catch(error => {
                this.profileFormPostError = error;
                this.profileFormPostFailed = true;
            });
    }

    /** Binds passwordfields to PasswordPair and sends them to back-end via userservice to update password. User is logged out if succesful. */
    onSubmitAccountForm(): Promise<any> {
        if (!this.accountForm.valid) { return; }

        // Create a body to send to back-end from form entries.
        const body = {
            OldPassword: this.accountForm.controls.currentPassword.value,
            NewPassword: this.accountForm.controls.newPassword.value
        } as PasswordPair;

        // Send body to user service, which will call the back-end.
        // If the back-end is offline or fails for any other reason(invalid parameters), we inform the user.
        return this.userService.changePassword(body)
            .then(x => {
                this.accountFormPostError = undefined;
                this.accountFormPostFailed = false;
                this.router.navigate(['logout']);
                this.displaySuccessMessage('Changed Password.', 10000, 'Login');
            })
            .catch(error => { this.accountFormPostError = error; this.accountFormPostFailed = true; });
        // TODO :: ADD ERROR MESSAGE WHY IT FAILED?
    }

    /** Deletes the user accuont. */
    deleteAccount() {
        this.disableDeleteAccount = true;
        this.userService.deleteCurrentUser()
            .then(() => { this.deleteFailed = false; this.displaySuccessMessage('Successfully deleted your account.', 10000); this.router.navigate(['logout']); })
            .catch(error => { console.error(error); this.deleteFailed = true; this.disableDeleteAccount = false; });
    }

    // Snackbar API page: https://material.angular.io/components/snack-bar/api for additional properties.
    private displaySuccessMessage(message: string, messageduration?, messageAction?) {
        // Define snackbar parameters
        let action;
        if (messageAction === 'Login') {
            action = 'Login';
        } else { action = ''; }

        // Display the snackbar.
        const snackbar = this.snackBar.open(message, action, {
            duration: (!!messageduration) ? messageduration : 3000,
        });

        // Maybe subscribe to events.
        if (action === 'Login') {
            snackbar.onAction().subscribe(() => { this.router.navigate(['/login']); });
        }
    }

    /** Updates the profile used in user-profile-settings-component with new information. */
    private updateLocalProfile(newProfile) {
        this.profile = {
            username: newProfile.username,
            email: newProfile.email,
            trackLocation: newProfile.trackLocation,
            height: newProfile.height,
            weight: newProfile.weight,
            sex: newProfile.sex,
            dateOfBirth: newProfile.dateOfBirth,
            homeAddress: newProfile.homeAddress,
            workAddress: newProfile.workAddress
        };
    }

    /** Binds result from a GET request for the user profile. */
    setProfileValues(x: UserProfile) {
        this.profile = {
            username: x.username,
            email: (x.email == null) ? '' : x.email,
            trackLocation: (x.trackLocation == null) ? false : x.trackLocation,
            height: (x.height == null) ? 0 : x.height,
            weight: (x.weight == null) ? 0 : x.weight,
            sex: (x.sex == null) ? '' : x.sex,
            dateOfBirth: (x.dateOfBirth == null) ? new Date() : x.dateOfBirth,
            homeAddress: (x.homeAddress == null) ? '' : x.homeAddress,
            workAddress: (x.workAddress == null) ? '' : x.workAddress
        };
        this.populateProfileForm();
        this.profileForm.controls.trackLocation.setValue(this.profile.trackLocation);
    }

    /** Resets the profileform if user decides to discard their changes. */
    resetProfileForm() {
        this.populateProfileForm();
    }

    /** Clears the accountform if user decides to discard their changes.  */
    resetAccountForm() {
        this.accountForm.reset();
    }

    /** Fills profileform with existing information. */
    private populateProfileForm() {
        if (this.profile != null) {
            this.profileForm.reset(this.profile);
        } else {
            this.profileForm.reset();
        }
    }

    /** public member for email used in html */
    get email() { return this.profileForm.get('email'); }

    /** public member for height used in html */
    get height() { return this.profileForm.get('height'); }

    /** public member for weight used in html */
    get weight() { return this.profileForm.get('weight'); }

    /** public member for sex used in html */
    get sex() { return this.profileForm.get('sex'); }

    /** public member for date of birth used in html */
    get dateOfBirth() { return this.profileForm.get('dateOfBirth'); }

    /** public member for the home address used in html */
    get homeAddress() { return this.profileForm.get('homeAddress'); }

    /** public member for the work address used in html */
    get workAddress() { return this.profileForm.get('workAddress'); }

    /** public member for password used in html */
    get currentPassword() { return this.accountForm.get('currentPassword'); }

    /** public member for new password used in html */
    get newPassword() { return this.accountForm.get('newPassword'); }

    /** public member for match password used in html */
    get confirmNewPassword() { return this.accountForm.get('confirmNewPassword'); }

    /** public member for profileform fail boolean used in html */
    get GetProfileFormFailed() { return this.profileFormPostError; }

    /** public member for accountform fail boolean used in html */
    get GetAccountFormFailed() { return this.accountFormPostError; }

    /** public member for profileform request fail boolean used in html */
    get GetProfileFormGetFailed() { return this.profileGetFailed; }

    /** public void for navigating app used in html */
    public navigate(path: string) {
        this.router.navigate([path]);
    }

    /** public member for email error message. */
    getEmailErrorMessage(): string {
        const errors = this.profileForm.controls.email.errors;
        if (errors == null) {
            return null;
        }
        return errors.required ? 'Email address is required' :
            errors.minlength ? 'At least 6 characters required.' :
                errors.maxlength ? 'Email address is too long.' :
                    errors.email ? 'Email address is not valid.' :
                        '';
    }

    /** public member for height error message. */
    getHeightErrorMessage(): string {
        const errors = this.profileForm.controls.height.errors;
        if (errors == null) {
            return null;
        }
        return errors.maxlength ? 'Height max 3 characters required.' :
                    errors.height ? 'Height is not valid.' :
                        '';
    }

    /** public member for weight error message. */
    getWeightErrorMessage(): string {
        const errors = this.profileForm.controls.weight.errors;
        if (errors == null) {
            return null;
        }
        return errors.maxlength ? 'Weight max 5 characters required.' :
                    errors.weight ? 'Weight is not valid.' :
                        '';
    }

    /** public member for sex error message. */
    getSexErrorMessage(): string {
        const errors = this.profileForm.controls.sex.errors;
        if (errors == null) {
            return null;
        }
        return errors.sex ? 'Sex is not valid.' :
                    '';
    }

    /** public member for date of birth error message. */
    getDateOfBirthErrorMessage(): string {
        const errors = this.profileForm.controls.dateOfBirth.errors;
        if (errors == null) {
            return null;
        }
        return errors.dateOfBirth ? 'Date of birth is not valid.' :
                    '';
    }

    /** public member for the home address error message */
    getHomeAddressErrorMessage(): string {
        const errors = this.profileForm.controls.homeAddress.errors;
        if (errors == null) {
            return null;
        }
        return errors.homeAddress ? 'Home address is not valid.' :
            '';
    }

    /** public member for the work address error message */
    getWorkAddressErrorMessage(): string {
        const errors = this.profileForm.controls.workAddress.errors;
        if (errors == null) {
            return null;
        }
        return errors.workAddress ? 'Work address is not valid.' :
            '';
    }

    /** public member for current/old password error message. */
    getOldPasswordErrorMessage(): string {
        const errors = this.accountForm.controls.currentPassword.errors;

        return errors.required ? 'Current password is required' :
            '';
    }

    /** public member for new password error message. */
    getNewPasswordErrorMessage(): string {
        const errors = this.accountForm.controls.newPassword.errors;

        return errors.required ? 'New password is required' :
            errors.maxlength ? 'New password is too long.' :
                errors.minlength ? 'At least 8 characters required.' :
                    '';
    }

    /** public member for confirm password error message. */
    getConfirmPasswordErrorMessage(): string {
        return 'Passwords do not match.';
    }

    /** clean up subscriptions on component destruction */
    ngOnDestroy() {
        if (!!this.settingsSubscription) { this.settingsSubscription.unsubscribe(); }
    }
}
