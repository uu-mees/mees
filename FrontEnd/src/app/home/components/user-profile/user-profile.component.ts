/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// Import services
import { UserService } from 'app/shared/services/user.service';

@Component({
    selector: 'user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: [
        './user-profile.component.scss'
    ]
})
export class UserProfileComponent {

    public linking = false;

    constructor(private userService: UserService, public router: Router, private activatedRoute: ActivatedRoute) {
    }

    public onSubmit(): void {
        // If the user has been deleted, redirect back to the login page
        this.userService.deleteCurrentUser()
            .then(x => this.router.navigate(['logout']))
            .catch((error) => {
                console.error(error);
            });
    }
}
