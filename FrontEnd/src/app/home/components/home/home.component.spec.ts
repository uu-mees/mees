/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators, ReactiveFormsModule, AbstractControl, ValidatorFn, NgModel } from '@angular/forms';

import { HomeComponent } from './home.component';
import { SensorService } from '../../../shared/services/sensor.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MapComponent } from '../map/map.component';
import { GraphComponent } from '../graph/graph.component';
import { RasterService } from '../../../shared/services/raster.service';
import { MaterialModule } from '../../../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteComponent } from '../route/route.component';
import { RoutePlannerComponent } from '../route/route-planner/route-planner.component';
import { RouteSelectionComponent } from '../route/route-selection/route-selection.component';
import { RatingFormComponent } from '../rating-form/rating-form.component';
import { AuthService } from '../../../shared/services/auth.service';
import { RegisterComponent } from '../../../login/components/register/register.component';
import { LoginComponent } from '../../../login/components/login/login.component';
import { LogoutComponent } from '../../../login/components/logout/logout.component';
import { Router } from '@angular/router';
import { CalendarModule } from 'angular-calendar';
import { ExposureHistoryCalendarComponent } from '../exposure/exposure-history-calendar/exposure-history-calendar.component';
import { CurrentExposureComponent } from '../current-exposure/current-exposure.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { ShortDurationPipe, ShortMomentPipe } from '../../../shared/pipes/moment.pipe';
import { ShortMetersPipe } from '../../../shared/pipes/distance.pipe';
import { QuestionnaireComponent } from '../questionnaire/questionnaire.component';
import { UserService } from '../../../shared/services/user.service';
import { NavigationService } from '../../../shared/services/navigation.service';
import { MapService } from '../../../shared/services/map.service';
import { LocationService } from '../../../shared/services/location.service';
import { RatingService } from '../../../shared/services/rating.service';
import { RouteService } from '../../../shared/services/route.service';
import { ExposureService } from '../../../shared/services/exposure.service';
import { SafePipe } from '../../../shared/pipes/safe.pipe';

@Pipe({
    name: 'translate'
})
class MockTranslatePipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
        return value;
    }
}

describe('HomeComponent', () => {
    let component: HomeComponent;
    let fixture: ComponentFixture<HomeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                FormsModule,
                MaterialModule,
                CalendarModule.forRoot(),
                RouterTestingModule.withRoutes([
                    { path: 'home', component: HomeComponent }
                ]),
                BrowserAnimationsModule
            ],
            declarations: [
                HomeComponent,
                MapComponent,
                GraphComponent,
                ExposureHistoryCalendarComponent,
                RouteComponent,
                RatingFormComponent,
                RoutePlannerComponent,
                RouteSelectionComponent,
                CurrentExposureComponent,
                UserProfileComponent,
                ShortDurationPipe,
                ShortMomentPipe,
                ShortMetersPipe,
                SafePipe,
                RatingFormComponent,
                LoginComponent,
                RegisterComponent,
                LogoutComponent,
                QuestionnaireComponent
            ], providers: [
                ExposureService,
                UserService,
                NavigationService,
                SensorService,
                RasterService,
                RatingService,
                RouteService,
                LocationService,
                MapService,
                HttpClient,
                HttpHandler,
                AuthService,
                FormBuilder,
                NgModel,
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
