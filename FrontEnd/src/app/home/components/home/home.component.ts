/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

// Import services
import { NavigationService } from 'app/shared/services/navigation.service';
import { MapService, MapState } from 'app/shared/services/map.service';
import { AuthService, Role } from 'app/shared/services/auth.service';
import { UserService } from 'app/shared/services/user.service';
import { Config } from 'app/common/utils';
import { environment } from 'environments/environment';
import { LocationService } from '../../../shared/services/location.service';

import { GeoLocation } from '../../../shared/models/geolocation.model';
import * as L from 'leaflet';

@Component({
    moduleId: module.id,
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: [
        './home.component.scss'
    ],
    providers: []
})
export class HomeComponent implements OnInit, OnDestroy {

    public navItems: any[] = [];
    public showSearchBar: boolean;
    public authenticationState: Role;
    public mobile: boolean;
    public appLink: String;
    private searchLocation: string;
    private subscriptions: Subscription[];


    @ViewChild(MatSidenav)
    public nav: MatSidenav;

    constructor(private authService: AuthService,
        public router: Router,
        public userService: UserService,
        private navigationService: NavigationService,
        private mapService: MapService,
        private locationService: LocationService,
        private changeDetectorRef: ChangeDetectorRef) { }

    ngOnInit() {
        this.navigationService.setSideNav(this.nav);
        this.userService.getUserResearch();
        this.subscriptions = [];
        this.subscriptions.push(this.mapService.mapState.subscribe(state => {
                if (state === MapState.Default) {
                    this.showSearchBar = true;
                    this.changeDetectorRef.detectChanges();
                } else {
                    this.showSearchBar = false;
                    this.changeDetectorRef.detectChanges();
                }
        }, error => {
            console.error(error);
        }));
        this.subscriptions.push(this.authService.authenticationStream.subscribe(x => {
            this.handleAuthenticationChange(x);
            this.changeDetectorRef.detectChanges();
        }, error => {
            console.error(error);
        }));
        this.mobile = Config.IS_MOBILE;
        this.appLink = environment.applicationLink;
    }

    ngOnDestroy() {
        this.changeDetectorRef.detach();
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /** Setup internal data to the correct authentication state format. */
    private handleAuthenticationChange(state: Role) {
        this.authenticationState = state;
        if (state !== Role.Unauthenticated) {
            this.setupLoggedIn();
        }
    }

    /** Setup menu and settings for a logged in user. */
    private setupLoggedIn(): void {
        const settingItems = [];
        settingItems.push({ routerLink: ['/settings'], text: 'Settings', icon: 'settings' });
        settingItems.push({ routerLink: ['/logout'], text: 'Sign out', icon: 'power_settings_new' });


        this.navItems = [[
            { routerLink: ['/exposure'], text: 'Exposure', icon: 'date_range'},
            { routerLink: ['/questionnaire'], text: 'Questionnaire', icon: 'description' },
            { routerLink: ['/minigames'], text: 'Minigames', icon: 'gamepad'}
            ], settingItems];

        this.userService.getUserProfile();
    }

    /** Navigate to the map. */
    public navigateMap(): void {
        this.mapService.mapState.next(MapState.Default);
        this.router.navigate(['']);
    }

    /** Navigate to the requested location on the map. */
    private navigateLocation(): void {
        this.locationService.getGeoLocationAddress(this.searchLocation).subscribe( result => {
            this.mapService.setView(new L.LatLng(result.results[0].latitude, result.results[0].longitude));
            this.mapService.setLongPressLocation(new GeoLocation(result.results[0]), new L.LatLng(result.results[0].latitude, result.results[0].longitude));
            }, error => {
            console.error(error);
        });
    }

    /** Display or hide the route planner depending on the research group the user is in. */
    private toggleRoutePlannerOn(): boolean {
        return this.mapService.checkRoutePlannerVisible();
    }

    /** Display or hide the personal exposure calender depending on the research group the user is in. */
    private toggleExposureOn(): boolean {
        return this.mapService.checkExposureVisible();
    }

    /** Display or hide the search bar */
    public toggleSearchBar(): boolean {
        return !this.router.url.includes('/route-planner');
    }

    public toggleHamburger(): boolean {
        return this.mapService.mapState.value != MapState.Presentation;
    }

    /** Check to whether or not to display the exposure side menu button */
    private check(nav): boolean {
        if (nav !== this.navItems[0][0]) {
            return true;
        } else {
            return this.toggleExposureOn();
        }
    }
}
