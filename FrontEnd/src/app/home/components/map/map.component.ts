/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, HostListener, ViewChild, OnDestroy } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

// Import services
import { RasterService } from 'app/shared/services/raster.service';
import { SensorService } from 'app/shared/services/sensor.service';
import { RouteService } from 'app/shared/services/route.service';
import { MapService, MapState } from 'app/shared/services/map.service';
import { LocationService } from 'app/shared/services/location.service';
import { RatingService } from 'app/shared/services/rating.service';
import { UserService } from 'app/shared/services/user.service';

// Import layers
import { Layer } from './layers/layer';
import { RasterLayer } from './layers/raster.layer';
import { RatingLayer } from './layers/rating.layer';
import { RouteLayer } from './layers/route.layer';
import { SensorLayer } from './layers/sensor.layer';
import { ExposureLayer } from './layers/exposure.layer';

import { RouteComponent } from '../route/route.component';

import { MatSnackBar } from '@angular/material/snack-bar';

import { Config } from 'app/common';
import * as L from 'leaflet';
import 'hammerjs';
import { headersToString } from 'selenium-webdriver/http';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    animations: [
        trigger('fallDownAnimation', [
            state('true', style({
                height: '33px',
                opacity: '1',
                display: 'show',
            })),
            state('false', style({
                height: '23px',
                opacity: '0',
                display: 'none',
            })),
            transition('true <=> false', animate('200ms ease-out')),
        ]),
    ]
})
export class MapComponent implements OnInit, OnDestroy {

    // Layer list
    private layers: { [id: string]: Layer };
    public rasterRetrieved: boolean;


    public showControls: boolean;
    public presentation: boolean;
    private subscriptions: Subscription[] = [];

    @ViewChild('routenav')
    public routenav: RouteComponent;

    public dropDownButtons = false;
    public showContextMenu = false;
    public contextMenuLocation = { x: 0, y: 0 };
    public params: any;
    public refreshmap: any;
    public focus: any;

    /** Listen to document click event */
    @HostListener('document:click')
    clickedOutside() {
        this.toggleContextMenu(false);
    }

    constructor(public rasterService: RasterService,
                public locationService: LocationService,
                public mapService: MapService,
                public ratingService: RatingService,
                public routeService: RouteService,
                public sensorService: SensorService,
                public userService: UserService,
                public router: Router,
                public route: ActivatedRoute,
                public snackbar: MatSnackBar) { }

    ngOnInit() {
        // Init the map in the mapservice
        var sub = this.route.params.subscribe(params => {
            this.params = params['param'];
         });
        if(this.params == 'presentation')
            this.mapService.mapState.next(MapState.Presentation);

        this.mapService.createMap();
        // Create the layers
        this.layers = {
            sensor: new SensorLayer(this.mapService, this.sensorService, this.router),
            raster: new RasterLayer(this.mapService, this.rasterService, this.snackbar),
            route: new RouteLayer(this.mapService, this.routeService),
            rating: new RatingLayer(this.mapService, this.ratingService),
            exposure: new ExposureLayer(this.mapService, this.userService)
        };

        this.rasterRetrieved = false;
        (<RasterLayer>this.layers.raster).rasterRetrieved.subscribe(x => this.rasterRetrieved = x);

        // Init each layer
        Object.keys(this.layers).map(key => { return this.layers[key]; }).forEach(element => {
            element.Init();
        });

        // Hide and show layers according the mapstate
        this.mapService.mapState.subscribe(mapState => {
            switch (mapState) {
                case MapState.Default: {
                    this.showControls = true;
                    this.presentation = false;
                    this.layers.route.toggleOff();
                    this.layers.exposure.toggleOff();
                } break;
                case MapState.RoutePlanner: {
                    this.showControls = true;
                    this.presentation = false;
                    this.layers.route.toggleOn();
                    this.layers.exposure.toggleOff();
                } break;
                case MapState.ExposureOverview: {
                    this.presentation = false;
                    this.showControls = false;
                    this.layers.sensor.toggleOff();
                    this.layers.raster.toggleOff();
                    this.layers.rating.toggleOff();
                    this.layers.route.toggleOff();
                    this.layers.exposure.toggleOn();
                 } break;
                 case MapState.Presentation: {
                    this.presentation = true;
                    this.showControls = false;
                    this.layers.sensor.toggleOn();
                    this.layers.raster.toggleOn();
                    this.layers.rating.toggleOff();
                    this.layers.route.toggleOff();
                    this.layers.exposure.toggleOff();
                 } break;
            }
        }, () => {
            console.error('Map state error.');
        });
        let focusedLocation = false;
        this.mapService.currentLocation.subscribe(result => {
            if (result != null && !focusedLocation) {
                this.mapService.setView(result);
                focusedLocation = true;
            }
        }, error => {
            console.error(error);
        });
        if (this.mapService.mapState.value === MapState.Presentation) {
            this.focus = setTimeout(() => {
                this.mapService.setView(this.mapService.USP);
            }, 10000);
            this.refreshmap = setInterval(() => {
                this.mapService.setView(this.mapService.USP);
                this.layers.raster.toggle();
                this.layers.raster.toggle();
            }, 1000000);
        }
    }

    /** Select a clicked position. */
    private setClickPosition(routeOption: number): void {

        // Get coordinates in tile space
        const x = this.contextMenuLocation.x;
        const y = this.contextMenuLocation.y;

        // Calculate point in xy space
        const pointXY = L.point(x, y);

        // Get latlng on map
        const pointlatlng = this.mapService.map.containerPointToLatLng(pointXY);

        // Set location in the mapService
        let queryParams;

        // if routeOption is 0 (from) set from queryparam
        if (routeOption === 0) {
            queryParams = { from: 's:' + pointlatlng.lat + ',' + pointlatlng.lng };
        // else if routeOption is 2 (to) set to queryparam
        } else if (routeOption === 1) {
            queryParams = { via: 's:' + pointlatlng.lat + ',' + pointlatlng.lng };
        // else if routeOption is 2 (to) set to queryparam
        } else if (routeOption === 2) {
            queryParams = { to: 's:' + pointlatlng.lat + ',' + pointlatlng.lng };
        }

        // Navigate to the route-planner with the created queryparams
        this.router.navigate(['route-planner'], { queryParams: queryParams, queryParamsHandling: 'merge' });
    }

    /** Returns the tooltip string, null if on mobile to disable tooltips. */
    public getTooltip(tip: string): string {
        return Config.IS_MOBILE ? '' : tip;
    }

    /** On contextmenu event. */
    public onContextMenu(e): void {
        if (e.button === 2) {
            this.setContextMenuLocation(e);
            this.toggleContextMenu(true);
        } else {
            this.toggleContextMenu(false);
            this.setContextMenuLocation(e);
        }
    }

    /** Leave rating on clicked position. */
    public leaveRating() {
        // Get coordinates in tile space
        const x = this.contextMenuLocation.x;
        const y = this.contextMenuLocation.y;

        // Calculate point in xy space
        const pointXY = L.point(x, y);

        // Get latlng on map
        const pointlatlng = this.mapService.map.containerPointToLatLng(pointXY);
        this.router.navigate(['/rating'], { queryParams: { lat: pointlatlng.lat, lng: pointlatlng.lng } });
    }

    /** On longpress map. */
    public onLongPress(e): void {
        if (this.routeService.noRoute()) {
            // Calculate point in xy space
            const pointXY = L.point(e.srcEvent.pageX, e.srcEvent.pageY);

            // Get latlng on map
            const pointlatlng = this.mapService.map.containerPointToLatLng(pointXY);

            // Set location in the mapService
            this.locationService.getGeoLocationLatLng(pointlatlng.lat, pointlatlng.lng).toPromise().then(
                result => {
                    const location = this.mapService.createLocation(pointlatlng, 2, result);
                    this.mapService.setLongPressLocation(location, pointlatlng);
                }).catch( error => {
                    console.error(error);
                }
            );
        }
    }
    /** Set XY position of the contextmenu. */
    private setContextMenuLocation(e): void {
        this.contextMenuLocation = { x: e.pageX, y: e.pageY };
    }

    /** Display or hide contextmenu. */
    public toggleContextMenu(toggle?: boolean): void {
        if (toggle === true || toggle === false) {
            this.showContextMenu = toggle;
        } else {
            this.showContextMenu = !this.showContextMenu;
        }
    }

    /** Display or hide option to give rating depending on user logged in or not */
    private toggleGiveRatingOn(): boolean {
        return this.mapService.checkLoggedIn();
    }

    /** Display or hide ratings if the user is in a research where you might not be able to see ratings. */
    public toggleRatingOn(): boolean {
        return this.mapService.checkRatingsVisible();
    }

    /** Display or hide the route planner depending on the research group the user is in. */
    public toggleRoutePlannerOn(): boolean {
        return this.mapService.checkRoutePlannerVisible();
    }

    /** Display or hide the sensors depending on the research group the user is in. */
    public toggleSensorsOn(): boolean {
        return this.mapService.checkSensorsVisible();
    }
    /** Display or hide the raster depending on the research group the user is in. */
    public toggleRasterOn(): boolean {
        return this.mapService.checkRasterVisible();
    }

    /** Display or hide the personal exposure calender depending on the research group the user is in. */
    public toggleExposureOn(): boolean {
        return this.mapService.checkExposureVisible();
    }

    /** Focus map to center around users location. */
    public focusLocation(setView: boolean = true): void {
        this.mapService.getCurrentLocation(setView);
    }

    /** Toggle side buttons. */
    public toggleButtons(toggle?: boolean) {
        if (toggle === true || toggle === false) {
            this.dropDownButtons = toggle;
        } else {
            this.dropDownButtons = !this.dropDownButtons;
        }
    }

    /** Toggle specific layer. */
    public toggleLayer(layerName: string): void {
        this.layers[layerName].toggle();
    }

    /** Toggle the sensor layer and make sensor graph invisible if necessary. */
    public toggleSensorLayer(): void {
        this.layers['sensor'].toggle();
        this.router.navigate(['']);
    }

    /** Check if layer is active */
    public isLayerActive(layerName: string): boolean {
        if (this.layers != null && this.layers[layerName]) {
            return this.layers[layerName].isActive;
        }

        return false;
    }

    /** Get the routelayer. */
    get routeLayer(): RouteLayer {
        return <RouteLayer>this.layers.route;
    }

    /** Get the exposurelayer. */
    get exposureLayer(): ExposureLayer {
        return <ExposureLayer>this.layers.exposure;
    }

    /** Get the ratinglayer. */
    get ratingLayer(): RatingLayer {
        return <RatingLayer>this.layers.rating;
    }

    /** Get all layers. */
    get allLayers(): { [id: string]: Layer } {
        return this.layers;
    }

    /** Cleanup subscriptions on component destruction. */
    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
