/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Router } from '@angular/router';
import { SensorService } from 'app/shared/services/sensor.service';
import { MapService } from 'app/shared/services/map.service';

import { Layer } from './layer';

import * as L from 'leaflet';
import * as moment from 'moment';

export class SensorLayer extends Layer {

    // Sensor variables
    private sensorGroup: L.FeatureGroup;
    private sensorList: Array<string> = [];

    // Default icon
    private basicIcon;

    constructor(private mapService: MapService,
                private sensorService: SensorService,
                private router: Router) {

        super();

        // Create a basic icon
        this.basicIcon = L.Icon.extend({
            options: {
                customId: '',
                iconSize: [44, 65],
                iconAnchor: [22, 65],
                popupAnchor: [0, -46]
            }
        });

        // Create layer for markers
        this.sensorGroup = new L.FeatureGroup();

        // Open sensors when graph is open
        if (this.router.url.includes('/graph')) {
            this.toggleOn();
        }


        // Add eventlistener for zooming and moving the map
        this.mapService.map.on('zoomend moveend', () => this.isActive ? this.retrieveLatestSensorBatches() : null);
    }

    /** Hides sensors when switched to inactive, adds sensors when switched to active. */
    protected handleStatusChange(active: boolean): void {
        if (active) {
            this.retrieveLatestSensorBatches();
            this.sensorGroup.addTo(this.mapService.map);
        } else {
            this.mapService.map.removeLayer(this.sensorGroup);
        }
    }

    /** Retrieve latest batches from the sensors. */
    public retrieveLatestSensorBatches(): void {
        this.sensorService.getLatestSensorBatches(this.mapService.map.getBounds())
            .then(
                sensorBatches => this.handleLatestSensorBatches(sensorBatches)
            ).catch((error) => {
                console.error(error);
            });
    }

    /** Handle all the individual sensorbatches. */
    private handleLatestSensorBatches(sensorBatches): void {
        sensorBatches.forEach(sensorBatch => {
            if (!this.sensorList.includes(sensorBatch.id)) {
                this.createMarker(sensorBatch.latitude, sensorBatch.longitude, sensorBatch.id, sensorBatch.batch.measurements[0].value);
                this.sensorList.push(sensorBatch.id);
            }
        });

        // Add all the sensors to the map
        this.sensorGroup.addTo(this.mapService.map);
    }

    /** Create and add a marker to the map. */
    private createMarker(latitude: number, longitude: number, id: string, measurement: number): L.Marker {
        const marker = L.marker([latitude, longitude], { customId: id }).on('click', (e) => this.markerOnClick(e, this));

        // Set icons
        const wrongIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_6.png' }),
            redIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_5.png' }),
            orangeIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_4.png' }),
            yellowIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_3.png' }),
            greenIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_2.png' }),
            blueIcon = new this.basicIcon({ iconUrl: '../assets/images/marker_1.png' });

        // Add the right marker colour
        if (measurement === -999) {
            marker.setIcon(wrongIcon);
        } else if (measurement > 125) {
            marker.setIcon(redIcon);
        } else if (measurement > 70) {
            marker.setIcon(orangeIcon);
        } else if (measurement > 40) {
            marker.setIcon(yellowIcon);
        } else if (measurement > 20) {
            marker.setIcon(greenIcon);
        } else if (measurement >= 0) {
            marker.setIcon(blueIcon);
        }

        // Add value to marker
        if (measurement !== -999) {
            marker.bindTooltip(
                Math.trunc(measurement).toString(), {
                    permanent: true,
                    className: 'sensorValue',
                    direction: 'top',
                    offset: [0, -16]
                });
        }

        // Add marker to the sensorGroup (Leaflet FeatureGroup)
        marker.addTo(this.sensorGroup);

        // Add sensor ID to the sensorList
        this.sensorList.push(id);

        return marker;
    }

    // TODO: Update function to modular time options
    /** Marker click event. */
    markerOnClick(e, self): void {
        const sensorid = e.target.options.customId;
        const to = moment().add(1, 'days').format('YYYY-MM-DD');
        const from = moment().format('YYYY-MM-DD');

        // Navigate to the graph page
        const queryParams = {
            id: sensorid,
            from: from,
            to: to
        };
        self.router.navigate(['graph'], { queryParams: queryParams });
    }

    /** Get sensorlist. */
    get sensorsList(): Array<string> {
        return this.sensorList;
    }

    /** Get mapservice */
    get mapHandler(): MapService {
        return this.mapService;
    }
}
