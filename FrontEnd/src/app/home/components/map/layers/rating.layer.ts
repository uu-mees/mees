/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

// Import services
import { RatingService } from 'app/shared/services/rating.service';
import { MapService } from 'app/shared/services/map.service';

import { Color } from 'app/common/utils/index';
import { Layer } from './layer';

import * as L from 'leaflet';
import * as M from 'leaflet.markercluster';

export class RatingLayer extends Layer {

    private ratingMarkerGroup: M.MarkerClusterGroup;
    private imageUrl = '../../../../assets/icons/rating.png';
    private imageHTML = `<img class=\'button-icon\' src=\'${this.imageUrl}\' style=\'width:10px;height:10px\'>`;
    private ratingData;

    constructor(private mapService: MapService,
                private ratingService: RatingService) {
            super();
    }

    /** Spawns ratings when switched to active, removes latings otherwise. */
    protected handleStatusChange(active: boolean) {
        if (active) {
            this.spawnRatings();

        } else {
            if (this.ratingMarkerGroup != null) {
                this.mapService.map.removeLayer(this.ratingMarkerGroup);
                this.mapService.map.closePopup();
            }
        }
    }

    /** Setup the marker clusters. */
    public setupMarkerClusterGroup() {
        // If there is a ratingMarkerGroup that is currently active we need to remove it first.
        if (!!this.ratingMarkerGroup) {
            this.mapService.map.removeLayer(this.ratingMarkerGroup);
        }

        // Make a new one with a custom iconCreateFunction
        this.ratingMarkerGroup = new M.MarkerClusterGroup({
            iconCreateFunction: (cluster) => {
                const childCount = cluster.getChildCount();

                let c = 'marker-cluster-custom-icon';
                if (childCount > 1) {
                    c += ' marker-cluster-parent';
                }

                const children = cluster.getAllChildMarkers();

                // Calculate average ranking
                let total = 0;
                for (let i = 0; i < children.length; i++) {
                    total += children[i].options.title;
                }

                // Round to one decimal precision
                const average = Math.round((total / children.length) * 10) / 10;

                // Get the color based on the quality of the rating
                const color = Color.getRatingColor(average);

                return new L.DivIcon({ html: '<div class=\'' + c + '\' style=\'background-color:' + color + '99\'><div style=\'background-color:' + color + '\'><span class="invert-text-color">' + average + '</span></div></div>',
                                       iconSize: new L.Point(40, 40)
                                    });
            }
        });

        // Set options
        this.ratingMarkerGroup.options.spiderfyOnMaxZoom = false;
        this.ratingMarkerGroup.options.singleMarkerMode = true;
        this.ratingMarkerGroup.options.chunkedLoading = true;
        this.ratingMarkerGroup.options.zoomToBoundsOnClick = false;
    }

    /** Show the ratings with clustering. */
    public async spawnRatings() {
        // Initialize the ratingMarkerGroup.
        this.setupMarkerClusterGroup();

        // Update and get the ratings.
        this.ratingData = await this.ratingService.getRatings();

        // If there is no ratingData there is nothing to display so we return.
        if (!this.ratingData) {
            return;
        }

        // For all the ratings we make a marker and add it to the clustergroup.
        for (let i = 0; i < this.ratingData.length; i++) {
            const a = this.ratingData[i];
            // Null or undefined checks
            if (!a.rating || !a.position || !a.position.latitude || !a.position.longitude) {
                continue;
            }
            const title = a.rating;
            const marker = L.marker(new L.LatLng(a.position.latitude, a.position.longitude), {
                                    radius: 5,
                                    color: '#000000',
                                    fill: true,
                                    fillColor: '#FFFFFF',
                                    fillOpacity: 1,
                                    weight: 2,
                                    title: title
                                });
            marker.bindPopup(this.markerContent(title));
            this.ratingMarkerGroup.addLayer(marker);
        }

        // Close popups on zoom
        this.mapService.map.on('zoom', (a) => {
            this.mapService.map.closePopup();
        });

        // When the user click on a cluster a popup is created and displayed.
        this.ratingMarkerGroup.on('clusterclick', (a) => {
            const children = a.layer.getAllChildMarkers();
            const content = this.clusterContent(children);
            L.popup().setLatLng(a.latlng).setContent(`${content}`).openOn(a.layer._map);
        });

        this.mapService.map.addLayer(this.ratingMarkerGroup);
    }

    /** Get the html content for a marker popup. */
    public markerContent(rating) {
        return `Rating: ${this.getStars(rating)}`;
    }

    // Get the html for the star image multiple times (as many as the specified rating).
    public getStars(rating) {
        return this.imageHTML.repeat(rating);
    }

    // Get the html for the cluster popup
    public clusterContent(children) {
        const buckets = [0, 0, 0, 0, 0];
        for (let i = 0; i < children.length; i++) {
            const value = children[i].options.title;
            buckets[Math.round(value) - 1]++;
        }

        const content = [];
        for (let i = 0; i < buckets.length; i++) {
            if (buckets[i] !== 0) {
                content[i] = `<tr>
                                <td>
                                    ${this.getStars(i + 1)}
                                </td>
                                <td>
                                    #${buckets[i]}
                                </td>
                            </tr>`;
            }
        }
        return `<table>
                    <tr>
                        <td>
                            Total
                        </td>
                        <td>
                            #${buckets.reduce((acc, val) => { return acc + val; })}
                        </td>
                    </tr>
                    ${content.join('')}
                </table>`;
    }
}
