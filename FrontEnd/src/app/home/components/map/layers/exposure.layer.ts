/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { OnDestroy } from '@angular/core';

// Import model
import { RouteLine } from 'app/shared/models/routeline.model';

// Import services
import { MapService } from 'app/shared/services/map.service';
import { UserService } from 'app/shared/services/user.service';

import { RouteLayer } from 'app/home/components/map/layers/route.layer';
import { TimeConverter } from 'app/shared/converters/time.converter';
import * as L from 'leaflet';
import { Config } from 'app/common';

export class ExposureLayer extends RouteLayer implements OnDestroy {

    constructor(mapService: MapService,
        private userService: UserService) {
        super(mapService, null);

        // this.routeGroup.addTo(this.mapService.map);
    }

    /** Clears the routes if layer switches to inactive, adds routes when active. */
    public handleStatusChange(active: boolean) {
        if (!active) {
            this.clearRoutes();
            this.mapService.clearMarkers();
        } else {
            this.routeGroup.addTo(this.mapService.map);
        }
    }

    /** Clear routes by clearing the routes and lines. */
    public clearRoutes() {
        this.routeGroup.clearLayers();
        this.mapService.clearRouteLines();
    }

    protected getLatestRoute() {
        return;
    }

    /** Create tooltip based on exposure and duration. */
    public createTooltip(exposure: number, duration: number, icon: string = null) {
        let tooltipExposure;
        const warningIcon = '<div style=\'width:100%; pointer-events: all; display:flex; justify-content:center;\'>' +
        '<a href=\'http://www.who.int/news-room/fact-sheets/detail/ambient-(outdoor)-air-quality-and-health\'>' +
        '<img src=\'' + this.warningIconUrl + '\'></a></div>';
        if (!!exposure) {
            tooltipExposure = `<span style="font-size: 1.2em; font-weight: bold; display: block; text-align: center;">NO2: ${ Math.round(exposure * 10) / 10 } </span>`;
            if (Math.round(exposure * 10) / 10 > 60) {
                tooltipExposure = warningIcon + tooltipExposure;
            }

        } else {
            tooltipExposure = `<span style="font-size: 1.2em; font-weight: bold; display: block; text-align: center;">NO2: <span style="color: #BBBBBB">?</span></span>`;
        }
        const tooltipDuration = `<span style="font-size: 1.1em; font-weight: normal; display: block; text-align: center;"> ${ TimeConverter.getTime(duration) } </span>`;

        return tooltipExposure + tooltipDuration;
    }

    /** Cleanup subscriptions on component destruction. */
    ngOnDestroy(): void {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
