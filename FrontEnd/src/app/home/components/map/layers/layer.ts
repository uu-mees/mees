/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export abstract class Layer {
    public isActive = false;
    public activityStatus: BehaviorSubject<boolean> = new BehaviorSubject(false);

    public Init() {
        this.activityStatus.subscribe(x => this.handleStatusChange(x));
    }

    protected abstract handleStatusChange(active: boolean): void;

    /** Switch layer to 'off'. */
    public toggleOff(): void {
        if (this.isActive) {
            this.toggle();
        }
    }

    /** Switch layer to 'on'. */
    public toggleOn(): void {
        if (!this.isActive) {
            this.toggle();
        }
    }

    /** Toggle layer. */
    public toggle(): void {
        this.isActive = !this.isActive;
        this.activityStatus.next(this.isActive);
    }
}
