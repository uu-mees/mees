/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { MatSnackBar } from '@angular/material/snack-bar';
import { EventEmitter } from '@angular/core';

// Import models
import { Raster } from 'app/shared/models/raster.model';

// Import services
import { MapService } from 'app/shared/services/map.service';
import { RasterService } from 'app/shared/services/raster.service';

import { Layer } from './layer';
import * as L from 'leaflet';

export class RasterLayer extends Layer {

    // Raster variables
    private rasterGroup: L.FeatureGroup;

    // Most recent timestamp
    public timestamp: Date;

    // Keep track of map activity at raster toggle off
    private isChanged: boolean;
    public rasterRetrieved: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private mapService: MapService,
                private rasterService: RasterService,
                private snackBar: MatSnackBar) {
        super();

        // Create layer for raster cells
        this.rasterGroup = new L.FeatureGroup();
    }

    /** Hides raster when inactive, retrieves raster when switched to active */
    protected handleStatusChange(active: boolean) {
        if (active) {
            this.retrieveRaster();
        } else {
            this.hideRaster();
        }
    }

    /** Retrieve raster from the back-end server */
    public retrieveRaster(): Promise<void> {
        // Always get the latest raster from back-end server
        return this.rasterService.getRaster(this.timestamp)
            .then((data) => {
                this.overlayRasterImage(data);
                this.rasterRetrieved.emit(true);
            })
            .catch((error) => {
                this.toggleOff();

                if (error.status === 304) {
                    return;
                }
                if (error.status === 204) {
                    const snackBarRef = this.snackBar.open('Raster was unavailable.');
                } else {
                    console.error(error);
                }
            });
    }

    /** Given an array of cell data, visualize them on the map */
    private overlayRasterImage(data: Raster) {
        if (!this.isActive) {
            return;
        }
        // We only do something if we actually got a new raster.
        // In that case we also have to destroy the old one so we don't draw over it.
        if (this.timestamp == null || data.timeStampPolled > this.timestamp) {
            this.destroyRaster();
            this.timestamp = data.timeStampPolled;
            const southWestCorner = L.latLng(data.southWestLatitude, data.southWestLongitude);
            const northEastCorner = L.latLng(data.northEastLatitude, data.northEastLongitude);
            const rasterBounds = L.latLngBounds(southWestCorner, northEastCorner);

            // Overlay the rasterimage
            const rasterImage = L.imageOverlay('data:image/png;base64,' + data.image,
                rasterBounds, { opacity: 0.45 }
            );

            rasterImage.addTo(this.rasterGroup);

            // Add the click event listener to show a popup with the amount of NO2. We only display the
            // popup when we click inside the raster, the raster is active and we did not click on a path.
            // We do not need to check ratings/markers because they are already handled correctly.
            this.mapService.map.on('click', (e) => {
                if (this.isActive && rasterBounds.contains(e.latlng) && e.originalEvent.target.nodeName !== 'path') {
                    this.rasterService.getExposure([e.latlng.lat, e.latlng.lng]).then((exposure) => {
                        this.rasterGroup.bindPopup('NO<sub>2</sub>: ' + Math.round(exposure) + ' ug/m<sup>3</sup>');
                        this.rasterGroup.openPopup(e.latlng);
                    });
                }
            });

            // Add the rasterGroup to the map
            this.rasterGroup.addTo(this.mapService.map);
        } else {
            this.showRaster();
        }
    }

    /** Removes the raster from the map, and destroys the raster objects */
    private destroyRaster() {
        // Clear rectangles from map
        this.rasterGroup.clearLayers();
    }

    /** Removes the raster from the map, without destroying any of the raster objects */
    private hideRaster() {
        this.mapService.map.removeLayer(this.rasterGroup);
    }

    /** Shows the raster */
    private showRaster() {
        this.rasterGroup.addTo(this.mapService.map);
    }
}
