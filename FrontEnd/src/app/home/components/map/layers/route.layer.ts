/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { OnDestroy } from '@angular/core';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

// Import models
import { Route } from 'app/shared/models/route.model';
import { GeoLocation } from 'app/shared/models/geolocation.model';
import { RouteLine } from 'app/shared/models/routeline.model';

// Import services
import { RouteService } from 'app/shared/services/route.service';
import { MapService } from 'app/shared/services/map.service';

import { TimeConverter } from 'app/shared/converters/time.converter';
import { Color, Config } from 'app/common';
import { Layer } from './layer';

import * as L from 'leaflet';

export class RouteLayer extends Layer implements OnDestroy {

    public loading = true;

    // List of routes
    protected routeGroup: L.FeatureGroup = new L.FeatureGroup();

    // Active route ID
    private routeSelect = new BehaviorSubject<number>(0);
    activeRoute = this.routeSelect.asObservable();

    // Default icon
    protected warningIconUrl = '../../../../assets/icons/warning-icon.png';

    private routeMarkers = new L.FeatureGroup<L.CircleMarker>();
    protected subscriptions: Subscription[] = [];

    constructor(protected mapService: MapService,
                private routeService: RouteService) {

        super();

        // Start subscriptions
        this.getLatestRoute();
    }

    /** Removes routes when switched to inactive, adds routes otherwise. */
    protected handleStatusChange(active: boolean) {
        if (active) {
            this.routeGroup.addTo(this.mapService.map);
        } else {
            this.clearRoutes();
            this.mapService.map.removeLayer(this.routeGroup);
        }
    }

    /** Subscribes on the latest route locations, then gets the route. */
    protected getLatestRoute() {
        this.subscriptions.push(this.routeService.latestRoute
        .subscribe(route => {
            this.retrieveRoute(route[0], route[2], route[1]);
        }, error => {
            console.error(error);
        }));
    }

    /** Retrieve a route from the server based on a starting and ending point. */
    public retrieveRoute(from: GeoLocation, dest: GeoLocation, via?: GeoLocation) {

        this.clearLayer();

        // Check if from and dest exist and handle their routes
        if (!!from && !!dest) {

            // Get LatLng of From and Destination location
            const fromLatLng = new L.LatLng(from.lat, from.lng);
            const destLatLng = new L.LatLng(dest.lat, dest.lng);

            // If route has a via
            if (!!via) {

                // Get LatLng of Via location
                const viaLatLng = new L.LatLng(via.lat, via.lng);

                // Get route From to Via
                const fromVia = this.routeService.getRoute(fromLatLng, viaLatLng)
                    .toPromise().then(
                        (routings) => { return routings; }
                    ).catch((error) => {
                        console.error(error);
                    });

                // Get route Via to Destination
                const viaDest = this.routeService.getRoute(viaLatLng, destLatLng)
                    .toPromise().then(
                        (routings) => { return routings; }
                    ).catch((error) => {
                        console.error(error);
                    });

                // Wait for all promises to resolve, combine them and set them as route
                Promise.all([fromVia, viaDest]).then(
                    (routings) => {
                        const fromViaDest = routings[0].routes.concat(routings[1].routes);
                        this.handleRoutings(fromViaDest);
                    }).catch((error) => {
                        this.routeService.setSearchingRoute(false);
                        console.error(error);
                    });
            } else {
                // Get and set route From to Destination
                const fromDest = this.routeService.getRoute(fromLatLng, destLatLng)
                    .toPromise().then(
                        (routings) => {
                            this.handleRoutings(routings.routes);
                        }).catch((error) => {
                            this.routeService.setSearchingRoute(false);
                            console.error(error);
                        });
            }
        }
    }

    /** Create the route for each of the routes recieved and fit the map to the routes. */
    private handleRoutings(routes) {

        // Create a route (line) foreach route
        routes.forEach(route => {
            if (!!route) {
                this.createRoute(route);

                // Create markers for start and end location
                this.createMarker(route.path[0].reverse()).addTo(this.routeMarkers);
                this.createMarker(route.path[route.path.length - 1].reverse()).addTo(this.routeMarkers);
            }
        });

        // Add route markers
        this.routeMarkers.addTo(this.routeGroup);

        // Add the routings to the map
        this.routeGroup.addTo(this.mapService.map);

        // Fit the routes into the maps bouding box
        this.fitRoutes();

        // Set searching value to false
        this.routeService.setSearchingRoute(false);
    }

    /** Create a polyline based on a route. */
    public createRoute(route: Route): RouteLine {
        // FeatureGroup for all the lines (active, inactive and the baseline)
        const routeLine = new L.FeatureGroup();

        // Tooltip for route information
        const tooltipContent = this.createTooltip(route.exposure, route.duration);

        // Create baseline (thicker, gray)
        const baseLine = this.createPolyLine(route.path, '#333333', 10).addTo(routeLine);

        // Create inactive line (smaller, lightgray)
        const inactiveLine = this.createPolyLine(route.path, '#C3C3C3', 6).addTo(routeLine);
        inactiveLine.bindTooltip(tooltipContent, { permanent: true });

        let activeLine;

        // Create active line (colored) if exposure is not null
        activeLine = this.createColoredPolyLine(route.path, 6, route.edgeExposures).addTo(routeLine);

        // Subscribe to the activeroute (and change accordingly)
        this.subscriptions.push(this.activeRoute.subscribe(
            active => {
                if (active === this.routeGroup.getLayerId(activeLine)) {
                    activeLine.setStyle({opacity: 1});
                    routeLine.bringToFront();
                } else {
                    activeLine.setStyle({opacity: 0});
                }
            }, error => {
                console.error(error);
            }
        ));

        // Change activeRoute on click event
        activeLine.on('click', (e) => this.setActiveRoute(e.target));

        // Add the complete route to the routeGroup
        routeLine.addTo(this.routeGroup);

        return new RouteLine(routeLine, activeLine);
    }

    /** Set the activeRoute. */
    private setActiveRoute(activeLine) {
        this.routeSelect.next(this.routeGroup.getLayerId(activeLine));
        this.bringMarkersToFront();
    }

    private bringMarkersToFront() {
        this.routeMarkers.getLayers().forEach(marker => marker.bringToFront());
    }

    /** Create and return a polyline. */
    protected createPolyLine(path: Array<Array<number>>, color: string, weight: number, opacity: number = 1): L.GeoJson {

        // Create the LineString
        const line = [{
            'type': 'LineString',
            'coordinates': path,
        }];

        // Set styling for the line
        const lineStyle = {
            'color': color,
            'weight': weight,
            'opacity': opacity,
        };

        // Create the polyline in GeoJSON format
        const polyLine = new L.GeoJSON(line, {
            style: lineStyle
        });

        return polyLine;
    }

    /** Create and return a colored polyline. */
    protected createColoredPolyLine(path: Array<Array<number>>, weight: number, edgeExposures: Array<number>): L.FeatureGroup {

        const coloredLine = new L.FeatureGroup();

        // Create line piece between each point
        path.forEach((point, i) => {

            let line = null;

            // Create line object from point to point
            if (i < path.length - 1) {
                line = [point, path[i + 1]];
            }

            // Create and return line with colors if line exists
            if (line !== null) {
                const lineColor = Color.getExposureColor(edgeExposures[i]);
                this.createPolyLine(line, lineColor, weight, 0).addTo(coloredLine);
            }
        });

        return coloredLine;
    }

    /** Create tooltip based on exposure and duration. */
    protected createTooltip(exposure: number, duration: number) {
        let tooltipExposure;
        const warningIcon = '<div style=\'width:100%; pointer-events: all; display:flex; justify-content:center;\'>' +
        '<a href=\'http://www.who.int/news-room/fact-sheets/detail/ambient-(outdoor)-air-quality-and-health\'>' +
        '<img src=\'' + this.warningIconUrl + '\'></a></div>';

        if (!!exposure) {
            tooltipExposure = `<span style="font-size: 1.2em; font-weight: bold; display: block; text-align: center;">NO2: ${ Math.round(exposure * 10) / 10 } </span>`;
            if (Math.round(exposure * 10) / 10 > 60) {
                tooltipExposure = warningIcon + tooltipExposure;
            }
        } else {
            tooltipExposure = `<span style="font-size: 1.2em; font-weight: bold; display: block; text-align: center;">NO2: <span style="color: #BBBBBB">?</span></span>`;
        }

        const tooltipDuration = `<span style="font-size: 1.1em; font-weight: normal; display: block; text-align: center;"> ${ TimeConverter.getTime(duration) } </span>`;

        return tooltipExposure + tooltipDuration;
    }

    /** Create location marker for a route. */
    private createMarker(latLng: Array<number>): L.Marker {
        const marker = L.circleMarker(latLng, {radius: 5, color: '#000000', fill: true, fillColor: '#FFFFFF', fillOpacity: 1, weight: 2});
        return marker;
    }

    /** Zoom to fit map to the boudingbox of the routes. */
    protected fitRoutes() {
        if (this.routeGroup.getLayers().length > 0) {
            let settings = {};

            // If not on mobile, add padding to the fitBounds to exclude the sidenav and controls
            if (Config.IS_WEB) {
                settings = {
                    paddingTopLeft: [300, 20],      // Exclude sidenav
                    paddingBottomRight: [40, 20]    // Exclude controls
                };
            }

            this.mapService.map.fitBounds(this.routeGroup.getBounds(), settings);
        }
    }

    /** Removes the routes from the map */
    protected clearRoutes(): void {
        this.routeMarkers.clearLayers();
        this.routeGroup.clearLayers();
    }

    /** Clear all layers corresponding to the route layer. */
    public clearLayer() {
        this.clearRoutes();
        this.mapService.clearMarkers();
    }

    /** Cleanup subscriptions on component destruction. */
    ngOnDestroy(): void {
        this.clearLayer();
        this.subscriptions.forEach(x => x.unsubscribe());
    }
}
