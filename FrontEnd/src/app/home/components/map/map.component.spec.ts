/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, tick, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { MapComponent } from './map.component';
import { HttpClient, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { GraphComponent } from '../graph/graph.component';
import { LocationService } from '../../../shared/services/location.service';
import { MapService } from '../../../shared/services/map.service';
import { RasterService } from 'app/shared/services/raster.service';
import { RasterServiceMock } from 'app/shared/services/raster.service.mock';
import { RatingService } from '../../../shared/services/rating.service';
import { RouteService } from 'app/shared/services/route.service';
import { SensorService } from 'app/shared/services/sensor.service';
import { SensorServiceMock } from '../../../shared/services/sensor.service.mock';
import { UserService } from '../../../shared/services/user.service';

import { environment } from 'app/../environments/environment';
import { TestUtils } from 'app/common/utils/tests/TestUtils';

import { AppModule } from 'app/app.module';
import { Routings } from 'app/shared/models/route.model';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from 'app/app.component';
import { RatingFormComponent } from '../rating-form/rating-form.component';
import { HttpTestingController } from '@angular/common/http/testing';

import { RouteLayer } from './layers/route.layer'
import { RasterLayer } from './layers/raster.layer';
import { SensorLayer } from './layers/sensor.layer';
import { AsyncAction } from 'rxjs/scheduler/AsyncAction';

// Sometimes the map starts corrupted (Leaflet bug); if you notice a long wait time, please restart ng test
jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  let router: Router;
  let location: Location;

  // Spy on service methods {BELONG IN MAPSERVICE}
//   let setLocationSpy: jasmine.Spy;
//   let getCurrentLocationSpy: jasmine.Spy;

    let focusLocationSpy: jasmine.Spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports: [
            AppModule,
            RouterTestingModule.withRoutes([
            {path: '', component: MapComponent},
            {path: 'rating', component: RatingFormComponent}
            ])
        ],
        declarations: [
        ],
        providers: [
            RasterService,
            LocationService,
            MapService,
            RatingService,
            RouteService,
            SensorService,
            UserService,
            HttpClient,
            HttpHandler
        ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(MapComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();

            router = TestBed.get(Router);
            location = TestBed.get(Location);

            router.initialNavigation();
        });
    }));

    beforeEach(() => {
            fixture = TestBed.createComponent(MapComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();

            router = TestBed.get(Router);
            location = TestBed.get(Location);

            router.initialNavigation();
    });

    afterEach(async(() => {
        // Destroy fixture to reset it for each next test
        fixture.destroy();
    }));

    // First tests check if layer get initialized
    it('should create', () => {
        expect(component).toBeTruthy('MapComponent not defined.');
    });

    it('should return all layers', () => {
        expect(component.allLayers).toBeDefined('Layer array returns undefined.');
    });

    it('should create sensor layer', () => {
        expect(component.allLayers['sensor']).toBeDefined('Sensor layer not defined.');
    });

    it('should create raster layer', () => {
        expect(component.allLayers['raster']).toBeDefined('Raster layer not defined.');
    });

    it('should create route layer', () => {
        expect(component.allLayers['route']).toBeDefined('Route layer not defined.');
    });

    it('should create rating layer', () => {
        expect(component.allLayers['rating']).toBeDefined('Rating layer not defined.');
    });

    it('should be able to return route layer', async(() => {
        fixture.whenStable().then(() => {
            expect(Object.prototype.toString.call(component.routeLayer)).toBe('[object Object]', 'Object returned not of type <RouteLayer>');
        });
    }));

    it('should be able to return exposure layer', async(() => {
        fixture.whenStable().then(() => {
            expect(Object.prototype.toString.call(component.exposureLayer)).toBe('[object Object]', 'Object returned not of type <ExposureLayer>');
        });
    }));

    it('should be able to focus location', async(() => {
        // Spy on focus location method.
        focusLocationSpy = spyOn(component, 'focusLocation').and.callThrough();
        // Wait for program to stabilize after loading.
        fixture.whenStable().then(() => {
            // Detect changes to variables.
            fixture.detectChanges();
            // Call method.
            component.focusLocation();
            expect(focusLocationSpy).toHaveBeenCalled();
        });
    }));

    it('should be able act on mouse long press', async(() => {
        // Spies to check if method gets executed properly
        let noRouteSpy, containerPointToLatLngSpy, getGeoLocationLatLngSpy, createLocationSpy, setLongPressLocationSpy: jasmine.Spy;
        noRouteSpy = spyOn(component.routeService, 'noRoute').and.callThrough();
        containerPointToLatLngSpy = spyOn(component.mapService.map, 'containerPointToLatLng').and.callThrough();
        getGeoLocationLatLngSpy = spyOn(component.locationService, 'getGeoLocationLatLng').and.callThrough();
        createLocationSpy = spyOn(component.mapService, 'createLocation').and.callThrough();
        setLongPressLocationSpy = spyOn(component.mapService, 'setLongPressLocation').and.callThrough();
        // Mock object for the event
        const e = {
            srcEvent: {
                pageX: 50,
                pageY: 50
            }
        };

        // When loading is done, go through the method.
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(component.routeService.noRoute()).toBeTruthy('Route present, can not execute long press.');
            // Call onLongPress method
            component.onLongPress(e);

            // Check if all segments of the method have been called.
            fixture.detectChanges();
            expect(noRouteSpy).toHaveBeenCalled();
            expect(containerPointToLatLngSpy).toHaveBeenCalled();
            expect(getGeoLocationLatLngSpy).toHaveBeenCalled();
            // expect(createLocationSpy).toHaveBeenCalled();
            // expect(setLongPressLocationSpy).toHaveBeenCalled();
        });
    }));

    it('should be able to leave a rating', async(() => {
        // First set a context menu location
        // Set up a simulated mouse event to simulate a user moving the mouse to a part of the screen and opening the context menu.
        const e = new MouseEvent('mousedown', {screenX: 50, screenY: 50, clientX: 50, clientY: 50, button: 2});
        component.onContextMenu(e);
        // Test if the x- and y-locations have been changed.
        expect(component.contextMenuLocation.x).toEqual(50, 'Context menu x-location did not change to 50.');
        expect(component.contextMenuLocation.y).toEqual(50, 'Context menu y-location did not change to 50.');

        // Spy on the map method that assigns a lat/long to the menu location
        let containerPointToLatLngSpy: jasmine.Spy;
        containerPointToLatLngSpy = spyOn(component.mapService.map, 'containerPointToLatLng').and.callThrough();

        // Call leaveRating method
        component.leaveRating();

        // Check if Leaflet conversion occurred.
        expect(containerPointToLatLngSpy).toHaveBeenCalled();
        // Allow the program some time to switch to the rating form
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            // Check if program navigated to rating page
            expect(location.path()).toContain('/rating'); // should be: .toBe('/rating?lat=' + lat + '&lng=' + lng); but not able to get lat and lng values
        });
    }));

    it('should handle onContextMenu event', () => {
            // Test if x- and y-locations are set to 0 by default, and that the context menu is not yet active.
            expect(component.contextMenuLocation.x).toEqual(0, 'Context menu x-location was not 0.');
            expect(component.contextMenuLocation.y).toEqual(0, 'Context menu y-location was not 0.');
            expect(component.showContextMenu).toBeFalsy('Expected showContextMenu to be set to false.');
            // Set up a simulated mouse event to simulate a user moving the mouse to a part of the screen and opening the context menu.
            let e = new MouseEvent('mousedown', {screenX: 50, screenY: 50, clientX: 50, clientY: 50, button: 2});
            component.onContextMenu(e);
            // Test if the x- and y-locations have been changed and that the context menu is active.
            expect(component.contextMenuLocation.x).toEqual(50, 'Context menu x-location did not change to 50.');
            expect(component.contextMenuLocation.y).toEqual(50, 'Context menu y-location did not change to 50.');
            expect(component.showContextMenu).toBeTruthy('Expected showContextMenu to be set to true, after running the method.');
            // Set up another simulated mouse event which has the mouse move to a different part of the screen and click the context menu off.
            e = new MouseEvent('mousedown', {screenX: 10, screenY: 10, clientX: 10, clientY: 10, button: 0});
            component.onContextMenu(e);
            // Test if the x- and y-locations have changed and that the context menu has deactivated.
            expect(component.contextMenuLocation.x).toEqual(10, 'Context menu x-location was not 10.');
            expect(component.contextMenuLocation.y).toEqual(10, 'Context menu y-location was not 10.');
            expect(component.showContextMenu).toBeFalsy('Expected showContextMenu to be set to false again.');
    });

    // {BELONGS IN MAPSERVICE SPEC}
    /* it('should set click position', async(() => {
        // Spy on the method
        setLocationSpy = spyOn(component.mapService, 'setLocation');

        component.setClickPosition(1);
        expect(setLocationSpy).toHaveBeenCalled();
    })); */

    it('should toggle side buttons', () => {
        // First test the toggle feature
        expect(component.dropDownButtons).toBeFalsy('Drop down buttons unexpectedly already activated.');
        component.toggleButtons();
        expect(component.dropDownButtons).toBeTruthy('Drop down buttons unexpectedly not activated.');
        // Test manual status switch
        component.toggleButtons(false);
        expect(component.dropDownButtons).toBeFalsy('Drop down buttons did not deactivate.');
        component.toggleButtons(true);
        expect(component.dropDownButtons).toBeTruthy('Drop down buttons did not activate.');
    });

    // {BELONGS IN MAPSERVICE}
    /*it('should focus my location', async(() => {
        // Spy on the method
        getCurrentLocationSpy = spyOn(component.mapService, 'getCurrentLocation');

        component.focusLocation();
        expect(getCurrentLocationSpy).toHaveBeenCalled();
    }));*/
});

  /*    =================================
   *    |      Raster Unit Tests        |
   *    =================================
   */

describe('MapComponent->Raster Layer', () => {
    let component: MapComponent;
    let fixture: ComponentFixture<MapComponent>;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          AppModule,
          RouterTestingModule.withRoutes([
            {path: '', component: MapComponent}
          ])
        ],
        declarations: [
        ],
        providers: [
            {provide: RasterService, useClass: RasterServiceMock},
            LocationService,
            MapService,
            RatingService,
            RouteService,
            SensorService,
            UserService,
            HttpClient,
            HttpHandler
        ]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MapComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
    }));

    afterEach(async(() => {
      // Destroy fixture to reset it for each next test
      fixture.destroy();
    }));

    /*
    // Test if raster cells initialize upon retrieval
    it('should contain raster cell data', async(() => {
        // Retrieve initial raster and test if the data is present in the component
        (<RasterLayer>component.allLayers['raster']).retrieveRaster()
            .then( data => {
                expect((<RasterLayer>component.allLayers['raster']).cellList.length).toBeGreaterThan(0);
            })
            .catch( error => {
                fail('No raster data found in mapComponent.');
            });
    }));

    // Test if raster adds new cells with 2 calls for mock cells
    it('should expand raster when given two different raster datasets', async(() => {
        let currRasterCells: number;

        // Retrieve initial raster and test if the data is present in the component
        (<RasterLayer>component.allLayers['raster']).retrieveRaster()
            .then( data => {
                currRasterCells = (<RasterLayer>component.allLayers['raster']).cellList.length;
                expect(currRasterCells).toBeGreaterThan(0);

                // Retrieve raster for a new area and test if the data present in the component increases
                (<RasterLayer>component.allLayers['raster']).retrieveRaster()
                .then( data2 => {
                    expect((<RasterLayer>component.allLayers['raster']).cellList.length).toBeGreaterThan(currRasterCells);
                })
                .catch( error => {
                    fail('No new raster data added to mapComponent.');
                });
            })
            .catch( error => {
                fail('No raster data found in mapComponent.');
            });
    }));

    // User toggles off Raster, then zooms out and turns on Raster again
    it('should expand raster on zoom out', async(() => {
        // Force map to default view, in case Leaflet bug pops up.
        const USP = [52.0850735, 5.1714655];
        component.mapService.map.setZoomAround(USP, 16, false);
        // Layer should be inactive at the start.
        expect(<boolean>component.isLayerActive('raster')).toBeFalsy('Layer should be inactive at the start');

        // First turn on the raster, as it is turned off by default.
        component.toggleLayer('raster');
        // Wait till the data is in.
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            // Layer should be active now.
            expect(<boolean>component.isLayerActive('raster')).toBeTruthy('Layer should be active now');
            // Expect a set of cell data.
            const rasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length;
            expect(rasterData).toBeGreaterThan(0, 'Raster should load on layer toggle');
        });

        // Save current state for comparison at the end.
        const firstRasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length;

        // Turn it off again now that we have raster data.
        component.toggleLayer('raster');
        // Layer should be inactive again.
        expect(<boolean>component.isLayerActive('raster')).toBeFalsy('Layer should be inactive again');

        const firstZoom = component.mapService.map.getZoom();

        // Zoom out by two levels to cover a large enough area to retrieve new cells over.
        component.mapService.map.setZoomAround([52.088505, 5.180549], 15, false); // setZoom, setView, and zoomOut are all broken for this test, hence setZoomAround

        const nextZoom = component.mapService.map.getZoom();

        // Expect new zoom level to be larger than the initial.
        expect(nextZoom).toBeLessThan(firstZoom, 'Zoom level should have changed');
        expect((<RasterLayer>component.allLayers['raster']).mapChanged).toBeTruthy('Expect program to log that the map view has changed');

        // Turn on the raster again.
        component.toggleLayer('raster');
        // Wait till the data is in.
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(<boolean>component.isLayerActive('raster')).toBeTruthy('Layer should be active again');
            // Expect more or equal data be present compared to the first toggle of the raster.
            const nextRasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length; // Each rectangle polygon is a layer in Leaflet
            expect(nextRasterData).toBeGreaterThanOrEqual(firstRasterData, 'Raster data should be equal or greater than first load');
        });
    }));

    // User toggles off Raster, then zooms in on another area and turns on Raster again.
    it('should expand raster after moving the map', async(() => {
        // Force map to default view, in case Leaflet bug pops up.
        const USP = [52.0850735, 5.1714655];
        component.mapService.map.setZoomAround(USP, 16, false);
        expect(<boolean>component.isLayerActive('raster')).toBeFalsy('Layer should be inactive at the start');

        // First turn on the raster, as it is turned off by default.
        component.toggleLayer('raster');
        expect(<boolean>component.isLayerActive('raster')).toBeTruthy('Layer should be active now');
        // Wait till the data is in.
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            // Expect a set of cell data.
            const rasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length;
            expect(rasterData).toBeGreaterThan(0, 'Raster should load on layer toggle');
        });

        // Save current state for comparison at the end.
        const firstRasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length;

        // Turn it off again now that we have raster data.
        component.toggleLayer('raster');
        expect(<boolean>component.isLayerActive('raster')).toBeFalsy('Layer should be inactive again');

        const firstLocation = component.mapService.map.getCenter();

        // Pan to a new area on the map.
        component.mapService.map.panTo([52.088505, 5.180549]);

        const nextLocation = component.mapService.map.getCenter();

        // Expect new location to differ from the old one.
        expect(nextLocation !== firstLocation).toBeTruthy('Location should have changed');
        // Expect program to log that the map view has changed.
        expect((<RasterLayer>component.allLayers['raster']).mapChanged).toBeTruthy('Program should log that the map view has changed');

        // Turn on the raster again.
        component.toggleLayer('raster');
        expect(<boolean>component.isLayerActive('raster')).toBeTruthy('Raster layer should be active again');
        // Wait till the data is in.
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            // Expect more or equal data be present compared to the first toggle of the raster.
            const nextRasterData = (<RasterLayer>component.allLayers['raster']).rasterFeatureGroup.getLayers().length; // Each rectangle polygon is a layer in Leaflet
            expect(nextRasterData).toBeGreaterThanOrEqual(firstRasterData, 'Raster data should be equal or greater than first load');
        });
    }));
    */
});


    /*    =================================
    *    |      Sensors Unit Tests       |
    *    =================================
    */

describe('MapComponent->Sensor Layer', () => {
    let component: MapComponent;
    let fixture: ComponentFixture<MapComponent>;

    // Spy on service methods
    let retrieveLatestSensorBatchesSpy: jasmine.Spy;
    // let getHistoricalSensorDataSpy: jasmine.Spy;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          AppModule,
          RouterTestingModule.withRoutes([
            {path: '', component: MapComponent}
          ])
        ],
        declarations: [
        ],
        providers: [
            RasterService,
            LocationService,
            MapService,
            RatingService,
            RouteService,
            {provide: SensorService, useClass: SensorServiceMock},
            UserService,
            HttpClient,
            HttpHandler
        ]
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(MapComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
    }));

    afterEach(async(() => {
      // Destroy fixture to reset it for each next test
      fixture.destroy();
    }));

    it('should toggle on and contain sensor data', fakeAsync(() => {
        fixture.detectChanges();
        // Monitor sensor layer's method to see if it gets called, without blocking its functionality.
        retrieveLatestSensorBatchesSpy = spyOn((<SensorLayer>component.allLayers['sensor']), 'retrieveLatestSensorBatches').and.callThrough();

        // Expect layer to be switched from off to on after and retrieving sensor data.
        expect(<boolean>component.isLayerActive('sensor')).toBeFalsy('Sensor layer registers as on when it should be off.');
        component.toggleLayer('sensor');
        expect(retrieveLatestSensorBatchesSpy).toHaveBeenCalled();
        expect(<boolean>component.isLayerActive('sensor')).toBeTruthy('Sensor layer registers as off when it should be on.');

        // Manually progress asynchronous call stack (synchronous-like async testing).
        tick(); // Requires fakeAsync so you can execute async step by step.

        // Look for changes in component.
        fixture.detectChanges();

        // Sensor data should be stored.
        expect((<SensorLayer>component.allLayers['sensor']).sensorsList.length).toBeGreaterThan(0, 'No sensor data stored.');
    }));

    it('should handle erronous call to get sensor data', async(() => {
        // Monitor sensor layer's method to see if it gets called, without blocking its functionality.
        retrieveLatestSensorBatchesSpy = spyOn((<SensorLayer>component.allLayers['sensor']), 'retrieveLatestSensorBatches').and.throwError('400 Bad Request');

        // Purposely prevent it from getting useful data to trigger error.
        (<SensorLayer>component.allLayers['sensor']).mapHandler.map = null;

        // Expect layer to be switched from off to on after and retrieving sensor data.
        expect(<boolean>component.isLayerActive('sensor')).toBeFalsy('Sensor layer registers as on when it should be off.');
        // Expect layer to handle erronous call.
        expect(function() {
            component.toggleLayer('sensor');
        }).toThrowError('400 Bad Request');
        // No data should have been added.
        expect((<SensorLayer>component.allLayers['sensor']).sensorsList.length).toEqual(0, 'Still retrieved sensor data, when it should not.');
    }));
});
