/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { MinigamesService } from 'app/shared/services/minigames.service';
import { NavigationService } from '../../../shared/services/navigation.service';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'seed-minigames',
  templateUrl: './minigames.component.html',
  styleUrls: ['./minigames.component.scss']
})
export class MinigamesComponent implements OnInit {
  constructor(public navigationService: NavigationService, public router: Router, private minigamesService: MinigamesService, location: LocationStrategy) {
    location.onPopState(() => {
        removeEventListener("message", this.sendScores);
    });
  }

  ngOnInit() { 
      //makes url for specific user, which sends user id, and if the user is researchparticipant.
      var url = this.minigamesService.makeUrl();
      //set the iframes source to that url
      document.getElementById('minigame-iframe').setAttribute('src', url);

      //binds the this the scope of sendscores, so that sendScores can still access functions from this class whille inside an event handler.

      //adding listener which listens for scores from the minigame, after the minigame page has been opened
      this.sendScores = this.sendScores.bind(this);
      window.addEventListener("message", this.sendScores); 
  }

  public sendScores(event){
    var minigameServerAdress = this.minigamesService.MinigameServerAdress();
        //check the origin of the data
        if (~event.origin.indexOf(minigameServerAdress)) {
            //navigate to the homepage if the game is closed
            if(event.data == "exit"){
                this.navigate(' ');
            }
            //otherwise procces the data as scores.
            else{
                this.minigamesService.sendScores(event.data);
            }
        } else { 
            //the source wasnt confirmed, the data wasnt from our server, so we wont use this data.
            console.error("cant confirm source: "+ event.origin);
            return; 
        }
  }

      // Navigate to a specific path.
      public navigate(path: string) {
        removeEventListener("message", this.sendScores);
        //this should remove the event listener, but im not sure why it doesnt yet.
        // Navigate to 'path' when closing is finshed
        this.router.navigate([path]);
    }
}
