/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// Import models
import { DailyExposure } from 'app/shared/models/daily-exposure.model';

// Import services
import { UserService } from 'app/shared/services/user.service';
import { MapService, MapState } from 'app/shared/services/map.service';
import { NavigationService } from 'app/shared/services/navigation.service';

import { CalendarMonthViewComponent } from 'angular-calendar';

import { Color, Config } from 'app/common';

import * as moment from 'moment';

@Component({
    selector: 'exposure-history-calendar',
    styleUrls: ['./exposure-history-calendar.component.scss'],
    templateUrl: './exposure-history-calendar.component.html',
})
export class ExposureHistoryCalendarComponent implements OnInit {

    exposureSet: DailyExposure[] = [];
    viewDate: Date;
    view = 'month';
    refreshing = false;
    error = false;

    public isMobile: boolean;
    public weekCount: number;

    @ViewChild('calendar')
    calendar: CalendarMonthViewComponent;

    monthNames: string[] = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];

    constructor(private userService: UserService,
                public router: Router,
                private activatedRoute: ActivatedRoute,
                private mapService: MapService,
                public navigationService: NavigationService) {
                    this.isMobile = Config.IS_MOBILE;
    }

    ngOnInit(): void {
        let date = new Date();

        if (this.activatedRoute.snapshot.queryParams['m']) {
            const dateString = this.activatedRoute.snapshot.queryParams['m'];
            const parts: any[] = dateString.split('-');

            date = new Date(parts[1] + '-' + parts[0] + '-' + 1);
        }

        this.viewDate = date;

        this.refreshExposure(0, date);
    }

    public getWeekCount(viewDate: Date): number {
        const date: Date = new Date(viewDate);

        const firstOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
        const lastOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 1, -0);

        const firstWeek = moment(firstOfMonth).isoWeek();
        const lastWeek = moment(lastOfMonth).isoWeek() === 1 ? 53 : moment(lastOfMonth).isoWeek();

        return lastWeek - firstWeek + 1;
    }

    /** Get exposure for specific date. */
    public getExposure(date: string) {
        const actualDate: Date = new Date(date);
        actualDate.setHours(0);

        const exposure: DailyExposure = this.exposureSet.find(x => this.compareDatesSameDay(actualDate, new Date(x.date)));
        if (exposure == null) {
            return null;
        } else if (exposure.averageExposure === 0) {
            return '?';
        }

        if (Config.IS_MOBILE) {
            return Math.round(exposure.averageExposure);
        }
        return Math.round(exposure.averageExposure * 100) / 100;
    }

    /** Get exposure color for specific exposure value. */
    public getExposureColor(date: string) {
        const exposure = this.getExposure(date);
        if (exposure === '?') {
            return Color.getExposureColor(0);
        }
        return Color.getExposureColor(exposure);
    }

    /** Check if the current month contains a warning.  */
    public monthContainsWarning(): boolean {
        const d: Date = new Date(this.viewDate);
        return this.exposureSet.filter(x => {
            const xDate = new Date(x.date);
            return xDate.getMonth() === d.getMonth() && xDate.getFullYear() === d.getFullYear() && this.warningThreshold(x.averageExposure);
        }).length > 0;
    }

    /** Warning threshold function. */
    public warningThreshold(exposure: number) {
        return exposure > 60;
    }

    /** Get width of circle in calendar. */
    public getCircleWidth(date: string) {
        const exposure = this.getExposure(date);
        /** Return 0 if there was no data for a day.
            If there is data, but it is 0, there are still segments for that day,
            which we want to represent with the smallest grey circle. */
        if (exposure === null) {
            return 0;
        } else if (exposure === '?') {
            return 35;
        }

        const exposureUpperBound = 200.0;
        const diamaterUpperBound = 85.0;
        const diameterUnderBound = 10.0;

        // Exposure is always a number after the previous if statements
        const y = Math.min(1, <number>exposure / exposureUpperBound) * 100;

        const x = (Math.log(y + 1) / Math.log(101)) * (diamaterUpperBound - diameterUnderBound) + diameterUnderBound;

        return x;
    }

    /** Refresh exposure if new data is available. */
    public refreshExposure(monthOffset: number, date: Date = this.calendar.viewDate) {
        const firstDay = new Date(date.getFullYear(), date.getMonth() + monthOffset, 1, 0);
        const lastDay = new Date(date.getFullYear(), date.getMonth() + monthOffset + 1, 1, -0);
        this.weekCount = this.getWeekCount(firstDay);

        this.error = false;

        this.router.navigate([], { queryParams: { m: (firstDay.getMonth() + 1) + '-' + firstDay.getFullYear() } });

        // Check if we already have data from this month.
        if (this.exposureSet.find(x => {
            const day = new Date(x.date);
            return day.getFullYear() === firstDay.getFullYear() && day.getMonth() === firstDay.getMonth();
        })) {
            return;
        }

        this.refreshing = true;

        this.userService.getDailyExposures(firstDay, lastDay).then(response => {
            this.refreshing = false;
            this.exposureSet = this.exposureSet.concat(response);
        }).catch(() => {
            this.error = true;
            this.refreshing = false;
        });
    }

    /** Check if dates are the same day. */
    public compareDatesSameDay(a: Date, b: Date): boolean {
        const difference = moment(a).diff(b, 'minutes');
        if (difference < 0) {
            return difference >= -12 * 60;
        } else {
            return difference < 12 * 60;
        }
    }

    /* Get name of the month. */
    public getMonthName(date: Date): string {
        const newDate = new Date(date);
        return this.monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear();
    }

    /** Navigate to specific date if date is found. */
    public navigate(date: string): void {
        const day = new Date(date);

        // Only navigate if ExposureSet contains data for this day.
        if (this.exposureSet.find(x => {
            const xDate = new Date(x.date);
            return xDate.getDate() === day.getDate() && xDate.getMonth() === day.getMonth() && xDate.getFullYear() === day.getFullYear();
        })) {
            this.router.navigate(['/exposure/day'], { queryParams: { d: moment(date).format('DD-MM-YYYY') } });
        }
    }

    /** Close modal and navigate to home or exposure/day. */
    public closeModal() {
        switch (this.mapService.mapState.value) {
            case MapState.Default: this.router.navigate(['']); break;
            case MapState.RoutePlanner: this.router.navigate(['route-planner']); break;
            case MapState.ExposureOverview: this.router.navigate(['exposure/day']);
        }
    }
}
