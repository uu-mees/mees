/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { ExposureHistoryCalendarComponent } from './exposure-history-calendar.component';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { AppModule } from '../../../../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../../../shared/services/user.service';
import { ActivatedRoute } from '@angular/router';

describe('ExposureHistoryCalendarComponent', () => {
    let component: ExposureHistoryCalendarComponent;
    let fixture: ComponentFixture<ExposureHistoryCalendarComponent>;

    describe('without query parameters', () => {
        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [
                    AppModule,
                    RouterTestingModule.withRoutes([
                        { path: 'exposure/overview', component: ExposureHistoryCalendarComponent }
                    ])
                ],
                declarations: [
                ],
                providers: [
                    UserService
                ]
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExposureHistoryCalendarComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        it('should create', () => {
            expect(component).toBeTruthy('ExposureHistoryCalendarComponent not defined');
        });

        it('should navigate to current month without query params', () => {
            const date = new Date();

            expect(component.viewDate.getMonth()).toBe(date.getMonth());
            expect(component.viewDate.getFullYear()).toBe(date.getFullYear());
        });
    });
});
