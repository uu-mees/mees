/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import {Component, Injectable, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Import services
import { RatingService } from 'app/shared/services/rating.service';

// Import models
import { GeoLocationOption } from 'app/shared/models/geolocation-option.model';
import { Rating } from 'app/shared/models/rating.model';
import { Pos } from 'app/shared/models/pos.model';

import 'hammerjs';

@Component({
    selector: 'app-rating-form',
    templateUrl: 'rating-form.component.html',
    styleUrls: ['rating-form.component.scss']
})

export class RatingFormComponent implements OnInit {

    private lat: number;
    private lng: number;
    private average: number;

    public ratingValue = 3;
    public address = '';

    constructor(private ratingService: RatingService,
                private router: Router,
                private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.createRating(params);
        }, error => {
            console.error(error);
        });
    }

    /** Create a rating based on the query params */
    private createRating(params) {
        if (!!params && !!params.lat && !!params.lng) {
            this.lat = params.lat;
            this.lng = params.lng;
            this.address = !!params.address ? params.address : GeoLocationOption[2].toLowerCase();

            this.getAverageRating();
        }
    }
    /** Submit a rating to the server. */
    public submitRating() {
        const position = new Pos(this.lat, this.lng);
        const rate = new Rating(position, this.ratingValue);

        this.ratingService.postRating(rate).toPromise().then(
            () =>  {
                this.closeModal();
            }
        ).catch((error) => console.error(error.message));
    }

    /** Create subscription for the average rating for location. */
    private getAverageRating() {
        this.ratingService.getAverage(this.lat, this.lng, 1000)
            .subscribe(average => this.average = average.toPrecision(2));
    }

    /** Close the rating modal. */
    public closeModal() {
        this.router.navigate(['']);
    }

    /** Return average rating. */
    get getAverage(): number {
        return this.average;
    }
}
