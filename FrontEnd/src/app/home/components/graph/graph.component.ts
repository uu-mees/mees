/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';

import { SensorService } from 'app/shared/services/sensor.service';

import { MeasurementComponent, Config } from 'app/common/index';
import * as moment from 'moment';
import { Chart } from 'chart.js';
import 'hammerjs';

@Component({
    selector: 'app-graph',
    templateUrl: './graph.component.html',
    styleUrls: ['./graph.component.scss'],
    animations: [
        trigger('slideAnimation', [
            state('true', style({
                display: 'block',
                bottom: '0'
            })),
            state('false', style({
                display: 'none',
                bottom: '-250px'
            })),
            transition('true <=> false', animate('100ms ease-in')),
        ]),
    ]
})
export class GraphComponent implements OnInit {

    public latestSensor = '';
    public latestMeasurement = [];
    public gettingGraphData = false;
    public visibility = false;
    public mobile = Config.IS_MOBILE;

    public graphFrom = null;
    public graphTo = null;

    public componentNames = [];
    public componentUnits = [];
    public componentDataSets : GraphData[] = [];
    public currentComponent = 0;

    private queryParams: {[key: string]: any} = {};

    private chart;

    constructor(private sensorService: SensorService,
                private activatedRoute: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {

        moment.locale('nl');

        this.createChart();

        this.activatedRoute.queryParams.subscribe(params => {

            // If params exist, create graph
            if (!!params.id && !!params.from && !!params.to) {
                this.queryParams = params;
                this.gettingGraphData = true;

                this.graphFrom = moment(params.from, 'YYYY-MM-DD').format('L');
                this.graphTo = moment(params.to, 'YYYY-MM-DD').format('L');

                this.setVisibility(true);
                this.getBatches();
            } else {
                this.router.navigate(['']);
            }
        }, error => {
            console.error(error);
        });
    }

    /** Initialize the chart.  */
    private createChart() {
        // Set basic options for the chart
        const options = {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    stacked: false,
                    gridLines: {
                        display: true,
                        color: 'rgba(0,0,0,0.2)'
                    },
                    scaleLabel: {
                        display: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    type: 'time',
                    time: {
                        displayFormats: {
                            hour: 'H:mm'
                        },
                        unit: 'hour'
                    }
                }]
            }
        };

        // Init chart with empty labels and datasets
        this.chart = Chart.Line('chart', {
            options: options,
            data: [{
                labels: [],
                datasets: [],
            }]
        });
    }

    /** Subscribe to the SensorService to receive the batch data. */
    public getBatches(): void {
        if (!!this.queryParams && !!this.queryParams.id && !!this.queryParams.from && !!this.queryParams.to) {
            this.sensorService.getHistoricalSensorData(this.queryParams.id, this.queryParams.from, this.queryParams.to)
            .then(
                batches => {
                    // Check if there are any batches
                    if (batches.length !== 0) {
                        this.handleBatches(batches);
                    // Otherwise display 'no data'
                    } else {
                        this.noData();
                    }

                    this.gettingGraphData = false;
                }, error => {
                    console.error(error);
                }
            );
        }
    }

    /** Handle batch data. */
    private handleBatches(batches): void {

        this.componentDataSets = [];
        this.componentNames = [];
        this.componentUnits = [];

        // Handle the batch data
        this.handleBatchData(batches);

        // Set labels to the timestamps
        this.chart.config.data.labels = this.componentDataSets[this.currentComponent].timeStamps;

        // Create basic dataset
        const dataset = {
            backgroundColor: 'rgba(20,20,20,0.2)',
            borderColor: 'rgba(20,20,20,1)',
            borderWidth: 2,
            hoverBackgroundColor: 'rgba(30,30,30,0.4)',
            hoverBorderColor: 'rgba(30,30,30,1)',
            data: this.componentDataSets[this.currentComponent].values
        };

        // Push basic dataset if none exists
        if (this.chart.config.data.datasets.length === 0) {
            this.chart.config.data.datasets.push(dataset);
        }

        // Set the dataset as data for the graph
        this.setNewDataSet(this.currentComponent);

        // Set the latest measurement
        this.setMeasurement(batches[0]);
    }

    /** Handle batch data. */
    private handleBatchData(batches) {
        // Create the measurements template
        batches[0].measurements.forEach((measurement, index) => {
            this.componentNames[index] = MeasurementComponent.getComponentName(measurement.component);
            this.componentUnits[index] = measurement.unit;
        });

        // Create measurements list for each batch
        batches.forEach((batch) => {
            const batchTime = moment(batch.timeStampPolled + 'Z').format();

            // Add value to specific list of
            batch.measurements.forEach((measurement, index) => {

                // Create empty array at index, if array doesn't exist
                if (!this.componentDataSets[index])
                    this.componentDataSets[index] = {values:[],timeStamps:[]};

                if (measurement.value > 0)
                {
                    this.componentDataSets[index].values.push(measurement.value);
                    this.componentDataSets[index].timeStamps.push(batchTime);
                }
            });
        });
    }

    /** Set the latest measurements.  */
    private setMeasurement(latestBatch): void {
        const helpArray = [];
        this.latestMeasurement = [];
        this.latestSensor = latestBatch.sensorId;
        latestBatch.measurements.forEach(measurement => {
            const obj: {[k: string]: any} = {};

            obj.component = MeasurementComponent.getComponentName(measurement.component);
            obj.value = measurement.value.toString();
            obj.unit = measurement.unit;

            if (!helpArray.includes(obj.value)) {
                helpArray.push(obj.value);
                this.latestMeasurement.push(obj);
            }

        });
    }

    /** Set new data to the chart. */
    public setNewDataSet(component: number) {

        // Set component
        this.currentComponent = component;

        // Create empty dataset
        const newDataset = new Array<any>();

        // Set labels to the timestamps
        this.chart.config.data.labels = this.componentDataSets[this.currentComponent].timeStamps;

        // Create new dataset for NO2 and push to the chart
        newDataset.push({
            backgroundColor: 'rgba(20,20,20,0.2)',
            borderColor: 'rgba(20,20,20,1)',
            borderWidth: 2,
            hoverBackgroundColor: 'rgba(30,30,30,0.4)',
            hoverBorderColor: 'rgba(30,30,30,1)',
            data: this.componentDataSets[this.currentComponent].values
        });

        // Push new data if it doesn't exist yet
        this.chart.config.data.datasets[0].data = newDataset[0].data;

        this.chart.config.data.datasets[0].label = this.componentNames[this.currentComponent];
        this.chart.config.options.scales.yAxes[0].scaleLabel.labelString = this.componentUnits[this.currentComponent];

        // Update chart with new data
        this.chart.update();
    }

    /** Set new date for the graph. */
    public setDate(inc: number, timeSpan: moment.unitOfTime.DurationConstructor = 'days') {
        let from, to;

        // Subtract 1 day if decrease, else increase by 1 day
        if (inc < 0) {
            from = moment(this.graphFrom, 'L').subtract(-1 * inc, timeSpan);
            to = moment(this.graphTo, 'L').subtract(-1 * inc, timeSpan);
        } else {
            from = moment(this.graphFrom, 'L').add(inc, timeSpan);
            to = moment(this.graphTo, 'L').add(inc, timeSpan);
        }

        // Check if from is earlier than now
        if (from.valueOf() < moment().valueOf()) {

            // Check if to is earlier than now
            const queryParams = {
                id: this.queryParams.id,
                from: from.format('YYYY-MM-DD'),
                to: to.format('YYYY-MM-DD')
            };

            // Navigate to the new page
            this.router.navigate([], { queryParams: queryParams });
        }
    }

    /** Set visibility of the graph component. */
    public setVisibility(visible: boolean): void {
        this.visibility = visible;

        if (!visible) {
            setTimeout(() => { this.router.navigate(['']); }, 150);
        }
    }

    /** No data is present. */
    private noData() {
        this.latestSensor = '';
    }
}

interface GraphData
{
    values: Array<any>;
    timeStamps: Array<any>;
}
