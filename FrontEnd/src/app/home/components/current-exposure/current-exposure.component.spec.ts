/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentExposureComponent } from './current-exposure.component';
import { AppModule } from '../../../app.module';
import { MaterialModule } from '../../../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('CurrentExposureComponent', () => {
  let component: CurrentExposureComponent;
  let fixture: ComponentFixture<CurrentExposureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
            AppModule,
            MaterialModule,
            RouterTestingModule.withRoutes([
                {path: '', component: CurrentExposureComponent}
              ])
        ],
        declarations: [
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentExposureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
