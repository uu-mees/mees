/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { Subscription } from 'rxjs/Subscription';

// Import services
import { MapService } from 'app/shared/services/map.service';
import { UserService } from 'app/shared/services/user.service';
import { ExposureService } from 'app/shared/services/exposure.service';
import { AuthService, Role } from 'app/shared/services/auth.service';
import { MapState } from 'app/shared/services/map.service';

import { Color } from 'app/common';
import * as L from 'leaflet';

@Component({
    selector: 'current-exposure',
    templateUrl: './current-exposure.component.html',
    styleUrls: ['./current-exposure.component.scss'],
    animations: [
        trigger('fadeInAnimation', [
            state('true', style({
                display: 'flex',
                opacity: 1
            })),
            state('false', style({
                display: 'none',
                opacity: 0
            })),
            transition('true <=> false', animate('50ms ease-in')),
        ]),
    ]
})
export class CurrentExposureComponent implements OnDestroy, OnInit {

    public mapstate = MapState;

    public exposure = 0;

    public multiplier = 1;

    public displayItems: boolean;

    private settingsSubscription: Subscription;
    private watchSubscription: Subscription;
    private authSubscription: Subscription;

    constructor(public mapService: MapService,
        private exposureService: ExposureService,
        private userService: UserService,
        private authService: AuthService) {
    }

    ngOnInit(): void {
        this.displayItems = true;

        this.authSubscription = null;
        this.settingsSubscription = null;
        this.watchSubscription = null;

        // Wait for the map to load...
        setTimeout(() => this.getExposure(), 2500);
    }

    // authsubscription for when the authentication status changes
    // settingssubscription for when the user settings.tracklocation changes
    // watchsubscription to see if component is already watching the user's location
    /** Get exposure for the users location. This requires various subscriptions. */
    private getExposure(): void {
        this.authSubscription = this.authService.authenticationStream.subscribe(x => {
            if (x !== Role.Unauthenticated) { // User is logged in.
                this.settingsSubscription = this.userService.userProfile.subscribe(settings => {
                        this.displayItems = true;
                        if (this.watchSubscription == null) {
                            this.watchSubscription = this.subscribeToWatchCurrentlocation();
                        }
                });
            } else { // User is not logged in.
                if (!!this.settingsSubscription) { this.settingsSubscription.unsubscribe(); }
                this.displayItems = true;
                if (this.watchSubscription == null) {
                    this.watchSubscription = this.subscribeToWatchCurrentlocation();
                }
            }
        });
    }

    /** Subscription to watchCurrentLocation, which follows the user geolocation. */
    private subscribeToWatchCurrentlocation() {
        return this.mapService.watchCurrentLocation().subscribe(
            (latlng) => {
                if (!!latlng) {
                    this.getExposureValue(latlng);
                }
            }, error => {
                console.error(error);
            }
        );
    }

    /** Get exposure value for a specific LatLng location. */
    private getExposureValue(latlng: L.LatLng): void {
        this.exposureService.getLocationExposure(latlng).toPromise().then(
            (exposure) => {
                this.exposureService.setLocationExposure(exposure);
                this.exposure = exposure;
            }).catch((error) => {
                console.error(error);
            });
    }

    /** Get color based on exposure. */
    public getExposureColor(): string {
        return Color.getExposureColor(this.exposure * this.multiplier);
    }

    /** Change multiplier based on location. */
    public setMultiplier(event, multiplier): void {
        this.multiplier = multiplier;
    }

    /** Round exposure to an Integer. */
    public roundExposure(exposure): string {
        if (!!exposure) {
            return Math.round(exposure).toString();
        }

        return '-';
    }

    public focusCurrentLocation() {
        this.mapService.focusCurrentLocation();
    }

    /** Cleanup subscriptions on component destruction. */
    ngOnDestroy() {
        if (!!this.settingsSubscription) { this.settingsSubscription.unsubscribe(); }
        if (!!this.watchSubscription) { this.watchSubscription.unsubscribe(); }
        if (!!this.authSubscription) { this.authSubscription.unsubscribe(); }
    }
}
