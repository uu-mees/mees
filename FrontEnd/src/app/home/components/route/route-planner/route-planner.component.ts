/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import 'rxjs/add/operator/filter';

// Load Models
import { GeoResult } from 'app/shared/models/georesult.model';
import { GeoLocation } from 'app/shared/models/geolocation.model';
import { Transportation } from 'app/shared/models/transportation.model';
import { GeoLocationOption } from 'app/shared/models/geolocation-option.model';
import { RouteParameters } from 'app/shared/models/route-parameters.model';

// Load Services
import { RouteService } from 'app/shared/services/route.service';
import { LocationService } from 'app/shared/services/location.service';
import { MapService, MapState } from 'app/shared/services/map.service';
import { NavigationService } from 'app/shared/services/navigation.service';

import { Config } from 'app/common';

import 'hammerjs';

@Component({
    selector: 'route-planner',
    templateUrl: './route-planner.component.html',
    styleUrls: ['./route-planner.component.scss']
})
export class RoutePlannerComponent implements OnInit {

    /** Search queries from the user */
    public locationQueries  = new Array<string>(3).fill('');
    public locations        = new Array<GeoLocation>(3).fill(null);

    /** Paramters that were searched last */
    public latestParams: RouteParameters;

    /** Array if multiple locations have been found */
    public multipleLocations = {
        index: 0,
        locations: new Array<GeoResult>(),
        query: ''
    };

    /** Error and message that get displayed below the route planner */
    public routingError = {
        message: ''
    };

    /** Transportation currently used, 'bike' (1) by default */
    public currentTransport = 1;

    /** Boolean if someone is planning a route */
    public planningRoute = false;

    /** Element that has the focus */
    public hasFocus = 0;

    /** If RouteNav should display the via option */
    public hasViaOption = false;

    /** Paramter options */
    private paramNames = ['from', 'via', 'to'];

    @ViewChild('routenav')
    public routenav: MatSidenav;

    constructor(private routeService: RouteService,
        private locationService: LocationService,
        private mapService: MapService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        public navigationService: NavigationService) {
        }

    ngOnInit(): void {
        this.mapService.mapState.next(MapState.RoutePlanner);

        // FIXME: open animation only showing with timeout of 50ms
        setTimeout(() => { this.routenav.open(); this.planningRoute = true; }, 50);

        this.activatedRoute.queryParams.subscribe(params => {

            // Get new route parameters
            const newParams = new RouteParameters(params);

            // Set update to true if no params were set
            if (!this.latestParams) {
                newParams.setUpdate(true);
            }

            // Set latest params
            this.latestParams = newParams;

            // Only update if necessary
            if (this.latestParams.update) {
                this.setLocationQueriesFromParams();
            }
        }, error => {
            console.error(error);
        });
    }

    /** Set queries for location based on paramaters from URL. */
    private setLocationQueriesFromParams() {
        this.paramNames.forEach((param, index) => {

            // Check if a via parameter is set
            if (param === this.paramNames[1] && !!this.latestParams[param]) {
                this.hasViaOption = true;
            } else if (param === this.paramNames[1] && !this.latestParams[param]) {
                this.hasViaOption = false;
            }

            // Check if a parameter already exists
            if (!!this.latestParams[param]) {
                if (!(!!this.locations[index] && this.latestParams[param] === this.locations[index].getQuery())) {
                    this.checkLocationQueriesFromParams(index, this.latestParams[param]);
                }
            // Check if parameter exists, if not, remove and update the RouteService
            } else if (!this.latestParams[param]) {
                this.clearLists(index);
            }
        });

        // Check if transport exists, if not, default to 'bike' (1)
        if (this.latestParams.transport != null) {
            this.setTransport(this.latestParams.transport);
        } else {
            this.setTransport(1);
        }
    }

    /** Check the location queries for extra information and return the resulting query. */
    private checkLocationQueriesFromParams(index: number, param: string): void {

        // Split incoming query
        const paramSplit = param.split(/:(.+)/);

        // Switch on first letter in query
        switch (paramSplit[0]) {
            // Search for a selected location (latlng)
            case 's':
                const latlng = paramSplit[1].split(',').map(coord => parseFloat(coord));

                // Check if the 2 elements latitude and longitude are set
                if (latlng.length === 2) {
                    this.getLocationLatLng(index, latlng[0], latlng[1], 2);
                }
                break;

            // Search for an address
            case 'a':
                const address = paramSplit[1];

                // Check if the address is not empty
                if (!!address) {
                    this.getLocationAddress(index, address, 0);
                }

                break;

            // Search for the current location
            case 'c':
                this.getCurrentLocation(index);

                break;

            default:
                this.locationQueries[index] = 'a:' + this.locationQueries[index];
                this.getLocationAddress(index, param, 0);
                break;
        }

    }

    /** Navigate to a specific path. */
    public navigate(path: string) {
        this.routenav.close();
        this.planningRoute = false;

        this.clearRouting();

        // Navigate to 'path' when closing is finshed
        this.routenav.onClose.subscribe(() => {
            this.router.navigate([path]);
        }, error => {
            console.error(error);
        });
    }

    /** Get location of an address. */
    private getLocationAddress(index: number, query: string, locationOption: GeoLocationOption): void {
        this.locationService.getGeoLocationAddress(query)
            .subscribe(result => {
                this.checkGeoResults(index, result.results, locationOption, query);
            }, error => {
                console.error(error);
            });
    }

    /** Get location of a latitude, longitude point */
    private getLocationLatLng(index: number, latitude: number, longitude: number, locationOption: GeoLocationOption): void {
        this.locationService.getGeoLocationLatLng(latitude, longitude)
            .subscribe(results => {
                // Create dummy results without address if no results have been found
                if (results.results.length === 0) {
                    const result = new GeoResult('', latitude, longitude);
                    results = { results: [result] };
                }
                this.checkGeoResults(index, results.results, locationOption);
            }, error => {
                console.error(error);
            });
    }

    /** Get location of the browser */
    private getCurrentLocation(index: number): void {

        const latlng = this.mapService.getCurrentLocation();

        // If a latlng has been set, create result, location and set it
        if (!!latlng) {
            const result = new GeoResult('', latlng.lat, latlng.lng);
            const location = new GeoLocation(result, 1, index);

            this.setRouteLocation(location);
        } else {
            this.locations[index] = null;
        }
    }

    /** Set location of a position. */
    private checkGeoResults(index: number, results: Array<GeoResult>, locationOption: GeoLocationOption, query?: string): void {

        // If 1 location is found, set it's location
        if (results.length === 1) {
            const location = new GeoLocation(results[0], locationOption, index, query);

            this.setRouteLocation(location);

        // If multiple locations are found, add to multipleLocations object and let user select
        } else if (results.length > 1) {
            this.multipleLocations.index = index;
            this.multipleLocations.locations = results;
            this.multipleLocations.query = query;

        // No location was found
        } else {
            let notFound;

            if (!!this.locationQueries[index]) {
                notFound = this.locationQueries[index];
            }

            if (!!notFound) {
                this.routingError.message = 'No locations found for <i>' + notFound + '</i>.';
                this.locations[index] = null;
            }
        }
    }

    /** Set location when multiple were possible. */
    private setSelectedGeoResult(index: number, result: GeoResult, query: string): void {
        // Create GeoLocation
        const location = new GeoLocation(result, 0, index, query);

        // Set route location in the routeService
        this.setRouteLocation(location);

        // Reset MultipleLocations
        this.multipleLocations = {
            index: 0,
            locations: new Array<GeoResult>(),
            query: ''
        };
    }

    /** Set location with index in the routeService. */
    private setRouteLocation(location: GeoLocation): void {

        // Check if location exists
        if (!!location) {
            const previousLocation = this.locations[location.routeOption];

            // If the previous location is not the same as the current
            // Change the lists and update the RouteService
            if (!previousLocation || (
                previousLocation.lat !== location.lat &&
                previousLocation.lng !== location.lng &&
                previousLocation.address !== location.address)) {
                this.locationQueries[location.routeOption] = !!location.address ? location.address : location.name;
                this.locations[location.routeOption] = location;

                this.updateRouteService();
            }
        }
    }

    /** Set transport type. */
    public setTransport(transport: any): void {
        const previousTransport = this.currentTransport;

        // Check transport type and return the right transportation number (0 to 2)
        if (typeof transport === 'string') {
            this.currentTransport = Transportation[String(transport)];
        } else {
            this.currentTransport = Number(transport);
        }

        // Set back to the previous transport if no new could be found
        if (this.currentTransport === undefined || this.currentTransport == null) {
            this.currentTransport = previousTransport;
        }

        // If the transportation has changed,
        // Change the transportation in RouteService
        if (this.currentTransport !== previousTransport) {
            // Set transport type in the route service
            this.routeService.setTransportation(Transportation[this.currentTransport]);

            // Recieve new routes if there is a route
            if (this.isRoute) {
                this.updateRouteService(false);
            }
        }
    }

    /** Set query to MyLocation. */
    private setMyLocation(): void {
        if (!this.isSearching) {
            this.locationQueries[this.hasFocus] = 'c:' + GeoLocationOption[1];
            this.latestParams[this.paramNames[this.hasFocus]] = 'c:' + GeoLocationOption[1];

            this.updateRouter();
        }
    }

    /** Set query to SelectedLocation. */
    private setSelectedLocation(routeOption: number): void {
        this.locationQueries[routeOption] = GeoLocationOption[2];
    }

    /** Set new locations in the routeService. */
    private updateRouteService(update: boolean = true): void {

        // Update RouteService if route exists
        if (this.isRoute) {

            // Set RouteService to searchint
            this.routeService.setSearchingRoute(true);

            // Hide Sidenav on mobile
            if (Config.IS_MOBILE) {
                this.routenav.close();
            }

            // Set the new values as route
            this.routeService.setLocation(this.locations);
        }

        this.updateRouter(update);
    }

    /** Set location on a clicked location. */
    private onClickLocation(location: GeoLocation): void {

        // Check if location is set and there is a routeoption
        if (!!location && location.routeOption !== null) {
            this.routenav.open();
            this.planningRoute = true;

            // If GeoCoding resulted in nothing, set it as a selection, not an address
            if (location.locationOption === 2) {
                this.setSelectedLocation(location.routeOption);
            }

            this.setRouteLocation(location);
        }
    }

    // #region Helper Functions

    /** Check if type of transport is the current transportation. */
    public isTransport(transport: any): boolean {
        if (typeof transport === 'number') {
            return this.currentTransport === transport;
        }

        return Transportation[this.currentTransport].toLowerCase() === String(transport).toLowerCase();
    }

    /** Check if the RouteService is searching for routes. */
    public get isSearching(): boolean {
        return this.routeService.isSearchingRoute();
    }

    /** Returns true if a route has a start and destination. */
    public get isRoute(): boolean {
        const from = !!this.locations[0];
        const to = !!this.locations[2];

        return !!from && !!to;
    }

    /** Returns true if the route has a via location. */
    public get hasVia(): boolean {
        const via = this.locations[1];
        this.hasViaOption = true;

        return !!via;
    }

    /** Returns true if a route has a start, via and destination. */
    public get isFullRoute(): boolean {
        return this.isRoute && this.hasVia;
    }

    // #endregion

    /** Set or unset the via option. */
    public toggleVia() {
        this.hasViaOption = !this.hasViaOption;

        // If there is no via anymore, clear these lists
        if (!this.hasViaOption) {
            this.clearLists(1);

            this.updateRouter();
        }
    }

    /** Clear routing variables. */
    public clearRouting(): void {
        this.clearLists(-1);

        this.routeService.resetLocations();
        this.routeService.setSearchingRoute(false);
        this.mapService.reset();

        this.planningRoute = false;
        this.router.navigate(['']);
    }

    /** Clear lists on a specified location, or all if value is below 0. */
    private clearLists(loc: number) {
        // Clear lists
        if (loc < 0) {
            this.locationQueries.fill('');
            this.locations.fill(null);
            this.latestParams = null;
        } else {
            // Clear location query list entry
            if (loc < this.locationQueries.length) {
                this.locationQueries[loc] = '';
            }
            // Clear location list entry
            if (loc < this.locations.length) {
                this.locations[loc] = null;
            }
            // clear parameter list entry
            if (loc < this.paramNames.length) {
                this.latestParams[this.paramNames[loc]] = '';
            }
        }
    }

    /** Switch start and end position by reversing the array. */
    public switchLocations(): void {
        this.locationQueries.reverse();
        this.locations.reverse();

        // Reverse latestParams
        const tempFrom = this.latestParams['from'];
        this.latestParams['from'] = this.latestParams['to'];
        this.latestParams['to'] = tempFrom;

        this.routeService.setLocation(this.locations);

        this.updateRouter(false);

        // Only update routeService when a route exists
        if (this.isRoute) {
            this.routeService.setSearchingRoute(true);

            // Hide Sidenav on mobile
            if (Config.IS_MOBILE) {
                this.routenav.close();
            }
        }
    }

    /** Update the router URL with the current location queries. */
    public updateRouter(update: boolean = true) {
        this.routingError.message = '';

        this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: this.getQueryParams(update)
        });
    }

    /** Get routing parameters from router URL query. */
    private getQueryParams(update: boolean): any {

        const params: any = {};

        // Set up the query for each parameter (from, via, to)
        this.paramNames.forEach((name, index) => {

            // Check if a query exists
            if (!!this.locationQueries[index]) {

                const split = this.locationQueries[index].split(':')[0];

                if (split === 'c') {
                    params[name] = this.locationQueries[index];

                // If no previous location exist, or if the new query is not the same as the old one
                } else if ((!this.locations[index]) || (this.locations[index].name !== this.locationQueries[index])) {
                    params[name] = 'a:' + this.locationQueries[index];
                } else {
                    params[name] = this.locations[index].getQuery();
                }

            // Check if there is a previous paramter
            } else if (!!this.latestParams[name]) {
                params[name] = this.latestParams[name];
            }
        });

        // Set transportation
        params.transport = Transportation[this.currentTransport];

        // Set update
        params.update = update;

        return params;
    }
}
