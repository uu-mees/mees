/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { RoutePlannerComponent } from './route-planner.component';

import { AppModule } from 'app/app.module';

import { LocationService } from 'app/shared/services/location.service';
import { RouteService } from 'app/shared/services/route.service';
import { MapService } from 'app/shared/services/map.service';


describe('RoutePlanner', () => {
    let component: RoutePlannerComponent;
    let fixture: ComponentFixture<RoutePlannerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
            AppModule,
            RouterTestingModule.withRoutes([
                {path: '', component: RoutePlannerComponent}
              ])
            ],
            declarations: [
            ],
            providers: [
            RouteService,
            LocationService,
            MapService,
            HttpClient,
            HttpHandler
            ]
        })
        .compileComponents()
        .then(() => {
            fixture = TestBed.createComponent(RoutePlannerComponent);
            component = fixture.componentInstance;
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoutePlannerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
