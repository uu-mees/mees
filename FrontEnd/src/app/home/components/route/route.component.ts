/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, ViewChild } from '@angular/core';

import { RoutePlannerComponent } from './route-planner/route-planner.component';

@Component({
    selector: 'app-route',
    templateUrl: './route.component.html',
    styleUrls: ['./route.component.scss']
})
export class RouteComponent {

    @ViewChild('planner')
    public plannerComponent: RoutePlannerComponent;

    constructor() {
    }

}
