/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { Subscription } from 'rxjs/Subscription';

// Import services
import { MapService } from 'app/shared/services/map.service';
import { RouteService } from 'app/shared/services/route.service';
import { RatingService } from 'app/shared/services/rating.service';

// Import models
import { GeoLocation } from 'app/shared/models/geolocation.model';

import * as L from 'leaflet';

@Component({
    selector: 'route-selection',
    templateUrl: './route-selection.component.html',
    styleUrls: ['./route-selection.component.scss'],
    animations: [
        trigger('popupAnimation', [
            state('true', style({
                display: 'flex',
                transform: 'translate(-50%, 0) scale(1, 1)',
            })),
            state('false', style({
                display: 'none',
                transform: 'translate(-50%, 0) scale(1, 0)',
            })),
            transition('true <=> false', animate('50ms ease-in')),
        ]),
    ]
})
export class RouteSelectionComponent implements OnDestroy {

    private subscriptions: Subscription[] = [];
    location = new GeoLocation(null);

    displayRouteSelection = false;

    constructor(private mapService: MapService,
                private routeService: RouteService,
                private ratingService: RatingService,
                private router: Router) {

        // Check for updates of the clicked position
        this.subscriptions.push(mapService.lastLongPressLocation.subscribe(
            location => {
                this.setLocation(location);
            }, error => {
                console.error(error);
            }));
    }

    /** Display location as popup */
    private setLocation(location: GeoLocation): void {
        if (!!location && location.routeOption !== null) {
            this.location = location;
            this.showRouteSelection();
        }
    }

    /** Select destination for route */
    public selectLocation(): void {
        let to = '';

        if (!!this.location.address) {
            to = 'a:' + this.location.address;
        } else {
            to = 's:' + [this.location.lat, this.location.lng].join(',');
        }

        this.router.navigate(['route-planner'], { queryParams: { to: to, update: 'true' } });
    }

    /** Set marker in the mapService */
    private setMarker(position: Array<number>): void {
        const marker = L.marker(position, { draggable: true });
        this.mapService.addMarker(marker, 2);
    }

    /** Hide route selection view */
    public hideRouteSelection(): void {
        this.displayRouteSelection = false;
    }

    /** Show route selection view */
    public showRouteSelection(): void {
        this.displayRouteSelection = true;
    }

    /** Hide route selection and reset the mapservice */
    public closeRouteSelection(): void {
        this.hideRouteSelection();
        this.mapService.reset();
    }

    /** Navigate to rating modal with the location */
    public leaveRating() {
        const address = this.location.address.split(',')[0];
        this.router.navigate(['/rating'], { queryParams: { lat: this.location.lat, lng: this.location.lng, address: address } });
    }

    /** Cleanup subscriptions */
    ngOnDestroy(): void {
        this.subscriptions.forEach(x => x.unsubscribe());
    }

    /** Display or hide the route planner depending on the research group the user is in. */
    public toggleRoutePlannerOn(): boolean {
        return this.mapService.checkRoutePlannerVisible();
    }

    /** Display or hide the personal exposure calender depending on the research group the user is in. */
    public toggleRatingOn(): boolean {
        return this.mapService.checkRatingsVisible() && this.mapService.checkLoggedIn();
    }
}
