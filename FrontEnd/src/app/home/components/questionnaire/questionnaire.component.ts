/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Import services
import { QuestionnaireService } from 'app/shared/services/questionnaire.service';
import { NavigationService } from '../../../shared/services/navigation.service';
import { Config } from 'app/common/utils';

@Component({
    selector: 'questionnaire-component',
    templateUrl: 'questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class QuestionnaireComponent implements OnInit {

    public isMobile: boolean;
    link = '';
    public questionnaireRetrieved;

    constructor(public navigationService: NavigationService, public router: Router, private questService: QuestionnaireService) { }

    ngOnInit() {
        this.isMobile = Config.IS_MOBILE;
        this.questionnaireRetrieved = false;
        this.questService.getQuestionnaire().then((questionnaire) => {
            this.questionnaireRetrieved = true;
            if (questionnaire != null) {
                this.link = questionnaire.questionnaireString + '?questionnaireid=' + questionnaire.questionnaireId;
            }
        });
    }

    /** Navigate to a specific path. */
    public navigate(path: string) {
        // Navigate to 'path' when closing is finished
        this.router.navigate([path]);
    }
}
