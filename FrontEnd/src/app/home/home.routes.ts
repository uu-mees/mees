/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { Routes } from '@angular/router';

// Import components
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from '../login/components/login/login.component';
import { ForgotPasswordComponent } from '../login/components/forgot-password/forgot-password.component';
import { ExposureHistoryCalendarComponent } from './components/exposure/exposure-history-calendar/exposure-history-calendar.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserProfileSettingsComponent } from './components/user-profile/user-profile-settings/user-profile-settings.component';
import { AuthGuardService } from '../shared/auth-guard.service';
import { LogoutComponent } from '../login/components/logout/logout.component';
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component';
import { MinigamesComponent } from './components/minigames/minigames.component';
import { RegisterComponent } from '../login/components/register/register.component';
import { RoutePlannerComponent } from './components/route/route-planner/route-planner.component';
import { RatingFormComponent } from './components/rating-form/rating-form.component';
import { GraphComponent } from './components/graph/graph.component';

export const HomeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'account-recovery', component: ForgotPasswordComponent },
            { path: 'route-planner', component: RoutePlannerComponent },
            { path: 'rating', component: RatingFormComponent },
            { path: 'graph', component: GraphComponent },
            { path: 'logout', component: LogoutComponent, canActivate: [AuthGuardService] },
            { path: 'settings', component: UserProfileSettingsComponent, canActivate: [AuthGuardService] },
            { path: 'profile', component: UserProfileComponent, canActivate: [AuthGuardService] },
            { path: 'questionnaire', component: QuestionnaireComponent, canActivate: [AuthGuardService] },
            { path: 'exposure', component: ExposureHistoryCalendarComponent, canActivate: [AuthGuardService] },
            { path: 'minigames', component: MinigamesComponent },
            { path: 'map/:param', component: HomeComponent }
        ]
    }
];
