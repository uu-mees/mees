/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Import modules
import { SHARED_MODULES } from './home.common';
import { MaterialModule } from '../shared/material.module';
import { CalendarModule } from 'angular-calendar';

// Import components
import { HomeComponent } from './components/home/home.component';
import { MapComponent } from './components/map/map.component';
import { GraphComponent } from './components/graph/graph.component';
import { RatingFormComponent } from './components/rating-form/rating-form.component';
import { ExposureHistoryCalendarComponent } from './components/exposure/exposure-history-calendar/exposure-history-calendar.component';
import { RouteComponent } from './components/route/route.component';
import { RoutePlannerComponent } from './components/route/route-planner/route-planner.component';
import { RouteSelectionComponent } from './components/route/route-selection/route-selection.component';
import { CurrentExposureComponent } from './components/current-exposure/current-exposure.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserProfileSettingsComponent } from './components/user-profile/user-profile-settings/user-profile-settings.component';
import { LoginComponent } from '../login/components/login/login.component';
import { ForgotPasswordComponent } from '../login/components/forgot-password/forgot-password.component';
import { RegisterComponent } from '../login/components/register/register.component';
import { LogoutComponent } from '../login/components/logout/logout.component';
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component';
import { MinigamesComponent } from './components/minigames/minigames.component';

// Import services
import { NavigationService } from 'app/shared/services/navigation.service';
import { UserService } from '../shared/services/user.service';

// Import pipes
import { ShortDurationPipe, ShortMomentPipe } from '../shared/pipes/moment.pipe';
import { ShortMetersPipe } from '../shared/pipes/distance.pipe';
import { SafePipe } from '../shared/pipes/safe.pipe';

@NgModule({
    imports: [
        ...SHARED_MODULES, HttpModule, CalendarModule.forRoot(), MaterialModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        HomeComponent,
        MapComponent,
        GraphComponent,
        ExposureHistoryCalendarComponent,
        RouteComponent,
        RatingFormComponent,
        RoutePlannerComponent,
        RouteSelectionComponent,
        CurrentExposureComponent,
        UserProfileComponent,
        UserProfileSettingsComponent,
        RatingFormComponent,
        LoginComponent,
        RegisterComponent,
        ForgotPasswordComponent,
        MinigamesComponent,
        LogoutComponent,
        QuestionnaireComponent,
        ShortMomentPipe,
        ShortMetersPipe,
        ShortDurationPipe,
        SafePipe,
    ],
    providers: [
        UserService,
        NavigationService
    ],
})
export class HomeModule {

    constructor(@Optional() @SkipSelf() parentModule: HomeModule) {
        if (parentModule) {
            throw new Error('HomeModule already loaded; Import in root module only.');
        }
    }
}
