/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

import { AppRoutingModule } from './app-routing.module';

import { HomeModule } from './home/home.module';

export const SHARED_MODULES: any[] = [
    AppRoutingModule,
    HomeModule,
];

export * from './app-routing.module';
