/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
export const environment = {
  production: true,
  backEndUrl: 'https://mees.uu.nl/api/',
  minigameUrl:  'https://mees.uu.nl/minigame',
  minigameServerAdress: 'https://mees.uu.nl',
  applicationLink: 'about:blank'
};
