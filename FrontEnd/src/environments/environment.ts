/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  backEndUrl: 'http://localhost:50500/',
  minigameUrl:  'http://localhost:2000/minigamesIndex.html',
  minigameServerAdress: 'http://localhost:2000',
  applicationLink: 'about:blank'
};
