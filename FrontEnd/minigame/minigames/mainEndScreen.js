/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

var textObject;
var textString;
var score;
var canSeeScores;
var height;
var width;
var gameScene;
var gracePeriod;

var buttonValuesEnd =[
    {x: 0.5, y: 0.65, text : "PLAY AGAIN"},
    {x: 0.5, y: 0.85, text : "BACK TO MENU"},
];
var endButtonSize;

class mainEndScreen extends Phaser.Scene{
    //initiate data given by last scene.
    init(data){
        score = data.score;
        misses = data.misses;
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        gameScene = data.sceneKey;
        endButtonSize = gameConfig.menuScreens.buttonSize;

    }

    constructor(){
        super({key:"mainEndScreen"})
    }
    
    //load assets
    preload(){
        this.load.image('button','assets/menu/button.png'); 
    }

    //initate all things in the scene.
    create(){
        //send the results to the front end
        //TODO: see if this is still accurate for all minigames, and check if it works.
        var scoreAndMisses = "score:" + score + " misses:" + misses + " mode:"+ mode+ ""; 

        window.parent.postMessage(scoreAndMisses, '*');

        //get the scene to use the create button method.
        var mainStartScreen = this.scene.get('mainStartScreen');

        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);
        //show the user their score, or if they participate in canSeeScores explain why they cant see it.
        textString = "";
        if(canSeeScores)
            textString += mainStartScreen.stringDivider(eval('`'+gameConfig[mode].endScreenText+'`'),gameConfig.lettersPerLine);
        else
            textString += mainStartScreen.stringDivider(gameConfig.researchText, gameConfig.lettersPerLine);

        //add buttons
        gracePeriod = gameConfig.gracePeriod;
        this.createButtons();

        //create the text object and scale it.
        textObject = this.add.text(0.1*width,0.1*width,textString,{ font: '30px Consolas',fill: "#0000000"});
        var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
        textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);
    }

    update(){
    }

    createButtons()
    {
        var mainStartScreen = this.scene.get('mainStartScreen');

        var playAgainButton = mainStartScreen.createButton(buttonValuesEnd[0].x, buttonValuesEnd[0].y, endButtonSize, this.restart, buttonValuesEnd[0].text, this);
        var backButton = mainStartScreen.createButton(buttonValuesEnd[1].x, buttonValuesEnd[1].y, endButtonSize, this.back, buttonValuesEnd[1].text, this);
        //Immediately make our buttons non-interactive and grayed out, so the user doesn't accidentally click them in the more intensive minigames
        playAgainButton.text.setStyle({color:"#999999"});
        backButton.text.setStyle({color:"#999999"});
        playAgainButton.disableInteractive();
        backButton.disableInteractive();
        
        //after a grace period, reactivate the buttons
        this.time.delayedCall(gracePeriod, this.makeButtonInteractive, [playAgainButton, endButtonSize], this);
        this.time.delayedCall(gracePeriod, this.makeButtonInteractive, [backButton, endButtonSize], this);

    }

    //Makes an end screen button interactive and removes the greyed out tints
    makeButtonInteractive(button, scale)
    {
        button.setInteractive();
        button.text.setStyle({color:"#000000"});
        //button.setDisplaySize(width*scale,0.2*height*scale);
        //button.setSize(width*scale,0.2*height*scale);


     }

    back()
    {
        this.scene.scene.start("mainMenu");
    }
    
    restart(){
        this.scene.scene.start(gameScene);
    }

}

