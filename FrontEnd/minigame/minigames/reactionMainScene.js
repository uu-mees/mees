/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//configureable values
var thresholdMax;
var thresholdMin;
var totalAmountOfGames;
var crossSize;

var numberOfCrosses;
var canSeeScores;
var heigth;
var width;
var textObject;
var textString;
var crossWidth;
var crossHeight;
var currentButton;
var group;
var isButtonDown = false;
var isWaitingForThreshold = true;
var isNotSupposedToPress = false;
var timer; //timer in milliseconds
var threshold;

var releaseScore;
var pressScore;

var currentAmountOfGames;
var sceneKey;

class reactionMainScene extends Phaser.Scene{

    constructor(){
        super({key: "reactionMainScene"})
    }

    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = "reactionMainScene";
        thresholdMin = gameConfig.reactionSpeed.maxTimeTillCross;
        thresholdMax = gameConfig.reactionSpeed.minTimeTillCross;
        totalAmountOfGames = gameConfig.reactionSpeed.totalAmountOfGames;
        crossSize = gameConfig.reactionSpeed.crossSize;
    }
    
    preload(){ //load assets
        this.load.image('reactionCross', 'assets/reaction/cross.png');
        this.load.image('reactionButton', 'assets/reaction/button.png');
        this.load.image('reactionBg','assets/bg.png');
    }

    create (){
        //reset all values before starting the game.
        score = misses = currentNrCrosses = timer = currentAmountOfGames = releaseScore = pressScore = threshold =0;

        numberOfCrosses = gameConfig.reactionSpeed.numberOfCrosses;
        //add background

        var background = this.add.image(width/2,height/2, 'reactionBg');
        background.setTint(gameConfig.colors.backgroundTwo);
        background.key = 'background';
        background.setDisplaySize(width,height);
        background.setInteractive(); //make the background clickable to measure the amount of misses.
        background.on('pointerdown', function(pointer)
        {
            misses+=1;
        });

        //add buttons
        this.addButton(0.5,0.7,0.2, 'reactionButton');
        this.addCrosses(numberOfCrosses, 0.3);

        if(canSeeScores){
            //add a background for the score
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.2*width);
            //add text
            textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
            textObject.setOrigin(0.5,0.5);
        }

        this.setThreshold();
    }
    
    update (time, delta){
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}        
        timer += delta; //add the time since the last update to the timer
        
        //If the user is not pressing the button when they're supposed to, reset the timer
        if(isWaitingForThreshold && !isButtonDown)
        {
            timer = 0;
        }

        textString = "";
        if(canSeeScores){
            textString += 'Score: ' + score + '   Misses: ' + misses + '';  
            textObject.setText(textString); //update score and timeleft, research participants wont see their scores.
            var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);    
        }
        
        //When the timer passes the threshold, 
        if(timer >= threshold && isWaitingForThreshold)
        {
            this.activateCross(); 
        }

        checksum = time;
    }

    //Add a button on the screen

    addButton(x, y, size, sprite){
        var button = this.add.image(x*width, y*height, sprite);
        button.setDisplaySize(size*width, size*width);
        button.setInteractive();

        //When the button is pressed, note the button is down and change the color of the button
        button.on('pointerdown', function(pointer)
        {
            this.setTint(0x00ff00);

            isButtonDown = true;
        });

        //If the user's mouse/finger moves off the button, return it to its base colour and note the button is up
        button.on('pointerout', function (pointer) 
        {
            this.clearTint();
            isButtonDown = false;
        });
    
        //If the user releases the button, return it to its base colour and note the button is no longer down
        button.on('pointerup', function (pointer) 
        {   
            //If the player is supposed to release the button, record the time it took them to release 
            if(!isWaitingForThreshold && !isNotSupposedToPress)
            {
                releaseScore += timer;
                timer = 0;
                isNotSupposedToPress = true;
            }    
            this.clearTint();
            isButtonDown = false;
        });
    }

    addCrosses(amount, crossRowHeight){
        var scaler = (width*crossSize)/50; //since phaser doesnt scale groups absolutely, but only relative, this ugly solution has to be here :(, 50 is the absolute width of the reactionCross.png
        //create group of crosses
        group = this.add.group({
            key:'reactionCross',
            repeat: amount-1,
            setXY:{x:(width/(amount*2)), y:crossRowHeight*height, stepX: width/(amount)},
            setScale: { x: scaler, y: scaler }
        });
        
        //Create an event on every cross that handles being clicked on. 
        //However, crosses are not made interactive yet on purpose
        group.getChildren().forEach(element => {
            element.on('pointerdown', function()
            {
                 this.crossClicked(element);
            }, this);
        });
    }

    //method that is handles an active cross being pressed
    crossClicked(cross)
    {
        //If the player is still holding the button, record a miss and exit
        if(isButtonDown)
            {
                misses += 1;
                return;
            }

        pressScore += timer; //Add the current timer value to the press score
        score = pressScore + releaseScore; //update the score visible to the user
        score = Math.round(score); //Score looks better without 15 decimals :)

        timer = 0;
        cross.disableInteractive(); //Make the cross non-interactive
        cross.clearTint(); //Remove the color from the cross
        this.setThreshold(); 
        isWaitingForThreshold = true;
        isNotSupposedToPress = false;
        currentAmountOfGames += 1;

        //If the total amount of games is played, call game-over
        if(currentAmountOfGames>=totalAmountOfGames) 
        {
            this.gameOver();
        }
    }

    //Method that sets a random cross on the screen to active
    activateCross()
    {
        var cross = Phaser.Math.Between(0,numberOfCrosses -1);
        group.getChildren()[cross].setTint(0x001100);
        group.getChildren()[cross].setInteractive();
        isWaitingForThreshold = false;
        timer = 0;
    }

    //Sets the timer threshold to a random value
    setThreshold()
    {
        threshold = Phaser.Math.Between(thresholdMin,thresholdMax);
    }

    //start mainEndScreen scene and pass all variables needed
    gameOver()
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}