/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//https://www.cambridgecognition.com/cantab/cognitive-tests/executive-function/multitasking-test-mtt/

//configureable values
var gameTime;
var buttonY;
var leftButtonX;
var rightButtonX;
var arrowY;
var buttonSize;
var posDirTextObject;

const posDir = 
{
    LEFT:  'left',
    RIGHT: 'right'
}

const answerType = 
{
    DIRECTIONMODE: 'direction',
    POSITIONMODE: 'position'
}

var numberOfButtons;
var canSeeScores;
var heigth;
var width;
var textObject;
var textString;
var buttonWidth;
var buttonHeight;
var timer; //timer in milliseconds
var checksum;

var sceneKey;

//Buttons and Arrows
var leftButton;
var rightButton;
var pLeftdLeftArrow;
var pLeftdRightArrow;
var pRightdLeftArrow;
var pRightdRightArrow;

var currentArrow; //The active arrow at the current time
var arrowPosition;
var arrowDirection;

var answerT;

class multitaskMainScene extends Phaser.Scene{

    constructor(){
        super({key: "multitaskMainScene"})
    }

    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = "multitaskMainScene";

        gameTime = gameConfig.multiTask.time;
    }
    
    preload(){ //load assets
        this.load.spritesheet('reactionCross', 'assets/reaction/cross.png',{ frameWidth: 50, frameHeight: 50 });
        this.load.image('reactionButton', 'assets/reaction/button.png');
        this.load.image('reactionBg','assets/bg.png');
        this.load.image('arrowLeft','assets/multitask/arrowLeft.png');
        this.load.image('arrowRight','assets/multitask/arrowRight.png');
    }

    create ()
    {
        timer = 0; score = 0; misses = 0;

        checksum = 0;

        buttonY = gameConfig.multiTask.buttonY;
        leftButtonX = gameConfig.multiTask.leftButtonX;
        rightButtonX = gameConfig.multiTask.rightButtonX;
        arrowY = gameConfig.multiTask.arrowY;
        buttonSize = gameConfig.multiTask.buttonSize;

        buttonHeight = buttonWidth = buttonSize*width; //set the size of a button relative to the width of the screen

        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);

        if(canSeeScores){
        //add a background for the score
        var graphics = this.add.graphics();
        graphics.fillStyle(0xFFFFFF,1);
        graphics.fillRect(0,0,width, 0.2*width);
        //add text
        textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
        textObject.setOrigin(0.5,0.5);
        }

        posDirTextObject = this.add.text(0.5*width,0.3*width,'',{ font: '40px Consolas',fill: "#0000000"});
        posDirTextObject.setOrigin(0.5,0.5);

        this.addLeftAndRightButtons();
        this.addArrows();
        this.setNextTarget();
    }
    
    update (time, delta)
    {
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}
        timer += delta; //add the time since the last update to the timer

        if(timer >= gameTime)
        {
            this.gameOver();
        }

        textString = ""; 
        if(canSeeScores){
            textString += 'Score: ' + score + '   Mistakes: ' + misses + '   Time Left:' + (Math.floor((gameTime - timer)/1000));
            textObject.setText(textString); //update score and timeleft, canSeeScores participants wont see their scores.
            var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);   
        }

        posDirTextObject.setText(answerT);
        var scalar = (0.4*width)/posDirTextObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
        posDirTextObject.setDisplaySize(0.4*width, scalar*posDirTextObject.height);

        checksum = time;
    }

    //Pick which arrow is shown on the screen next
    setNextTarget()
    {
        if(currentArrow) {currentArrow.setVisible(false);}

        switch(Phaser.Math.Between(0,1))
        {
            case 0: answerT = answerType.DIRECTIONMODE; break;
            case 1: answerT = answerType.POSITIONMODE; break;
            default: console.error("Random number between 0 and 3 somehow became not a number between 0 and 3"); answerT=answerType.POSITIONMODE; break;
        }
        switch(Phaser.Math.Between(0,3))
        {
            case 0: currentArrow = pLeftdLeftArrow; break;
            case 1: currentArrow = pLeftdRightArrow; break;
            case 2: currentArrow = pRightdRightArrow; break;
            case 3: currentArrow = pRightdLeftArrow; break;
            default: console.error("Random number between 0 and 3 somehow is not a number between 0 and 3"); currentArrow = pLeftdLeftArrow; break;
        }

        currentArrow.setVisible(true);
    }

    //Logic that handles a button being clicked
    handleButtonClick(button)
    {
        var temp
        if(answerT == answerType.POSITIONMODE) 
        {
            temp = currentArrow.pos;
        }
        else 
        {
            temp = currentArrow.dir;
        }

        if(temp == button.pos) {score += 1;}
        else {misses += 1;}

        this.setNextTarget();
    }

    //Add the left and right buttons to the screen
    addLeftAndRightButtons()
    {
        this.addButton(leftButtonX,buttonY, 'reactionButton', posDir.LEFT);
        this.addButton(rightButtonX,buttonY, 'reactionButton', posDir.RIGHT);
    }

    //Add a button to the screen
    addButton(x, y, sprite, position){
        var button = this.add.image(x*width, y*height, sprite);
        
        button.setDisplaySize(buttonWidth, buttonHeight);
        button.setInteractive();

        button.pos = position;

        //When the button is pressed, change the color of the button
        button.on('pointerdown', function(pointer)
        {
            button.setTint(0x00ff00);
        },this);

        //If the user's mouse/finger moves off the button, return it to its base colour
        button.on('pointerout', function (pointer) 
        {
            this.clearTint();
        });
    
        //If the user releases the button, return it to its base colour and call handle button click
        button.on('pointerup', function (pointer) 
        {
            button.clearTint();
            this.handleButtonClick(button);
        },this);
    }

    //Adds the arrow objects to the scene
    addArrows()
    {
        pLeftdLeftArrow = this.addArrow(leftButtonX,arrowY,'arrowLeft', posDir.LEFT, posDir.LEFT); 
        pLeftdRightArrow = this.addArrow(leftButtonX,arrowY,'arrowRight', posDir.LEFT, posDir.RIGHT);
        pRightdLeftArrow = this.addArrow(rightButtonX,arrowY,'arrowLeft', posDir.RIGHT, posDir.LEFT);
        pRightdRightArrow = this.addArrow(rightButtonX,arrowY,'arrowRight', posDir.RIGHT, posDir.RIGHT);
    }

    //Adds an arrow to the scene and makes it invisible
    addArrow(x,y,sprite,position,direction)
    {
        var arrow = this.add.image(x*width,y*height,sprite);
        
        arrow.setDisplaySize(buttonWidth/2,buttonHeight/2)

        arrow.pos = position;
        arrow.dir = direction;

        arrow.setVisible(false);

        return arrow;
    }

    //start mainEndScreen scene and pass all variables needed
    gameOver()
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}