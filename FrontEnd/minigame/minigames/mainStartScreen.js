/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

var textObject;
var textString;

var score;
var canSeeScores;
var height;
var width;
var sceneKey;
var camera;
var icon;

var buttonValuesStart =[
    {x: 0.5, y: 0.4, text : "   START   "},
    {x: 0.5, y: 0.6, text : "HOW TO PLAY"},
    {x: 0.5, y: 0.8, text : "   BACK    "}
];
var startMenuButtonSize;
var startMenuButtonOffSet;

class mainStartScreen extends Phaser.Scene{
    //initiate data given by last scene.
    init(data){
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = data.sceneKey;
        icon = data.icon;
    }

    constructor(){
        super({key:"mainStartScreen"})
    }
    
    //load assets
    preload(){
        this.load.image('button','assets/menu/button.png'); 
    }

    //initate all things in the scene.
    create(){
        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);

        startMenuButtonSize = gameConfig.menuScreens.buttonSize;
        startMenuButtonOffSet = gameConfig.menuScreens.buttonOffset;
        var iconSize = gameConfig.menuScreens.iconSize;

        //add the icon of the minigame above the buttons
        var gameIcon = this.add.image(0.5*width,0.2*height,icon);

        gameIcon.setSize(iconSize*width,iconSize*width);
        gameIcon.setDisplaySize(iconSize*width,iconSize*width);       

        //add buttons
        this.createButton(buttonValuesStart[0].x,buttonValuesStart[0].y,startMenuButtonSize,this.start,      buttonValuesStart[0].text, this);
        this.createButton(buttonValuesStart[1].x,buttonValuesStart[1].y,startMenuButtonSize,this.tutorial,   buttonValuesStart[1].text, this);
        this.createButton(buttonValuesStart[2].x,buttonValuesStart[2].y,startMenuButtonSize,this.back,       buttonValuesStart[2].text, this);
    }

    update(){
    }

    //creates a button on relative x,y with text text, calling function dest, in scene scene (just use this when calling it).
    //creates a button on x/1 and y/1 part of the screen, so x = 0.5 will result in the button being centered, 0.1 will have it on the left, and 0.9 on the right ect.
    //this code is currently only used in other menus, such as the start and end screen.
    createButton(x,y, scale, dest, text, scene){
        //add a button and its edge (the part that doesnt move when pressing button).
        var buttonEdge = scene.add.image(x*width,y*height,'buttonBgEdge2');
        var button = scene.add.image(x*width,y*height,'buttonBg2').setInteractive();
        var buttonText = scene.add.text(x*width,y*height, text, { font: '60px Consolas', fill: '#000000', align: "center" })//add text in button
        buttonText.setOrigin(0.5,0.5);
        //set the size of the button, its edge and text relative to the screen

        button.setDisplaySize(width*scale,0.2*height*scale);
        button.setSize(width*scale,0.2*height*scale);   

        buttonEdge.setDisplaySize(width*scale,0.2*height*scale);         
        buttonEdge.setSize(width*scale,0.2*height*scale);

        buttonText.setDisplaySize(0.8*width*scale, 0.1*height*scale);
        buttonText.setSize(0.8*width*scale, 0.1*height*scale);
        

        //save the original location, so the button can "click back" into its original position.
        button.originalX = x*width;
        button.originalY = y*height;

        //set the color of the button and its edge
        buttonEdge.setTint(gameConfig.colors.foregroundTwo);
        button.setTint(gameConfig.colors.foregroundOne);

        var offset = startMenuButtonOffSet*width;
        buttonEdge.x += offset;
        buttonEdge.y += offset;

        button.on("pointerup", dest);                              //set the destination of the button

        //offset the button if it is pressed, and reset the button when the button is released, or the mouse moved out of the button
        button.on('pointerdown', function()
        {
            button.x = button.originalX+offset;
            button.y = button.originalY+offset;
            buttonText.x = button.originalX+offset;
            buttonText.y = button.originalY+offset;
        },this);
        button.on('pointerup', function()
        {
            button.x = button.originalX;
            button.y = button.originalY;
            buttonText.x = button.originalX;
            buttonText.y = button.originalY;
        },this);
        button.on('pointerout', function()
        {
            button.x = button.originalX;
            button.y = button.originalY;
            buttonText.x = button.originalX;
            buttonText.y = button.originalY;
        },this);

        button.text = buttonText;
        return button;
    }

    back()
    {
        this.scene.scene.start("mainMenu");
    }

    tutorial()
    {
        this.scene.scene.start("mainTutorialScreen", {canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }

    start(){
        this.scene.scene.start(sceneKey, {canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
    
    //adds newline in strings for wordwrap, because (recurring theme) phasers method is &*%$
    stringDivider(str, width) {
        if (str.length>width) {
            var p=width
            for (;p>0 && str[p]!=' ';p--) {
            }
            if (p>0) {
                var left = str.substring(0, p);
                var right = str.substring(p+1);
                return left +'\n' +this.stringDivider(right, width);
            }
        }
        return str;
    }
}
