/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//configurable values
var totalTime;
var maxNrCrosses;  
var crossSize;

//values to be reset when starting game
var score;
var misses;
var currentNrCrosses;
var backbuttonBottom = 0;
var crosswidth;
var crossheight;

//objects
var textObject;
var textString;
var stopTimer;

//data from previous scene
var canSeeScores;
var heigth;
var width;
var textheight;
var mode;
var sceneKey;


class motorMainScene extends Phaser.Scene{

    constructor(){
        super({key: "motorMainScene"})
    }
    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = data.sceneKey;
        if(mode == "motorSpeedMultiple"){
            totalTime = gameConfig.motorSpeedMultiple.time;
            maxNrCrosses = gameConfig.motorSpeedMultiple.nrOfCrosses;
        }
        else{
            totalTime = gameConfig.motorSpeedSingle.time;
            maxNrCrosses = gameConfig.motorSpeedSingle.nrOfCrosses;
        }
    }

    preload(){ //load assets
        this.load.image('cross', 'assets/motor/cross.png');
        this.load.image('bgMain','assets/bg.png');
    }

    create (){

        backbuttonBottom = 0;
        var crossSize = gameConfig[mode].crossSize;
        crossheight = crosswidth = crossSize*width; //set the size of a cross relative to the width of the screen

        //add background
        var background = this.add.image(width/2,height/2, 'bgMain');
        background.setTint(gameConfig.colors.backgroundTwo);
        background.key = 'background';
        background.setDisplaySize(width,height);
        background.setInteractive(); //make the background clickable to measure the amount of misses.
        background.on('pointerdown',function(pointer){this.clickHandler(pointer, background)}, this);



        //add a background for the score
        if(canSeeScores){
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.2*width);
            textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
            textObject.setOrigin(0.5,0.5);
        }

        //reset all values before starting the game.
        score = misses = currentNrCrosses = 0;
        stopTimer = this.time.delayedCall(totalTime, this.gameOver, [], this); //if time is over initate gameover to go to end scene

    } 

    update (time){
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}

        textString = "";
        if(canSeeScores){
            textString += 'Score: ' + score + '   ';
            textString += 'Time left: '+ Math.floor((totalTime - stopTimer.getElapsed())/1000);
            textObject.setText(textString); //update score and timeleft, canSeeScores participants wont see their scores.
            var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textObject.setDisplaySize(0.81*width, scaleFactor*textObject.height);
            backbuttonBottom = scaleFactor*textObject.height+width*0.15;
    
        }



        if(currentNrCrosses < maxNrCrosses) //add a cross if the previous one is clicked.
        {
            for(var i = 0;i<maxNrCrosses-currentNrCrosses;i++){
                this.addNewCross();
                currentNrCrosses += 1;
            }
        }

        checksum = time;
    }

    addNewCross(){ //adds a clickable cross on a random screen location.
        var x = Phaser.Math.Between(0+0.5*crosswidth, width-0.5*crosswidth);
        var y = Phaser.Math.Between(backbuttonBottom + 0.5*crossheight, height - 30 - 0.5*crossheight);
        var cross = this.add.image(x,y,'cross');
        cross.setDisplaySize(crosswidth,crossheight);
        cross.setInteractive();
        cross.on('pointerdown', function(pointer){
            this.clickHandler(pointer, cross);
        }, this);

    }

    clickHandler (pointer, gameobject) //if a cross is clicked, set invisible and unclickable
    {
        //count miss if the user clicks the background
        if(gameobject.key == 'background')
        {
            misses += 1;
        }
        else //if the user clicks a cross, add 1 to the score and remove the cross
        {
            score = score+1;
            gameobject.input.enabled = false;
            gameobject.setVisible(false);
            currentNrCrosses -= 1; //make sure a new cross is added in update method.
        }
    }

    back()
    {
        this.scene.scene.start("mainMenu");
    }

    gameOver() //start mainEndScreen scene and pass all variables needed
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}