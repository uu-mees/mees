/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

var textObject;
var textString;
var score;
var canSeeScores;
var height;
var width;
var sceneKey;
var camera;

var buttonValuesTutorial =[
    {x: 0.5, y: 0.85, text : "   BACK    "}
];

class mainTutorialScreen extends Phaser.Scene{
    //initiate data given by last scene.
    init(data){
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = data.sceneKey;
    }

    constructor(){
        super({key:"mainTutorialScreen"})
    }
    
    //load assets
    preload(){
        this.load.image('button','assets/menu/button.png'); 
    }

    //initate all things in the scene.
    create(){
        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);

        var mainStartScreen = this.scene.get('mainStartScreen');
        //add buttons
        var buttonSize = gameConfig.menuScreens.buttonSize;
        mainStartScreen.createButton(buttonValuesTutorial[0].x, buttonValuesTutorial[0].y, buttonSize, this.back, buttonValuesTutorial[0].text, this);
  

        //get the game explaination from the config, and format it
        textString = mainStartScreen.stringDivider(eval('`'+gameConfig[mode].startScreenText+'`'), gameConfig.lettersPerLine);
        //explain why the user cant see scores (if the users can see it)
        if(!canSeeScores){ 
            textString+="\n\n" + mainStartScreen.stringDivider(gameConfig.researchText, gameConfig.lettersPerLine);
        }

        //create the text object and scale it.
        textObject = this.add.text(0.1*width,0.1*width,textString, { font: '30px Consolas',fill: "#0000000"});
        var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
        textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);
    }

    update(){
    }

    back()
    {
        this.scene.scene.start("mainStartScreen");
    }
}
