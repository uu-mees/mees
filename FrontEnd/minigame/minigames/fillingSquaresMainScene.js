/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//configureable values
var maxSquareSize;
var minSquareSize;
var totalAmountOfMisses;
var squareFillSpeed;
var squareSize;
var scoreModifier;
var amountOfRows;
var amountOfColumns;

var minimumTimeBetweenSquaresBase;
var minimumTimeBetweenSquaresRange;
var standardTimeBetweenSquaresBase;
var standardTimeBetweenSquaresRange;
var timeModifier;

var canSeeScores;
var heigth;
var width;
var sceneKey;

var totalAmountOfSquares;

var textObject;
var textString;
var squareWidth;
var squareHeight;

var squares;
var backSquares;
var timer;

class fillingSquaresMainScene extends Phaser.Scene{

    constructor(){
        super({key: "fillingSquaresMainScene"})
    }

    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = "fillingSquaresMainScene";
        totalAmountOfMisses = gameConfig.fillingSquares.totalAmountOfMisses;
        maxSquareSize = gameConfig.fillingSquares.maxSquareSize;
        minSquareSize = gameConfig.fillingSquares.minSquareSize;
        squareFillSpeed = gameConfig.fillingSquares.squareFillSpeed;
        scoreModifier = gameConfig.fillingSquares.scoreModifier;
        amountOfRows = gameConfig.fillingSquares.amountOfRows;
        amountOfColumns = gameConfig.fillingSquares.amountOfColumns;

        totalAmountOfSquares = amountOfColumns * amountOfRows;

        minimumTimeBetweenSquaresBase = gameConfig.fillingSquares.minimumTimeBetweenSquaresBase;
        minimumTimeBetweenSquaresRange = gameConfig.fillingSquares.minimumTimeBetweenSquaresRange;
        standardTimeBetweenSquaresBase = gameConfig.fillingSquares.standardTimeBetweenSquaresBase;
        standardTimeBetweenSquaresRange = gameConfig.fillingSquares.standardTimeBetweenSquaresRange;
        timeModifier = gameConfig.fillingSquares.timeModifier; 
    }
    
    preload(){ //load assets
        this.load.image('squareSquare','assets/spatialSpan/spatialButton.png')
        this.load.image('bgMain','assets/bg.png');
    }

    create (){
        squares = [];
        backSquares = [];
        squareSize = gameConfig.fillingSquares.squareSize;
        squareHeight = squareWidth = squareSize*width; //set the size of a square relative to the width of the screen
        //reset all values before starting the game.
        score = misses = timer = 0;
        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);

        if(canSeeScores){
            //add a background for the score
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.2*width);
            //add text
            textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
            textObject.setOrigin(0.5,0.5);
        }


        this.fillSquareArray('squareSquare');
        this.newSquare();
    }
    
    update (time, delta)
    {
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}        
        timer += delta; //add the time since the last update to the timer


        if(canSeeScores){
            textString = "";
            textString += 'Score: ' + score + '   Misses: ' + misses;  
            textObject.setText(textString); //update score and timeleft, canSeeScores participants wont see their scores.
            var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textheight = scaleFactor*textObject.height+textObject.y; //this wouldnt be here if phaser had it's shit together.
            textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);
        }
        this.updateSquares(delta);

        checksum = time;
    }

    //Method that activates a new square, then calls itself with a delay based on how long the game has been going
    newSquare()
    {
        var i = Phaser.Math.Between(0,totalAmountOfSquares-1);
        var timeTillNext = Math.max(
            Phaser.Math.Between(minimumTimeBetweenSquaresBase,minimumTimeBetweenSquaresBase + minimumTimeBetweenSquaresRange), 
            Phaser.Math.Between(standardTimeBetweenSquaresBase-timer/timeModifier,standardTimeBetweenSquaresBase+standardTimeBetweenSquaresRange-(timer/timeModifier))
            );

        this.time.delayedCall(timeTillNext,this.newSquare,[],this);

        this.activateSquare(i);
    }

    //Add a new square to the square and backsquare array
    addSquare(x, y, sprite)
    {
        x*= width;
        y*=height;
        var i = squares.length;

        //Create the background squaree
        var backSquare = this.add.image(x,y,sprite);
        backSquare.setDisplaySize(squareWidth, squareHeight);
        backSquare.index = i;
        backSquare.setVisible(false);
        backSquares[i] = backSquare;

        //Create the square itself
        var square = this.add.image(x,y,sprite);

        square.index = i;
        square.setVisible(false);

        square.on('pointerdown', function()
        {
            this.deactivateSquare(square.index);
        },this);

        squares[i] = square;
    }
    
    //Makes one of the squares visible and interactive
    activateSquare(i)
    {
        if(squares[i].visible) {return;} //If the square is already active, just return

        var square = squares[i];
        var backSquare = backSquares[i];

        square.setTint(0x00ff00);
        
        square.setDisplaySize(squareWidth*minSquareSize,squareHeight*minSquareSize);
        square.currentSize = minSquareSize;

        backSquare.setVisible(true);
        square.setVisible(true);
        square.setInteractive();
    }

    //Deactivate a square and record a score or miss increase
    deactivateSquare(i)
    {
        if(!squares[i].visible) {console.error('attempted to deactivate a non-active square'); return;}

        var square = squares[i];
        var backSquare = backSquares[i];

        //Calculate a score increase based on how close the current size of the square is to the size of the backsquare
        var tempScore = 1.0-(Math.abs(1.0-(square.currentSize))*scoreModifier);

        if(tempScore <= 0 || square.currentSize>=maxSquareSize) {misses+=1;} //If the square is too big or too small, record a miss
        else { score += Math.ceil(tempScore*100); } //Otherwise increase the score by 100 * the temp score

        //Actually deactivate the square
        square.disableInteractive();
        square.clearTint();
        square.setVisible(false);
        backSquare.setVisible(false);

        //If the player has missed a certain number of times, the game is over
        if(misses>=totalAmountOfMisses) { this.gameOver(); }
    }

    fillSquareArray(sprite)
    {
        var positions =  this.scene.get('mainMenu').alignGrid(0.5,0.3,0.7,amountOfColumns,totalAmountOfSquares)

        for(var i = 0; i < totalAmountOfSquares; i++)
        {
            this.addSquare(positions[i].x/width,positions[i].y/height,sprite);
            
        }
    }


    //Function that scales up every visible square and removes them when too big
    updateSquares(delta)
    {
        for(var i = 0; i<totalAmountOfSquares; i++)
        {
            if(!squares[i].visible) {continue;} //Skip inactive Squares

            var square = squares[i];

            square.currentSize += delta*squareFillSpeed; //Increase the size of the current Square
            square.setDisplaySize(square.currentSize*squareWidth,square.currentSize*squareHeight);

            if(square.currentSize>=1.0)
            {
                square.setTint(0xff0000); //If the square has grown over the ideal size, colour it red to indicate that the square is about to disappear
                
                //If the square is too big, deactivate it (it will always record a miss in this case)
                if(square.currentSize >maxSquareSize)
                {
                    this.deactivateSquare(square.index);
                }
            }
        }
    }

    //start mainEndScreen scene and pass all variables needed
    gameOver()
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}