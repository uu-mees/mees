/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//https://www.cambridgecognition.com/cantab/cognitive-tests/executive-function/spatial-span-ssp/

//configureable values
const spatialGameMode = 
{
    REGULAR: 'regular',
    REVERSE: 'reverse'
}

const buttonColours = [0x0000ff, 0x00ff00, 0xff0000, 0xff00ff, 0xffff00, 0x00ffff, 0x000088, 0x008800, 0x880000];

var numberOfButtons;
var canSeeScores;
var heigth;
var width;
var textObject;
var textString;
var buttonWidth;
var buttonHeight;
var timer; //timer in milliseconds
var interacting;
var sequence;
var currentIndex;
var timer;
var threshold = 1000;
var timeBetweenColours = 100;
var currentSpatialGameMode;

var sceneKey;

//Buttons
var buttons;

class spatialSpanMainScene extends Phaser.Scene{

    constructor(){
        super({key: "spatialSpanMainScene"})
    }

    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;

        switch(mode)
        {
            case 'spatialSpan': currentSpatialGameMode = spatialGameMode.REGULAR; break;
            case 'spatialSpanReverse': currentSpatialGameMode = spatialGameMode.REVERSE; break;
            default: console.error('Spatial Span minigame initiallized with non-existing gamemode. Defaulted to regular.'); currentSpatialGameMode = spatialGameMode.REGULAR; break;
        }

        sceneKey = "spatialSpanMainScene";
    }
    
    preload(){ //load assets
        this.load.image('spatialButton', 'assets/spatialSpan/spatialButton.png');
        this.load.image('bgMain','assets/bg.png');
    }

    create ()
    {
        score = 0; misses = 0; interacting = false; currentIndex = 0; buttons = []; sequence = []; timer = 0;
        buttonHeight = buttonWidth = 0.2*width; //set the size of a cross relative to the width of the screen
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);

        if(canSeeScores){
            //add a background for the score
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.2*width);
            //add text
            textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
            textObject.setOrigin(0.5,0.5);
        }

        this.fillButtonArray('spatialButton');
        this.generateSequence();
    }
    
    update (time, delta)
    {
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}
        timer += delta;


        if(canSeeScores){
            textString = 'Score: ' + score;  
            textObject.setText(textString); //update score and timeleft, canSeeScores participants wont see their scores.
            var scaleFactor = (0.4*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textheight = scaleFactor*textObject.height+textObject.y; //this wouldnt be here if phaser had it's shit together.
            textObject.setDisplaySize(0.4*width, scaleFactor*textObject.height);
        }
        //If we're not currently waiting for user input, call the showSequence method.
        if(!interacting)
        {
            this.showSequence();
        }

        checksum = time;
    }

    //Switching from non-interactive to interactive state
    switchToInteractive()
    {
        interacting = true;
        currentIndex = 0;
        timer = 0;
    }

    //Switching from interactive to non-interactive state
    switchToNonInteractive()
    {
        interacting = false;
        currentIndex = 0;
        timer = 0;
        this.generateSequence();
    }

    //Generate a button sequence, and light up the first button
    generateSequence()
    {
        for(var i = 0; i<= score; i++)
        {
            sequence[i] = Phaser.Math.Between(0,8);
        }

        if(currentSpatialGameMode == spatialGameMode.REGULAR)
        { currentIndex = 0; }
        else
        { currentIndex = sequence.length - 1; }

        timer = 0;
        this.time.delayedCall(timeBetweenColours, this.activateButton, [currentIndex], this);
    }

    //Code that handles lighting up the correct buttons
    showSequence()
    {
        if(timer>threshold)
        {
            timer = 0;
            buttons[sequence[currentIndex]].clearTint(); //clear the currently lit button
            
            if(currentSpatialGameMode == spatialGameMode.REGULAR)
            {currentIndex++;}
            else
            {currentIndex--;}

            //If we're at the end of the sequence, switch to interactive mode
            if(currentIndex >= sequence.length || currentIndex < 0)
            {
                this.switchToInteractive();
                return;
            }
            
            //Call that lights up the next button. Delayed so the user can see repetitions in the sequence
            this.time.delayedCall(timeBetweenColours, this.activateButton, [currentIndex], this);
        }
        
    }

    //Method that changes the colour of a button to its assigned colour
    activateButton(i)
    {
        var b = buttons[sequence[i]];
        b.setTint(b.flashCol);
        timer = 0;
    }

    //Logic that handles a button being clicked
    handleButtonClick(button)
    {
        //If the user presses the wrong button, it's game over
        if(button.ind != sequence[currentIndex])
        {
            this.gameOver();
            return;
        }

        //Otherwise, put the index up by one
        //if the entire sequence was completed correctly, add one to the score and switch to non-interactive
        currentIndex++;
        if(currentIndex>=sequence.length)
        {
            score += 1;
            this.switchToNonInteractive();
        }
    }

    //Init method that fills the button array
    fillButtonArray(sprite)
    {
        for(var i = 0; i<9; i++)
        {
            var x = Math.floor(i/3);
            var y = i%3;

            x = ((x*0.25) + 0.25);
            y = ((y*0.20) + 0.40);

            var col = buttonColours[i];
            buttons[i] = this.addButton(x,y,sprite,col, i);
        }
    }

    //Add a button to the screen
    addButton(x, y, sprite, flashColour, i){
        var button = this.add.image(x*width, y*height, sprite);
        
        button.setDisplaySize(buttonWidth, buttonHeight);
        button.setInteractive();
        button.flashCol = flashColour;
        button.ind = i;

        //When the button is pressed, change the color of the button
        button.on('pointerdown', function(pointer)
        {
            if(interacting) {button.setTint(flashColour);}
        },this);

        //If the user's mouse/finger moves off the button, return it to its base colour
        button.on('pointerout', function (pointer) 
        {
            if(interacting){ this.clearTint();}
        });
    
        //If the user releases the button, return it to its base colour and call handle button click
        button.on('pointerup', function (pointer) 
        {
            if(interacting)
            {
                button.clearTint();
                this.handleButtonClick(button);
            }
        },this);
        return button;
    }

    //start mainEndScreen scene and pass all variables needed
    gameOver()
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}