/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * �Copyright Utrecht University(Department of Information and Computing Sciences) */

var welcomeTextObject;
var welcomeTextString;
var canSeeScores;
var height;
var width;
var gameConfig;

//values for the grid
var gridXcenter = 0.5;
var gridYtop    = 0.15;
var gridWidth   = 0.6
var iconsPerRow = 3;
var gridSize    = 9;

var buttonSize;
var labelSize;
var buttonOffset; //how much the button moves when pressed.


var buttonValues = [
    {mode:'motorSpeedSingle',       sceneKey:'motorMainScene',          label: ' Motor 1 ',         image: 'motorSpeedSingleMenuButton'  },
    {mode:'motorSpeedMultiple',     sceneKey:'motorMainScene',          label: ' Motor 5 ',         image: 'motorSpeedMultipleMenuButton'},
    {mode:'reactionSpeed',          sceneKey:'reactionMainScene',       label: 'Reaction ',         image: 'reactionMenuButton'          },
    {mode:'rapidVisualProcessing',  sceneKey:'rapidMainScene',          label: ' Visual  ',         image: 'rapidProcessingMenuButton'   },
    {mode:'multiTask',              sceneKey:'multitaskMainScene',      label: 'Multitask',         image: 'multitaskMenuButton'         },
    {mode:'spatialSpan',            sceneKey:'spatialSpanMainScene',    label: ' Spatial ',         image: 'spatialSpanMenuButton'       },
    {mode:'spatialSpanReverse',     sceneKey:'spatialSpanMainScene',    label: 'Spatial 2',         image: 'spatialSpanReverseMenuButton'},
    {mode:'fillingSquares',         sceneKey:'fillingSquaresMainScene', label: ' Squares ',         image: 'fillingSquaresMenuButton'},
    {mode:'additionGame',           sceneKey:'additionMainScene',       label: 'Addition ',         image: 'additionMenuButton'},
]
//first scene opened by the game, also responsible for reading the URL.
//this scene represents the menu.

class mainMenu extends Phaser.Scene{

    constructor(){
        super({key:"mainMenu"});
    }

    //load needed files
    preload(){
        this.load.json('gameConfig', 'gameConfig.json');
        this.load.image('mainMenuBg','assets/menu/mainMenuBg.png');
        this.load.image('buttonBgEdge2','assets/menu/menuButtonEdge2.png'); 
        this.load.image('buttonBg2','assets/menu/menuButtonBg2.png'); 
        this.load.image('motorSpeedMultipleMenuButton','assets/menu/motorSpeedMultipleMenuButton.png');
        this.load.image('motorSpeedSingleMenuButton','assets/menu/motorSpeedSingleMenuButton.png');
        this.load.image('reactionMenuButton','assets/menu/reactionMenuButton.png');
        this.load.image('rapidProcessingMenuButton','assets/menu/rapidProcessingMenuButton.png');
        this.load.image('spatialSpanMenuButton','assets/menu/spatialSpanMenuButton.png');
        this.load.image('multitaskMenuButton','assets/menu/multitaskMenuButton.png');
        this.load.image('spatialSpanReverseMenuButton','assets/menu/spatialSpanReverseMenuButton.png');
        this.load.image('fillingSquaresMenuButton','assets/menu/fillingSquaresMenuButton.png');
        this.load.image('additionMenuButton','assets/menu/additionMenuButton.png');
        this.load.image('buttonBg','assets/menu/menuButtonTemplateInner.png');
        this.load.image('buttonBgEdge','assets/menu/menuButtonTemplate.png');
    }

    //initate all things needs in this scene.
    create(){

        //get the config file
        gameConfig = this.cache.json.get('gameConfig');
        //get all variables from url
        canSeeScores = this.getQueryVariable("canSeeScores");

        //save screen height and width
        width = this.sys.game.canvas.width;
        height = this.sys.game.canvas.height;
        buttonSize = gameConfig.mainMenu.buttonSize;
        labelSize = gameConfig.mainMenu.labelSize;
        buttonOffset =gameConfig.mainMenu.buttonOffset;

        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundOne);

        //add all the icons
        this.createGrid();
    }

    update(){
    }

    startGame(mode, sceneKey){
        this.scene.scene.scene.start('mainStartScreen', { canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }


    createGrid(){
        var keys = [];
        for(var i in buttonValues){
            keys[i]=buttonValues[i].image;//add all the button images to a list of keys
        }

        //create a group for the icons, for their edges and backgrounds with those keys
        var buttonBgEdgesRaster = this.add.group({key: 'buttonBgEdge', repeat: keys.length-1 });
        var buttonBgsRaster = this.add.group({key: 'buttonBg', repeat: keys.length-1 });
        var buttonRaster = this.add.group({key: keys});

        //get all the gameobjects from those groups
        var buttons = buttonRaster.getChildren();
        var buttonBgs = buttonBgsRaster.getChildren();
        var buttonBgEdges = buttonBgEdgesRaster.getChildren();

        //generate the positions for the buttons
        var positions = this.alignGrid(gridXcenter, gridYtop, gridWidth, iconsPerRow, gridSize);

        //get the colors for the buttons
        var bgColor = gameConfig.colors.foregroundOne
        var edgeColor = gameConfig.colors.foregroundTwo

        //check if te icons should be different colors
        var differentColorIcons = gameConfig.mainMenu.differentColorIcons;
        //enable the buttons and set their size and position and data such as the scene they refer to and the mode.
        for(let i=0;i<buttons.length;i++){
            if(differentColorIcons)
                this.configureMenuButton(buttons[i],buttonBgs[i], buttonBgEdges[i], buttonValues[i], positions[i].x, positions[i].y, gameConfig.mainMenu.iconColorPalette[i], edgeColor);
            else                
                this.configureMenuButton(buttons[i],buttonBgs[i], buttonBgEdges[i], buttonValues[i], positions[i].x, positions[i].y, bgColor, edgeColor);
        }
    }

    //gets a button from a group, 
        configureMenuButton(button, buttonBg, buttonBgEdge, buttonValue, x, y, bgColor, edgeColor){
            button.setInteractive();
            button.setDataEnabled();

            //scale the buttons and their backgrounds and edges
            button.setSize(buttonSize*width,buttonSize*width);         //set size for the button icons
            button.setDisplaySize(buttonSize*width,buttonSize*width);
            buttonBgEdge.setSize(buttonSize*width,buttonSize*width);   //their background edges
            buttonBgEdge.setDisplaySize(buttonSize*width,buttonSize*width);  
            buttonBg.setSize(buttonSize*width,buttonSize*width);       //and their backgrounds
            buttonBg.setDisplaySize(buttonSize*width,buttonSize*width);     
            
            button.x = buttonBg.x = x;    //set the buttons(and their backgrounds) position.
            button.y = buttonBg.y = y;
    
            var offset = buttonOffset*width; 
            //offset the edges to create a 3d effect.
            buttonBgEdge.x = x + offset;
            buttonBgEdge.y = y + offset;
    
            //link the data(the scene mode, image) ect to the specific button
            button.data.set('button',buttonValue);     
    
            //offset the button if it is pressed, and reset the button when the button is released, or the mouse moved out of the button
            button.on('pointerdown', function()
            {
                button.x = buttonBg.x = x+offset;
                button.y = buttonBg.y = y+offset;
            },this);
            button.on('pointerup', function()
            {
                button.x = buttonBg.x = x;
                button.y = buttonBg.y = y;
                this.buttonHandler(button);
            },this);
            button.on('pointerout', function()
            {
                button.x = buttonBg.x = x;
                button.y = buttonBg.y = y;
            },this);
    
            //set the color of the buttons, if differentcoloricons, then apply the color palette from the config
            buttonBg.setTint(bgColor);
            buttonBgEdge.setTint(edgeColor);
    
            //adds text under the buttons if desired.
            if(gameConfig.mainMenu.displayLabels){
                var label = this.add.text(0,0,buttonValue.label,{font: '20px Consolas',fill: "#0000000"});
                var scalar = (labelSize*width)/label.width;
                label.setDisplaySize(labelSize*width, scalar*label.height);
                label.setSize(labelSize*width, scalar*label.height);
                Phaser.Display.Align.To.BottomCenter(label, button);
            }
        }


    //handles the clicks on the buttons, starts a scene with a specific mode.
    buttonHandler(gameobject){
        var mode = gameobject.data.values.button.mode;
        var sceneKey = gameobject.data.values.button.sceneKey;  
        var icon = gameobject.data.values.button.image;  
        this.scene.scene.scene.start('mainStartScreen', { canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey, icon: icon});
    }

    //own made method to replace phasers gridAlign (because it is terrible)
    //gets a relative x and y, and totalwidth, realative to the total width of the screen
    //the x,y pos is the position of the top center cell.
    //returns a array of absolute x y pairs
    alignGrid(x,y,totalWidth,columns,amountOfCells){
        var x = x-(0.5*totalWidth);
        var columnWidth = totalWidth/(columns-1);
        var rowHeight = columnWidth;
        var rows = Math.ceil(amountOfCells/columns);
        var positions = [];
        for(let j = 0;j<rows;j++){
            for(let i = 0; i<columns;i++){
                positions.push({x: (x+(i*columnWidth))*width, y: ((y*height)+(j*rowHeight*width))});
            }
        }
        return positions;
    }


    getQueryVariable(variable) //gets information from the URL the game is started with.
    {
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return decodeURI(pair[1]);}
       }
       return(false);
    }

}

