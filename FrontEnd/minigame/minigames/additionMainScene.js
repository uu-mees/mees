/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//configureable values
var highestNumber;
var gameTime;
var wrongAnswerOneRange;
var wrongAnswerTwoRange;
var wrongAnswerThreeRange;

var wrongAnswerOneInTens;
var wrongAnswerTwoInTens;
var wrongAnswerThreeInTens;

var canSeeScores;
var height;
var width;
var sceneKey;

var textObject;
var textString;
var buttonWidth;
var buttonHeight;
var timer; //timer in milliseconds

var answerBoxes;
var additionBox;

//Buttons
var buttons;

class additionMainScene extends Phaser.Scene{

    constructor(){
        super({key: "additionMainScene"})
    }

    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        highestNumber = gameConfig.additionGame.highestNumber;
        gameTime = gameConfig.additionGame.gameTime;

        sceneKey = "additionMainScene";
    }
    
    preload(){ //load assets
        this.load.image('numberBox','assets/rapid/numberBox.png');
    }

    create()
    {
        score = 0; misses = 0; answerBoxes = []; timer = 0;
        if(highestNumber <=0) {console.error('Highest number cannot be 0 or smaller'); gameOver();}

        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);
        wrongAnswerOneRange = gameConfig.additionGame.wrongAnswerOneRange;
        wrongAnswerTwoRange = gameConfig.additionGame.wrongAnswerTwoRange;
        wrongAnswerThreeRange = gameConfig.additionGame.wrongAnswerThreeRange;

        wrongAnswerOneInTens = gameConfig.additionGame.wrongAnswerOneInTens;
        wrongAnswerTwoInTens = gameConfig.additionGame.wrongAnswerTwoInTens;
        wrongAnswerThreeInTens = gameConfig.additionGame.wrongAnswerThreeInTens;


        //add a background for the score
        if(canSeeScores){
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.2*width);
            textObject = this.add.text(0.5*width,0.1*width,'',{ font: '40px Consolas',fill: "#0000000"});
            textObject.setOrigin(0.5,0.5);
        }


        this.createAnswerBoxes('numberBox');
        this.createAdditionBox('numberBox');

        this.nextQuestion();
    }
    
    update (time, delta)
    {
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}        
        
        timer += delta;

        if(timer>=gameTime) {this.gameOver();}

        if(canSeeScores){
            textString = ""; 
            textString += 'Score: ' + score + '   Mistakes: ' + misses + '   Time left: ' + Math.floor((gameTime-timer)/1000);  
            textObject.setText(textString); //update score and timeleft, canSeeScores participants wont see their scores.
            var scaleFactor = (0.8*width)/textObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            textheight = scaleFactor*textObject.height+textObject.y; //this wouldnt be here if phaser had it's shit together.
            textObject.setDisplaySize(0.8*width, scaleFactor*textObject.height);
        }

        checksum = time;
    }

    //Function that creates the four Answer Boxes on the screen
    createAnswerBoxes(sprite)
    {
        for(var i = 0; i<4;i++)
        {
            this.createAnswerBox(i%2*0.5 + 0.25, 0.60 + (Math.floor(i/2)*0.25),sprite, i);
        }
    }

    //Function that creats an answer box
    createAnswerBox(x, y, sprite, i)
    {
        var numberBox = this.add.image(x*width,y*height, sprite);
        numberBox.setDisplaySize(0.4*width,0.2*height);
        numberBox.index = i;
        numberBox.correct = false;       

        //If the box is clicked, call the clickhandler
        numberBox.setInteractive();
        numberBox.on('pointerup', function(pointer)
        {
            this.handleClick(numberBox);
        },this);

        var nO = this.add.text(0,0,'',{ font: '100px Consolas',fill: "#0000000"});
        numberBox.numberObject = nO;

        answerBoxes[i] = numberBox;
    }    

    //Function that creates the box in the top of the screen that shows the addition
    createAdditionBox(sprite)
    {
        var numberBox = this.add.image(0.5*width,0.3*height, sprite);
        numberBox.setDisplaySize(0.75*width,0.2*height); 
        
        var nO = this.add.text(0,0,'',{ font: '100px Consolas',fill: "#0000000"});
        numberBox.numberObject = nO;   
        additionBox = numberBox;
    }
    
    // display a new number, flash the box for a short time, so the user noticess if the same number is displayed twice.
    displayNumber(numberBox, number, w)
    {
        numberBox.numberObject.setText(number);                                           // set the number       
        numberBox.numberObject.setDisplaySize(w*width,0.25*width);                     // set size
        numberBox.numberObject.setSize(w*width,0.25*width);
        Phaser.Display.Align.In.Center(numberBox.numberObject, numberBox);                // align the number in the box
    }

    //Generate the next question, and display it
    nextQuestion()
    {
        //reset all number boxes
        for(var i = 0; i<4; i++)
        {
            answerBoxes[i].correct = false;
        }

        //Get two random numbers that add up to a number between 1 and (highestNumber*2)-1
        var n1 = Phaser.Math.Between(1,highestNumber);
        var n2 = Phaser.Math.Between(0,highestNumber-1);
        
        this.displayNumber(additionBox,n1 + ' + ' + n2,0.75);

        var correctAnswer = n1+n2; //Calculate the correct answer
        var wrongAnswer1 = this.generateWrongAnswer(correctAnswer, wrongAnswerOneRange, wrongAnswerOneInTens);
        var wrongAnswer2 = this.generateWrongAnswer(correctAnswer, wrongAnswerTwoRange, wrongAnswerTwoInTens);
        var wrongAnswer3 = this.generateWrongAnswer(correctAnswer, wrongAnswerThreeRange, wrongAnswerThreeInTens);

        //make sure there are no duplicates in the wrong answers
        while(wrongAnswer2 == wrongAnswer1 || wrongAnswer2 == wrongAnswer3 || wrongAnswer2 == correctAnswer)
        {
            wrongAnswer2++;
            if(wrongAnswer2 > 2*highestNumber) { wrongAnswer2 = this.generateWrongAnswer(correctAnswer, 15);}
        }

        while(wrongAnswer3 == wrongAnswer1 || wrongAnswer3 == wrongAnswer2 || wrongAnswer3 == correctAnswer)
        {
            wrongAnswer3++;
            if(wrongAnswer3 > 2*highestNumber) {wrongAnswer3 = this.generateWrongAnswer(correctAnswer, 2*highestNumber);}
        }

        if(wrongAnswer1 == wrongAnswer2 || wrongAnswer1 == wrongAnswer3 || wrongAnswer2 == wrongAnswer3)
        {
            console.error("duplicate wrong answers exist, even though they shouldn't");
        }

        if(wrongAnswer1 == correctAnswer || wrongAnswer2 == correctAnswer || wrongAnswer3 == correctAnswer)
        {
            console.error("a wrong answer is equal to the correct answer, which shouldn't happen");
        }

        //Make an array which order the answers will be in and randomly swap it
        var answerOrders = [0,1,2,3];
        this.arraySwap(answerOrders);

        //Assign each box an answer
        var correctBox = answerBoxes[answerOrders[0]];
        var wrongBox1 = answerBoxes[answerOrders[1]];
        var wrongBox2 = answerBoxes[answerOrders[2]];
        var wrongBox3 = answerBoxes[answerOrders[3]];

        //Set the correct box to correct
        correctBox.correct = true;
        
        this.displayNumber(correctBox,correctAnswer,0.25);
        this.displayNumber(wrongBox1,wrongAnswer1,0.25);
        this.displayNumber(wrongBox2,wrongAnswer2,0.25);
        this.displayNumber(wrongBox3,wrongAnswer3,0.25);
    }

    //Simple method that randomizes an array
    //There's probably a better algorithm but this does the job
    arraySwap(arr)
    {
        for(var i = 0; i<arr.length;i++)
        {
            var newSpot = Phaser.Math.Between(0, arr.length-1);
            var temp = arr[i];
            arr[i] = arr[newSpot];
            arr[newSpot] = temp;
        }
    }

    //Generate a wrong answer based on the correct answer and a difference limit.
    generateWrongAnswer(correctAnswer, maxDifference, sameTenDigit)
    {
        if(sameTenDigit) 
        {
            return this.generateWrongAnswerWithSameTensDigit(correctAnswer, maxDifference);
        }

        if(Phaser.Math.Between(0,1) == 0)
        {
            //Calculate an answer that is above the correct answer
            return Math.min(2*highestNumber,Phaser.Math.Between(correctAnswer+1, correctAnswer+maxDifference));
        }

        //Calculate and answer that is below the correct answer
        return Math.max(0,Phaser.Math.Between(correctAnswer-maxDifference, correctAnswer-1));
        
    }

    //Generate a wrong answer with the same second digit as the correct answer
    generateWrongAnswerWithSameTensDigit(correctAnswer, maxDifferenceTens)
    {
        var ans = correctAnswer;
        var i = Phaser.Math.Between(1, maxDifferenceTens);
        
        //Quick check to make sure the answer cannot be the same as teh correct answer
        if(i == (2*highestNumber)/10)
        {
            i = 1;
        }
        
        if(Phaser.Math.Between(0,1) == 0)
        {
            //Add 10 to the number i times
            ans += i*10;
            
            while(ans>2*highestNumber)
            {
                ans -= (2*highestNumber);
            }

            return ans;
        }   
        
        ans -= i*10;

        while(ans<0)
        {
            ans += (2*highestNumber);
        }
        return ans;        
    }

    //Method that handles button clicks
    handleClick(numberBox)
    {        
        //Check if the correct button was clicked
        if(numberBox.correct)
        {
            score+=1;
        }
        else
        {
            misses+=1;
        }

        this.nextQuestion();
    }

    //start mainEndScreen scene and pass all variables needed
    gameOver()
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}