/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

//initiate the game, start fullscreen, unless the aspect ratio is wider than 0.8:1, then open 0.8:1
var config = { 
    type:Phaser.AUTO,
    height: "100%", //this somehow yields better scaling results than window.innerheight 
    width: Math.min(window.innerWidth, window.innerHeight*0.8),
    scale: 
    {        
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    //add scenes, phaser will start the mainMenu scene first.
    scene: [mainMenu, mainStartScreen, mainTutorialScreen, motorMainScene, mainEndScreen, reactionMainScene, multitaskMainScene, rapidMainScene, spatialSpanMainScene, fillingSquaresMainScene, additionMainScene] 
} ; 

var game = new Phaser.Game(config);