/* This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software and Game project course(First half of 2019)
 * ©Copyright Utrecht University(Department of Information and Computing Sciences) */

// configureable game values
var timePerNr;// time till the number switches in milliseconds
var amountOfSequences;
var sequenceLength;
var minChainLenght;
var maxChainLength;
var lowestNr;
var highestNr;


// global objects
var scoreTextObject;
var sequenceTextObject;
var numberBox;
var numberObject;
var numberTimer;
var flashTimer; // timer oject for when the next number has to be shown.
var flashTimerLength = 100;

// global values generated during gameloop
var chainSequenceLength;    // length of chain and sequence, also the point at which the user has to press.
var chain;                  // array that stores chain of numbers.
var chainNr;                // how many chains have already been passed.
var currentSequence;        // array that stores the sequence
var currentCount;           // how far that number is in the chain.
var isNotSupposedToPress;
var timeSinceSequence;

var score;
var misses;

// data from previous scene.
var canSeeScores;
var height;
var width;
var mode;
var sceneKey;

/**this game shows the user a specific sequence of numbers, which they then have to recognise in a seemingly random chain of numbers,
 * when they recognise it, they have to press the button as quickly as possible.
 *  if they miss the sequence, or press the button to early, the game registers a miss.
 * 
 * in the following code sequence means the specific sequence the user is to recognise, 
 * and chain means all the random numbers that come before it.
  */

class rapidMainScene extends Phaser.Scene{

    constructor(){
        super({key: "rapidMainScene"})
    }
    init(data){ //gets data from the previous scene
        canSeeScores = data.canSeeScores;
        height = data.height;
        width = data.width;
        mode = data.mode;
        sceneKey = "rapidMainScene";
        //load configariable values;
        timePerNr = gameConfig.rapidVisualProcessing.timePerSequence;
        amountOfSequences = gameConfig.rapidVisualProcessing.amountOfSequences;
        sequenceLength = gameConfig.rapidVisualProcessing.sequenceLength;
        minChainLenght = gameConfig.rapidVisualProcessing.minChainLength;
        maxChainLength = gameConfig.rapidVisualProcessing.maxChainLength;
        lowestNr = gameConfig.rapidVisualProcessing.lowestNr;           
        highestNr = gameConfig.rapidVisualProcessing.highestNr;
    }
    preload(){ //load assets
        this.load.image('numberBox','assets/rapid/numberBox.png');
        this.load.image('rapidButton', 'assets/rapid/rapidButton.png');
    }

    create (){
        //add background
        this.cameras.main.setBackgroundColor(gameConfig.colors.backgroundTwo);


        //add a box to display the numbers in
        numberBox =  this.add.image(0.5*width,0.5*height, 'numberBox');
        numberBox.setSize(0.5*width,0.5*height)
        numberBox.setDisplaySize(0.5*width,0.5*height);
        numberObject = this.add.text(0,0,'',{ font: '200px Consolas',fill: "#0000000"});

        //initiate the sequence and score textobjects
        sequenceTextObject = this.add.text(0.5*width,0.2*height,'',{ font: '80px Consolas',fill: "#0000000"});
        sequenceTextObject.setOrigin(0.5,0.5);

        if(canSeeScores){
            //add a background for the score
            var graphics = this.add.graphics();
            graphics.fillStyle(0xFFFFFF,1);
            graphics.fillRect(0,0,width, 0.15*width);
            //add text
            scoreTextObject = this.add.text(0.5*width,0.075*width,'',{ font: '40px Consolas',fill: "#0000000"});
            scoreTextObject.setOrigin(0.5,0.5);
        }
        
        //create the button
        var signalButton = this.add.image(0.5*width,0.85*height,'rapidButton');
        signalButton.setDisplaySize(0.2*width,0.2*width);
        signalButton.setInteractive();
        signalButton.on('pointerdown', function(){
            this.clickHandler(signalButton);
        },this)

        //reset all values before the start of the game.
        currentCount = score = misses = chainNr = 0;

        // start the first chain of numbers, and display the first number.
        this.startNewChain(true);
        this.setNextNr(true);
    }

    update (time, delta){
        //Code that makes surTooe the update only runs if it didn't already, because phaser has decided to run it twice for some fucking reason
        if(checksum == time) {return;}

        timeSinceSequence += delta;

        //display the scores.
        if(canSeeScores){
            var scoreString = "Score: " + score + "   Mistakes: " + misses;
            scoreTextObject.setText(scoreString); //update score and timeleft, research participants wont see their scores.
            var scaleFactor = (0.8*width)/scoreTextObject.width; //some custom textscaling because settextbounds apperantly doesnt work anymore in phaser 3
            scoreTextObject.setDisplaySize(0.8*width, scaleFactor*scoreTextObject.height);
        }

        checksum = time;
    }

   

    // display the sequence on screen
    displaySequence(){
        var sequenceString = "";
        //put a dash between all numbers
        for(var i in currentSequence){
            sequenceString += (currentSequence[i] + "-");
        }
        // remove the last dash
        sequenceString = sequenceString.slice(0, -1);

        //set the text in the object, and rescale it.
        sequenceTextObject.setText(sequenceString);
        sequenceTextObject.setDisplaySize(0.4*width,0.1*width);
        sequenceTextObject.setSize(0.4*width,0.1*width);
    }


    // display a new number, flash the box for a short time, so the user noticess if the same number is displayed twice.
    displayNumber(adjustTint, number){
        if(adjustTint){
            numberBox.setTintFill(0xbbbbbb);                                    // tint the box unless the user has just pressed the button.(in which case it already flashes a different color.)
            flashTimer = this.time.delayedCall(100, this.clearTint, [], this);  // set a timer to turn it back white after 100 msec
            
        }
        numberObject.setText(number);                                           // set the number       
        numberObject.setDisplaySize(0.25*width,0.25*width);                     // set size
        numberObject.setSize(0.25*width,0.25*width);
        Phaser.Display.Align.In.Center(numberObject, numberBox);                // align the number in the box
    }

    // clear the tint of the numberbox
    // needs an own method because numberbox is inaccesable through a clickhandler.
    clearTint(){
        numberBox.clearTint();
    }

    // display the next number on screen and set a timer for changing the number next time.
    setNextNr(adjustTint){
        numberTimer = this.time.delayedCall(timePerNr, this.setNextNr, [true], this); 
        //if we are at the end of the chain, start a timer to measure how fast the user observes the sequence.
        if(currentCount == chainSequenceLength-1){
            //reset the timer.
            timeSinceSequence = 0;
            isNotSupposedToPress = false;
        }
        //if the user didnt recognise the sequence, start a new chain, with the same sequence.
        if(currentCount >= chainSequenceLength){
            this.startNewChain(false);
        }
        this.displayNumber(adjustTint,  chain[currentCount]); //display the current number
        currentCount++; //keep track of where we are in the chain.
    }


    //start a new chain, either with the old sequence, if the user has wrongly identified the sequence, or if the user has not yet spotted the sequence.
    //if the user spotted the sequence, start a chain with a new sequence.
    startNewChain(needNewSequence){
        //end the game after a the user has guessed x sequences right.
        if(chainNr >= amountOfSequences){
            if(needNewSequence || !gameConfig.rapidVisualProcessing.countMistakes){

                this.gameOver();
            }
            else
            {
                misses++;
                this.gameOver();
            }
        }
        // if the user recognised the sequence, change it and display it
        if(needNewSequence){
            currentSequence = this.generateSequence();
            this.displaySequence();
            chainNr++;
        }
        else{
            misses++;
            if(gameConfig.rapidVisualProcessing.countMistakes)
                chainNr++;
        }

        //start a new chain, make sure that if the user presses now, it is a miss again.
        isNotSupposedToPress = true;
        chain = this.generatechain();
        currentCount = 0;
    }

    
    // count the score if someone presses the button
    clickHandler(){
        // if the player presses correctly
        if(!isNotSupposedToPress){ 
            score += Phaser.Math.RoundTo(timeSinceSequence, 0);     //add the time to their score
            this.startNewChain(true);                               // start a new chain of numbers with a new sequence
            numberBox.setTintFill(0x88ff88);                        // flash the box green
        }
        //if the player presses while not supposed to
        else{
            //misses++;                                               // add 1 to their misses
            this.startNewChain(false);                              // start a new chain of numbers with the old sequence                     
            numberBox.setTintFill(0xff8888);                        // flash the box red
        }

        //add a new timer to set the numberbox white again.
        flashTimer.remove();
        flashTimer = this.time.delayedCall(flashTimerLength, this.clearTint, [], this); 

        numberTimer.remove(); //remove the old timer, because setNextNr will start one again.
        this.setNextNr(false);
    }

     //generate a chain of numbers that only contains the sequence once at the end. 
     generatechain(){
        // chainlength without the sequence
       var chainLength = Phaser.Math.Between(minChainLenght, maxChainLength);
       chainSequenceLength = chainLength + sequenceLength;
       
        // generate a chain of random numbers of a random length shorter than the maxchainlength
       chain = [];
       for(let i = 0; i < chainLength; i++){
           chain.push(Phaser.Math.Between(lowestNr, highestNr));
       }

       // repeat over the chain to see if the sequence is accidentaly in it until it is confirmed empty of duplicates
       var noDuplicateSeq = false;
        while(noDuplicateSeq == false){
            noDuplicateSeq = true;
            // loop over the entire chain
            for(let i = 0; i < chainLength; i++){
                var duplicateCounter = 0;
                // if you encouter the first digit of a sequence, count how many are duplicate.
                while (chain[i+duplicateCounter] == currentSequence[duplicateCounter] && chain[i+duplicateCounter] != undefined ){
                    duplicateCounter++;
                    // if there are more duplicates than the chain is long, replace that with a random number, and check the entire chain again.
                    if(duplicateCounter >= sequenceLength){
                        //choose a new random number as first number of the discovered sequence.
                        chain[i] = Phaser.Math.Between(lowestNr,highestNr);
                        noDuplicateSeq = false;
                    }
                }
            }
        }
      
       // put a generated sequence behind that.
       chain = chain.concat(currentSequence);
       return chain;
    }

    //generate a random sequence of numbers of lenght sequence length
    generateSequence(){
        var sequence = [];
        for(let i = 0; i < sequenceLength; i++){
            sequence.push(Phaser.Math.Between(lowestNr, highestNr));
        }
        return sequence;
    }
    
    gameOver() //start mainEndScreen scene and pass all variables needed
    {
        this.scene.scene.scene.start('mainEndScreen', {score : score, misses: misses, canSeeScores: canSeeScores, height: height, width: width, mode: mode, sceneKey: sceneKey});
    }
}