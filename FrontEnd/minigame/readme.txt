this minigame is writen in phaser 3
phaser 3 is relatively new, so a lot of info you will find online is for phaser 2, which might be outdated.
however, they have a website with lots of code examples: http://labs.phaser.io/

possibly usefull tutorials:
https://www.youtube.com/watch?v=7cpZ5Y7THmo

running the project:
to run the project you will need to host it locally, opening the http doesnt suffice.
their website advices WAMP server, although i found http-server for node js to work and be installed much easier.
to install this run 'npm install http-server -g' in console
to run the server run 'http-server *directory you want to serve*'
the server will run the game in localhost:8080/yourindexhtmlfile.html
you can give some variables throught the url by adding '?variable=value&anothervariable=anothervalue' to the url
currently(22 march) the available variables are userID, userName, research
research is a bool which you can include by simply adding '?research' to the url

dont break your head on the same things i did:
(insights that might be usefull if you start coding minigames)
- *object*.setSize(x,y) and *object*.setDisplaySize(x,y) are two seperate functions, but should probably be used together if you dont want weird shit to happen
	- setDisplaySize changes the size on screen
	- setSize changes the size other functions assume the object is
	- if you want to use alignment, use them together
- if you want to draw text onscreen, you make a textobject, which you give a certain string
- scenes get data from each other through scene.scene.start('somescene', {somename: somedata, someothername: someotherdata})
	- to get this data every scene should have a init(data) which can get the values trough data.somename

TL;DR
run 'npm install http-server -g'
run 'http-server C:/ -somestuff- /motor-task-game'
open browser on 'localhost:8080/motor-task-game.html' or 'localhost:8080/motor-task-game.html?userName=someuser&research'


